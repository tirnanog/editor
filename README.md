TirNanoG Role Playing Game Editor
=================================

<img src="https://codeberg.org/tirnanog/editor/raw/main/src/misc/tnge.png">

[TirNanoG Editor](https://tirnanog.codeberg.page) is designed to create *Adventure and Action RPG Games* without the
need for programming skills. For more information, visit the [homepage](https://tirnanog.codeberg.page).


Since the output [game.tng](https://codeberg.org/tirnanog/editor/src/branch/main/docs/tng_format.md) files contain only data,
they are 100% virus-free and multiplatform. This means end users have to install a native [TirNanoG Player](https://tirnanog.codeberg.page/player)
application only once, then they can play as many games as they want. And you, the developer don't have to create games for each
platform, the same game file will work on Linux, Windows and on Android as well.

The editor is mainly used on Linux, however it should work under Windows too. If you run into any trouble, feel free to use
the [issue tracker](https://codeberg.org/tirnanog/editor/issues). There's an experimental MacOS port too, but officially
not supported (at least not yet).

IMPORTANT NOTE: some people on the internet falsely claims that TirNanoG can't be used for FOSS games. Know that they are liars,
and if you're in doubt, simply ask them to quote the part of the license that supposedly disallows FOSS games, and you'll see
that they won't be able to answer.

Installation
------------

### Windows

1. download [tnge-i686-win-static.zip](https://codeberg.org/tirnanog/editor/raw/binaries/tnge-i686-win-static.zip)
2. extract it to `C:\Program Files` directory and enjoy!

This is a portable executable, no installation required.

### Linux

1. download [tnge-x86_64-linux-static.tgz](https://codeberg.org/tirnanog/editor/raw/binaries/tnge-x86_64-linux-static.tgz)
2. extract it to the `/usr` directory and enjoy!

Alternatively you can download the [deb](https://codeberg.org/tirnanog/editor/raw/binaries/tnge_1.0rc-amd64.deb)
version and install that with
```
sudo dpkg -i tnge_*.deb
```

Dependencies
------------

- libc

Yeah, no mistake! This is how a [suckless](https://suckless.org) project looks like! For compilation from source, see the
[readme](https://codeberg.org/tirnanog/editor/src/branch/main/src/README.md) in the `src` directory.

Unlike the tarball, the deb version depends on the SDL2 shared library too, but that's where the list of dependencies ends.

Using the Editor
----------------

Just run `tnge`. Please see the [TirNanoG Editor User's Manual](https://tirnanog.codeberg.page/manual.html) for details.

TirNanoG Editor PRO
-------------------

The binary you can download contains the professional version of the editor. The one and only difference is, the pro version
can save an encrypted game file and can generate decryption keys for it (which you can sell to your end users), while the GPL'd
version can only generate unencrypted game files, see licensing below. In every other aspect the GPL'd version is *exactly the
same* as the pro version. There's no such distinction with the [TirNanoG Player](https://tirnanog.codeberg.page/player); only
one GPL'd version of the player exists, which supports both encrypted and unencrypted game files equally.

Known Issues
------------

- You cannot import scripts from game files (yet). The disassembled bytecode is printed in comments, you have to rewrite the
  scripts using that info.

- There's place for hexagonal tiles and maps, the file format can store it, the editor has functions and icons for it, however I
haven't impemented all the required aspects, so this is disabled for now. Probably soon.

- With Adobe Palette formats (.aco and .ase), only RGB color space was tested. Grayscale, HSB, LAB and CMYK should work too,
but have never been tested.

- I wrote the Model 3D and Wavefront OBJ model importers, so those are supported by me. On the other hand all the other
formats use Assimp, which may or may not load the models correctly. If they work that's great, but if they don't then I
can't and won't give support for those formats. Ask around on Assimp's forum.

License
-------

IMPORTANT NOTE: the program's and the file format's license has nothing to do with the game's license stored in this format:

- editor: copyrighted to me, Free and Open Source.
- file format: copyrighted to me, and dual licensed depending on if the file is encrypted or not.
- your game: copyrighted to *you*, and it is entirely up to you what license you choose for your game. For commercial games I
  recommend the proprietary file format option as it gives you not just encryption but a legal guarantee too that your assets
  in the .tng file can't be stolen.

The TirNanoG Editor is a Free and Open Source software, licensed under *GPLv3* or any later version of that license. By default
it allows creating unencrypted game files under *CC-by-nc-sa* file format license. If you want to create a game for fun and share
with friends or with the Open Source community, go ahead, you can do it with this editor for free! (Note: the license of the code
of the program does not apply to the program's output, the output's license is governed by the
[TirNanoG File Format Dual-License](https://codeberg.org/tirnanog/editor/src/branch/main/docs/tng_format.md) and that only.)

On the other hand to create encrypted game files with this editor,
[you must ask for permission](https://codeberg.org/tirnanog/editor/issues/new)
from the file format's author in a *confidential* issue or in an email. In return I'll send you a file that has to be copied under
the game project's folder. This will unlock the creation of encrypted game.tng files and generation of end user license keys
within the editor.

**Assets used in the game files** are entirely the resposibility of the editor's users, and has nothing to do with the editor's
nor with the file format's license. The editor does not ship any of these assets, and it provides an easy way of embedding
copyright info and asset author attributions into the game files (which in turn are displayed by the TirNanoG Player to end
users), but it is the resposibility of the editor's users to fill in those info correctly.

The decision and the responsibility is on the user using the editor. Under no circumstances can I, bzt, the TirNanoG Editor and
TirNanoG File Format's author be held responsible for the game files created by others, encrypted or not.

Authors
-------

- [SDL](https://www.libsdl.org) (zlib) Sam Lantinga
- [SDL_mixer](https://www.libsdl.org/projects/SDL_mixer) (zlib) Sam Lantinga
- [xcf parser](https://github.com/libsdl-org/SDL_image/blob/main/IMG_xcf.c) (from SDL_image, zlib) Sam Lantinga
- [freetype2](https://freetype.org) (GPLv2) David Turner, Robert Wilhelm, and Werner Lemberg
- [libmpg123](https://www.mpg123.de) (LGPLv2.1) Michael Hipp and others
- [libpng](https://www.libpng.org/pub/png/libpng.html) (zlib) Guy Eric Scalnat, Andreas Dilger, Glenn Randers-Pehrson, Cosmin Truta and others
- [libtheora](https://theora.org) (BSD-3clause) Xiph.org Foundation
- [libvorbis](https://xiph.org/vorbis) (BSD-3clause) Xiph.org Foundation
- [libwebp](https://github.com/webmproject/libwebp) (BSD-3clause) Google Inc.
- [libxmp](https://github.com/libxmp/libxmp) (MIT) Claudio Matsuoka and Hipolito Carraro Jr
- [mbedtls](https://tls.mbed.org) (Apache-2.0) various contributors
- [stb_image](https://github.com/nothings/stb) (PD) Sean Barrett and others
- [SSFN2](https://gitlab.com/bztsrc/scalable-font2) (MIT) bzt
- [TinyGL](https://github.com/C-Chads/tinygl) (BSD-3clause) Fabrice Bellard, Gek, C-Chads, modified by bzt
- [Model 3D](https://bztsrc.gitlab.io/model3d) (MIT) bzt
- [zlib](https://zlib.net) (zlib) Jean-loup Gailly, Mark Adler
- [on-screen touch controls](https://thoseawesomeguys.com/prompts) (CC0) Nicolae Berbece
- [character demo](https://github.com/flareteam/flare-game) (CC-by-sa) Clint Bellanger, modified by bzt
- [Celtic md](https://www.fontmeme.com/fonts/celtic-md-font/) font (free, even for commercial use) unknown origin, modified by bzt
- [univga](http://www.inp.nsk.su/~bolkhov/files/fonts/univga/) (MIT) Dmitry Bolkhovityanov
- [unifont](http://unifoundry.com/unifont/index.html) (GPLv2+ with the GNU font embedding exception) Roman Czyborra, Paul Hardy and others
- all the other assets (GPLv3+) bzt (with some icons borrowed from the GIMP)
- all palette parsers (GPLv3+) bzt, no third party libs used
- zip implementation (GPLv3+) bzt, no PKZIP sources nor third party libs used
- TirNanoG Editor (GPLv3+) bzt
- TirNanoG File Format (dual licensed CC-by-nc-sa or anything else, including proprietary) bzt

NOTE: by the terms of GPL section 5c, all these libraries and assets embedded in the TirNanoG Editor are re-licensed under
GPLv3+ (or any later version of that license). I've paid extra attention not to include anything GPL licensed into the generated
game files, those contain exclusively user-provided assets only.

NOTE: the original character demo animation was licensed CC-by-sa-3.0 with permission to re-license under any later version
of that license. My derivative therefore is licensed under CC-by-sa-4.0 which allows embedding in a GPLv3+ code as it is
[one-way compatible](https://creativecommons.org/share-your-work/licensing-considerations/compatible-licenses) with GPLv3+.

Cheers,
bzt
