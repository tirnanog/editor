<h1 tab27 attributes>Attributes</h1>

Configuring Attributes
----------------------

Go to `Game` > `Attributes`. On the left you'll see the list of already configured attributes. Double-clicking on one
of them will load that attribute.

Attributes are the essential building blocks of your game, that describe the current state. They can belong to characters,
NPCs, objects or to the world.

Attributes Setup
----------------

By clicking on the <imgt ../img/setup.png> "Attribute setup" button at the center bottom, you'll see the setup form. Here you can
assign attributes to the engine. There's only a few of them, like which attribute stores the player's speed or the light's
radius for light sources, or the level up attribute which you should add points to when the character advances a level.

This is also the place to configure the attribute limits in free-form mode. Read about these in [free-form mode characters].

Attribute Properties
--------------------

As for the non-setup mode, you can specify the attribute here. Each attribute has an internal name used inside the editor,
a translatable title, a type and some additional information. If title is not given, then the attribute will be hidden from
the user. When given, then it will be listed with its current value in-game, on the inventory's "Stats" tab. You can also
display attributes as [progressbars] on the Heads-Up Display. As for the types, you can choose primary, calculated, variable and
global.

<h3 attr_pri>Primary</h3>

These are primary attributes that influences all the other attributes of a character (or NPC). In free-form mode the user are
free to assign values to these. They are configured during character generation, and either not modified later, or just by
special events, like level up. Typically "Strength", "Intelligence", etc.

<h3 attr_var>Variables</h3>

The are the typical attributes, used throughout your game logic. Event handlers can change their values directly, using the
<imgt ../img/let.png> "assign" command, and they also have their own "on change" event handlers.

Typically skills that the players learn during the game are stored as variable attributes, like "swordfighting", "lock
picking", etc. But it is up to you how you set up attributes in your game.

<h3 attr_calc>Calculated</h3>

These are defined by an expression, and their values are always calculated, and cannot be directly modified, only indirectly,
through modifying the attributes their expressions depending on. Typically "HP", "HPmax", "MP", "MPmax" etc. which rely on
certain primary values.

<h3 attr_glob>Global Value</h3>

Just like the variable attributes, they have a default value, they can be modified directly with the <imgt ../img/let.png> "assign"
command, and they have event handlers. The only difference is, while variable attributes belong to the characters (or NPCs or
objects), global attributes belong to the world. They are useful to store the status of your overall, player-independent story line.

Typically things like a castle's gate is open or not, the king has dispatched the knights or not, how many days left before
the princess starves to death in the tower etc.

<h3 attr_glob_calc>Global Calculated</h3>

Similar to per-character caluclated attributes, but for world attributes.
