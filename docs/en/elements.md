<h1 tab19 ui_elements>UI Elements</h1>

Specifying the Game's Look'n'Feel
---------------------------------

Go to `Interface` > `Elements`. Here you can specify all the UI elements, therefore it is one single form with a bunch of
sprite and font selector buttons.

There's really not much to talk about this one. You have to select which sprites to be used for the UI. It is a boring and
repetitive task, but has to be done only once.

Preview
-------

By clicking on the <imgt ../img/preview.png> "Preview" button at the bottom, you'll be able to see how the selected sprites will
make up the interface. You'll get an example window with example tabs, inputs, scrollbars, etc. Below the window, there'll be
an example dialog box.

If you have set cursor sprites too, then on the top left corner you'll see three hotspots (with slightly different background
colors, redish, greenish and blueish). Hovering the mouse over them will show you how those cursor sprites look (including
animation, handy for the loading cursor for example).

The preview IS NOT a working user interface. Only the checkbox and the buttons can be pressed to check how the pressed state
looks. You won't be able to move the slider or the scrollbars.
