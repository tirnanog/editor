<h1 tab13 sprites>Sprites</h1>

HINT: If you don't want to load your own custom sprites, you can also use the sprite [generator].

<h2 tab0 importing_sprites_from_images>Importing Sprites from Images</h2>

Go to `Assets` > `Sprites`. You'll see the list of available sprites here. Note the category selector at the bottom middle,
here you can filter the sprites. The categories are as follows:
- <imgt ../img/sprcat0.png> user interface related sprites
- <imgt ../img/sprcat1.png> tiles and on map object sprites
- <imgt ../img/sprcat2.png> character and equipted object sprites
- <imgt ../img/sprcat3.png> portraits in dialogs
- <imgt ../img/sprcat4.png> fullscreen background images

You can change the category here, but also changeable later before you do the actual import. Now click on the <imgt ../img/add.png>
Add button in the bottom right corner.

The screen will change and sprite sheet specification window will appear. Here you can see a toolbar on the top. The icons are
as follows:

- load an image as sprite sheet
- change the palette
- resize to half
- resize to double
- flip image vertically
- flip image horizontally
- grid specification (a checkbox and multiple number inputs)
- layout (a combobox and a save icon)
- sprite's name
- erase sprite from import list

Below the toolbar you'll see the sheet image on the left (big area) and the selected sprites list on the right.

A Typical Workflow
------------------

<imgw ../img/sprimg.png>

1. Load an image file.
2. From the layout drop-down, select the desired sprite layout.
3. When the layout is chosen, the import list will be filled up with animated sprites automatically.
4. If the import list preview looks okay, press <ui1>Import</ui1> to add sprites to the project.

Loading the Sprite Sheet
------------------------

Click on the <imgt ../img/load.png> "Load Sprite Sheet" icon in the top left corner (first icon on the toolbar, but also duplicated
on the button bar for easy access).

A sprite sheet is an image with several sprites on it. These can be independent sprites, or animated frames of the same sprite.
You can also import 3D models and generate sprites from those. For more details, see [Models].

Palette Conversions
-------------------

Many designers like to create games with limited colors to provide a consistent look-n-feel. To help you with that, you can
restrict the loaded image to a certain palette.

Palettes can be loaded from all common formats (.gpl, .ase, .aco, .pal etc.) and from images. With images for the best results,
you should use an indexed image (gif or quantized png), or a true-color image that has no more than 256 different colors in it.
Dimensions and number of pixels doesn't matter, only how many different pixels there are.

When finding a match for a color on the sheet to a color on the palette, you have two options: either use sRGB distance (fast,
mostly good results, but in some very rare edge cases makes bad matches) or CIE76 (slow, works with converting both the image and
palette colors to CIE LAB color space first and then calculating the color distances there).

If you're using non-PNG images, then for some exotic formats the color channels might be swapped. You can fix that with the
<imgt ../img/swaprgb.png> "Swap Red and Blue channels" button. Should not be needed, just in case.

Basic Conversions
-----------------

You can resize to half or double, but not to arbitrary sizes. You should load the image with proper dimensions, but it might
happen that your game uses 32 x 32 sprites but you only have 64 x 64 sheets. In that case to avoid using a third party imaging
tool, you can quickly resize. Both the downscaling and upscaling uses bilinear interpolation, so the results should be good.
However keep in mind that upscaling images are always bad, try to avoid that as much as you can.

For portraits, they can be on the dialog's left (face looking to right) as well as on the right side (face looking to the left).
The editor and the player does this conversion for you, so you must import all portraits as face *looking to the left*. It might
happen that you only have a portrait facing in the wrong direction. To avoid the use of third party tools, you can quickly flip
the image here with a single click.

Some exotic formats stores the image up-side-down. The image loader will take care of that, but if not, you can press the flip
image button to quickly fix that. Should not be needed, just in case.

These operations are for quick fix only, the editor does not (and never intended to) replace a sophisticated image manipulation
tool.

Specifying the Sprites
----------------------

The next on the toolbar is the grid specification. You can uncheck the checkbox and use the entire image as one sprite. Or
you can have the checkbox checked and specify a grid of sprites. It is pretty straightforward, play with the width, height,
margin and padding values to see what areas are marked for possible selection on the sheet.

If the sprite sheet does not fit in the box, you can move it around by <mbr> right clicking on it.

### Single Sprites

On the right, there's the list of selected sprites. If you haven't clicked on any, or clicked on the topmost "New sprite" icon,
then each time you make a selection on the sheet with <mbl> left button, a new sprite will be added. This is ideal for selecting
tiles from a tilemap for example.

### Animations

To import animations, first you have to select the first sprite using "New sprite". Then you must click on the sprite in the
list on the right for which you want to add further frames (or use the <mbw> mouse wheel). This might sound complicated, but
actually doing it is not: frames are added to the sprite which is selected on the right.

HINT: After adding the first frame of a sprite, press the <kbd>⯆</kbd> down cursor key, and then you can select its further frames.
When finished, press the <kbd>⯅</kbd> up key to go back to add new sprite mode.

When a sprite in the list on the right is selected, you'll be able to see it as an animation. You can also set the animation's
type (play once, play forth and back, play in a loop) and its direction (which will help you a lot later filtering the sprites).
Animations that happen in place should be marked as "south", otherwise you have 8 directions to choose from. You don't have
to use all the directions, it is possible that a certain game only uses 4 for example.

### Larger Sprites

Not all sprites are equal in size, some are larger. For that you can select multiple adjacent grid blocks on the sheet, just
<mbl> left click on the first block, hold the mouse button down, move the cursor to increase the selection, and when you're happy
with it, release the mouse button. For animations, all frames must be the same size.

### Deleting

If you have accidentally added a bad sprite from the sheet, you can select it on the right and click on the <imgt ../img/erase.png>
Erase icon above the list, that will remove the entire sprite. Pressing the <kbd> ⇦ </kbd> Backspace key will delete the last
frame from the selected sprite, and only remove the sprite if no more frames left.

Layouts
-------

Specifying the grid and sprite selections might be very time consuming and repetative. To help you with that, you can save
the sprite layout: on the toolbar, right to the grid specification, you can enter a layout name and press the <imgt ../img/save.png>
Save icon.

Next time you load a sprite sheet image, you'll be able to click on the layout name combobox, and select the layout from a
dropdown list. After that, all the grid and sprite selections will be applied to the new sprite sheet image, and selected
sprites will appear in the list on the right automagically.

Even more, when applying a layout on the entire image and if the image has a 8 x 16 palette at its bottom right corner, then
dynamic [recoloring] will take place. With this you can import a big variety of different sprites using the same sprite sheet.
You can also select the "?" (ask) color variant, in which case the palette will be imported separately, and you can set it on
the [character] options. With this, the player will be able to select the color variant during character generation.

You can also specify which part of the sheet should be used with a layout. If you hold down <kbd>Shift</kbd> and make a selection
with the mouse <mbl> left button, then areas outside of that selection will turn red. When you choose a layout from the combobox,
those red parts won't be parsed at all. This is useful if you have multiple for example terrain types on an atlas, each with its
own wang-set, and therefore should be parsed separately. You select the first terrain type, then apply the layout, and import
the sprites. Then you select the second terrain type, apply the layout and import etc., no need to cut up the atlas into separate
terrain sheet images apriori.

Do the Actual Import
--------------------

When you're satisfied with the selected sprites on the right, you have to give a name to them in the input box above the list.
Sprites witout a name will be imported with a "_xxx" suffix where the "xxx" is a serial number from 001 to 999.

You got that selected list, you've given a name to the list (input box above), and named the sprites (above each sprite image
in the list), you've double checked that you're in the correct category (icons at bottom middle), then you can press on the
<ui1>Import</ui1> button. After that you can go back to the sprites page or load a new sheet to import further sprites.

Working with Atlases
--------------------

Normally the editor saves each sprite in a separate PNG file (with frames stored horizontally), which is good for compatiblity
if you want to organize, copy or edit sprites with a third party software. However having lots of small files could slow down
project loading considerably. To speed that up, the editor supports sprite atlases. It can not only load these atlases, but it
is also capable of doing the sprite packing and unpacking on its own, no third party tools required.

Atlas packing `-p` reads in all PNG files in the current directory which start with `(atlas)`, and saves `(atlas)_atls.png`.
For example, assuming you have sprites like `sword_walk_soo6.png`, `sword_walk_weo6.png`, `sword_walk_noo6.png`,
`sword_walk_eao6.png`, `sword_slash_soo6.png`, `sword_slash_weo6.png`, `sword_slash_noo6.png` and `sword_slash_eao6.png`, then
```
tnge -p sword
```
will pack all of these into `sword_atls.png`. Atlas meta info is stored in PNG comment.

Unpacking `-u` does the opposite, it reads in `(atlas)_atls.png`, and saves separate sprite PNG files. For example
```
tnge -u sword
```
will read in `sword_atls.png` and will save the aforelisted PNG files.

If you're just interested which sprites are stored in an atlas, but don't want to unpack, you can use
```
tnge -l sword
```
to list the sprites.

WARNING: Never modify these sprite atlases in a third party pixel editor, because you might loose the atlas meta info.

WARNING: Although the TirNanoG Editor saves atlas info with the tilesets into the [maps], Tiled does not support that, so it is
commented out for now. This means you *must* unpack any tile and object atlases if you intend to edit the maps in Tiled as well.
