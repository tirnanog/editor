<h1 tab1 translations>Translations</h1>

Providing Translations
----------------------

Select the `Translations` menu. In the drop down, you can select one of the existing translations, or you can choose
the "New language" for a new language.

Identifier
----------

On the top right corner, you'll see the language's two letter ISO-639-1 code, and its native name. The code is like "en",
"es", "ru", "de", "ja" etc. The name is always the one how the language is called by its native speakers, so for example
it's not "German", rather "Deutsch". Or with another example, it's not "Japanese" rather it must be "日本語". There are some
codes and names predefined for the most commonly spoken ones and for the languages of the EU, meaning when you enter the code,
the name field gets filled in automatically. You can of course modify that name if you wish.

Specifying Speech
-----------------

Some audio files are special in that they contain speech which might be translated. All audio files that are categorized as
such will be listed here, and then you can assign a translated audio file for each. It is not 100% possible, but always try
to keep the translated audio files to have the same duration as the original ones. A few seconds more or less doesn't matter,
but much more like double the duration can cause troubles in cutscenes where the speech uses precise timings.

Specifying Text
---------------

Below the audio block, you can see all the texts in your game. These are collected automatically, so if you modify your game
(for example you add a new object), then the list will change accordingly.

You can use this interface to fill in the translated texts. Pasting from another application (like from a text editor) works as
expected. But if you find it problematic, then you can save the translation to a file. It is stored in a format that resembles
.po files, so you should have no issues translating them with your usual tools. Keep in mind that the comment in the first line
is MANDATORY, do not change that. Also the first id-string pair must be the language code and the translated name of the language,
and the second is the title of the game.

When you're finished with translating the texts, don't forget to press <ui1>Save</ui1>.
