<h1 tab33 objects>Objects</h1>

Configuring Objects
-------------------

Go to `Game` > `Objects`. By default objects are just roadblocks on the map. If you want the player to interact with them,
you have to add meta information too. On the left you can see all the objects that already have such a meta info. Double-clicking
on the list will load that object. To load an object that does not have meta info yet, use the object sprite chooser on the
tool bar.

NOTE: Beneath the object list, you can see the usual <imgt ../img/del.png> "Delete" button, but there's also a <imgt ../img/dupobj.png>
"Duplicate object" input box and button. Give a name to the copy and press the button, that will duplicate not only the sprite,
but all the meta info as well.

On the tool bar you can see the object sprite chooser, the "On map" icon and the "In inventory" icon. These icons will switch
tabs, because there are quite a lot configuration options for objects, did not fit into one page.

Objects on Map
--------------

The simplest one is when the object is on map, like a wall or a street lamp. These have no in-game name. All you need is a
collision mask for them, which can be set on the right. Select the sprite and then you'll see the grid with the transparent
image of the object.

### Collision Mask

<imgr ../img/coll.png>Visibility (transparency of the object) can be changed by clicking on the "Visibility" icon above the mask.
It has `25%`, `50%`, `75%`, `100%` steps, <mbl> left clicking goes in ascending direction, while <mbr> right clicking goes in
the other, descending direction.

On the grid, you can click on each cell individually. Here <mbl> left clicking changes the status forwards, while <mbr> right
clicking changes it backwards. Each cell can be of different collision:
- empty means walkable and flyable but not swimmable
- <imgt ../img/block.png> wall means block, not walkable, not swimmable and not flyable
- <imgt ../img/swim.png> ship means not walkable but swimmable and flyable,
- <imgt ../img/fly.png> airplane icon means not walkable and not swimmable but flyable,
- <imgt ../img/climb.png> a ladder means walkable, but played with a different animation,
- <imgt ../img/jump.png> a jumping man means not walkable, not swimmable but flyable; except with jump it is walkable,
- <imgt ../img/above.png> half wall below means block underground, but walkable above,
- <imgt ../img/below.png> half wall above means walkable underground, but block above.

For simple objects on the map, you don't need events at all, but by toggling the blue board game figure "Event" icon
(<imgt ../img/event.png>), you can specify the event trigger hotspots for the object.

<h3 obj_event_handlers>Event Handlers</h3>

If you specify event trigger hotspots, then you can set up events handlers on the left. These are exactly like for [NPCs],
except where NPCs have behaviour, objects have an action category. This category tells the engine that when the object is
equipted and being used, which one of the [default action handlers] should be executed.

Let's say you have configured "action 1" as a melee attack, and "action 2" as a ranged attack. Then for a sword object, you want
to choose "action 1" category, while for a bow "action 2". This only selects the default event handler (when the player is using
that object), but you can add further event handlers for other interactions.

For example to automatically pick up items, one would need to specify an "on touch" event handler with a "remove object from map"
and "give item to player" commands.

### Equipted Objects

When objects are equipted, they might alter the character's outlook. This is optional, and only make sense with clothes, weapons,
shields, etc. Below the collision mask, you can configure this.

First, you can use a common outlook for all character groups (if you choose "(default)"), or you can set one for each, if you
select a specific character option (the available options came from the [option group] that is marked as "base look out").

There are only two layers for equipted objects: one that's above the character sprite layers, and one that's behind. You can set
up sprites for each standard actions (idle, walking, swimming etc.), but the action sprite is only available if you have chosen
an action category for the object.

<h3 obj_onmap_preview>Preview</h3>

With the <imgt ../img/preview.png> "Preview" button, you can check how an equipted object looks like on the characters. This looks
and works exactly like the preview for the character options, for more detials see the [option sprites] in the character chapter.

In Inventory
------------

On this tab, you can configure how an object looks like in the player's inventory. If the object can be picked up (or is a
virtual one, like a magic spell), then you must set a translatable in-game name for it in the "Title" field.

If you want, then in the inventory you can set a different sprite for the object than the sprite used when the object is on the
map. This can be set on the right in the same row as the "Title". This sprite should be the same size as the slot size specified
for the [inventory] slots in the `Interface` > `HUD` menu.

On the left, you can configure where the object can be equipted at, and if you want to have specific sprites, with what sprites.
Below are two inputs, one for the sound effect thats played when the player places the object in an inventory or equipment slot,
and the other being the object's base price (with units, see [creating a merchant]). You can specify the price category as you
wish, it is entirely up to you, it is optional. For example, you could say that category 1 is meat and 2 is hardware, and a
certain weaponsmith only buys items in category 2, but not items in category 1.

On the right, you can set up the object's attributes.

<h3 obj_equip_objects>Equipted Objects</h3>

Some objects might be equipted. You can check here in which equipment slots (slots are defined in the [inventory], see
`Interface` > `HUD` menu). There's a special equipment slot, the "skill". If you check this, then the "object" will be treated
not as a material object, but a fictionary; it will be shown on the Stats tab of the HUD and not on the Inventory tab. These
cannot be equipted as normal object, yet they need a place on the player's item bar so that they can use them and access them by
a shortcut key. In a fantasy RPG, these are typically used for magic spells, but you can use these "skills" any way you like in
your game.

By default, the same object sprite will be shown in the equipment slot as in the inventory slot. However there are some
objects that require multiple equipment slots, in which case you might want to use different sprites in the equipment slots.
For example a bow would require both hands, but showing the same bow icon in both hands looks strange. Therefore you can keep
the default icon for the primary hand, and use an arrow icon for the secondary hand for example.

Here you can set the price for the object, if you want to. Unit is freely configurable depending on your game, for example
it could be a gold coin.

Finally, if the selected action category is a ranged one, then you should specify a projectile sprite for the object (like
an arrow for a bow, or a fireball for a magic wand), and if it requires ammunition or not.

### Object Attributes

Not all objects can be equipted by all players. Some require a certain skill or level. To describe that, you can fill up the
"Required attributes" list on the top right. If the player has all the attribute requirements met, then they can equip that object.

Furthermore, when objects are equipted or used, they might alter the character attributes temporarily. For example a longsword
might add a modifier to the "Attack" attribute. Or a magic wand might increase the "MPmax" attribute, etc. These are configured
in the "Attribute modifiers" list. You have two options how to change a value: by adding a constant to it, or increasing by
a percentage.

There's one special modifier, which does not modify any attribute, rather changes how the player moves, and it is called
"(transport)". For example, in a fantasy RPG the player could find a dragon's wing, and when that wing is equipted that may
alter the transportation mode from "walking" to "flying".

<h3 obj_ininv_preview>Preview</h3>

If you click on the <imgt ../img/preview.png> "Preview" icon when the "In inventory" tab is active, then you'll see how the object
will look like in the inventory.

When you've finished configuring the object, don't forget to press <ui1>Save</ui1>.
