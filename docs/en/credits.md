<h1 tab23 credits>Credits</h1>

Specifying Authors and Attributions
-----------------------------------

Go to `Interface` > `Credits`. It is one single form with all the relevant options.

This page looks very similar to the [main menu]: you can specify the background music, the background image, the font's
size, style, color etc.

Just like the other menu entries under the Interface menu, this also has a <imgt ../img/preview.png> "Preview" button at the bottom.

<h2 credit_bg>Background</h2>

To select the background music and image.

Header
------

Specifies the category header's font.

<h2 credit_font>Font</h2>

The normal font to be used with authors' names.

Category
--------

Selects the author list for that category.

Authors
-------

When you select a name, the <imgt ../img/erase.png> "Erase" button will became active and you'll be able to remove. If you enter a
name into the input box below the list and click on the <imgt ../img/add.png> "Add" button, then the author's name will be added to
the list lexicographyically ordered.
