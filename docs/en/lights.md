Lights
======

Ambient Lights
--------------

First of all, there's a daylight component to each and every map, in `Map` > `Map meta info`, see [maps]. It is specified by two
variables:
- length of the daylight in minutes
- and a sprite with the light map

The sprite should be N x 1 pixel. The overlay's color is taken from this sprite depending on the time of the day modulo sprite's
width. If that pixel is fully transparent, or if the sprite isn't set, then there will be no light overlay. If that pixel is
fully opaque black, that means pitch dark and then the light overlay will hide everything in the game area. You probably never
want to use a black pixel with more than 75% alpha, so that the users can see at least something without a light source.

![A daylight sprite example](../img/daylight.png)
Setting the daylight length to 20 minutes and providing a sprite that has a gradient with 75% black (midnight) to 50% orange to
25% orange (morning) to full transparent (day, afternoon) to 25% orange to 50% orange (evening) and then to 75% black (night)
again will provide a nice time of the day effect to the game by mimicing sunlight's colour and dark at night.

For dungeons without sunlight, you want to set daylight length to 1 minute and provide a sprite with a single pixel
of 75% transparent black. This means everything would be dark, except some circles around light sources.

For chained maps, you probably want to have the same daylight length and light map sprite configuration, otherwise you'll get
some very strange effects in-game.

Wielded Lights
--------------

In the `Attributes` menu, under the [Attributes setup] you can set which attribute is responsible for the light radius. Then
you can assign a value to this attribute for each object under `Objects` > `In inventory` > `Attribute modifiers` (see object's
[in inventory]). When the player is equipted with an object that has this attribute set (or just wears such an object on his/her
belt), then the light overlay will get a circular transparent "hole" in it with that radius and with the player in its centre.

Stationary Light Sources
------------------------

Similarily, you can place an object with light radius attribute set on the map, like for example a tourch on the wall. These
also punch a circular transparent "hole" on the light overlay of that radius, with the object's position on the map as centre.
