<h1 tab37 quests>Quests</h1>

Adding Quests
-------------

Go to `Game` > `Quests`. On the left you can see the list of already existing quests. You can load one by double-clicking,
or you can just create a new quest.

On the tool bar, you have a checkbox to set the quest global, and on the right the internal name of the quest that the
editor uses.

On the button bar, you have the usual buttons: pressing the <imgt ../img/del.png> "Remove" button will remove the selected quest.
Clicking on the <imgt ../img/erase.png> "Erase" button on the bottom center on the other hand will just clear the form. You also
have a <imgt ../img/preview.png> "Preview" button as usual.

Quest alerts can be configured in the `Interface` > `Alerts` menu, see [alerts] for details.

Defining Quests
---------------

In the main editor area, you'll see the details of the quest. There's a translatable title, shown in-game in the [inventory]'s
"Quests" tab.

The description can be longer, but newline characters not allowed, line breaks are handled automatically. On the other hand you
can use `{}`, which will be replaced by the player's name, or `{attribute}` which will be replaced by that attribute's value.

A quest can be global or local. Local quests can be fulfilled by each player individually. Global quests can be completed only
by one player.

Below the form, there's the "On completition" event handler. Here you can specify commands to be executed when the quest is
completed. You can revard the player with items, change things on the map, you can play a quest specific sound effect (only if
you haven't set up a common audio effect in [alerts]), display a dialog to the player, etc.

Preview Quest
-------------

On the button bar, you have a <imgt ../img/preview.png> "Preview" button, with that you can check how the quest dialog looks
like and how the quests shows up in the inventory. If you don't like the dialog window or the font, you can set those in the
`Interface` > `Elements` menu, see [UI Elements].

When you're finished with the quest, don't forget to press <ui1>Save</ui1>.
