<h1 tab26 startup>Startup</h1>

Configuring Startup Scripts
---------------------------

Go to `Game` > `Startup`. That page has multiple event handlers actually, all starting when the game starts (more precisely when
the "New Game" was choosen in the [main menu]). The main one runs only once, while the others are executed later on at specific
intervals too.

Using Event Handlers
--------------------

Unlike the other manual pages, this mostly isn't about a specific menu entry per se. Instead event handlers can be found in
several places, on multiple forms. The most prominent one being in the menu `Startup`, but this description applies to the others
as well.

In general event handlers are list of commands and they encode simple game logic algorithms. You can put anything in them, and
since there's sequence, conditional and iteration too, all kinds of algorithms can be described. For every event handler,

- on the left, the bigger part is the event handler editor area. You can re-arrange its commands by drag'n'drop.
- on the right, there's always a command palette. You can add commands to the event handler by simply drag'n'dropping icons from this palette to the editor area.

To delete a command, just remove it from the handler, e.g. drag'n'drop off the editor area.

Control Structures
------------------

### Sequence

Simple, just put one command after another. The execution flow's direction is up to down.

### Conditional

<imgt ../img/cond.png> Is a mixture of "if" and "switch". You have a "+" and "-" icon to add and remove branches, of which by default there's two,
1 and 0. If the given expression returns 1, then the branch on the left is executed, if it returns 0, then the branch
on the right. You can use more complex expressions returning more possible values if you add more branches.

### Iteration

<imgt ../img/loop.png> There's only one kind, equivalent of "while" in programming languages. The block is repeated until the given expression returns 0.

### Assignment

<imgt ../img/let.png> With this, you can assign an expression's value to a local variable or attribute.

Variables
---------

You can use any attribute as a variable, just by referencing them by their names. If the names aren't prefixed, they refer
to the entity's attribute that triggered the event. For example, in an object's "on touch" event handler, they refer to
that object's attribute. If you prefix these attributes by `@`, then they refer to the other party in the event (which is
typically the player, but in case of default action handlers, the other party is the attacked NPC).

Each event handler has local variables. These are ones with only one letter names, from `a` to `z`. Since they are local,
all event handler has a copy of those, and are free to use them without interfering with other event handlers. There's one
exception to this rule, when the "call another event handler" command is used. There the local variables of the caller are
copied to the callee's local variables, and when the called event handler finished, it's `a` local variable is copied back
to the caller's `a` local variable.

In general, functions (like choosers and dialogs) always return their results in `a`.

Commands
--------

<imgt ../img/func.png> These do many different things. They can play sounds, replace the background music, play a specific animation etc.
There are also commands to manipulate the map, and to give items to the player (or take away). Basically the game logic
is described by these commands, no logic is hardwired in the engine. This gives absolute freedom over your game, but
also means it is easy to mess up, because logic is only syntactically checked, but there is no (and can be no) semantic
checking.

More info can be found about event handlers (when encoded as scripts) in the [TirNanoG Project](https://codeberg.org/tirnanog/editor/src/branch/main/docs/proj_format.md) documentation.
