Getting Started
===============

Installing
----------

NOTE: The default game assets are distributed separately to avoid licensing conflict with GPL. The TirNanoG Base template can be
downloaded from [here](https://tirnanog.codeberg.page#templates).

Go to the [repository](https://codeberg.org/tirnanog/editor/src/branch/binaries) and download the archive for your operating system.

<h3 inst_windows>Windows</h3>

1. download [tnge-i686-win-static.zip](https://codeberg.org/tirnanog/editor/raw/binaries/tnge-i686-win-static.zip)
2. extract it to `C:\Program Files` directory and enjoy!

This is a portable executable, no installation required.

<h3 inst_linux>Linux</h3>

1. download [tnge-x86_64-linux-static.tgz](https://codeberg.org/tirnanog/editor/raw/binaries/tnge-x86_64-linux-static.tgz)
2. extract it to the `/usr` directory and enjoy!

Alternatively you can download the [deb](https://codeberg.org/tirnanog/editor/raw/binaries/tnge_1.0rc-amd64.deb)
version and install that with
```
sudo dpkg -i tnge_*.deb
```

Running
-------

You can start the editor any time by running `tnge`. The tarball includes a *.desktop* file to place it in your Application
menu too (for Windows, a similar *.lnk* shortcut exists).

HINT: Just start `tnge` once, it will add itself to the menu for your convenience.

The editor will create a directory called `~/TirNanoG` in your home
folder to store all your [game projects](https://codeberg.org/tirnanog/editor/src/branch/main/docs/proj_format.md). If this
doesn't suit you, you can specify a different projects directory as a command line argument.

Your machine's localization will be autodetected, and if possible, the editor will greet you in your own language. If you want,
you can explicitly select a language on the command line, for example `tnge -L ru` or `tnge -L ja` (on Linux) or `tnge /L ru`
or `tnge /L ja` (on Windows).

<dl>
<dt>Windows</dt><dd>Right click on the <i>.lnk</i> file, from the popup menu choose "Properties". In the "Target" field you can add the command line options:
<imgc ../img/winlang.png><fig>Setting options on Windows</fig></dd>
<dt>Linux</dt><dd>Use your favourite terminal for the command line, or edit the desktop file <tt>/usr/share/applications/TirNanoG.desktop</tt>.<pre>
[Desktop Entry]
Version=1.0
Type=Application
Name=TirNanoG Editor
Comment=Create TirNanoG games
Exec=/usr/bin/tnge <hl>-L hu</hl>
Icon=tnge
Terminal=false
StartupNotify=false
Categories=Development;
</pre></dd></dl>

### Command Line Options

On Windows, replace `-` with `/` for the flags (because that's the Windows' way of specifying flags, for example `/t`, `/vv`),
otherwise all options are identical.

```
tnge [-L <xx> ] [-t <theme> ] [-g <gameid> ] [-c <gameid> ] [-v|-vv] [projdir]
tnge [-p|-u|-l] <atlas>
```

It also has five non-GUI, command line only operation modes: `-g`, `-c`, `-p`, `-u` and `-l`. These modes do not open a GUI
window, nor do  they need graphical environment or any installed shared libraries, they merely work in command line alone.

| Option        | Description |
|---------------|-------------|
| `-L <xx>`     | The argument of this flag can be "en", "es", "de", "fr" etc. Using this flag forces a specific language dictionary for the editor and avoids automatic detection. If there's no such dictionary, then English is used. Also sets the default language for the game. |
| `-t <theme>`  | Loads a GUI color theme for the editor's window from a GIMP Palette file. Does not influence the created game. |
| `-g <gameid>` | Generates a new, and each time unique end user license key (game file decryption key) and prints it to the standard output. Requires `~/TirNanoG/(gameid)/license.txt`. |
| `-c <gameid>` | Loads a game in [TirNanoG Project](https://codeberg.org/tirnanog/editor/src/branch/main/docs/proj_format.md) format and creates a production ready game file in [TirNanoG File Format](https://codeberg.org/tirnanog/editor/src/branch/main/docs/tng_format.md). The result is saved as `~/TirNanoG/(gameid)/game.tng.` |
| `-v, -vv`     | Enable verbose mode. `tnge` will print out detailed information to the standard output, so run this from a terminal. |
| `[projdir]`   | Overrides the default projects directory where the *gameid* is looked for. If not given, the default directory is `~/TirNanoG`. |
| `-p <atlas>`  | Pack all PNG files starting with `(atlas)` into an atlas `(atlas)_atls.png`. |
| `-u <atlas>`  | Unpack sprites from an atlas `(atlas)_atls.png` into multiple PNG files. |
| `-l <atlas>`  | List sprites in an atlas `(atlas)_atls.png`. |

For more details on how to use the `-g` switch, the section [Selling Your Game] has some tips and tricks.
About the `-p`, `-u` and `-l` flags, see [working with atlases].

Templates
---------

Starting a game from scratch and creating everything from ground up every time is very cumbersome and time consuming. To help
you with that, you can use [template zip](https://tirnanog.codeberg.page#templates) files. You just copy them into
the `~/TirNanoG` projects folder, don't bother to extract, the editor will do that for you.

<imgc ../img/winfolder.png><fig>A downloaded template and its folder location</fig>

The [purpose] of these templates is to give you a quick start creating games. These contain pre-configured sprites, animations,
map tiles, objects, NPC definitions etc. Starting from the TirNanoG Base template makes the use of the TirNanoG Editor very
similar to RPG Maker (but much more featureful and hopefully with a much simpler to use user interface). The editor can also
create a template from your project with a single click, no 3rd party zip tools required.

NOTE: Not all templates contain everything needed for a game. There are also smaller, so called module templates, which provide
assets for just a specific feature. You can import these modules later on into your project with the [Import Template] function.

Interface Overview
==================

The window is splitted into 5 horizontal areas with 2 vertical blocks in the middle:
<imgw ../img/interface.png>

### 1. The Menu Bar

This has the usual menus, plus two bottons: <imgt ../img/helpbtn.png> Help button, which opens this manual (also accessible by the
shortcut key <kbd>F1</kbd>); and the <imgt ../img/playbtn.png> Play button which saves and then runs the game (its shortcut is
<kbd>Ctrl</kbd> + <kbd>R</kbd>).

You can access the menus by pressing and releasing the <kbd>Alt</kbd> key. Selecting "Quit" from the first menu is the same as
pressing <kbd>Alt</kbd> + <kbd>Q</kbd>.

### 2. The Tool Bar

Depending on which page you're on, the tool bar might be missing. However usually it has at least the edited entity's internal
name on the right.

### 3. Entity Selector

Depending on which page you're on, the entity selector might be missing. This is where you choose which entity to edit. On the
figure above, which was made on the Map Editor, this entity selector lists maps. On the Object Editor page, it would list the
objects.

### 4. The Main Editing Area

Is where you edit the selected entity. The lookout of this area highly depends on which page you're on.

### 5. The Button Bar

If there's an Entity Selector then it usually holds a <imgt ../img/del.png> Delete button for deleting the highlighted entity;
a main editor area <imgt ../img/erase.png> Clear button; sometimes (if applicable) a <imgt ../img/preview.png> Preview button; and
finally and probably the most importantly the <ui1>Save</ui1> button which saves the modifications you made to the entity.

### 6. The Status bar

This is where you can see the status messages. It will print out information and tips as you hover over elements, and this
is also the place to display error messages.
