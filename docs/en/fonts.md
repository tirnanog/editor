<h1 tab12 fonts>Fonts</h1>

Go to `Assets` > `Fonts`. You'll see the list of available fonts here. To add a new one, click on the <imgt ../img/add.png> "Add"
button in the bottom right corner. A file selector window will appear.

You can use the [sfnedit](https://gitlab.com/bztsrc/scalable-font2) editor to modify the already imported fonts, or you can modify
the fonts with [FontForge](https://fontforge.org/) before you import them.

Bitmap and Vector Fonts
-----------------------

In the file selector window select a font. In the button bar, you can specify a range to import, but most of the time just leave
it as-is. When the font is selected, click on the <ui1>Import</ui1> button.

Pixel Fonts
-----------

You can also import images as fonts. In this case it is very important to specify the range. Both from and to fields can be
an UTF-8 character or a UNICODE codepoint if prefixed by "U+" followed by (up to 6) hex digits.

Glyphs can be placed both vertically and horizontally on the image. If the image is wider than tall, then the font's height
will be the image's height, and image width will be divided by the range getting that many glyphs. Likewise, for taller than
wide images, the image's width will be the font's width, and image height will be divided by the range to get that many glyphs.

For example, if you want to import a pixel font with numbers, your image should look like

```
0123456789
```

or

```
0
1
2
3
4
5
6
7
8
9
```

In the range specify `0` (the first character in the image) and `9` (the last character in the image). You could also use
"U+30" and "U+39" to specify in UNICODE codepoints. This is very important, because without a range it is not known what
glyphs will be imported from the image. Finally click on the <ui1>Import</ui1> button.
