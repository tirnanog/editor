Models
======

Importing Sprites from 3D Models
--------------------------------

Go to `Assets` > `Sprites`. You'll see the list of available sprites here. Click on the <imgt ../img/add.png> Add button in the bottom
right corner.

The screen will change and sprite sheet specification window will appear. Click on the <imgt ../img/load.png> "Load Sprite Sheet" icon
in the top left corner (first icon on the toolbar, but also duplicated on the button bar for easy access).

Loading the Model
-----------------

Several formats supported, and the steps required for importing them might slightly vary depending what is stored in the model
file. For importing sprite sheets from 2D images, see [Sprites].

On the file selector window, select a 3D model file. At the bottom left, specify the size to rasterize at (width and height will
be the same), set the Frames Per Second value and in which direction the model is facing (for Model 3D files that is always "south").
By checking the "Manual" checkbox, you'll be able to select the action animations from the timeline manually (see below).
<imgc ../img/impm3d.png>

NOTE: Movies usually use 25 - 30 FPS, but **NOT** animated sprites. An animated sprite usually has 3-5 frames (for a one second
long animation that's 12 FPS), maybe 10 frames in total if the animation is very fluid.

When file choosen and dimension given, click on the <ui1>Load</ui1> button.

### Model 3D Format (.m3d)

This is the most featureful format and it is fully supported. If you choose this, nothing left to do, everything will be taken
care for you. You will be redirected back to the sprite specification page with a fully populated sprite import list.

You can convert any 3D format into [Model 3D](https://bztsrc.gitlab.io/model3d) format, and there's a
[Blender export plugin](https://gitlab.com/bztsrc/model3d/tree/master/blender) too.

### Wavefront Object (.obj)

These can only store static (non-animated) objects, but it is a very popular and widespread format, supported by many tools
and model editors. Again, if you choose this, nothing left to do, because there will be only one sprite imported in 8 directions
without animations.

### Other Formats

For that, you'll have to have an installed [Assimp](http://assimp.org) library. On Linux, use your distro's package manager, for
example `apt-get install libassimp`. On Windows, yeah, good luck finding the pre-built DLL that the
[installation](https://assimp-docs.readthedocs.io/en/latest/about/installation.html) page is talking about. But if you've
found it, copy `assimp32.dll` to `C:\Program Files\TirNanoG` (next to `tnge.exe`).

If the library is detected, other 3D models in various formats (.3ds, .blend, .glTF, .b3d, .collada, .dae, .fbx, .x3d, etc.) will
automatically show up in the editor's file selector window.

WARNING: I give no support for Assimp. If you run into any problems, try asking on their forums.

<h2 model_spec_sprites>Specifying the Sprites</h2>

If the model contains an animation but no action specifications, or you have checked the "Manual" checkbox, then you have to
select action frames from a timeline.

<imgw ../img/sprtml.png>

1. source, animated model
2. timeline with frames
3. the add "New Sprite" button

Just like with 2D sprite sheets, you'll see the source on the left (big area), and sprite selections on the right (import list).
Only this time you'll see one big model, and a timeline beneath instead of a sheet. You don't select images for 3D models, rather
frame intervals on the animation timeline for each action. Once a selection is made, press the "New Sprite" button in the topmost
record of the import list, and sprites will be generated and added to the import list in all 8 directions.

HINT: You can move the selection on the timeline by pressing the <kbd>⯇</kbd> left and <kbd>⯈</kbd> right cursor keys.

<h2 model_do_import>Do the Actual Import</h2>

When you're satisfied with the sprite list on the right, you have to give a name to them in the input box above the list.
Sprites without a name will be imported with a "_xxx" suffix where the "xxx" is a serial number from 001 to 999.

You got that selected list, you've given a name to the list (input box above), and named the sprites (above each sprite image
in the list), you've double checked that you're in the correct category (icons at bottom middle), then you can press on the
<ui1>Import</ui1> button. After that you can go back to the sprites page or load a new sheet to import further sprites.
