TirNanoG Editor User's Manual
=============================

Welcome to the manual for [TirNanoG Editor](https://tirnanog.codeberg.page), the Free and Open Source *Adventure
and Action RPG Games* creation suite.

Getting Started
---------------

- [Installing] and [running] the TirNanoG Editor
- [Creating a Project]
- [Import Game] from `game.tng`
- Directory structure of a [TirNanoG Project](https://codeberg.org/tirnanog/editor/src/branch/main/docs/proj_format.md) (external link)

HINT: This manual can be used off-line. From the <mbr> right-click pop-up menu, choose "Save As". Save it under
`C:\Program Files\TirNanoG\doc` (Windows) or `/usr/share/doc/tnge` (Linux). If `manual_en.html` can be found on your machine in
the above directory, then the editor will open that whenever you press the <imgt ../img/helpbtn.png> Help button or the <kbd>F1</kbd>
key. Otherwise it opens the [on-line manual](https://tirnanog.codeberg.page/manual_en.html).

Supported Formats
-----------------

The editor is not only capable of creating games, but you can also *import assets in different formats* with it. The required
conversion is done under the hood, you can concentrate on your game without worrying about the details.

### Fonts

It'd be easier to list what's not supported. Most common formats include:

- TrueType (.ttf) and OpenType (.otf)
- FontForge (.sfd)
- WebFonts (.woff, .woff2)
- X11 Fonts (.bdf, .pcf, also their compressed versions, .bdf.gz, .pcf.gz)
- Scalable Screen Fonts (.sfn)
- and any image file for colorful pixel fonts

As for the encoding, only UTF-8 encoded UNICODE supported, so the fonts must contain glyphs for the ISO-10464 code page.
Special characters above the Basic Multilingual Plane (U+0 - U+FFFF) like emoji icons are supported too (up to U+10FFFF)
as well as multi-character ligatures.

### Sprites

To import your sprites it is greatly recommended to use true-color PNG with alpha channel (32-bit), but you can also use

- any Portable Network Graphics (.png, 8-bit, 16-bit, grayscale, 24-bit true-color, indexed etc.)
- GIMP eXperimental Computing Facility (.xcf)
- Photoshop Document (.psd, limited features, not all files supported)
- Joint Photographic Experts Group's (.jpg .jpeg, baseline and progressive too, but not all)
- WebPicture (.webp, all variants, lossy and lossless, animations too)
- Graphic Interchange Format (.gif, animations supported too)
- Targa Graphics (.tga)
- Windows Bitmap (.bmp, non-rle compressed only)
- [Model 3D](https://bztsrc.gitlab.io/model3d) (.m3d, animated 3D models)
- Wavefront OBJ (.obj, static 3D models)
- any 3D model in [other formats] (.blend, .dae, .fbx, .glTF, etc. if you have [assimp](https://assimp.org) installed)

### Palettes

- GIMP Palette files (.gpl)
- Adobe Photoshop Color File (.aco)
- Adobe Swatch Exchange files (.ase)
- Microsoft Palette files, JASC-PAL (.pal)
- Paint.NET Palette files (.hex, .txt)
- plus you can use any image to load a palette from.

### Audio

- Xiph.org's Ogging Vorbis Audio (.ogg)
- MPEG Audio (.mp3)
- Windows Wave Format (.wav)
- Protracker Module (.mod)
- Fast Tracker II (.xm)
- Scream Tracker 3 (.s3m)
- Impulse Tracker (.it)
- FLAC (if you have libflac installed)
- Opus (if you have libopus installed)

### Video

- Xiph.org's Theora Video (.ogv)
- any other video format (.mp4, .mkv, .avi, .mpg, .flv, etc. if you have [ffmpeg](https://ffmpeg.org) installed)

Do not try to import full-length movies, only short (1 - 2 min) cutscenes.

Get Involved
------------

This manual is written by volunteers. Please consider to join the effort and
[contribute to this manual](https://codeberg.org/tirnanog/editor/src/branch/main/docs) by submitting a PR to the
[git repo](https://codeberg.org/tirnanog/editor).

Currently we are in a big need for translators.
