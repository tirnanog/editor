<h1 tab30 npcs>NPCs</h1>

Adding Non Player Characters
----------------------------

Go to `Game` > `NPCs`. On the left you can see the list of already defined NPCs. Double-clicking on one of them will load
that NPC.

On the tool bar, you can see the "basic configuration" and "inventory configuration" icons, and on the right, the internal
NPC name. These icons will switch tabs, because there are quite a lot configuration options for NPCs, did not fit into one page.

HINT: To duplicate an NPC, load them, then modify the internal name and press <ui1>Save</ui1>.

Configuring Behaviour
---------------------

The first and most important thing to configure is what the NPC should do when the players do not interact with them.
- Standing makes the NPC stand in one place
- Wandering makes the NPC randomly move
- Patrol makes the NPC to follow a predefined path

All have the "Speed" argument (even standing), but "Wandering" also has a frequency how often the NPC should change course,
and "Patrol" has a patrol path id argument (see [maps] on how to set up a patrol path).

You can change the behaviour from event handlers. For example, you can set the default to "Wandering", and in the "on approach"
event handler switch that to "Attack".

HINT: Treasure chests are standing "NPC"s with just an "on touch" event handler.

Event Handlers
--------------

The second important thing is to configure how an NPC would react when they interact with a player. These event handlers can
change the NPC's behaviour, or make the NPC to start a dialog, open up their invertory for doing business etc.

- on click: activates if the player uses no object, or an object in any action category
- on touch: when player collides with the NPC
- on leave: when player's and NPC's distance increases above the given value
- on approach: when player's and NPC's distance decreases below the given value
- on action: when the player uses an object on the NPC which belongs to a specific action category
- on using: when the player uses one particular, specific object

<h2 npc_attributes>Attributes, Sprites and Preview</h2>

Looks like and works exactly like in character options, except there's only provided attributes for NPCs. For more details,
please refer to the [character] chapter.

HINT: You can create a great variety of NPC characters with the sprite [generator].

<h2 npc_inventory>Inventory</h2>

By pressing the "Inventory" icon on the tool bar, you'll get the NPC's inventory setup.

Here you can specify the in-game name of the NPC and what items they should have. Quantity and chance of having those can be
configured too. When the NPC gets killed, all the items in their inventory is dropped on the map as loot.

NOTE: Note the difference between these:
```
50%, 2 swords
```
means there's a 50% chance of the NPC having 2 swords or none. No way they are going to have 1 sword. On the other hand
```
50%, 1 sword
50%, 1 sword
```
means the NPC might have 0, 1 or 2 swords.

### Creating a Merchant

If you want to create someone whom the player can trade with, you must specify "Sell price" and "Buy price" event handlers. These
receive the given object's base price (see object's [in inventory] tab) in the `a` local variable and category in `b`, and should
return the modified price in the same `a` local variable. Normally you want to have something like this

```
a:=a*110/100
```

to sell 10% more expensive than the base price, and

```
a:=a*50/100
```

to buy at half of the base price. But since you have the control structures for these event handlers, you can create exciting
<imgt ../img/cond.png> conditionals too, like for example a racist orc Merchant who sells to humans for double the price.

Using the price category in `b` is optional, and you can set it up as you wish. But if you want a certain merchant to only accept
certain items, then simply only return a price if the item is in the desired category, for example:
```
+-----------------------------+
| [+] [-]  b                  |
+---2---+------1------+---0---+
| a:=0  | a:=a*50/100 | a:=0  |
+-------+-------------+-------+
```

There's another feature for merchants, you can set their inventory to replenish themselves. Normally an item creation executed
only once, when the NPC is created, but with this you can repeat the process every N minutes. A merchant can't have more than
999999 items of the same kind.
