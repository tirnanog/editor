Character Animations
====================

For characters and their assets, those must use the TirNanoG Base sprite sheet layout (see an example sheet below). This
is very important, because using the same layout is what guarantees compatibility and makes the sprite [generator] possible.

Proportions
-----------

Characters are squashed, roundish, and not realistically proportioned.
Bases are approximately two and a half heads tall and in the same
perspective as the tiles.  The base should fit in a 32x48 space and
the clothing should fit in 48x64 space.  The outlines should be black
or near black, no selective outlining.

<imgc ../img/characters.png><fig>Example of accessorized characters</fig>

Modularity
----------

Characters are made up of multiple, strictly-positioned sprite sheets. This means that in TirNanoG Base the characters
are modular, you can select different sprite sheets to create a player or NPC character of your liking.

<imgc ../img/sheets.png><fig>Demonstrating modularity through multiple sprite sheets</fig>

### Body and Head

The first and most important layer is the `body`, which consist of a torso, arms and legs and the second layer is `head`.
There must be at least two variations of it:

* female (feminine)
* male (masculine)

For both, the positions of the head and the hands are fixed, they must be at the same spot for every frame, depending on
the animation and the direction.

### Clothes and Accessories

This layer could mean multiple layers, depending on the accessory's requirements. A full-body armour typically needs
one, while a modern day outfit would probably need at least three (shirt, trousers, shoes). There's no limitation,
these layers are fully customizable in the character sprite [generator].

Since a clothes asset is using the same guides as the body layer, there should be multiple variations of it as well (female,
male, etc.), but unlike the body layer, it is okay if only one of the variations exists for clothes.

### Tools

Everything that has to be positioned in the character's hands. Includes weapons, magic staff, torch etc. too. The guidelines
provide standardized hand positions, which means there should be no need for multiple variations of tools per body (e.g. the
same sword sprite sheet should work with both the male and female body layer). This is the most relaxed layer for animation
requirement: typically all of movement type animations are required, but only one "use" animation is needed for them.

Tools layer typically consist of one single layer. But it might be split into two: `toolb` is behind all the other layers and
`toolf` is on top of all the other layers. This is needed if the tool has a long handle, like a scythe for example, or if you
don't want to "cut out" parts of the accessory where the body should cover it.

Animation Types
---------------

| Name       | #frames | Description                           |
|------------|--------:|---------------------------------------|
| idle       |  2      | idle animation                        |
| hurt       |  2      | played when character takes damage    |
| block      |  2      | successfully blocked an attack        |
| walk       |  6      | normal walking                        |
| run        |  6      | similar to walk, but faster movement  |
| jump       |  6      | vertical jump                         |
| slash      |  6      | action 1: melee attack with sword     |
| thrust     |  6      | action 2: melee attack with spear     |
| bowing     |  6      | action 3: ranged attack, firing a bow |
| cast       |  6      | action 4: casting a magic spell       |
| push       |  6      | action 5: push objects, same as walk  |
| carry      |  6      | action 6: carry objects, same as walk |
| climb      |  6      | climbing on a ladder ^^1 direction^^  |
| spawn      |  6      | character appearing ^^1 direction^^   |
| die        |  6      | out of HP, death ^^1 direction^^      |

Sheet Layout
------------

INFO: You can download this grid as well as the guidelines in [guides.zip](https://tirnanog.codeberg.page/guides.zip).
This archive contains `guides.xcf` for GIMP, but in case you aren't using that, each layer is saved as a PNG as well.

Animations are arranged on the sprite sheet by strict rules. Directions are encoded in rows (South, West, North, East in this
order), and animation frames in coloumns (except *idle*, *hurt* and *block* animations, which have only 2 frames and are packed
together, all animations have 6 frames).

Next, each animation (6 x 4 grid cells) is arranged on the sheet in a strict order, like this:

<imgw ../img/layout.png><fig>The TirNanoG Base sprite sheet layout</fig>

You can import these sprite sheets under the [sprites] menu using the "TNG-Base" layout, and they are also used by the
character sprite [generator].

The bottom right corner of the layout contains a 4 x 5 grid size portrait. This is used to construct a modular portrait,
but *without* facial expressions.

Expressions for portraits are also modular and made up from multiple images (one for the eyes, one for the nose, one for the
mouth etc.), but contain 4 times 4 x 5 grid size sprites in a row (so total of 16 x 5 grid cells), one sprite for each expression
in order: neutral, happy, sad, angry.

<imgw ../img/portraits.png><fig>The Portrait layout</fig>

Expanding
---------

You can add new sprite templates any time. These are stored in `~/TirNanoG/.common`, and all are PNG files. Just like with
the other assets, there's no database, the editor picks up new image files automatically.

### Layer Definitions

Images by the name `(layer).png` which are 16 x 16 pixels in size contain an icon, and define a layer type. Since layers are
ordered alphabetically, it worth starting its name with a number (that number won't be shown in the layer's name, it just
specifies the correct order, like `1hair`, `2clothes`). There are 4 hardcoded layers, `body`, `head`, `toolb` and
`toolf` (the reason for this is, again, to guarantee correct layering order).

INFO: The generator constructs the final sprite sheet by adding layers in this order: first, `toolb` layers. Then other layers
except `toolf` ordered by the variant's `(number)`, within each iteration `body`, `head` first, then all the user defined layers
in alphabethical order, then finally `toolf`. Within a layer, variants are ordered alphabethically.

### Sprite Templates

Non-icon sprite template images are named like `(layer)_(number)_(variant).png`, where `(layer)` is one of the hardcoded names
or the user specified layer's name. Images with height of 16 times the grid's size are character animation templates, they must
follow the TirNanoG Base sprite [sheet layout]. These layers might add parts to the portrait too, but only if those parts aren't
facial expression related. The tools layer is special in a way that it has a background layer `toolb` and a foreground layer
`toolf` too, handled together. The `(number)` part is a number between `0` and `9`, and it just tells the correct ordering of the
variants, and finally, `(variant)` is the name, free to choose, but without spaces (use underscore) or parenthesis (see below).

Portrait templates are named `(layer)_(number)_(variant).png` too, but their height is 5 times the grid's size.
They do not add to the character animation sprites, only to the portrait sprite, but with multiple facial expressions.

The layer types aren't hardcoded, you can create new types any time just by providing an icon first. For example, you can add
an icon by the name `ears.png`, and then you can start adding templates like `ears_1_elf.png`, `ears_1_small.png` etc.

Optionally template names might look like `(layer)_(number)_(variant)_((depends)).png`, for example `ears_1_elf_(male).png`.
These variants are only selectable if the variant `*_[0-9]_(depends).png` is enabled. Only one parenthesis allowed, and only
at the end of the filename. If there are multiple templates by the same name on different layers, then only the first
match is counted as dependency, however the `(variant)` part of the templates should be unique in the first place.

### Recoloring

You can use a neat trick with the templates to allow dynamic recoloring. For that, the image must have some special
grayscale colors, namely `#101010`, `#303030`, `#505050`, `#707070`, `#909090`, `#b0b0b0`, `#d0d0d0` or `#f0f0f0`, and a
8 x 16 pixel matrix *at the bottom right corner* (or, in case of a character [option sprites], on a separate sprite). Here each
row corresponds to a palette. These grayscale values index one of the coloumns (brightest on the left, darkest on the right),
and the user can choose one of the rows. Then the generator will replace these grayscale colors on the image with those
selected colors.

<imgc ../img/recolor.png><fig>Explanation of how color variants work</fig>

In order for a color variant to show up, the brigthest color (at image width - 8, the one which replaces `#f0f0f0`) must be
specified. Others depend on which grayscale colors are used on the image. For example, the second color can be left empty
if there's no `#d0d0d0` on the image.

The [sprsheet](https://codeberg.org/tirnanog/sprsheet) tool can automatically de-colorize any image for you with the `-p` flag.
Follow these steps:

| Step | Description                                                                                                            |
|-----:|------------------------------------------------------------------------------------------------------------------------|
|    1 | <imgr ../img/decolor1.png>create a transparent image of 8 x 16 pixels size, this will hold the palette variants        |
|    2 | <imgr ../img/decolor2.png>choose a color and add its gradients (exactly as they appear on the sprite) to the top row   |
|    3 | <imgr ../img/decolor3.png>add more color variations                                                                    |
|    4 | use `sprsheet -p palette.png lpc2tngbase.csv output.png input.png` to de-colorize and create a recolorizable new sheet |

You have to create this `palette.png` only once, it can be used to de-colorize any number of sheets. For example, create a
palette image with the hair color variants (brown, blonde, gray, pink, orange etc.), with the brown hair's gradients on top.
Then you'll only ever need just the brown hair LPC sprites (long, short, curly, messy etc.), and you can convert and add the
recolor option to them all.

In case some sprites use a different palette (for example the brown shirt uses one kind of brown gradient, but brown boots is
using another one), then you can use `sprsheet -r replace.png` to replace the colors. Here `replace.png` must have at least 2
rows, but can have more, and it has as many coloumns as many colors you want to replace. The first row is the "to" palette,
others are the "from" palettes.
