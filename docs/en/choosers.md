<h1 tab24 choosers>Choosers</h1>

Specifing Route Choosers
------------------------

Go to `Interface` > `Choosers`. Choosers are a construct that display a list of icon options on screen and may alter
the game's state. They are typically used to choose a game route, but could be useful for other things as well.

Just like the other menu entries under the Interface menu, this also has a <imgt ../img/preview.png> "Preview" button at the bottom.

Choosers List
-------------

On the left, in the Entity Selector there's a list of already configured choosers. By double-clicking on an item, it will be loaded.

Pressing the <imgt ../img/del.png> "Remove" button on the bottom left corner will remove the selected chooser. Clicking on the
<imgt ../img/erase.png> "Erase" button on the bottom center on the other hand will just clear the form.

To add a new one, specify a name on the top right and press <ui1>Save</ui1> in the bottom right corner.

Chooser Tool Bar
----------------

At the top, in the tool bar, you can see the usual inputs for specifying margins and paddings for the icons.

On the right, you can specify the chooser's internal name.

Chooser Options
---------------

Under that there's the list of available options. This list has 4 coloumns: the position, the icons' images, a required field
and a provided field. On the right you also have two buttons to change the icons' order on the stack if desired.
<imgc ../img/chooser0.png>

Positions
---------

There are several stacks depending on position, and you can add multiple icons in each one. Typically there's a stack for
center middle horizontal, top left corner horizontal, top center horizontal, top right horizontal, top right vertical,
middle right vertical etc.
<imgc ../img/chooser1.png>

The last option isn't an icon stack, rather a list of backround areas. This last position is specified by providing a rectangle
with coordinates in percentage. There's a coordinate picker helper too, which works fine but quite useless unless you load an
example background image (this background image is not saved with the chooser, it is only used for the coordinate picker).

Requires
--------

You can specify if a certain icon has some condition in order to show up or not. Local variables are from `a` to `z`, and
if you choose and empty local variable from the list (a space), then you can select an attribute as well.

Expressions are simple, in the form if a `variable=value` (variable equals value) or `variable=value&variable>=value`
(one variable equals a value and another variable is bigger or equal to another value).

Provides
--------

Choosers always provide the selected icon's index in the local variable `a`. If you want, you can set other local variables
or attributes too.

The "Keep running" checkbox, if checked, avoids the chooser quiting when that option is selected.

Example
-------

Imagine you have a background image with two mountains, and you want to display two separate lists of options to the user
depending which mountain is chosen. To do this,

1. Create an icon stack (let's say at the top left corner horizontal) where every icon has its "requires" set to `b=1`.
2. Then create another icon stack (let's say at the top right corner vetical) where every icon has its "requires" set to `b=2`.
3. Specify a background area on the first mountain (use the coordinate picker), set its "provides" to `b=1` and check "Keep running".
4. Specify another background area on the second mountain, set its "provides" to `b=2` and check "Keep running".

In the game, this will look like: the background image with the mountains and the chooser displayed somewhere (with a cutscene
for example). When the user clicks on the first mountain, the icon stack on the top left corner will show up. When they click on
the second mountain, this list will disappear and the one on the top right corner will show up instead.

Now imagine that you want certain icons to only appear for experienced players, and you have a `score` attribute to know how
experienced the players are. For that change the "requires" to `b=2&score>=100` for example.
