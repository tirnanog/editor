<h1 tab31 spawners>Spawners</h1>

Configuring NPC Spawners
------------------------

Go to `Game` > `Spawners`. On the left you can see the list of already defined spawners. Double-clicking on one of them will load
that spawner.

Number of maximum NPCs
----------------------

This is the number of maximum how many NPCs this spawner handles at any given time. If it has already spawned this many, no new
NPCs will be born.

Spawning Frequency
------------------

Is a time interval, when to check if the maximum number of NPCs has been spawned.

NPC list
--------

This list contains what kind of NPCs will be spawned by this spawner. You can add an NPC type multiple times and thus increasing
the chance of spawning that kind. The list always sums up to 100% chance.

You can set a condition to a kind. For example, let's say you have a `level` character attribute, and a spawner only spawns weak
slimes if it's less than 5, but it also might spawn minotaurs if the player's `level` is above 10.

In the provides coloumn, you can specify an attribute which will be set on the spawned NPC. For example, in conjuction with
the conditional, you can set different strengths to the NPCs. If the provides expression is empty, then the NPCs will be spawned
with the attributes specified on the [NPC attributes] page.
