Colors
======

INFO:Things that are important enough to be worth repeating
* No pure colors!
* Vary the hues of your color ramps!
* Highlighted areas are yellow-ish, shadowed areas are purple-ish.
* Block your art out first, including shadows, before adding detail.

Shades
------

Roughly a “toon style rendering” with medium-low levels of texturing.
We want some detail, but not so much that things are so ornate that it
might make collaboration difficult.  We don’t want things to be
“noisy” either.  But detail is good.  Texturing should have a
toon-style; this is accomplished with medium to low amounts of
texturing.  Details should be used sparsely, so make them count!

<imgc ../img/shades.png><fig>Process of creating shades</fig>

Shade in blocks.  Begin drawing by blocking out the object first,
paying attention to its volume.  Lines should be added afterward, and
generally only around the edges and with very important details.
Details should mostly be implied by form and color, not by outlines.

Lighting
--------

Lighting should primarily come from above.  If there is any side
directionality, it should come from the left, but not by much: keep it
mostly center.

<imgc ../img/sun.png><fig>The direction of sublight (yellow arrows)</fig>

An example scene in blender with a sun light pointing in the appropriate location:

<imgw ../img/lighting.png><fig>Left example shows from the “top-facing” camera used in this style;
right example shows a sideview.  Yellow “sun” with ray is the light
source, big orange triangle on right is the camera.</fig>

Your light to dark color ramps should *never* all have the same hue.
Vary the hue and saturation a bit as you go from light to dark, or
your objects will look flat.

Shadows
-------

To create shadows or lowlights, once you have the main color the shade
you want it to be move the hue (HSV selector) slightly towards the
closest purple. To create highlight color, do the same but instead of
purple and shade move the hue slightly towards the closest yellow and
lighten it up. Adjust as necessary until it looks “right”.

Inside, things will be cooler in overall color, with slightly less
contrast.  This is doubly true for things like basements, caves and
other underground areas.

All drop shadows should be done with the color #322125 at 60 percent
opacity.  If it makes sense, one may also provide a combined version
of two tiles, so only one layer is needed to, say, put a house on a
grass background.

Dithering should be used sparingly if at all.  None of the base
artwork has dithering.

Outlines
--------

Outlines should be a darker version of the current color, or a dark
color generally, not black.  (Extreme circumstances obviously may have
exceptions.)

<imgc ../img/rock.png><fig>Note how the outline is a darker version of the same color</fig>

Props and Objects
-----------------

Props should be colored so that they don’t blend in with the
surrounding background tiles (vary color, brightness, and saturation
to provide contrast).

There’s should be a large difference in lighting between the sides and
the top of objects.  Look at these objects as example:

<imgc ../img/barrel_and_bucket.png><fig>Examples of object lighting via a barrel and a bucket</fig>

Props should have shadows, or they will appear not to be part of the
scene.  Shadows should follow the same transparency blending rules as
mentioned in the “lighting“ and “shadows” sections above.

Character Colors
----------------

Characters should have their own color palettes so that they stand out
from the background.  Drop shadows should follow the same rule as the
tiles, #322125 at 60% opacity.

NOTE: The sprite generator can handle dynamic [recoloring] of the character layers.

Palettes
--------

There’s no specific palette *required* for TirNanoG Base asset
conformance, but it’s generally best to try to match to the colors
used in the base set.
