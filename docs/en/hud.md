<h1 tab21 hud>Heads-Up Display</h1>

Configuring the Heads-Up Display
--------------------------------

Go to `Interface` > `HUD`. Here you can set up the look'n'feel of the HUD during the game.

Just like the other menu entries under the Interface menu, this also has a <imgt ../img/preview.png> "Preview" button at the bottom.

Controls
--------

For touchscreen devices, there must be a navigation control and an action buttons control on screen. These use a default
image in the game, but if you want, you can customize these to your liking to match your game's look'n'feel.

The navigation circle is shown in the bottom left corner, while the action buttons on the bottom right.

Itembar
-------

This is which shows what items the characters are carrying on their belt and in their main hand.

You can select a background image, and if you want, a semi-transparent front cover image too.

The various number inputs specify the number of items, one item's width, height, starting position on the background image
and the gap between the items in pixels (in this order). Because you probably want to visually separate the item hold in hand,
there's also an additional gap between the handheld item and the other items.

The item bar is always displayed in the bottom middle of the screen.

Statusbar
---------

This is where the user's status is shown. Typically health points, mana points, experience and such.

You can have the statusbar combined on the itembar's background image. Typically used with orb like progress bars.
When you have a separate statusbar, then it is displayed on the top middle of the screen.

Similarily to the itembar, you have a background image, and you can also have a semi-transparent front cover.

Progressbars
------------

With this engine there's no hard coded stat, everything is configurable. Therefore you must specify what variable to show where,
and this is what the progressbar specification list is for.

In the list you can see the bars that are already defined. Below the list you have input boxes to specify a new one. By selecting
a progressbar from the list, the <imgt ../img/erase.png> "Erase" button will became active and the selected bar can be removed.

For defining bars, you have the options:

- direction: can be left-to-right (standard), bottom-to-top (orb-like bars) and alpha-channel (for temporary effects,
  like poisoning), enemy's HP bar (shown above the enemy)
- position: on the statusbar (or itembar) in pixels. When both coordinates are 0, that defines a temporary effect progressbar.
- value attribute: select the attribute to be used to get the value from.
- max attribute: selects the attribute which holds the maximum value for the progressbar.
- image: the sprite for the bar. Depending on the direction only either the left or the bottom part will be shown.
- background image: this is optional, and can be useful for temporary effects when they don't use alpha as direction.

With "enemy" direction the progress bar is left-to-right and not shown on the statusbar, rather above the enemy character.
It is used to specify which sprites to use to display the enemy's HP.

Temporary effects are stacked and displayed at the top middle of the screen regardless to the statusbar. For the position
there's a coordinate picker, but in order to use it, first you have to specify the status bars' (or item bars') background
image as well as the progressbar's image.

When all input box are filled in, click on the <imgt ../img/add.png> "Add" button to add a new progressbar to the list.

If you have misplaced a bar on the statusbar's background, there's no need to delete and add it again. When a progressbar is
selected, you can <mbl> left click on the direction and position coloumns to increase them, and <mbr> right click to decrease
them. This is not very intuitive, but a neat trick to move the bars around without the need of redefining them.

Inventory
---------

Shows what the user has. It has three tabs: "Items", "Stats" and "Quests". Here you can configure the first one (for the others
see [objects] with skills, and [quests]).

In the inventory's first row, you can specify the inventory's normal and pressed icons, similar sizes and margins for the
items as with the itembar, the font to be used to display the number of items in the inventory, and finally three sprites:
normal item's background, selected item's background and an image for the equipment slots.

In the game, the inventroy's icon is shown on the top right corner of the screen. It must be 32 x 32 pixels in size (if not,
it will be resized automatically).

Similarily to the HUD's item bar, you can specify the item's size and the position where the item is displayed. Here there's
no gap configuration, because that's specified by the slot background sprite, and therefore the item's position is relative
to the slot background sprite. You have two slot images, one for normal slots, and one for the active, selected slot.

The last sprite, the equipment figure must contain at least one slot, but might have more. This should be an image of a figure
which displays what items are worn by the user and where. The one mandatory slot is for the handheld item. The item's size on
the equipment figure is the same as with the slots, but if you want background slots too, those must be drawn on the sprite image
(might mean more work for the designer, but greater flexibility).

Equipment Figure and Slots
--------------------------

Now the problematic part is, that the TirNanoG engine does not hardcode how many slots there can be, therefore you must
specify all of them here. As said, only one slot is mandatory, for the handheld item, and that must be the first (typically the
right hand of the figure). Others (like body for armour, legs for trousers, head for hat etc.) depends what game you're
creating. The table below lists these slots.

By selecting a slot from the list, the <imgt ../img/erase.png> "Erase" button will became active and you can delete that slot.

After specifying the starting position (relative to the equipment figure's background image), and a comment, press the
<imgt ../img/add.png> "Add" button to add the slot to the list. The same trick works here as with the progressbar, for the selected
slot in the list, <mbl> left clicking the position will increase, while <mbr> right clicking will decrease the coordinate,
allowing to re-position the slots without the need to removing and adding again (you can use the <imgt ../img/preview.png> "Preview"
button to see if the coordinates are correct). Just like with the item bar, there's a coordinate picker helper too.

The slot's name is irrelevant, and won't be shown to the player. It is merely a comment for the developer so that they can
select the slot more easily in the objects' [in inventory] tab when they are configuring the "equipted at" choices for a
certain object.
