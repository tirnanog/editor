<h1 tab29 character>Character</h1>

Configuring Character Options
-----------------------------

Go to `Game` > `Characters`. On the left you'll see the list of already defined option groups. Double-clicking on one of them
will load that option group.

There's no clear-cut instructions on how to set up characters, because that mainly depends on what kind of game you're creating.
The TirNanoG engine gives you absolute control over how your define these. See also how to create [predefined heroes],
[hero classes] and [free-form mode characters].

Character options are groupped. For example, brown hair or blonde hair are options, and they probably belong to the same hair
option group. This can be difficult at first to wrap your head around. When the players are creating characters, they can choose
different options, exactly one from each group. It is hard explain this better, because of the freedom the engine gives you.
Let's say one option group is "spicies", with options like "human", "elf", "orc" etc. Another option group is "class", with
options like "warrior", "thief", "mage" etc. The first group assigns sprites to the character, the second assigns attributes.
Hope this example helps.

This could get more complicated because the engine allows you to define multiple groups that alter the outlook of the character.
If you choose this, then one of those option groups must be marked as "base look out". For example, an option group can be
"body", with options like "human male", "human female", "elf male", "elf female" etc. These would assign different base sprites.
Then you can have option group for "hair" for example. In this case, the player would be able to select the base look out, the
body and the character's hair independently during character creation.

NOTE: When "base look out" is selected, you also have to tell where the base line is (which is the ground level on the sprite
with other words). The base line is normally at the vertical center, but you can move it down by a delta y value to match the
character's foot. This is very important to set if you want to add oversized sprites too.

Option Group
------------

The name of the option group which is internally used in the editor can be set in the top right corner.

Options
-------

Options with translatable names are added to the list in the middle. Each option might add attributes, inventory items or sprite
overlays to the character.

### Option Attributes

There are two kinds of attributes, required and provided. If you set the relation to "(set)", then it's going to be a provided
attribute. If you use any other relation, then a required attribute. Again, it is easier to explain through an example. Let's say
like in the previous example, you have an option group for "spicies", with options "human male" and "human female". For the first
one, you can add a provided attribute "sex" with value 1, and for the latter with value 2. Then, in another option group,
which would be "career", you can add an option "blacksmith" with the required attribute of "sex" being 1, and "waitress" with
required attribute "sex" being 2. In this case the player would be to choose during character generation between "human male"
and "human female". Depending on that choice, they would be offered different career choices: with a "human male", they can
choose "blacksmith", but not "waitress", and similarily, if they choose "human female", then they can choose "waitress",
but not "blacksmith". This makes also sense when you think about having different sprites, for example "human male" can have a
different set of options in the "hair" group than "human female" characters.

### Option Inventory

You can specify inventory items for an option which will be added to the player's starter inventory if they choose this option.
For example, in the option group "class", the option "warrior" might add a sword, and "mage" might add a magic wand to the
starter inventory.

### Option Sprites

Normally a sprite sheet would offer you ready to use characters. However it is possible that the imported sprites are modular:
they only contain parts of the character (like separate sprite sheet for body, face, ears etc.) For this latter case, you can
add multiple sprite layers. Each layer should contain a specific kind of sprites: for example the first layer should contain the
body, the second layer the hair etc. If you have ready to use sprites with fully drawn characters, then you'll need only one
layer.

If you have sprites with special grayscale colors (`#101010` to `#f0f0f0`), then you can select a 8 x 16 pixel palette sprite on
the toolbar. With this, the player will be able to choose different color variants during character generation (for more details,
see [recoloring]). You can get such a palette if you have imported [sprites] with the "?" (ask) color variant.

On a layer, you have to specify sprites for each action. This could be overwhelming at first, but the sprite selector helps you
a lot: you can search sprites by name, which filter is kept. For example, click on the "walking" coloumn's button. In the pop up
selector, you search for "elf". The list will be filtered only to sprites with "elf" in their names. You choose the sprite by
clicking on it. Now in the same row you select another action, eg. "swimming". The sprite selector will pop up again, but only
with sprites depicting elves (you can clear the search field to list all sprites again).

Now you don't need ALL actions. If you don't plan to use swimming or flying in your game, then you can leave those sprites empty
entirely. If your collision masks contain no climbing cell, then you won't need the climbing animations either. You can also skip
the block attack, but you probably should add sprites for hurt and die. With the actions, only those need to be set up, for which
you have added event handlers.

Below the sprite matrix are the copy'n'paste controls. You can copy and paste the entire layers configuration from one option to
another (or as a matter of fact, to other layers too, like to an NPC's character layer.) There are two more buttons with up / down
arrows on them. With these you can re-arrange the layers' ordering by moving the selected layer up or down.

By clicking on the <imgt ../img/preview.png> "Preview" button, you'll be able to see the selected action's animations in all
directions. This is very handy to see if you haven't specified all directions then which direction's sprites will be used to fill
in. It is also useful to see how the sprites on different layers are merged and if they look nice together or not. If some sprites
are oversized (bigger than the base character sprite), you can also check here if they fit to the base line properly.

HINT: the preview shows the selected action by default. But in preview mode, you can press <kbd>⯇</kbd> left and <kbd>⯈</kbd>
right cursor arrows to switch actions. When the number of frames (or the playback type of the animation) differs on some
layers, a flashing Warning sign will be shown next to that merged sprite.

### Option Description

Finally, if you want, then you can add a translatable description for the option.

Once you have configured the character option, don't forget to press <ui1>Save</ui1>.

Character Examples
------------------

### Predefined Heroes

The are many games with a storyline which demands no configurability and a specific protagonist (like Geralt of Rivia). There are
also many games with storylines that only allow selecting one of a few predefined characters (like Bernard, Laverne and Hoogie).
In this section I'll explain how to set up these.

First of all, you'll only need exactly one option group, no more.

In that group, add as many options as many choosable characters your game has. For each option specify both the attributes
and the sprites (and maybe inventory too). It's very likely that you'll have ready to use sprites for these characters, so
you'll need only one Layer. But it is also possible to use multiple layers and construct the protagonist character from modular
sprite sheets.

It is also possible to have only one predefined character, but let the user choose it's primary attributes. For that, do not
set up attributes, just sprites, and in the [Attributes Setup] menu configure points. For more details, read free-form section
below.

### Hero Classes

A typical RPG allows the player to choose a character and a hero class independently.

For this, you'll need at least two option groups. In the first group, call it let's say "spicies", define the base look out
of the character by defining the sprites, but configure no attributes nor inventory.

In the second group, name it let's say "class", add options with only attributes and inventory, but no sprites. For example,
the "warrior" option should have large "Strength" value, but lower "Intelligence", while the "mage" option should have lower
"Strength" and higher "Intelligence".

You can also use sprites with this second group of course, for example you can set up the "mage" option to add a pointy
sorcerer's hat to the character's portrait.

It is also possible to configure this with freemode primary attributes. In this case configure points in [Attributes Setup].
Then in the second group, which is called "class", specify *required* attributes, for example for "warrior", specify "Strength"
at least 8, and for "mage" specify "Intelligence" at least 8. In this case the player will be able to freely configure their
character's primary attributes, but they won't be able to choose certain classes if the given primary attribute is too low. It
is also possible to use calculated attributes with the classes in freemode. See the free-form section below for more details.

### Free-Form Mode Characters

Go to `Game` > `Attributes` and on the [attributes] page click on the <imgt ../img/setup.png> `Attributes setup` button. On that
form, you can set up the **total points** and the **maximum point** per attribute.

When these are set, then the players can freely assign the total points among the primary attributes, but no attribute
can have more points than the given maximum.

In this case the character's main attributes would be probably calculated attributes that rely on these primary attribute
values. For example, you can have 3 primary attribute ("Strength", "Intelligence", "Endurance"), each with maximum points of
10, and you can give 21 points in total to the player to assign between these 3 as they like. Then the base attributes could
be all calculated (like "Physical" = (Strength + Endurance) / 2, "Mental" = Intelligence, "Attack" = Strength, "Defense" =
(Strength + Intelligence) / 2 etc.) Note, these are just examples, you can set up the attributes as you like.

Go to `Game` > `Characters`. With the character option groups, you can specify attributes for certain options, but do not
specify any primary, unless as a required attribute.
