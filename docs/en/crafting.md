<h1 tab34 crafting>Crafting</h1>

Defining Crafting Rules
-----------------------

Go to `Game` > `Crafting`. On the left you can see the already created crafting dialogs. Double-clicking on one of them will load
that dialog.

You have on the right the internal name of the dialog that the editor uses, otherwise the tool bar is empty.

Crafting Dialog
---------------

Similarly to normal [dialogs], you can display crafting dialogs from event handlers, using the "Craft" command.

Title is mandatory, translatable and shown in-game. It contains the name of the NPC who is doing the crafting (could be a
blacksmith or a fireplace).

Message is translatable, and optional. You can't place newline characters in the message, line breaks are handled automatically,
because each player might have a different screen resolution, and with that a different dialog size. On the other hand you
can use `{}` characters, which will be replaced by the player's name, or `{attribute}` which will be replaced by that attribute's
value. Do not use large texts, just brief descriptions to hint the users what can be crafted in this dialog.

If you want a sound effect to be played when the crafting is done, you can set it up here.

There's also a "order irrelevant" checkbox. By default the placement of the ingredients are important, and crafting only works
if the player places them exactly as you set it up in the recipe. If you check this checkbox, then the player can place the
ingredients in any order.

On the right, you have a button to set the portrait. This has to be independent to the NPCs, because not only NPCs can make
dialogs. For example an owen could have a crafting dialog too.

Recipes
-------

Each crafting dialog must have at least one, but might have more recipes. These describe the ingredients and the product.

INFO: You can set up required quantity of 0. This means when the crafting is done, that item won't be removed from the player's
inventory, but still required to craft the product (for example a pan).

Preview Crafting Dialog
-----------------------

To get a glimpse how the dialog will look like in the game, press the <imgt ../img/preview.png> "Preview" button. If you don't
like the dialog window or the font, you can set those in the `Interface` > `Elements` menu, see [UI Elements].

When you're satisfied with the dialog, do not forget to press <ui1>Save</ui1>.
