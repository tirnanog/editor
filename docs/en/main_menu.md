<h1 tab20 main_menu>Main Menu</h1>

Customizing the Main Menu
-------------------------

Go to `Interface` > `Main Menu`. It is one single form with all the relevant options.

Just like the other menu entries under the Interface menu, this also has a <imgt ../img/preview.png> "Preview" button at the
bottom, but here you have two: one for the main menu itself, and another one for the character generation window which is
shown when the user selects the "New Game" option.

Intro
-----

You can select an intro cutscene which is played before the main menu is first shown. You typically want to select a
cutscene with a neat animation of your team's logo here.

It is possible that a game doesn't have a main menu at all. It that case, uncheck the "main menu enabled" checkbox.

Background
----------

You can select a background music, a background movie or a background image with 3 parallaxes. Music works both for movie
and background image, however movie files could have their own audio tracks as well.

Title
-----

Here you have two options. Either you can use a header image sprite with some cool graphics of the game's title, or you
can display the game's title using a specified font. You can also do both, if it this is a sequel, then the header image should
be the same as the in the first game (it is important that users recognize it), and text title should be the episode's title.

NOTE: The textual title is translated to the end user's language, while the header image isn't.

Buttons
-------

These are the buttons used to display the main menu's options. Could have used the standard buttons, but thought game makers
would like to use menu options on the main menu that stand out. Just like with the UI elements, you can specify the button's
left sprite, background and right sprite, as well as font type, size, color etc.

New Game
--------

Probably not the best place for this, but when the player clicks on the "New Game" main menu option, they have to be asked for
their character's name, and also an attribute distribution graph is shown. As this is the one and only place where the player
needs to enter text into an input box, as well as the only place where the distribution graph is displayed, I put their
configurations here.

Exit Game
---------

To configure what is shown when the player selects the "Exit Game" option, see [Credits].
