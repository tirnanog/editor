Media
=====

Importing Audio and Video
-------------------------

Go to `Assets` and select the media type. On the page that appears, you'll see the list of available media files of that kind.
Note the category selector at the bottom middle, you can select media category there too. To add a new one, click on the
<imgt ../img/add.png> Add button in the bottom right corner. A file selector window will appear.

<h3 tab15 music>Instrumental Music</h3>

In the file selector window select a music file. It can be in various formats, .ogg, .mp3, .wav, .voc, .mod, .xm, .it etc.
(if you have `libflac` or `libopus` libraries installed, then those formats are supported as well). When the file selected, click
on the <ui1>Import</ui1> button.

All music will be converted into .ogg (with stereo or surround 5.1 channels) automatically.

For the music, I recommend to use [SoundTracker](http://www.soundtracker.org) or [MilkyTracker](https://milkytracker.org) (you
can record music with Audacity too). For old school goodies, there's a modernized clone of
[FastTracker II](https://github.com/8bitbubsy/ft2-clone) running on Linux.

<h3 tab16 sounds>Sound Effects</h3>

Same as with music files, but they are going to be converted to an .ogg with mono channel (sounds are mixed into the output
using a placement effect, so they must be mono).

For the sounds, I recommend to use [Audacity](https://www.audacityteam.org).

<h3 tab17 speech>Speech</h3>

Recordings of voice actors are similar to sound effects, will be converted to an .ogg with mono channel. The only difference
is, the filename will be suffixed by the ISO language code, for example `_en`.

<h3 tab18 movies>Movies</h3>

You can always import .ogv Theora videos without any dependency. But if you have [ffmpeg](https://ffmpeg.org) installed, then
all the other formats (.mp4, .mkv, .mpg, .avi, .flv etc.) supported as well, and they will automatically show up in the editor's
file chooser window.

On Linux, use your distro's package manager, for example `apt-get install ffmpeg`. On Windows, download the latest from
[ffmpeg.org](https://ffmpeg.org) (probably called `ffmpeg-git-essentials` something), and from the downloaded archive, extract
`ffmpeg.exe` (and that file only) to `C:\Program Files\TirNanoG` (or anywhere in your `%PATH%`).

Do not try to import full-length movies, only short (1 - 2 min) cutscenes. Audio tracks in videos **must never** contain
speech, only instrumental music and sound effects (that's because game translation would be impossible otherwise). The editor
(and the player) takes care of mixing the localized speech audio track onto the video.

To create videos, you can use [OpenShot](https://www.openshot.org) or [ShotCut](https://shotcut.org). You can also generate
videos of animated 3D models and scenes with [Blender](https://www.blender.org). All of these tools can export to Theora.

NOTE: You can convert your video in any format into Theora by hand: <pre>$ ffmpeg -i movie.avi -q:v 5 -r 30 -q:a 3 -ar 44100 -ac 2 movie.ogv</pre>
BTW, this is what the editor does under the hood.
