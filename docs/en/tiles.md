<h1 tab35 tiles>Tiles</h1>

Defining Meta Data for Tiles
----------------------------

Go to `Game` > `Tiles`. By default tiles just provide the background of the map. But you can add meta info to them, and by that
give properties to them. On the left you can see all the tiles that already have such a meta info. Double-clicking on the list will
load that tile. To load a tile that does not have meta info yet, use the tile sprite chooser on the tool bar.

On the tool bar you can see the tile sprite chooser and the tile combining tool.

Combining Tiles
---------------

It might happen that your imported tiles contain an edge or corner, but not in combination with another background tile, just
with a transparent background. TirNanoG supports multiple layers of tiles, so you can place both the background tile and the
transparent tile at the same spot on the map, yet sometimes that manz layers isn't enough. When that happens, you can combine
those tiles into a new single tile which then can be placed on one layer.

For example, in an imported tileset there can be a lake. The shore of the water might be a unique tile (like water with grassland,
water with rocks, water with snow etc.), or it could be just the shore with a transparent background. For the latter, open
the grassland tile, select the water shore tile then press <imgt ../img/comb.png> "Combine" icon. You'll get a new tile saved as
if grassland + water combination would be provided by the imported tileset.

Tile Properties
---------------

You might want to set how fast the characters can move on specific tiles. For example, on ice the speed can be 110%, and on
rocky mountains only 50%. If you set the modifier to 100%, there'll be no change. It is common to set the modifier to 150%
for "road" type tiles, so that characters will be able to move faster on roads.

Tiles doesn't have event handlers, but you can set an audio for the "on walk", "on swim", and "on fly" events.
These are played when the player moves above a tile of this kind with that transportation method.

Automap Rules
-------------

Placing edge and corner tiles on maps can be overwhelming. To help you with that, TirNanoG supports automapping (which is
different to autotiling). In autotiling you have tiles splitted into 2 x 2 subtiles. This only works with orthographic tiles,
and with a little hack with isometric, but not for hexagonal ones. Automap on the other hand defines a pattern, which if matched,
then the tile is automatically placed. This works with all kinds of grids without hacks. You have a list of these pattern rules,
meaning you can set up multiple combinations with different neightbours if you want.
