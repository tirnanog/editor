<h1 tab22 alerts>Alerts</h1>

Configuring Alerts
------------------

Go to `Interface` > `Alerts`. Here you can set up the look'n'feel of the pop-up messages.

Just like the other menu entries under the Interface menu, this also has a <imgt ../img/preview.png> "Preview" button at the bottom.

Font
----

Specify which font to use.

Duration
--------

Specify (in partial seconds) for how long an alert message should be shown. You can also specify the fade in and fade out
times with the sliders.

Messages
--------

The translatable text and sound effect to be used. Currently only [quests] have alerts.
