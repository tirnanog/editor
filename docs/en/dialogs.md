<h1 tab32 dialogs>Dialogs</h1>

Creating Dialogs
----------------

Go to `Game` > `Dialogs`. On the left you can see the already created dialogs. Double-clicking on one of them will load that
dialog.

You have on the right the internal name of the dialog that the editor uses, otherwise the tool bar is empty.

Who said and What is Said
-------------------------

Title is translatable and shown in-game. It contains the name of the NPC who is saying the dialog, and optional.

Message is translatable, and mandatory. You can't place newline characters in the message, line breaks are handled automatically,
because each player might have a different screen resolution, and with that a different dialog size. On the other hand you
can use `{}` characters, which will be replaced by the player's name, or `{attribute}` which will be replaced by that attribute's
value.

If your game has voice actors and speech recordings, then you can also set an audio file speaking the words of the message.

On the right, you have a button to set the portrait. This has to be independent to the NPCs, because not only NPCs can make
dialogs. For example a sign on the wall might pop up a dialog with the translated text (in which case there should be no
portrait at all, or could be a portrait of the sign). Second, NPCs might have several face expressions, so you should choose
the proper one for this dialog (like happy, angry etc.).

Possible Answers
----------------

You don't have to specify answers, but you can. If you do, you can add as many options as you like, but keep in mind that for
a good in-game experience 4 or 5 tops is recommended. More than 8 will most definitely confuse the players and will ruin your
game.

If you are creating a game where the players can't customize their character (see [predefined heroes]), you might want to add
speech audio to the answers. With customizable characters I do not recommend this, because it looks strange (or rather funny)
when a brutal orc warrior sounds like a little girl.

Furthermore, dialog answers can be conditionals and they can provide attribute changes. For example, a certain answer can only
be shown if the player's character is an elf.

Using in Event Handlers
-----------------------

You can display dialogs from event handlers, using the "Dialog" command. To give you more control than just providing attributes,
it will return the selected answer's number in the local variable "a" in reverse order (first answer = number of answers - 1,
last answer = 0). <imgr ../img/dlgcmd.png>If you place a conditional structure right after a "Dialog" command and set its expression
to "a", then you can do different things depending on the answer. The first branch will be executed if the player chooses the first
answer, the second branch when they choose the second answer, etc. and the last, zero branch when they choose the last answer.

For example

```
+---------------------+
| () dialog1    00:01 |
+---------------------+
+-----------------------------------+
| /\ [+][-] a                       |
|     2     |     1     |     0     |
+-----------+-----------+-----------+
| things    | things    | things    |
| to do on  | to do on  | to do on  |
| first     | second    | third     |
| answer    | answer    | answer    |
+-----------+-----------+-----------+
```

Preview Dialog
--------------

To get a glimpse how the dialog will look like in the game, press the <imgt ../img/preview.png> "Preview" button. If you don't
like the dialog window or the font, you can set those in the `Interface` > `Elements` menu, see [UI Elements].

When you're satisfied with the dialog, do not forget to press <ui1>Save</ui1>.
