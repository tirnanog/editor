Tile Naming
===========

NOTE: The editor allows any naming scheme you'd like. The one below is just the naming convention used in the
[TirNanoG Base](https://tirnanog.codeberg.page#templates) asset set.

HINT: The suffix guide is also available in the [guides.zip](https://tirnanog.codeberg.page/guides.zip) as GIMP layers and
as individual PNG files too.

General Suffixes
----------------

Tiles are named systematically. Most variants are labelled `a` - `z`, sometimes with `1` - `9`. Things that can be open or
closed are always suffixed by `0` (closed) or `1` (open). Suffix strings follow a simple rule, a combination of letters:
`l` (left), `m` (middle, repeatable), `r` (right) if it's horizontal, and `t` (top), `m` (middle, repeatable), `b` (bottom)
if the item is vertical. If it has both horizontal and vertical variations, then the suffix contains `h` or `v` too,
respectively. If there's also a one slot big version, that's suffixed `c` (center).

```
t                 vt
m     l m m r     vm  hl hm hm hr
m                 vm
b                 vb  c
```

When the item can be stretched both horizontally and vertically at the same time, then the combinations are `tl` (top left),
`tm` (top middle, this item repeatable horizontally), `tr` (top right); `ml` (middle left, repeatable vertically),
`bg` (instead of middle-middle it's called background, repeatable both vertically and horizontally), `mr` (middle right,
repeatable vertically); `bl` (bottom left), `bm` (bottom middle, repeatable horizontally), `br` (bottom right). If the middle
item can be used in combination with another (giving a two slots wide version), then that additional tile is suffixed `n` (next),
but more commonly alternate version are just simply numbered.

```
tl tm tm tr
ml bg bg mr
ml bg bg mr
bl bm bm br
```

Terrain Suffixes
----------------

Terrain types are simply suffixed `a` - `r`. There's no logic in that, instead when you search for a terrain type it should be
displayed in a visually helpful way in the search results box. Here `d`, `l` is repeatable horizontally, `f`, `n` vertically.
The center item `a` is always repeatable both vertically and horizontally, and it usually has alternate versions
(suffixed by `b`, `i`, `j`) to break the monotonity. Items `q` and `r` have all their edges cleared, with a little bit of
terrain in the middle. The wangset correlations are as follows:

```
11 - a, b   00 - c   00 - d   00 - e   01 - f   11 - g   11 - h
11          01       11       10       01       10       01

11 - i, j   01 - k   11 - l   10 - m   10 - n   10 - o   01 - p
11          00       00       00       10       11       11

00 - q, r
00
```

Roof Suffixes
-------------

Similar to general suffixes, but with little additions depending on the direction the roof is facing.

### South

This is the default, the widest area shown of the roof. The edges are 2 x 2 grid sized to accomodate ornaments if any. Therefore
the top middle and bottom middle sprites are 1 x 2 grids, and middle left and middle right 2 x 1 grids in size.

```
so_tl    so_tm    so_tr
so_ml    so_bg    so_mr
so_bl    so_bm    so_br
```

### North

Since the TirNanoG Base asset is an orthographic set, this is more likely top view. Usually roofs might have a flat top, that's
their North side.

```
no_tl    no_tm    no_tr
no_ml    no_bg    no_mr
no_bl    no_bm    no_br
```

There's a twist, because it usually has an edge, so there's a need for some continuation sprites too (using wangset notation):

```
10 - no_bg1    01 - no_bg2
00             00

00 - no_bg3    00 - no_bg4
10             01
```

### South-West and South-East

These are beveled diagonal, and consist of bigger sprites. You cannot stretch these, because they must match the 1:2 sloop,
have fixed size of 2 x 4 grids.

```
sw    se
```

### Rooftop Parts

These mini towers also have their own roof. Usually they are smaller, perpendicular protrusion parts. Single sprite each, with
fixed size, 2 x 2 grids (or with south direction 3 x 2).

```
we_top    ea_top

     so_top
```

### Joins

These are named after the two directions to make life simpler. The `t1` and `t2` are both top elements, the only difference is,
there's no more roof above `t1`, while the roof continues upwards for `t2`.


```
                  we_so_t1    ea_so_t1
                  we_so_t2    ea_so_t2
         we_so_m                        ea_so_m
we_so_b                                          ea_so_b

                   so_we_t    so_ea_t
          so_we_m        so_bg         so_ea_m
so_we_b   so_bm          so_bm         so_bm     so_ea_b
```

### West-East Roof

This is the case when you see both sides of the roof going down in West-East direction (up-side down V). There are two versions,
an acute angled (suffixed `acute_`) and a bevel angled roof (suffixed `bevel_`). The last suffix part comes from the position:

```
        la    ra
    lb  lc    rc  rb
ld  le  lf    rf  re  rd
lg  lh  li    ri  rh  rg
lj  lk  ll    rl  rk  rj
lm                    rm
```

On both sides the `le` or `lh` is the background (depends on the roof's "thickness"), so those are stretchable both vertically
and horizontally. The only difference between acute and bevel is, that the second column on both sides (`lb`, `le`, `lh`, `lk`
and `rb`, `re`, `rh`, `rk`) have double width for bevel to accomodate the 1:2 sloop. The last row, `lm` and `rm` is always
twice the width, even for `acute_lm` and `acute_rm`.

The roofs are made carefully in a way so that you can mix the second columns of acute and beveled roofs, creating a "break" in
the roof's sloop if you like. For example:

```
                              acute_lb
                            acute_lb
                    bevel_lb
            bevel_lb
  acute_lb
acute_lb
```
