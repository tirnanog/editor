Objects and Tiles
=================

There are not much to say about objects, because they have minimal compatibility requirements. Keep in mind that objects
have orthograpic (top-down) [perspective], and you must choose the colors for [props and objects] carefully so that they
nicely fit with the tiles.

When you're creating bigger objects, don't forget that players might walk *inside*. That's only possible if you
create multiple layers of an object.

<imgc ../img/bridges.png><fig>Separate large object into layers</fig>

Grid Size
---------

The general tile grid is 32x32 with (possible) sub tiles at 16x16.
The rationale being that the basic size of a square object (eg a chair
or a character) is a 32x32 area.  All base assets are designed to work
at 32x32 tiling, and it’s recommended that you build yours to be so as
well. However, for versatility tiling can happen at 16x16 resolution.

<imgc ../img/32x32grid.png><fig>32x32 grid example</fig>

Perspective
-----------

The camera angle is top-down, roughly 60 degrees.

<imgc ../img/ortho.png><fig>Top-down, orthographic projection</fig>

Rendering should be orthographic, which means there is *no*
perspective... things **do not** get smaller as they move into the
distance.  If you’re using perspective techniques on your props or
tiles, that’s wrong.

<imgc ../img/chest.png><fig>Chest perspective example</fig>

Tile Authoring
--------------

Details are simplistic.  There are more details on the edges, and the
center tile should be either one color or a very subtle pattern.

Occasional detail tiles should be thrown in to break the monotony of
having a single repeating tile.

<imgc ../img/tiles.png><fig>Demonstrating tiles that can be used to make more complex patterns</fig>

Edged tiles such as walls and floors should be arranged in a similar
manner as the establishing art.

HINT: You can set up [automap rules] and then the map editor will automatically place these edge tiles whenever preferable.
