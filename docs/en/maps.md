<h1 tab36 maps>Maps</h1>

The Map Editor
--------------

Go to `Game` > `Maps`. On the left, you'll see the list of maps. Double-clicking on one of them will load that map.

In the toolbar, you'll see the "Map Meta Information" icon, the Layer Selector, the item selector, a special icon to select an
"erase" item, and on the right the map's internal name, used within the editor. Other icons on the toolbar depend on which layer
is the active one, for example for tile type layers, there will be painting tools, selection tools, copy'n'paste tools etc.

The zoom control is located below the map, on the button bar.

HINT: To duplicate a map, first load it, give it a different name and press <ui1>Save</ui1>.

Shortcuts
---------

There are some shortcuts that work on the map regardless which layer is active:
- using the <mbr> right-click, holding it down and moving the mouse will move the map
- <kbd>⯇</kbd> left, <kbd>⯈</kbd> right, <kbd>⯅</kbd> up and <kbd>⯆</kbd> down cursor keys will scroll the map
- <kbd>Ctrl</kbd> + <kbd>⯇</kbd> left, <kbd>⯈</kbd> right, <kbd>⯅</kbd> up and <kbd>⯆</kbd> down cursor keys will rotate the map
- <kbd>-</kbd> minus will zoom out, <kbd>+</kbd> / <kbd>=</kbd> plus and equal will zoom in
- <mbw> mouse wheel will scroll the map (in horizontal too, if your mouse / touchpad supports that)
- <kbd>Ctrl</kbd> + <mbw> mouse wheel will zoom in or out
- <kbd>Ctrl</kbd> + <kbd>Z</kbd> will undo the last modification
- <kbd>Ctrl</kbd> + <kbd>Y</kbd> will redo it
- <kbd>Ctrl</kbd> + <kbd>S</kbd> will save the map (only if it has an internal name set)

Map Meta Information
--------------------

If you click on the <imgt ../img/info.png> "Meta Info" icon, then you'll be able to see and edit the map's meta information.
- Here title is the map's translatable, in-game name, if any.
- Daylight specifies the light overlay, you can set the daylight's duration in minutes, and a palette using a sprite. For more information, see the [lights] section.
- This is also the place to set a background music for the map.
- Below are the parallaxes. You can have more of them, and you can set them below or above the map layers.

The first and third values specify how much the parallax moves every given time intervals on the X and Y axis (good for example
creating a raining effect). The second and fourth values on the other hand specify how much the parallax moves when the player
moves (good to create a perception of depth). The combination of these can give a very nice-looking effect on the parallaxes.

Map Layers
----------

Next to the info icon, there's the <imgt ../img/layers.png> "Layers" icon. Clicking on that will bring up the layer selector.
Here you can click on the visibility (eye) icon to show and hide layers individually, or you can double-click on the layer's
name to select that layer for editing. Keep in mind that the selected layer is always set to visible. You can also change the
current layer with the <kbd>PgUp</kbd> and <kbd>PgDn</kbd> keys.

There are several layers:

- "Ground 1" to "Ground 4" are **under** the player, and there's more of them so that you can use transparent tiles too.
- "Objects" is at the same level as the player, and it also loads the [collision mask] if the tile has an object meta info.
- "NPCs" and "Spawners" are at the same level as the player too. They are separated because they use different sprite sets.
- "Roof 1" to "roof 4" are **above** the player, and "Roof 4" layer disappears when the player walks under.

There are two additional techinal layers, not shown in-game:

- "Collision" is a read-only layer, calculated automatically from the "Objects" layer.
- "Patrol paths" contains routes, directed vector lines.

Choosing Brush
--------------

Depending on which layer you are editing, the brush chooser will offer different items. For all layers except the paths, the
<imgt ../img/erase.png> "Erase" icon selects a special "clear" item. For the path layer erase icon erases the path.
The keyboard shortcut for selecting the item is the <kbd>Space</kbd>. Press <kbd>E</kbd> to switch to erase mode.

HINT: There's a logic behind tile suffixes, which could help you a lot picking the correct one. See [tile naming] for details.
You can search for tile tags, and if translation exists, translated tags too.

Pipette
-------

Just like in GIMP, using this <imgt ../img/pipette.png> Pipette tool you can select a tile or object from the map to draw with.
The keyboard shortcut for this tool is <kbd>O</kbd>.

Paint
-----

The default tool looks like a brush <imgt ../img/paint.png>, and you can paint the selected item on the map by <mbl> left clicking
and holding the mouse button down. The keyboard shortcut for this tool is <kbd>P</kbd>.

If you press the <kbd>Shift</kbd> key, then you can draw lines, just like in GIMP.

NOTE: If there's an active selection, then you'll be only able to draw inside that selection.

Bucket Fill
-----------

The <imgt ../img/fill.png> bucket fill tool replaces contiguous areas with the selected tile. The keyboard shortcut for this
tool is <kbd>B</kbd>.

If there's a selection and you <kbd>Shift</kbd> + <mbl> click, then the entire selection will be filled, no matter the
continuity in tiles.

If you press <kbd>AltGr</kbd> / <kbd>GUI</kbd> while you <mbl> click, then all occurances of the tile at the position
where you clicked will be replaced with the selected tile.

NOTE: Just like with the <imgt ../img/paint.png> paint tool, if there's an active selection, only the selected area is modified.

Selections
----------

Works exactly like in GIMP. You have three tools:
- <imgt ../img/boxsel.png> rectangle selection (shortcut <kbd>R</kbd>),
- <imgt ../img/freesel.png> free-form polygon selection (shortcut <kbd>F</kbd>) and
- <imgt ../img/fuzzysel.png> fuzzy selection (shortcut <kbd>U</kbd>).

If you just click on the map with the rectangle selection, that will clear all selection. If you hold down the <mbl> left mouse
button and move the mouse, then you can select a rectangular area with rectangle select.

With polygon select, first you click on the start point. Then you can make further clicks on other coordinates, each will add a
line from the previous point to the new one. When you click again on a point which has the same map coordinate as the first point,
then the path is closed, and the selection is made.

Fuzzy select will select all neightbouring tiles of the same kind. If you press <kbd>AltGr</kbd> / <kbd>GUI</kbd> during
selection, then all tiles of the same kind will be selected, not just the neightbouring ones.

Normally a new selection will clear the previous one, but just like in GIMP, you can combine selections.

- By holding <kbd>Shift</kbd> down, you can add a new selection to the previous one.
- By holding <kbd>Ctrl</kbd> down, you can intersect the previous selection with the new one.
- <kbd>Ctrl</kbd> + <kbd>I</kbd> will invert the current selection.
- <kbd>Ctrl</kbd> + <kbd>A</kbd> will select all.

There's no restriction, you can combine selection tools as you like. For example, you can select a rectangular area, then
switch to polygon select, press and hold <kbd>Ctrl</kbd>, make a selection to "cut out" some parts of that rectangular area.

Copy'n'paste
------------

Selected areas are not only good to limit where Paint and Bucket fill can modify the map. You can also copy the selected
items to a clipboard (<kbd>Ctrl</kbd> + <kbd>C</kbd>), and then paste them (<kbd>Ctrl</kbd> + <kbd>V</kbd>) at a different
position. To cut out the selected part, use <kbd>Ctrl</kbd> + <kbd>X</kbd>. With this you can easily edit larger parts of the map.

HINT: You can copy'n'paste between layers too. For example to move an entire area to another layer, select the area, press
<kbd>Ctrl</kbd> + <kbd>X</kbd>, switch to the new layer and press <kbd>Ctrl</kbd> + <kbd>V</kbd>.

Flipping
--------

You can flip the copy'n'paste clipboard vertically (<kbd>V</kbd>) or horizontally (<kbd>H</kbd>). This way you can make a
selection, copy it, flip it and then paste it to create more variations on the map.

Flip also works for patrol paths, but there it changes the direction.

Chaining
--------

The size of one map is limited (by default to 32 x 32). But don't worry, these relatively small maps can be chained indefinitely
(ok, to be precise you can chain up a few million maps). When you scroll the map (or you <mbr> right click on the map and move the
mouse), on the corners in the corresponding directions a <imgt ../img/linkmap.png> "Link" button will appear with an anchor icon.
Clicking on these will pop up the list of maps, and you can select which map should be in that direction. These chained maps are
loaded automatically in-game, so the player will not know where one map ends and where another starts, for them it will look like
one big map.

It is not necessary to chain maps. You can create separate maps too, which are not chained, or chained in a local group.
These can still be accessed by players via the "set player location" command in event handlers. Typical example is multiple
chained maps of the overworld, and when the player touches the cave's entrace, he gets teleported to another group of
chained maps, the dungeon.

<h2 map_preview>Preview</h2>

On the button bar, you have a <imgt ../img/preview.png> "Preview" button. With that you can see the map in its entirety: with
neightbours, parallaxes, lights etc.

When you're finished with the map, don't forget to press <ui1>Save</ui1>.

Tiled Compatibility
-------------------

The editor saves maps in TMX format, which is native to the Tiled mapeditor. There's a catch though, TirNanoG can handle tilesets
in atlases, while Tiled can't, so you *must* unpack atlases first, otherwise you'll get "image not found" errors in Tiled. See
[working with atlases] for more details. (For the records, this is an open, and still unresolved issue in Tiled since 2015.) For
the TirNanoG Editor this simply doesn't matter, it works with both packed and separate sprites, no matter how tilesets are stored
(FYI it does not use the image URI in the TMX at all, just passes the tileset's `name` attribute to its sprite manager).
