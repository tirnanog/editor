Projects
========
<h2 tab2 creating_a_project>Creating a Project</h2>

In the menu, choose `Project` > `New Project`. Fill in the fields:

- **GameID** is the internal name of your game. Up to 16 characters, which may only contain Latin letters and numbers.
- **Title** is the translateable name of your game that user's see. You can use any UTF-8 character in this.
- **Template** if you have downloaded some game [templates], you can choose one of them here.
- **Tiles** type and size of the map tiles. Tile type can be either:<grid><gr><gd><imgl ../img/ortho.png><i><b>orthographic</b></i> (also top-down)</gd><gd><imgl ../img/iso.png><i><b>isometric</b></i> (sometimes called 2.5D)</gd></gr></grid>
- **Map Size** in tiles. One map will only be N x N tiles in size, but you can chain up multiple maps into a big open world.

WARNING: The product of the Tile Size and the Map Size should not exceed 4096, because that could lead to problems for some
players with older GPUs or limited resource handhelds.

When ready, press the <ui1>Create New Project</ui1> button.

<h2 tab3 loading_a_project>Loading a Project</h2>

If you have already created a project, go to `Project` > `Load Projects`. From the list, select your game and double-click on it.

When you start the TirNanoG Editor, and you already have some projects, this is the page that will greet you.

More info about what files are stored in a [TirNanoG Project](https://codeberg.org/tirnanog/editor/src/branch/main/docs/proj_format.md).

<h2 tab4 import_game>Import Game</h2>

You can import a game in production ready [TirNanoG File Format](https://codeberg.org/tirnanog/editor/src/branch/main/docs/tng_format.md)
into the editor. Go to `Project` > `Import Game`. Select the desired game file and press the <ui1>Import</ui1> button.

This will also create a project for that game.

<h2 tab5 import_template>Import Template</h2>

Go to `Project` > `Import Template`. Select the desired template file and press the <ui1>Import</ui1> button.

This is very similar to creating a new project from template, except this will not create a new project, rather loads the template
into an existing project. With this feature you can load several smaller templates into the same project, thus it is not used with
full game templates, rather with small modules.

Export Game
-----------

Go to `Project` > `Export Game`. You can also access this with the <kbd>F2</kbd> keyboard shortcut.

Output `game.tng` files will be generated under the corresponding project's `~/TirNanoG/(gameid)` subdirectory. To use these,
you'll need the [TirNanoG Player](https://tirnanog.codeberg.page/player).

You can play the `game.tng` file from the project's directory by pressing the <imgt ../img/playbtn.png> Play button, or copy first to
the game's normal path, `C:\Program Files\(gameid)\game.tng` (on Windows) or `/usr/share/games/(gameid)/game.tng` (on Linux), where
the TirNanoG Player is looking for these files. In addition to the `.tng` file, launchers like a `(game title).lnk` shortcut link
(for Windows) and `(game title).desktop` (for Linux) are also generated. These start the game from its normal path.

<h2 tab7 export_extension>Export Extension</h2>

Go to `Project` > `Export Extension`. You can also access this with the <kbd>Shift</kbd> + <kbd>F2</kbd> keyboard shortcut.

An extension is very similar to a `game.tng` file, but it must have a different name, and you can control which parts of the
project to export into the file.

Export Template
---------------

You can not only load templates into you project, but you can also generate a template from a project. For that, go to
`Project` > `Export Template`.

The resulting template will be saved as `~/TirNanoG/(gameid).zip`.

<h2 tab9 license_keys>License Keys</h2>

### Editor License for Game Files

The `Project` > `License Keys` menu might be inactive. To create encrypted and proprietary games with this editor,
[you must ask for permission](https://codeberg.org/tirnanog/editor/issues/new) from the author in a *confidential* issue
(see [TirNanoG File Format License]). In return I'll send you a `license.txt` file that has to be copied under the game
project's folder. This will unlock the creation of **encrypted** `game.tng` files and generation of end user **decryption keys**
within the editor.
![Generating end user license keys](../img/tngel.png)
How you distribute and sell those decryption keys and to whom is entirely up to you (see some tips and tricks below). When
end users try to play an encrypted game file, the TirNanoG Player automatically asks for these license keys and decrypts, no
third-party tool nor special player required. What's more it is not possible to import encrypted game files back into the
TirNanoG Editor for further modifications.

<h3 webshop selling_your_game>Selling Your Game</h3>

You have several methods to do this, in increasing order of automation.

#### Method 1: Manually

NOTE: Ideal for hobbyists, fan and doujin games, with just a static website or an FB profile.

The simplest of all, go to `Project` > `License Keys`. On that page, press the <ui1>Generate</ui1> button. A new end user decryption
key will be copied to your clipboard, and you can paste that into an e-mail, into a chat etc.

Although this is extremely simple, this method requires human interaction each time a game is sold.

#### Method 2: Pre-generated Keys

NOTE: Ideal for somewhat popular indie game studios and semi-professionals, renting their site from a webhost
provider (typically running WordPress, Drupal, Joomla, eCommerce etc.).

Second choice is to pre-generate a bunch of keys. Like before, go to `Project` > `License Keys` and press <ui1>Generate</ui1>
button a couple of times (and now, press it a few more times). Then upload `~/TirNanoG/(gameid)/endusers.txt` to your server,
and use that file from your webshop. You can import that into an SQL table, or just read the file directly, up to you.

WARNING: Your webshop is in charge of keeping track which keys in the file were sold.

When your webshop runs out of available keys, just press <ui1>Generate</ui1> a couple more times and upload the new `endusers.txt`
to your server.

This does not need anything special on the server, just a plain text file (so this is *extremely secure* and easy to integrate,
works with all kinds of webshops) and it only requires human interaction occassionally.

#### Method 3: On Demand Key Generation

NOTE: This method is for professional game makers who make enough profit to afford their own webserver (typically Azure, AWS, etc.
instance) where they can run 3rd party applications.

On your server, create a directory `/home/(webserver user)/TirNanoG/(gameid)`, and copy your editor license there. For
example, `/home/apache/TirNanoG/fantasticrpg/license.txt`. When a new key is needed, execute `tnge -g (gameid)` from your
webshop, which returns a new, unique key on the standard output.

For example, in PHP you could do:
```php
$new_end_user_key = trim(exec("/usr/bin/tnge -g fantasticrpg"));
```

Or in Python:
```python
new_end_user_key = os.popen("/usr/bin/tnge -g fantasticrpg").read().strip()
```

This does not need human interaction at all, but in return is has to execute TirNanoG on the server. Because no user
input is passed on the command line and just a string constant is executed, the *security risk of doing this is very minimal*.

HINT: You don't have to copy any of the project files, nor will you need dynamic libraries nor a graphical enviroment installed on
the server! None of those! All you need is the `tnge` executable and a `license.txt` file, nothing else.
