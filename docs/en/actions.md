<h1 tab28 actions>Actions</h1>

Configuring Default Actions
---------------------------

Go to `Game` > `Actions`.

There are interactions between the player and other elements of the game, like NPCs, objects etc. These are always
interpreted in relation. You can either set a handler for all kinds, or set a specific for a particular element. You
can configure the former, global handlers here. For the element specific handler (which override the global ones if
they exists), see [NPCs] and [Objects].

Default Action Handlers
-----------------------

You can have 9 actions. How you define these is totally up to you and to the game you're creating. In a simple fantasy RPG,
action 1 could be the melee attack, and action 2 could be the ranged attack. In an adventure type game action 1 could be
the use object action, action 2 the examine, action 3 the open, action 4 the push, action 5 the talk to etc.

For actions that does not need close proximity, you'll have to tell which attribute stores the range of that action. For example,
if action 2 is the ranged attack, then you could choose the Strengh attribute for the range, meaning the stronger the player
become, the farther they will fire with a bow. Just an example, it is up to you.

What is important, that for all actions that you intend to use, create a default event handler. For example, if action 1 is melee
attack, then this event handler is triggered when the player uses an object which has action 1 set (like a sword), and then
this event handler should describe if the sword hits or not, and if so it should decrease the opponent's HP etc. Without default
event handlers, the objects in your game are just for show and useless.
