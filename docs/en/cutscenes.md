<h1 tab25 cutscenes>Cutscenes</h1>

Creating and Editing Cutscenes
------------------------------

Go to `Interface` > `Cutscenes`. These are used for various purposes: displaying your team's logo, telling the background
story for your game, creating the mood between levels etc.

Just like the other menu entries under the Interface menu, this also has a <imgt ../img/preview.png> "Preview" button at the bottom.

Cutscenes List
--------------

On the left, in the Entity Selector there's a list of already configured cutscenes. By double-clicking on an item, it will be loaded.

Pressing the <imgt ../img/del.png> "Remove" button on the bottom left corner will remove the selected cutscene. Clicking on the
<imgt ../img/erase.png> "Erase" button on the bottom center on the other hand will just clear the form.

To add a new one, specify a name on the top right and press <ui1>Save</ui1> in the bottom right corner.

Cutscene Tool Bar
-----------------

At the top, in the tool bar, you can see and interval and a <imgt ../img/play.png> "Play" button. If there's a selection on the
timeline, pressing this will play the selected part of the background music. It is used to help you locate where you are on
the timeline.

On the right, you can specify the cutscene's internal name.

Below the toolbar is the timeline with the channels:

- Background Audio (Music or Movie, depending on type)
- Slideshow
- Speech
- Subtitles
- Scripts

Clicking on the channel's icon will operate on that channel. By <mbr> right clicking on the timeline you can move it around, or
you can also use the scrollbar. The granulatity is in 1 / 100th of a second, meaning 100 pixels cover 1 second duration.

Adding Background
-----------------

Click on the "Movie" or "Music" channel's icon. Below the timeline four selectors will appear.
<imgc ../img/cutscn0.png>

The first row is for the play once (or play first) movie and music, the second row is for the play in a loop. If you have set both,
then the play once will be played for the first time, then afterwards the play in a loop will be repeated until the user interrupts.
Without a play once, only the play in a loop will be repeated.

Here the video and the audio isn't related: for example you can have a short movie with only video as play first, then another
movie with only video as play in a loop, and one long, play once music for them both. The editor (and the player) will cut these
together automatically.

This background is what gives the backbone of the cutscene. Without you can't use the other channels.

Adding Slides
-------------

Click on the "Slideshow" channel's icon. Select a range on a timeline. To help you with the selection, the background's audio
is visualized, and you can also press the "Play" icon on the toolbar to listen to that selected range. When you're happy with
the selection, below the timeline specify the details.
<imgc ../img/cutscn1.png>

You can also set the selected range in numeric form. After that with the sliders you can set the fade in and fade out intervals.
If you move the left one to the beginning and the right one to the end, then the image will appear and disappear instantly,
there'll be no fading. The last icon in this row, the <imgt ../img/erase.png> "Erase" button will remove this slide from the timeline.

Below there are two input fields, as you have two options to specify the slide: either you pick a background image, or a color.
Using a fullscreen color slide with specific fade in or out intervals can be used to cover other channels of the cutscene. For
example, if you want a map to fade in nicely, then create a full black slide which fades out at the time you want the map to appear.

Slides may overlap, but only two at any given time. If you have two images with the same background but different front
composition (let's say actor at a different position), then you can set them up in a way that the first's fade out
happens exactly the same time as the second's fade in. <imgc ../img/fadeanim.gif> That way what the user will see is an unchanged
background image on which the the actor's first position fades out, and at the same time their new position fades in, giving a
simple illusion of animation. This kind of "animation" is particularly common with voice narrative and still images.

Later you can click on the slide on the timeline's slideshow channel to modify (or remove).

Adding Speech
-------------

Click on the "Speech" channel's icon. Select the time when you want the narration to start (you could select an entire range,
but the length of the range will be overridden according to the length of the loaded audio file).
<imgc ../img/cutscn2.png>

That is why the range's end numeric input is inactive. Next to that you can choose the audio file from the selectbox. For your
convenience, there's a "Play" button here too to listen to what you've selected. The last icon in this row, the <imgt ../img/erase.png>
"Erase" button will remove this audio from the timeline.

WARNING: Different translations of the narration might be a bit longer or shorter than the version you're using. Keep this in mind
when you select the starting positions. Narrations cannot overlap.

NOTE: If you also use scripts, then don't forget that "Dialog" commands might also contain translated speech, which will override
the speech channel.

Clicking on a speech entry on the timeline will allow you to modify (or remove).

Adding Subtitles
----------------

Likewise to the others, click on the "Subtitles" channel's icon and select the range. Just like with slides, in the first row
you have the numerical range input, sliders to specify the fade in and fade out times, and the last icon in this row, the
<imgt ../img/erase.png>  "Erase" button will remove this subtitle from the timeline.
<imgc ../img/cutscn3.png>

These can be actually used more for just subtitles. Therefore in the second row, you can specify a position (with a coordinate
helper) and the alignment of the text. A typical subtitle would have position of `50%, 98%` and would be `center`ed, but you can
set up different values too. After this you can find the usual font selector, specifying the font color, font weight, font size,
font family for the text.

In the third row, you have one text input field, for the text itself. In it you can use `{}`, which will be replaced by the
player's name, or `{attribute}` which will be replaced by that attribute's value. No newlines allowed, but you can add multiple
subtitles with adjacent positions.

As with the others, you can click on a subtitle entry on the timeline to modify (or erase).

Now texts can not only overlap, but it is very likely that you want to display multiple texts at the same time, just in different
positions. Selecting multiple entries on the timeline which have exactly the same starting and ending range isn't possible. For
that reason, below the text input there's also a list selector for the subtitles, where you can choose by text, no matter if
they overlap or not.

Adding Scripts
--------------

Finally, you have a "Scripts" channel too. You cannot really add entries for it on the timeline, since there's only one. But if
you set a range, the script will start with an appropriate "Delay" command (and if you change the range, the "Delay" command will
change automatically).
<imgc ../img/cutscn4.png>

On the left you have the main script editor area. On the right there is the command palette. You can drag'n'drop commands from the
palette into the editor area to add new commands. You can also drag'n'drop commands on the editor area to re-order them. To
delete a command, simply drag it and drop it *outside* of the editor area.

First things first, you should add a "Select a scene" command and select a map and position, so that you'll have a background for
your script. This also selects the daytime, which is important if you have set up an [ambient lights] overlay for that map.

Here in the cutscene editor you have limited number of commands on the palette. No control structures (like iteration or
conditional), but you have some Logo-like moving commands for the actors. To use these, first you have to "Place an NPC" on the
map. After that, that NPC will become the selected actor. If you have more NPCs, then there's a "Select the NPC" command to select
which one you want to control. Giving movement commands to actors happens **asynchronuously**, this means you can select an actor,
give a pile of movement commands to him, then select another actor and add another pile of movements to her. The script won't wait
until they finish those movements, it will continue immediately, and both actors will do the movements **simultaneously**. If you
want the script to stop execution until the actors are finished, you have a "Wait for actor to finish task" command (which also
applies to the selected actor, just like the movement commands).

There's one exception to this, and that's the dialog. Dialogs are shown for the specified amount of time, and they stop the
script until that time passes. This is so because you can display a dialog while the actors are moving or playing a character
animation.

Other things you can do in a cutscene includes removing and placing objects on the map; playing object and NPC animations
etc. One more thing, with objects the same selection mechanism applies as with actors, meaning the object that was last placed on
the map will become the selected object, and that's the one that will play the object animation. To select another object, then
"replace" the object on the map with exactly the same object.

Checking and Saving the Cutscene
--------------------------------

You can check the entire cutscene by clicking on the <imgt ../img/preview.png> "Preview" button, even in an unfinished state.

But before you could save the cutscene you have to add an internal name to it (last input box on the toolbar), then press the
<ui1>Save</ui1> button.
