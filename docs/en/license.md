<h1 tab10 editor_license>TirNanoG Editor License</h1>

<imgr ../img/gplv3.png>The TirNanoG Editor is Free and Open Source software available under [GPLv3+](https://codeberg.org/tirnanog/editor/src/branch/main/LICENSE) or any later version of that license.

```
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
```

<h1>TirNanoG File Format License</h1>

NOTE: The file format's license has nothing to do with the game's license stored in this format, just like how
PKWARE's copyright does not influence the file's licenses stored in a .zip file.

The TirNanoG File Format is **dual licensed**:

Unencrypted
-----------

<imgr ../img/ccbyncsa.png>By default, TirNanoG File Format is licensed under [CC-by-nc-sa](https://codeberg.org/tirnanog/editor/src/branch/main/LICENSE) (attribution required, non-commercial, share-alike):

```
CREATIVE COMMONS - BY - NC - SA
https://creativecommons.org/licenses/by-nc-sa/

You are free to:

 - Share — copy and redistribute the material in any medium or format

 - Adapt — remix, transform, and build upon the material
     The licensor cannot revoke these freedoms as long as you follow
     the license terms.

Under the following terms:

 - Attribution — You must give appropriate credit, provide a link to
     the license, and indicate if changes were made. You may do so in
     any reasonable manner, but not in any way that suggests the
     licensor endorses you or your use.

 - NonCommercial — You may not use the material for commercial purposes.

 - ShareAlike — If you remix, transform, or build upon the material,
     you must distribute your contributions under the same license as
     the original.
```

Encrypted
---------

With the written permission of the TirNanoG File Format's author, you can create encrypted game files, see chapter [License Keys]
for more details. For commercial games I recommend this file format option as it gives you not just encryption but a legal
guarantee too that your assets in the .tng file can't be stolen.

The decision and the responsibility of which licensing version to use for the game files is up to the editor's users, and it
**does not influence the game's license** stored in this format. Under no circumstances can I, bzt, the TirNanoG Editor and TirNanoG
File Format's author be held responsible for the game files created by others.

Trying to decrypt, disassemble or any other way reverse engineer the proprietary encrypted format is strictly forbidden to
protect the rights of the copyright holders of the assets inside the file.

<h1>TirNanoG Base License</h1>

<imgr ../img/ccbysa.png>The default game template, the TirNanoG Base is licensed under [CC-by-sa](https://creativecommons.org/licenses/by-sa/)
(as well as the LPC assets it is based on). Uses the 4.0 version, or any later version of that license.

```
CREATIVE COMMONS - BY - SA
https://creativecommons.org/licenses/by-sa/

You are free to:

 - Share — copy and redistribute the material in any medium or format

 - Adapt — remix, transform, and build upon the material
     The licensor cannot revoke these freedoms as long as you follow
     the license terms.

Under the following terms:

 - Attribution — You must give appropriate credit, provide a link to
     the license, and indicate if changes were made. You may do so in
     any reasonable manner, but not in any way that suggests the
     licensor endorses you or your use.

 - ShareAlike — If you remix, transform, or build upon the material,
     you must distribute your contributions under the same license as
     the original.
```
