Purpose
=======

To give you a quick start creating games, TirNanoG provides the [TirNanoG Base](https://tirnanog.codeberg.page#templates) asset set.

NOTE: Unlike in other programs, you're not forced to use a specific asset set with the editor. This is just a convenience set,
one of the many possibilities, the TirNanoG Editor can work with any game [templates].

This part of the manual describes everything you need to know if you want to expand that default set with new assets in a
compatible way.

Assets in the TirNanoG Base template were originally based on the Liberated Pixel Cup, however diverged from that
significantly. Most notable differences:

- Standardized animations
- Standardized number of frames
- Standardized head and hands positions on every frame
- Standardized sprite sheet (see [layouts])
- Use of [guidelines](https://tirnanog.codeberg.page/guides.zip) to help keeping compatibility
- Strictly using **CC-by-sa-4.0** license only (or any later version of that license).

To convert the old LPC assets into this new TirNanoG Base format, you can use the [sprsheet](https://codeberg.org/tirnanog/sprsheet)
converter tool.

1. use the sprsheet tool to convert the old assets into TirNanoG Base layout
2. use your favourite pixel editor, load both the guides and the newly created sheet
3. do fine adjustments so that each frame match the guidelines properly

When creating assets from scratch, follow the instruction in this section of the manual. The most important part is to
match your frames with the guidelines, so that your set will fit with the already existing assets nicely.
