<h1 tab14 generator>Generator</h1>

Go to `Assets` > `Generator`. Here you can generate sprites using modular templates with just a few clicks.

NOTE: If the generator appears empty and all options are inactive, then you have to install the
[TirNanoG Base](https://tirnanog.codeberg.page#templates) to get the sprite templates.

HINT: For the TirNanoG Base, the generator is also available over a [web interface](https://tirnanog.codeberg.page/base).

On the toolbar on the right, you have the input field for the name of the sprite to be generated.

Components
----------

Other icons on the toolbar select which component you want to configure, like body, head, clothes, etc.

On the left in the entity list area, you can see the list of available variants of the selected component.
Each variant has a checkbox, with which you can enable / disable that particular variant. For some components,
like body, usually you'll only need one active variant, however other components, like clothes, might need more.

Some templates might have dynamic recoloring options. If that's the case, then you'll see the available color
variants for the currently selected option beneath the entity list. Clicking on the color chooses that variant.

Component types are flexible, and fully customizable. See [expanding] the character sprite generator for more details.

Sprite Preview
--------------

On the right in the main area you'll see a preview of the character to be generated.

When you're satisfied with the preview, press the <ui1>Import</ui1> button, and sprites will be generated and added
to your project. After that they will appear on the "Sprites" tab, and you can use them for your NPCs.

HINT: If you want to use some of these as a player [character] option too, you can enable exactly one variant and generate
sprites for that. Alternatively you can import the sprite template image on the [Sprites] tab by selecting the "TNG-Base"
layout.
