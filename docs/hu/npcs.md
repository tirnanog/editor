<h1 tab30 njk_k>NJK-k</h1>

Nem Játékos Karakterek hozzáadása
---------------------------------

Válaszd a `Játék` > `NJK-k` menüpontot. Balra, az entitásválasztóban látszanak a már felkonfigurált NJK-k. Duplán
kattintva egy sorra az adott NJK betöltésre kerül.

Az eszköztáron látható két ikon, az "NJK beállítása" és az "NJK leltára, loot-ok", valamint jobbra az NJK belsős, technikai
neve. Ezek az ikonok váltják az oldalt, mert olyan sok az opció, ami már nem fért ki egy oldalra.

HINT: Duplikáláshoz kattints kétszer az NJK-ra, módosítsd a nevét, majd kattints az <ui1>Elment</ui1>-re.

Viselkedés beállítása
---------------------

Az első és legfontosabb dolog, hogy hogyan viselkedik az NJK, amikor nincs kapcsolata a játékossal.

- Áll, az NJK csak egyhelyben álldogál
- Bóklászik, véletlenszerűen mászkál
- Járőrözik, egy kijelölt útvonal mentén halad

Mindnek van egy "Sebesség" paramétere (még az álldogálnak is), de a "Bóklászik"-nál egy gyakoriság is megadható, hogy mennyi
időközönként váltson irányt, illetve a "Járőrözik"-nél pedig a járt útvonal sorszáma (az átvonalakat a [térképek] menüpontban
lehet megadni).

Az eseménykezelőkből felülbírálható a viselkedés. Például, alapból lehet "Bóklászik", és a "megközelítéskor" eseménykezelőben
átválthatunk "támadás"-ra.

HINT: A kincsesládák olyan "NJK"-k, amik egyhelyben állnak és csak egy "hozzáéréskor" eseménykezelőjük van.

Eseménykezelők
--------------

A második legfontosabb dolog, hogy miként reagál az NJK, ha kapcsolatba kerül a játékossal. Ezek az eseménykezelők
megváltoztathatják a viselkedést, párbeszédet kezdhetnek, vagy megmutathatják a leltárjukat csere-beréhez, stb.

- kattintáskor: akkor aktiválódik, amikor a játékos tárgy nélkül, vagy semmilyen kategóriába nem tartozó tárggyal kattint
- hozzáéréskor: amikor a játékos hozzáér az NJK-hoz
- távozáskor: amikor a játékos és az NJK távolsága a megadott értéken túllép
- megközelítéskor: amikor a játékos és az NJK távolsága a megadott érték alá csökken
- cselekvéskor: amikor a játékos egy adott kategóriába tartozó tágyat használ az NJK-n
- használatkor: amikor a játékos egy bizonyos konkrét, megadott tárgyat használ

<h2 njk_attributumai>Attribútumok, szprájtok és előnézet</h2>

Úgy néz ki, és pontosan úgy is működik, mint a karakteropciók esetében, kivéve, hogy NJK-knak csak beállítandó attribútumaik
lehetnek. Bővebb infóért lásd a [karakter] fejezetet.

HINT: A szprájt [generáló]val sokféle NJK karaktert ki tudsz generálni.

<h2 npc_inventory>Leltár</h2>

Az "NJK leltára" ikonra kattintva az eszköztáron, beállíthatod az NJK tárgyait.

Itt megadható az NJK játékon belüli, lefordítható neve, és hogy milyen tárgyakkal rendelkezik. A mennyiség és az előfordulás
esélye ugyancsak beállítható. Amikor az NJK meghal, a tárgyai a térképre szóródnak.

NOTE: Fontos a különbség a következő kettő között:
```
50%, 2 kard
```
azt jelenti, hogy 50%-os esélye van, hogy az NJK-nak 2 kardja van, vagy egy se. Nincs olyan, hogy csak 1 kardja van. Másrészről
```
50%, 1 kard
50%, 1 kard
```
azt jelenti, hogy az NJK-nak lehet 0, 1 vagy 2 kardja.

### Kereskedő létrehozása

Ha olyan valakit szeretnél megadni, akivel a játékos kereskedhet, akkor meg kell adni az "Eladási ár" és a "Vételár"
eseménykezelőket. Ezek a hivatkozott tárgy alap árát (lásd az objektumok menüpont [leltárban] fülét) az `a` változóban kapják meg,
a kategóriát pedig a `b`-ben, és ugyancsak ebben az `a` változóban kell visszaadniuk a módosított árat. Általában valami ilyesmi
```
a:=a*110/100
```

hogy 10%-os felárral adjon tovább a tárgyakon, és

```
a:=a*50/100
```

hogy féláron vásárolja vissza őket. De mivel vezérlő struktúrák is a rendelkezésedre állnak, ezért izgalmas
<imgt ../img/cond.png> elágazások alakíthatók ki, például egy rasszista ork kereskedő, aki embereknek dupla áron értékesít.

A `b` árkategória használata szabadon választható, és nem kötelező. De ha azt szeretnéd, hogy egy kereskedő csak bizonyos
kategóriába tartozó tárgyakat adjon-vegyen, akkor egyszerűen csak a kívánt kategória esetén kell árat visszaadni. Például:
```
+-----------------------------+
| [+] [-]  b                  |
+---2---+------1------+---0---+
| a:=0  | a:=a*50/100 | a:=0  |
+-------+-------------+-------+
```

Van egy másik hasznos tulajdonsága is a kereskedőknek, a leltárjuk képes feltölteni magát. Általában a tárgyak egyszer adódnak
a leltárhoz, amikor az NJK megjelenik, de ezzel N percenként megismételhető ez a hozzáadás. De nem a végtelenségig, egy
kereskedőnek nem lehet egyfajta tárgyból több, mint 999999 darabja.
