<h1 tab12 betutipusok>Betűtípusok</h1>

Válaszd a `Kellékek` > `Betűtípusok` menüpontot. Meg fog jelenni az elérhető betűtípusok listája. Hogy újat adj hozzá, kattints
a <imgt ../img/add.png> "Hozzáad" gombra a jobb alsó sarokban. Egy fájlválasztó fog megjelenni.

A már beimportált betűtípusok szerkeszthetők az [sfnedit](https://gitlab.com/bztsrc/scalable-font2) programmal, vagy még
importálás előtt a [FontForge](https://fontforge.org/) programmal.

Bitmap és vektor betűtípusok
----------------------------

A fájlválasztó ablakban válassz ki egy ilyen fontot. A funkciósávban megadható a betöltendő tartomány, de általában csak úgy
kell hagyni, ahogy van. Ha kiválasztottad a font fájlt, kattints az <ui1>Importálás</ui1> gombra.

Pixel betűtípusok
-----------------

Képek is betöltehetők fontnak. Ebben az esetben nagyon fontos, hogy a tartomány meg legyen adva. Mind a mettől, mind a meddig
mező lehet egy UTF-8 karakter, vagy egy UNICODE kódpont "U+" előtaggal és legfeljebb hat hexa számmal.

A glifeket vízszintesen és függőlegesen is el lehet helyezni a képen. Ha a kép szélesebb, mint amilyen magas, akkor a betűtípus
magassága a kép magassága lesz, a kép szélessége pedig elosztásra kerül a tartomány méretével. Hasonlóan, ha a kép magasabb, mint
amilyen széles, akkor a kép szélessége lesz a betűtípus szélessége, és a magasság lesz elosztva a tartomány méretével.

Például, egy számokat tartalmazó pixel font kép esetén a kép lehet

```
0123456789
```

vagy

```
0
1
2
3
4
5
6
7
8
9
```

A tartomány mettőljébe `0`-t kell megadni (az első karakter a képen) és a meddig mezőjébe pedig `9`-et (az utolsó karakter a képen).
Használhattunk volna "U+30"-t és "U+39"-t is, ha UNICODE kódpontokban akartuk volna megadni. Ennek megadása nagyon fontos, mert
tartomány nélkül nem lehet tudni, hogy mi található a képen. Végezetül kattints az <ui1>Importálás</ui1> gombra.
