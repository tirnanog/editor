<h1 tab21 kepernyoadatok>Képernyőadatok</h1>

A képernyőadatok beállítása
---------------------------

Válaszd a `Felület` > `Képernyőadatok` menüpontot. Itt adható meg, hogy hogyan jelenjenek meg a képernyőadatok a játék közben.

Akárcsak a többi felület menüpont esetében, itt is van alul <imgt ../img/preview.png> "Előnézet" gomb.

Irányítás
---------

Az érintőképernyős eszközöknek kell egy navigációs kör és pár akció gomb a képernyőn. Ezek alapból a beégetett szprájtokat
használják, de ha szeretnéd, itt megadhatsz olyan szprájtokat, amik illenek a játékod hangulatához.

A navigációs kör balra lent, míg az akciógombok jobbra lent jelennek meg a játék közben.

Eszköztár
---------

Ez mutatja, hogy a karakter miket cipel az övén és a fő kézben.

Kiválasztható a háttérkép, és ha szükség van rá, akkor egy áttetsző előtérkép is.

A különböző számbeviteli mezők adják meg, hogy mennyi elem lehet az övön, mekkora egy elem szélessége, magassága, induló
pozíciója a háttérképen, és az eltartás az elemek között (ebben a sorrendben). Mivel valószínűleg a főkéz elemet vizuálisan
is el akarjuk szeparálni a többi elemtől, ezért ehhez külön megadható egy elemek és főkéz közötti eltartás.

Az eszköztár mindig a képernyő alján, középen jelenik meg.

Statusztár
----------

Itt jelenik meg a játékos státusza. Tipikusan életerő pontok, mana pontok, tapasztalati pontok és hasonlók.

A státuszt össze lehet vonni az eszköztár hátterével. Jellemzően gömb alakú kijelzők esetén hasznos. Amikor külön van,
akkor mindig a képernyő tetején, középen jelenik meg.

Az eszköztárhoz hasonlóan, megadható a háttérkép, és szükség esetén egy áttetsző előtérkép is.

Kijelzők
--------

Ebben a motorban nincs beégetett státusz, minden konfigurálható. Éppen ezért muszáj megadni, hogy mely változók jelenjenek meg
itt, és ehhez van a folyamatkijelző specifikációs lista.

A listában látható, hogy milyen kijelzők vannak már definiálva. Alatta bemeneti mezők vannak, amivel az új adható meg. Ha
kiválasztasz egyet a lsitából, akkor a <imgt ../img/erase.png> "Törlés" gomb aktívvá válik, és a kijelölt folyamatjelző törölhető.

A megadáshoz a következő mezők állnak rendelkezésre:

- irány: lehet balról-jobbra (hagyományos), lentről-felfelé (gömb típusú) vagy alfa csatorna (ideiglenes hatásokhoz, például
  mérgezés), és az ellenfél életereje (az ellenfél feje felett jelenik meg).
- pozíció: a státusztáron (vagy eszköztáron) pixelben megadva. Ha mind a két koordináta 0, akkor ideiglenes hatás kijelzőt definiál.
- érték attribútum: kiválasztja, hogy melyik attribútum tárolja a kijelző értékét.
- maximum attribútum: kiválasztja, hogy melyik attribútum tárolja a kijelző maximális értékét.
- kép: a kijelző szprájtja. Attól függően, hogy milyen irányú, csak a bal vagy alsó része fog megjelenni.
- háttérkép: opcionális, az ideiglenes hatások és az ellenfél életereje esetén hasznos, ha az irány nem az alfa csatorna.

Az "ellenfél" irány olyan, mint a balról-jobbra, és nem a státusztáron jelenik meg, hanem az ellenfél feje fölött. Ez adja meg,
hogy melyik szprájt jelenítse meg az életerejét.

Az ideiglenes hatások egy gyűjtőbe kerülnek, és a középen fent jelennek meg, függetlenül attól, hogy a státusztár összevont-e.
A pozíció megadásához van egy koordináta választó segéd, de ahhoz, hogy használni lehessen, előbb meg kell adni a státusztár
(vagy eszköztár) háttérképét, valamint a kijelző képét.

Ha minden mező kitöltésre került, akkor kattints a <imgt ../img/add.png> "Hozzáad" gombra, hogy az új folyamatkijelző a listához
adódjon.

Ha váletlenül rossz pozícióra raktad, akkor nem kell kitörölni és újra hozzáadni. Amikor egy kijelző ki van választva,
<mbl> bal egérkattintással állítható az irány és növelhető pozíció, <mbr> jobb egérkattintással pedig csökkenthető.
Nem túl intuitív, de legalább könnyedén mozgatható vele a kijelző, annélkül, hogy újra kéne definiálni.

Leltár
------

Azt mutatja, hogy milye van a játékosnak. Három tabja van: "Tárgyak", "Képességek" és "Küldetések". Itt az elsőt lehet
beállítani (a másik kettő az [objektumok] képesség funkció alatt, illetve a [küldetések] menüpont alatt állítható).

A leltár első sorában megadható a normál és a lenyomott ikon, hasonló méretdefiníciók és eltartások, mint az eszköztár esetén,
az elemszám betűtípusa, és végezetül három szprájt: a normál rekesz háttere, a kijelölt rekesz háttere és a felszerelési pontok
háttere.

Játék közben a leltár ikonja a jobb felső sarokban jelenik meg. Mindenképp 32 x 32 pixeles (ha nem, akkor automatikusan át
lesz méretezve).

Hasonlóan a karakteradatok eszköztárához, megadható egy rekesz mérete és pozíciója, hogy hol jelenjen meg. Itt nincs eltartás,
mert az az elem háttérképétől függ, és ezért a pozíció az elem háttérképén értendő. Két szprájtot kell megadni, a normált
és a kiválasztottat.

Az utolsó szprájt, a felszerelési pontok szprájtja legalább egy, de inkább több rekeszt tartalmaz. Ez jellemzően egy ember figura,
rekeszekkel, hogy hol mit visel. Az egyetlen kötelező rekesz a főkézé. A felszerelési pont rekeszének mérete azonos a leltár
rkeszével, de ha szeretnél hozzá hátteret, akkor azt a felszerelési pont szprájtjára kell rárajzolni (több meló a grafikusnak,
de nagyon szabadságot tesz lehetővé).

Felszerelési pontok és rekeszek
-------------------------------

A problémás rész az, hogy a TirNanoG motorba nincs bedrótozva, hány rekeszt használ a játék, ezért ezeket itt mind definiálni kell.
Amint korábban említettük, csak egy rekesz kötelező, a főkézé, ennek kell a legelsőnek lennie a listában (tipikusan a figura
jobb kezénél). A többi (mint törzs a páncélhoz, lábak a gatyához, fej a sisaknak stb.) azon múlik, hogy milyen játékot csinálunk
épp. A táblázat a szprájt alatt ezeket a rekeszeket és pozícióikat mutatja.

Egy rekeszt kiválasztva a <imgt ../img/erase.png> "Törlés" gomb aktívvá válik, és a rekesz törölhető lesz.

Miután megadtad a startpozíciót (relatív a felszerelési pontok hátteréhez képest) és a megjegyzést, kattints a
<imgt ../img/add.png> "Hozzáad" gombra, hogy a rekesz a listába kerüljön. Itt is működik az a trükk, hogy <mbl> bal egérkattintással
növelhetjük, <mbr> jobb egérkattintással pedig csökkenthetjük az értékeket, ezáltal átmozgathatjuk a rekeszt annélkül, hogy újra
kéne definiálnunk (használd az <imgt ../img/preview.png> "Előnézet" gombot, hogy lásd, jók-e a megadott koordináták). Akárcsak
az eszköztárnál, itt is elérhető a koordináta választó segéd.

A rekesz neve lényegtelen, és a játékos nem is fogja látni. Ez csupán egy megjegyzés a készítőnek, hogy könnyebben tudjon rekeszt
választani, amikor az objektumok [leltárban] oldalán megadja, hogy egy adott tárgy hova szerelehető fel.
