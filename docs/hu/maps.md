<h1 tab36 terkepek>Térképek</h1>

A térkép szerkesztő
-------------------

Válaszd a `Játék` > `Térképek` menüpontot. Balra, az entitásválasztóban látszanak a már felkonfigurált térképek. Duplán
kattintva egy sorra az adott térkép betöltésre kerül.

Az eszköztáron található a "Térkép meta infó" ikon, a rétegválasztó, az ecsetválasztó, egy speciális "radír" ecset választó,
és jobbra a térkép technikai neve, ami a szerkesztőn belül használatos. Az eszköztár többi ikonja attól függ, hogy épp melyik
réteg van kiválasztva, például a csempe réteg esetén van festés ikon, kijelölés ikon, másolás ikon stb.

A nagyítás-kicsinyítés ikonok a térkép alatt, a funkciósorban kaptak helyet.

HINT: Térkép duplikálásához előbb töltsd be, adj neki egy másik nevet, majd kattints az <ui1>Elment</ui1>-re.

Gyorsgombok
-----------

Van pár gyorsbillentyű és gombkattintás, ami mindig működik a térképen, függetlenül attól, hogy melyik réteg van kiválasztva:
- a <mbr> jobb egérgombot lenyomva tartva és az eger mozgatva mozgatható a térkép
- <kbd>⯇</kbd> balra, <kbd>⯈</kbd> jobbra, <kbd>⯅</kbd> fel és <kbd>⯆</kbd> le kurzorgombokkal szkrollozható a térkép
- <kbd>Ctrl</kbd> + <kbd>⯇</kbd> balra, <kbd>⯈</kbd> jobbra, <kbd>⯅</kbd> fel és <kbd>⯆</kbd> le gombok forgatják a térképet
- <kbd>-</kbd> mínusz távolít, <kbd>+</kbd> / <kbd>=</kbd> plusz és egyelő gomb pedig ránagyít a térképre
- az <mbw> egérgörgő szintén szkrollozza a térképet (vízszintesen is, ha az egered / tapipadod támogatja)
- <kbd>Ctrl</kbd> + <mbw> egérgörgő távolít illetve nagyít
- <kbd>Ctrl</kbd> + <kbd>Z</kbd> visszavonja a legutóbbi módosítást
- <kbd>Ctrl</kbd> + <kbd>Y</kbd> újra végrehajtja a visszavont módosítást
- <kbd>Ctrl</kbd> + <kbd>S</kbd> elmenti a térképet (csak ha van már technikai neve)

Térkép meta információk
-----------------------

Ha a <imgt ../img/info.png> "Térkép meta infó" ikonra kattintasz, akkor láthatod és szerkesztheted a térkép adatait.
- A cím a térkép játékon belüli, lefordítható neve.
- A napfény megvilágíthatja a térképet, megadhatod a nap hosszát percekben és a színpalettát egy szprájt segítségével. Bővebb infóért lásd a [szórt fény] fejezetet.
- Ugyancsak itt állítható a térkép háttérzenéje.
- Alatta találhatók a parallaxok. Több is lehet, és a térkép fölé vagy mögé is beállíthatod őket.

Az első és a harmadik érték állítja, hogy mennyit mozduljon el a parallax megadott időközönként az X és Y tengely mentén
(arra jó például, hogy hulló eső hatást keltsünk vele). A második és negyedik érték ellenben azt állítja, hogy mennyit mozduljon
el a parallax, amikor a játékos is elmozdul (ez meg arra jó, hogy mélységérzetet alakítsunk ki vele). Ezek kombinációja nagyon
király hatásokat tud eredményezni a parallaxokkal.

Térkép rétegei
--------------

Az infó ikon mellett található a <imgt ../img/layers.png> "Réteg kiválasztása" ikon. Erre kattintva előugrik a rétegválasztó.
Itt külön kattinthatsz a láthatóság (szem) ikonra, ami ki- bekapcsolja az egyes rétegeket, vagy duplán kattintva egy réteg
nevére kiválasztja az adott réteget szerkesztésre. Fontos, hogy az éppen kiválasztott réteg mindig látható, annak nem lehet
elvenni a láthatóságát. Az aktuális réteget állíthatod még a <kbd>PgUp</kbd> és <kbd>PgDn</kbd> billentyűkkel is.

Több réteg is a rendelkezésedre áll:

- "Talaj 1"-től "Talaj 4"-ig rétegek a játékos **alatt** helyezkednek el, és mivel több van belőlük, áttetsző csempéket is használhatsz.
- az "Objektumok" réteg ugyanabban a magasságban van, mint a játékos, és az itteni csempékhez betöltődik az [ütközésmaszk] is, ha van.
- az "NJK-k" és "Keltetők" szintén a játékossal egy magasságban vannak, és csak azért kerültek külön rétegre, mert egyedi szprájtokat lehet elhelyezni rájuk.
- "Tető 1"-től "Tető 4"-ig rétegek a játékos **felett** találhatók, és a "Tető 4" réteg eltűnik, amikor a játékos alatta tartózkodik.

Van még két további technikai réteg, amik nem jelennek meg a játékban:

- "Ütközés" egy nem módosítható réteg, ami az "Objektumok" réteg szprájtjai alapján automatikusan generálódik.
- az "Útvonalak" pedig irányított szakaszokból áll, és az NJK-k útvonalait tartalmazza.

Ecset kiválasztása
------------------

Attól függően, hogy melyik réteg az aktív, a szprájtválasztó más és más elemeket fog felkínálni. Az útvonal réteget leszámítva
minden rétegen a <imgt ../img/erase.png> "Radír" ikon egy speciális törlés elemet választ. Az útvonalak esetén ténylegesen
törli a kiválasztott útvonalat. Az ecsetválasztás gyorsbillentyűje a <kbd>Szóköz</kbd>. Az <kbd>E</kbd> pedig eltöröl, azaz
a radír módot választja ki.

HINT: A csempék elnevezése rendszert alkot, ami nagyban segíti a kiválasztást. További infóért lásd a [csempe elnevezés] fejezetet.
Kereshetsz a csempék címkéire, és ha van fordítás, akkor a lefordított címkékre is.

Pipetta
-------

Akárcsak a GIMP-ben, a <imgt ../img/pipette.png> pipetta lehetővé teszi, hogy felvegyél egy csempét vagy objektumot a térképről,
amivel aztán rajzolhatsz. A gyorsbillentyűje az <kbd>O</kbd>.

Festés
------

Az alapértelmezett szerszám a festés, az <imgt ../img/paint.png> ecset ikon, amivel a <mbl> bal egérgomb lenyomásával rajzolhatsz
a térképre. A gyorsbillentyűje a <kbd>P</kbd>.

Ha lenyomod a <kbd>Shift</kbd> gombot, akkor vonalakat húzhatsz, akárcsak a GIMP-ben.

NOTE: Ha van aktív kijelölés, akkor csak a kijelölt területet tudod módosítani.

Kitöltés
--------

A <imgt ../img/fill.png> kitöltés szerszám összefüggő területeket tölt ki egy adott csempével. A gyorsbillentyűje a <kbd>B</kbd>.

Amennyiben van aktív kijelölés, és <kbd>Shift</kbd> + <mbl> bal egérgombbal kattintasz, akkor a teljes kijelölést ki fogja tölteni,
függetlenül attól, hogy összefüggő területek vannak-e kijelölve.

Ha lenyomod az <kbd>AltGr</kbd> / <kbd>GUI</kbd> gombot, miközben <mbl> bal egérrel kattintasz, akkor a kattintott csempe összes
előfordulása a térképen lecserélődik az éppen kiválasztott csempére.

NOTE: Akárcsak a <imgt ../img/paint.png> festés esetén, ha van aktív kijelölés, akkor csakis a kijelölt rész módosul.

Kijelölések
-----------

Pontosan úgy működik, mint a GIMP-ben. Három eszközöd van hozzá:
- <imgt ../img/boxsel.png> téglalap kijelölés (gyorsgombja az <kbd>R</kbd>),
- <imgt ../img/freesel.png> szabad sokszög kijelölés (gyorsgombja az <kbd>F</kbd>) és
- <imgt ../img/fuzzysel.png> típus kijelölés (gyorsgombja az <kbd>U</kbd>).

Ha csak úgy kattintasz egyet a térképre a téglalap kijelöléssel, akkor az megszűnteti az összes korábbi kijelölést. Ha lenyomva
tartod a <mbl> bal egérgombot, és úgy mozgatod az egeret, akkor egy téglalapot fog kijelölni.

A sokszög kijelölés esetén először kattintasz egy pontra. Aztán további pontokra más koordinátákon, minden kattintás egy új vonalat
fog hozzáadni az előző pontból. Amikor végül megint egy olyan pontra kattintasz, aminek a térképkoordinátája egybeesik a legelső
pontéval, akkor a sokszög bezárul, és a területe kijelölődik.

Ha a típuskijelölővel kattintasz, akkor a kiválasztott csempe, és az összes vele értintkező szomszédja, ami ugyanaz a csempe,
ki fog jelölődni.Ha kattintáskor lenyomod az <kbd>AltGr</kbd> / <kbd>GUI</kbd> gombokat, akkor minden egyes azonos csempe
ki lesz jelölve, nemcsak a szomszédosak.

Alapesetben egy új kijelölés kitörli az előzőt, de akárcsak a GIMP-nél, itt is kombinálhatod őket.

- ha lenyomva tartod a <kbd>Shift</kbd> gombot, akkor az új kijelölés hozzáadódik a korábbiakhoz.
- ha a <kbd>Ctrl</kbd> gombot tartod lenyomva, akkor kimetszhetsz egy részt a korábbi kijelölésekből.
- <kbd>Ctrl</kbd> + <kbd>I</kbd> invertálja, azaz megfordítja a kijelölést.
- <kbd>Ctrl</kbd> + <kbd>A</kbd> pedig mindent kijelöl.

Nincs semmi megkötés, szabadon kombinálhatod ezeket a kijelölő eszközöket. Például, kiválaszthatsz egy téglalapot, aztán
átválthatsz sokszögre, lenyomhatod a <kbd>Ctrl</kbd>-t, és a sokszög kijelöléssel "kivehetsz" egy részt a téglalap közepéből.

Másolás-beillesztés
-------------------

A kijelölés nemcsak arra jó, hogy korlátozza, hol dolgozhatsz a festés és kitöltés szerszámokkal. Le is lehet másolni a kijelölt
területet a vágólapra (<kbd>Ctrl</kbd> + <kbd>C</kbd>), aztán beillesztheted (<kbd>Ctrl</kbd> + <kbd>V</kbd>) egy új helyre.
A kijelölt részt ki is vághatod a <kbd>Ctrl</kbd> + <kbd>X</kbd> kombinációval. Ezáltal könnyedén szerkesztheted a térkép akár
egy nagyobb részét is egyszerre.

HINT: Lehet másolni a rétegek között is. Például hogy egy egész területet átmozgassunk egy másik rétegre, jelöljük ki, üssük le
a <kbd>Ctrl</kbd> + <kbd>X</kbd>-et, váltsunk át a másik rétegre, majd ott üssük le a <kbd>Ctrl</kbd> + <kbd>V</kbd>-t.

Tükrözés
--------

A vágólapra helyezett térképrészletet tükrözheted függőlegesen (vertikálisan, <kbd>V</kbd>) vagy vízszintesen (horizontálisan,
<kbd>H</kbd>). Ezáltal csinálhatsz egy kijelölést, lemásolhatod, tükrözheted, majd lerakhatod, és így könnyedén változatosabb
térképet tudsz csinálni.

A tükrözés működik útvonalak esetén is, de ott az útvonal irányát fordítja meg.

Összeláncolás
-------------

Egy térkép mérete korlátozott (alapból 32 x 32). De nem kell aggódni, mert ezek a relatív kicsi térképek a végtelenségig
(na jó, hogy pontosak legyünk, pár millió térképig) összeláncolhatók egy nagy térképpé. Amikor eléggé elszkrollozol egy térképet
(vagy <mbr> jobb egérkattintással eléggé elhúzod), akkor az adott irány sarkában meg fog jelenni egy <imgt ../img/linkmap.png>
"Összekapcsolás" gomb, horgony ikonnal. Ezekre kattintva előjön a térképek listája, és kiválaszthatod, melyik másik térkép legyen
az adott irányban. Ezeket az összeláncolt térképeket automatikusan tölti be a lejátszó, ezért a játékos nem fogja tudni, hol
ér véget az egyik, és hol kezdődik a másik, az ő szemszögéből egy hatalmas nagy térképnek fog tűnni az egész.

A térképek összeláncolása nem kötelező. Csinálhatsz különálló térképeket, amik egyáltalán nincsenek láncolva, vagy kissebb
csoportokban vannak összeláncolva. Ezekre továbbra is el tud jutni a játékos az eseménykezelők "Karakter áthelyezése pontra"
parancsa segítségével. Tipikus példa, hogy van egy csomó összeláncolt térkép, amik a felszínt ábrázolják, és amin ha a játékos
hozzáér a lejárathoz, akkor átteleportálódik egy másik csapat összeláncolt térképre, a földalatti kazamatákba.

<h2 map_preview>Előnézet</h2>

A funkciósorban van a szokásos <imgt ../img/preview.png> "Előnézet" gomb. Ezzel a térképet a teljes egészében láthatod:
szomszédokkal, parallaxokkal, fényekkel stb.

Amikor végeztél a térképpel, ne felejtsd el lenyomni az <ui1>Elment</ui1> gombot.

Tiled kompatíbilitás
--------------------

A szerkesztő a térképeket TMX formátumban menti le, ami a Tiled térképszerkesztő program natív formátuma. Van egy kis bökkenő
azonban, a TirNanoG képes a csempéket atlaszokban kezelni, míg a Tiled nem, szóval mindenképp ki *kell* csomagolni az atlaszokat,
különben "kép nem található" hibákat fog dobni a Tiled. Bóvebb infókért lásd az [atlaszok kezelése] fejezetet. (A teljesség
kedvéért, ez egy 2015 óta ismert, mégis ezidáig kijavítatlan hiba a Tiled-ben.) A TirNanoG szerkesztő számára ez lényegtelen,
mivel az működik mind atlaszba csomagolt, mind egyedi szprájtokkal, nem számít (ti. nem használja a TMX-ben lévő URI-t, csak
simán átpasszolja a `name` attribútum értékét a szprájtmenedzserrének).
