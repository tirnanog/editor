Média
=====

Hang és videó importálása
-------------------------

Válaszd ki a `Kellékek` menüpontban a kívánt típust. Az oldalon, ami megjelenik, látni fogod az adott típusú média fájlokat.
Alul középen ugyancsak kiválaszthatod a típust. Új hozzáadáshoz kattints a <imgt ../img/add.png> "Hozzáad" gombra a jobb alsó
sarokban. A fájlválasztó ablak fog megjelenni.

<h3 tab15 music>Aláfestő zene</h3>

A fájlválasztóban válassz egy zene fájlt. Különféle formátumban lehet, .ogg, .mp3, .wav, .voc, .mod, .xm, .it stb.
(ha telepítve van a `libflac` vagy a `libopus` könyvtár, akkor azok az olyan formátumú fájlok is megjelennek). Miután
kiválasztottad a fájlt, kattints az <ui1>Importálás</ui1> gombra.

Minden zene automatikusan .ogg formátumúvá konvertálódik (sztereó vagy 5.1-es csatornákkal).

A zenekészítéshez javaslom a [SoundTracker](http://www.soundtracker.org) vagy a [MilkyTracker](https://milkytracker.org) programot
(Audacityvel is felveheted). Régi motorosoknak ott van a modernizált [FastTracker II](https://github.com/8bitbubsy/ft2-clone)
klón, ami fut Linuxon.

<h3 tab16 sounds>Hangeffektek</h3>

Akárcsak a zenék, de ezek mono csatornás .ogg-á lesznek konvertálva (a hangok térhatással kerülnek mixelésre, ezért mono
csatornásoknak kell lenniük.)

A hangok felvételéhez javaslom az [Audacity](https://www.audacityteam.org) programot.

<h3 tab17 speech>Beszéd</h3>

A szinkronszínészek beszédei hasonlóak a hangeffektekhez, mono csatornás .ogg-á lesznek konvertálva. Az összes különbség annyi,
hogy a fájlnevük egy ISO nyelvi kód utótagot kap, például `_en`.

<h3 tab18 movies>Filmek</h3>

Az .ogv Theora videók importálása mindig, mindenféle függőség nélkül elérhető. De ha telepítve van az [ffmpeg](https://ffmpeg.org),
akkor más formátumokat (.mp4, .mkv, .mpg, .avi, .flv stb.) is támogat, és ezek is meg fognak jelenni a fájlválasztó ablakban.

Linux alatt használd a disztród csomagkezelőjét, például `apt-get install ffmpeg`. Windowson töltsd le a legfrissebbet az
[ffmpeg.org](https://ffmpeg.org) oldalról (valószínűleg `ffmpeg-git-essentials` vagy valami hasonló a neve), és a letöltött
archívumból csomagold ki az `ffmpeg.exe` fájlt (és csakis azt) a `C:\Program Files\TirNanoG` mappába (vagy bárhova a `%PATH%`-ba).

Ne is próbálj meg egészestés filmeket importálni, csak rövid (1 - 2 perces) vágójeleneteket. A videó hangsávjaiban **sosem
lehet** beszéd, csak hangszeres zenei aláfestés vagy hangeffektek (ez azért van, mert különben a fordítás lehetetlen lenne). A
szerkesztő (és a lejátszó is) gondoskodik róla, hogy a lefordított beszédet tartalmazó hangsávot rávágja a videóra.

A videók létrehozásához használható az [OpenShot](https://www.openshot.org) vagy a [ShotCut](https://shotcut.org) program.
Ugyancsak kigenerálható 3D-s modellekből a videó a [Blender](https://www.blender.org) program segítségével. Ezen programok
mindegyike támogatja a Theora formátumba mentést.

NOTE: Kézzel is átkonvertálhatod a videót Theorába: <pre>$ ffmpeg -i movie.avi -q:v 5 -r 30 -q:a 3 -ar 44100 -ac 2 movie.ogv</pre>
Egyébként ez pont az a parancs, amit a szerkesztő is meghív a háttérben.
