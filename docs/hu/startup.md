<h1 tab26 indito>Indulás</h1>

Indító szkriptek megadása
-------------------------

Válaszd a `Játék` > `Indulás` menüpontot. Ezen az oldalon több eseménykezelő is van, mind akkor indul el, amikor a játék
(pontosabban amikor a játékos az "Új játék"-ra kattint a [főmenü]ben). A főoldali szkript csak egyszer fut le, míg a többi
adott időközönként újra és újra.

Eseménykezelők használata
-------------------------

A többi leírással ellentétben ez a fejezet nem kifejezetten egy menüpontról szól. Ezek az eseménykezelők több helyen is
előfordulnak. A legjellenmzőbb az `Indulás` menü, de ez a leírás ugyanúgy érvényes a többi oldalra is.

Általánosságban az eseménykezelők parancsok sorozata, amik egyszerű játéklogikát írnak le. Bármit rakhatsz ezekbe, és mivel
van szekvencia, elágazás és ciklus is, bármilyen algoritmus leírására alkalmasak. Minden eseménykezelő esetében

- balra, a nagyobb rész az eseménykezelő szerkesztő mezője. A parancsokat áthúzással átrendezheted bennük.
- jobbra pedig van egy parancspaletta. Ikonokat innen a szerkesztőmezőbe húzva lehet új parancsokat hozzáadni.

Parancs törléséhez csak el kell távolítani az eseménykezelőből, azaz kihúzni a mezőből.

Vezérlés
--------

### Szekvencia

Nagyon egyszerű, csak az egyik parancsot a másik után kell rakni. A végrehajtás fentről lefelé történik.

### Elágazás

<imgt ../img/cond.png> A szokásos "if" és "switch" keveréke. Van egy "+" és egy "-" ikon, amivel újabb ágakat lehet hozzá
adni vagy eltávolítani. Alapból két ág van, 1 és 0 cimkékkel. Ha a megadott kifejezés értéke 1, akkor a baloldali ág fut le,
ha 0, akkor meg a jobboldali. Összetettebb kifejezések is használhatók ha több ág van hozzáadva.

### Ciklus

<imgt ../img/loop.png> Csak egyfajta van, ami a "while" a programozási nyelvekben. A blokk addig ismétlődik, amíg a kifejezés
értéke 0 nem lesz.

### Értékadás

<imgt ../img/let.png> Ezzel egy kifejezés eredményét lehet értékül adni helyi változónak vagy attribútumnak.

Változók
--------

Bármelyik attribútum használható változóként, csak a nevére kell hivatkozni. Ha nincs a név előtt előtag, akkor annak az
entitásnak az attribútumára vonatkozik, ami előidézte az eseményt. Például egy tárgy "Hozzáéréskor" eseménykezelőjében
az adott tárgy attribútumára. Ha van megadva `@` előtag, akkor az esemény másik részvevőjének attribútumára (ami jellemzően
a játékosé, de például az alapértelmezett cselekvések eseménykezelői esetén a másik fél a megtámadott NJK).

Minden eseménykezelőnek vannak helyi változói. Ezek egyetlen betűből állnak, `a`-tól `z`-ig. Mivel helyiek, ezért minden
eseménykezelőnek saját példánya van belőlük, és szabadon módosíthatják, annélkül, hogy egy másik eseménykezelő változóinak
bezavarnának. Ez alól egyetlen kivétel van, a "Másik objektum kattintás eseménykezelőjének futtatása" parancs. Itt a hívó
helyi változói átmásolódnak a hívott változóiba, és amikor a meghívott eseménykezelő végez, az `a` változója értéke
visszamásolódik a hívó `a` helyi változójába.

Általánosságban a funkciók (mint választók vagy dialógusok) mindig az `a` változóban adják vissza az értéküket.

Parancsok
---------

<imgt ../img/func.png> Ezek sokmindenre használhatók. Hangokat adhatnak ki, lecserélhetik a háttérzenét, animációt
játszhatnak le stb. Van parancs a térkép módosítására, vagy hogy tárgyakat adjunk a játékosnak (vagy épp elvegyünk).
Általánosságban a játék logikája ezen parancsok által van leírva, semmi sincs beleégetve a motorba. Ez tökéletes
szabadságot biztosít a játékkészítők számára, ugyanakkor azt is jelenti, hogy könnyű elszúrni, mert a parancsok csak
szintaktikailag vannak ellenőrizve, szemantikai ellenőrzés nincs (és nem is lehet).

Több infó arról, hogy hogyan tárolódnak az eseménykezelők szövegesen, a [TirNanoG Projekt](https://codeberg.org/tirnanog/editor/src/branch/main/docs/proj_format.md)
dokumentációjában található.
