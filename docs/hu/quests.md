<h1 tab37 kuldetesek>Küldetések</h1>

Küldetések szerkesztése
-----------------------

Válaszd a `Játék` > `Küldetések` menüpontot. Balra, az entitásválasztóban látszanak a már felkonfigurált küldetések.
Duplán kattintva egy sorra az adott küldetés betöltésre kerül, vagy csak megadhatsz egy újat.

Az eszköztáron van egy pipa, amivel egyedivé tehető a küldetés, és jobbra pedig a belsős technikai neve, amit a szerkesztő
használ.

A funkciósávban megtalálhatók a szokásos gombok: a <imgt ../img/del.png> "Törlés" gomb törli a kijelölt küldetést.
A <imgt ../img/erase.png> "Törlés" gomb alul középen pedig csak az űrlapot üresíti ki. Van továbbá a megszokott
<imgt ../img/preview.png> "Előnézet" gomb is.

A küldetésértesítők a `Felület` > `Értesítők` menüpont alatt állíthatók, bővebben lásd az [értesítők] fejezetben.

Küldetés hozzáadása
-------------------

A fő szerkesztési területen látni fogod a küldetés részleteit. Van lefordítható cím, ami megjelenik a játékban is a [leltár]
"Küldetések" fülén.

A leírás lehet hosszabb, de nem lehet benne újsor karakter, a tördelés automatikusan történik, mivel minden lejátszó felbontása
más-más lehet, és emiatt a leírást tartalmazó dialógusablak mérete is változó. Másrészről lehet benne `{}`, ami a játékos nevére
cserélődik le, illetve `{attribútum}` hivatkozás, ami meg az adott attribútum értékére.

Egy küldetés lehet helyi vagy globális. A helyi küldetést minden játékos elvégezheti, egymástól függetlenül. A globális
küldetés azonban csak egyszer teljesíthető, és csakis egyetlen játékos által.

Az űrlap alatt található a "teljesítéskor" eseménykezelő. Az itt megadott parancsok akkor futnak le, amikor a küldetést sikeresen
teljesíteték. Megjutalmazhatod a játékost és tárgyakat adhatsz neki, megváltoztathatsz elemeket a térképen, vagy akár lejátszhatsz
egy küldetésspecifikus hangot (csak akkor, ha nem állítottál be általános értesítő hangot az [értesítők] oldalon), megjeleníthetsz
egy dialógust, stb.

Küldetések előnézete
--------------------

Alul a funkciósávban található az <imgt ../img/preview.png> "Előnézet" gomb, amivel megnézheted, hogy mutat a leírás és a
küldetés a leltárban. Ha nem tetszik a dialógusablak betűtípusa, akkor azt beállíthatod a `Felület` > `Elemek` menüpontban,
lásd [UI Elemek].

Amikor végeztél a küldetés megadásával, ne felejts el az <ui1>Elment</ui1> gombra kattintani.
