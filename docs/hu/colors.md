Színek
======

INFO: Dologok, amiket nem lehet elégszer ismételni
* Ne használj sima egyforma színeket!
* Használj változatos "hue" értékeket a színátmeneteidnél!
* A kiemelt részek sárgásak, az árnyékban lévők lilásak.
* Vázold fel a művedet előbb, árnyékokkal együtt, mielőtt a részleteken kezdenél dolgozni.

Árnyalatok
----------

Nagyjából "rajzfilmszerű" megjelenés, közepes vagy kevés textúrával.
Kellenek a részletek, de nem túl sok, semmiképp sem annyira sok, ami
már nehézzé teszi az összedolgozást. Nem jó az sem, ha "zavaros".
De a részletek jók. A textúrának rajzfilmszerűnek kell lennie; ez
közepesen sok vagy kifejezetten kevés átmenettel érhető el.
A részletekkel csínján kell bánni, ezért úgy használd, hogy számítson!

<imgc ../img/shades.png><fig>Árnyalatok hozzáadásának menete</fig>

Blokkokban árnyalj. Először rajzold meg a tárgyat, odafigyelve a méretére.
A vonalakat később add csak hozzá, és jellemzően csakis a széleknél és
fontos részleteknél. A részleteket elsősorban az adott szín árnyalataival
kell kifejezni, és nem körvonalakkal.

Megvilágítás
------------

A fénynek elsősorban felülről kell érkeznie. Ha az oldalsó irány fontos,
akkor egy kicsit balról, de nem nagyon: igyekezz középen tartani.

<imgc ../img/sun.png><fig>A napsugarak iránya (sárga nyilak)</fig>

Egy példa szcenárió Blenderben, ahol a napsugarak a kellő szögben esnek be:

<imgw ../img/lighting.png><fig>Bal oldalon a kamera "fentről-le", ahogy itt használjuk;
jobb oldalon az oldalnézet.  A sárga "nap" sugarakkal a fényforrás,
a nagy narancssárga háromszög a kamera.</fig>

A világosból sötétbe színátmenetekben *soha* ne használj egyetlen "hue" értéket.
Változtasd a "hue" és szaturáció értékét egy kicsit, ahogy a világosból haladsz a
sötét felé, különben a tárgyaid túl laposnak fognak hatni.

Árnyékok
--------

Hogy árnyékokat vagy kevésbé megvilágított felületeket alkoss, használd
az alapszínt és mozdísd el a "hue" értékét (a HSV színválasztón) kissé
a lila felé. Hogy egy kiemelő színt kapj, csináld ugyanezt, de a legközelebbi
sárga felé mozdítsd el kicsit, és növeld meg a "value" értékét. Addig tologasd
a csúszkákat, amíg "jól" nem néz ki.

Belül az összszín hűvösebb árnyalatait használd, kevesebb konktraszttal.
Ez duplán is érvényes az olyan dolgokra, mint alagsor, barlangok és egyéb
földalatti területek.

Minden vetett árnyék színe #322125 kell legyen, 60% átlátszósággal (alfa).
Ha lehetséges, akkor csináld meg a két csempe kombinált változatát is,
hogy csak egy rétegre legyen szükség, például rakd a házat füves háttérre.

Ditheringet nagyon keveset, vagy inkább egyáltalán ne használj. Az alap
készletben egyik szprájton se lett használva.

Körvonalak
----------

A körvonalaknak az aktuális szín sötétebb változatának kell lenniük, vagy
úgy általában egy sötét színnek, de nem feketének. (Különleges esetekben persze
lehet ez alól kivétel.)

<imgc ../img/rock.png><fig>Figyeld meg, hogy a körvonal az alapszín sötétebb árnyalata</fig>

Tereptárgyak és objektumok
--------------------------

A tereptárgyaknak színesnek kell lenniük, hogy ne olvadjanak be a környező háttérbe
(változtasd a színt, fényerőt, szaturációt, hogy kontrasztosabb legyen).

Hsználj nagy fényerőkülönbséget az oldalak és a teteje között. Nézd meg például ezeket:

<imgc ../img/barrel_and_bucket.png><fig>Tárgy fényerejére példák hordó és vödör esetén</fig>

A tereptárgyaknak mindig árnyékot kell vetniük, különben nem fog úgy tűnni, hogy részei
a jelenetnek. Az árnyékokra itt is ugyanaz érvényes, mint amit a fentebbi "megvilágítás"
és "árnyékok" fejezetben leírtunk.

Karakterek színei
-----------------

A karaktereknek külön színpalettájuk kell legyen, hogy elkülönüljenek a háttértől. A vetett
árnyékokra ugyanaz vonatkozik, mint a csempékre, #322125 kell legyen 60% átlátszósággal.

NOTE: A szprájt generáló képes a karakterek dinamikus [átszínezés]ére.

Paletták
--------

Nincs konkrétan előírt, kötelező paletta a TirNanoG Alap készletben, de a legjobb az, ha
olyan színeket próbálsz használni, amik már szerepelnek az alap csomagban.
