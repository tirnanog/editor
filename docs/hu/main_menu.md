<h1 tab20 fomenu>Főmenü</h1>

A főmenü testre szabása
-----------------------

Válaszd a `Felület` > `Főmenü` menüpontot. Ez egy egyetlen űrlap az összes opcióval.

Akárcsak a többi felület menüpont esetében, itt is van alul <imgt ../img/preview.png> "Előnézet" gomb, de itt kettő is:
egy magához a főmenühöz, és egy a karaktergeneráló ablakhoz, ami akkor jelenik meg, amikor a játékos az "Új játék" opciót
választja.

Bevezető
--------

Megadhatsz egy átvezetőt, ami akkor jelenik meg, mielőtt a főmenü legelőször bejönne. Tipikusan egy olyan átvezetőt szokás
ide választani, amin a csapat csecse animált logója látható.

Előfordulhat, hogy egy játékhoz egyáltalán nem kell főmenü. Ebben az esetben ki kell venni a pipát a "főmenü engedélyezve"
mellől.

Háttér
------

Kiválaszthatod a háttérzenét, háttérfilmet, vagy háttérképet 3 parallaxal. A zene működik film és kép esetén is, bár a film
is tartalmazhat saját hangsávot.

Cím
---

Két lehetőséged is van. Vagy fejlécképet használsz szprájttal és király grafikával a játék címével, vagy megjelenítheted a
címet a megadott betűtípussal. Lehet mindkettő is, ha például ez egy folytatás, akkor érdemes meghagyni a fejlécképet az előző
részből (fontos, hogy a játékosok felismerjék), és a szöveges cím pedig lehet a fejezet címe.

NOTE: A szöveges cím lefordítható a játékos nyelvére, de a fejléckép nem.

Gombok
------

Ezek azok a gombok, amikkel a főmenü opciói megjelennek. Használhattam volna a sima gombokat is, de úgy voltam vele, hogy
a játékkészítők valószínáleg szeretnék, ha a főmenü kitűnne. Akárcsak a felület elemei esetében, megadható a gombok bal,
közép és jobb szprájtja, valamint betűtípusa, mérete, színe stb.

Új játék
--------

Valószínűleg nem a legjobb hely, de amikor a játkos az "Új játék" opciót választja, be kell írni a játékos nevét, és a
tulajdonsággráf is ekkor jelenik meg. Ez az egyetlen hely a játék során, ahol szövegbeviteli mező van, és a gráf is csak
itt bukkan fel, ezért ide raktam a konfigurációjukat.

Kilépés
-------

Annak a leírása, hogy hogyan kell beállítani, mi jelenjen meg, ha a "Kilépés" opciót választja a játékos, a [stáblista]
fejezetben található.
