<h1 tab13 szprajtok>Szprájtok</h1>

HINT: Ha nem akarsz egyedi szprájtokat betölteni, akkor használhatod a szprájt [generáló]t is.

<h2 tab0 szprajtok_importalasa_keprol>Szprájtok importálása képről</h2>

Válaszd a `Kellékek` > `Szprájtok` menüpontot. Látni fogod a rendelkezésre álló szprájtok listáját. Alul, középen található a
kategória válsztó, amivel a szprájtokat szűrheted. A kategóriák a következők:
- <imgt ../img/sprcat0.png> felhasználói felület szprájtjai
- <imgt ../img/sprcat1.png> térképelem és térképen fekvő objektumok szprájtjai
- <imgt ../img/sprcat2.png> karakter és felszerelt objektumok szprájtjai
- <imgt ../img/sprcat3.png> dialógusablak portré szprájtjai
- <imgt ../img/sprcat4.png> teljesképernyős háttérképek

Itt válaszhatsz kategóriát, de később is megadható, mielőtt a tényleges importálásra sor kerülne. Kattints a <imgt ../img/add.png>
"Hozzáad" gombra a jobb alsó sarokban.

A képernyő meg fog változni, és a szprájtlap specifikációs ablak fog megjelenni. Itt látható egy eszköztár felül, aminek az
ikonjai a következők:

- kép betöltése szprájtlapként
- paletta cseréje
- átméretezés felére
- átméretezés duplájára
- függőleges tükrözés
- vízszintes tükrözés
- rács megadása (egy pipa és több numerikus mező)
- elrendezés (egy kombódoboz és egy mentés ikon)
- szprájt neve
- szprájt törlése az import listából

Az eszköztár alatt látható a szprájtlap balra (a nagy terület) és a kijelölt import lista jobbra.

Egy tipikus munkamenet
----------------------

<imgw ../img/sprimg.png>

1. Kép betöltése.
2. Az elrendezés legördülőből a kívánt elrendezés kiválasztása.
3. Amikor az elrendezést kiválasztottuk, az import lista automatikusan feltöltésre került animált szprájtokkal.
4. Az import lista előnézet megfelelőnek tűnik, kattints az <ui1>Importálás</ui1> gombra, és a szprájtok hozzáadódnak a projekthez.

Szprájtlap betöltése
--------------------

Kattints a <imgt ../img/load.png> "Szprájtlap betöltése" ikonra a bal felső sarokban (az eszköztár legelső ikonja, de a funkciósávban
is megtalálható a könnyebb elérés kedvéért).

A szrájtlap egy olyan kép, ami több szprájtot tartalmaz. Ezek lehetnek független szprájtok, vagy ugyanannak az animált szprájtnak
a képkockái is. Betölthetsz 3D-s modelleket is, és azokból is generálhastsz szprájtokat, lásd [modellek].

Paletta konverziók
------------------

Sok dezánjer szereti limitált színekkel elkészíteni a játékot, hogy egységesebb legyen a kinézet. Hogy ebben segítsen, a
szerkesztő lehetővé teszi, hogy minden betöltött kép egy adott palettát használjon csak.

Palettát sokféle általános formátumból be lehet tölteni (.gpl, .ase, .aco, .pal stb.) és képekről is. Képek esetén a legjobb
eredmény eléréséért érdemes indexált képet használni (gif vagy kvantált png), vagy egy olyan true-color képet, amint nincs
több, mint 256 különböző szín. A mérete és pixelek száma nem számít, csak az, hogy hányféle különböző pixel van a képen.

A képen lévő szín és a paletta színének összehasonlítására két lehetőség is adott: vagy sRGB távolság (gyors, általában jó
eredményt produkál, de néhány határesetnél téveszt) vagy CIE76 (lassú, átkonvertálja mind a képet, mind a palettát CIE LAB
színtérre először, aztán ott számolja ki a távolságot).

Ha nem PNG képeket használsz, akkor néhány obskurus formátum esetén előfordulhat, hogy a színcsatornák fel vannak cserélve.
Ez helyrerakható a <imgt ../img/swaprgb.png> "Kék és piros csatornák felcserélése" gombbal. Nincs rá szükség, csak biztos,
ami biztos elérhető.

Alap konverziók
---------------

Át tudod méretezni a képet felére vagy duplájára, de nem akármekkorára. Eleve a megfelelő mérettel kell betölteni a képeket,
de előfordulhat, hogy a játéknak 32 x 32 pixeles szprájtok kellenek, de csak 64 x 64 pixeles képek vannak. Ebben az esetben,
hogy ne kelljen egy harmadik fél programját használni, gyorsan át lehet méretezni. Mind a kicsinyítés, mind a nagyítás bilineáris
interpolációt használ, ezért a végeredménynek tűrhetőnek kell lennie. Mindazonáltal tartsd észben, hogy a nagyítás mindig
minőségromlással jár, ezért amikor csak lehet, kerülendő.

Portré esetén a kép megjelenhet a dialógusablak bal (arc jobbra néz) vagy jobb (arc balra néz) oldalán is. A szerkesztő és a
lejátszó automatikusan elvégzi ezt a konverziót, ezért minden portrét úgy kell importálni, hogy az *arc balra néz*. Előfordulhat,
hogy csak olyan képed van, amint a portré rossz irányba néz. Hogy ne kelljen egy másik alkalmazást betölteni, egy kattintással át
lehet tükrözni a képet.

Némelyik obskurus képformátum fejjel lefelé tárolja a képet. A képbetöltő gondoskodik erről, de ha mégsem, akkor szintén egy
kattintással függőlegesen tükrözhető a betöltött kép. Általában nincs rá szükség, csak a biztonság kedvéért.

Ezek a funkciók csak gyorsjavításra valók, a szerkesztő nem alkalmas (és nem is volt tervezve) egy szofisztikált képszerkesztő
kiváltására.

Szprájtok megadása
------------------

A következő ikon az eszköztáron a rács megadása. Kiveheted a pipát, és ekkor az egész kép egyben lesz betöltve, vagy bekapcsolatod,
és akkor ráccsal feldarabolt szprájtokat tudsz megadni. Eléggé magától értetődő, játsz a magasság, szélesség, margó és eltartás
értékekkel, és egyből látni fogod, mik a kijelölésre alkamas területek a képen.

Ha a kép nem férne be a mezőbe, akkor a <mbr> jobbklikk lenyomva tartásával mozgathatod.

### Egyedi szprájtok

Jobbra látható a kijelölt szprájtok listája. Ha még nem kattintottás egyikre sem, vagy a legfelső "Új szprájt" ikon van kijelölve,
akkor minden esetben, ha a képen kijelölsz <mbl> balkattintással, egy úgy szprájt kerül a listába. Ez ideális például csempék
kiválasztására egy nagy csempekészletből.

### Animációk

Ahhoz, hogy animációt jelölj ki, először is ki kell választani az első képkockát az "Új szprájt" funkcióval. Aztán ki kell
választani a jobb oldali listából azt a szprájtot, amihez képkockákat akarsz hozzáadni (használhatod a <mbw> görgőt is). Ez
így bonyolultnak tűnhet, de igazából nem az: a képkockák mindig ahhoz a szprájthoz adódnak, ami épp ki van jelölve jobbra.

HINT: Miután hozzáadtad az első képkockát, nyomd le a <kbd>⯆</kbd> lefelé kurzor billentyűt, majd kattints a további képkockákra.
Amikor végeztél, nyomd le a <kbd>⯅</kbd> felfelé billentyűt, hogy visszaállj új szprájt módba.

Amikor egy szprájt ki van jobbra választva, akkor animációként fogod látni. Beállíthatod az animáció típusát (egyszeri, oda-vissza,
körkörös lejátszás) és az irányát (ami a későbbiekben segítségedre lesz a szűréssel, mivel kevesebb szprájtból kell majd
választanod). Az egyhelyben történő animációknál "dél" irányt érdemes megadni, egyébként 8 irányból választhatsz. Nem kell minden
irányhoz szprájtot megadni, lehetséges, hogy egy játék csak 4 irányt használ például.

### Nagyméretű szprájtok

Nem minden szprájtnak egyforma a mérete, néhány nagyobb, mint a többi. Ehhez ki lehet jelölni több szomszédos rácsmezőt, csak
nyomd le a <mbl> bal egérgombot, hűzd el az egeret, majd ha elégedett vagy a kijelölt területtel, engedd el az egérgombot.
Animációk esetében minden képkockának ugyanakkora méretűnek kell lennie.

### Törlés

Ha véletlenül rossz szprájtot adtál hozzá a jobboldali listához, akkor jelöld ki, majd kattints a <imgt ../img/erase.png>
"Törlés" ikonra a lista fölött, ez törölni fogja a szprájtot minden képkockájával együtt. Ha lenyomod a <kbd> ⇦ </kbd> Backspace
billentyűt, akkor csak a legutolsó képkocka kerül törlésre, és a szprájt csak akkor tűnik el, ha már nem maradt egyetlen
képkockája sem.

Elrendezések
------------

A rács megadása és a kijelölés eléggé fáradalmas, időigényes és monoton tud lenni. Hogy ezt megkönnyítse, a szerkesztő lehetőséget
ad az elrendezés lementésére: az eszköztáron, a rácsmegadástól jobbra megadhatod az elrendezés nevét, majd kattinthatsz az
<imgt ../img/save.png> "Elment" ikonra.

Legközelebb, amikor betöltesz a szprájtlap képet, az elrendezés kombóboxra kattintva, és a felugró listából kiválasztva
betöltheted az elrendezést. Ezután minden rácsbeállítás és szprájtképkocka kijelölés végrehajtódik az új szprájtlapon, és a
kijelölt szprájtlista automatikusan megjelenik a jobboldali listában.

Ennél is jobb, hogy amikor egy elrendezés végrehajtódik, és a kép jobb alsó sarkában található egy 8 x 16 pixeles paletta,
akkor dinamikus [átszinezés]re kerül sor. Ezáltal számtalan szprájt variációt betölthetsz ugyanarról a szprájtlapról.
Válaszható a "?" (rákérdez) színvariáns is, ekkor a paletta is külön importálásra kerül, és megadhatod a [karakter] opciónál.
Ebben az esetben a játékos a karaktergeneráláskor tud majd magának színt választani.

Megadható az is, hogy a szprájtlap mely részére legyen érvényes az elrendezés. Ha lenyomod a <kbd>Shift</kbd> gombot, és új
jelölsz ki a <mbl> bal egérgombbal, akkor a kijeölésen kívüli részek pirosas árnyalatot kapnak. Ezután amikor elrendezést
választasz a kombóboxból, ezek a piros részek ki lesznek hagyva. Ez akkor nagyon hasznos, ha például több tereptípus is szerepel
egy atlaszon, mind a saját wang halmazával, ezért külön értelmezendő elrendezéssel. Kijelölöd az első tereptípust, majd
kiválasztod az elrendezést és importálod a szprájtokat. Ezután kijelölöd a második tereptípust, majd megint kiválasztod az
elrendezést és importálsz, és így tovább. Nem szükséges egy külön programmal feldarabolni az atlaszt külön tereptípus képekre.

A tényleges importálás
----------------------

Ha elégedett vagy a kijelölt szprájtok listájával jobboldalt, nevet kell adni a szprájtgyűjteménynek a lista fölött. Név nélküli
szprájtok "_xxx" utótagot kapnak, ahol az "xxx" egy szigorűan monoton növekedő sorszám, 001-től 999-ig.

Szóval megvan a kijelölt lista, adtál nevet a listának (a lista fölötti mezőben), valamint az egyes szprájtoknak is (minden egyes
listaelem fölött), leellenőrizted, hogy megfelelő a kiválasztott kategória (ikonok alul középen), akkor rányomhatsz az
<ui1>Importálás</ui1> gombra. Ezt követően visszamehetsz a szprájtok oldalra, vagy akár betölthetsz egy újabb szprájtlapot
és további szprájtokat importálhatsz.

Atlaszok kezelése
-----------------

Alapesetben minden szprájtot külön PNG fájlba ment a szerkesztő (amiben a képkockák vízszintesen vannak elrendezve), ami nagyon
jó kompatíbilitási okoból, ha rendezni, másolni, vagy netán harmadik fél programjával szerkeszteni akarod őket. Azonban a sok
kis kép jelentősen lelassítja a projekt betöltését. Hogy ezt elkerülje, és a projektbetöltés gyors maradjon, a szerkesztő
támogatja a szprájt atlaszokat. Ezeket nemcsak betölteni tudja, hanem ki- és becsomagolni is, nincs szükség másik programra.

Az atlasz becsomagolása, a `-p` beolvas minden PNG fájlt az aktuális könyvtárból, ami `(atlas)` sztringgel kezdődik, és
elmenti őket egyetlen `(atlas)_atls.png` képbe. Például, tegyük fel, hogy a következő képeid vannak: `sword_walk_soo6.png`,
`sword_walk_weo6.png`, `sword_walk_noo6.png`, `sword_walk_eao6.png`, `sword_slash_soo6.png`, `sword_slash_weo6.png`,
`sword_slash_noo6.png` és `sword_slash_eao6.png`, ekkor a
```
tnge -p sword
```
parancs ezt mind becsomagolja egy `sword_atls.png` fájlba. Az atlasz adatait a PNG megjegyzés mezőjében tárolja.

A kicsomagolás, a `-u` ennek az elekezője, beolvassa a `(atlas)_atls.png` fájlt, aztán lementi a több különálló PNG fájlt.
Például
```
tnge -u sword
```
beolvassa a `sword_atls.png` fájlt és lementi a korábban felsorolt összes PNG fájlt.

Ha csak arra vagy kíváncsi, hogy mely szprájtokat tárol egy atlasz, akkor nem kell kicsomagolni, elég a
```
tnge -l sword
```
parancsot futtatni a szprájtok kilistázásához.

WARNING: Sose módosítsd az atlasz képeket egy másik programmal, mert az atlasz meta adatok elveszhetnek.

WARNING: Ámbár a TirNanoG Szerkesztő elmenti az atlaszhivatkozást a [térképek]be, a Tiled nem támogatja még ezt, ezért
egyelőre ki van kommentezve. Ez azt jelenti, hogy ki *kell* csomagolnod minden atlaszt, muszáj, ha tényleg Tiled-be is be
akarod tölteni a térképeket.
