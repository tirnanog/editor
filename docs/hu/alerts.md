<h1 tab22 ertesítok>Értesítők</h1>

Értesítők beállítása
--------------------

Válaszd a `Felület` > `Értesítők` menüpontot. Itt a felugró üzenetek megjelenését adhatod meg.

Akárcsak a többi felület menüpont esetében, itt is van alul <imgt ../img/preview.png> "Előnézet" gomb.

Betűtípus
---------

Melyik betűtípust használja.

Időtartam
---------

Megadható (törtmásodpercben), hogy mennyi ideig jelenjen meg az értesítő. Csúszkákkal ugyancsak megadható, hogy mennyi idő alatt
árnyaljon be, illetve ki a felirat.

Üzenetek
--------

A lefordítható üzenetszövegek és a hanghatások. Jelenleg csak a [küldetések]hez vannak értesítők.
