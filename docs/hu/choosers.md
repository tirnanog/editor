<h1 tab24 esetvalasztok>Esetválasztók</h1>

Útvonal- és esetválasztók megadása
----------------------------------

Válaszd a `Felület` > `Esetválasztók` menüpontot. Az esetválasztók olyan konstrukciók, amik ikonokkal opciólistát jelenítenek
meg, és befolyásolni tudják a játék állapotát. Tipikusan játék útvonalválasztásra használhatók, de másra is jók lehetnek.

Akárcsak a többi felület menüpont esetében, itt is van alul <imgt ../img/preview.png> "Előnézet" gomb.

Esetválasztó lista
------------------

Balra, az entitáslistában látható a már felkonfigurált esetválasztók listája. Duplán kattintva egy sorra az adott választó
betöltésre kerül.

A bal alsó sarokban a <imgt ../img/del.png> "Törlés" gombra kattintva a kijelölt esetválasztó törlésre kerül. Az alul, középen
lévő <imgt ../img/erase.png> "Törlés" gomb másrészről csak az űrlapot üresíti ki.

Új hozzáadásához adj meg egy nevet és kattints a jobb alsó sarokban az <ui1>Elment</ui1> gombra.

Választó eszköztár
------------------

Felül, az eszköztárban látható a szokásos margó és eltartás beállítás.

Jobbra van a választó belső techinkai neve.

Választó opciói
---------------

Az eszköztár alatt található a választó opcióinak listája. Ennek 4 oszlopa van: a pozíció, az ikonok, egy elvárt mező és egy
beállítandó mező. Jobbra van továbbá még két nyíllal jelölt gomb, amivel a sorrendet lehet befolyásolni, ha szükséges.
<imgc ../img/chooser0.png>

Pozíció
-------

Több gyűjtő is a rendelkezésre áll, attól függően, hogy hova akarjuk pozicionálni az ikonokat, és mindbe több ikon is betölthető.
Tipikusan van egy gyűjtő középre vízszintesen, egy balra függőlegesen, egy fent középre vízszintesen, fent jobbra függőlegesen,
stb.
<imgc ../img/chooser1.png>

Az utolsó nem egy gyűjtő, hanem a háttérkép elemeinek listája. Ezt az utolsó pozíciót úgy kell megadni, hogy kijelölünk
négyzeteket százalékban megadott koordinátákkal a háttérképen. Van koordináta választó segéd, ami remekül működik, de csak akkor,
ha előbb kiválasztasz egy háttérképet mintának (ez a háttérkép nem lesz lementve a választóhoz, csakis a segéd használja).

Feltételes
----------

Be lehet állítani, hogy egy ikon csak akkor jelenjen meg, ha egy bizonyos feltétel teljesül. A helyi változók `a`-tól `z`-ig
vannak, és ha az üreset (szóközt) választod, akkor attribútum is hivatkozható.

A kifejezések egyszerűek, vagy `változó=érték` (változó egyenlő az értékkel), vagy `változó=érték&változó>=érték`
(az első változó egyenlő a megadott értékkel és a második változó nagyobb, vagy egyenlő a megadott értéknél).

Amit beállít
------------

A választók mindig beállítják a kiválasztott ikon sorszámát az `a` helyi változóba. Ha szeretnéd, további változók és
attribútumok is módosíthatók.

A "Fusson tovább a választó" pipa, ha be van pipálva, megakadályozza, hogy kilépjen a választó, amikor az adott ikonra
kattintanak.

Példa
-----

Képzeljük el, hogy van egy hátterünk két heggyel, és különböző opciókat szeretnék megjeleníteni attól függően, hogy melyik
hegyre kattint a játékos. Ehhez a következőket kell beállítani:

1. Válasszunk egy ikongyűjtőt (mondjuk balra fent vízszintes), ahol minden ikonnál a feltételt `b=1`-re állítjuk.
2. Válasszunk egy másikat (mondjuk jobbra fent függőleges), ahol meg minden ikonnál a feltételt `b=2`-re állítjuk.
3. Adjuk meg az első hegyet háttérkép elemnek (használd a koordináta segédet), és az "amit állít"-nál adjuk meg, hogy `b=1` és pipáljuk be a "Fusson tovább a választó"-t.
4. Adjuk meg a második hegyet is, ennek az "amit állít"-ja legyen `b=2`, és itt is pipáljuk be a "Fusson tovább a választó"-t.

A játékon belül ez így fog kinézni: a háttérkép a hegyekkel és a választó valahol meghívódik (monjuk egy átvezető után). Amikor
a játékos az első hegyre kattint, akkor a balra fent vízszintes gyűjtőben lévő ikonok meg fognak jelenni. Ha a második hegyre
kattint, akkor ez a gyűjtő eltűnik, és helyette a jobbra fent függőleges gyűjtő ikonjai jelennek meg.

Namost előfordulhat, hogy bizonyos ikonokat csak tapasztalt játékosoknak akarunk megjeleníteni, és van egy `pontszam` attribútumunk,
ami jelzi, hogy mennyire tapasztalt az adott játékos. Ehhez bizonyos ikonoknál állítsuk át a feltételt `b=2&pontszam>=100`-re
például.
