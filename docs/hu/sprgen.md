<h1 tab14 generalo>Generáló</h1>

Válaszd a `Kellékek` > `Generáló` menüpontot. Itt pár kattintással szprájtokat tudsz kigenerálni moduláris sablonképek
felhasználásával.

NOTE: Ha a generáló üresen jelenik meg és minden opció inaktív, akkor telepítened kell előbb a
[TirNanoG Alap](https://tirnanog.codeberg.page#templates) csomagot, hogy legyenek sablonképeid.

HINT: Ami a TirNanoG Alap-ot illeti, a generáló elérhető [webes interfész](https://tirnanog.codeberg.page/base)en is.

Az eszköztár jobb oldalán megdható a generálandó szprájt neve.

Komponensek
-----------

A többi ikon az eszköztárban kiválasztja azt a komponenst, amit konfigurálni szeretnél, úgy mint törzs, fej, ruha, stb.

Balra az entitáslitában látható az adott komponenshez tartozó összes opció. Mindegyiknek van egy saját pipája, amivel
ki-be kapcsolhatod az adott opciót. A legtöbb komponens esetében, mint például törzs, általában csak egy opcióra lesz szükséged,
de akad olyan is, mint például a ruha, ahol több opció is választható egyszerre.

Néhány  sablonkép dinamikusan átszinezhető. Ha ez a funkció elérhető, akkor az épp kiválasztott opcióhoz tartozó színvariánsok
megjelennek az entitáslista alatt. Az adott színre kattintva lehet a kívánt színvariánst kiválasztani.

A komponensek típusai rugalmasak, és teljesen átszabhatók. Lásd a karaktergeneráló [kibövítés]e fejezetet a részletekért.

Szprájt előnézet
----------------

Jobba a fő területen látható a kigenerálandó szprájtok előnézete.

Ha elégedett vagy az előnézettel, nyomd le az <ui1>Importálás</ui1> gombot, és a szprájtok hozzáadódnak a projektedhez.
Ezt követően meg fognak jelenni a "Szprájtok" menüpont alatt, és használhatod az NJK-hoz is.

HINT: Amennyiben játékos [karakter] opciónak is akarod használni, akkor csak egyetlen egy opciót kapcsolj be, és ahhoz generáltass
szprájtokat. Alternatívaként be is importálhatod a sablonképet [szprájtok]ként, a "TNG-Base" elrendezés kiválasztásával.
