Objektumok és csempék
=====================

Nincs nagyon mit mondani az objektumokról, mivel csak minimális kompatíbilitási igényeik vannak. Ne felejtsd el, hogy a
tárgyak esetében is a [perspektíva] ortografikus (fentről-le), a [tereptárgyak és objektumok] színeit gondosan kell
megválasztani, hogy jól illeszkedjenek a csempékhez.

Amikor nagyobb objektumokat csinálsz, akkor ne feledkezz meg arról, hogy a játékosok *belül* is sétálhatnak. Ez csak akkor
lehetséges, ha több rétegben adod meg az objektumot.

<imgc ../img/bridges.png><fig>Nagyobb, átjárható objektumok esetén használj több réteget</fig>

Rácsméret
---------

Az általános rácsméret 32 x 32 pixel, de (előfordulhat hogy) néhány csempe 16 x 16-os darabokból áll.
Ennek az az oka, hogy általános négyzetes tárgy (például szék vagy karakter) belefér a 32 x 32-be.
Minden kellék úgy lett megrajzolva, hogy belesimuljon a 32 x 32-es rácsba, és javasolt, hogy az
általad készített kellékek is kövessék ezt. De a részletesség kedvéért ileszkedhetnek 16 x 16-osra is.

<imgc ../img/32x32grid.png><fig>32 x 32-es rács példa</fig>

Perspektíva
-----------

A kamera szöge fentről-lefelé, szemből nagyjából 60 fokban.

<imgc ../img/ortho.png><fig>Fentről-lefelé, ortografikus vetület</fig>

A megjelenítés ortografikus, ami igazából azt jelenti, hogy *nincs*
perspektíva... a dolgok **nem** lesznek kissebbek, ahogy távolodunk
tőlük. Ha a tereptárgyaidon vagy csempéiden használsz perspektívát,
az baj.

<imgc ../img/chest.png><fig>Perspektíva példa ládán</fig>

Csempe illesztés
----------------

A részletek egyszerűek. Lehet kicsit részletesebb a széleknél, de a közepe
lehetőleg legyen egyszínű, vagy nagyon enyhe mintázatú.

Alkalmanként be lehet dobni egy-két részletgazdagabb csempét, hogy megtörjék
a folyton ismétlődő csempék által keltett monotonitást.

<imgc ../img/tiles.png><fig>Összetetebb mintázat eléréshez összerendezett csempék</fig>

A sarkokat és széleket érdemes így elrendezni.

HINT: Be lehet állítani [automap szabályok]at, akkor a térképszerkesztő igény esetén magától fogja elhelyezni ezeket
a sarok és szél elemeket.
