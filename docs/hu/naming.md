Csempe elnevezés
================

NOTE: A szerkesztő bármilyen elnevezést megenged. Az alábbi csupán a [TirNanoG Alap](https://tirnanog.codeberg.page#templates)
készletben használt konvenció.

HINT: Az utótag séma megtalálható a [guides.zip](https://tirnanog.codeberg.page/guides.zip) fájlban is GIMP rétegként és
különáló PNG-ként is.

Általános utótagok
------------------

A csempék elnevezése következetes. A legtöbb variáns `a` - `z` cimkéjű, néha `1` - `9`. Azok a dolgok, amik nyithatók és
zárhatók, mindig rendelkeznek `0` (zárt) vagy `1` (nyitott) utótaggal. Az utótagok egyszerű szabályt követnek, pár betű
kombinációi: `l` (left, bal), `m` (middle, közép, ismételhető), `r` (right, jobb) ha víszszintes, és `t` (top, teteje),
`m` (middle, közép, ismételhető), `b` (bottom, alja) ha az elem függőleges. Ha mind vízszintes és függőleges változata is
van, akkor az utótag tartalmaz egy plusz betűt `h` (horizontális, vízszintes) vagy `v` (vertikális, függőleges) is. Ha van
egy egyetlen rácscellányi verziója is, akkor annak az utótagja `c` (center, abszolút közép).

```
t                 vt
m     l m m r     vm  hl hm hm hr
m                 vm
b                 vb  c
```

Ha az egyetlen verzió egyszerre nyújtható vízszintes és függőleges irányban is, akkor a kombinációk `tl` (teteje bal),
`tm` (teteje közép, ez vízszintesen ismételhető), `tr` (teteje jobb); `ml` (közép bal, függőlegesen ismételhető),
`bg` (közép-közép helyett, háttér, mind vízszintesen, mind függőlegesen ismételhető), `mr` (közép jobb, függőlegesen ismételhető);
`bl` (alja bal), `bm` (alja közép, vízszintesen ismételhető), `br` (alja jobb). Ha a középső elem egy másikkal együtt is
használható (ami így egy dupla rácscellányi szprájtot ad), akkor annak a másiknak az utótagja `n` (next, rákövetkező), de
többnyire a különböző hátterek csak simán sorszámozva vannak.

```
tl tm tm tr
ml bg bg mr
ml bg bg mr
bl bm bm br
```

Terep utótagok
--------------

A terep csempék utótagja simán `a` - `r`. Nincs benne logika, helyette az volt a cél, hogy amikor a keresőben megjelenik egy
terep találatként, akkor könnyen választhatóan jelenjenek meg az elemei. Itt a `d`, `l` vízszintesen ismételhető, az `f`, `n`
pedig függőlegesen. A középső `a` elem mindig ismételhető mind vízszintesen, mind függőlegesen, és általában vannak alternatívái
(a `b`, `i`, `j` utótagúak), hogy megtörjék a monotonitást. A `q` és `r` esetén a szélek mindig átlátszóak, és középen egy
kissebb darab van a terepből. A wangset megfeleltetés a következő egyébként:

```
11 - a, b   00 - c   00 - d   00 - e   01 - f   11 - g   11 - h
11          01       11       10       01       10       01

11 - i, j   01 - k   11 - l   10 - m   10 - n   10 - o   01 - p
11          00       00       00       10       11       11

00 - q, r
00
```

Tető utótagok
-------------

Hasonló az általános utótagokhoz, de egy kis csavarral attól függően, hogy melyik irányba lejt a tető.

### Dél

Ez az alap, amikor a tető lehető legnagyobb felülete látszódik. A sarkok 2 x 2 rácscellányi méretűek, hogy lehessen díszést is
tenni rájuk. Emiatt a `tm` (teteje közép) és a `bm` (alja közép) 1 x 2 rácscellányi, míg a `ml` (közép bal) és `mr` (közép jobb)
pedig 2 x 1 rácscellányi méretű.

```
so_tl    so_tm    so_tr
so_ml    so_bg    so_mr
so_bl    so_bm    so_br
```

### Észak

Mivel a TirNanoG Alap egy ortografikus készlet, ez leginkább a felülrőlnek felel meg. Tipikusan a tetőknek lehet lapos tetejük,
itt az az "északi" oldaluk.

```
no_tl    no_tm    no_tr
no_ml    no_bg    no_mr
no_bl    no_bm    no_br
```

Van egy kis csavar, mivel ezeken átlalában van szél, ezért szükség van sarokcsempékre is (wangset jelöléssel):

```
10 - no_bg1    01 - no_bg2
00             00

00 - no_bg3    00 - no_bg4
10             01
```

### Dél-nyugat és dél-kelet

Ezek lejtenek, és nagyobb szprájtok. Nem ismételhetőek és nem nyújthatóak, mivel illeszkedniük kell az 1:2 arányú lankás
lejtéshez, így 2 x 4 rácscellányi méretűek.

```
sw    se
```

### Tetőablakok

Ezeknek a kis tornyoknak saját tetejük van. Általában kisebbek, merőlegesek a tetőre. Egy-egy szprájt mind, fix mérettel,
2 x 2 cellányiak (illetve a déi irány 3 x 2).

```
we_top    ea_top

     so_top
```

### Illesztések

Az egyszerűség kedvéért ezek az illesztendő két irányról lettek elnevezve. A `t1` és `t2` mindkettő felső elem, a különbség
csak annyi, hogy amíg a `t1` felett nincs több tető, addig a `t2` esetében a tető folytatódik felfelé.


```
                  we_so_t1    ea_so_t1
                  we_so_t2    ea_so_t2
         we_so_m                        ea_so_m
we_so_b                                          ea_so_b

                   so_we_t    so_ea_t
          so_we_m        so_bg         so_ea_m
so_we_b   so_bm          so_bm         so_bm     so_ea_b
```

### Nyugat-kelet tető

Ez az az eset, amikor mindkét oldala látszik a tetőnek, ahogy nyugatra és kelere lefele megy (fejtetőre állított V alakú).
Két verzió is van ebből, csúcsos (utótagja `acute_`), illetve lankás (utótagja `bevel_`). Az utótag vége a pozícióból adódik:

```
        la    ra
    lb  lc    rc  rb
ld  le  lf    rf  re  rd
lg  lh  li    ri  rh  rg
lj  lk  ll    rl  rk  rj
lm                    rm
```

Mindkét oldalon az `le` vagy `lh` a háttér (a tető vastagságától függően), szóval ezek ismételhetők mind vízszintes, mind
függőleges irányban. Az egyetlen különbség a csúcsos és a lankás között, hogy a második oszlop mindkét oldalon (`lb`, `le`, `lh`,
`lk` és `rb`, `re`, `rh`, `rk`) dupla szélességű, hogy beleférjen a 1:2 arányú lejtés. Az utolsó sor, `lm` és `rm` ugyancsak
dupla szélességű, még az `acute_lm` és `acute_rm` esetén is.

A tetők gondosan úgy lettek kivágva, hogy a csúcsos és lankás tetők második oszlopa összekeverhető legyen, ezáltal egy "törés"
rakható a lejtésbe. Például:

```
                              acute_lb
                            acute_lb
                    bevel_lb
            bevel_lb
  acute_lb
acute_lb
```
