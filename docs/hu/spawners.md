<h1 tab31 keltetok>Keltetők</h1>

NJK keltetetők beállítása
-------------------------

Válaszd a `Játék` > `Keltetők` menüpontot. Balra, az entitásválasztóban látszanak a már felkonfigurált keltetők. Duplán
kattintva egy sorra az adott keltető betöltésre kerül.

Maximális NJK-k száma
---------------------

Ez a szám mondja meg, hogy egy adott időben legfeljebb mennyi NJK-t kezel a keltető. Ha már létrehozott ennyi NJK-t, akkor
nem születnek újak.

Keltetés gyakorisága
--------------------

Ez az az időtartam, amennyi időnként ellenőrzi, hogy kikelt-e a megadott számú NJK.

NJK lista
---------

Ez a lista mondja meg, hogy milyen típusú NJK-t hoz létre ez a keltető. Egy NJK többször is hozzáadható a listához, ezáltan
növelve az esélyét, hogy az az adott típusú NJK születik. A listaelemek esélyéinek összege mindig 100%.

A típusokhoz feltétel is rendelhető. Például, legyen mondjuk egy `szint` karakter attribútumunk, és a keltető csak gyenge NJK-at
hoz létre, ha ez kevesebb, mint 5, de ha nagyobb, mint 10, akkor már minotauruszokat is létrehozhat.

A beállítandó oszlopban megadható, hogy az újonnan létrehozott NJK bizonyos attribútuma mi legyen. Például, kiegészítve a
feltételes születést, beállíthatod az NJK-k erősségét is vele. Ha a beállítandó mező üres, akkor az NJK azokkal az attribútumokkal
születik, amik az [NJK attribútumai] oldalon meg lett adva.
