<h1 tab25 atvezetok>Átvezetők</h1>

Átvezetők létrehozása és módosítása
-----------------------------------

Válaszd a `Felület` > `Átvezetők` menüpontot. Az átvezetők különféle célt szolgálnak: megjelenítik a csapatod logóját, elmesélik
a játékod történetét, hangulatot keltenek a pályák között stb.

Akárcsak a többi felület menüpont esetében, itt is van alul <imgt ../img/preview.png> "Előnézet" gomb.

Átvezetők listája
-----------------

Balra, az entitásválasztóban látszanak a már felkonfigurált átvezetők. Duplán kattintva egy sorra az adott átvezető betöltésre
kerül.

A bal alsó sarokban a <imgt ../img/del.png> "Törlés" gombra kattintva a kijelölt átvezető törlésre kerül. Az alul, középen
lévő <imgt ../img/erase.png> "Törlés" gomb másrészről csak az űrlapot üresíti ki.

Új hozzáadásához adj meg egy nevet és kattints a jobb alsó sarokban az <ui1>Elment</ui1> gombra.

Átvezetők eszköztára
--------------------

Felül, az eszköztárban látod az időtartamot, és egy <imgt ../img/play.png> "Lejátszás" gombot. Ha ki van jelülve az idővonal
egy része, akkor erre a gombra kattintva lejátsza a háttérzene kijelölt szakaszát. Ez arra való, hogy segítsen eligazodni,
merre vagy az idővonalon.

Jobbra van az átvezető belső techinkai neve.

Az eszköztár alatt található az idővonal a csatornákkal:

- Háttér audió (zene vagy film, típustól függően)
- Képek (diavetítő)
- Beszéd
- Feliratok
- Szkript (parancsok, utasítások)

A csatorna ikonjára kattintva azon a csatornán fog dolgozni. Ha <mbr> jobb egérrel kattintasz az idővonalon, akkor mozgathatod,
vagy használhatod a szkrollt is. A felbontás 1 / 100 másodperc, azaz 100 pixel fed le egy másodpercnyi időtartamot.

Háttér megadása
---------------

Válaszd a "Film" vagy "Zene" csatornát. Az idővonal alatt négy beviteli mező fog feltűnni.
<imgc ../img/cutscn0.png>

Az első sor tartalmazza egy egyszer (vagy elsőnek) lejátszandó filmet illetve zenét, míg a második sor az ismételten lejátszandó.
Ha mindkettőt beállítod, akkor az egyszer lejátszandó fog először lejátszódni, aztán az ismétlődő fog ismétlődni, amíg a felhasználó
meg nem szakítja. Egyszer lejátszandó nélkül csak az ismétlődő fog ismétlődni.

Itt a kép és a hang nincs összekötve: például beállíthatsz egy rövid filmet csak képpel egyszer lejátszandónak, aztán egy másikat
szintén csak képpel ismétlődőnek, és egyetlen hosszú zenét mindkettő alá. A szerkesztő (illetve a lejátszó) ezeket magától szépen
össze fogja ollózni.

A háttér az, ami az átvezetőnek gerincet és teljes időtartamot ad. E nélkül nem lehet használni a többi csatornát.

Képek hozzáadása
----------------

Válaszd a "Képek" csatornát. Jelölj ki egy időtartamot az idővonalon. Ebben segíteni fog, hogy a háttéraudió vizuálisan is
megjelenik, és lenyomhatod a "Lejátszás" gombot is, hogy belehallgass a kijelölt részbe. Amikor megfelelő a kijelölés, az
idővonal alatt megadhatod a többi paramétert.
<imgc ../img/cutscn1.png>

Az időtartamot számokkal is beállíthatod. Ettől jobbra látható csúszkákkal a be- és kiárnyalás adható meg. Ha a baloldali csúszkát
legbalrább tolod, a jobboldalit meg legjobbrább, akkor nem lesz árnyalás. Az utolsó ikon, a <imgt ../img/erase.png> "Törlés"
gomb, eltávolítja ezt a diát az idővonalról.

Ez alatt adható meg a dia maga, két mezővel: vagy kiválasztasz egy háttérképet, vagy megadsz egy teljes képernyőt kitöltő színt.
A teljes képrenyős szín remekül használható arra, hogy az átvezető többi csatornáját kitakarjuk vele. Például, tegyük fel, hogy
szeretnénk a térképhelyszínt beárnyékolni, ehhez csináljunk egy totál fekete diát, ami a kiárnyékolódik, pont, amikor a térképnek
elő kell tűnni.

A diák átfedhetik egyást, de egyszerre mindig csak kettő. Ha van két képed, amin a háttér azonos, de az előtéri kompozíció nem
(mondjuk a szereplő máshol van rajtuk), akkor beállíthatod úgy, hogy az első pont akkor árnyékolódjon ki, amikor a második be.
<imgc ../img/fadeanim.gif> Ezáltal a felhasználó azt fogja látni, hogy a háttér nem változik, de a szereplő az első helyről
fokozatosan eltűnik, közben az új helyen meg fokozatosan megjelenik, ami a mozgás látszatát kelti. Ez a fajta egyszerű "animáció"
különösen kedvelt a narrált állóképes átvezetők esetén.

A későbbiekben kattinthatsz az idővonalon a diára, hogy aztán módosítsd (vagy kitöröld).

Beszéd hozzáadása
-----------------

Válaszd ki a "Beszéd" csatornát. Jelöld meg az időpontot, amikor szeretnéd, hogy a beszéd elkezdődjön (kijelölhetsz időtartamot
is, de a narráció esetében az időtartam végét mindig a betöltött hangfájl hossza határozza meg).
<imgc ../img/cutscn2.png>

Emiatt az időtartam számokkal nem megadható, ez a mező inaktív. Mellette egy legördülő menüből választhatod ki a kívánt audiót.
A kényelmed érdekében itt is van egy "Lejátszás" gomb, amivel meghallgathatod, melyik audió lett kiválasztva. A sor utolsó ikonja,
a <imgt ../img/erase.png> "Törlés" gomb eltávolítja a beszédet az idővonalról.

WARNING: A különboző fordítások kicsit hosszabbak vagy rövidebbek lehetnek, mint az a verzió, amit épp használsz. Tartsd ezt
észben, amikor megadod a kezdő pozíciókat. A narrálások nem fedhetik át egymást.

NOTE: Ha szkriptet is használsz, akkor ne felejtsd el, hogy a "Dialógus" parancs ugyancsak tartalmazhat lefordítható beszédet,
ami felül fogja bírálni a beszéd csatornát.

A későbbiekben kattinthatsz az idővonalon a beszédre, hogy aztán módosítsd (vagy kitöröld).

Feliratok hozzáadása
--------------------

Akárcsak a többinél, kattints a "Feliratok" csatorna ikonjára, és jelölj ki egy időtartamot. Mint a diák esetében, az első sor
itt is a számmal megadást, be- és kiárnyalás csúszkákat tartalmazza, az utolsó ikon pedig, a <imgt ../img/erase.png> "Törlés"
gomb eltávolítja a feliratot az idővonalról.
<imgc ../img/cutscn3.png>

A második sorban megadhatod a pozíciót (százalékos koordinátákban, segéddel) és a szöveg igazítását. Egy tipikus feliratozás
pozíciója `50%, 98%`, és `középre` igazított, de persze megadhatsz más értékeket is. Ez követi a szokásos betűtípusválasztó,
szín, stílus, méret, betűtípus.

A harmadik sorban lehet megadni magát a megjelenítendő szöveget. Tartalmazhat `{}`-t, ami a játékos nevére fog lecserélődni,
vagy `{attribútum}`-ot, ami meg a hivatkozott attribútum értékére. Újsor karakter nem megengedett, de több feliratot is kitehetsz
ugyanabban az időpontban, csak egymásalatti pozícióra.

Akárcsak a többinél, itt is kattinthatsz az idővonalon a feliratra, hogy aztán módosítsd (vagy kitöröld).

Csakhogy a feliratok nemcsak átfedhetik egymást, hanem nagyon is gyakori, hogy több felirat is van ugyanabban az időpontban, csak
épp más pozícióval. Több felirat kijelölése az idővonalon lehetetlen, ezért a feliratokat egy listából is kiválaszthatod,
függetlenül attól, hogy átfedik-e egymást vagy sem.

Szkript megadása
----------------

Végezetül van egy "Szkript" csatorna is. Nemigazán lehet több elemet hozzáadni az idővonalon, minvel csak egy lehet belőle. De
ha megadsz egy kijelölést, akkor a szkript egy megfelelő "Késleltetés" paranccsal fog kezdődni (és ha változtatod az időtartamot,
akkor a "Késleltetés" parancs is automatikusan változni fog).
<imgc ../img/cutscn4.png>

Balra látható a fő szerkesztő felület. Jobbra pedig a parancspaletta. A palettáról szimplán áthúzhatod a parancsokat a szerkesztőbe,
ezáltal új parancsokat hozzáadva a szkripthez. A parancsokat a szerkesztőben is áthúzhatod, hogy átrendezd őket. Parancs törléséhez
csak egyszerűen ki kell húzni a szkript szerkesztő mezőből és a mezőn *kívül* lerakni.

Legelöször is, érdemes a szkriptet egy "Helyszín beállítása" paranccsal kezdeni, amivel megadjuk a térképet és rajta a pozíciót,
hogy legyen a szkriptünknek háttere. Ez egyben kiválasztja a napszakot, ami akkor fontos, ha beállítottál a térképhez [szórt fény]t.

Itt az átvezető szerkesztőben csak nagyon kevés parancs áll rendelkezésedre. Nincsenek vezérlések (úgy mint ciklus, elágazás),
van viszont pár Logo-szerű parancs a szereplők mozgatására. Hogy ezeket használhasd, előbb kell egy "NJK elhelyezése" parancs,
ami lerak a térképre egy szereplőt. Ezt követően az adott NJK lesz a kijelölt szereplő. Ha több szereplőd is van, akkor
használd az "NJK kiválasztása" parancsot, hogy megadd, melyik szereplőt akarod mozgatni. A mozgatás parancsok kiadása
**aszinkron**, ami azt jelenti, hogy kiválaszthatsz egy szereplőt, adhatsz neki egy halom parancsot, majd kiválaszthatsz egy
másikat, és neki is adhatsz egy halom másik parancsot. A szkript nem fog megakadni amíg ezek a parancsok végrehajtódnak,
hanem egyből továbblép, és a szereplők a mozgásokat **egyszerre** hajták végre. Ha szeretnéd, hogy a szkript ne folytatódjon,
míg a mozgásokat befejezik, akkor ehhez van egy "Várakozás amíg az NJK befejezi a dolgát" parancs (ami szintén a kijelölt
szerepelőre vonatkozik, akárcsak a mozgatás parancsok).

Van egy kivétel ezalól, az pedig a dialógus. A dialógusablak addig jelenik meg, ameddig meg lett adva, és erre az időre
megakasztja a szkript futását. Ez azért lett így, hogy amíg a dialógus látszik, addig legyen idejük a szereplőknek befejezni
a mozgást vagy az animáció lejátszását.

Amit még tehetsz az átvezető szkriptekben, az a tárgyak elhelyezése és eltávolítása a térképről; tárgy és NJK animációk lejátszása,
stb. Még egy dolog, a tárgyak esetében is érvényes a "csak a kiválasztottra vonatkozik" metódus, akárcsak a szereplőknél. Ez
azt jelenti, hogy legutoljára lerakott tárgy lesz a kiválasztott, és az fogja lejátszani a kért animációt. Egy másik tárgy
kiválasztásához le kell cserélni a térképen a tárgyat pont ugyanarra, mint ami ott volt.

Átvezető leellenőrzése és elmentése
-----------------------------------

Megnézheted a teljes átvezetőt az <imgt ../img/preview.png> "Előnézet" gombra kattintva, akár félkész állapotban is.

De mielőtt elmentenéd, mindenképp meg kell adni egy technikai nevet (az eszköztár utolsó mezője), aztán kattinthatsz az
<ui1>Elment</ui1> gombra.
