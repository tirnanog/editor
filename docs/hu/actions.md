<h1 tab28 actions>Cselekvések</h1>

Alapértelmezett cselekvések beállítása
--------------------------------------

Válaszd ki a `Játék` > `Cselekvések` menüpontot.

A játékban különböző interakciók vannak NJK-kal, tárgyakkal stb. Ezek mindig relációban értendők. Beállíthatsz egy eseménykezelőt
minden típushoz, vagy beállíthatsz egy specifikus kezelőt egy bizonyos elemhez. Az előbbi, globális eseménykezelőket lehet itt
megadni. Az elem specifikus eseménykezelők (amik, ha fel vannak konfigurálva, akkor felülbírálják az itt megadottat), az
[NJK-k] és [Objektumok] oldalon állíthatók.

Alapértelmezett eseménykezelők
------------------------------

Összesen 9 cselekvést lehet megadni. Hogy ezt hogyan osztod fel az teljesen rajtad és a készített játékon áll. Egy egyszerű
fantasy RPG-ben például az 1-es cselekvés lehet a közelharci támadás, a 2-es cselekvés pedig az távolraható íjászat. Egy kaland
típusú játék esetén az 1-es cselekvés lehet a használd a tárgyat, a 2-es a megvizsgál, a 3-as a kinyit, a 4-es az ellök,
az 5-ös a beszélget stb.

Azon cselekvések esetén, amikhez nem kell érintkezés, meg kell adnod, hogy melyik attribútum tárolja a hatótávot. Például,
ha a 2-es cselekvés az íjászat, akkor választhatod hatótávnak az Erő attribútumot. Minnél erősebb a karakter, annál messzebbre
tudja kilőni a nyilat az íjjal. Csak egy példa, teljesen rajtad áll.

Ami fontos, hogy minden cselekvéshez, amit használni szeretnél a játékban, kell hogy megadj alapértelmezett eseménykezelőt.
Például ha az 1-es cselekvés a közelharc, akkor ennek a cselekvésnek az eseménykezelője fog lefutni, amikor a játékos egy
olyan tárgyat használ, ami az 1-es cselekvéshez tartozik (például kard), és ez az az eseménykezelő, ami leírja, hogy a kardcsapás
betalált-e vagy sem, és ha igen, akkor mennyivel csökkenti az ellenfél életerő attribútumát stb. Eseménykezelők nélkül a játékban
szerepelő tárgyak csak használhatatlan kellékek.
