Projektek
=========
<h2 tab2 uj_projekt>Új projekt</h2>

Válaszd a menüben a `Projekt` > `Új projekt` menüpontot. Töltsd ki a mezőket:

- **Azonosító** a játék technikai neve (gameid). 16 karakter lehet, amiben csak ékezet nélküli betűk és számok szerepelhetnek.
- **Cím** a játék játékosok által látott, lefordítható neve. Ebben bármilyen UTF-8 karakter megengedett.
- **Sablon** ha letöltöttél pár [sablon]t, akkor itt tudod kiválasztani az egyiket.
- **Csempe** a térkép típusa és a térképelemek mérete. A típus lehet:<grid><gr><gd><imgl ../img/ortho.png><i><b>orthografikus</b></i> (fentről-le)</gd><gd><imgl ../img/iso.png><i><b>izometrikus</b></i> (hívják még 2.5D-nek is)</gd></gr></grid>
- **Térképméret** csempékben. Egy térkép mindössze N x N csempe méretű, de a térképek összefűzhetők egy nagy nyitott világgá.

WARNING: A csempeméret és a térképméret szorzata lehetőleg ne haladja meg a 4096-ot, mert az gondot jelenthet néhány lejátszónak
régebbi gépeken vagy limitált erőforrású eszközökön.

Ha megvagy, kattints az <ui1>Új projekt létrehozása</ui1> gombra.

<h2 tab3 projekt_betoltese>Projekt betöltése</h2>

Ha már korábban hoztál létre projektet, akkor válaszd a `Projekt` > `Projekt betöltése` menüpontot. A listából válaszd ki a megfelelőt,
és kattints kétszer rá.

Amikor elindítod a TirNanoG Szerkesztőt, és már van projekted, akkor ez az oldal fogad.

Ha szeretnél többet tudni arról, hogy milyen formátumban vannak tárolva a fájlok a projektben, kattints a
[TirNanoG Projekt](https://codeberg.org/tirnanog/editor/src/branch/main/docs/proj_format.md) dokumentációra (angol).

<h2 tab4 jatek_importalasa>Játék importálása</h2>

A terjesztése alkalmas, [TirNanoG Fájl Formátum](https://codeberg.org/tirnanog/editor/src/branch/main/docs/tng_format.md)ban
tárolt játékok vissza is tölthetők a szerkesztőbe. Ehhez válaszd a `Projekt` > `Játék importálása` menüpontot. Keresd meg a kívánt
játék fájlt, majd kattints az <ui1>Importálás</ui1> gombra.

Ez egy új projektet hoz létre a beimportált játéknak.

<h2 tab5 sablon_importalasa>Sablon importálása</h2>

Válaszd a `Projekt` > `Sablon importálása` menüpontot. Keresd meg a kívánt sablon fájlt, majd kattints az <ui1>Importálás</ui1>
gombra.

Ez nagyon hasonló a projekt készítése sablonból opcióhoz, de ez nem hoz létre új projektet, hanem egy már meglévő projektbe
tölti be a sablont. Ezzel a funkcióval több különböző sablon is betöltehető ugyanabba a projektbe, ezért nem játéksablonokhoz
használatos, hanem kisebb modul sablonok esetén.

Játék exportálása
-----------------

Válaszd a `Projekt` > `Játék exportálása` menüpontot. Elérhető az <kbd>F2</kbd> gyorsbillentyűvel is.

Kigenerálja a `game.tng` fájlt a projekthez tartozó `~/TirNanoG/(gameid)` mappába. Hogy ezt a fájlt használhasd, szükséges a
[TirNanoG Lejátszó](https://tirnanog.codeberg.page/player).

Futtathatod a játékot úgy is, hogy a `game.tng` fájl a projekt mappábájában van a <imgt ../img/playbtn.png> Lejátszás gombra
kattintva, vagy előbb átmásolhatod a végső helyére, `C:\Program Files\(gameid)\game.tng` (Windowson) illetve
`/usr/share/games/(gameid)/game.tng` (Linuxon), ahol a TirNanoG Lejátszó keresi ezeket a fájlokat. A `.tng` fájlok mellé
lementésre kerül egy indítófájl is, `(cím).lnk` (Windowshoz) és `(cím).desktop` (Linuxhoz). Ezek a végső helyéről indítják a
játékot.

<h2 tab7 export_extension>Kiegészítő exportálása</h2>

Válaszd a `Projekt` > `Kiegészítő exportálása` menüpontot. Ugyancsak elérhető a <kbd>Shift</kbd> + <kbd>F2</kbd> gyorsbillentyűvel.

Egy kiegészítő nagyon hasonló a `game.tng` fájlhoz, de máshogy kell hívni, és szabad kezet kapsz, hogy a projekt mely elemeit
exportálja bele.

Sablon exportálása
------------------

Nemcsak betölteni lehet sablonokat egy projektbe, hanem egy projektet sablonnak is lementhetsz. Ehhez válaszd a
`Projekt` > `Sablon exportálás` menüpontot.

Az elkészült sablon `~/TirNanoG/(gameid).zip` néven kerül lementésre.

<h2 tab9 license_keys>Licenszkulcsok</h2>

### Szerkesztő licensz játékfájlokhoz

A `Projekt` > `Licenszkulcsok` menüpont lehet, hogy inaktív. Ahhoz, hogy nem szabad és kereskedelmi (akár zárt kódú) játékot
készíthess, a [készítő engedélyét kell kérni](https://codeberg.org/tirnanog/editor/issues/new) egy *bizalmas* bejelentőben
(lásd [TirNanoG Fájl Formátum Licensz]). Válaszul egy `license.txt` fájlt fogok küldeni, amit a projekt mappájába kell
lementeni. Ez lehetővé teszi **kódolt** `game.tng` fájlok létrehozását és **végfelhasználói kikódoló kulcsok** generálását
a szerkesztőben.
![Végfelhasználói licenszkulcsok generálása](../img/tngel.png)
Hogy hogyan értékesíted és kinek ezeket a kikódoló kulcsokat, az teljesen rajtad áll (néhány tippet találsz alább). Amikor
egy végfelhaszáló kódolt játékfájlt tölt be, a TirNanoG Lejátszó automatikusan bekéri ezt a kikódoló licenszkulcsot és
kikódolja vele az adatfájlt, nincs szükség harmadik fél programjára vagy speciális lejátszóra. Sőt mi több, ezek a kódolt
adatfájlok nem importálhatók vissza a szerkesztőbe, és nem módosíthatók tovább.

<h3 webshop jatek_ertekesitese>Játék értékesítése</h3>

Több mód is a rendelkezésedre áll, az automatizáltság függvényében növekvő sorrendben ezek a következők.

#### 1-es metódus: Kézivezérlés

NOTE: Hobbistáknak, rajongói játékok készítőinek ideális, akiknek csak egy statikus weboldaluk vagy csak egy FB profiluk van.

A lehető legegyszerűbb, válaszd a `Projekt` > `Licenszkulcsok` menüpontot. Azon az oldalon, kattints a <ui1>Generálás</ui1>
gombra. Egy új végfelhasználói kikódoló kulcs kerül a vágólapodra, amit aztán beilleszthetsz e-mailbe, csetre, stb.

Bár ez felettébb egyszerű, ez a metódus emberi beavatkozást igényel valahányszor elkel egy példány a játékból.

#### 2-es metódus: Előregenerált kulcsok

NOTE: Valamennyire ismert indie játékfejlesztőknek és félprofi stúdióknak ideális, akik saját weloldalt bérelnek egy szolgáltatótól
(és amin tipikusan WordPress, Drupal, Joomla, eCommerce stb. szolgáltatja a honlapot).

A második lehetőség egy rakat kulcs előre generálása. Akárcsak az előbb, válaszd a `Projekt` > `Licenszkulcsok` menüpontot, majd
kattints a <ui1>Generálás</ui1> gombra párszor (és most, még jópárszor kattints rá). Ezután töltsd fel a
 `~/TirNanoG/(gameid)/endusers.txt` fájlt a szerveredre, és használd a webboltodból. Berakhatod SQL táblába, vagy használhatod
a fájlt direktben is, rajtad áll.

WARNING: A webboltod felelőssége nyilvántartani, hogy mely, a fájlban szereplő kulcs lett már eladva.

Amikor a webboltod kifogy az eladható kulcsokból, akkor csak kattints még párszor a <ui1>Generálás</ui1> gombra, és másold fel
újra az `endusers.txt` fájlt a szerverre.

Ehhez nem szükséges semmi különös a szerveren, csak egy sima szöveges fájl (ezért ez a módszer *rendkívül biztonságos* és
könnyedén illeszthető, mindenféle webbolt programmal működőképes) és csak néha-néha igényel emberi beavatkozást.

#### 3-as metódus: Kulcsgenerálás igény esetén

NOTE: Ez profi játékkészítőknek való, akik megengedhetnek maguknak egy saját webszervert (tipikusan Azure, AWS, stb.
virtuálisgépet), ahol szabadon futtathatnak harmadik féltől származó programot.

A szerveren hozz létre egy `/home/(webszerver felhasználó)/TirNanoG/(gameid)` nevű mappát, és másold oda a szerkesztő licensz
fájlját. Például `/home/apache/TirNanoG/fantasztikusrpg/license.txt`. Amikor egy új kulcsra van szükség, futtasd le a
`tnge -g (gameid)` parancsot a webboltodból, ami egy új, mindig egyedi végfelhasználói kulcsot fog visszaadni a szabványos
kimeneten.

Például, PHP-ban ez így néz ki:
```php
$uj_vegfelhasznaloi_kulcs = trim(exec("/usr/bin/tnge -g fantasztikusrpg"));
```

Vagy Python-ban:
```python
uj_vegfelhasznaloi_kulcs = os.popen("/usr/bin/tnge -g fantasztikusrpg").read().strip()
```

Ez nem igényel emberi beavatkozást egyáltalán, de cserébe a TirNanoG-ot a szerveren kell futtatni. Mivel felhasználói bemenet
nem kerül a parancssorba, és csak egy konstans sztringet futtat le, ennek a *biztonsági kockázata nagyon minimális*.

HINT: Nincs szükség a projektfájlokra, sem függvénykönyvtárakra vagy grafikus felületre a szerveren! Semmire sem! Össz-vissz
ami kell, az a `tnge` futtatható és a `license.txt` fájl, semmi más.
