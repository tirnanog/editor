<h1 tab33 objektumok>Objektumok</h1>

Objektumok hozzáadása
---------------------

Válaszd a `Játék` > `Objektumok` menüpontot. Alapból a tárgyak csak akadályok a térképen. Ha szerenéd, hogy a játékos
interakcióba léphessen velük, akkor meg kell adni az infóit. Balra, az entitásválasztóban látszanak a már felkonfigurált
infóval rendelkező objektumok. Duplán kattintva egy sorra az adott objektum infói betöltésre kerülnek. Ha egy olyan
objektumot szeretnél betölteni, aminek még nincsenek infói, akkor használd az objektum szprájtválasztót az eszköztárban.

NOTE: Az objektumok listája alatt látható a szokásos <imgt ../img/del.png> "Törlés" gomb, de van itt még egy
<imgt ../img/dupobj.png> "Objektum duplikálása" mező és gomb is. Írj be egy nevet és kattints a gombra, ez nemcsak a szprájtot
másolja le, hanem a meta infóit is.

Az eszköztáron balra van az objektum szprájtválasztó, a "Térképen" ikon és a "Leltárban" ikon. Ezek az ikonok átváltják az
oldalt, mert olyan sok konfigurálási lehetőség van az objektumokhoz, ami nem fért ki egy oldalra.

Objektumok a térképen
---------------------

A legegyszerűbb, amikor a tárgy csak a térképen van, mint egy fal vagy utcai lámpa. Ezeknek nincs játékon belüli nevük.
Minden, amire szükségük van, az csupán egy ütközési maszk, ami jobbra állítható. Válassz ki egy szprájtot, és látni fogod
a rácsot, rajta félig áttetszően a tárgy képével.

### Ütközésmaszk

<imgr ../img/coll.png>Láthatóság (a tárgy áttetszősége) a tárgy feletti "Láthatóság állítása" ikonnal állítható. Lehetséges
értékei `25%`, `50%`, `75%`, valamint `100%` lehetnek, ami <mbl> bal egérkattintással növekedő irányban változik, míg <mbr>
jobb egérkattintással csökken.

A rácson, minden egyes cella külön-külön kattintható. Itt a <mbl> bal egérgomb előre tekeri a státuszt, míg a <mbr> jobb
egérgomb hátrafelé. Minden cellának egy saját ütközési értéke lehet:
- az üres azt jelenti, hogy sétálható és repülhető, de nem úszható
- <imgt ../img/block.png> a fal azt jelenti ütközés, nem sétálható, nem repülhető, nem úszható,
- <imgt ../img/swim.png> a hajó nem sétálható de úszható és repülhető,
- <imgt ../img/fly.png> a repülőgép jelentése nem sétálható, nem úszható, de repülhető,
- <imgt ../img/climb.png> a létra sétálható, de egy másik animációt használ hozzá,
- <imgt ../img/jump.png> az ugró ember nem sétáható és nem úszható de repülhető; kivéve, ha ugranak, akkor sétálható,
- <imgt ../img/above.png> a fél fal alul azt jelenti ütközés föld alatt, de föld fölött járható,
- <imgt ../img/below.png> a fél fal felül meg azt, hogy ütközés föld felett, de föld alatt járható.

Egyszerű tárgyak esetében nincs szükség eseményekre, de a kék sakkbábu, az <imgt ../img/event.png> "Esemény" ikon ki-be
kapcsolásával állítható, hogy mely cellák váltanak ki eseménykezelő futtatást.

<h3 obj_event_handlers>Eseménykezelők</h3>

Ha adtál meg esemény cellát, akkor az eseménykezelőit balra tudod megadni. Ezek pont olyanok, mint az [NJK-k] esetében,
kivéve, hogy ahol az NJK-knál a viselkedést állítod, a tárgyaknál ugyanott a cselekvéskategóriát. Ez a kategória mondja meg,
hogy amikor a tárgy fel van szereve és használják, akkor az [alapértelmezett eseménykezelők] közül melyiket kell lefuttatni.

Tegyük fel, hogy az "1-es cselekvés" a közelharci támadás, és a "2-es cselekvés" a távolsági. Ekkor egy kardnál az első kategóriát
kell beállítani, míg egy íj esetén a másodikat. Ez csak az alapértelmezett eseménykezelőt állítja (amikor a játékos használja
az adott tárgyat), de további eseménykezelők is megadhatók más eseményekhez.

Például, ha azt szeretnénk, hogy a játékos automatikusan felszedje a térképről az adott tárgyat, akkor azt meg kell adni a
"hozzáéréskor" eseménykezelőben egy "Objektum eltávolítása a térképről" és egy "Objektum játékosnak adása" paranccsal.

### Felszerelt tárgyak

Amikor valamelyik tárgyat felszereli a játékos, az megváltoztathatja a karakter kinézetét. Ez opcionális, és főként olyan
tárgyak esetén van értelme, mint ruhák, fegyverek, pajzsok stb. Az ütközési maszk alatt lehet ezt beállítani.

Először is, minden karaktercsoporthoz megadható ugyanaz a kinézet (ha a "(alapért)" van kiválasztva), vagy beállítható
mindengyikhez külön-külön, ha egy adott karaktercsoport opciót választasz (a lehetséges opciók abból az [opciócsoport]ból
jönnek, amit "alap kinézet"-tel jelöltél meg).

A felszerelt tárgyaknál csak két réteg van: egy, ami a karakterrétegek fölött, és egy, ami alattuk található. Minden alap
cselekvéshez megadható szprájt (álldogál, sétál, úszik, stb.), de a felhasználó által konfigurált cselekvéshez csak akkor,
ha megadtál cselekvéskategóriát a tárgyhoz.

<h3 obj_onmap_preview>Előnézet</h3>

Az <imgt ../img/preview.png> "Előnézet" gombra kattintva megnézheted, hogy mutat a tárgy a karakteren. Ez pont úgy néz ki,
és úgy is működik, mint a karakteropciók előnézete, bővebb leírásért lásd az [opciószprájtok] fejezetet a karaktereknél.

Leltárban
---------

Ezen a fülön beállítható, hogy hogyan látszódjon a tárgy, amikor a karakter leltárában van. Ha felvehető a tárgy (vagy
virtuális, mint mondjuk egy varázslat), akkor mindenképp meg kell adni játékbeli lefordítható nevét a "Cím" mezőben.

Ha szeretnéd, akkor megadhatod, hogy a leltárban máshogy nézzen ki a tárgy, mint a térképen. Ezt jobbra, a "Cím" mezővel
egy vonalban lehet beállítani. Ennek a szprájtnak a mérete ugyanaz, mint ami a [leltár]nál megadott rekeszméret a
`Felület` > `Képernyőadatok` menüpontban.

Balra lehet felkonfigurálni, hogy a tárgy hova szerelhető fel, és ha szeretnél másik szprájtot hozzá, akkor azt, hogy melyiket.
Ezalatt két mező van, egy a hangeffektnek, amikor a tárgyat áthelyezed vagy felszereled, a másik pedig a tárgy alapértelmezett
ára (egységgel együtt, bővebb infóért lásd a [kereskedő létrehozása] fejezetet). Megadhatod az árkategóriát is, ahogy csak
akarod, ennek a használata nem kötelező. Például dönthetsz úgy, hogy az 1-es kategória a hús, a 2-es a fegyverek, és egy bizonyos
fegyverkovács csak a 2-es kategóriában lévő tárgyakat hajlandó megvenni, 1-est nem.

Jobbra lehet állítani a tárgy attribútumait.

<h3 obj_equip_objects>Felszerelt tárgyak</h3>

Néhány tárgy felszerelhető. Ha igen, akkor itt adható meg, hogy hova (a lehetséges helyeket a [leltár]nál lehet megadni a
`Felület` > `Képernyőadatok` menüpontban). Van egy speciális hely, a "képesség". Ha ezt bepipálod, akkor a "tárgy" nem egy
igazi tárgyként lesz kezelve, hanem képzeletbeliként; a leltár "Képességek" fülén fog megjelenni, és nem a "Tárgyak" között.
Ezeket nem lehet úgy felszerelni, mint az igazi tárgyakat, de mégis lehetőséget kell biztosítani, hogy az eszköztárra
tegyük őket, mivel a játékosnak el kell tudnia érni őket a gyorsbillentyűkkel. Egy fantasy játékban ezek tipikusan a
varázslatok, de arra használhatod ezeket a "képességeket" a játékodban, amire csak szeretnéd.

Alapból a felszerelt rekeszben ugyanazzal a szprájttal jelennek meg a tárgyak, mint a leltárban. Mindazonáltal előfordulhat,
hogy bizonyos tárgy több rekeszt is igényel, és minden rekesznél más-más szprájtot szeretnénk megjeleníteni. Például az
íjhoz mindkét kézre szükség van, és hülyén nézne ki, ha mindkét kézben íj jelenne meg. Ezért az elsődleges kéznél meghagyható az
eredeti íj szprájt, a másodlagos kézhez meg megadható egy nyílvessző például.

Itt lehet beállítani a tárgy alapértelmezett árát is, ha szeretnéd. Az egység szabadon választható, attól függően, mire van
igénye a játékodnak, például lehet egy aranyérme.

Végezetül, ha kiválasztott kategória távolsági, akkor érdemes megadni a lövedék szprájtját (például íj esetén egy nyílvessző,
vagy tűzgolyó varázslóbotnál), és hogy igényel-e lőszert vagy sem.

### Tárgyattribútumok

Nem minden tárgy szerelhető fel minden játékos által. Néhány igényel egy bizonyos szintet. Hogy ez megadd, fel kell tölteni az
"Szükséges attribútumok" táblázatot jobbra. Amennyiben egy játékos teljesíti az előír attribútumokat, akkor felszerelheti a
tárgyat.

Továbbá, amikor egy tárgy fel van szerelve vagy használják, akkor befolyásolhatják a karakter attribútumait ideiglenesen.
Például egy hosszúkard hozzáadhat a "Támadás" attribútumhoz. Vagy egy varázspálca megnövelheti a "ManaMax" attribútumot, stb.
Ezek az "Attribútummódosítók" táblázatban adhatók meg. A növelésre két lehetőség is adott: vagy egy adott konstans értéket adnak
az attribútum értékéhez, vagy pedig pedig adott százalékkal növelik.

Van egy speciális módosító, ami nem egy attribútumot változtat, hanem azt, hogy hogyan közlekedik a karakter, ez pedig a
"(közlekedés)". Például, egy fantasy játékban a játékos találhat egy sárkányszárnyat, amit ha felszerel, akkor az megváltoztathatja
a közlekedés módját "sétál"-ásról "repül"-ésre.

<h3 obj_ininv_preview>Előnézet</h3>

Ha úgy kattintasz az <imgt ../img/preview.png> "Előnézet" gombra, hogy közben a "Leltárban" fül az aktív, akkor azt fogja
megmutatni, hogy hogyan néz ki az adott tárgy a leltárban.

Amikor végeztél a tárgy felkonfigurálásával, ne felets el az <ui1>Elment</ui1> gombra kattintani.
