Karakteranimációk
=================

A karaktereknek és felszerelhető tárgyaiknak kötelező a TirNanoG Alap elrendezést használniuk (lásd a példát alább).
Ez nagyon fontos, mert az azonos elrendezés garantálja a kompatíbilitást és teszi lehetővé a szprájt [generáló]t.

Arányok
-------

A karakterek kicsit összenyomottak, kerekdedek, nem realisztikusan arányosak.
A test magassága nagyjából két és félszerese a fej magasságának, és ugyanaz a perspektívájuk,
mint a csempéknek. Az alap karakternek be kell férnie egy 32 x 48-as területre és
felszereléssel egy 48 x 64-esbe. A körvonalnak feketének, vagy nagyon sötétnek kell
lennie, nem az alapszín sötét árnyalatának.

<imgc ../img/characters.png><fig>Példa felszerelt karakterekre</fig>

Mudolaritás
-----------

A karakterek több, szigorúan pozícionált rétegből épülnek fel. Ez azt jelenti, hogy a TirNanoG Alapban a karakterek
modulárisak, különböző szprájtlapok használatával különböző karaktereket és NJK-kat legózhatsz össze a kedved szerint.

<imgc ../img/sheets.png><fig>Modularitás több réteg használatával demonstrálása</fig>

### Törzs és fej

Az első és legfontosabb réteg a `body` (törzs), ami áll a testből, karokból és lábakból. A második a `head` (fej).
Ezekből több verziónak kell lennie:

* női (feminin)
* férfi (maszkulin)

Mindettő esetén a fej és kezek pozíciója kötött, képkockánként mindig ugyanott kell lenniük, az animációtól és iránytól függően.

### Ruhák és felszerelések

Ez a réteg igazából több rétegből is állhat, a felszerelés igényeitől függően. Egy teljes vért tipikusan csak egy réteget igényel,
míg egy modern ruha legalább hármat (póló, nadrág, cipő). Nincs korlátozás, ezek a rétegek szabadon definiálhatók és teljesen
személyre szabhatók a szprájt [generáló]ban.

Mivel a ruhák ugyanazokat az iránymutató sémákat használják, mint a test, ezekből is több variáció létezhet (női, férfi, stb.),
de a törzs réteggel ellentétben nem kell mind, elég, ha csak az egyik variációhoz van.

### Eszközök

Minden, amit a karakterek kezébe kell pozicionálni. Magában foglalja a fegyvereket, varázspálcát, fáklyát, stb. Az iránymutató
sémák sztandardizált kéz pozíciókat tesznek lehetővé, ezért nincs szükség külön variációra minden testhez (azaz például egy kard
szprájtnak illeszkednie kell mind a női, mind a férfi törzs rétegéhez). Animációk szempontjából ez a leglazább réteg: a mozgás
animációkra tipikusan itt is szükség van ugyan, de a "használ" animációk közül csak egyetlen egy kell.

Az eszközök réteg tipikusan egy rétegből áll, de előfordulhat, hogy ketté van véve: `toolb` az összes többi réteg mögött
helyezkedik el, míg a `toolf` az összes többi felett. Erre akkor lehet szükség, ha az eszköznek hosszú szára van, mint például
egy kaszának, vagy ha a kellék rétegből nem akarod "kivágni" azokat a részeket, amiket a test rétegnek ki kell takarnia.

Animáció típusai
----------------

| Név        | Képkocka | Leírás                                               |
|------------|---------:|------------------------------------------------------|
| idle       |  2       | semmittevés animáció                                 |
| hurt       |  2       | amikor a karakter megsérül                           |
| block      |  2       | amikor sikeresen kivéd egy támadást                  |
| walk       |  6       | normális sétálás                                     |
| run        |  6       | futás, mint a sétálás, csak gyorsabb mozgás          |
| jump       |  6       | függőleges ugrás                                     |
| slash      |  6       | 1-es cselekvés: közelharc karddal                    |
| thrust     |  6       | 2-es cselekvés: közelharc lándzsával                 |
| bowing     |  6       | 3-as cselekvés: távolsági, lövés íjjal               |
| cast       |  6       | 4-es cselekvés: varázslás                            |
| push       |  6       | 5-ös cselekvés: tárgyak eltolása, olyan, mint a séta |
| carry      |  6       | 6-os cselekvés: tárgyak cipelése, olyan, mint a séta |
| climb      |  6       | létrán mászás ^^csak 1 irány^^                       |
| spawn      |  6       | megjelenés ^^csak 1 irány^^                          |
| die        |  6       | elfogyott az életerő, meghalás ^^csak 1 irány^^      |

Szprájtlap elrendezés
---------------------

INFO: Ez a rács valamint az iránymutató sémák letölthetők innen: [guides.zip](https://tirnanog.codeberg.page/guides.zip).
Ebben van egy `guides.xcf` a GIMP-hez, de ha nem azt használnád, akkor minden réteg PNG-ben is le lett mentve.

Egy animáció szigorú szabályok szerint van elrendezve a szprájtlapon. Az irányok a sorokban vannak tárolva (dél, nyugat,
észak, kelet, ebben a sorrendben), az animáció képkockái pedig az oszlopokban (kivéve a *semmittevés*, *sérülés*, *védés*
animációkat, amiknek csak 2 képkockájuk van, és egymás mellé vannak pakolva, minden animáció 6 képkockás).

Ezt követően minden animáció (6 x 4 rácscellányi) szintén szigorú szabályok szerint van elhelyezve egymás mellé, így:

<imgw ../img/layout.png><fig>A TirNanoG Alap szprájtlap elrendezése</fig>

Az ilyen szprájtlapokat beimportálhatod a [szprájtok] menüpontban a "TNG-Base" elrendezést használva, de a szprájt
[generáló] is használja őket.

A jobb alsó sarokban van egy 4 x 5 rácscellányi portré. Ez moduláris portrék összeállításához használatos, de csak akkor,
ha az adott kellék *nem* befolyásolja az arckifejezést.

Az arckifejezések szintén modulárisak és több képből tevődnek össze (egy a szemeknek, egy az orrnak, egy a füleknek stb.)
de 4 darab 4 x 5 rácscellányi, vízszintesen elhelyezett képből állnak (a teljes méret így 16 x 5 rácscella), ahol minden
kép egy arckifejezésnek felel meg, rendre: semleges, vidám, szomorú, mérges.

<imgw ../img/portraits.png><fig>A portrék elrendezése</fig>

Kibővítés
---------

Bármikor új szprájtlapokkal bővítheted a készletet. Ezek a `~/TirNanoG/.common` mappában találhatók, és mind PNG fájl. Akárcsak
a többi kellék esetén, nincs adatbázis, a szerkesztő automatikusan beolvassa az új fájlokat.

### Rétegdefiníciók

A `(réteg).png` nevű, 16 x 16 pixeles képek ikonokat tartalmaznak és egy új réteget definiálnak. Mivel a rétegeket ábécé
szerint rendezi, ezért érdemes egy számmal kezdeni a nevet (ez a szám nem fog megjelenni a felületen, csupán a helyes
sorrendet garantálja, például `1haj`, `2ruha`). Van 4 beégetett réteg, `body` (törzs), `head` (fej), `toolb` (eszköz háttér) és
`toolf` (eszköz előtér), ennek oka szintén a helyes rétegzési sorrend biztosítása.

INFO: A generáló a végső szprájt összeszerkesztésekor a rétegeket ebben a sorrendben rakja össze: legelőször a `toolb` rétegek.
Aztán a `toolf`-et leszámítva minden egyéb réteg, a bennük szereplő `(szám)` alapján rendezve, minden egyes iteráció során
először a `body`, `head` rétegekkel kezdve, és a felhasználó által definiált többi réteggel ábécé sorrendben folytatva, míg végül
a `toolf` rétegekkel zárva a sort. Egy rétegen belül a változatok szintén ábécé sorrendben kerülnek sorra.

### Szprájtsablonok

Minden olyan képnek, ami nem ikon, az elnevezése `(réteg)_(szám)_(változat).png`, ahol a `(réteg)` egyike a beégetett neveknek
vagy az egyik felhasználó által definiált ikon neve. A 16 rácscellányi magasságú képek karakterszprájtlapként lesznek kezelve, és
muszáj a TirNanoG Alap [szprájtlap elrendezés]t követniük. Ezek adhatnak a portréhoz is, de csak arckifejezés független elemeket.
Az eszköz réteg speciális, mivel a `toolb` és `toolf` igazából két réteget takar, de ezek együtt, egyben lesznek kezelve. A
`(szám)` mező egy `0` és `9` közötti szám, és csak a változatok korrekt sorrendjét biztosítja, végezetül a `(változat)` a
szprájtlap neve, szabadon választható, kivéve, hogy nem lehet benne szóköz (használj aláhúzást) és zárójel (lásd alább).

A portrésablonok elnevezése szintén `(réteg)_(szám)_(változat).png`, de ezek magassága 5 rácscellányi. Nem adnak a karakter
kinézetéhez, csakis a portréjához, viszont ahhoz többféle arckifejezést is.

Ezek a rétegek sincsenek beégetve, bármikor újat definiálhatsz csupán egy ikon kép megadásával. Például, csinálhatsz egy
`fulek.png` ikont, aztán máris elkezdhetsz hozzáadni olyan képeket, mint `fulek_1_tunde.png`, `fulek_1_kicsi.png` stb.

Opcionálisan a szprájtsablonok elnevezése lehet olyan, hogy `(réteg)_(szám)_(változat)_((függőség)).png`, például
`fulek_1_tunde_(ferfi).png`. Ezek a változatok csak akkor választhatók, ha a `*_[0-9]_(függőség).png` változat is ki
lett választva. Csak egy zárójel megengedett, és csakis a fájlnév végén lehet. Ha több, más-más rétegen lévő sablonnak is
ugyanaz a neve, akkor csak a legelső találatot veszi figyelembe, de a fájlnevek `(változat)` részének leginkább egyedinek
kell lenniük.

### Átszínezés

Használhatsz egy jópofa trükköt a sablonok dinamikus átszínezésére. Ehhez a képen kell legyen pár speciális szürkeárnyalatos
pixel, egészen pontosan `#101010`, `#303030`, `#505050`, `#707070`, `#909090`, `#b0b0b0`, `#d0d0d0` vagy `#f0f0f0`, valamint
egy 8 x 16 pixeles mátrixnak a *jobb alsó sarokban* (vagy, a karakteres [opciószprájtok] esetében külön szprájtban). Itt
minden sor egy palettát tartalmaz. Ezek a szürkeárnyalatok az egyik oszlopbeli színre hivatkoznak (a legvilágosabb balra, a
legsötétebb szín jobbra található), a sorok között pedig a felhasználó választhat. Ezután a generáló lecseréli a szürkeárnyalatos
pixeleket az így kiválasztott színre.

<imgc ../img/recolor.png><fig>A színvariációk működése</fig>

Ahhoz, hogy egy színvariáció megjelenjen, a legvilágosabb színt (ami a kép szélessége - 8. pozícióban van, és amire a `#f0f0f0`
lecserélődik) mindenképp meg kell adni. A többi szín azon múlik, hogy milyen szürkeárnyalatok fordulnak elő a képen. Például,
a második színt üresen hagyhatjuk, ha a képen nincs `#d0d0d0`.

Az [sprsheet](https://codeberg.org/tirnanog/sprsheet) alkalmazás képes automatikusan színteleníteni bármilyen képet a `-p`
kapcsoló megadásával. Az ehhez szükséges lépések:

| Lépés | Leírás                                                                                                                |
|------:|-----------------------------------------------------------------------------------------------------------------------|
|     1 | <imgr ../img/decolor1.png>csinálj egy 8 x 16 pixeles átlátszó képet, ezen lesznek a palettavariációk                  |
|     2 | <imgr ../img/decolor2.png>válassz egy színt, és az átmeneteit (pont, ahogy a képen vannak) másold át a legfelső sorba |
|     3 | <imgr ../img/decolor3.png>adj hozzá tetszőlegesen más színátmeneteket                                                 |
|     4 | futtasd a `sprsheet -p paletta.png lpc2tngbase.csv kimenet.png bemenet.png` parancsot, hogy átszínezhető sablont kapj |

Ezt a `paletta.png`-t csak egyszer kell megcsinálni, aztán használható akárhány sablon színtelenítésére. Például, csinálj egy
palettát a hajszínekkel (barna, szőke, rózsaszín, narancssárga stb.), úgy, hogy a barna árnyalatok legyenek legfelül.
Ezután csakis a barna hajú szprájtlapokra lesz szükséged az LPCből (hosszú, rövid, göndör, kócos stb.), és ezeket elég egyszer
átkonvertálni, miközben mindhez egyszerre hozzáadhatod az átszínezés lehetőségét is.

Bizonyos esetekben a szprájtok eltérő palettát használnak sajnos (például a barna ing egy bizonyos barna átmenetet, míg a
barna gatya egy másik barna átmenetet), ilyenkor használható a `sprsheet -r csere.png` a színek cseréjéhez. Ehhez a
`csere.png`-nek legalább két sort kell tartalmaznia, de lehet több is, annyi, ahányféle árnyalat alőfordul. Az első sorban
van az "amivé" paletta, a többi sor az "amiről" paletta.
