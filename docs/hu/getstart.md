Indulás
=======

Telepítés
---------

NOTE: Az alap kellékek külön kerülnek terjesztésre a GPL licenszgondok elkerülése miatt. A TirNanoG Alap sablont
[innen](https://tirnanog.codeberg.page#templates) tudod letölteni.

Löltsd le az operációs rendszerednek megfelelő binárist a [repó](https://codeberg.org/tirnanog/editor/src/branch/binaries)ból.

<h3 inst_windows>Windows</h3>

1. [tnge-i686-win-static.zip](https://codeberg.org/tirnanog/editor/raw/binaries/tnge-i686-win-static.zip) letöltése
2. csomagold ki a `C:\Program Files` mappába és kész!

Ez egy hordozható futtható, semmilyen teleptés sem szükséges.

<h3 inst_linux>Linux</h3>

1. [tnge-x86_64-linux-static.tgz](https://codeberg.org/tirnanog/editor/raw/binaries/tnge-x86_64-linux-static.tgz) letöltése
2. csomagold ki az `/usr` mappába és kész!

Alternatívaként letöltheted a [deb](https://codeberg.org/tirnanog/editor/raw/binaries/tnge_1.0rc-amd64.deb) változatot is, amit
aztán a következő paranccsal telepíthetsz
```
sudo dpkg -i tnge_*.deb
```

Futtatás
--------

A szerkesztő bármikor indítható a `tnge` paranccsal. A csomagban találsz egy *.desktop* fájlt, ami az Alkalmazások között is
megjeleníti (Windowshoz egy hasonszőrű *.lnk* található).

HINT: Csak egyszer futtasd a `tnge` parancsot, a kényelmed érdekében hozzá fogja adni magát a menühöz.

A szerkesztő egy `~/TirNanoG` mappát fog létrehozni a saját könyvtáradban, és itt tárol minden
[játék projekt](https://codeberg.org/tirnanog/editor/src/branch/main/docs/proj_format.md)et. Ha ez valamiért nem lenne megfelelő,
akkor a könyvtár parancssori paraméterrel megadható.

Magától detektálja a géped nyelvét, és ha lehetséges, akkor a szerkesztő a saját nyelveden fog köszönteni. Ha szeretnéd, akkor
a nyelv direktben is beállítható parancssorból, például `tnge -L ru` vagy `tnge -L ja` (Linuxon) illetve `tnge /L ru`
vagy `tnge /L ja` (Windowson).

<dl>
<dt>Windows</dt><dd>Jobbklikk az <i>.lnk</i> fájlon, majd a felugró menüben válaszd ki a "Tulajdonságok"-at. A "Cél" mezőben
megadhatod a parancssori opciókat, például:
<imgc ../img/winlang.png><fig>Parancssori opciók megadása Windowson</fig></dd>
<dt>Linux</dt><dd>Használd a kedvenc terminálod, vagy szerkeszd az <tt>/usr/share/applications/TirNanoG.desktop</tt> fájlt.<pre>
[Desktop Entry]
Version=1.0
Type=Application
Name=TirNanoG Editor
Comment=Create TirNanoG games
Exec=/usr/bin/tnge <hl>-L hu</hl>
Icon=tnge
Terminal=false
StartupNotify=false
Categories=Development;
</pre></dd></dl>

### Parancssori Opciók

Windows alatt `-` helyett `/` karakterrel kezdődnek a kapcsolók (mert ez a Windows módi, például `/t`, `/vv`),
ezt leszámítva minden kapcsoló és opció ugyanaz.

```
tnge [-L <xx> ] [-t <theme> ] [-g <gameid> ] [-c <gameid> ] [-v|-vv] [projdir]
tnge [-p|-u|-l] <atlas>
```

Van öt, nem grafikus, csakis parancssori működési módja: `-g`, `-c`, `-p`, `-u` és `-l`. Ezek a módok nem nyitnak GUI ablakot,
és nem is kell nekik grafikus környezet vagy bármilyen telepített függvénykönyvtár, kizárólag parancssorban működnek.

| Opció         | Leírás      |
|---------------|-------------|
| `-L <xx>`     | A kapcsoló paramétere "en", "es", "de", "fr" stb. lehet. A megadásával egy adott szótárat használ a szerkesztő, és nem detektálja a nyelvet. Ha nincs a megadott szótár, akkor angolra vált. Ez egyben beállítja a játék alapnyelvét is. |
| `-t <theme>`  | A szerkesztő ablakához tölt be egy színsémát GIMP Palette fájlból. Nincs kihatással a készített játékra. |
| `-g <gameid>` | Generál egy új, mindig egyedi végfelhasználói licenszkulcsot (kikódoló kulcsot) és kiírja a szabványos kimenetre. Kell hozzá a `~/TirNanoG/(gameid)/license.txt` fájl. |
| `-c <gameid>` | Betölti a játékot [TirNanoG Projekt](https://codeberg.org/tirnanog/editor/src/branch/main/docs/proj_format.md) formátumban, és csinál belőle egy terjesztésre alkalmas fájlt [TirNanoG Fájl Formátum](https://codeberg.org/tirnanog/editor/src/branch/main/docs/tng_format.md)ban. Az eredmény `~/TirNanoG/(gameid)/game.tng.` néven kerül lementésre. |
| `-v, -vv`     | Szószátyár mód. `tnge` részletes infókat fog kiírni a szabvány kimenetre, ezért ez a kapcsoló terminálból használatos. |
| `[projdir]`   | Felülbírálja az alapértelmezett projekt mappát, ahol a *gameid*-t (játék azonosító) keresi. Ha nincs megadva, az alapértelmezett mappa a `~/TirNanoG`. |
| `-p <atlas>`  | Minden, `(atlas)`-al kezdődő PNG fájlt becsomagol egy `(atlas)_atls.png` nevű atlaszba. |
| `-u <atlas>`  | Kicsomagolja a szprájtokat az `(atlas)_atls.png` nevű atlaszból több PNG fájlba. |
| `-l <atlas>`  | Kilistázza, hogy mely szprájtok találhatók az `(atlas)_atls.png` atlaszban. |

Bővebb infót és tippeket a `-g` kapcsoló használatáról a [Játék értékesítése] fejezetben találsz.
A `-p`, `-u` és `-l` kapcsolók használatával kapcsolatban lásd az [atlaszok kezelése] fejezetet.

Sablon
------

Egy játék készítése a nulláról úgy, hogy mindent magad csinálsz eléggé macerás és időigényes. Hogy ezt elkerüld, használhatók
[sablon zip](https://tirnanog.codeberg.page#templates) fájlok. Ezeket csak bemásolod a `~/TirNanoG` mappába a projektek mellé,
nem kell kicsomagolni, azt majd a szerkesztő megteszi.

<imgc ../img/winfolder.png><fig>Egy letöltött sablon és helye a mappákban</fig>

A sablonok [cél]ja az, hogy könnyedén elkezdhess játékokat csinálni. Ezekben előre konfigurált szprájtok, animációk, térképelemek,
objektumok, NJK definíciók stb. találhatók. Ha a TirNanoG Alap sablonnal indítasz, akkor a TirNanoG Szerkesztő használata nagyon
hasonló az RPG Maker-hez (de remélhetőleg a felülete jóval egyszerűbb és többet is tud). A szerkesztővel létre is hozhatók ezek
a sablonok egyetlen kattintással, nincs szükség harmadik fél zip programjára.

NOTE: Nem minden sablon tartalmaz mindent egy játékhoz. Vannak kissebb, moduloknak hívott sablonok is, amik egy adott funkcióhoz
tartalmaznak csak kellékeket. Ezeket a későbbiekben a [Sablon importálása] menüpontban tudod beimportálni.

Felület áttekintése
===================

Az ablak 5 vízszintes, és középen két függőlegesen kettévágott területre osztható fel:
<imgw ../img/interface.png>

### 1. Menüsor

Ebben a szokásos menüpontok találhatók, és még két gomb: a <imgt ../img/helpbtn.png> Súgó gomb, ami ezt a kézikönyvet nyitja meg
(elérhető a <kbd>F1</kbd> gyorsgombbal is); valamint a <imgt ../img/playbtn.png> Lejátszás gomb, ami elmenti a játékod aztán
futtatja (a gyorsgombja a <kbd>Ctrl</kbd> + <kbd>R</kbd>).

A menük elérhetők az <kbd>Alt</kbd> lenyomásával majd elngedésével is. A "Kilépés"-t választani az első menüből pont ugyanaz,
mint leütni az <kbd>Alt</kbd> + <kbd>Q</kbd> kombinációt.

### 2. Eszköztár

Attól függően, hogy melyik oldalon tartózkodsz, az eszköztár hiányozhat. De általában mindig megtalálható rajta a szerkesztett
entitás technikai neve jobbra.

### 3. Entitásválasztó

Attól függően, hogy melyik oldalon tartózkodsz, az entitásválasztó hiányozhat. Ez az, ahol kiválasztod, hogy melyik entitást
akarod szerkeszteni. A fenti ábrán, ami a Térképszerkesztőt ábrázolja, ez az entitásválasztó a térképeket listázza. Ha az
Objektumok oldalról készült volna az ábra, akkor az objektumokat listázná.

### 4. Fő szerkesztési terület

Ez az, ahol a kijelölt entitást szerkesztheted. A kinézete nagyban függ attól, hogy épp melyik oldalon tartózkodsz.

### 5. Funkciósor

Ha van entitásválasztó, akkor tipikusan itt található a <imgt ../img/del.png> Törlés gomb, ami a kijelölt entitást törli;
a fő szerkesztési területet kiüresítő <imgt ../img/erase.png> Törlés gomb; és néha (már ahol van értelme), akkor az
<imgt ../img/preview.png> Előnézet gomb; és végül de nem utolsó sorban a valószínűleg legeslegfontosabb gomb, az
<ui1>Elment</ui1>, ami az entitáson végzett módosításaidat menti el.

### 6. Státuszsor

Itt jelennek meg a státuszüzenetek. Infókat ad azon dolgokról, ami felett az egeret viszed, de itt jelennek meg a hibaüzenetek is.
