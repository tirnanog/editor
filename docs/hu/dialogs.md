<h1 tab32 dialogusok>Dialógusok</h1>

Dialógusablak megadása
----------------------

Válaszd a `Játék` > `Dialógusok` menüpontot. Balra, az entitásválasztóban látszanak a már felkonfigurált párbeszédablakok. Duplán
kattintva egy sorra az adott dialógus betöltésre kerül.

Az eszköztáron jobbra található a dialógus belsős, technikai neve, egyébként az eszköztár itt üres.

Ki mondta és mit
----------------

A cím lefordítható és megjelenik a játékban. Az NJK nevét tartalmazza, aki a dialógus folytatja, és opcionális.

Az üzenet szintén lefordítható, és kötelező. Nem lehet benne újsor karakter, a tördelés automatikusan történik, mivel minden
lejátszó felbontása más-más lehet, és emiatt a dialógusablak mérete is változó. Másrészről lehet benne `{}`, ami a játékos
nevére cserélődik le, illetve `{attribútum}` hivatkozás, ami meg az adott attribútum értékére.

Ha a játéknak vannak szinkronszínészei és beszédfelvételei, akkor megadható, melyik tartalmaza kimondva az üzenetet.

Jobbra van egy gomb a portré kiválasztásához. Ennek függetlennek kell lennie az NJK-tól, mert nemcsak NJK nyithat dialógusablakot.
Például egy falon lévő felirat is megjeleníthet dialógust, lefordított szöveggel (ekkor nincs szükség portréra, vagy a portré
lehet egy tábla). Másodszor, az NJK-knak lehet különféle arckifejezésük, és mindig a szöveghez illőt kell tudni kiválasztani
(úgy mint vidám, mérges, stb.).

Lehetséges válaszok
-------------------

Nem kell megadni válaszokat, de lehet. Ha megadsz, akkor annyi opciót adhatsz meg, amennyit csak akarsz, de tartsd észben, hogy
a jó játék élményért 4 vagy 5-nél több nem javasolt. 8-nál több egészen biztosan összezavarja a játékosokat és elrontja a
játék élvezetét.

Ha olyan játékot készítesz, ahol a játékosok nem szabhatják személyre a karakterüket (lásd [előredefiniált hősök]), akkor
megadhatsz beszéd audió fájlokat is a válaszokhoz. Személyreszabható karakterek esetén ezt nem javaslom, mert furcsán
(vagy inkább viccesen) veszi ki magát, amikor egy brutális ork harcos kislányhangon szólal meg.

Továbbá a válaszok lehetnek feltételesek, és befolyásolhatják az attribútumokat is. Például, egy bizonyos válaszlehetőség
csak akkor elérhető, ha a játékos egy tünde.

Használata eseménykezelőkben
----------------------------

A dialógusokat eseménykezelőkből lehet megjeleníteni, a "Dialógus" parancs segítségével. Hogy több lehetőség legyen, mint csak
attribútumokat állítani, ezért visszaadja a kiválaszott válasz sorszámát az `a` helyi változóban, fordított sorrendben
(tehát az első válasz = válaszok száma - 1, utolsó válasz = 0). <imgr ../img/dlgcmd.png>Ha egy elágazást raksz röktön egyből
a "Dialógus" parancs után, aminek a kifejezése `a`, akkor választól függően mást csinálhatsz. Az első ág akkor fog lefutni,
amikor az első választ adták, a második ág, amikor másodikat, és így tovább, az utolsó amikor az utolsót.

Például

```
+---------------------+
| () dialog1    00:01 |
+---------------------+
+-----------------------------------+
| /\ [+][-] a                       |
|     2     |     1     |     0     |
+-----------+-----------+-----------+
| teendők   | teendők   | teendők   |
| az első   | a második | az utolsó |
| válasz    | válasz    | válasz    |
| esetén    | esetén    | esetén    |
+-----------+-----------+-----------+
```

Dialógus előnézete
------------------

Hogy szemrevételezd, hogy fog kinézni a dialógus játékon belül, kattints az <imgt ../img/preview.png> "Előnézet" gombra.
Ha nem tetszik a betűtípus, akkor azt beállíthatod a `Felület` > `Elemek` menüpontban, lásd [UI Elemek].

Amikor elégedett vagy a kinézettel, ne felejts el az <ui1>Elment</ui1> gombra kattintani.
