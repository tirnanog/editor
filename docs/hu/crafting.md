<h1 tab34 osszeszereles>Összeszerelés</h1>

Összeszerelő szabályok megadása
-------------------------------

Válaszd a `Játék` > `Összeszerelés` menüpontot. Balra, az entitásválasztóban látszanak a már felkonfigurált összeszerelőablakok.
Duplán kattintva egy sorra az adott összeszerelőablak betöltésre kerül.

Az eszköztáron jobbra található az összeszerelő dialógus belsős, technikai neve, egyébként az eszköztár itt üres.

Összeszerelő dialógus
---------------------

Nagyon hasonló a sima [dialógusok]hoz, az összeszerelő dialógusok is eseménykezelőkből jeleníthetők meg, az "Összeszerelés"
parancs segítségével.

A cím kötelező, lefordítható, és a játékon belül megjelenik. Ez lehet az NJK neve, aki az összeszerelést végzi (lehet egy
kovács vagy akár tűzhely is).

Az üzenet lefordítható, és opcionális. Nem lehet benne újsor karakter, a tördelés automatikusan történik, mivel minden
lejátszó felbontása más-más lehet, és emiatt a dialógusablak mérete is változó. Másrészről lehet benne `{}`, ami a játékos
nevére cserélődik le, illetve `{attribútum}` hivatkozás, ami meg az adott attribútum értékére. Ne használj hosszú szöveget,
igyekezz minnél tömörebben összefoglalni, hogy ebben a dialógusablakban mi szerelhető össze.

Ha szeretnéd, hogy egy hangeffekt lejátszódjon, amikor az összeszerelés elkészült, azt is itt lehet megadni.

Van továbbá egy "sorrendfüggetlen" pipa. Alapból a hozzávalók elhelyezése lényeges, és csak akkor történik meg az összeszerelés,
ha a játékos pontosan oda helyezi el az elemeket, mint ahogy itt a receptben meg lett adva. Ha ezt bepipálod, akkor a játékos
bármilyen sorrendben elhelyezheti az összetevőket.

Jobbra van egy gomb a portré kiválasztásához. Ennek függetlennek kell lennie az NJK-tól, mert nemcsak NJK nyithat dialógusablakot.
Például egy sütőnek is lehet összeszerelő dialógusablaka.

Receptek
--------

Minden összeszerelő dialógusnak kell hogy legyen legalább egy, de lehet több receptje is. Ezek írják le az összetevőket és
a végeredményt.

INFO: A szükséges mennyiség lehet 0 is. Ez azt jelenti, hogy amikor az összeszerelés kész, a tárgy nem lesz eltávolítva a
játékos leltárából, mégis szükség van rá az összeszereléshez (például egy bogrács).

Összeszerelő dialógus előnézete
-------------------------------

Hogy szemrevételezd, hogy fog kinézni a dialógus játékon belül, kattints az <imgt ../img/preview.png> "Előnézet" gombra.
Ha nem tetszik a betűtípus, akkor azt beállíthatod a `Felület` > `Elemek` menüpontban, lásd [UI Elemek].

Amikor elégedett vagy a kinézettel, ne felejts el az <ui1>Elment</ui1> gombra kattintani.
