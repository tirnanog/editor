<h1 tab23 stablista>Stáblista</h1>

Készítők és hozzájárulók megadása
---------------------------------

Válaszd a `Felület` > `Stáblista` menüpontot. Egy nagy űrlap az összes opcióval.

Ez az oldal nagyon hasonlít a [főmenü]re: megadható a háttérzene, háttérkép, a betűtípus stílusa, mérete, színe, stb.

Akárcsak a többi felület menüpont esetében, itt is van alul <imgt ../img/preview.png> "Előnézet" gomb.

<h2 credit_bg>Háttér</h2>

A háttérzene és háttérkép megadása.

Fejléc
------

A kategória betűtípusát itt lehet beállítani.

<h2 credit_font>Szöveg</h2>

A normál betűtípus a készítők neveihez használatos.

Kategória
---------

Kiválasztja, hogy melyik készítők találhatók a megadott kategóriában.

Készítők
--------

Amikor kiválasztasz egy nevet, a <imgt ../img/erase.png> "Törlés" gomb aktívvá válik, és a név törölhető lesz. Ha beírtál egy
nevet a mezőbe, kattints a <imgt ../img/add.png> "Hozzáad" gombra, és a készítő neve ábécé sorrendben hozzáadódik a listához.
