<h1 tab27 attributumok>Attribútumok</h1>

Attribútumok konfigurálása
--------------------------

Válaszd a `Játék` > `Attribútumok` menüpontot. Balra, az entitásválasztóban látszanak a már felkonfigurált attribútumok. Duplán
kattintva egy sorra az adott attribútum betöltésre kerül.

Az attribútumok a játékod alapvető építőkockái, amik állapotot írnak le. Tartozhatnak karakterhez, NJKhoz, tárgyakhoz, vagy épp
a világhoz.

Attribútumok beállításai
------------------------

Az <imgt ../img/setup.png> "Attribútumok beállítása" gombra kattintva alul középen, meg fog jelenni az űrlap. Itt lehet a motorhoz
rendelni az egyes attribútumokat. Csak pár van, mint például melyik attribútum tárolja a sebességet vagy a fényerőt, vagy épp
a szintlépéskor elkölthető pontokat.

Ugyancsak itt lehet a pontelosztásos mód határértékeit beállítani. Bővebb infót erről a [pontelosztásos karakterek] címszó alatt
találsz.

Attribútum tulajdonságok
------------------------

Ami a nem beállítási módot illeti, ilyenkor az attribútumokat konfigurálhatod itt. Minden attribútumnak van egy belsős technikai
neve, egy lefordítható címe, típusa és néhány egyéb infója. Ha a cím nincs megadva, akkor az attribútum rejtve lesz, és nem
fog megjelenni a játékon belül a leltár "Képességek" fülén. Az attribútumok a képernyőadatok között is megjeleníthetők
a [kijelzők] segítségével. Típus szerint lehet elsődleges, számolt, változtatható vagy akár globális.

<h3 attr_pri>Elsődleges</h3>

Az elsődleges attribútumok azok, amik az összes többi karakter (vagy NJK) attribútumát befolyásolják. Pontelosztásos módszernél
a játékos szabadon oszthatja el a pontokat ezek között. Karaktergenráláskor kapnak értéket, és a későbbiekben nem módosulnak,
vagy csak speciális eseményekkor, mint például a szintlépés. Tipikusan "Erő", "Intelligencia" stb.

<h3 attr_var>Változó</h3>

Ezek a legtipikusabb fajta attribútumok, amikre a játék logikája úton útfélen hivatkozik. Az eseménykezelők direktben állíthatják
az értékeiket az <imgt ../img/let.png> "értékadás" paranccsal, és van saját "változáskor" eseménykezelőjük is.

Tipikusan a játékos játék közben elsajátított képességei vannak változó attribútumokban tárolva, mint például "kardozás",
"zárnyitás", stb. De ez teljesen rajtad áll, hogy milyen attribútumokat alakítasz ki a játékodban.

<h3 attr_calc>Számolt</h3>

Ezek értékét egy képlet határozza meg, ami mindig úgy számítódik, emiatt direktben nem állíthatók, csak indirekten, a képletben
szereplő további attribútumok állításával. Tipikusan "Életerő", "ÉleterőMax", "Mana", "ManaMax" stb., olyanok, amik az elsődleges
attribútumoktól függenek.

<h3 attr_glob>Globális változó</h3>

Akárcsak a sima változó attribútumnál, van egy alapértelmezett értékük, direktben állíthatók az <imgt ../img/let.png> "értékadás"
paranccsal és van saját eseménykezelőjük. Az egyetlen különség az, hogy amíg a sima változó attribútumok karakterhez (vagy NJKhoz,
tárgyhoz) kapcsolódnak, addig a globális változó attrtibútumok a világhoz. Az általános, szereplőfüggetlen történetszál tárolására
valók.

Tipikusan olyan dolgok, hogy a kastély kapuja nyitva van-e, a király kiküldte-e a lovagokat már vagy sem, hány nap van vissza,
míg a királylány éhenhal a toronyban stb.

<h3 attr_glob_calc>Globális számolt</h3>

Hasonló a karakterenkénti számolthoz, csak a világ attribútumokhoz való.
