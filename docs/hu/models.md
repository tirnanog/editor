Modellek
========

Szprájtok importálása 3D modellekről
------------------------------------

Válaszd a `Kellékek` > `Szprájtok` menüpontot. Látni fogod a rendelkezésre álló szprájtok listáját. Kattints a <imgt ../img/add.png>
"Hozzáad" gombra a jobb alsó sarokban.

A képernyő meg fog változni, és a szprájtlap specifikációs ablak fog megjelenni. Itt kattints a <imgt ../img/load.png>
"Szprájtlap betöltése" ikonra a bal felső sarokban (az eszköztár legelső ikonja, de a funkciósávban is megtalálható a könnyebb
elérés kedvéért).

Modell betöltése
----------------

Többféle formátum is támogatott, és a szükséges lépések kicsit eltérnek attól függően, hogy mi mindent tárol egy bizonyos modell.
Amennyiben 2D-s képekről szeretnél importálni, lásd a [szprájtok] fejezetet.

A fájlválasztóban válassz egy 3D-s modell fájlt. A bal alsó sarokban, add meg a raszterizálás méretét (a szélesség és a magasság
ugyanaz lesz), állítsd be a másodpercenkénti képkockák számát, és hogy melyik irányba néz a modell (Model 3D fájlok esetében ez
mindig "dél"). Ha bepipálod a "Manuális" dobozt, akkor kézzel kiválaszhatod egy idővonalról, hogy az egyes akciók mettől meddig
tartanak (lásd alább).
<imgc ../img/impm3d.png>

NOTE: Filmek esetében másodpercenként 25 - 30 képkocka van, de az animált szprájtok esetében **NEM**. Egy tipikus animált
szprájt általában 3-5 képkockával rendelkezik (egy másodpercre vetítve ez 12 FPS), talán 10-el, ha az animáció nagyon
folyamatos.

Ha kiválasztottad a fájlt és megadtad a paramétereket, kattints a <ui1>Betölt</ui1> gombra.

### Model 3D Formátum (.m3d)

Ez a legépkézlábabb formátum, és fullosan támogatva van. Ha ezt választod, akkor nem marad más teendő, mindent elintéz helyetted
a szerkesztő. Vissza leszel irányítva a szprájtlap specifikációs oldalra, ahol egy teljesen kitöltött kijelölés lista fog fogadni.

Bármilyen 3D-s formátum átkonvertálható [Model 3D](https://bztsrc.gitlab.io/model3d) formátummá, és van
[Blender export plugin](https://gitlab.com/bztsrc/model3d/tree/master/blender) is hozzá.

### Wavefront Object (.obj)

Ezek csak statikus (nem animált) objektumokat tudnak tárolni, de nagyon széles körben elterjedt, és a modell szerkesztők által
általánosan támogatott formátum. Ismételten, ha ezt választod, nem marad más teendő, mivel csak egy szprájt lesz importálva
mind a 8 irányban, animációk nélkül.

### Egyéb formátum

Ehhez telepíteni kell az [Assimp](http://assimp.org) könyvtárat. Linux alatt használd a disztród csomagkezelőjét, például
`apt-get install libassimp`. Windowson sok sikert az előre lefordított DLL megtalálásához, amiről a
[telepítés](https://assimp-docs.readthedocs.io/en/latest/about/installation.html) oldal beszél. De ha megtaláltad, akkor
másold be `assimp32.dll` néven a `C:\Program Files\TirNanoG` mappába (a `tnge.exe` mellé).

Ha megtalálta a könyvtárat, akkor különböző formátumú 3D-s modellek (.3ds, .blend, .glTF, .b3d, .collada, .dae, .fbx, .x3d, stb.)
csak úgy maguktól meg fognak jelenni a szerkesztő fájlválasztó ablakában.

WARNING: Nincs támogatás az Assimpra. Ha problémába ütközöl, próbálj meg segítséget kérni a fórumjain.

<h2 model_spec_sprites>Szprájtok megadása</h2>

A modell tárol animációt, de nincs benne akció specifikáció, vagy bepipáltad a "Manuális" jelölőt, akkor egy idővonalról ki
kell választanod, hogy melyik akció mettől meddig tart.

<imgw ../img/sprtml.png>

1. forrás, animált modell
2. idővonal képkockákkal
3. az "Új szprájt" hozzáadása gomb

Akárcsak a 2D-s szprájtlapok esetében, a forrást balra találod (a nagy terület), a kijelölt szprájtokat pedig jobbra (import lista).
Azonban itt egy nagy modellt látsz, és idővonalat alatta szprájtlap helyett. Nem képeket kell kijelölni 3D-s modellek esetében,
hanem képkocka intervallumokat az idővonalon, egyet egyet minden akcióhoz. Amint megvan a kijelölés, nyomd le az "Új szprájt"
gombot az importlista legfelső eleménél, és a szrpájtok kigenerálódnak mind a 8 irányban.

HINT: A kijelölést az idővonalon mozgathatod a <kbd>⯇</kbd> balra és a <kbd>⯈</kbd> jobbra kurzor gombokkal is.

<h2 model_do_import>A tényleges importálás</h2>

Ha elégedett vagy a kijelölt szprájtok listájával jobboldalt, nevet kell adni a szprájtgyűjteménynek a lista fölött. Név nélküli
szprájtok "_xxx" utótagot kapnak, ahol az "xxx" egy szigorűan monoton növekedő sorszám, 001-től 999-ig.

Szóval megvan a kijelölt lista, adtál nevet a listának (a lista fölötti mezőben), valamint az egyes szprájtoknak is (minden egyes
listaelem fölött), leellenőrizted, hogy megfelelő a kiválasztott kategória (ikonok alul középen), akkor rányomhatsz az
<ui1>Importálás</ui1> gombra. Ezt követően visszamehetsz a szprájtok oldalra, vagy akár betölthetsz egy újabb szprájtlapot
és további szprájtokat importálhatsz.
