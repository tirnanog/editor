<h1 tab29 karakter>Karakter</h1>

Karakteropciók beállítása
-------------------------

Válaszd a `Játék` > `Karakter` menüpontot. Balra, az entitásválasztóban látszanak a már felkonfigurált opciócsoportok. Duplán
kattintva egy sorra az adott opciócsoport betöltésre kerül.

Nincsenek kőbevésett szabályok arra vonatkozóan, hogy hogyan állítsd be ezeket, mert ez főként attól függ, milyen játékot
szeretnél csinálni. A TirNanoG motor szabad kezet ad ebben. Példákért lásd még az [előredefiniált hősök], [karakterkasztok]
és a [pontelosztásos karakterek] fejezeteket.

A karakteropciók csoportosítva vannak. Például a barna haj és a szőke haj opciók, és valószínűleg mindkettő a haj csoportba
tartozik. Ezt elsőre bonyolult lehet felfogni, amikor a játékos egy karaktert generál, akkor különböző opciókból választhatnak,
minden egyes csoportból pontosan egy opciót. Nehéz jobban elmagyarázni, mert a motor teljesen szabad kezet ad. Legyen mondjuk
az egyik opciócsoport a "faj", olyan opciókkal, mint "ember", "tünde", "ork" stb. Egy másik csoport legyen a "kaszt", olyan
opciókkal, mint "harcos", "tolvaj", "varázsló" stb. Az első csoport szprájtokat rendel a karakterhez, míg a második attribútum
értékeket. Remélhetőleg így érthetőbb.

Ez sokkal bonyoltultabbá válik, mivel a motor megengedi, hogy több csoport is kihatással legyen a kinézetre. Ha ezt választod,
akkor az egyik opciócsoportot meg kell jelölni, mint "alap kinézet". Például az egyik opciócsoport lehet "test", olyan opciókkal,
mint "ember férfi", "ember nő", "tünde férfi", "tünde nő", stb. Ezek határozzák meg az alap szprájtot. Aztán lehet további
csoport például a "haj". Ebben az esetben a játékos karaktergeneráláskor egymástól függetlenül választhat alap kinézetet, azaz
testet valamint hajat.

NOTE: Amikor az "alap kinézet" be van pipálva, akkor azt is meg kell adni, hogy hol található az alapvonal (ami a talaj szintje
a szprájt függőleges közepéhez képest). Az alapvonal általában függőleges középen van, de le lehet vinni, hogy pont a karakter
lábfeje alatt legyen. Ezt fontos beállítani, ha túlméretezett szprájtjaink is vannak.

Opciócsoport
------------

Az opciócsoport belsős technikai nevét, amit a szerkesztőn belül használsz a jobb felső sarokban lehet megadni.

Opciók
------

A lefordítható névvel rendelkező opciók a középső listába kerülnek. Minden opció adhat hozzá attribútumokat, leltári tárgyakat
vagy szprájt rétegeket a karakterhez.

### Opcióattribútumok

Kétféle attribútumot lehet hozzáadni, elvártat és beállítandót. Ha a reláció "(állít)", akkor beállítandó, bármilyen más
reláció esetén elvárt. Ezt is könnyebb egy példán keresztül elmagyarázni. Legyen egy csoport, mint az előbb, "faj", olyan
opciókkal, mint "ember férfi", "ember nő". Az elsőhöz hozzárendeljük a "nem" attribútumot 1-es értékkel, a másodikhoz 2-es
értékkel. Ezután, csinálunk egy másik csoportot, "foglalkozas" néven. Ebben az első opció a "kovács", aminél az elvárt
"nem" attribútum 1, míg a második opció a "pincérnő", 2-es elvárt értékkel. Ezt követően a játékos karaktergeneráláskor
választhat "ember férfi" és "ember nő" között. Ettől a választástól függően, más más lehetőségeket kínál fel foglalkozásra:
"ember férfi" esetén választhatja a "kovács"-ot, de a "pincérnő"-t nem, illetve ha az előbb az "ember nő"-t válaszották, akkor
"kovács"-ot nem és a "pincérnő"-t igen. Az elvárt attribútumnak akkor is lehet jelentősége, ha különböző szprájtokra gondolunk,
például az "ember férfi" esetén más "haj" opciók közül választhat a játékos, mint "ember nő" esetén.

### Opcióleltár

Az opciónál megadott leltári tárgyak a játékos induló leltárába kerülnek, ha meg vannak adva. Például a "kaszt" csoportnál,
ha az opció "harcos", akkor kardot adhat hozzá, míg ha "varázsló", akkor meg egy varázspálca kerülhet az induló leltárba.

### Opciószprájtok

Általában a szprájtlap kulcsrakész karaktereket ábrázol. De az is lehet, hogy a szprájtok modulárisak: csak a karakter bizonyos
részeit ábrázolják (úgy mint egy szrpájtcsoport a testet, egy másik az arcot, egy harmadik a füleket stb.) Ez utóbbi esetben,
több szprájt réteg is megadható. Minden rétegen egy adott típusú szprájt szerepel: például az első rétegen a test, a másodikon
a haj, stb. Ha kulcsrakész karaktereket ábrázoló szprájtjaid vannak, akkor csak egyetlen egy rétegre lesz szükséged.

Ha olyan szprájtjaid vannak, amin speciális szürkeárnyalatos színek találhatók (`#101010`-tól `#f0f0f0`-ig), akkor megadhatsz
egy külön, 8 x 16 pixeles paletta szprájtot is az eszköztáron. Ezáltal a játékos karaktergeneráláskor különböző színvarációk
közül is választhat (bővebb infóért lásd az [átszinezés] fejezetet). Ilyen paletta szprájtra akkor tehetsz szert, ha a
[szprájtok]at a "?" (rákérdez) színvariánssal importáltad.

Egy rétegen belül minden cselekvéshez meg lehet adni szprájtot. Ez eléggé megterhelő lehet elsőre, de a szprájtválasztó sokat
segít benne: kereshetsz a névre, és ez a szűrő megmarad. Például, kattints a "sétálás" oszlop gombjára. A felugró ablakban keress
a "tündé"-re. A listában ekkor csak azok a szprájtok jelennek meg, amik tartalmazzák a "tünde" szót a nevükben. Válaszd ki a
szrpájtot. Most ugyanabban a sorban, kattints egy másik cselekvés gombjára, mondjuk az "úszás"-ra. A szprájtválasztó megint
felugrik, de már eleve csak tündéket ábrázoló szprájtokkal (a keresés mező törlésével újra láthatóvá válik minden szprájt).

Namost nincs szükség MINDEN cselekvésre. Ha nem tervezed használni mondjuk a repülést a játékodban, akkor azokat a szprájtokat
üresen hagyhatod. Ha az ütközési maszkjaidban sehol sem szerepel mászás, akkor nem lesz szükség mászás animációra sem. Kihagyhatod
a támadás hárítátását is, de valószínűleg szükséged lesz a sebződésre és a meghalásra. Csak azokat a cselekvéseket kell megadni,
amik szerepelnek így vagy úgy az eseménykezelőkben.

A szprájtmátrix alatt találsz kivágás-másolás-beillesztés gombokat. Ezekkel egy komplett réteget lehet az egyik opcióról a másikra
másolni (illetve ami azt illeti, más rétegekhez is, például az NJK-k szprájtrétegeihez). Van még további két gomg fel / le
nyilakkal, ezekkel a rétegek sorrendjét lehet átállítani.

Az <imgt ../img/preview.png> "Előnézet" gombra kattintva láthatod az éppen kiválaszott cselekvés animációit minden irányban. Ez
nagyon hasznos, mert ha nem adtál meg minden irányba szprájtot, akkor azt is megmutatja, hogy mely szprájtokkal fogja a hiányzó
irányokat helyettesíteni. Arra is kiváló, hogy lássuk, hogy illeszkednek a rétegek agymásra. Ha valamelyik szprájt túlméretezett
(nagyobb, mint az "alap kinézet" szprájtja), akkor azt is ellenőrizheted, hogy az alapvonalra megfelelően illeszkednek-e.

HINT: Az előnézet alapból a kiválasztott cselekvést mutatja. De az előnézeti módban lenyomhatod a <kbd>⯇</kbd> balra, illetve
<kbd>⯈</kbd> jobbra kurzor gombokat a választáshoz. Amikor a képkockák száma (vagy az animáció típusa) eltér valamelyik rétegen,
akkor egy villogó figyelmeztetés ikon fog megjelenni amellett az összeillesztett szprájt mellett.

### Opcióleírás

Végül, ha szeretnéd, megadhatsz egy lefordítható leírást is az opcióhoz.

Miután végeztél az opció felkonfigurálásával, ne felejtsd el megnyomni az <ui1>Elment</ui1> gombot.

Karakter példák
---------------

### Előredefiniált hősök

Nagyon sok játékban olyan történetszál található, ami megköveteli, hogy ne lehessen változtatni, mivel a főhős egy bizonyos
valaki (például Ríviai Geralt). Olyan játékok is vannak, amiknél a történetszálak több, előredefiniált karaktert igényelnek
(mint például Bernad, Laverne és Hoogie). Ebben a fejezetben arról lesz szó, hogy az ilyen eseteket hogy kell beállítani.

Először is, egyetlen egy opciócsoportra lesz csak szükség, nem többre.

Ehhez a csoporthoz adj hozzá pontosan annyi opciót, ahányféle karakter szerepelhet a játékban. Minden opciónál add meg mind
az attribútumokat, mind a szprájtokat (és valószínűleg leltári tárgyakat is). Nagyon valószínű, hogy ebben az esetben
kulcsrakész szprájtjaid vannak, ezért csak egy rétegre lesz szükség. De előfordulhat, hogy több moduláris szprájtrétegből
ollózod össze a főszerepelőket.

Arra is van lehetőség, hogy előredefiniálunk pontosan egy karaktert, de hagyjuk a játékost, hogy meghatározza az elsődleges
attribútumok értékeit. Ehhez ne add meg az opciónál az attribútumokat, csak a szprájtokat, és az [attribútumok beállításai]nál
adj meg elosztható pontokat. Bővebb infóért lásd a pontelosztásos karakterek fejezetet alább.

### Karakterkasztok

Egy tipikus RPG megengedi, hogy a játékos függetlenül válasszon karaktert és kasztot.

Ehhez legalább két opciócsoportra lesz szükséged. Az első csoportnál, hívjuk mondjuk "faj"-nak, add meg az alap kinézetet
szprájtokkal, de ne adj meg se attribútumokat, se leltári tárgyakat.

A második csoporthoz, hívjuk mondjuk az egyszerűség kedvéért "kaszt"-nak, olyan opciókat adj, amiknél van megadva attribútum
és esetleg tárgyak, de nincsenek szprájtok. Például a "harcos" esetén lehet az "Erő" magas, de alacsony "Intelligencia", míg
"varázsló" esetén az "Erő" az alacsonyabb, és az "Intelligencia" magasabb.

Persze ennél a második csoportnál is használhatsz szprájtokat, például a "varázsló" egy csúcsos varázslósapkát adhat a karakter
kinézetéhez.

Ezzel is használható a pontelosztásos módszer. Először is az [attribútumok beállításai]nál adj meg elosztható pontokat.
Aztán a második csoportnál, amit "kaszt"-nak hívtunk, adj meg *elvárt* attribútumokat, például a "harcos" esetén legyen az
"Erő" legalább 8, míg a "varázsló" esetén az "Intelligencia" legalább 8. Ekkor a játékos szabadon eloszthatja a pontjait az
elsődleges attribútumok között, de csak akkor választhatja az adott kasztot, ha a megadott attribútum nem túl alacsony. Ugyancsak
használható számított attribátum, lásd alább.

### Pontelosztásos karakterek

Válaszd a `Játék` > `Attribútumok` menüpontot és ott az [attribútumok] oldalon kattints az <imgt ../img/setup.png> "Attribútumok
beállításai" gombra. Azon az űrlapon, add meg a **Szabadon elosztható pontok** és a **Attribútumonkénti max.** mezőkben a pontok
számát.

Ha ezek meg vannak adva, akkor a játékos szabadon eloszthatja a pontokat az elsődleges attribútumok között, de egy attribútumra
nem rakhat többet, mint a megadott maximum.

Ekkor a karakter jellemző attribútumai valószínűleg számított attribútumok lesznek, amik ezektől az elsődleges attribútumoktól
függenek. Például, legyen három elsődleges attribútum ("Erő", "Intelligencia", "Kitartás"), mind 10-es maximum ponttal, és adjunk
összesen 21 pontot a játékosnak, hogy elossza eközött a három között, ahogy csak szeretné. Ezután az alap attribútumok lehetnek
számítottak, (például "Fizikális" = (Erő + Kitartás) / 2, "Mentális" = Intelligencia, "Támadás" = Erő, "Védekezés" = (Erő +
Intelligencia) / 2 stb.) Megjegyzem, ezek csak példák, bárhogyan felkonfigurálhatod az attribútumokat.

Ezután válaszd a `Játék` > `Karakter` menüpontot. A karakter opcióinál megadhatsz attribútumot, csak ne elsődlegest (hacsak
nem elvártként).
