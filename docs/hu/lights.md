Fények
======

Szórt fény
----------

Először is, minden egyes térképnél megadható a napfény, a `Térképek` > `Térkép meta információk` alatt, lásd [térképek].
Két dolog írja le:
- a nap hossza percekben
- és egy szprájt a színekkel

A szprájtnak N x 1 pixelesnek kell lennie. A fényréteg színét a szprájtról veszi a napszak modulo szprájt szélessége alapján.
Ha az a pixel teljesen átlátszó, vagy nincs megadva szprájt, akkor nem lesz fényréteg. Ha az a pixel teljesen fekete, akkor
az azt jelenti, hogy koromsötét van, és a fényréteg mindent ki fog takarni a játékmezőben. Valószínűleg sosem kell fekete
színt használni 75%-os alfánál nagyobb értékkel, hogy a játékosok legalább valamit lássanak fényforrás nélkül is.

![Szórt fény szprájt példa](../img/daylight.png)
A nap hosszát 20 percre állítva, és olyan szprájtot használva, ami 75%-os feketével indít (éjfél), majd átmegy 50%-os narancssárgába
(reggel), majd teljesen átlátszóba (nappal, délután), aztán megint 50%-os narancssárgába (este) és ismét 75%-os feketébe (éjjel),
akkor egy egyszerű, de nagyon hatásos effektet lehet elérni a napfény színének imitálásával és éjjeli sötétséggel.

A kazamatákban, ahová nem jut be a napfény, érdemes a nap hosszát 1 percre állítani, és egy olyan szprájtot megadni, amin egyetlen
75%-osan átlásszó fekete pixel van. Ezáltal minden sötét lesz, kivéve a fényforrások környékét.

Összeláncolt térképek esetén érdemes az összes térkép naphosszát és fény szprájtját ugyanarra beállítani, különben furcsa,
nemkívánt hatást fog eredményezni játékon belül.

Mozgó fényforrások
------------------

Az `Attribútumok` menüpontban, az [attribútumok beállításai] alatt lehet megadni, hogy melyik attribútum tárolja a fényerő
sugarát csempékben. Ezt követően a tárgyakoz meg lehet adni ezt az attribútumot értékkel az `Objektumok` > `Leltárban` >
`Attribútummódosítók` táblázatban (lásd az objektumok [leltárban] fülét). Amikor a játékos felszerel egy olyan tárgyat, aminél
az az attribútum meg van adva (vagy csak az övére tűzi), akkor a fényrétegen egy adott sugarú lyuk keletkezik, a játékossal
a középpontban.

Álló fényforrások
-----------------

Hasonlóan, a térképre is helyezhető olyan tárgy, aminél a fényerő attribútum meg lett adva, például egy fáklya a falon. Ezek
szintén lyukat ütnek a fényrétegre, egy kört a megadott sugárral, a tárggyal a középpontban.
