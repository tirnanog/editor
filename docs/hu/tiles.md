<h1 tab35 csempek>Csempék</h1>

Infók megadása csempékhez
-------------------------

Válaszd a `Játék` > `Csempék` menüpontot. Alapból a csempék csupán a térkép hátteréül szolgálnak. De adhatsz hozzájuk meta infót,
ezáltal tulajdonságokkal ruházhatod fel őket. Balra, az entitásválasztóban látszanak a már felkonfigurált infóval rendelkező
csempék. Duplán kattintva egy sorra az adott csempe infói betöltésre kerülnek. Ha egy olyan csempét szeretnél betölteni, aminek
még nincsenek infói, akkor használd a csempe szprájtválasztót az eszköztárban.

Az eszköztárban találod a csempeválasztót és a csempekombinálót.

Csempék kombinálása
-------------------

Előfordulhat, hogy egy beimportált csempe sarkot vagy szélet ábrázol, de nem egy másik háttércsempével együtt, hanem csak úgy
magában átlászó háttérrel. TirNanoG lehetőséget ad több rétegben elhelyezni a csempéket, így megoldható, hogy az egyik rétegre
rakod a háttér csempét, egy másikra pedig az átlátszó hátterűt, ugyanarra a koordinátára, de meglehet, hogy a rétegek száma
mégis kevés. Ebben az esetben összemontázsolhatod a csempéket egyetlen eggyé, amit aztán el tudsz helyezni egyetlen rétegre.

Például, az importált csempekészlet tartalmazhat egy tavat. A vízpart lehet egyedi (mint például víz fűvel, víz sziklával, víz
homokkal stb.), vagy lehet csak a víz átlátszó háttérrel. Ez utóbbihoz töltsd be a füves hátteret, válaszd ki vízpart csempét,
majd kattints a <imgt ../img/comb.png> "Másik csempével kombinálás" gombra. Erre kapni fogsz egy olyan fű és vízpart csempét,
mintha az már eleve az importáltak között szerepelt volna.

Csempe tulajdonságok
--------------------

Megadhatod, hogy mennyire gyorsan mozoghatnak a játékosok bizonyos csempéken. Például, jégen lehet a sebesség 110%, míg
sziklás hegyeken csak 50%. Ha a módosítót 100%-ra állítod, akkor nem lesz hatása. Jellemző, hogy az "út" típusú csempéknél
a sebességhez 150%-ot adnak meg, így a játékosok gyorsabban közlekedhetnek az utakon.

A csempéknek nincs eseménykezelőjük, de azért itt megadható pár hanghatás "sétáláskor", "úszáskor", és "repüléskor" eseménynél.
Ezek akkor lesznek lejátszva, ha a játékos az adott csempén az adott közlekedési módszerrel áthalad.

Automap szabályok
-----------------

A sarok és szél csempék lehelyezése fárasztó tud lenni. Hogy ebben segítsen, a TirNanoG támogatja az automapping-et (ami más,
mint az autotiling). Autotiling esetén a csempék 2 x 2-es részekre vannak felbontva. Ez csak akkor működik, ha a térkép
ortografikus, illetve kis hackeléssel izometrikus esetén, de hatszögletű csempékkel semmiképp sem. Automap esetén viszon
mintákat kell megadni, és ha valamelyik minta egyezik, akkor a csempét automatikusan lerakja. Ez a módszer működik mindféle
rács esetén. Több minta is hozzáadható a listához, ezért többféle szomszédos csempe kombinációja is megadható.
