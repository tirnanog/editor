Cél
===

Hogy könnyen és gyorsan elkezdhess játékokat csinálni, a TirNanoG a rendelkezésedre bocsát egy alap kelléktárat, a
[TirNanoG Alap](https://tirnanog.codeberg.page#templates) készletet.

NOTE: Más programokkal ellentétben nem kötelező egy kelléktárat használni ezzel a szerkesztővel. Ez csupán egy kényelmi
szolgáltatás, egy a számos lehetőség közül, a TirNanoG Szerkesztőben szabadon választhatsz [sablon]t.

A kézikönyv ezen része azzal foglalkozik, hogy hogyan lehet kibővíteni az alap készletet úgy, hogy az kompatíbilis maradjon
a már meglévő kellékekkel.

A TirNanoG Alap kellékei eredetileg a Liberated Pixel Cupra épültek, de időközben jelentősen eltértek attól. Legjellemzőbb
különbségek:

- Sztandardizált animációk
- Sztandardizált képkockaszám
- Sztandardizált fej és kéz pozíciók minden képkockán
- Sztandardizált szprájtlapok (lásd [elrendezések])
- [Iránymutató sémák](https://tirnanog.codeberg.page/guides.zip) sémák használata a kompatibilitás érdekében
- A **CC-by-sa-4.0** licensz kizárólagos használata (vagy tetszés szerint bármely későbbi verziója)

A régi LPC kellékek TirNanoG Alap formátumra történő könnyű átkonvertálásában segít az [sprsheet](https://codeberg.org/tirnanog/sprsheet)
konvertáló alkalmazás.

1. használd az sprsheet alkalmazást, hogy a régi kellékeket átkonvertált TirNanoG Alap elrendezésűre
2. töltsd be a kedvenc pixelszerkesztődbe mind az újjonnan konvertált kelléket, mind az iránymutató sémát
3. finomhangold a képkockákat, hogy azok pontosan illeszkedjenek az iránymutató vonalakra

Amikor új kelléket hozol létre, akkor használd az ebben a fejezetben található leírást és iránymutatást. A legfontosabb
rész az, hogy mindig figyelja arra, a képkockák jól illeszkednek-e az iránymutató vonalakra, mert ez garantálja, hogy
az új kellék kompatíbilis lesz-e a már meglévő kellékekkel.
