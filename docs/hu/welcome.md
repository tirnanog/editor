TirNanoG Szerkesztő Kézikönyv
=============================

Üdvözöl a [TirNanoG Szerkesztő](https://tirnanog.codeberg.page), a Szabad és Nyílt Forráskódú *Kaland és
Akció Szerepjáték* készítő kézikönyve.

Indulás
-------

- A Tirnanog Szerkesztő indítása, [telepítés] és [futtatás]
- [Új projekt] létrehozása
- [Játék importálása] már meglévő `game.tng` fájlból
- A [TirNanoG Projekt](https://codeberg.org/tirnanog/editor/src/branch/main/docs/proj_format.md) könyvtárstruktúrája (külső hivatkozás, angol nyelvű)

HINT: Ez a kézikönyv internet nélkül is használható. A <mbr> jobbklikk menüben válaszd a "Mentés mint" opciót, és mentsd le a
`C:\Program Files\TirNanoG\doc` (Windows) vagy `/usr/share/doc/tnge` (Linux) mappába. Ha a `manual_hu.html` megtalálható ezen
könyvtárakban a gépeden, akkor a szerkesztő azt fogja megnyitni, valahányszor a <imgt ../img/helpbtn.png> Súgó ikonra kattintasz
vagy leütöd az <kbd>F1</kbd> gombot. Egyébként az [onlájn kézikönyv](https://tirnanog.codeberg.page/manual_hu.html)et nyitja
meg.

Támogatott formátumok
---------------------

A szerkesztővel nemcsak játékokat készíthetsz, de *különböző formátumú kellékeket is importálhatsz*. A szükséges konverziót
a szerkesztő fű alatt elvégzi, így nyugodtan a játékkészítésre koncentrálhatsz, nem kell az apróságokkal törődnöd.

### Betűtípusok

Könnyebb lenne felsorolni, hogy mi nem támogatott. A tipikus formátumok közül:

- TrueType (.ttf) és OpenType (.otf)
- FontForge (.sfd)
- WebFonts (.woff, .woff2)
- X11 Fonts (.bdf, .pcf, valamint a tömörített verzióik, .bdf.gz, .pcf.gz)
- Scalable Screen Fonts (.sfn)
- továbbá bármilyen kép, színes fontokhoz

Ami a kódolást illeti, csakis az UTF-8 kódolt UNICODE támogatott, ezért a fontoknak az ISO-10464 kódlaphoz kell karaktereket
tartalmaznia. Más kódlap nem fog működni. A speciális karakterek az Alap Többnyelvű Síkon (BMP, U+0 - U+FFFF) felül, mint pl.
az emojik, szintén támogatottak (egészen U+10FFFF-ig), akárcsak a többkarakteres ligatúrák.

### Szprájtok

A szprájtok importálásához nagyon javalott a true-color PNG képek használata, alfa csatornával (32 bites), de használható továbbá

- bármilyen Portable Network Graphics (.png, 8-bit, 16-bit, szürkeárnyalatos, 24-bit true-color, indexált stb.)
- GIMP eXperimental Computing Facility (.xcf)
- Photoshop Document (.psd, limitált funkciókkal, nem minden fájl támogatott)
- Joint Photographic Experts Group's (.jpg .jpeg, baseline és progressive szintén, de nem mindegyik)
- WebPicture (.webp, minden fajtája, adatvesztéses és adatvesztés nélküli, animációk is)
- Graphic Interchange Format (.gif, animált gifek szintén)
- Targa Graphics (.tga)
- Windows Bitmap (.bmp, csak a nem tömörített változat)
- [Model 3D](https://bztsrc.gitlab.io/model3d) (.m3d, animált 3D modellek)
- Wavefront OBJ (.obj, statikus 3D modellek)
- bármilyen 3D modell [egyéb formátum]ban (.blend, .dae, .fbx, .glTF, stb. ha telepítve van az [assimp](https://assimp.org))

### Paletták

- GIMP Palette fájlok (.gpl)
- Adobe Photoshop Color fájlok (.aco)
- Adobe Swatch Exchange fájlok (.ase)
- Microsoft Palette fájlok, JASC-PAL (.pal)
- Paint.NET Palette fájlok (.hex, .txt)
- továbbá bármilyen képről is betölthető paletta.

### Hang

- Xiph.org's Ogging Vorbis Audio (.ogg)
- MPEG Audio (.mp3)
- Windows Wave Format (.wav)
- Protracker Module (.mod)
- Fast Tracker II (.xm)
- Scream Tracker 3 (.s3m)
- Impulse Tracker (.it)
- FLAC (amennyiben a libflac telepítve van)
- Opus (amennyiben a libopus telepytve van)

### Videó

- Xiph.org's Theora Video (.ogv)
- bármilyen videó formátum (.mp4, .mkv, .avi, .mpg, .flv, stb. ha az [ffmpeg](https://ffmpeg.org) telepítve van)

Ne is próbálj meg egészestés filmeket importálni, csak rövid (1 - 2 perces) vágójeleneteket.

Segíts
------

Ezt a kézikönyvet önkéntesek írják. Ha teheted, támogasd az erőfeszítést, és
[segíts megírni](https://codeberg.org/tirnanog/editor/src/branch/main/docs) a kézikönyvet. Küldj PR-t a
[git repo](https://codeberg.org/tirnanog/editor)ba.

Jelenleg nagy szükség van fordítókra.
