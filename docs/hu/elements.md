<h1 tab19 ui_elemek>UI Elemek</h1>

A játék kinézetének megadása
----------------------------

Válaszd a `Felület` > `Elemek` menüpontot. Itt láthatod az összes felhasználói felület elemet, egy mindent egybe, nagy
űrlap képében, rengeteg szprájt és betűtípus opcióval.

Nincs igazából mit mondani erről az oldalról. Meg kell adni minden UI által használt szprájtot. Unalmas és monoton munka,
de csak egyszer kell megcsinálni.

Előnézet
--------

Az <imgt ../img/preview.png> "Előnézet" gombra kattintva alul, látni fogod, hogy hogyan fog kinézni a felület. Kapsz minta
ablakot fülekkel, beviteli mezőkkel, szkrollmezőkkel stb. Az ablak alatt egy minta dialógus ablak látható.

Ha megadtál kurzorszprájtokat is, akkor a bal felső sarokban lesz három mező (kicsit eltérő színű, pirosas, kékes, zöldes
háttérrel). Ha ezek fölé viszed az egeret, akkor az egérkurzor át fog változni, és láthatod, hogy néznek ki (animációt
beleértve, pl. betöltés kurzor esetén hasznos).

Az előnézet NEM egy működő felület. Csak a pipa és a gombok kattinthatók, hogy lásd hogy mutat a lenyomott verzió. A csúszka
és a szkroll nem fog működni.
