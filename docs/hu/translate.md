<h1 tab1 fordítasok>Fordítások</h1>

Fordítások megadása
-------------------

Válaszd a `Fordítások` menüt. A legördülő menüben kiválaszthatsz egy már létező fordítást, vagy választhatod a legalsó,
"Új nyelv hozzáadása" opciót.

Azonosító
---------

A jobb felső sarokban látni fogod a nyelv kétbetűs ISO-639-1 kódját, és a natív elnevezését. A kód lehet "en", "es", "ru", "de",
"ja" stb. A név az az, ahogy az adott nyelvet beszélők nevezik, például nem "Német", hanem "Deutsch". Vagy egy másik példa, nem
"Japán" hanem inkább "日本語". Néhány kód és név előre van definiálva a leggyakrabban beszélt nyelvekhez és az EU nyelveihez,
ami azt jelenti, hogy elég beírni a kódot, a név magától kitöltődik. Persze átírhatod, ha szeretnéd.

Beszéd megadása
---------------

Néhány audió fájl speciális abban a tekintetben, hogy beszédet tartalmaz, ami lefordítható. Minden olyan audió, ami ebbe a
kategóriába tartozik, fel lesz itt sorolva, hogy aztán összerendelhesd a fordításokat. 100%-ig nem lehetséges, de mindig
törekedni kell rá, hogy a lefordított beszédek azonos hosszúságúak legyenek, mint az eredeti. Pár másodperc ide vagy oda
nem számít, de egy sokkal hosszabb felvétel gondot okozhat az ávezetőknél, ahol a beszéd időzítve van.

Szöveg megadása
---------------

Az audió blokk alatt található a játékban előforduló összes szöveg. Ezeket automatikusan kigyűjti a szerkesztő, szóval ha
módosítod a játékot (például egy új tárgyat adsz hozzá), akkor a lista ennek megfelelően automatikusan változni fog.

Ez a felület használható a szövegek lefordítására. Más programokból (például szövegszerkesztőből) való beillesztés is működik.
De ha mégis macerásnak éreznéd a használatát, akkor lementheted a fordítást egy fájlba. Olyan formátumban van tárolva, ami
emlékeztet a .po fájlokra, szóval nem okozhat gondot a lefordításuk a megszokott eszközeiddel. Tartsd észben, hogy a legeslegelső
komment sor kötelező, és nem szabad megváltoztatni. Továbbá a legeslegelső azonosító-szöveg páros mindig a nyelv
lefordított neve, és a második páros pedig a játék lefordított címe.

Amint végeztél a szövegek lefordításával, ne felejts el az <ui1>Elment</ui1> gombra kattintani.
