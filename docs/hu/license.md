<h1 tab10 szerkeszto_licensz>TirNanoG Szerkesztő Licensz</h1>

<imgr ../img/gplv3.png>A TirNanoG Szerkesztő Szabad és Nyílt Forráskódú szoftver, ami [GPLv3+](https://codeberg.org/tirnanog/editor/src/branch/main/LICENSE)
(vagy tetszés szerint ennek bármelyik újabb verziója) alatt kerül terjesztésre.

```
 Ez a program szabad szoftver; terjeszthető illetve módosítható a Free Software
 Fundation által kiadott GNU General Public License dokumentumban leírtak;
 akár a licensz 3-as, akár (tetszőleges) későbbi változata szerint.

 Ez a program abban a reményben kerül közreadásra, hogy hasznos lesz, de minden
 egyéb GARANCIA NÉLKÜL, az ELADHATÓSÁGRA vagy BÁRMELY CÉLÚ ALKALMAZHATÓSÁGRA
 való garanciát is beleértve. További részleteket a GNU General Public License
 tartalmaz.

 A felhasználónak a programmal együtt meg kell kapnia a GNU General Public
 License egy példányát; ha mégsem kapta meg, akkor írjon erre a címre:
 Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
```

<h1>TirNanoG Fájl Formátum Licensz</h1>

A TirNanoG Fájl Formátuma **kettős licenszelésű**:

Nem-Titkosított
---------------

<imgr ../img/ccbyncsa.png>Alapból a TirNanoG Fájl Formátum licensze [CC-by-nc-sa](https://codeberg.org/tirnanog/editor/src/branch/main/LICENSE)
(szerzők megnevezendők, nem eladható, azonos feltételekkel terjeszthető):

```
CREATIVE COMMONS - BY - NC - SA
https://creativecommons.org/licenses/by-nc-sa/

A művet szabadon:

 - Megoszthatod — másolhatod és terjesztheted a művet bármilyen módon
    vagy formában

 - Átdolgozhatod — származékos műveket hozhatsz létre, átalakíthatod
     és új művekbe építheted be. A jogtulajdonos nem vonhatja vissza
     ezen engedélyeket míg betartod a licensz feltételeit.

Az alábbi feltételekkel:

 - Nevezd meg! — A szerzőt megfelelően fel kell tüntetned, hivatkozást
     kell létrehoznod a licenszre és jelezned, ha a művön változtatást
     hajtottál végre. Ezt bármilyen ésszerű módon megteheted, kivéve
     oly módon ami azt sugallná, hogy a jogosult támogat téged vagy a
     felhasználásod körülményeit.

 - Ne add el! — Nem használhatod a művet üzleti célokra.

 - Így add tovább! — Ha feldolgozod, átalakítod vagy gyűjteményes művet
     hozol létre a műből, akkor a létrejött művet ugyanazon licensz-
     feltételek mellett kell terjesztened, mint az eredetit.
```

Titkosított
-----------

A TirNanoG Fájl Formátum megalkotójának írásos engedélyével használható a titkosított formátum, bővebb részletekért lásd a
[szerkesztő licensz játékfájlokhoz] fejezetet. Kereskedelmi játékok esetén javaslom ezt a a fájlformátum opciót, mivel ez
nemcsak titkosítást ad, de jogi garanciát is tartalmaz arra vonatkozóan, hogy a .tng fájlban tárolt kellékeidet ne lehessen
ellopni.

A döntés és a felellőség abban, hogy melyik licensz opciót használja egy adott játék fájlja teljesen a szerkesztőprogramot
használó felhasználóé, és **nem befolyásolja a játék licenszét**, amit ebben a formátumban tárolnak. Semmilyen körülmények
között nem lehetek én, bzt, a TirNanoG Szerkesztő és a TirNanoG Fájl Formátum megalkotója felelős mások által létrehozott
játék fájlokért.

A fájlban tárolt kellékek jogtulajdonosainak érdekeinek védelmében, megpróbálni kititkosítani és kinyerni az adatokat vagy
bármilyen más módon visszafejteni a titkosított formátumot szigorúan tilos és illegális.

<h1>TirNanoG Alap Licensz</h1>

<imgr ../img/ccbysa.png>Az alap játék sablon, a TirNanoG Alap licensze [CC-by-sa](https://creativecommons.org/licenses/by-sa/)
(akárcsak a LPC-é, amire épült). A licensz 4.0-ás, vagy igény szerint bármilyen későbbi verziója is használható.

```
CREATIVE COMMONS - BY - SA
https://creativecommons.org/licenses/by-sa/

A művet szabadon:

 - Megoszthatod — másolhatod és terjesztheted a művet bármilyen módon
     vagy formában

 - Átdolgozhatod — származékos műveket hozhatsz létre, átalakíthatod
     és új művekbe építheted be. A jogtulajdonos nem vonhatja vissza
     ezen engedélyeket míg betartod a licensz feltételeit.

Az alábbi feltételekkel:

 - Nevezd meg! — A szerzőt megfelelően fel kell tüntetned, hivatkozást
     kell létrehoznod a licenszre és jelezned, ha a művön változtatást
     hajtottál végre. Ezt bármilyen ésszerű módon megteheted, kivéve
     oly módon ami azt sugallná, hogy a jogosult támogat téged vagy a
     felhasználásod körülményeit.

 - Így add tovább! — Ha feldolgozod, átalakítod vagy gyűjteményes művet
     hozol létre a műből, akkor a létrejött művet ugyanazon licensz-
     feltételek mellett kell terjesztened, mint az eredetit.
```
