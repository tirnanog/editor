TirNanoG Documentation
======================

This directory holds the description of the project format (proj_format.md) and the description of the distributable game
TirNanoG File Format (tng_format.md), as well as the source of the user's manual.

The user's manual is created with [gendoc](https://bztsrc.gitlab.io/gendoc). You'll have to download and compile that tool.
```
wget https://gitlab.com/bztsrc/gendoc/-/raw/main/gendoc.c
gcc gendoc.c -o gendoc
```
Simple as that. Once you have gendoc, you can create the manual by going into one of the language code directories and running
```
cd docs/en
gendoc ../manual_en.html manual.xml
```
Finally, copy the generated `manual_(langcode).html` into the `public` directory in the
[pages](https://codeberg.org/tirnanog/pages) repo.

If you keep the manual html in `docs`, then running `make package` in the `src` directory will add a local copy of the manuals
to the package.

Translating the User's Manual
-----------------------------

1. Copy the `en` directory to the ISO-639-1 language code of the new language, for example `hu`.
2. There are some translateable texts in the doc (like title, "Search Results" caption, labels of the "Previous" and "Next"
   page buttons etc.), these are specified in `hu/manual.xml` between the `<doc>` ... `</doc>` tag.
3. Everything else is in MarkDown files, which you can translate freely, except:
4. Label ids for headings are generated from the heading's text automatically. You can change those texts freely, but
5. Label ids must be unique (they are used in the URL), so if you have multiple headings with the same text, specify the
   id manually. Again, you can use whatever you want for the label id (as long as it is unique).
6. Some headings have label aliases as well, like `tabXX`. **DO NOT** change these. The editor uses the aliases to open the
   manual on a certain page, regadless to the language of the manual.

```
<h1>Heads-Up Display</h1>              will use "heads_up_display" as label id
<h1 hud>Heads-Up Display</h1>          will use "hud" as label id
<h1 tab20 hud>Heads-Up Display</h1>    will use both "tab20" and "hud" as label ids, DO NOT change "tab20"
```

7. copy the generated `manual_hu.html` to the `public` directory in the `pages` branch (see above).
8. add a link and the language code to the JS list in `public/manual.html` (this is just a small dispatcher page that redirects
   to the manual in the browser's configured language).

Translating the Editor
----------------------

1. Increase the `NUMLANGS` define in [lang.h](../src/lang.h).
2. Copy [lang_en.h](../src/lang_en.h) to a new file, for example `lang_hu.h`.
3. This contains only strings, translate them. The first is the two-letter ISO-639-1 language code. The keys can be found in `lang.h`.
4. Include this new dictionary header in [main.c](../src/main.c) in the `dict` array.
5. (Optional) If the language's native name hasn't been added already, add it to `translate_names` in [translate.c](../src/translate.c).

To add a new language flag as well:

1. Edit [icons.png](../src/misc/icons.png), there are placeholders on the bottom left for new flags.
2. Add the ISO language code to [ui.c](../src/ui.c) in the `ui_lngicn` array.

NOTE: these aren't country flags, these are language flags. If multiple countries are speaking that language, use a splitted flag,
see for example English (splitted USA and GB flags) or German (splitted Germany and Swiss flags).

Finally, run `make clean all` in the [src](https://codeberg.org/tirnanog/editor/src/branch/main/src) directory. Adding a new
dictionary or changing the icons.png requires a clean recompilation (should take no more than a minute).
