TirNanoG Sprite Guidelines
==========================

Part of the [TirNanoG project](https://tirnanog.codeberg.page). For an actual sprite set, which uses these guides,
see [TirNanoG Base](https://codeberg.org/tirnanog/base).

Guides
------

The file `guides.xcf` contains all guidelines as layers. But in case you aren't using GIMP, each layer is saved as a PNG as well.

License
-------

These guidelines are licensed under **CC-BY-SA-4.0**. They were based on [Liberated Pixel Cup](https://lpc.opengameart.org)
assets, but diverged from that original significantly.
