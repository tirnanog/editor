/*
 * tnge/sprites.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Sprites window
 *
 */

#include "main.h"

int sprites_cat = 0, sprites_oldcat = -1;
void sprites_new(void *data) { (void)data; ui_switchtab(SUBMENU_NEWSPRITE); }
void sprites_gen(void *data) { (void)data; ui_switchtab(SUBMENU_GENERATOR); }
void sprites_zoom(void *data);
void sprites_delete(void *data);
void sprites_tglmove(void *data);
void sprites_chkmove(void *data);
void sprites_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
char sprites_info[64];

SDL_Texture *sprites_anims[8] = { 0 };
ui_table_t sprites_table = { NULL, SPRITES_NONE, 64, 64, 0, 0, 0, 0, 0, 0, 0, -1, 0,
    sizeof(ui_sprlist_t), sprites_drawcell, NULL, NULL, NULL };

ui_table_t sprites_sprites = { NULL, 0, 64, 64, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(ui_sprlist_t), NULL, NULL, NULL, NULL };

ui_icongrpelem_t sprites_catelems[] = {
    { ICON_UI, SPRITES_CAT_UI },
    { ICON_TILE, SPRITES_CAT_TILE },
    { ICON_NPC, SPRITES_CAT_CHAR },
    { ICON_PORT, SPRITES_CAT_PORT },
    { ICON_BACKGR, SPRITES_CAT_BACKS }
};
ui_icongrp_t sprites_cats = { &sprites_cat, 0, 5, sprites_catelems };

/**
 * The form
 */
ui_form_t sprites_form[] = {
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, SPRITES_ADD, (void*)ICON_ADD, sprites_new },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, SPRITES_GEN, (void*)ICON_NPC, sprites_gen },
    { FORM_ICONGRP, 20, 0, 6*24-4, 20, 0, 0, &sprites_cats, sprites_chkmove },
    { FORM_ZOOM, 0, 0, 68, 20, 0, 0, NULL, sprites_zoom },
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, SPRITES_DELETE, (void*)ICON_REMOVE, sprites_delete },
    { FORM_BUTTONICN, 56, 0, 20, 20, 0, SPRITES_MOVE, (void*)ICON_MOVE, sprites_tglmove },
    { FORM_TEXT, 0, 0, 0, 16, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 0, 0, 16, 0, 0, NULL, NULL },
    { FORM_TABLE, 10, 29, 0, 0, 0, 0, &sprites_table, NULL },
    { FORM_TABLE, 10, 0, 0, 0, 0, 0, &sprites_sprites, NULL },
    /* 10 */
    { FORM_TEXT, 0, 0, 16, 16, 0, 0, "\x11", NULL },
    { FORM_TEXT, 0, 0, 16, 16, 0, 0, "\x12", NULL },
    { FORM_TEXT, 0, 0, 16, 16, 0, 0, "\x13", NULL },
    { FORM_TEXT, 0, 0, 16, 16, 0, 0, "\x14", NULL },
    { FORM_TEXT, 0, 0, 16, 16, 0, 0, "\x15", NULL },
    { FORM_TEXT, 0, 0, 16, 16, 0, 0, "\x16", NULL },
    { FORM_TEXT, 0, 0, 16, 16, 0, 0, "\x17", NULL },
    { FORM_TEXT, 0, 0, 16, 16, 0, 0, "\x18", NULL },
    { FORM_LAST }
};

/**
 * Draw (render) table cell
 */
void sprites_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    ui_sprlist_t *spr = (ui_sprlist_t*)data;
    ui_sprite_t *s = NULL;
    SDL_Rect rect;
    int i;

    (void)hdr;
    if(!data) return;
    for(i = 0; i < 8 && !spr->dir[i].data; i++);
    if(i >= 8) return;
    s = &spr->dir[i];
    ui_fit(w - 4, h - 4, s->w, s->h, &rect.w, &rect.h);
    rect.x = x + 2 + (w - 4 - rect.w) / 2; rect.y = y + 2 + (h - 4 - rect.h) / 2;
    spr_blit(s, idx == sel ? 255 : 160, dst, &rect);
}

/**
 * Zoom the sprite table
 */
void sprites_zoom(void *data)
{
    (void)data;
    switch(ui_popup_page) {
        case 0: if(sprites_table.row > 16) sprites_table.row >>= 1; break;
        case 1: sprites_table.row = 64; break;
        case 2: if(sprites_table.row < 256) sprites_table.row <<= 1; break;
    }
    sprites_table.col = sprites_table.row;
}

/**
 * Delete a sprite
 */
void sprites_delete(void *data)
{
    int i;
    char *s;

    (void)data;
    if(!sprites_table.data || sprites_table.val < 0) return;
    if(ui_modal(ICON_WARN, LANG_SURE, project.sprites[sprites_cat][sprites_table.val].name)) {
        spr_del(sprites_cat, sprites_table.val);
        for(i = 0; i < 8; i++) {
            sprintf(projfn, "%s" SEP "%s" SEP "%s_", project.id, project_dirs[PROJDIRS_SPRITES + sprites_cat],
                project.sprites[sprites_cat][sprites_table.val].name);
            s = projfn + strlen(projfn);
            spr_wrname(sprites_cat, sprites_table.val, i, s);
            strcpy(s + 4, ".png");
            if(verbose) printf("sprites_delete: removing %s\n", projdir);
            remove(projdir);
        }
        sprites_init(SUBMENU_SPRITES);
    }
}

/**
 * Toggle move sprite mode
 */
void sprites_tglmove(void *data)
{
    (void)data;
    if(!sprites_table.data || sprites_table.val < 0) return;
    if(sprites_form[5].type == FORM_BUTTONICNP) {
        sprites_form[5].type = FORM_BUTTONICN;
        sprites_oldcat = -1;
    } else {
        sprites_form[5].type = FORM_BUTTONICNP;
        sprites_oldcat = sprites_cat;
    }
}

/**
 * Check move sprite mode
 */
void sprites_chkmove(void *data)
{
    int i;
    char *oldfn, *s;

    (void)data;
    if(sprites_form[5].type == FORM_BUTTONICNP && sprites_oldcat != -1 && sprites_oldcat != sprites_cat &&
      sprites_table.data && sprites_table.val >= 0 && sprites_table.val < sprites_table.num &&
      project.sprites[sprites_oldcat][sprites_table.val].x == -1) {
        /* move png files */
        oldfn = (char*)main_alloc(PATH_MAX + FILENAME_MAX + 1);
        for(i = 0; i < 8; i++) {
            if(project.sprites[sprites_oldcat][sprites_table.val].dir[i].data) {
                sprintf(projfn, "%s" SEP "%s" SEP "%s_", project.id, project_dirs[PROJDIRS_SPRITES + sprites_oldcat],
                    project.sprites[sprites_oldcat][sprites_table.val].name);
                s = projfn + strlen(projfn);
                spr_wrname(sprites_oldcat, sprites_table.val, i, s);
                strcpy(s + 4, ".png");
                strcpy(oldfn, projdir);
                sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_SPRITES + sprites_cat]);
                project_mkdir(projdir);
                sprintf(projfn + strlen(projfn), SEP "%s_", project.sprites[sprites_oldcat][sprites_table.val].name);
                s = projfn + strlen(projfn);
                spr_wrname(sprites_oldcat, sprites_table.val, i, s);
                strcpy(s + 4, ".png");
                if(verbose) printf("sprites_chkmove: moving %s to %s\n", oldfn, projdir);
                rename(oldfn, projdir);
            }
            /* when moving from tiles or objects, delete meta info */
            if(sprites_oldcat == 1) {
                sprintf(projfn, "%s" SEP "%s" SEP "%s.til", project.id, project_dirs[PROJDIRS_SPRITES + sprites_oldcat],
                    project.sprites[sprites_oldcat][sprites_table.val].name);
                if(!remove(projdir) && verbose) printf("sprites_chkmove: removing %s\n", projdir);
                sprintf(projfn, "%s" SEP "%s" SEP "%s.obj", project.id, project_dirs[PROJDIRS_OBJECTS],
                    project.sprites[sprites_oldcat][sprites_table.val].name);
                if(!remove(projdir) && verbose) printf("sprites_chkmove: removing %s\n", projdir);
            }
            /* there's only one version for fullscreen sprites */
            if(project.sprites[sprites_oldcat][sprites_table.val].dir[i].type == 0x7F) break;
        }
        free(oldfn);
        /* move in the sprite manager too */
        spr_move(sprites_table.val, sprites_oldcat, sprites_cat);
    }
    sprites_form[5].type = FORM_BUTTONICN;
    sprites_oldcat = -1;
    sprites_table.val = 0;
    sprites_init(SUBMENU_SPRITES);
}

/**
 * Exit sprites window
 */
void sprites_exit(int tab)
{
    int i;

    (void)tab;
    for(i = 0; i < 8; i++)
        if(sprites_anims[i]) {
            SDL_DestroyTexture(sprites_anims[i]);
            sprites_anims[i] = NULL;
        }
}

/**
 * Enter sprites window
 */
void sprites_init(int tab)
{
    sprites_exit(tab);
    sprites_table.data = project.sprites[sprites_cat];
    sprites_table.num = project.spritenum[sprites_cat];
    if(sprites_table.val >= sprites_table.num)
        sprites_table.val = sprites_table.num - 1;
}

/**
 * Resize the view
 */
void sprites_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    sprites_form[0].y = sprites_form[1].y = sprites_form[2].y = sprites_form[3].y = sprites_form[4].y = sprites_form[5].y = h - 48;
    sprites_form[0].x = w - 40;
    sprites_form[1].x = w - 80;
    sprites_form[2].x = w / 4 - sprites_form[3].w / 2;
    sprites_form[3].x = w / 2 - sprites_form[4].w - 10;
    sprites_form[6].x = sprites_form[7].x = w / 2 + 64;
    sprites_form[6].w = sprites_form[7].w = sprites_form[1].x - sprites_form[6].x - 10;
    sprites_form[6].y = h - 56;
    sprites_form[7].y = h - 40;
    sprites_sprites.row = sprites_sprites.col = (w - 22) / 8;
    sprites_form[9].w = w - 20;
    sprites_form[9].h = sprites_sprites.col + 2;
    sprites_form[9].y = h - 59 - sprites_form[9].h;
    sprites_form[8].w = w - 20;
    sprites_form[8].h = sprites_form[9].y - sprites_form[8].y - 10;
    sprites_form[9].x = w - 10 - sprites_form[9].w;
    for(i = 0; i < 8; i++) {
        sprites_form[10 + i].x = sprites_form[9].x + 1 + i * sprites_sprites.col + (sprites_sprites.col - 16) / 2;
        sprites_form[10 + i].y = sprites_form[9].y + 1 + (sprites_sprites.row - 16) / 2;
    }
}

/**
 * View layer
 */
void sprites_redraw(int tab)
{
    ui_sprlist_t *spr = (ui_sprlist_t*)sprites_table.data;
    ui_sprite_t *s;
    int i, mw = 0, mh = 0, mf = 0, mt = -1;

    (void)tab;
    ui_form = sprites_form;
    sprites_form[4].inactive = sprites_form[5].inactive = sprites_table.val < 0 ||
        project.sprites[sprites_cat][sprites_table.val].x != -1;
    sprites_form[6].param = sprites_form[7].param = NULL;
    if(spr && sprites_table.val >= 0 && sprites_table.val < sprites_table.num) {
        sprites_form[6].param = spr[sprites_table.val].name;
        for(i = 0; i < 8; i++) {
            s = &spr[sprites_table.val].dir[i];
            if(s->data) {
                if(mt < 0) mt = s->type;
                if(s->w > mw) mw = s->w;
                if(s->h > mh) mh = s->h;
                if(s->nframe > mf) mf = s->nframe;
                spr_texture(s, &sprites_anims[i]);
                sprites_form[10 + i].inactive = 2;
            } else
                sprites_form[10 + i].inactive = 0;
        }
        sprites_form[7].param = sprites_info;
        sprintf(sprites_info, "%4d x %4d, %c%2d %s", mw, mh, mt == 0x7F ? 25 : mt + 6, mf, lang[SPRITES_FRAMES]);
    }
}

/**
 * View layer
 */
void sprites_render(int tab)
{
    ui_sprlist_t *spr = (ui_sprlist_t*)sprites_table.data;
    ui_sprite_t *s;
    SDL_Rect rect;
    int i;

    (void)tab;
    if(spr && sprites_table.val >= 0 && sprites_table.val < sprites_table.num) {
        for(i = 0; i < 8; i++) {
            s = &spr[sprites_table.val].dir[i];
            rect.x = sprites_form[9].x + 3 + i * sprites_sprites.col;
            rect.y = sprites_form[9].y + 3;
            rect.w = rect.h = sprites_sprites.col - 4;
            spr_render(s, -1, sprites_anims[i], &rect);
        }
    }
}
