/*
 * tnge/npcs.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Non-player characters window
 *
 */

#include "main.h"

/* for the npc's inventory preview */
extern ui_num_t hud_invw, hud_invh, hud_invl, hud_invt;
extern ui_sprsel_t hud_invimg, hud_invsel, hud_invequip;

typedef struct {
    int obj;    /* object id */
    int qty;    /* quantity 1 - 999999 */
    int per;    /* chance percentage 1 - 100 */
    int frq;    /* replanish occurance in minutes */
} npcs_inv_t;

/* string arrays for saving numeric codes to files */
const char *npcs_movecode[] = { "stand", "random", "patrol", NULL };

void npcs_load(void *data);
void npcs_new(void *data);
void npcs_save(void *data);
void npcs_delete(void *data);
void npcs_preview(void *data);
void npcs_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void npcs_attrdel(void *data);
void npcs_attradd(void *data);
void npcs_drawattr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void npcs_lyrdel(void *data);
void npcs_lyradd(void *data);
void npcs_lyrclk(void *data);
void npcs_lyrup(void *data);
void npcs_lyrdown(void *data);
void npcs_lyrcopy(void *data);
void npcs_lyrcut(void *data);
void npcs_lyrpaste(void *data);
void npcs_drawspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void npcs_renderspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void npcs_invdel(void *data);
void npcs_invadd(void *data);
void npcs_drawinv(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

SDL_Texture *npcs_anim = NULL;
char npcs_name[PROJ_NAMEMAX], *npcs_moves[4] = { 0 }, *npcs_events[19] = { 0 }, npcs_title[PROJ_TITLEMAX];
int npcs_tab = 0;
ui_input_t npcs_nameinp = { INP_ID, sizeof(npcs_name), npcs_name };
ui_icongrpelem_t npcs_selelems[] = {
    { ICON_NPC, NPCS_CONFIG },
    { ICON_INVENT, NPCS_INV }
};
ui_icongrp_t npcs_sel = { &npcs_tab, 0, 2, npcs_selelems };
ui_select_t npcs_move = { 0, 0, npcs_moves };
ui_num_t npcs_speed = { 100, 1, 1000, 1 };
ui_num_t npcs_change = { 1000, 100, 3000, 10 };
ui_num_t npcs_path = { 1, 1, 99, 1 };
ui_select_t npcs_event = { 0, 0, npcs_events };
ui_num_t npcs_eventnum1 = { 1, 1, 15, 1 };
ui_num_t npcs_eventnum2 = { 1, 1, 15, 1 };
ui_sprsel_t npcs_eventobj[3] = { { -1, 1, 158, 4 }, { -1, 1, 158, 4 }, { -1, 1, 158, 4 } };
ui_select_t npcs_eventtimers = { 0, 0, project_eventtimers };
ui_cmdpal_t npcs_pale = { CMDPAL_NPCEVT, 0, 0 };
ui_cmdpal_t npcs_palp = { CMDPAL_PRICE, 0, 0 };
ui_cmd_t npcs_cmd[19] = { 0 };
ui_select_t npcs_attr = { 0, 0, NULL };
ui_select_t npcs_rel = { -1, LANG_SET, project_rels };
ui_num_t npcs_val = { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 };
ui_sprsel_t npcs_spr_hidden = { -1, 2, 25, 0 };
ui_sprsel_t npcs_spr_hidden2 = { -1, 3, 186, 1 };
ui_input_t npcs_titinp = { INP_NAME, sizeof(npcs_title), npcs_title };
ui_num_t npcs_chance = { 100, 1, 100, 1 };
ui_num_t npcs_qty = { 1, 1, 999999, 1 };
ui_num_t npcs_freq = { 0, 0, 10080, 1 }; /* max 7*24*60 minutes, one week */
ui_sprsel_t npcs_obj = { -1, 1, 158, 3 };

ui_tablehdr_t npcs_hdr[] = {
    { SUBMENU_NPCS, 0, 0, 0 },
    { 0 }
};
ui_table_t npcs_tbl = { npcs_hdr, NPCS_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(ui_sprlist_t), npcs_drawcell, NULL,
    NULL, NULL };

ui_tablehdr_t npcs_attrshdr[] = {
    { -1, 20, 0, 0 },
    { CHARS_ATTRS, 0, 0, 0 },
    { LANG_VALUE, 82, 0, 0 },
    { 0 }
};
ui_table_t npcs_attrs = { npcs_attrshdr, CHARS_NOATTR, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(chars_attrs_t), npcs_drawattr,
    NULL, NULL, NULL };

ui_tablehdr_t npcs_sprshdr[] = {
    { NUMTEXTS + ICON_IDLE, 20, CHARS_ANIM_IDLE, 0 },
    { NUMTEXTS + ICON_WALK, 20, CHARS_ANIM_WALK, 0 },
    { NUMTEXTS + ICON_CLIMB, 20, CHARS_ANIM_CLIMB, 0 },
    { NUMTEXTS + ICON_JUMP, 20, CHARS_ANIM_JUMP, 0 },
    { NUMTEXTS + ICON_RUN, 20, CHARS_ANIM_RUN, 0 },
    { NUMTEXTS + ICON_SWIM, 20, CHARS_ANIM_SWIM, 0 },
    { NUMTEXTS + ICON_FLY, 20, CHARS_ANIM_FLY, 0 },
    { NUMTEXTS + ICON_SHIELD, 20, CHARS_ANIM_BLOCK, 0 },
    { NUMTEXTS + ICON_HURT, 20, CHARS_ANIM_HURT, 0 },
    { NUMTEXTS + ICON_DEATH, 20, CHARS_ANIM_DIE, 0 },
    { NUMTEXTS + ICON_NUMACT + 1, 20, CHARS_ANIM_A1, 0 },
    { NUMTEXTS + ICON_NUMACT + 2, 20, CHARS_ANIM_A2, 0 },
    { NUMTEXTS + ICON_NUMACT + 3, 20, CHARS_ANIM_A3, 0 },
    { NUMTEXTS + ICON_NUMACT + 4, 20, CHARS_ANIM_A4, 0 },
    { NUMTEXTS + ICON_NUMACT + 5, 20, CHARS_ANIM_A5, 0 },
    { NUMTEXTS + ICON_NUMACT + 6, 20, CHARS_ANIM_A6, 0 },
    { NUMTEXTS + ICON_NUMACT + 7, 20, CHARS_ANIM_A7, 0 },
    { NUMTEXTS + ICON_NUMACT + 8, 20, CHARS_ANIM_A8, 0 },
    { NUMTEXTS + ICON_NUMACT + 9, 20, CHARS_ANIM_A9, 0 },
    { NUMTEXTS + ICON_PORT, 20, NPCS_PORTRAIT, 0 },
    { 0 }
};
ui_table_t npcs_sprs = { npcs_sprshdr, CHARS_NOLYR, 20, 20, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(int), npcs_drawspr, npcs_renderspr,
    NULL, NULL };

ui_tablehdr_t npcs_invhdr[] = {
    { NPCS_CHANCE, 80, 0, 0 },
    { NPCS_QTY, 68, 0, 0 },
    { NPCS_ITEMS, 0, 0, 0 },
    { NPCS_FREQ, 112, 0, 0 },
    { 0 }
};
ui_table_t npcs_invtbl = { npcs_invhdr, CHARS_NOATTR, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(npcs_inv_t), npcs_drawinv,
    NULL, NULL, NULL };

/**
 * The form
 */
ui_form_t npcs_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, NPCS_DELETE, (void*)ICON_REMOVE, npcs_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, npcs_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, NPCS_PREVIEW, (void*)ICON_PVIEW, npcs_preview },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, npcs_new },
    { FORM_INPUT, 0, 30, 200, 20, 0, NPCS_NAME, &npcs_nameinp, NULL },
    { FORM_TABLE, 10, 30, 200, 0, 0, 0, &npcs_tbl, npcs_load },
    /* 6 */
    { FORM_ICONGRP, 0, 30, 2*24-4, 20, 0, 0, &npcs_sel, NULL },
    /* 7 */
    { FORM_SELECT, 0, 54, 210, 20, 0, NPCS_MOVE, &npcs_move, NULL },
    { FORM_TEXT, 0, 78, 0, 20, 0, 0, NULL, NULL },
    { FORM_NUM, 0, 78, 42, 20, 0, NPCS_SSPEED, &npcs_speed, NULL },
    { FORM_TEXT, 0, 78, 0, 20, 0, 0, NULL, NULL },
    { FORM_TIME, 0, 78, 56, 20, 0, NPCS_SCHANGE, &npcs_change, NULL },
    { FORM_TEXT, 0, 78, 0, 20, 0, 0, NULL, NULL },
    { FORM_NUM, 0, 78, 42, 20, 0, NPCS_PATH, &npcs_path, NULL },
    /* 14 */
    { FORM_SELECT, 0, 108, 0, 20, 0, 0, &npcs_event, NULL },
    { FORM_NUM, 0, 108, 38, 20, 0, NPCS_SDIST, &npcs_eventnum1, NULL },
    { FORM_NUM, 0, 108, 38, 20, 0, NPCS_SDIST, &npcs_eventnum2, NULL },
    { FORM_SPRITE, 0, 108, 0, 20, 0, 0, &npcs_eventobj[0], NULL },
    { FORM_SPRITE, 0, 108, 0, 20, 0, 0, &npcs_eventobj[1], NULL },
    { FORM_SPRITE, 0, 108, 0, 20, 0, 0, &npcs_eventobj[2], NULL },
    { FORM_SELECT, 0, 108, 62, 20, 0, 0, &npcs_eventtimers, NULL },
    /* 21 */
    { FORM_CMDPAL, 0, 132, 38, 0, 0, 0, &npcs_pale, NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[0], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[1], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[2], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[3], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[4], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[5], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[6], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[7], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[8], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[9], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[10], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[11], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[12], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[13], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[14], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[15], NULL },
    { FORM_COMMANDS, 0, 132, 0, 0, 0, NPCS_ONEVENT, &npcs_cmd[16], NULL },
    /* 39 */
    { FORM_TABLE, 0, 54, 415, 100, 0, 0, &npcs_attrs, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_DELATTR, (void*)ICON_REMOVE, npcs_attrdel },
    { FORM_SELECT, 0, 0, 0, 20, 0, 0, &npcs_attr, NULL },
    { FORM_SELECT, 0, 0, 40, 20, 0, CHARS_RELATTR, &npcs_rel, NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, 0, &npcs_val, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_ADDATTR, (void*)ICON_ADD, npcs_attradd },
    /* 45 */
    { FORM_TABLE, 0, 0, 415, 182, 0, 0, &npcs_sprs, npcs_lyrclk },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_DELLYR, (void*)ICON_REMOVE, npcs_lyrdel },
    { FORM_CHARBTN, 0, 0, 20, 20, 0, CHARS_UPLYR, (void*)11014, npcs_lyrup },
    { FORM_CHARBTN, 0, 0, 20, 20, 0, CHARS_DWNLYR, (void*)11015, npcs_lyrdown },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_ADDLYR, (void*)ICON_ADD, npcs_lyradd },
    /* 50 */
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_PSTLYR, (void*)ICON_PASTE, npcs_lyrpaste },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_CUTLYR, (void*)ICON_CUT, npcs_lyrcut },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_CPYLYR, (void*)ICON_COPY, npcs_lyrcopy },
    /* 53 */
    { FORM_TEXT, 0, 54, 0, 20, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 54, 0, 20, 0, NPCS_TITLE, &npcs_titinp, NULL },
    /* 53 */
    { FORM_TABLE, 0, 78, 0, 0, 0, 0, &npcs_invtbl, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, NPCS_DELINV, (void*)ICON_REMOVE, npcs_invdel },
    { FORM_NUM, 0, 0, 42, 20, 0, NPCS_PERINV, &npcs_chance, NULL },
    { FORM_NUM, 0, 0, 64, 20, 0, NPCS_QTYINV, &npcs_qty, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, 0, &npcs_obj, NULL },
    { FORM_TEXT, 0, 0, 8, 20, 0, 0, "/", NULL },
    { FORM_NUM, 0, 0, 56, 20, 0, NPCS_FRQINV, &npcs_freq, NULL },
    { FORM_TEXT, 0, 0, 8, 20, 0, 0, "m", NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, NPCS_ADDINV, (void*)ICON_ADD, npcs_invadd },
    /* 64 */
    { FORM_CMDPAL, 0, 0, 38, 128, 0, 0, &npcs_palp, NULL },
    { FORM_COMMANDS, 0, 0, 0, 0, 0, NPCS_SELL, &npcs_cmd[17], NULL },
    { FORM_COMMANDS, 0, 0, 0, 0, 0, NPCS_BUY, &npcs_cmd[18], NULL },
    { FORM_STAT, 0, 0, 0, 0, 0, NPCS_CALCPRICE, NULL, NULL },

    { FORM_LAST }
};

/**
 * Draw table cell
 */
void npcs_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    ui_sprlist_t *npc = (ui_sprlist_t*)data;

    (void)idx; (void)sel; (void)w; (void)h; (void)hdr;
    if(!data) return;
    ui_text(dst, x + 4, y + 1, npc->name);
}

/**
 * Draw table cell
 */
void npcs_drawattr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    chars_attrs_t *attr = (chars_attrs_t*)data;
    char tmp[16];

    (void)idx; (void)sel; (void)w; (void)h;
    if(!data) return;
    if(attr->rel >= 0)
        ui_icon(dst, x + 4, y + 1, ICON_QMARK);
    ui_text(dst, x + 4 + hdr[0].w, y + 1, project.attrs[attr->attr]);
    if(attr->rel >= 0)
        ui_text(dst, x + 4 + hdr[0].w + hdr[1].w, y + 1, project_rels[attr->rel]);
    sprintf(tmp, "%6d", attr->val);
    ui_text(dst, x + 20 + hdr[0].w + hdr[1].w, y + 1, tmp);
}

/**
 * Draw table cell
 */
void npcs_drawspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    ui_sprite_t *s;
    SDL_Rect rect;

    (void)hdr;
    if(!data) return;
    s = spr_getidx((idx % SPRPERLYR) == SPRPERLYR - 1 ? npcs_spr_hidden2.cat : npcs_spr_hidden.cat, *((int*)data), -1, 0);
    if(!s)
        ui_icon(dst, x + 2, y, ICON_INADD);
    else
    if(idx != sel) {
        ui_fit(w, h, s->w, s->h, &rect.w, &rect.h);
        rect.x = x + (w - rect.w) / 2; rect.y = y + h - rect.h;
        spr_blit(s, 160, dst, &rect);
    } else
        spr_texture(s, &npcs_anim);
}

/**
 * Draw table cell
 */
void npcs_renderspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    SDL_Rect rect;

    (void)dst; (void)hdr;
    if(!data || !npcs_anim || *((int*)data) < 0 || idx != sel) return;
    rect.x = x; rect.y = y; rect.w = w; rect.h = h;
    spr_render(spr_getidx((idx % SPRPERLYR) == SPRPERLYR - 1 ? npcs_spr_hidden2.cat : npcs_spr_hidden.cat, *((int*)data), -1, 1),
        -1, npcs_anim, &rect);
}

/**
 * Draw table cell
 */
void npcs_drawinv(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    npcs_inv_t *inv = (npcs_inv_t*)data;
    char tmp[32];
    int t = ui_clip.w;

    (void)idx; (void)sel; (void)w; (void)h; (void)hdr;
    if(!data) return;
    sprintf(tmp, "%u%%", inv->per);
    ui_text(dst, x + 32, y + 1, tmp);
    sprintf(tmp, "%6u", inv->qty);
    ui_text(dst, x + hdr[0].w + 8, y + 1, tmp);
    ui_clip.w = hdr[0].w + hdr[1].w + hdr[2].w - 2;
    ui_text(dst, x + hdr[0].w + hdr[1].w + 4, y + 1, project.sprites[1][inv->obj].name);
    ui_clip.w = t;
    if(inv->frq == 0)
        ui_text(dst, x + hdr[0].w + hdr[1].w + hdr[2].w + 4, y + 1, lang[NPCS_ONETIME]);
    else {
        sprintf(tmp, "/%3um", inv->frq);
        ui_text(dst, x + hdr[0].w + hdr[1].w + hdr[2].w + 4, y + 1, tmp);
    }
}

/**
 * Exit npcs window
 */
void npcs_exit(int tab)
{
    (void)tab;
    npcs_tbl.clk = -1;
    if(npcs_anim) SDL_DestroyTexture(npcs_anim);
    npcs_anim = NULL;
}

/**
 * Enter npcs window
 */
void npcs_init(int tab)
{
    npcs_exit(tab);
    npcs_form[1].param = lang[LANG_SAVE];
    npcs_form[1].w = ui_textwidth(npcs_form[1].param) + 40;
    if(npcs_form[1].w < 200) npcs_form[1].w = 200;
    npcs_form[53].param = lang[NPCS_TITLE];
    npcs_form[53].w = ui_textwidth(npcs_form[53].param);
    if(!npcs_name[0]) npcs_tbl.val = -1;
    npcs_tbl.data = project.sprites[PROJ_NUMSPRCATS - 2];
    npcs_tbl.num = project.spritenum[PROJ_NUMSPRCATS - 2];
    if(npcs_tbl.val >= npcs_tbl.num) npcs_tbl.val = npcs_tbl.num - 1;
    npcs_attr.opts = project.attrs;
    npcs_moves[0] = lang[NPCS_MOVE_STAND];
    npcs_moves[1] = lang[NPCS_MOVE_RANDOM];
    npcs_moves[2] = lang[NPCS_MOVE_PATROL];
    npcs_form[8].param = lang[NPCS_SPEED];
    npcs_form[10].param = lang[NPCS_CHANGE];
    npcs_form[12].param = lang[NPCS_PATH];
    npcs_events[0] = lang[CMD_EVT_ONCLICK];
    npcs_events[1] = lang[CMD_EVT_ONTOUCH];
    npcs_events[2] = lang[CMD_EVT_ONLEAVE];
    npcs_events[3] = lang[CMD_EVT_ONAPPROACH];
    npcs_events[4] = actions_name[0][0] ? actions_name[0] : lang[CMD_EVT_ONACTION1];
    npcs_events[5] = actions_name[1][0] ? actions_name[1] : lang[CMD_EVT_ONACTION2];
    npcs_events[6] = actions_name[2][0] ? actions_name[2] : lang[CMD_EVT_ONACTION3];
    npcs_events[7] = actions_name[3][0] ? actions_name[3] : lang[CMD_EVT_ONACTION4];
    npcs_events[8] = actions_name[4][0] ? actions_name[4] : lang[CMD_EVT_ONACTION5];
    npcs_events[9] = actions_name[5][0] ? actions_name[5] : lang[CMD_EVT_ONACTION6];
    npcs_events[10] = actions_name[6][0] ? actions_name[6] : lang[CMD_EVT_ONACTION7];
    npcs_events[11] = actions_name[7][0] ? actions_name[7] : lang[CMD_EVT_ONACTION8];
    npcs_events[12] = actions_name[8][0] ? actions_name[8] : lang[CMD_EVT_ONACTION9];
    npcs_events[13] = lang[CMD_EVT_ONOBJ1];
    npcs_events[14] = lang[CMD_EVT_ONOBJ2];
    npcs_events[15] = lang[CMD_EVT_ONOBJ3];
    npcs_events[16] = lang[CMD_EVT_ONTIMER];
}

/**
 * Resize the view
 */
void npcs_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    npcs_form[0].y = npcs_form[1].y = npcs_form[2].y = npcs_form[3].y = h - 48;
    npcs_form[1].x = w - 20 - npcs_form[1].w;
    npcs_form[2].x = npcs_form[1].x - 52;
    ui_table_resize(&npcs_form[5], npcs_form[5].w, h - 89);
    npcs_form[3].x = npcs_form[5].x + npcs_form[5].w + 20;
    npcs_form[4].x = w - 10 - npcs_form[4].w;
    npcs_form[6].x = npcs_form[7].x = npcs_form[8].x = npcs_form[10].x = npcs_form[12].x = npcs_form[14].x = npcs_form[53].x =
        npcs_form[55].x = npcs_form[56].x = npcs_form[65].x = npcs_form[67].x = npcs_form[5].x + npcs_form[5].w + 10;
    npcs_form[39].x = npcs_form[40].x = npcs_form[45].x = npcs_form[46].x = w - 10 - npcs_form[45].w;
    npcs_form[7].w = npcs_form[45].x - npcs_form[7].x - 10;
    npcs_form[9].x = npcs_form[7].x + npcs_form[7].w / 2 - 2 - npcs_form[9].w;
    npcs_form[8].w = npcs_form[9].x - npcs_form[8].x - 4;
    npcs_form[10].x = npcs_form[12].x = npcs_form[7].x + npcs_form[7].w / 2 + 2;
    npcs_form[11].x = npcs_form[7].x + npcs_form[7].w - npcs_form[11].w;
    npcs_form[10].w = npcs_form[11].x - npcs_form[10].x - 4;
    npcs_form[13].x = npcs_form[7].x + npcs_form[7].w - npcs_form[13].w;
    npcs_form[12].w = npcs_form[13].x - npcs_form[12].x - 4;
    npcs_form[15].x = npcs_form[7].x + npcs_form[7].w - npcs_form[15].w;
    npcs_form[16].x = npcs_form[7].x + npcs_form[7].w - npcs_form[16].w;
    npcs_form[17].x = npcs_form[18].x = npcs_form[19].x = npcs_form[14].x + npcs_form[7].w / 2 + 4;
    npcs_form[17].w = npcs_form[18].w = npcs_form[19].w = npcs_form[7].w / 2 - 2;
    npcs_form[20].x = npcs_form[7].x + npcs_form[7].w - npcs_form[20].w;
    npcs_form[21].x = npcs_form[7].x + npcs_form[7].w - npcs_form[21].w;
    npcs_form[21].h = h - 59 - npcs_form[21].y;
    for(i = 0; i < 17; i++) {
        npcs_form[i + 22].x = npcs_form[5].x + npcs_form[5].w + 10;
        npcs_form[i + 22].w = npcs_form[21].x - npcs_form[i + 22].x - 4;
        npcs_form[i + 22].h = h - 59 - npcs_form[21].y;
    }
    ui_table_resize(&npcs_form[39], npcs_form[39].w, h - 82 - 30 - npcs_form[45].h - npcs_form[39].y);
    npcs_form[40].y = npcs_form[41].y = npcs_form[42].y = npcs_form[43].y = npcs_form[44].y =
        npcs_form[39].y + npcs_form[39].h + 2;
    npcs_form[41].x = npcs_form[40].x + npcs_form[40].w + 10;
    npcs_form[44].x = w - 10 - npcs_form[44].w;
    npcs_form[43].x = npcs_form[44].x - 4 - npcs_form[43].w;
    npcs_form[42].x = npcs_form[43].x - 4 - npcs_form[42].w;
    npcs_form[41].w = npcs_form[43/*42*/].x - 4 - npcs_form[41].x;
    npcs_form[45].y = h - 82 - npcs_form[45].h;
    ui_table_resize(&npcs_form[45], npcs_form[45].w, npcs_form[45].h);
    npcs_form[46].y = npcs_form[47].y = npcs_form[48].y = npcs_form[49].y = npcs_form[50].y = npcs_form[51].y =
        npcs_form[52].y = h - 48 - 30;
    npcs_form[49].x = w - 10 - npcs_form[49].w;
    npcs_form[48].x = npcs_form[49].x - 10 - npcs_form[48].w;
    npcs_form[47].x = npcs_form[48].x - 4 - npcs_form[47].w;
    npcs_form[50].x = npcs_form[47].x - 100 - npcs_form[50].w;
    npcs_form[51].x = npcs_form[50].x - 4 - npcs_form[51].w;
    npcs_form[52].x = npcs_form[51].x - 4 - npcs_form[52].w;

    npcs_form[54].x = npcs_form[53].x + npcs_form[53].w + 10;
    npcs_form[54].w = w - 10 - npcs_form[54].x;
    ui_table_resize(&npcs_form[55], w - 10 - npcs_form[55].x, h - 59 - 30 - npcs_form[64].h - npcs_form[55].y);
    npcs_form[56].y = npcs_form[57].y = npcs_form[58].y = npcs_form[59].y = npcs_form[60].y = npcs_form[61].y =
        npcs_form[62].y = npcs_form[63].y = npcs_form[55].y + npcs_form[55].h + 4;
    npcs_form[57].x = npcs_form[55].x + npcs_invhdr[0].w - npcs_form[57].w - 2;
    npcs_form[58].x = npcs_form[55].x + npcs_invhdr[0].w + npcs_invhdr[1].w - npcs_form[58].w - 2;
    npcs_form[59].x = npcs_form[55].x + npcs_invhdr[0].w + npcs_invhdr[1].w + 2;
    npcs_form[59].w = npcs_invhdr[2].w - 4;
    npcs_form[60].x = npcs_form[55].x + npcs_invhdr[0].w + npcs_invhdr[1].w + npcs_invhdr[2].w + 2;
    npcs_form[61].x = npcs_form[60].x + npcs_form[60].w + 2;
    npcs_form[62].x = npcs_form[61].x + npcs_form[61].w + 2;
    npcs_form[63].x = w - 10 - npcs_form[63].w;
    npcs_form[64].x = w - 10 - npcs_form[64].w;
    npcs_form[64].y = npcs_form[65].y = npcs_form[66].y = npcs_form[67].y = h - 59 - npcs_form[64].h;
    npcs_form[65].h = npcs_form[66].h = npcs_form[67].h = npcs_form[64].h;
    npcs_form[65].w = npcs_form[66].w = (npcs_form[64].x - npcs_form[65].x) / 2 - 4;
    npcs_form[66].x = npcs_form[65].x + npcs_form[65].w + 4;
    npcs_form[67].w = (npcs_form[64].x - npcs_form[67].x) - 4;
}

/**
 * View layer
 */
void npcs_redraw(int tab)
{
    int i;

    (void)tab;
    ui_form = npcs_form;
    ui_form[0].inactive = (npcs_tbl.val < 0);
    ui_form[1].inactive = (!npcs_name[0]);
    ui_form[2].inactive = (!npcs_tab && (!npcs_sprs.num || !npcs_sprs.data))/* || (npcs_tab && !npcs_title[0])*/;
    for(i = 7; ui_form[i].type; i++)
        ui_form[i].inactive = (i < 53 ? (npcs_tab ? 2 : 0) : (npcs_tab ? 0 : 2));
    if(!npcs_tab) {
        switch(npcs_move.val) {
            case 0:
                ui_form[10].inactive = ui_form[11].inactive = 1;
                ui_form[12].inactive = ui_form[13].inactive = 2;
            break;
            case 1:
                ui_form[10].inactive = ui_form[11].inactive = 0;
                ui_form[12].inactive = ui_form[13].inactive = 2;
            break;
            case 2:
                ui_form[12].inactive = ui_form[13].inactive = 0;
                ui_form[10].inactive = ui_form[11].inactive = 2;
            break;
        }
        ui_form[15].inactive = (npcs_event.val == 2 ? 0 : 2);
        ui_form[16].inactive = (npcs_event.val == 3 ? 0 : 2);
        ui_form[17].inactive = (npcs_event.val == 13 ? 0 : 2);
        ui_form[18].inactive = (npcs_event.val == 14 ? 0 : 2);
        ui_form[19].inactive = (npcs_event.val == 15 ? 0 : 2);
        ui_form[20].inactive = (npcs_event.val == 16 ? 0 : 2);
        ui_form[14].w = (npcs_event.val == 2 || npcs_event.val == 3 ? ui_form[7].w - ui_form[15].w - 4 :
            (npcs_event.val == 16 ? ui_form[7].w - ui_form[20].w - 4 :
            (npcs_event.val >= 13 ? ui_form[7].w / 2 - 2 : ui_form[7].w)));
        for(i = 0; i < 17; i++)
            ui_form[i + 22].inactive = (npcs_event.val == i ? 0 : 2);
        ui_form[40].inactive = (npcs_attrs.val < 0);
        ui_form[46].inactive = (npcs_sprs.val < 0);
        ui_form[47].inactive = (npcs_sprs.val < SPRPERLYR);
        ui_form[48].inactive = (npcs_sprs.val < 0 || npcs_sprs.val >= npcs_sprs.num - SPRPERLYR);
        ui_form[50].inactive = (!ui_sprite_clpbrd || !ui_sprite_clpbrd_num);
        ui_form[51].inactive = ui_form[52].inactive = (!npcs_sprs.num);
        ui_form[42].inactive = 2;
    } else {
        ui_form[56].inactive = (npcs_invtbl.val < 0);
    }
}

/**
 * Erase an attribute from npc
 */
void npcs_attrdel(void *data)
{
    chars_attrs_t *attrs = (chars_attrs_t*)npcs_attrs.data;

    (void)data;
    if(!attrs || npcs_attrs.val < 0 || npcs_attrs.val >= npcs_attrs.num) return;
    memcpy(&attrs[npcs_attrs.val], &attrs[npcs_attrs.val + 1], (npcs_attrs.num - npcs_attrs.val) * sizeof(chars_attrs_t));
    npcs_attrs.num--;
    if(npcs_attrs.val >= npcs_attrs.num)
        npcs_attrs.val = npcs_attrs.num - 1;
    if(!npcs_attrs.num) { free(attrs); npcs_attrs.data = NULL; }
}

/**
 * Add a new attribute to npc
 */
void npcs_attradd(void *data)
{
    chars_attrs_t *attrs = (chars_attrs_t*)npcs_attrs.data;
    int i;

    (void)data;
    for(i = 0; attrs && i < npcs_attrs.num; i++)
        if(attrs[i].attr == npcs_attr.val) {
            npcs_attrs.val = i;
            attrs[i].rel = npcs_rel.val;
            attrs[i].val = npcs_val.val;
            return;
        }
    attrs = (chars_attrs_t*)realloc(attrs, (npcs_attrs.num + 1) * sizeof(chars_attrs_t));
    npcs_attrs.data = attrs;
    if(!attrs) main_error(ERR_MEM);
    i = npcs_attrs.val = npcs_attrs.num++;
    attrs[i].rel = npcs_rel.val;
    attrs[i].val = npcs_val.val;
    attrs[i].attr = npcs_attr.val;
}

/**
 * Return an attribute value
 */
int npcs_attrget(int attr)
{
    chars_attrs_t *attrs = (chars_attrs_t*)npcs_attrs.data;
    int i;

    if(attr < 0) return 0;
    for(i = 0; attrs && i < npcs_attrs.num; i++)
        if(attrs[i].rel == -1 && attrs[i].attr == attr)
            return attrs[i].val;
    return 0;
}

/**
 * Erase a sprite layer from npc
 */
void npcs_lyrdel(void *data)
{
    int *lyrs = (int*)npcs_sprs.data, i;

    (void)data;
    if(!lyrs || npcs_sprs.val < 0 || npcs_sprs.val >= npcs_sprs.num) return;
    i = npcs_sprs.val - (npcs_sprs.val % SPRPERLYR);
    memcpy(&lyrs[i], &lyrs[i + SPRPERLYR], (npcs_sprs.num - i - SPRPERLYR) * sizeof(int));
    npcs_sprs.num -= SPRPERLYR;
    if(npcs_sprs.val >= npcs_sprs.num)
        npcs_sprs.val -= SPRPERLYR;
    if(!npcs_sprs.num) { free(lyrs); npcs_sprs.data = NULL; }
}

/**
 * Add a new sprite layer to npc
 */
void npcs_lyradd(void *data)
{
    int *lyrs;
    int i;

    (void)data;
    lyrs = (int*)realloc(npcs_sprs.data, (npcs_sprs.num + SPRPERLYR) * sizeof(int));
    npcs_sprs.data = lyrs;
    if(!lyrs) main_error(ERR_MEM);
    npcs_sprs.val = npcs_sprs.num;
    for(i = 0; i < SPRPERLYR; i++) lyrs[npcs_sprs.val + i] = -1;
    npcs_sprs.num += SPRPERLYR;
}

/**
 * Click on a sprite on a layer, pop up sprite picker
 */
void npcs_lyrclk(void *data)
{
    int *lyrs = (int*)npcs_sprs.data;

    (void)data;
    if(!lyrs || npcs_sprs.val < 0 || npcs_sprs.val >= npcs_sprs.num) return;
    if(ui_curbtn == 1) {
        ui_pressed = -1;
        ui_sprite_ptr = &lyrs[npcs_sprs.val];
        ui_sprite_picker(scr_w, scr_h, (npcs_sprs.val % SPRPERLYR) == SPRPERLYR - 1 ? &npcs_spr_hidden2 : &npcs_spr_hidden);
    } else
        lyrs[npcs_sprs.val] = -1;
}

/**
 * Move sprite layer up
 */
void npcs_lyrup(void *data)
{
    int *lyrs = (int*)npcs_sprs.data, i, tmp[SPRPERLYR];

    (void)data;
    if(!lyrs || npcs_sprs.val < SPRPERLYR || npcs_sprs.val >= npcs_sprs.num) return;
    i = npcs_sprs.val - (npcs_sprs.val % SPRPERLYR);
    memcpy(&tmp, &lyrs[i - SPRPERLYR], SPRPERLYR * sizeof(int));
    memcpy(&lyrs[i - SPRPERLYR], &lyrs[i], SPRPERLYR * sizeof(int));
    memcpy(&lyrs[i], &tmp, SPRPERLYR * sizeof(int));
    npcs_sprs.val -= SPRPERLYR;
}

/**
 * Move sprite layer down
 */
void npcs_lyrdown(void *data)
{
    int *lyrs = (int*)npcs_sprs.data, i, tmp[SPRPERLYR];

    (void)data;
    if(!lyrs || npcs_sprs.val < 0 || npcs_sprs.val >= npcs_sprs.num - SPRPERLYR) return;
    i = npcs_sprs.val - (npcs_sprs.val % SPRPERLYR);
    memcpy(&tmp, &lyrs[i + SPRPERLYR], SPRPERLYR * sizeof(int));
    memcpy(&lyrs[i + SPRPERLYR], &lyrs[i], SPRPERLYR * sizeof(int));
    memcpy(&lyrs[i], &tmp, SPRPERLYR * sizeof(int));
    npcs_sprs.val += SPRPERLYR;
}

/**
 * Copy sprite layers to clipboard
 */
void npcs_lyrcopy(void *data)
{
    (void)data;
    if(!npcs_sprs.data || !npcs_sprs.num) return;
    ui_sprite_clpbrd_num = npcs_sprs.num;
    ui_sprite_clpbrd = (int*)realloc(ui_sprite_clpbrd, ui_sprite_clpbrd_num * sizeof(int));
    if(ui_sprite_clpbrd) memcpy(ui_sprite_clpbrd, npcs_sprs.data, ui_sprite_clpbrd_num * sizeof(int));
}

/**
 * Cut sprite layers to clipboard
 */
void npcs_lyrcut(void *data)
{
    (void)data;
    if(!npcs_sprs.data || !npcs_sprs.num) return;
    if(ui_sprite_clpbrd) free(ui_sprite_clpbrd);
    ui_sprite_clpbrd = npcs_sprs.data;
    npcs_sprs.data = NULL;
    ui_sprite_clpbrd_num = npcs_sprs.num;
    npcs_sprs.num = 0;
}

/**
 * Paste sprite layers from clipboard
 */
void npcs_lyrpaste(void *data)
{
    (void)data;
    if(!ui_sprite_clpbrd || !ui_sprite_clpbrd_num) return;
    npcs_sprs.data = realloc(npcs_sprs.data, (npcs_sprs.num + ui_sprite_clpbrd_num) * sizeof(int));
    if(npcs_sprs.data) {
        memcpy((int*)npcs_sprs.data + npcs_sprs.num, ui_sprite_clpbrd, ui_sprite_clpbrd_num * sizeof(int));
        npcs_sprs.val = npcs_sprs.num;
        npcs_sprs.num += ui_sprite_clpbrd_num;
    } else
        npcs_sprs.num = 0;
}

/**
 * Erase an inventory item from npc
 */
void npcs_invdel(void *data)
{
    npcs_inv_t *inv = (npcs_inv_t*)npcs_invtbl.data;

    (void)data;
    if(!inv || npcs_invtbl.val < 0 || npcs_invtbl.val >= npcs_invtbl.num) return;
    memcpy(&inv[npcs_invtbl.val], &inv[npcs_invtbl.val + 1], (npcs_invtbl.num - npcs_invtbl.val) * sizeof(npcs_inv_t));
    npcs_invtbl.num--;
    if(npcs_invtbl.val >= npcs_invtbl.num)
        npcs_invtbl.val = npcs_invtbl.num - 1;
    if(!npcs_invtbl.num) { free(inv); npcs_invtbl.data = NULL; }
}

/**
 * Add a new inventory item to npc
 */
void npcs_invadd(void *data)
{
    npcs_inv_t *inv = (npcs_inv_t*)npcs_invtbl.data;
    int i;

    (void)data;
    for(i = 0; inv && i < npcs_invtbl.num; i++)
        if(inv[i].obj == npcs_obj.val) {
            npcs_invtbl.val = i;
            inv[i].per = npcs_chance.val;
            inv[i].qty = npcs_qty.val;
            inv[i].frq = npcs_freq.val;
            return;
        }
    inv = (npcs_inv_t*)realloc(inv, (npcs_invtbl.num + 1) * sizeof(npcs_inv_t));
    npcs_invtbl.data = inv;
    if(!inv) main_error(ERR_MEM);
    i = npcs_invtbl.val = npcs_invtbl.num++;
    inv[i].per = npcs_chance.val;
    inv[i].qty = npcs_qty.val;
    inv[i].frq = npcs_freq.val;
    inv[i].obj = npcs_obj.val;
}

/**
 * Free resources
 */
void npcs_free()
{
    int i;

    npcs_name[0] = npcs_title[0] = 0;
    npcs_speed.val = npcs_chance.val = 100; npcs_change.val = 1000;
    npcs_path.val = npcs_eventnum1.val = npcs_eventnum2.val = npcs_qty.val = 1;
    npcs_move.val = npcs_event.val = npcs_eventobj[0].val = npcs_eventobj[1].val = npcs_eventobj[2].val = npcs_eventtimers.val =
        npcs_attr.val = npcs_val.val = npcs_obj.val = npcs_freq.val = npcs_tab = 0;
    npcs_rel.val = -1;
    for(i = 0; i < 19; i++)
        ui_cmd_free(&npcs_cmd[i]);
    if(npcs_attrs.data) { free(npcs_attrs.data); npcs_attrs.data = NULL; }
    npcs_attrs.num = 0; npcs_attrs.val = -1;
    if(npcs_sprs.data) { free(npcs_sprs.data); npcs_sprs.data = NULL; }
    npcs_sprs.num = 0; npcs_sprs.val = -1;
    if(npcs_invtbl.data) { free(npcs_invtbl.data); npcs_invtbl.data = NULL; }
    npcs_invtbl.num = 0; npcs_invtbl.val = -1;
}

/**
 * Clear form, new npc
 */
void npcs_new(void *data)
{
    (void)data;
    npcs_free();
}

/**
 * Delete npc
 */
void npcs_delete(void *data)
{
    (void)data;
    if(npcs_tbl.val < 0 || npcs_tbl.val >= project.spritenum[PROJ_NUMSPRCATS - 2]) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.npc", project.id, project_dirs[PROJDIRS_NPCS],
        project.sprites[PROJ_NUMSPRCATS - 2][npcs_tbl.val].name);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("npcs_delete: removing %s\n", projdir);
        remove(projdir);
        sprintf(projfn, "%s" SEP "%s" SEP "%s_npc.png", project.id, project_dirs[PROJDIRS_NPCS],
            project.sprites[PROJ_NUMSPRCATS - 2][npcs_tbl.val].name);
        if(verbose) printf("npcs_delete: removing %s\n", projdir);
        npcs_new(NULL);
        npcs_init(SUBMENU_NPCS);
    }
}

/**
 * Save npc
 */
void npcs_save(void *data)
{
    chars_attrs_t *attrs = (chars_attrs_t*)npcs_attrs.data;
    npcs_inv_t *inv = (npcs_inv_t*)npcs_invtbl.data;
    ui_sprite_t out = { 0 };
    int *sprs = (int*)npcs_sprs.data;
    FILE *f;
    int i, mw = project.tilew, mh = project.tileh;

    (void)data;
    if(!npcs_name[0]) return;
    f = project_savefile(npcs_tbl.val < 0 || npcs_tbl.val >= project.spritenum[PROJ_NUMSPRCATS - 2] ||
        strcmp(project.sprites[PROJ_NUMSPRCATS - 2][npcs_tbl.val].name, npcs_name),
        project_dirs[PROJDIRS_NPCS], npcs_name, "npc", PROJMAGIC_NPC, "npcs_save");
    if(f) {
        fprintf(f, "%s\r\n", npcs_title[0] ? npcs_title : "-");
        fprintf(f, "%s %u %u %u\r\n", npcs_movecode[npcs_move.val], npcs_speed.val, npcs_change.val, npcs_path.val);
        for(i = 0; attrs && i < npcs_attrs.num; i++)
            if(attrs[i].rel < 0)
                fprintf(f, " a %s %d\r\n", project.attrs[attrs[i].attr], attrs[i].val);
            else
                fprintf(f, " r %s%s%d\r\n", project.attrs[attrs[i].attr], project_rels[attrs[i].rel], attrs[i].val);
        for(i = 0; inv && i < npcs_invtbl.num; i++) {
            fprintf(f, "i %u %u %u ", inv[i].per, inv[i].qty, inv[i].frq);
            project_wrsprite2(f, inv[i].obj, 2);
            fprintf(f, "\r\n");
        }
        for(i = 0; sprs && i < npcs_sprs.num; i++) {
            if(!(i % SPRPERLYR)) fprintf(f, "%sl ", i ? "\r\n" : "");
            else fprintf(f, " ");
            project_wrsprite2(f, sprs[i], (i % SPRPERLYR) == SPRPERLYR - 1 ? npcs_spr_hidden2.cat : npcs_spr_hidden.cat);
        }
        if(npcs_sprs.num)
            fprintf(f, "\r\n");
        for(i = 0; i < 19; i++) {
            if(!npcs_cmd[i].root.len) continue;
            fprintf(f, "e %s ", project_eventcodes[i]);
            switch(i) {
                case 2: fprintf(f, "%u ", npcs_eventnum1.val); break;
                case 3: fprintf(f, "%u ", npcs_eventnum2.val); break;
                case 13:
                case 14:
                case 15: project_wrsprite(f, &npcs_eventobj[i - 13]); fprintf(f, " "); break;
                case 16: project_wridx(f, npcs_eventtimers.val, project_eventtimers); break;
            }
            project_wrscript(f, &npcs_cmd[i]);
        }
        fclose(f);
        /* save npc as separate image file too because tmx needs to reference it */
        spr_merge(&out, &mw, &mh, NULL, sprs, 1, 1, npcs_sprs.num, npcs_spr_hidden.cat, 0, SPRPERLYR, -1);
        if(out.data) {
            sprintf(projfn, "%s" SEP "%s" SEP "%s_npc.png", project.id, project_dirs[PROJDIRS_NPCS], npcs_name);
            f = project_fopen(projdir, "wb+");
            if(f) {
                if(image_save(f, mw, mh, mw * 4, (uint8_t*)out.data))
                    spr_add(PROJ_NUMSPRCATS - 2, npcs_name, 0, 0, 1, mw, mh, mw * 4, out.data);
                fclose(f);
            }
            free(out.data);
        }
        npcs_init(SUBMENU_NPCS);
        for(npcs_tbl.val = 0; npcs_tbl.val < npcs_tbl.num &&
            strcmp(npcs_name, project.sprites[PROJ_NUMSPRCATS - 2][npcs_tbl.val].name); npcs_tbl.val++);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.npc", project.id, project_dirs[PROJDIRS_NPCS], npcs_name);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVENPC]);
}

/**
 * Load npc handler
 */
void npcs_load(void *data)
{
    (void)data;
    if(npcs_tbl.val < 0 || npcs_tbl.val >= project.spritenum[PROJ_NUMSPRCATS - 2] || !project.sprites[PROJ_NUMSPRCATS - 2]) return;
    if(!npcs_loadnpc(npcs_tbl.val, 0))
        ui_status(1, lang[ERR_LOADNPC]);
    else
        strncpy(npcs_name, project.sprites[PROJ_NUMSPRCATS - 2][npcs_tbl.val].name, sizeof(npcs_name) - 1);
}

/**
 * Load npc
 */
int npcs_loadnpc(int idx, int onlysprites)
{
    char *str, *s, t;
    int i, *sprs;

    if(idx < 0 || idx >= project.spritenum[PROJ_NUMSPRCATS - 2] || !project.sprites[PROJ_NUMSPRCATS - 2]) return 0;
    str = project_loadfile(project_dirs[PROJDIRS_NPCS], project.sprites[PROJ_NUMSPRCATS - 2][idx].name, "npc",
        PROJMAGIC_NPC, "npcs_loadnpc");
    if(str) {
        npcs_new(NULL);
        s = project_skipnl(str);
        s = project_getstr2(s, npcs_title, 2, sizeof(npcs_title));
        s = project_skipnl(s);
        s = project_getidx(s, &npcs_move.val, (char**)npcs_movecode, 0);
        s = project_getint(s, &npcs_speed.val, npcs_speed.min, npcs_speed.max);
        s = project_getint(s, &npcs_change.val, npcs_change.min, npcs_change.max);
        s = project_getint(s, &npcs_path.val, npcs_path.min, npcs_path.max);
        while(*s) {
            s = project_skipnl(s);
            if(!*s || s[1] != ' ') break;
            t = *s; do { s++; } while(*s == ' ');
            switch(t) {
                case 'a':
                    npcs_rel.val = -1;
                    s = project_getidx(s, &npcs_attr.val, project.attrs, 0);
                    s = project_getint(s, &npcs_val.val, npcs_val.min, npcs_val.max);
                    npcs_attradd(NULL);
                break;
                case 'r':
                    s = project_getidx(s, &npcs_attr.val, project.attrs, 0);
                    s = project_getrel(s, &npcs_rel.val);
                    s = project_getint(s, &npcs_val.val, npcs_val.min, npcs_val.max);
                    npcs_attradd(NULL);
                break;
                case 'l':
                    npcs_lyradd(NULL);
                    sprs = (int*)npcs_sprs.data;
                    if(sprs && npcs_sprs.num > 0)
                        for(i = npcs_sprs.num - SPRPERLYR; *s && i < npcs_sprs.num; i++) {
                            s = project_getsprite(s, (i % SPRPERLYR) == SPRPERLYR - 1 ? &npcs_spr_hidden2 : &npcs_spr_hidden);
                            sprs[i] = (i % SPRPERLYR) == SPRPERLYR - 1 ? npcs_spr_hidden2.val : npcs_spr_hidden.val;
                        }
                break;
                case 'i':
                    if(onlysprites) goto end;
                    s = project_getint(s, &npcs_chance.val, npcs_chance.min, npcs_chance.max);
                    s = project_getint(s, &npcs_qty.val, npcs_qty.min, npcs_qty.max);
                    s = project_getint(s, &npcs_freq.val, npcs_freq.min, npcs_freq.max);
                    s = project_getobj(s, &npcs_obj.val, 0);
                    npcs_invadd(NULL);
                break;
                case 'e':
                    if(onlysprites) goto end;
                    s = project_getidx(s, &i, (char**)project_eventcodes, -1);
                    if(i >= 0) {
                        switch(i) {
                            case 2: s = project_getint(s, &npcs_eventnum1.val, npcs_eventnum1.min, npcs_eventnum1.max); break;
                            case 3: s = project_getint(s, &npcs_eventnum2.val, npcs_eventnum2.min, npcs_eventnum2.max); break;
                            case 13:
                            case 14:
                            case 15: s = project_getobj(s, &npcs_eventobj[i - 13].val, 0); break;
                            case 16: s = project_getidx(s, &npcs_eventtimers.val, project_eventtimers, 0); break;
                        }
                        s = project_getscript(s, &npcs_cmd[i]);
                    }
                break;
            }
        }
end:    npcs_event.val =  npcs_attr.val = npcs_val.val = npcs_freq.val = npcs_obj.val = npcs_tab = 0; npcs_rel.val = -1;
        npcs_chance.val = 100; npcs_qty.val = 1;
        free(str);
        return 1;
    }
    return 0;
}

/**
 * Generate all sprites in all directions
 */
ui_sprite_t *npcs_loadsprites()
{
    int i;
    ui_sprite_t *ret;

    ret = (ui_sprite_t*)main_alloc(8 * SPRPERLYR * sizeof(ui_sprite_t));
    for(i = 0; i < SPRPERLYR; i++)
        spr_merge(&ret[i * 8], NULL, NULL, NULL, npcs_sprs.data, 8, 0, npcs_sprs.num, npcs_spr_hidden.cat, i, SPRPERLYR, -1);
    return ret;
}

/**
 * Load strings for translation
 */
void npcs_loadstr()
{
    char *str, *s;
    int i;

    for(i = 0; i < project.spritenum[PROJ_NUMSPRCATS - 2]; i++) {
        str = project_loadfile(project_dirs[PROJDIRS_NPCS], project.sprites[PROJ_NUMSPRCATS - 2][i].name, "npc",
            PROJMAGIC_NPC, "npcs_loadstr");
        if(str) {
            s = project_skipnl(str);
            s = project_gettrstr(s, 2, sizeof(npcs_title));
            free(str);
        }
    }
}

/**
 * Preview npc
 */
void npcs_preview(void *data)
{
    npcs_inv_t *inv = (npcs_inv_t*)npcs_invtbl.data;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *texture = NULL, *portrait = NULL;
    SDL_Rect psrc, pdst, dst;
    int wf = SDL_GetWindowFlags(window), i, j, k, m, x, y, p, top, left, bottom, w, h, scw, sch, slw, slh;
    uint32_t *pixels;
    ui_sprite_t *s = NULL, *spr[64], ps[8];
    ui_tablehdr_t hdr[] = {
        { NPCS_TITLESTR, 0, 0, 0 },
        { 0 }
    };

    (void)data;
    if(verbose) printf("npcs_preview: started\n");
    if(!npcs_tab) {
        chars_sprites(npcs_sprs.data, npcs_sprs.num, npcs_spr_hidden.cat, npcs_sprs.val % SPRPERLYR);
    } else {
        memset(ps, 0, sizeof(ps));
        spr_merge(ps, &psrc.w, &psrc.h, NULL, npcs_sprs.data, 1, 0, npcs_sprs.num, npcs_spr_hidden2.cat, SPRPERLYR - 1, SPRPERLYR, -1);
        if(ps[0].data) {
            spr_texture(&ps[0], &portrait);
            free(ps[0].data);
        }
        memset(spr, 0, sizeof(spr));
        lang[NPCS_TITLESTR] = npcs_title;
        SDL_GetDesktopDisplayMode(0, &dm);
        SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
        SDL_ShowCursor(SDL_DISABLE);
        SDL_RenderClear(renderer);
        SDL_RenderPresent(renderer);
        ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

        hud_load();
        texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
        if(texture) {
            SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
            /* checker */
            for(j = k = 0; j < dm.h; j++, k += p/4)
                for(i = 0; i < dm.w ; i++) {
                    x = (i >> 5); y = (j >> 5);
                    pixels[k + i] = theme[(y & 1) ^ (x & 1) ? THEME_LIGHT : THEME_DARK];
                }
            SDL_UnlockTexture(texture);
            elements_load();
            elements_loadfonts();
            elements_winsizes(hdr, 0, &w, &top, NULL, &bottom, &left);
            y = 32; left += 10; scw = elements_vscrsize();
            s = spr_getsel(&hud_invimg, 0); slw = s->w; slh = s->h;
            s = spr_getsel(&hud_invsel, 0); if(slw < s->w) { slw = s->w; } if(slh < s->h) slh = s->h;
            slw += 4; slh += 4;
            i = ((dm.w / 3) - 2 * elements_pad[4].val - scw) / slw;
            i = i * slw + 2 * elements_pad[4].val + scw;
            if(i > w) w = i;
            s = spr_getsel(&hud_invequip, 0);
            if(s->w > w) w = s->w;
            y += top; i = (portrait ? s->h + elements_pad[3].val : 0);
            sch = (dm.h * 3 / 4 - y - bottom - i - 2 * elements_pad[3].val) / slh;
            h = sch * slh + i + 2 * elements_pad[3].val;
            x = left;
            elements_window(texture, dm.w, dm.h, x, y, w, h, hdr, 0);
            elements_freefonts();
            y += elements_pad[3].val;
            if(portrait) {
                ui_fit(w, s->h, psrc.w, psrc.h, &pdst.w, &pdst.h);
                pdst.x = left + (w - pdst.w) / 2; pdst.y = y; psrc.x = psrc.y = 0;
                y += s->h + elements_pad[3].val;
            }
            elements_vscr(texture, left + w - elements_pad[4].val, y, sch * slh, 0);
            x += elements_pad[4].val;
            w = (w - 2 * elements_pad[4].val) / slw;
            for(k = m = 0; m < sch; y += slh, m++)
                for(p = 0; p < w; p++, k++) {
                    s = spr_getsel(!k ? &hud_invsel : &hud_invimg, 0);
                    dst.x = x + p * slw; dst.y = y;
                    ui_blitbuf(texture, dst.x + (slw - s->w) / 2, dst.y + (slh - s->h) / 2, s->w, s->h, s->data, 0, 0, s->w, s->h,
                        s->w * s->nframe * 4);
                    if(inv && k < npcs_invtbl.num && inv[k].obj >= 0 && inv[k].obj < project.spritenum[1]) {
                        s = spr_getidx(2, inv[k].obj, -1, 0);
                        if(s) {
                            ui_fit(hud_invw.val, hud_invh.val, s->w, s->h, &dst.w, &dst.h);
                            dst.x += (slw - dst.w) / 2; dst.y += (slh - dst.h) / 2;
                            spr_blit(s, 255, texture, &dst);
                        } else {
                            dst.w = slw; dst.h = slh;
                            spr_blit(&ui_spr_none, 255, texture, &dst);
                        }
                        break;
                    }
                }
        }

        while(1) {
            /* events */
            SDL_WaitEvent(&event);
            if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
                SDL_PushEvent(&event); goto cleanup;
            }
            switch(event.type) {
                case SDL_KEYUP: goto cleanup; break;
                case SDL_MOUSEBUTTONUP: goto cleanup; break;
            }
            /* render */
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
            SDL_RenderClear(renderer);
            if(texture) SDL_RenderCopy(renderer, texture, NULL, NULL);
            if(portrait) SDL_RenderCopyEx(renderer, portrait, &psrc, &pdst, 0, NULL, SDL_FLIP_HORIZONTAL);
            SDL_RenderPresent(renderer);
        }

cleanup:
        hud_exit(SUBMENU_HUD);
        if(portrait) SDL_DestroyTexture(portrait);
        if(texture) SDL_DestroyTexture(texture);
        SDL_ShowCursor(SDL_ENABLE);
        SDL_SetWindowFullscreen(window, 0);
        if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
    }
    if(verbose) printf("npcs_preview: stopped\n");
}

/**
 * Get references
 */
void npcs_ref(tngctx_t *ctx, int idx)
{
    ui_cmd_t cmd = { 0 };
    char *str, *s, t;
    int i, j;

    if(!tng_sprref(ctx, PROJ_NUMSPRCATS - 2, idx)) return;
    str = project_loadfile(project_dirs[PROJDIRS_NPCS], project.sprites[PROJ_NUMSPRCATS - 2][idx].name, "npc",
        PROJMAGIC_NPC, "npcs_ref");
    if(str) {
        s = project_skipnl(str);
        s = project_skipnl(s);
        while(*s) {
            s = project_skipnl(s);
            if(!*s || s[1] != ' ') break;
            t = *s; do { s++; } while(*s == ' ');
            switch(t) {
                case 'a':
                case 'r':
                    s = project_getidx(s, &j, project.attrs, 0);
                    attrs_ref(ctx, j);
                break;
                case 'i':
                    s = project_skiparg(s, 3);
                    s = project_getobj(s, &j, -1);
                    objects_ref(ctx, j);
                break;
                case 'e':
                    s = project_getidx(s, &i, (char**)project_eventcodes, -1);
                    if(i >= 0) {
                        switch(i) {
                            case 13:
                            case 14:
                            case 15:
                                s = project_getobj(s, &j, -1);
                                objects_ref(ctx, j);
                            break;
                        }
                        s = project_getscript(s, &cmd);
                        ui_cmd_ref(ctx, &cmd.root, 0);
                        ui_cmd_free(&cmd);
                    }
                break;
            }
        }
        free(str);
    }
}

/**
 * Save tng
 */
int npcs_totng(tngctx_t *ctx)
{
    chars_attrs_t *attrs;
    npcs_inv_t *inv;
    int *sprs, sprids[SPRPERLYR];
    ui_bc_t bc[18];
    uLongf cl;
    uint8_t *buf, *ptr, *comp;
    int i, j, k, l, m, err;

    if(!ctx || !project.sprites[PROJ_NUMSPRCATS - 2] || !project.spritenum[PROJ_NUMSPRCATS - 2]) return 1;
    /* check if there's at least one NPC referenced */
    for(j = 0; j < project.spritenum[PROJ_NUMSPRCATS - 2] && project.sprites[PROJ_NUMSPRCATS - 2][j].idx == -1; j++);
    if(j >= project.spritenum[PROJ_NUMSPRCATS - 2]) return 1;
    /* add NPCs to output */
    tng_section(ctx, TNG_SECTION_NPCS);
    for(m = 0; m < project.spritenum[PROJ_NUMSPRCATS - 2]; m++) {
        for(j = 0; j < project.spritenum[PROJ_NUMSPRCATS - 2] && project.sprites[PROJ_NUMSPRCATS - 2][j].idx != m; j++);
        if(j >= project.spritenum[PROJ_NUMSPRCATS - 2]) break;
        if(!npcs_loadnpc(j, 0)) {
            ui_switchtab(SUBMENU_NPCS);
            npcs_tbl.val = j;
            npcs_tab = 0;
            ui_status(1, lang[ERR_LOADNPC]);
            return 0;
        }
        attrs = (chars_attrs_t*)npcs_attrs.data;
        inv = (npcs_inv_t*)npcs_invtbl.data;
        sprs = (int*)npcs_sprs.data;
        memset(&bc, 0, sizeof(bc)); err = 0;
        l = 17 + npcs_attrs.num * 8 + npcs_invtbl.num * 10 + SPRPERLYR * 3;
        for(i = k = 0; i < 19; i++)
            if(npcs_cmd[i].root.len) {
                ui_cmd_tobc(ctx, &bc[i], &npcs_cmd[i].root, 0, &err);
                if(err) {
                    for(k = 0; k <= i; k++)
                        if(bc[k].data) free(bc[k].data);
                    ui_switchtab(SUBMENU_NPCS);
                    npcs_tbl.val = j;
                    npcs_event.val = i < 17 ? i : 0;
                    npcs_tab = i < 17 ? 0 : 1;
                    ui_status(1, lang[ERR_SCRIPT]);
                    return 0;
                }
                if(bc[i].data && bc[i].len) {
                    l += 7 + bc[i].len;
                    k++;
                }
            }
        buf = ptr = (uint8_t*)main_alloc(l);
        ptr = tng_asset_text(ptr, ctx, npcs_title);
        *ptr++ = npcs_move.val;
        memcpy(ptr, &npcs_speed.val, 2); ptr += 2;
        memcpy(ptr, &npcs_change.val, 2); ptr += 2;
        *ptr++ = npcs_path.val;
        *ptr++ = npcs_attrs.num;
        *ptr++ = npcs_invtbl.num;
        *ptr++ = SPRPERLYR;
        *ptr++ = k;
        for(i = 0; attrs && i < npcs_attrs.num; i++) {
            *ptr++ = attrs[i].rel < 0 ? 0xff : attrs[i].rel;
            ptr = tng_asset_idx(ptr, ctx, project.attrs, TNG_IDX_ATR, attrs[i].attr);
            memcpy(ptr, &attrs[i].val, 4); ptr += 4;
        }
        for(i = 0; inv && i < npcs_invtbl.num; i++) {
            *ptr++ = inv[i].per;
            memcpy(ptr, &inv[i].qty, 3); ptr += 3;
            memcpy(ptr, &inv[i].frq, 3); ptr += 3;
            ptr = tng_asset_sprite(ptr, ctx, 2, inv[i].obj);
        }
        spr_temp(sprids, sprs, npcs_sprs.num, npcs_spr_hidden.cat, SPRPERLYR);
        for(i = 0; i < SPRPERLYR; i++)
            ptr = tng_asset_sprite(ptr, ctx, i == SPRPERLYR - 1 ? npcs_spr_hidden2.cat : npcs_spr_hidden.cat, sprids[i]);
        for(i = 0; i < 19; i++) {
            if(!npcs_cmd[i].root.len || !bc[i].data || !bc[i].len) continue;
            *ptr++ = i;
            switch(i) {
                case 2: memcpy(ptr, &npcs_eventnum1.val, 3); ptr += 3; break;
                case 3: memcpy(ptr, &npcs_eventnum2.val, 3); ptr += 3; break;
                case 13:
                case 14:
                case 15: ptr = tng_asset_sprite(ptr, ctx, 2, npcs_eventobj[i - 13].val); break;
                case 16: memcpy(ptr, &npcs_eventtimers.val, 3); ptr += 3; break;
                default: *ptr++ = 0; *ptr++ = 0; *ptr++ = 0; break;
            }
            memcpy(ptr, &bc[i].len, 3); ptr += 3;
            memcpy(ptr, bc[i].data, bc[i].len); ptr += bc[i].len;
            free(bc[i].data);
        }
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        if(cl) {
            tng_desc(ctx, project.sprites[PROJ_NUMSPRCATS - 2][j].name, cl);
            tng_asset_int(ctx, comp, cl);
        } else
            free(comp);
        free(buf);
        /* free resources */
        npcs_new(NULL);
    }
    return 1;
}

/**
 * Read from tng
 */
int npcs_fromtng(tngctx_t *ctx)
{
    FILE *f;
    tng_asset_desc_t *asset;
    uint8_t *buf, *ptr, *end;
    int i, j, l, len, na, ni, ns, ne, r;
    ui_sprite_t out = { 0 };
    int *sprs = NULL;
    int mw = project.tilew, mh = project.tileh;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_NPCS; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;
    for(j = 0; j < len; j++, asset++) {
        ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
        buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &l);
        if(buf) {
            if(!asset->name || asset->name >= TNG_SECTION_SIZE(&ctx->tbl[0]) || l < 17) { free(buf); continue; }
            end = buf + l;
            f = project_savefile(0, project_dirs[PROJDIRS_NPCS], (char*)ctx->sts + asset->name, "npc", PROJMAGIC_NPC,
                "npcs_fromtng");
            if(f) {
                ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                fprintf(f, "%s %u %u %u\r\n", npcs_movecode[ptr[0]], *((uint16_t*)&ptr[1]), *((uint16_t*)&ptr[3]), ptr[5]);
                ptr += 6;
                na = *ptr++;
                ni = *ptr++;
                ns = *ptr++;
                ne = *ptr++;
                for(i = 0; i < na && ptr < end; i++) {
                    r = *ptr++;
                    if(r == 0xff) {
                        fprintf(f, "a ");
                        ptr = tng_wridx(ctx, ptr, f); fprintf(f, " ");
                    } else {
                        fprintf(f, "r ");
                        ptr = tng_wridx(ctx, ptr, f);
                        fprintf(f, "%s", project_rels[r]);
                    }
                    ptr = tng_wrnum(ctx, ptr, 4, f);  fprintf(f, "\r\n");
                }
                for(i = 0; i < ni && ptr < end; i++) {
                    fprintf(f, "i %u ", ptr[0]);
                    ptr = tng_wrnum(ctx, ptr + 1, 3, f); fprintf(f, " ");
                    ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " ");
                    ptr = tng_wrsprite(ctx, ptr, f); fprintf(f, "\r\n");
                }
                if(ns) {
                    sprs = (int*)main_alloc(ns * sizeof(int));
                    for(i = 0; i < ns && ptr < end; i++) {
                        if(!(i % SPRPERLYR)) fprintf(f, "%sl ", i ? "\r\n" : "");
                        else fprintf(f, " ");
                        memcpy(&sprs[i], ptr + 1, 2);
                        ptr = tng_wrsprite(ctx, ptr, f);
                    }
                    fprintf(f, "\r\n");
                }
                for(i = 0; i < ne && ptr < end; i++) {
                    r = *ptr++;
                    fprintf(f, "e %s ", project_eventcodes[r]);
                    switch(r) {
                        case 2: ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " "); break;
                        case 3: ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " "); break;
                        case 13:
                        case 14:
                        case 15: ptr = tng_wrsprite(ctx, ptr, f); fprintf(f, " "); break;
                        case 16: r = 0; memcpy(&r, ptr, 3); ptr += 3; fprintf(f, "%s ", project_eventtimers[r]); break;
                        default: ptr += 3; break;
                    }
                    r = 0; memcpy(&r, ptr, 3); ptr += 3;
                    ptr = tng_wrscript(ctx, ptr, r, f);
                }
                fclose(f);
                if(sprs && ns) {
                    memset(&out, 0, sizeof(out));
                    spr_merge(&out, &mw, &mh, NULL, sprs, 1, 1, ns, npcs_spr_hidden.cat, 0, SPRPERLYR, -1);
                    if(out.data) {
                        sprintf(projfn, "%s" SEP "%s" SEP "%s_npc.png", project.id, project_dirs[PROJDIRS_NPCS],
                            (char*)ctx->sts + asset->name);
                        f = project_fopen(projdir, "wb+");
                        if(f) {
                            if(verbose) printf("npcs_fromtng: saving %s\n", projdir);
                            if(image_save(f, mw, mh, mw * 4, (uint8_t*)out.data))
                                spr_add(PROJ_NUMSPRCATS - 2, (char*)ctx->sts + asset->name, 0, 0, 1, mw, mh, mw * 4, out.data);
                            fclose(f);
                        }
                        free(out.data);
                    }
                    free(sprs); sprs = NULL;
                }
            }
            free(buf);
        }
    }
    return 1;
}
