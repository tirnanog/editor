/*
 * tnge/alert.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Quest alert configuration window
 *
 */

#include "main.h"

void alerts_load();
void alerts_save(void *data);
void alerts_delete(void *data);
void alerts_preview(void *data);

int alerts_tab, alerts_style = 0, alerts_fadein = 0, alerts_fadeout = 0;
char alerts_texts[16][PROJ_TITLEMAX];
char *alerts_opts[] = { alerts_texts[2], alerts_texts[3], alerts_texts[4], alerts_texts[5], alerts_texts[6], alerts_texts[7],
    alerts_texts[8], alerts_texts[9], alerts_texts[10], alerts_texts[11], alerts_texts[12], alerts_texts[13], alerts_texts[14],
    alerts_texts[15], NULL };
uint32_t alerts_color[16];
ui_icontgl_t alerts_bold = { ICON_BOLD, SSFN_STYLE_BOLD, &alerts_style };
ui_icontgl_t alerts_italic = { ICON_ITALIC, SSFN_STYLE_ITALIC, &alerts_style };
ui_num_t alerts_size = { 32, 16, 192, 1 };
ui_select_t alerts_font = { -1, LANG_DEF, NULL };
ui_num_t alerts_dur = { 100, 100, 3000, 10 };
ui_fade_t alerts_fade = { &alerts_fadein, &alerts_fadeout, NULL, &alerts_dur.val, 0, 0 };
ui_select_t alerts_snd[16] = {
    { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL },
    { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL },
    { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL },
    { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL }, { -1, LANG_NONE, NULL } };
ui_input_t alerts_inp[16] = {
    { INP_NAME, sizeof(alerts_texts[0]), alerts_texts[0] },
    { INP_NAME, sizeof(alerts_texts[1]), alerts_texts[1] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[2] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[3] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[4] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[5] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[6] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[7] },
    { INP_NAME, sizeof(alerts_texts[0]), alerts_texts[8] },
    { INP_NAME, sizeof(alerts_texts[1]), alerts_texts[9] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[10] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[11] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[12] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[13] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[14] },
    { INP_NAME, sizeof(alerts_texts[2]), alerts_texts[15] }
};

/**
 * The form
 */
ui_form_t alerts_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, ALERT_DELETE, (void*)ICON_REMOVE, alerts_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, alerts_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, ALERT_PREVIEW, (void*)ICON_PVIEW, alerts_preview },
    /* 3 */
    { FORM_TEXT, 10, 29, 0, 18, 0, 0, NULL, NULL },
    { FORM_ICONTGL, 0, 29, 20, 20, 0, FONTS_BOLD, &alerts_bold, NULL },
    { FORM_ICONTGL, 0, 29, 20, 20, 0, FONTS_ITALIC, &alerts_italic, NULL },
    { FORM_NUM, 0, 29, 42, 20, 0, FONTS_SIZE, &alerts_size, NULL },
    { FORM_SELECT, 0, 29, 0, 20, 0, FONTS_FAMILY, &alerts_font, NULL },
    /* 8 */
    { FORM_TEXT, 10, 53, 0, 20, 0, 0, NULL, NULL },
    { FORM_TIME, 0, 53, 56, 20, 0, 0, &alerts_dur, NULL },
    { FORM_FADE, 0, 53, 0, 20, 0, 0, &alerts_fade, NULL },
    /* 11 */
    { FORM_TEXT, 10, 96, 0, 18, 0, 0, NULL, NULL },
    { FORM_COLOR, 0, 96, 20, 20, 0, 0, &alerts_color[0], NULL },
    { FORM_INPUT, 0, 96, 0, 20, 0, 0, &alerts_inp[0], NULL },
    { FORM_SNDSEL, 0, 96, 200, 20, 0, 0, &alerts_snd[0], NULL },
    /* 15 */
    { FORM_TEXT, 10, 128, 0, 18, 0, 0, NULL, NULL },
    { FORM_COLOR, 0, 128, 20, 20, 0, 0, &alerts_color[1], NULL },
    { FORM_INPUT, 0, 128, 0, 20, 0, 0, &alerts_inp[1], NULL },
    { FORM_SNDSEL, 0, 128, 200, 20, 0, 0, &alerts_snd[1], NULL },
    /* 19 */
    { FORM_TEXT, 10, 160, 0, 18, 0, 0, NULL, NULL },
    { FORM_COLOR, 0, 160, 20, 20, 0, 0, &alerts_color[2], NULL },
    { FORM_INPUT, 0, 160, 0, 20, 0, 0, &alerts_inp[2], NULL },
    { FORM_SNDSEL, 0, 160, 200, 20, 0, 0, &alerts_snd[2], NULL },
    /* 23 */
    { FORM_TEXT, 10, 192, 0, 18, 0, 0, "#3", NULL },
    { FORM_COLOR, 0, 192, 20, 20, 0, 0, &alerts_color[3], NULL },
    { FORM_INPUT, 0, 192, 0, 20, 0, 0, &alerts_inp[3], NULL },
    { FORM_SNDSEL, 0, 192, 200, 20, 0, 0, &alerts_snd[3], NULL },
    /* 27 */
    { FORM_TEXT, 10, 224, 0, 18, 0, 0, "#4", NULL },
    { FORM_COLOR, 0, 224, 20, 20, 0, 0, &alerts_color[4], NULL },
    { FORM_INPUT, 0, 224, 0, 20, 0, 0, &alerts_inp[4], NULL },
    { FORM_SNDSEL, 0, 224, 200, 20, 0, 0, &alerts_snd[4], NULL },
    /* 32 */
    { FORM_TEXT, 10, 256, 0, 18, 0, 0, "#5", NULL },
    { FORM_COLOR, 0, 256, 20, 20, 0, 0, &alerts_color[5], NULL },
    { FORM_INPUT, 0, 256, 0, 20, 0, 0, &alerts_inp[5], NULL },
    { FORM_SNDSEL, 0, 256, 200, 20, 0, 0, &alerts_snd[5], NULL },
    /* 36 */
    { FORM_TEXT, 10, 288, 0, 18, 0, 0, "#6", NULL },
    { FORM_COLOR, 0, 288, 20, 20, 0, 0, &alerts_color[6], NULL },
    { FORM_INPUT, 0, 288, 0, 20, 0, 0, &alerts_inp[6], NULL },
    { FORM_SNDSEL, 0, 288, 200, 20, 0, 0, &alerts_snd[6], NULL },
    /* 40 */
    { FORM_TEXT, 10, 320, 0, 18, 0, 0, "#7", NULL },
    { FORM_COLOR, 0, 320, 20, 20, 0, 0, &alerts_color[7], NULL },
    { FORM_INPUT, 0, 320, 0, 20, 0, 0, &alerts_inp[7], NULL },
    { FORM_SNDSEL, 0, 320, 200, 20, 0, 0, &alerts_snd[7], NULL },
    /* 44 */
    { FORM_TEXT, 10, 352, 0, 18, 0, 0, "#8", NULL },
    { FORM_COLOR, 0, 352, 20, 20, 0, 0, &alerts_color[8], NULL },
    { FORM_INPUT, 0, 352, 0, 20, 0, 0, &alerts_inp[8], NULL },
    { FORM_SNDSEL, 0, 352, 200, 20, 0, 0, &alerts_snd[8], NULL },
    /* 48 */
    { FORM_TEXT, 10, 384, 0, 18, 0, 0, "#9", NULL },
    { FORM_COLOR, 0, 384, 20, 20, 0, 0, &alerts_color[9], NULL },
    { FORM_INPUT, 0, 384, 0, 20, 0, 0, &alerts_inp[9], NULL },
    { FORM_SNDSEL, 0, 384, 200, 20, 0, 0, &alerts_snd[9], NULL },
    /* 52 */
    { FORM_TEXT, 10, 416, 0, 18, 0, 0, "#10", NULL },
    { FORM_COLOR, 0, 416, 20, 20, 0, 0, &alerts_color[10], NULL },
    { FORM_INPUT, 0, 416, 0, 20, 0, 0, &alerts_inp[10], NULL },
    { FORM_SNDSEL, 0, 416, 200, 20, 0, 0, &alerts_snd[10], NULL },
    /* 56 */
    { FORM_TEXT, 10, 448, 0, 18, 0, 0, "#11", NULL },
    { FORM_COLOR, 0, 448, 20, 20, 0, 0, &alerts_color[11], NULL },
    { FORM_INPUT, 0, 448, 0, 20, 0, 0, &alerts_inp[11], NULL },
    { FORM_SNDSEL, 0, 448, 200, 20, 0, 0, &alerts_snd[11], NULL },
    /* 60 */
    { FORM_TEXT, 10, 480, 0, 18, 0, 0, "#12", NULL },
    { FORM_COLOR, 0, 480, 20, 20, 0, 0, &alerts_color[12], NULL },
    { FORM_INPUT, 0, 480, 0, 20, 0, 0, &alerts_inp[12], NULL },
    { FORM_SNDSEL, 0, 480, 200, 20, 0, 0, &alerts_snd[12], NULL },
    /* 64 */
    { FORM_TEXT, 10, 512, 0, 18, 0, 0, "#13", NULL },
    { FORM_COLOR, 0, 512, 20, 20, 0, 0, &alerts_color[13], NULL },
    { FORM_INPUT, 0, 512, 0, 20, 0, 0, &alerts_inp[13], NULL },
    { FORM_SNDSEL, 0, 512, 200, 20, 0, 0, &alerts_snd[13], NULL },
    /* 68 */
    { FORM_TEXT, 10, 544, 0, 18, 0, 0, "#14", NULL },
    { FORM_COLOR, 0, 544, 20, 20, 0, 0, &alerts_color[14], NULL },
    { FORM_INPUT, 0, 544, 0, 20, 0, 0, &alerts_inp[14], NULL },
    { FORM_SNDSEL, 0, 544, 200, 20, 0, 0, &alerts_snd[14], NULL },
    /* 72 */
    { FORM_TEXT, 10, 576, 0, 18, 0, 0, "#15", NULL },
    { FORM_COLOR, 0, 576, 20, 20, 0, 0, &alerts_color[15], NULL },
    { FORM_INPUT, 0, 576, 0, 20, 0, 0, &alerts_inp[15], NULL },
    { FORM_SNDSEL, 0, 576, 200, 20, 0, 0, &alerts_snd[15], NULL },

    { FORM_LAST }
};

/**
 * Exit alert window
 */
void alerts_exit(int tab)
{
    int i;

    (void)tab;
    alerts_style = alerts_fadein = 0;
    alerts_size.val = 32;
    alerts_font.val = -1;
    alerts_dur.val = alerts_fadeout = 100;
    alerts_color[0] = 0xFF808080;
    alerts_color[1] = 0xFF00FF00;
    alerts_color[2] = 0xFF0000FF;
    for(i = 0; i < 16; i++) {
        alerts_snd[i].val = -1;
        alerts_texts[i][0] = 0;
        if(i > 2) alerts_color[2] = 0;
    }
}

/**
 * Enter alert window
 */
void alerts_init(int tab)
{
    int i;

    alerts_exit(tab);
    alerts_form[1].param = lang[LANG_SAVE];
    alerts_form[1].w = ui_textwidth(alerts_form[1].param) + 40;
    if(alerts_form[1].w < 200) alerts_form[1].w = 200;
    alerts_form[3].param = lang[ALERT_FONT];
    alerts_tab = ui_textwidth(alerts_form[3].param);
    alerts_form[8].param = lang[ALERT_DUR];
    i = ui_textwidth(alerts_form[8].param); if(i > alerts_tab) alerts_tab = i;
    alerts_form[11].param = lang[ALERT_NEW];
    i = ui_textwidth(alerts_form[11].param); if(i > alerts_tab) alerts_tab = i;
    alerts_form[15].param = lang[ALERT_COMPLETED];
    i = ui_textwidth(alerts_form[15].param); if(i > alerts_tab) alerts_tab = i;
    alerts_form[19].param = lang[ALERT_FAILED];
    i = ui_textwidth(alerts_form[19].param); if(i > alerts_tab) alerts_tab = i;
    alerts_tab += 20;
    alerts_form[4].x = alerts_form[9].x = alerts_tab; alerts_form[5].x = alerts_tab + 24;
    for(i = 0; i < 16; i++) {
        alerts_form[12 + i * 4].x = alerts_tab;
        alerts_form[13 + i * 4].x = alerts_tab + 24;
        alerts_snd[i].opts = project.sounds;
    }
    alerts_form[6].x = alerts_tab + 48;
    alerts_form[7].x = alerts_tab + 48 + 46;
    alerts_form[10].x = alerts_form[9].x + alerts_form[9].w + 4;
    alerts_font.opts = project.fonts;
    alerts_load();
}

/**
 * Resize the view
 */
void alerts_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    alerts_form[0].y = alerts_form[1].y = alerts_form[2].y = h - 48;
    alerts_form[1].x = w - 20 - alerts_form[1].w;
    alerts_form[2].x = alerts_form[1].x - 52;
    alerts_form[7].w = w - 10 - alerts_form[7].x;
    alerts_form[10].w = w - 10 - alerts_form[10].x;
    for(i = 0; i < 16; i++) {
        alerts_form[13 + i * 4].w = w - 58 - alerts_form[14 + i * 4].w - alerts_form[12 + i * 4].x;
        alerts_form[14 + i * 4].x = w - 10 - alerts_form[14 + i * 4].w;
        if(alerts_form[11 + i * 4].y + alerts_form[11 + i * 4].h > alerts_form[0].y - 8) {
            alerts_form[11 + i * 4].inactive = alerts_form[11 + i * 4 + 1].inactive =
            alerts_form[11 + i * 4 + 2].inactive = alerts_form[11 + i * 4 + 3].inactive = 2;
        } else {
            alerts_form[11 + i * 4].inactive = alerts_form[11 + i * 4 + 1].inactive =
            alerts_form[11 + i * 4 + 2].inactive = alerts_form[11 + i * 4 + 3].inactive = 0;
        }
    }
    ui_fade_set(alerts_form[10].w, &alerts_fade);
}

/**
 * View layer
 */
void alerts_redraw(int tab)
{
    (void)tab;
    ui_form = alerts_form;
    alerts_form[2].inactive = (!alerts_texts[0][0]);
}

/**
 * Delete alert
 */
void alerts_delete(void *data)
{
    (void)data;
    sprintf(projfn, "%s" SEP "%s" SEP "alerts.cfg", project.id, project_dirs[PROJDIRS_UI]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("alerts_delete: removing %s\n", projdir);
        remove(projdir);
        alerts_exit(SUBMENU_ALERTS);
    }
}

/**
 * Save alert
 */
void alerts_save(void *data)
{
    int i;
    FILE *f;

    (void)data;
    f = project_savefile(0, project_dirs[PROJDIRS_UI], "alerts", "cfg", PROJMAGIC_ALERT, "alerts_save");
    if(f) {
        project_wrfont(f, alerts_style, alerts_size.val, alerts_font.val); fprintf(f, "\r\n");
        fprintf(f, "%u %u %u\r\n", alerts_dur.val, alerts_fadein, alerts_fadeout);
        for(i = 0; i < 16; i++) {
            fprintf(f, "%08X ", alerts_color[i]);
            project_wridx(f, alerts_snd[i].val, project.sounds);
            fprintf(f, " %s\r\n", alerts_texts[i]);
        }
        fclose(f);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVEQCFG]);
}

/**
 * Load alert
 */
void alerts_load()
{
    int i;
    char *str, *s;

    memset(&alerts_texts, 0, sizeof(alerts_texts));
    str = project_loadfile(project_dirs[PROJDIRS_UI], "alerts", "cfg", PROJMAGIC_ALERT, "alerts_load");
    if(str) {
        s = project_skipnl(str);
        s = project_getfont(s, &alerts_style, &alerts_size.val, &alerts_font.val);
        s = project_skipnl(s);
        s = project_getint(s, &alerts_dur.val, alerts_dur.min, alerts_dur.max);
        s = project_getint(s, &alerts_fadein, 0, alerts_dur.val);
        s = project_getint(s, &alerts_fadeout, alerts_fadein, alerts_dur.val);
        s = project_skipnl(s);
        for(i = 0; i < 16; i++) {
            s = project_getcolor(s, &alerts_color[i]);
            s = project_getidx(s, &alerts_snd[i].val, project.sounds, -1);
            s = project_getstr2(s, alerts_texts[i], 2, sizeof(alerts_texts[i]));
            s = project_skipnl(s);
        }
        free(str);
    } else
        ui_status(1, lang[ERR_LOADQCFG]);
}

/**
 * Load strings for translation
 */
void alerts_loadstr()
{
    int i;
    char *str, *s;

    str = project_loadfile(project_dirs[PROJDIRS_UI], "alerts", "cfg", PROJMAGIC_ALERT, "alerts_load");
    if(str) {
        s = project_skipnl(str);
        s = project_skipnl(s);
        s = project_skipnl(s);
        for(i = 0; i < 16; i++) {
            s = project_skiparg(s, 2);
            s = project_gettrstr(s, 2, sizeof(alerts_texts[i]));
            s = project_skipnl(s);
        }
        free(str);
    }
}

/**
 * Preview alert
 */
void alerts_preview(void *data)
{
    Mix_Music *music = NULL;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *bg = NULL, *texture = NULL;
    SDL_Rect rect, dst;
    int wf = SDL_GetWindowFlags(window), i, j, k, l, x, y, w, h, b, p, noaudio = 0, now = 0;
    char *str = alerts_texts[0];
    uint32_t *pixels, base;
    ui_font_t fnt;
    ssfn_buf_t buf;

    (void)data;
    if(verbose) printf("alerts_preview: started\n");

    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

    if(Mix_OpenAudio(FREQ, AUDIO_S16SYS, 2, 4096) < 0) { ui_status(1, lang[ERR_AUDIO]); noaudio = 1; }

    bg = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
    if(bg) {
        SDL_LockTexture(bg, NULL, (void**)&pixels, &p);
        /* checker */
        for(j = k = 0; j < dm.h; j++, k += p/4)
            for(i = 0; i < dm.w ; i++) {
                x = (i >> 5); y = (j >> 5);
                pixels[k + i] = theme[(y & 1) ^ (x & 1) ? THEME_LIGHT : THEME_DARK];
            }
        SDL_UnlockTexture(bg);
    }

    memset(&rect, 0, sizeof(rect));
    ui_font_load(&fnt, alerts_style, alerts_size.val, alerts_font.val);
    ssfn_bbox(&fnt.ctx, str, &w, &h, &l, &b);
    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, w + l + 2, h + 2);
    if(texture) {
        buf.w = rect.w = w + l + 2;
        buf.h = rect.h = h + 2;
        buf.x = l + 2;
        buf.y = b + 2;
        buf.bg = 0;
        buf.fg = 0x7F000000;
        SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(texture, NULL, (void**)&buf.ptr, &p);
        memset(buf.ptr, 0, p * h);
        buf.p = p;
        fnt.ctx.style &= ~SSFN_STYLE_A;
        while(*str && ((i = ssfn_render(&fnt.ctx, &buf, str)) > 0 || i == SSFN_ERR_NOGLYPH)) str += (i < 1 ? 1 : i);
        buf.x = l;
        buf.y = b;
        buf.fg = alerts_color[0];
        str = alerts_texts[0];
        while(*str && ((i = ssfn_render(&fnt.ctx, &buf, str)) > 0 || i == SSFN_ERR_NOGLYPH)) str += (i < 1 ? 1 : i);
        fnt.ctx.style |= SSFN_STYLE_A;
        SDL_UnlockTexture(texture);
        rect.x = (dm.w - rect.w) / 2;
        rect.y = (dm.h - rect.h) / 2;
    }
    ui_font_free(&fnt);

    if(alerts_snd[0].val >= 0 && !noaudio) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 1],
            project.sounds[alerts_snd[0].val]);
        if(verbose) printf("alerts_preview: loading sound %s\n", projdir);
        music = Mix_LoadMUS(projdir);
        if(music) {
            Mix_VolumeMusic(MIX_MAX_VOLUME);
            Mix_PlayMusic(music, 0);
        } else
            ui_status(1, lang[ERR_LOADSND]);
    }

    base = SDL_GetTicks();
    while(now < alerts_dur.val * 10) {
        now = SDL_GetTicks() - base;

        /* events */
        if(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
                SDL_PushEvent(&event); goto cleanup;
            }
            switch(event.type) {
                case SDL_KEYUP: goto cleanup; break;
                case SDL_MOUSEBUTTONUP: goto cleanup; break;
            }
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        if(bg) SDL_RenderCopy(renderer, bg, NULL, NULL);
        if(texture) {
            i = 255; dst.w = rect.w; dst.h = rect.h;
            if(now >= alerts_dur.val * 10) i = 0; else
            if(now < alerts_fadein * 10 && alerts_fadein > 0) {
                i = now * 255 / alerts_fadein / 10;
                dst.w = rect.w * now / alerts_fadein / 10;
                dst.h = rect.h * now / alerts_fadein / 10;
            } else
            if(now > alerts_fadeout * 10 && alerts_dur.val > alerts_fadeout)
                i = (alerts_dur.val - now / 10) * 255 / (alerts_dur.val - alerts_fadeout);
            dst.x = (dm.w - dst.w) / 2;
            dst.y = (dm.h - dst.h) / 2;
            SDL_SetTextureAlphaMod(texture, i);
            SDL_RenderCopy(renderer, texture, NULL, &dst);
        }
        SDL_RenderPresent(renderer);
        SDL_Delay(10);
    }

cleanup:
    if(!noaudio) {
        if(music) { Mix_HaltMusic(); Mix_FreeMusic(music); }
        Mix_CloseAudio();
    }
    if(texture) SDL_DestroyTexture(texture);
    if(bg) SDL_DestroyTexture(bg);
    if(verbose) printf("alerts_preview: stopped\n");
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Save tng
 */
int alerts_totng(tngctx_t *ctx)
{
    int i, j;

    if(!ctx) return 1;
    alerts_load();
    tng_section(ctx, TNG_SECTION_ALERTS);
    tng_font(ctx, alerts_style, alerts_size.val, alerts_font.val);
    tng_data(ctx, &alerts_dur.val, 2);
    tng_data(ctx, &alerts_fadein, 2);
    tng_data(ctx, &alerts_fadeout, 2);
    for(j = 3; j < 16 && alerts_texts[j][0]; j++);
    i = j; tng_data(ctx, &i, 1);
    for(i = 0; i < j; i++) {
        tng_data(ctx, &alerts_color[i], 4);
        tng_ref(ctx, TNG_IDX_SND, alerts_snd[i].val);
        tng_idx(ctx, project.sounds, alerts_snd[i].val);
        tng_text(ctx, alerts_texts[i]);
    }
    return 1;
}

/**
 * Read from tng
 */
int alerts_fromtng(tngctx_t *ctx)
{
    FILE *f;
    uint8_t *buf, *end;
    int i, l, len;

    if(!ctx || !ctx->tbl || !ctx->numtbl) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_ALERTS; i++);
    if(i >= ctx->numtbl) return 1;
    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
    buf = (uint8_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]);
    end = buf + len;
    if(len < 12) return 0;
    f = project_savefile(0, project_dirs[PROJDIRS_UI], "alerts", "cfg", PROJMAGIC_ALERT, "alerts_fromtng");
    if(f) {
        buf = tng_wrfont(ctx, buf, f); fprintf(f, "\r\n");
        fprintf(f, "%u %u %u\r\n", *((uint16_t*)buf), *((uint16_t*)&buf[2]), *((uint16_t*)&buf[4])); buf += 6;
        l = *buf++;
        for(i = 0; i < l && buf < end; i++) {
            fprintf(f, "%08X ", *((uint32_t*)buf)); buf += 4;
            buf = tng_wridx(ctx, buf, f); fprintf(f, " ");
            buf = tng_wrtext(ctx, buf, f); fprintf(f, "\r\n");
        }
        fclose(f);
        return 1;
    }
    return 0;
}
