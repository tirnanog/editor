/*
 * tnge/crafts.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Crafting dialogs window
 *
 */

#include "main.h"

/* for the preview */
extern ui_num_t hud_invw, hud_invh, hud_invl, hud_invt, elements_size[];
extern ui_sprsel_t hud_invimg, hud_invsel, hud_invequip;

void crafts_load(void *data);
void crafts_new(void *data);
void crafts_save(void *data);
void crafts_delete(void *data);
void crafts_preview(void *data);
void crafts_erase(void *data);
void crafts_up(void *data);
void crafts_down(void *data);
void crafts_add(void *data);
void crafts_clk(void *data);
void crafts_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

typedef struct {
    int prodnum, prod, ingnum[16], ing[16];
} crafts_opt_t;

char crafts_name[PROJ_NAMEMAX], crafts_title[PROJ_TITLEMAX], crafts_desc[PROJ_DESCMAX];
int crafts_tab, crafts_unordered = 0;
ui_input_t crafts_nameinp = { INP_ID, sizeof(crafts_name), crafts_name };
ui_input_t crafts_titinp = { INP_NAME, sizeof(crafts_title), crafts_title };
ui_input_t crafts_descinp = { INP_NAME, sizeof(crafts_desc), crafts_desc };
ui_sprsel_t crafts_portrait = { -1, 3, 186, 1 };
ui_select_t crafts_craftedsnd = { -1, LANG_NONE, NULL };
ui_num_t crafts_prodnum = { 1, 1, 999, 1 };
ui_sprsel_t crafts_product = { -1, 1, 158, 3 };
ui_num_t crafts_ingnum[16] = {
    { 0, 0, 999, 1 }, { 0, 0, 999, 1 }, { 0, 0, 999, 1 }, { 0, 0, 999, 1 },
    { 0, 0, 999, 1 }, { 0, 0, 999, 1 }, { 0, 0, 999, 1 }, { 0, 0, 999, 1 },
    { 0, 0, 999, 1 }, { 0, 0, 999, 1 }, { 0, 0, 999, 1 }, { 0, 0, 999, 1 },
    { 0, 0, 999, 1 }, { 0, 0, 999, 1 }, { 0, 0, 999, 1 }, { 0, 0, 999, 1 }
};
ui_sprsel_t crafts_ingreds[16] = {
    { -1, 1, 158, 3 }, { -1, 1, 158, 3 }, { -1, 1, 158, 3 }, { -1, 1, 158, 3 },
    { -1, 1, 158, 3 }, { -1, 1, 158, 3 }, { -1, 1, 158, 3 }, { -1, 1, 158, 3 },
    { -1, 1, 158, 3 }, { -1, 1, 158, 3 }, { -1, 1, 158, 3 }, { -1, 1, 158, 3 },
    { -1, 1, 158, 3 }, { -1, 1, 158, 3 }, { -1, 1, 158, 3 }, { -1, 1, 158, 3 }
};

ui_tablehdr_t crafts_hdr[] = {
    { SUBMENU_CRAFTS, 0, 0, 0 },
    { 0 }
};
ui_table_t crafts_tbl = { crafts_hdr, DIALOGS_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), NULL, NULL,
    NULL, NULL };

ui_tablehdr_t crafts_opthdr[] = {
    { CRAFTS_OPTS, 0, 0, 0 },
    { CRAFTS_PROD, 80, 0, 0 },
    { 0 }
};
ui_table_t crafts_opts = { crafts_opthdr, CRAFTS_NOOPTS, 36, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(crafts_opt_t),
    crafts_drawcell, NULL, NULL, NULL };

/**
 * The form
 */
ui_form_t crafts_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, DIALOGS_DELETE, (void*)ICON_REMOVE, crafts_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, crafts_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, DIALOGS_PREVIEW, (void*)ICON_PVIEW, crafts_preview },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, crafts_new },
    { FORM_INPUT, 0, 30, 200, 18, 0, CRAFTS_NAME, &crafts_nameinp, NULL },
    { FORM_TABLE, 10, 30, 200, 0, 0, 0, &crafts_tbl, crafts_load },
    /* 6 */
    { FORM_TEXT, 0, 55, 0, 18, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 54, 0, 20, 0, DIALOGS_TITLE, &crafts_titinp, NULL },
    /* 8 */
    { FORM_TEXT, 0, 79, 0, 18, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 78, 0, 20, 0, LANG_ATTRSTR, &crafts_descinp, NULL },
    /* 10 */
    { FORM_SNDSEL, 0, 102, 0, 20, 0, CRAFTS_SND, &crafts_craftedsnd, NULL },
    { FORM_BOOL, 0, 103, 0, 18, 0, CRAFTS_UNORDERED, &crafts_unordered, NULL },
    /* 12 */
    { FORM_SPRITE, 0, 54, 68, 68, 0, DIALOGS_PORT, &crafts_portrait, NULL },
    /* 13 */
    { FORM_TABLE, 0, 128, 0, 0, 0, 0, &crafts_opts, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CRAFTS_ERASE, (void*)ICON_REMOVE, crafts_erase },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CRAFTS_ADD, (void*)ICON_ADD, crafts_add },
    { FORM_COMMENT, 0, 0, 16, 20, 0, 0, "\xe2\x9e\xa1", NULL },
    /* 17 */
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_prodnum, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_product, NULL },
    /* 19 */
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[0], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[0], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[1], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[1], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[2], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[2], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[3], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[3], crafts_clk },

    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[4], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[4], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[5], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[5], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[6], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[6], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[7], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[7], crafts_clk },

    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[8], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[8], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[9], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[9], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[10], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[10], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[11], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[11], crafts_clk },

    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[12], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[12], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[13], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[13], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[14], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[14], crafts_clk },
    { FORM_NUM, 0, 0, 46, 20, 0, CRAFTS_PROD, &crafts_ingnum[15], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CRAFTS_PROD, &crafts_ingreds[15], crafts_clk },

    { FORM_LAST }
};

/**
 * Draw table cell
 */
void crafts_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    crafts_opt_t *opt = (crafts_opt_t*)data;
    ui_sprite_t *s;
    SDL_Rect rect;
    int i, j = x + 4, l;
    char tmp[32];

    (void)idx; (void)sel;
    if(!data) return;
    ui_clip.x = x; ui_clip.w = hdr[0].w - 4;
    rect.y = y + h / 2 - 16;
    for(i = 0; i < 16; i++)
        if(opt->ing[i] != -1) {
            s = spr_getidx(crafts_product.cat, opt->ing[i], -1, 0);
            if(s) {
                sprintf(tmp, "%u", opt->ingnum[i]); l = ui_textwidth(tmp);
                ui_text(dst, j, y + h / 2 - 8, tmp);
                rect.x = j + 2 + l;
                ui_fit(32, 32, s->w, s->h, &rect.w, &rect.h);
                spr_blit(s, 255, dst, &rect);
                j += 44 + l;
            }
        }
    ui_clip.x = x; ui_clip.w = w;
    if(opt->prod != -1) {
        s = spr_getidx(crafts_product.cat, opt->prod, -1, 0);
        if(s) {
            sprintf(tmp, "%u", opt->prodnum); l = ui_textwidth(tmp);
            ui_text(dst, x + hdr[0].w + 4, y + h / 2 - 8, tmp);
            rect.x = x + hdr[0].w + 6 + l;
            ui_fit(32, 32, s->w, s->h, &rect.w, &rect.h);
            spr_blit(s, 255, dst, &rect);
        }
    }
    ui_clip.x = 0; ui_clip.w = scr_w;
}

/**
 * Exit crafting dialogs window
 */
void crafts_exit(int tab)
{
    (void)tab;
    crafts_tbl.clk = -1;
}

/**
 * Enter crafting dialogs window
 */
void crafts_init(int tab)
{
    int i;

    crafts_exit(tab);
    crafts_form[1].param = lang[LANG_SAVE];
    crafts_form[1].w = ui_textwidth(crafts_form[1].param) + 40;
    if(crafts_form[1].w < 200) crafts_form[1].w = 200;
    crafts_form[6].param = lang[CRAFTS_TITLE];
    i = crafts_tab = ui_textwidth(crafts_form[6].param); crafts_form[6].w = i;
    crafts_form[8].param = lang[DIALOGS_DESC];
    i = ui_textwidth(crafts_form[8].param); crafts_form[8].w = i;
    if(i > crafts_tab) crafts_tab = i;
    crafts_form[6].x = crafts_form[8].x = crafts_form[13].x = crafts_form[14].x = crafts_form[5].x + crafts_form[5].w + 10;
    crafts_form[7].x = crafts_form[9].x = crafts_form[10].x = crafts_form[6].x + crafts_tab + 4;
    project_freedir(&project.crafts, &project.numcrafts);
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_DIALOGS]);
    project.crafts = project_getdir(projdir, ".cft", &project.numcrafts);
    if(!crafts_name[0]) crafts_tbl.val = -1;
    crafts_tbl.data = project.crafts;
    crafts_tbl.num = project.numcrafts;
    if(crafts_tbl.val >= crafts_tbl.num) crafts_tbl.val = crafts_tbl.num - 1;
}

/**
 * Resize the view
 */
void crafts_resize(int tab, int w, int h)
{
    int i, j;

    (void)tab;
    crafts_form[0].y = crafts_form[1].y = crafts_form[2].y = crafts_form[3].y = h - 48;
    crafts_form[1].x = w - 20 - crafts_form[1].w;
    crafts_form[2].x = crafts_form[1].x - 52;
    ui_table_resize(&crafts_form[5], crafts_form[5].w, h - 89);
    crafts_form[3].x = crafts_form[5].x + crafts_form[5].w + 20;
    crafts_form[4].x = w - 10 - crafts_form[4].w;
    crafts_form[12].x = w - 10 - crafts_form[12].w;
    crafts_form[7].w = crafts_form[9].w = crafts_form[12].x - 4 - crafts_form[7].x;
    crafts_form[10].w = crafts_form[11].w = crafts_form[7].w / 2;
    crafts_form[11].x = crafts_form[10].x + crafts_form[10].w + 16;
    ui_table_resize(&crafts_form[13], w - 10 - crafts_form[13].x, h - 59 - 4 * 24 - crafts_form[13].y);
    crafts_form[14].y = crafts_form[15].y = crafts_form[13].y + crafts_form[13].h + 4;
    crafts_form[15].x = w - 30;
    crafts_form[17].y = crafts_form[18].y = crafts_form[14].y + 36;
    crafts_form[16].y = crafts_form[17].y + 2;
    j = crafts_form[14].x + crafts_form[14].w + (crafts_form[15].x - crafts_form[14].x - crafts_form[14].w -
        crafts_form[16].w - 30 - 5 * (crafts_form[19].w + crafts_form[20].w + 14)) / 2;
    for(i = 0; i < 16; i++) {
        crafts_form[19 + 2 * i].y = crafts_form[19 + 2 * i + 1].y = crafts_form[13].y + crafts_form[13].h + 4 + (i / 4) * 24;
        crafts_form[19 + 2 * i].x = j + (i % 4) * (crafts_form[19].w + crafts_form[20].w + 14);
        crafts_form[19 + 2 * i + 1].x = crafts_form[19 + 2 * i].x + crafts_form[19].w + 4;
    }
    crafts_form[16].x = crafts_form[26].x + crafts_form[26].w + 20;
    crafts_form[17].x = crafts_form[16].x + crafts_form[16].w + 20;
    crafts_form[18].x = crafts_form[17].x + crafts_form[17].w + 4;
}

/**
 * View layer
 */
void crafts_redraw(int tab)
{
    int i;

    (void)tab;
    ui_form = crafts_form;
    ui_form[0].inactive = (crafts_tbl.val < 0);
    ui_form[1].inactive = (!crafts_name[0] || !crafts_title[0] || crafts_opts.num < 1);
    ui_form[2].inactive = (!crafts_title[0]);
    ui_form[12].inactive = (!strcmp(crafts_title, "{}"));
    ui_form[14].inactive = (crafts_opts.val < 0);
    for(i = 0; i < 16 && crafts_ingreds[i].val == -1; i++);
    ui_form[15].inactive = (crafts_product.val == -1 || i >= 16);
    if(ui_form[12].inactive) crafts_portrait.val = -1;
}

/**
 * Erase recipe
 */
void crafts_erase(void *data)
{
    crafts_opt_t *list = (crafts_opt_t*)crafts_opts.data;

    (void)data;
    if(!list || crafts_opts.val < 0 || crafts_opts.val >= crafts_opts.num) return;
    memcpy(&list[crafts_opts.val], &list[crafts_opts.val + 1], (crafts_opts.num - crafts_opts.val) *
        sizeof(crafts_opt_t));
    crafts_opts.num--;
    if(crafts_opts.val >= crafts_opts.num)
        crafts_opts.val = crafts_opts.num - 1;
    if(!crafts_opts.num) { free(list); crafts_opts.data = NULL; }
}

/**
 * Add recipe
 */
void crafts_add(void *data)
{
    crafts_opt_t *list = (crafts_opt_t*)crafts_opts.data;
    int i, j;

    (void)data;
    for(i = 0; i < 16 && crafts_ingreds[i].val == -1; i++);
    if(crafts_product.val == -1 || i == 16) return;
    list = (crafts_opt_t*)realloc(list, (crafts_opts.num + 1) * sizeof(crafts_opt_t));
    crafts_opts.data = list;
    if(!list) main_error(ERR_MEM);
    i = crafts_opts.num++;
    list[i].prodnum = crafts_prodnum.val; crafts_prodnum.val = crafts_prodnum.min;
    list[i].prod = crafts_product.val; crafts_product.val = -1;
    for(j = 0; j < 16; j++) {
        list[i].ingnum[j] = crafts_ingnum[j].val; crafts_ingnum[j].val = crafts_ingnum[j].min;
        list[i].ing[j] = crafts_ingreds[j].val; crafts_ingreds[j].val = -1;
    }
}

/**
 * Recipe sprite clicked
 */
void crafts_clk(void *data)
{
    ui_sprsel_t *spr = data;
    int i = spr - crafts_ingreds;
    if(spr->val == -1) crafts_ingnum[i].val = 0;
    else if(crafts_ingnum[i].val == 0) crafts_ingnum[i].val = 1;
}

/**
 * Free resources
 */
void crafts_free()
{
    int i;

    crafts_name[0] = crafts_title[0] = crafts_desc[0] = 0;
    if(crafts_opts.data) { free(crafts_opts.data); crafts_opts.data = NULL; }
    crafts_opts.num = crafts_unordered = 0; crafts_portrait.val = crafts_opts.val = crafts_product.val = -1;
    crafts_prodnum.val = crafts_prodnum.min;
    for(i = 0; i < 16; i++) { crafts_ingreds[i].val = -1; crafts_ingnum[i].val = crafts_ingnum[i].min; }
}

/**
 * Clear form, new crafting dialog
 */
void crafts_new(void *data)
{
    (void)data;
    crafts_free();
}

/**
 * Delete crafting dialog
 */
void crafts_delete(void *data)
{
    char **list = (char**)crafts_tbl.data;

    (void)data;
    if(crafts_tbl.val < 0 || !list) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.cft", project.id, project_dirs[PROJDIRS_DIALOGS], list[crafts_tbl.val]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("crafts_delete: removing %s\n", projdir);
        remove(projdir);
        crafts_new(NULL);
        crafts_init(SUBMENU_CRAFTS);
    }
}

/**
 * Save dialog
 */
void crafts_save(void *data)
{
    crafts_opt_t *opts = (crafts_opt_t*)crafts_opts.data;
    char **list = (char**)crafts_tbl.data;
    FILE *f;
    int i, j;

    (void)data;
    if(!crafts_name[0] || !crafts_title[0] || crafts_opts.num < 1) return;
    f = project_savefile(!list || crafts_tbl.val < 0 || strcmp(list[crafts_tbl.val], crafts_name),
        project_dirs[PROJDIRS_DIALOGS], crafts_name, "cft", PROJMAGIC_CRAFT, "crafts_save");
    if(f) {
        fprintf(f, "%s\r\n", crafts_title);
        fprintf(f, "%s\r\n", crafts_desc[0] ? crafts_desc : "-");
        project_wridx(f, crafts_craftedsnd.val, project.sounds); fprintf(f, " ");
        project_wrsprite(f, &crafts_portrait); fprintf(f, " ");
        fprintf(f, "%u\r\n", crafts_unordered);
        for(i = 0; i < crafts_opts.num; i++, opts++) {
            fprintf(f, "%u ", opts->prodnum);
            project_wrsprite2(f, opts->prod, crafts_product.cat);
            for(j = 0; j < 16; j++) {
                fprintf(f, " %u ", opts->ingnum[j]);
                project_wrsprite2(f, opts->ing[j], crafts_product.cat);
            }
            fprintf(f, "\r\n");
        }
        fclose(f);
        crafts_init(SUBMENU_CRAFTS);
        for(crafts_tbl.val = 0; crafts_tbl.val < crafts_tbl.num &&
            strcmp(crafts_name, project.crafts[crafts_tbl.val]); crafts_tbl.val++);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.cft", project.id, project_dirs[PROJDIRS_DIALOGS], crafts_name);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVECFT]);
}

/**
 * Load crafting dialog
 */
int crafts_loadcft(char *fn, int opts)
{
    char *str, *s;
    int i;

    if(!fn || !*fn) return 0;
    str = project_loadfile(project_dirs[PROJDIRS_DIALOGS], fn, "cft", PROJMAGIC_CRAFT, "crafts_loadcft");
    if(str) {
        crafts_new(NULL);
        s = project_skipnl(str);
        s = project_getstr2(s, crafts_title, 2, sizeof(crafts_title));
        s = project_skipnl(s);
        s = project_getstr2(s, crafts_desc, 2, sizeof(crafts_desc));
        s = project_skipnl(s);
        s = project_getidx(s, &crafts_craftedsnd.val, project.sounds, -1);
        s = project_getsprite(s, &crafts_portrait);
        s = project_getint(s, &crafts_unordered, 0, 1);
        if(opts) {
            while(*s) {
                s = project_skipnl(s);
                if(!*s) break;
                crafts_product.val = -1; crafts_prodnum.val = crafts_prodnum.min;
                for(i = 0; i < 16; i++) { crafts_ingreds[i].val = -1; crafts_ingnum[i].val = crafts_ingnum[i].min; }
                s = project_getint(s, &crafts_prodnum.val, crafts_prodnum.min, crafts_prodnum.max);
                s = project_getsprite(s, &crafts_product);
                for(i = 0; i < 16; i++) {
                    s = project_getint(s, &crafts_ingnum[i].val, crafts_ingnum[i].min, crafts_ingnum[i].max);
                    s = project_getsprite(s, &crafts_ingreds[i]);
                }
                crafts_add(NULL);
            }
        }
        crafts_product.val = -1; crafts_prodnum.val = crafts_prodnum.min;
        for(i = 0; i < 16; i++) { crafts_ingreds[i].val = -1; crafts_ingnum[i].val = crafts_ingnum[i].min; }
        free(str);
        strncpy(crafts_name, fn, sizeof(crafts_name) - 1);
        return 1;
    }
    return 0;
}

/**
 * Load dialog handler
 */
void crafts_load(void *data)
{
    char **list = (char**)crafts_tbl.data;

    (void)data;
    if(crafts_tbl.val < 0 || !list) return;
    if(!crafts_loadcft(list[crafts_tbl.val], 1))
        ui_status(1, lang[ERR_LOADCFT]);
}

/**
 * Load strings for translation
 */
void crafts_loadstr()
{
    char *str, *s;
    int i;

    for(i = 0; i < project.numcrafts; i++) {
        str = project_loadfile(project_dirs[PROJDIRS_DIALOGS], project.crafts[i], "cft", PROJMAGIC_CRAFT, "crafts_loadstr");
        if(str) {
            s = project_skipnl(str);
            s = project_gettrstr(s, 2, sizeof(crafts_title));
            s = project_skipnl(s);
            s = project_gettrstr(s, 2, sizeof(crafts_desc));
            free(str);
        }
    }
}

/**
 * Preview a crafting dialog
 */
void crafts_preview(void *data)
{
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *texture = NULL, *portrait = NULL;
    SDL_Rect psrc, pdst, dst;
    int wf = SDL_GetWindowFlags(window), nl, i, j, k, m, x, y, p, top, left, bottom, w, h, slw, slh, th;
    char *str = NULL;
    uint32_t *pixels;
    ui_sprite_t *s = NULL;
    ui_tablehdr_t hdr[] = {
        { NPCS_TITLESTR, 0, 0, 0 },
        { 0 }
    };

    (void)data;
    if(verbose) printf("crafts_preview: started\n");
    lang[NPCS_TITLESTR] = strcmp(crafts_title, "{}") ? crafts_title : lang[MAINMENU_MERLIN];
    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

    if(crafts_portrait.val >= 0) {
        s = spr_getidx(crafts_portrait.cat, crafts_portrait.val, -1, 0);
        if(s) {
            pdst.w = psrc.w = s->w; pdst.h = psrc.h = s->h;
            spr_texture(s, &portrait);
        }
    }

    hud_load();
    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
    if(texture) {
        SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
        /* checker */
        for(j = k = 0; j < dm.h; j++, k += p/4)
            for(i = 0; i < dm.w ; i++) {
                x = (i >> 5); y = (j >> 5);
                pixels[k + i] = theme[(y & 1) ^ (x & 1) ? THEME_LIGHT : THEME_DARK];
            }
        SDL_UnlockTexture(texture);
        elements_load();
        elements_loadfonts();
        elements_winsizes(hdr, 0, &w, &top, NULL, &bottom, &left);
        y = 32; left += 10;
        s = spr_getsel(&hud_invimg, 0); slw = s->w; slh = s->h;
        s = spr_getsel(&hud_invsel, 0); if(slw < s->w) { slw = s->w; } if(slh < s->h) slh = s->h;
        slw += 4; slh += 4;
        i = ((dm.w / 3) - 2 * elements_pad[4].val) / slw;
        i = i * slw + 2 * elements_pad[4].val;
        if(i > w) w = i;
        s = spr_getsel(&hud_invequip, 0);
        if(s->w > w) w = s->w;
        y += top;
        str = elements_dlgstr(crafts_desc, w, 0, 3, &nl);
        th = str && *str ? nl * elements_size[3].val + elements_pad[3].val : 0;
        i = (portrait ? s->h + elements_pad[3].val : 0) + th;
        m = (dm.h * 3 / 4 - y - bottom - i - 2 * elements_pad[3].val) / slh;
        if(m < 4 && portrait) {
            h = dm.h * 3 / 4 - y - bottom;
            s->h = h - 3 * elements_pad[3].val - th - 4 * slh;
        } else
            h = 4 * slh + i + 2 * elements_pad[3].val;
        x = left;
        elements_window(texture, dm.w, dm.h, x, y, w, h, hdr, 0);
        y += elements_pad[3].val;
        if(portrait && s->h > 0) {
            ui_fit(w, s->h, psrc.w, psrc.h, &pdst.w, &pdst.h);
            pdst.x = left + (w - pdst.w) / 2; pdst.y = y; psrc.x = psrc.y = 0;
            y += s->h + elements_pad[3].val;
        } else
            psrc.h = 0;
        if(str && *str) {
            elements_text(texture, dm.w, dm.h, x + elements_pad[4].val, y, w - 2 * elements_pad[4].val,
                elements_size[3].val, str, 3);
            y += th;
        }
        elements_freefonts();
        s = spr_getsel(&hud_invsel, 0);
        dst.x = x + w - elements_pad[4].val - slw; dst.y = y + 2 * slh - slh / 2;
        ui_blitbuf(texture, dst.x + (slw - s->w) / 2, dst.y + (slh - s->h) / 2, s->w, s->h, s->data, 0, 0, s->w, s->h,
            s->w * s->nframe * 4);
        x += elements_pad[4].val;
        s = spr_getsel(&hud_invimg, 0);
        for(m = 0; m < 4; y += slh, m++)
            for(k = 0; k < 4; k++) {
                dst.x = x + k * slw; dst.y = y;
                ui_blitbuf(texture, dst.x + (slw - s->w) / 2, dst.y + (slh - s->h) / 2, s->w, s->h, s->data, 0, 0, s->w, s->h,
                    s->w * s->nframe * 4);
            }
    }

    while(1) {
        /* events */
        SDL_WaitEvent(&event);
        if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
            SDL_PushEvent(&event); goto cleanup;
        }
        switch(event.type) {
            case SDL_KEYUP: goto cleanup; break;
            case SDL_MOUSEBUTTONUP: goto cleanup; break;
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        if(texture) SDL_RenderCopy(renderer, texture, NULL, NULL);
        if(portrait && psrc.h) SDL_RenderCopyEx(renderer, portrait, &psrc, &pdst, 0, NULL, SDL_FLIP_HORIZONTAL);
        SDL_RenderPresent(renderer);
    }

cleanup:
    hud_exit(SUBMENU_HUD);
    if(portrait) SDL_DestroyTexture(portrait);
    if(texture) SDL_DestroyTexture(texture);
    if(str) free(str);
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
    if(verbose) printf("crafts_preview: stopped\n");
}

/**
 * Get references
 */
void crafts_ref(tngctx_t *ctx, int idx)
{
    char *str, *s;
    int j;

    if(!tng_ref(ctx, TNG_IDX_CFT, idx)) return;
    str = project_loadfile(project_dirs[PROJDIRS_DIALOGS], project.crafts[idx], "cft", PROJMAGIC_CRAFT, "crafts_ref");
    if(str) {
        s = project_skipnl(str);
        s = project_skipnl(s);
        s = project_skipnl(s);
        while(*s) {
            s = project_skipnl(s);
            if(!*s) break;
            for(j = 0; j < 17; j++) {
                s = project_skiparg(s, 1);
                s = project_getsprite(s, &crafts_product);
                objects_ref(ctx, crafts_product.val);
            }
        }
        crafts_product.val = -1;
        free(str);
    }
}

/**
 * Save tng
 */
int crafts_totng(tngctx_t *ctx)
{
    crafts_opt_t *opts;
    uLongf cl;
    uint8_t *buf, *ptr, *comp;
    int i, j, k, l;

    if(!ctx || !project.crafts || !project.numcrafts) return 1;
    /* check if there's at least one crafting dialog referenced */
    for(j = 0; j < project.numcrafts && !ctx->idx[TNG_IDX_CFT].ref[j]; j++);
    if(j >= project.numcrafts) return 1;
    /* add crafting dialogs to output */
    tng_section(ctx, TNG_SECTION_CRAFTS);
    for(j = 0; j < project.numcrafts; j++) {
        if(!ctx->idx[TNG_IDX_CFT].ref[j]) continue;
        if(!crafts_loadcft(project.crafts[j], 1)) {
            ui_switchtab(SUBMENU_CRAFTS);
            crafts_tbl.val = j;
            ui_status(1, lang[ERR_LOADCFT]);
            return 0;
        }
        opts = (crafts_opt_t*)crafts_opts.data;
        l = 14 + crafts_opts.num * 17 * 4;
        buf = ptr = (uint8_t*)main_alloc(l);
        ptr = tng_asset_text(ptr, ctx, crafts_title);
        ptr = tng_asset_text(ptr, ctx, crafts_desc);
        ptr = tng_asset_idx(ptr, ctx, project.sounds, TNG_IDX_SND, crafts_craftedsnd.val);
        if(!strcmp(crafts_title, "{}")) {
            *ptr++ = crafts_portrait.cat; *ptr++ = 0xF0; *ptr++ = 0xFF;
        } else
            ptr = tng_asset_sprite(ptr, ctx, crafts_portrait.cat,  crafts_portrait.val);
        *ptr++ = crafts_unordered;
        *ptr++ = crafts_opts.num;
        for(i = 0; i < crafts_opts.num; i++) {
            memcpy(ptr, &opts[i].prodnum, 2); ptr += 2;
            tng_sprref(ctx, crafts_product.cat, opts[i].prod);
            memcpy(ptr, &project.sprites[crafts_product.cat][opts[i].prod].idx, 2); ptr += 2;
            for(k = 0; k < 16; k++) {
                memcpy(ptr, &opts[i].ingnum[k], 2); ptr += 2;
                if(opts[i].ing[k] < 0 || opts[i].ing[k] >= project.spritenum[crafts_product.cat]) {
                    *ptr++ = 0xff; *ptr++ = 0xff;
                } else {
                    tng_sprref(ctx, crafts_product.cat, opts[i].ing[k]);
                    memcpy(ptr, &project.sprites[crafts_product.cat][opts[i].ing[k]].idx, 2); ptr += 2;
                }
            }
        }
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        if(cl) {
            tng_desc(ctx, project.crafts[j], cl);
            tng_asset_int(ctx, comp, cl);
        } else
            free(comp);
        free(buf);
        /* free resources */
        crafts_new(NULL);
    }
    return 1;
}

/**
 * Read from tng
 */
int crafts_fromtng(tngctx_t *ctx)
{
    FILE *f;
    tng_asset_desc_t *asset;
    uint8_t *buf, *ptr, *end;
    int i, j, k, l, len, n;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_CRAFTS; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;
    for(j = 0; j < len; j++, asset++) {
        ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
        buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &l);
        if(buf) {
            if(!asset->name || asset->name >= TNG_SECTION_SIZE(&ctx->tbl[0]) || l < 14) { free(buf); continue; }
            end = buf + l;
            f = project_savefile(0, project_dirs[PROJDIRS_DIALOGS], (char*)ctx->sts + asset->name, "cft", PROJMAGIC_CRAFT,
                "crafts_fromtng");
            if(f) {
                ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                ptr = tng_wridx(ctx, ptr, f); fprintf(f, " ");
                ptr = tng_wrsprite(ctx, ptr, f); fprintf(f, " ");
                fprintf(f, "%u\r\n", *ptr++);
                n = *ptr++;
                for(i = 0; i < n && ptr < end; i++) {
                    for(k = 0; k < 17 && ptr < end; k++) {
                        fprintf(f, "%s%u ", k ? " " : "", (ptr[1] << 8) | ptr[0]);
                        ptr = tng_wrsprite2(ctx, ptr + 2, f);
                    }
                    fprintf(f, "\r\n");
                }
                fclose(f);
            }
            free(buf);
        }
    }
    return 1;
}
