/*
 * tnge/translate.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Route translate window
 *
 */

#include "main.h"
#include "theora.h"

/* if we can find translation in our dictionary */
extern char *dict[NUMLANGS][NUMTEXTS + 1];

/* Add a few language names with ISO-639-1 codes. Languages of EU, Asia plus some of the most spoken ones
 * all around the world. Hopefully one day there will be a dictionary in TirNanoG Editor for all of them. */
const char *translate_names[] = { "enEnglish", "huMagyar", "deDeutch", "esEspañol", "frFrançais", "ruРусский", "ja日本語", "zh中文",
    "arالعربية", "bgбългарски", "bnবাংলা", "caCatalà", "czČesky", "daDansk", "elελληνικά", "eoEsperanto", "etEesti keel", "fiSuomi",
    "gaGaeilge", "heעברית", "hiहिन्दी", "hrHrovatski", "iaInterlingua", "itItaliano", "jvꦧꦱꦗꦮ", "ko한국어", "ltLietuvių",
    "lvLatviešu", "mkМакедонски", "mtMalti", "nlNederlands", "noNorsk", "plPolski", "ptPortuguês", "roRomână", "skSlovenčina",
    "slSlovenski", "sqShqip", "srSrpski", "svSvenska", "trTürkçe", "ukУкраїнська", "urاردو", "viTiếng Việt", NULL };

void translate_getname(void *data);
void translate_load();
void translate_save(void *data);
void translate_delete(void *data);
void translate_getstrings();
void translate_drawspc(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void translate_spcclk(void *data);
void translate_drawtrn(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

SDL_Thread *translate_th = NULL;
char translate_name[PROJ_NAMEMAX] = { 0 }, translate_code[3] = { 0 }, *translate_fn;
int *translate_durs = NULL;
ui_input_t translate_nameinp = { INP_NAME, sizeof(translate_name), translate_name };
ui_input_t translate_codeinp = { INP_ID, sizeof(translate_code), translate_code };
ui_input_t translate_strinp = { INP_NAME, 0, NULL };

ui_tablehdr_t translate_spchdr[] = {
    { TRANSLATE_ORIG, 0, 0, 0 },
    { TRANSLATE_TRANS, 132, 0, 0 },
    { 0 }
};
ui_table_t translate_spctbl = { translate_spchdr, TRANSLATE_NOSPC, 22, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*),
    translate_drawspc, NULL, NULL, NULL };

ui_table_t translate_tbl = { NULL, 0, 42, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(trn_t), translate_drawtrn, NULL,
    NULL, NULL };

/**
 * The form
 */
ui_form_t translate_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, TRANSLATE_DELETE, (void*)ICON_REMOVE, translate_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, translate_save },
    { FORM_INPUT, 0, 30, 200, 18, 0, TRANSLATE_NAME, &translate_nameinp, NULL },
    { FORM_INPUT, 0, 30, 24, 18, 0, TRANSLATE_CODE, &translate_codeinp, translate_getname },
    { FORM_TABLE, 10, 54, 0, 160, 0, 0, &translate_spctbl, translate_spcclk },
    { FORM_TABLE, 10, 0, 0, 0, 0, 0, &translate_tbl, NULL },
    { FORM_INPUT, 0, 0, 0, 20, 0, 0, &translate_strinp, NULL },
    { FORM_LAST }
};

/**
 * Draw table cell
 */
void translate_drawspc(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    char *spc, tmp[16];
    int i = translate_code[0] && translate_code[1], t = ui_clip.w;

    (void)w;
    if(!data || ! *((char**)data)) return;
    spc = *((char**)data);
    ui_clip.w = x + hdr[0].w - 24 - 72;
    ui_text(dst, x + 4, y + (h - 16) / 2, spc);
    ui_clip.w = t;
    if(translate_durs && translate_durs[idx * 2]) {
        media_dur(translate_durs[idx * 2], tmp);
        ui_text(dst, x + hdr[0].w - 24 - 68, y + (h - 20) / 2, tmp);
    } else
        ui_text(dst, x + hdr[0].w - 24 - 68, y + (h - 20) / 2, "--:--.--");
    if(translate_durs && translate_durs[idx * 2 + 1]) {
        media_dur(translate_durs[idx * 2 + 1], tmp);
        ui_text(dst, x + hdr[0].w + 4, y + (h - 20) / 2, tmp);
    } else
        ui_text(dst, x + hdr[0].w + 4, y + (h - 20) / 2, "--:--.--");
    if(idx == sel) {
        ui_iconbtn(dst, x + hdr[0].w - 24, y + (h - 20) / 2, ICON_PLAY, translate_spctbl.fld == -2);
        sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2], project.speech[sel],
            translate_code[0], translate_code[1]);
        ui_iconbtn(dst, x + hdr[0].w + 72, y + (h - 20) / 2, ICON_PLAY,
            !i || !project_exists(projdir) ? -1 : (translate_spctbl.fld == -3));
        ui_iconbtn(dst, x + hdr[0].w + 72 + 24, y + (h - 20) / 2, ICON_ADD, !i ? -1 : (translate_spctbl.fld == -4));
        translate_spctbl.clk = translate_spctbl.val;
    }
}

/**
 * Draw table cell
 */
void translate_drawtrn(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    trn_t *trn = (trn_t*)data;

    (void)idx; (void)sel; (void)h; (void)hdr;
    if(!data) return;

    ui_text(dst, x + 4, y + 1, trn->id);
    if(idx != sel)
        ui_text(dst, x + 6, y + 4 + 16, trn->str);
    else {
        translate_form[6].x = x + 4;
        translate_form[6].y = y + 19;
        translate_form[6].w = w - 4;
        translate_form[6].inactive = 0;
        translate_strinp.maxlen = trn->len;
        translate_strinp.str = trn->str;
        translate_tbl.clk = translate_tbl.val;
    }
}

/**
 * Button release handler
 */
void translate_spcup(ui_table_t *table)
{
    switch(table->fld) {
        case -2:
            sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                project.speech[table->val], lang[-1][0], lang[-1][1]);
            SDL_SetCursor(cursors[CURSOR_LOADING]);
            media_play(projdir);
            SDL_SetCursor(cursors[CURSOR_PTR]);
        break;
        case -3:
            sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                project.speech[table->val], translate_code[0], translate_code[1]);
            SDL_SetCursor(cursors[CURSOR_LOADING]);
            media_play(projdir);
            SDL_SetCursor(cursors[CURSOR_PTR]);
        break;
        case -4:
            translate_fn = project.speech[table->val];
            fileops_type = FILEOPS_TRANSLATION;
            ui_switchtab(SUBMENU_IMPORT);
        break;
    }
}

/**
 * Click on speech table
 */
void translate_spcclk(void *data)
{
    (void)data;
    translate_spctbl.fld = 0;
    if(ui_curx >= ui_form[4].x + translate_spchdr[0].w - 24 && ui_curx < ui_form[4].x + translate_spchdr[0].w - 4)
        translate_spctbl.fld = -2;
    if(translate_code[0] && translate_code[1]) {
        if(ui_curx >= ui_form[4].x + translate_spchdr[0].w + 72 && ui_curx < ui_form[4].x + translate_spchdr[0].w + 72 + 20) {
            sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                project.speech[translate_spctbl.val], translate_code[0], translate_code[1]);
            if(project_exists(projdir)) translate_spctbl.fld = -3;
        }
        if(ui_curx >= ui_form[4].x + translate_spchdr[0].w + 72 + 24 && ui_curx < ui_form[4].x + translate_spchdr[0].w + 72 + 44)
            translate_spctbl.fld = -4;
    }
    if(translate_spctbl.fld)
        ui_table_btnup = translate_spcup;
}

/**
 * Load speech durations
 */
int translate_worker(void *data)
{
    int i;
    SDL_Event event;

    (void)data;
    event.type = SDL_WINDOWEVENT;
    event.window.event = SDL_WINDOWEVENT_EXPOSED;
    event.window.windowID = SDL_GetWindowID(window);
    if(translate_durs && translate_spctbl.num) {
        for(i = 0; i < translate_spctbl.num; i++) {
            if(!translate_durs[2 * i]) {
                sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                    project.speech[i], lang[-1][0], lang[-1][1]);
                translate_durs[2 * i] = theora_getduration(projdir);
            }
            translate_durs[2 * i + 1] = 0;
            if(translate_code[0] && translate_code[1]) {
                sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                    project.speech[i], translate_code[0], translate_code[1]);
                translate_durs[2 * i + 1] = theora_getduration(projdir);
            }
            if(translate_durs[2 * i] || translate_durs[2 * i + 1])
                SDL_PushEvent(&event);
        }
    }
    translate_th = NULL;
    return 0;
}

/**
 * Get name for code
 */
void translate_getname(void *data)
{
    trn_t *list = (trn_t*)translate_tbl.data;
    int i, l;

    (void)data;
    if(!translate_code[0] || !translate_code[1]) return;
    if(translate_code[0] == lang[-1][0] && translate_code[1] == lang[-1][1]) {
        ui_status(1, lang[ERR_LANG]);
        return;
    }
    for(i = 0; translate_names[i]; i++)
        if(translate_code[0] == translate_names[i][0] && translate_code[1] == translate_names[i][1]) {
            strcpy(translate_name, translate_names[i] + 2);
            break;
        }
    if(!translate_th)
        translate_th = SDL_CreateThread(translate_worker, "worker", NULL);
    /* fill up missing strings from our dictionary if found. There's a +1 shift in both arrays:
     * list[0] is the project's name, and dict[0] is the language code */
    if(list)
        for(l = 0; l < NUMLANGS; l++)
            if(translate_code[0] == dict[l][0][0] && translate_code[1] == dict[l][0][1]) {
                for(i = GAME_LOADGAME + 1; i < NUMTEXTS + 1; i++)
                    if(list[i - GAME_LOADGAME].str && !list[i - GAME_LOADGAME].str[0])
                        strncpy(list[i - GAME_LOADGAME].str, dict[l][i], PROJ_TITLEMAX);
                break;
            }
}

/**
 * Exit translate window
 */
void translate_exit(int tab)
{
    (void)tab;
    translate_freestrings();
    if(translate_th) { SDL_WaitThread(translate_th, &tab); }
    if(translate_durs) { free(translate_durs); translate_durs = NULL; }
}

/**
 * Enter translate window
 */
void translate_init(int tab)
{
    translate_exit(tab);
    translate_fn = NULL;
    translate_form[1].param = lang[LANG_SAVE];
    translate_form[1].w = ui_textwidth(translate_form[1].param) + 40;
    if(translate_form[1].w < 200) translate_form[1].w = 200;
    translate_spctbl.data = project.speech;
    translate_spctbl.num = project.numspeech;
    if(translate_spctbl.num) {
        translate_durs = (int*)main_alloc(translate_spctbl.num * 2 * sizeof(int));
        if(!translate_th)
            translate_th = SDL_CreateThread(translate_worker, "worker", NULL);
    }
    if(translate_spctbl.clk == -1) translate_spctbl.val = -1;
    translate_getstrings();
    translate_load();
    translate_strinp.maxlen = 0;
    translate_strinp.str = NULL;
    translate_tbl.clk = -1;
    translate_form[6].inactive = 2;
}

/**
 * Resize the view
 */
void translate_resize(int tab, int w, int h)
{
    (void)tab;
    translate_form[0].y = translate_form[1].y = h - 48;
    translate_form[1].x = w - 20 - translate_form[1].w;
    translate_form[2].x = w - 10 - translate_form[2].w;
    translate_form[3].x = w - 14 - translate_form[2].w - translate_form[3].w;
    ui_table_resize(&translate_form[4], w - 10 - translate_form[4].x, translate_form[4].h);
    translate_form[5].y = translate_form[4].y + translate_form[4].h + 10;
    ui_table_resize(&translate_form[5], w - 10 - translate_form[5].x, h - 59 - translate_form[5].y);
}

/**
 * View layer
 */
void translate_redraw(int tab)
{
    (void)tab;
    ui_form = translate_form;
    ui_form[0].inactive = (!translate_code[0]);
    ui_form[1].inactive = (!translate_code[0] || !translate_code[1] || !translate_name[0]);
    if(!translate_name[0]) translate_getname(NULL);
}

/**
 * Set translation code
 */
void translate_set(char *code)
{
    if(!code || !code[0] || !code[1]) return;
    if(code[0] == lang[-1][0] && code[1] == lang[-1][1]) {
        ui_status(1, lang[ERR_LANG]);
        return;
    }
    translate_code[0] = code[0];
    translate_code[1] = code[1];
    translate_code[2] = 0;
    translate_name[0] = 0;
    translate_getname(NULL);
    translate_spctbl.clk = translate_spctbl.val = translate_tbl.clk = translate_tbl.val = -1;
    translate_form[6].inactive = 2;
}

/**
 * Clear form, new translation
 */
void translate_new()
{
    translate_code[0] = 0;
    translate_name[0] = 0;
    translate_freestrings();
    translate_spctbl.clk = translate_spctbl.val = translate_tbl.clk = translate_tbl.val = -1;
    translate_form[6].inactive = 2;
}

/**
 * Delete translation
 */
void translate_delete(void *data)
{
    char **list, ext[8];
    int i, num;

    (void)data;
    if(!translate_code[0]) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.po", project.id, project_dirs[PROJDIRS_LANG], translate_code);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("translate_delete: removing %s\n", projdir);
        remove(projdir);
        sprintf(ext, "_%c%c.ogg", translate_code[0], translate_code[1]);
        sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_MEDIA + 2]);
        list = project_getdir(projdir, ext, &num);
        for(i = 0; list && i < num; i++) {
            sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                list[i], translate_code[0], translate_code[1]);
            if(verbose) printf("translate_delete: removing %s\n", projdir);
            remove(projdir);
        }
        project_freedir(&list, &num);
        project_loadlangs();
        translate_new(NULL);
        translate_init(SUBMENU_NEWLANG);
    }
}

/**
 * Save translation
 */
void translate_save(void *data)
{
    trn_t *list = (trn_t*)translate_tbl.data;
    FILE *f;
    int i;

    (void)data;
    if(!translate_code[0] || !translate_code[1] || !translate_name[0]) return;
    if(translate_code[0] == lang[-1][0] && translate_code[1] == lang[-1][1]) {
        ui_status(1, lang[ERR_LANG]);
        return;
    }
    f = project_savefile(0, project_dirs[PROJDIRS_LANG], translate_code, "po", PROJMAGIC_TRANSLATE, "translate_save");
    if(f) {
        project_wrmsg(f, translate_code, translate_name, sizeof(translate_name));
        for(i = 0; i < translate_tbl.num; i++)
            project_wrmsg(f, list[i].id, list[i].str, list[i].len);
        fclose(f);
        project_loadlangs();
        sprintf(projfn, "%s" SEP "%s" SEP "%s.po", project.id, project_dirs[PROJDIRS_LANG], translate_code);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVEPO]);
}

/**
 * Load translation
 */
int translate_loadpo(char *code)
{
    trn_t *list = (trn_t*)translate_tbl.data;
    char *str, *s, *d, fn[3];
    int i, l = 0;

    if(!code || !code[0] || !code[1] || !list) return 0;
    for(i = 0; i < translate_tbl.num; i++)
        list[i].str[0] = 0;
    fn[0] = code[0]; fn[1] = code[1]; fn[2] = 0;
    str = s = project_loadfile(project_dirs[PROJDIRS_LANG], fn, "po", PROJMAGIC_TRANSLATE, "translate_loadpo");
    if(str) {
        i = -1; l = 0;
        while(*s) {
            s = project_skipnl(s);
            if(!memcmp(s, "msgid ", 6)) {
                s = project_getstr(s + 6, &d, 4, PROJ_DESCMAX);
                if(d) {
                    for(i = 0; i < translate_tbl.num && strcmp(list[i].id, d); i++);
                    if(i >= translate_tbl.num) i = -1;
                    free(d);
                }
            } else
            if(!memcmp(s, "msgstr ", 7)) {
                s = project_getstr(s + 7, &d, 4, PROJ_DESCMAX);
                if(d) {
                    if(!l)
                        strncpy(translate_name, d, sizeof(translate_name) - 1);
                    else if(i >= 0 && i < translate_tbl.num)
                        strncpy(list[i].str, d, list[i].len);
                    free(d);
                }
                l = 1;
                i = -1;
            }
        }
        free(str);
        return 1;
    }
    return 0;
}

/**
 * Load translation
 */
void translate_load()
{
    if(!translate_code[0] || !translate_code[1] || !translate_tbl.data) return;
    if(!translate_loadpo(translate_code))
        ui_status(1, lang[ERR_LOADPO]);
}


/**
 * Add a string to translate list
 */
void translate_addstr(char *str, int maxlen)
{
    trn_t *list = (trn_t*)translate_tbl.data;
    int i;

    if(!str || !*str) return;
    if(list) {
        for(i = 0; i < translate_tbl.num && strcmp(list[i].id, str); i++);
        if(i < translate_tbl.num) {
            if(maxlen > list[i].len) {
                list[i].str = (char*)realloc(list[i].str, maxlen + 1);
                list[i].len = maxlen;
            }
            return;
        }
    }
    list = (trn_t*)realloc(translate_tbl.data, (translate_tbl.num + 1) * sizeof(trn_t));
    if(list) {
        list[translate_tbl.num].id = str;
        list[translate_tbl.num].str = (char*)main_alloc(maxlen + 1);
        list[translate_tbl.num++].len = maxlen;
    } else
        translate_tbl.num = 0;
    translate_tbl.data = list;
}
void translate_addstr2(char *str, int maxlen)
{
    char *str2;

    if(!str || !*str) return;
    str2 = (char*)main_alloc(maxlen);
    strcpy(str2, str);
    translate_addstr(str2, maxlen);
}

/**
 * Free the list
 */
void translate_freestrings()
{
    trn_t *translate_list = (trn_t*)translate_tbl.data;
    int i;

    if(!translate_list) return;
    for(i = 0; i < translate_tbl.num; i++) {
        if(translate_list[i].id) free(translate_list[i].id);
        if(translate_list[i].str) free(translate_list[i].str);
    }
    free(translate_list);
    translate_tbl.data = NULL;
    translate_tbl.num = 0;
}

/**
 * Collect all translatable strings
 */
void translate_getstrings()
{
    int i;

    translate_freestrings();

    translate_addstr2(project.name, PROJ_TITLEMAX);
    /* make the language checker happy: GAME_USERNAME, GAME_PASSWORD, GAME_EXISTS, GAME_OVERWR, GAME_SAVEDGAMES,
     * GAME_LOAD, GAME_LOGTOSERVER, GAME_LOGIN, GAME_BADUSER */
    for(i = GAME_LOADGAME; i < NUMTEXTS; i++)
        translate_addstr2(lang[i], PROJ_TITLEMAX);

    cutscn_loadstr();
    attrs_loadstr();
    chars_loadstr();
    npcs_loadstr();
    objects_loadstr();
    crafts_loadstr();
    dialogs_loadstr();
    maps_loadstr();
    alerts_loadstr();
    quests_loadstr();
}

/**
 * Save tng
 */
int translate_totng(tngctx_t *ctx)
{
    trn_t *list = (trn_t*)translate_tbl.data;
    uLongf cl;
    uint8_t *buf, *ptr, *comp;
    char *n;
    int i, j, k, l;

    if(!ctx || !list) return 1;
    /* check if there's at least one translation selected */
    if(ctx->langs) {
        for(j = 0; j < project.numlang + 1 && !ctx->langs[j]; j++);
        if(j >= project.numlang + 1) return 1;
    }
    /* save texts for the original language */
    if(!ctx->langs || ctx->langs[project.numlang]) {
        tng_section(ctx, TNG_SECTION_TRANSLATION);
        for(l = i = 0; i < translate_tbl.num; i++)
            if(list[i].id) l += strlen(list[i].id) + 1;
        buf = ptr = (uint8_t*)main_alloc(l);
        tng_data(ctx, lang[-1], 2);
        n = "Unknown?";
        for(i = 0; translate_names[i]; i++)
            if(lang[-1][0] == translate_names[i][0] && lang[-1][1] == translate_names[i][1]) {
                n = (char*)translate_names[i] + 2;
                break;
            }
        for(i = 0; i < translate_tbl.num; i++)
            if(list[i].id) {
                k = strlen(list[i].id);
                memcpy(ptr, list[i].id, k);
                ptr += k + 1;
            }
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        free(buf);
        if(cl) {
            tng_desc(ctx, n, cl);
            tng_asset_int(ctx, comp, cl);
        } else {
            /* this should never happen, just in case */
            free(comp);
            translate_code[0] = translate_code[1] = 0;
            ui_switchtab(SUBMENU_NEWLANG);
            return 0;
        }
        /* add localized audio tracks */
        if(project.speech && project.numspeech)
            for(i = 0; i < project.numspeech; i++) {
                if(!ctx->idx[TNG_IDX_SPC].ref[i]) continue;
                sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                    project.speech[i], lang[-1][0], lang[-1][1]);
                cl = project_filesize(projdir);
                tng_desc(ctx, project.speech[i], cl);
                tng_asset_ext(ctx, projdir, cl);
            }
    }
    /* save translations */
    for(j = 0; project.languages && j < project.numlang; j++) {
        ui_progressbar(1, 2, project_curr++, project_total, LANG_COLLECT);
        if(ctx->langs && !ctx->langs[j]) continue;
        if(!translate_loadpo(project.languages[j])) {
            translate_code[0] = project.languages[j][0];
            translate_code[1] = project.languages[j][1];
            ui_switchtab(SUBMENU_NEWLANG);
            return 0;
        }
        tng_section(ctx, TNG_SECTION_TRANSLATION);
        for(l = i = 0; i < translate_tbl.num; i++)
            if(list[i].str) l += strlen(list[i].str) + 1;
        buf = ptr = (uint8_t*)main_alloc(l);
        tng_data(ctx, project.languages[j], 2);
        for(i = 0; i < translate_tbl.num; i++)
            if(list[i].str) {
                k = strlen(list[i].str);
                memcpy(ptr, list[i].str, k);
                ptr += k + 1;
            }
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        free(buf);
        if(cl) {
            tng_desc(ctx, translate_name, cl);
            tng_asset_int(ctx, comp, cl);
        } else {
            /* this should never happen, just in case */
            free(comp);
            translate_code[0] = project.languages[j][0];
            translate_code[1] = project.languages[j][1];
            ui_switchtab(SUBMENU_NEWLANG);
            return 0;
        }
        if(project.speech && project.numspeech)
            for(i = 0; i < project.numspeech; i++) {
                if(!ctx->idx[TNG_IDX_SPC].ref[i]) continue;
                sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                    project.speech[i], project.languages[j][0], project.languages[j][1]);
                cl = project_filesize(projdir);
                tng_desc(ctx, project.speech[i], cl);
                tng_asset_ext(ctx, projdir, cl);
            }
    }
    return 1;
}

/**
 * Read from tng
 */
int translate_fromtng(tngctx_t *ctx)
{
    FILE *f;
    trn_t *list = (trn_t*)translate_tbl.data;
    tng_asset_desc_t *asset;
    uint8_t *buf, *ptr, *end;
    char code[3];
    int i, j, k, len;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->f) return 0;
    translate_freestrings();
    for(i = 0, j = -1; i < ctx->numtbl; i++)
        if(TNG_SECTION_TYPE(&ctx->tbl[i]) == TNG_SECTION_TRANSLATION) {
            if(j == -1) j = i;
            buf = ctx->sec + ctx->tbl[i].offs;
            if(buf[0] == lang[-1][0] && buf[1] == lang[-1][1]) { j = i; break; }
        }
    if(j == -1) return 0;
    /* load base translation */
    code[2] = 0;
    memcpy(&code, ctx->sec + ctx->tbl[j].offs, 2);
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[j].offs + 2);
    buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &len);
    if(!buf) return 0;
    strncpy(project.name, (char*)ptr, PROJ_TITLEMAX - 1);
    for(end = buf + len; ptr < end; ptr += len) {
        len = strlen((char*)ptr) + 1;
        list = (trn_t*)realloc(list, (translate_tbl.num + 1) * sizeof(trn_t));
        if(list) {
            list[translate_tbl.num].id = (char*)main_alloc(len);
            memcpy(list[translate_tbl.num].id, ptr, len);
            list[translate_tbl.num].str = NULL;
            list[translate_tbl.num++].len = len;
        } else
            translate_tbl.num = 0;
    }
    translate_tbl.data = list;
    free(buf);
    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[j].offs + 2 + sizeof(tng_asset_desc_t));
    len = (TNG_SECTION_SIZE(&ctx->tbl[j]) - 2 - sizeof(tng_asset_desc_t)) / sizeof(tng_asset_desc_t);
    if(len > 0) {
        project.numspeech = len;
        project.speech = (char**)main_alloc((len + 1) * sizeof(char*));
        sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_MEDIA + 2]);
        project_mkdir(projdir);
        for(i = 0; i < len; i++, asset++) {
            k = strlen((char*)ctx->sts + asset->name) + 1;
            project.speech[i] = (char*)main_alloc(k);
            memcpy(project.speech[i], ctx->sts + asset->name, k);
            sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                ctx->sts + asset->name, code[0], code[1]);
            if(verbose) printf("translate_fromtng: saving %s\n", projdir);
            tng_save_asset(ctx, asset->offs, asset->size, projdir);
            ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
        }
    }
    /* other translations */
    for(i = 0; i < ctx->numtbl; i++)
        if(i != j && TNG_SECTION_TYPE(&ctx->tbl[i]) == TNG_SECTION_TRANSLATION) {
            memcpy(&code, ctx->sec + ctx->tbl[i].offs, 2);
            asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs + 2);
            buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &len);
            if(buf) {
                end = buf + len;
                f = project_savefile(0, project_dirs[PROJDIRS_LANG], code, "po", PROJMAGIC_TRANSLATE, "translate_fromtng");
                if(f) {
                    project_wrmsg(f, code, (char*)ctx->sts + asset->name, sizeof(translate_name));
                    for(k = 0; k < translate_tbl.num && ptr < end; k++, ptr += strlen((char*)ptr) + 1)
                        project_wrmsg(f, list[k].id, (char*)ptr, PROJ_DESCMAX);
                    fclose(f);
                }
                free(buf);
            }
            asset++;
            len = (TNG_SECTION_SIZE(&ctx->tbl[i]) - 2 - sizeof(tng_asset_desc_t)) / sizeof(tng_asset_desc_t);
            if(len > 0) {
                buf = (uint8_t*)main_alloc(1024 * 1024);
                for(; len; len--, asset++) {
                    if(asset->size < 1) continue;
                    sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                        ctx->sts + asset->name, code[0], code[1]);
                    if(verbose) printf("translate_fromtng: saving %s\n", projdir);
                    tng_save_asset(ctx, asset->offs, asset->size, projdir);
                    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
                }
                free(buf);
            }
        }
    return 1;
}
