/*
 * tnge/saveext.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Save extension window
 *
 */

#include "main.h"

extern const char *translate_names[];

void saveext_save(void *data);
void saveext_load(void *data);
void saveext_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void saveext_cellclk(void *data);
void saveext_selall(void *data);

int saveext_tab, saveext_remote = 0, saveext_encrypt = 0;
int saveext_optall = 0, saveext_langall = 0, saveext_mapall = 0;
char saveext_name[PROJ_NAMEMAX], saveext_currlang[35], *saveext_opts = NULL, *saveext_lang = NULL, *saveext_map = NULL;
ui_combo_t saveext_nameinp = { { INP_ID, sizeof(saveext_name), saveext_name }, { -1, 0, NULL } };
ui_input_t saveext_urlinp = { INP_NAME, 256, project.url };

ui_tablehdr_t saveext_optshdr[] = {
    { -1, 20, 0, 0 },
    { SAVEEXT_OPTS, 0, 0, 0 },
    { 0 }
};
ui_table_t saveext_optstbl = { saveext_optshdr, 0, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char),
    saveext_drawcell, NULL, NULL, NULL };

ui_tablehdr_t saveext_langhdr[] = {
    { -1, 20, 0, 0 },
    { SAVEEXT_LANGS, 0, 0, 0 },
    { 0 }
};
ui_table_t saveext_langtbl = { saveext_langhdr, SAVEEXT_NOLANGS, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*),
    saveext_drawcell, NULL, NULL, NULL };

ui_tablehdr_t saveext_maphdr[] = {
    { -1, 20, 0, 0 },
    { SUBMENU_MAPS, 0, 0, 0 },
    { 0 }
};
ui_table_t saveext_maptbl = { saveext_maphdr, MAPS_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*),
    saveext_drawcell, NULL, NULL, NULL };

/**
 * The form
 */
ui_form_t saveext_form[] = {
    { FORM_COMBO, 0, 30, 220, 20, 0, SAVEEXT_NAME, &saveext_nameinp, saveext_load },
    { FORM_BUTTON, 0, 0, 0, 20, 1, 0, NULL, saveext_save },
    /* 2 */
    { FORM_TEXT, 10, 54, 0, 20, 0, 0, NULL, NULL },
    { FORM_BOOL, 0, 54, 16, 16, 0, SAVEEXT_STREMOTE, &saveext_remote, NULL },
    { FORM_INPUT, 0, 54, 0, 20, 0, 0, &saveext_urlinp, NULL },
    { FORM_BOOL, 0, 54, 0, 16, 0, SAVEEXT_ENCRYPT, &saveext_encrypt, NULL },
    /* 6 */
    { FORM_BOOL, 10, 78, 16, 16, 0, SAVEEXT_SELALL, &saveext_optall, saveext_selall },
    { FORM_TABLE, 10, 102, 0, 120, 0, 0, &saveext_optstbl, saveext_cellclk },
    /* 8 */
    { FORM_BOOL, 0, 78, 16, 16, 0, SAVEEXT_SELALL, &saveext_langall, saveext_selall },
    { FORM_TABLE, 10, 102, 0, 120, 0, 0, &saveext_langtbl, saveext_cellclk },
    /* 10 */
    { FORM_BOOL, 0, 78, 16, 16, 0, SAVEEXT_SELALL, &saveext_mapall, saveext_selall },
    { FORM_TABLE, 10, 102, 0, 120, 0, 0, &saveext_maptbl, saveext_cellclk },
    { FORM_LAST }
};

/**
 * Draw table cell
 */
void saveext_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    char **opt = (char**)data, *bools = NULL;

    (void)sel; (void)w; (void)h;
    if(!data) return;
    bools = (hdr == saveext_langhdr ? saveext_lang : (hdr == saveext_maphdr ? saveext_map : saveext_opts));
    ui_bool(dst, x + 6, y, 16, bools ? bools[idx] : 0, NULL);
    ui_text(dst, x + 24, y + 1, hdr == saveext_optshdr ? lang[SUBMENU_ELEMENTS + idx] : (!*opt ? saveext_currlang : *opt));
}

/**
 * Click on a table cell
 */
void saveext_cellclk(void *data)
{
    char *bools = NULL;
    ui_table_t *tbl = (ui_table_t*)data;

    if(!data || !tbl->data || tbl->val < 0) return;
    bools = (tbl == &saveext_langtbl ? saveext_lang : (tbl == &saveext_maptbl ? saveext_map : saveext_opts));
    if(bools) bools[tbl->val] ^= 1;
}

/**
 * Click on a select all checkbox
 */
void saveext_selall(void *data)
{
    char *bools = NULL;
    int i, *all = (int*)data;
    ui_table_t *tbl;

    if(!data) return;
    if(all == &saveext_optall) {
        bools = saveext_opts;
        tbl = &saveext_optstbl;
    } else
    if(all == &saveext_langall) {
        bools = saveext_lang;
        tbl = &saveext_langtbl;
    } else {
        bools = saveext_map;
        tbl = &saveext_maptbl;
    }
    if(tbl) {
        tbl->val = tbl->clk = -1;
        if(bools)
            for(i = 0; i < tbl->num; i++)
                bools[i] = *all;
    }
}

/**
 * Exit saveext window
 */
void saveext_exit(int tab)
{
    (void)tab;
    if(saveext_opts) { free(saveext_opts); saveext_opts = NULL; }
    if(saveext_lang) { free(saveext_lang); saveext_lang = NULL; }
    if(saveext_map) { free(saveext_map); saveext_map = NULL; }
    project_freedir(&saveext_nameinp.sel.opts, NULL);
}

/**
 * Enter saveext window
 */
void saveext_init(int tab)
{
    int i;

    (void)tab;
    strcpy(saveext_currlang, lang[-1]);
    for(i = 0; translate_names[i]; i++)
        if(lang[-1][0] == translate_names[i][0] && lang[-1][1] == translate_names[i][1]) {
            saveext_currlang[2] = ' ';
            strcpy(saveext_currlang + 3, translate_names[i] + 2);
            break;
        }
    saveext_form[1].param = lang[LANG_SAVE];
    saveext_form[1].w = ui_textwidth(saveext_form[1].param) + 40;
    if(saveext_form[1].w < 200) saveext_form[1].w = 200;
    saveext_form[2].param = lang[SAVEEXT_REMOTE];
    saveext_form[2].w = ui_textwidth(saveext_form[2].param);
    saveext_form[5].w = ui_textwidth(lang[saveext_form[5].status]) + 16;
    if(!project.url[0])
        strcpy(project.url, "tng://127.0.0.1:4433");
    saveext_optstbl.num = SUBMENU_QUESTS - SUBMENU_ELEMENTS + 1;
    saveext_langtbl.data = project.languages;
    saveext_langtbl.num = project.numlang + 1;
    saveext_maptbl.data = project.maps;
    saveext_maptbl.num = project.nummaps;
    saveext_optstbl.val = saveext_langtbl.val = saveext_maptbl.val =
    saveext_optstbl.clk = saveext_langtbl.clk = saveext_maptbl.clk = -1;
    if(saveext_optstbl.num)
        saveext_opts = saveext_optstbl.data = (char*)main_alloc(saveext_optstbl.num);
    if(saveext_langtbl.num)
        saveext_lang = (char*)main_alloc(saveext_langtbl.num);
    if(saveext_maptbl.num)
        saveext_map = (char*)main_alloc(saveext_maptbl.num);
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_EXTS]);
    saveext_nameinp.sel.opts = project_getdir(projdir, ".cfg", NULL);
    saveext_load(NULL);
}

/**
 * Resize the view
 */
void saveext_resize(int tab, int w, int h)
{
    (void)tab;
    saveext_form[0].x = w - 10 - saveext_form[0].w;
    saveext_form[1].y = h - 48;
    saveext_form[1].x = w - 20 - saveext_form[1].w;
    saveext_form[3].x = saveext_form[2].x + saveext_form[2].w + 10;
    saveext_form[4].x = saveext_form[3].x + saveext_form[3].w + 4;
    saveext_form[5].x = w - 10 - saveext_form[5].w;
    saveext_form[4].w = saveext_form[5].x - saveext_form[4].x - 20;
    saveext_form[6].w = saveext_form[8].w = saveext_form[10].w = (w - 10) / 3 - 10;
    saveext_form[8].x = saveext_form[9].x = saveext_form[6].x + saveext_form[6].w + 10;
    saveext_form[10].x = saveext_form[11].x = saveext_form[8].x + saveext_form[6].w + 10;
    ui_table_resize(&saveext_form[7], saveext_form[6].w, h - 59 - saveext_form[7].y);
    ui_table_resize(&saveext_form[9], saveext_form[6].w, h - 59 - saveext_form[9].y);
    ui_table_resize(&saveext_form[11], saveext_form[6].w, h - 59 - saveext_form[11].y);
}

/**
 * View layer
 */
void saveext_redraw(int tab)
{
    (void)tab;
    ui_form = saveext_form;
    ui_form[1].inactive = (!saveext_name[0]);
    ui_form[4].inactive = (!saveext_remote);
    ui_form[5].inactive = (!project.author[0]);
    ui_form[6].inactive = (!saveext_opts);
    ui_form[8].inactive = (!saveext_lang);
    ui_form[11].inactive = (saveext_opts && !saveext_opts[SUBMENU_MAPS - SUBMENU_ELEMENTS]);
    ui_form[10].inactive = (!saveext_map || ui_form[11].inactive);
    if(!project.author[0]) saveext_encrypt = 0;
}

/**
 * Save extension
 */
void saveext_save(void *data)
{
    char **list;
    FILE *f;
    int i;

    (void)data;
    if(!saveext_name[0]) return;
    f = project_savefile(0, project_dirs[PROJDIRS_EXTS], saveext_name, "cfg", PROJMAGIC_EXTCFG, "saveext_save");
    if(f) {
        if(!saveext_remote) project.url[0] = 0;
        if(saveext_remote && !project.url[0]) saveext_remote = 0;
        fprintf(f, "%u %s\r\n", saveext_encrypt, project.url[0] ? project.url : "-");
        if(saveext_opts) {
            for(i = 0; i < saveext_optstbl.num; i++)
                if(saveext_opts[i])
                    fprintf(f, "o %u\r\n", i);
        }
        if(saveext_lang && saveext_langtbl.data) {
            for(i = 0; i < project.numlang; i++)
                if(saveext_lang[i])
                    fprintf(f, "l %c%c\r\n", project.languages[i][0], project.languages[i][1]);
            if(saveext_lang[project.numlang])
                fprintf(f, "l %c%c\r\n", lang[-1][0], lang[-1][1]);
        }
        if(saveext_map && saveext_maptbl.data && !saveext_form[11].inactive) {
            list = (char**)saveext_maptbl.data;
            for(i = 0; i < saveext_maptbl.num; i++)
                if(saveext_map[i])
                    fprintf(f, "m %s\r\n", list[i]);
        }
        fclose(f);
        /* totng will change tabs (might be to pages with errors) */
        if(project_totng(saveext_name, (saveext_encrypt ? TNG_FLAG_ENCRYPT : 0), saveext_opts, saveext_lang, saveext_map)) {
            sprintf(projfn, "%s" SEP "%s.tng", project.id, saveext_name);
            ui_status(0, lang[LANG_SAVED]);
        }
    } else
        ui_status(1, lang[ERR_SAVECFG]);
}

/**
 * Load extension configuration
 */
void saveext_load(void *data)
{
    char *str, *s, t;
    int i;

    (void)data;
    if(!saveext_name[0]) return;
    str = project_loadfile(project_dirs[PROJDIRS_EXTS], saveext_name, "cfg", PROJMAGIC_EXTCFG, "saveext_load");
    if(str) {
        if(saveext_opts) memset(saveext_opts, 0, saveext_optstbl.num);
        if(saveext_lang) memset(saveext_lang, 0, saveext_langtbl.num);
        if(saveext_map) memset(saveext_map, 0, saveext_maptbl.num);
        saveext_optall = saveext_langall = saveext_mapall = 0;
        s = project_skipnl(str);
        s = project_getint(s, &saveext_encrypt, 0, 1);
        project.url[0] = 0;
        s = project_getstr2(s, project.url, 2, 256);
        saveext_remote = project.url[0] ? 1 : 0;
        while(*s) {
            s = project_skipnl(s);
            if(!*s) break;
            t = *s++; if(*s == ' ') s++;
            if(!*s) break;
            switch(t) {
                case 'o':
                    s = project_getint(s, &i, 0, saveext_optstbl.num);
                    if(saveext_opts)
                        saveext_opts[i] = 1;
                break;
                case 'l':
                    if(saveext_lang) {
                        if(s[0] == lang[-1][0] && s[1] == lang[-1][1])
                            saveext_lang[project.numlang] = 1;
                        else
                            for(i = 0; i < project.numlang; i++)
                                if(s[0] == project.languages[i][0] && s[1] == project.languages[i][1]) {
                                    saveext_lang[i] = 1;
                                    break;
                                }
                    }
                    s += 2;
                break;
                case 'm':
                    s = project_getidx(s, &i, (char**)saveext_maptbl.data, -1);
                    if(saveext_map && i >= 0 && i < saveext_maptbl.num)
                        saveext_map[i] = 1;
                break;
            }
        }
        free(str);
        if(saveext_opts)
            for(i = 0, saveext_optall = 1; i < saveext_optstbl.num; i++)
                if(!saveext_opts[i]) { saveext_optall = 0; break; }
        if(saveext_lang)
            for(i = 0, saveext_langall = 1; i < saveext_langtbl.num; i++)
                if(!saveext_lang[i]) { saveext_langall = 0; break; }
        if(saveext_map)
            for(i = 0, saveext_mapall = 1; i < saveext_maptbl.num; i++)
                if(!saveext_map[i]) { saveext_mapall = 0; break; }
    }
}
