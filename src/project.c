/*
 * tnge/project.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Project object
 *
 */

#include "main.h"
#include "zstd/zstd.h"
#include "mbedtls/common.h"
#include "mbedtls/sha256.h"

#ifdef __WIN32__
/* only include these when necessary, it conflicts with SDL_mixer... */
#include <windows.h>
#include <winnls.h>
#include <fileapi.h>
#include <shlobj.h>
#include <shellapi.h>
wchar_t szFull[PATH_MAX + FILENAME_MAX + 1];
char wdname[FILENAME_MAX + 1], ffmpegexe[256];
#define dname wdname
#else
#define dname ent->d_name
#endif

/* keep order, other file use this too. Must match PROJDIRS_* defines */
const char *project_dirs[] = {
    "music", "sounds", "speech", "movies",                      /* media.c uses these */
    "ui", "tiles", "chars", "portraits", "backs",               /* newsprite.c, sprites.c, spr.c uses these */
    "objects", "fonts", "game", "npcs", "dialogs", "maps", "quests", "lang", "ext", NULL }; /* project_load() and others */

const char *project_directions[] = { "so", "sw", "we", "nw", "no", "ne", "ea", "se", NULL };
const char *project_animtypes = "ofl";
const char *project_animframes = "0123456789ABCDEFGHIJKLMNOPQRSTUV";
const char *project_eventcodes[] = { "click", "touch", "leave", "approach", "action1", "action2", "action3", "action4",
    "action5", "action6", "action7", "action8", "action9", "using1", "using2", "using3", "timer", "sell", "buy", NULL };
char *project_eventtimers[] = { "1/10s", "1s", "10s", "20s", "30s", "1m", "2m", "3m", "5m", "10m", "20m", "30m", "1h", NULL };

/* important: longer match first */
char *project_rels[] = { "=", "!=", ">=", ">", "<=", "<", NULL };
char *project_behs[4] = { 0 }, *project_trns[4] = { 0 };
char project_times[192], *project_clock[25] = { 0 }, *project_actions[20] = { 0 };

project_t project;
char *projdir = NULL, *projfn = NULL;
#ifdef __WIN32__
struct _stat64 projst;
#else
struct stat projst;
#endif
uint64_t project_curr = 0, project_total = 0, project_last = 0;

static char full[PATH_MAX + FILENAME_MAX + 1];
static int skipbytes;
extern char fileops_path[];
extern int media_ffmpeg;
extern ui_table_t maps_tbl;
#ifdef _TNGE_PRO_
int  pro_license(char *id, uint8_t *buf);
void pro_encmask();
void pro_encbase(unsigned char *o);
void pro_encode(unsigned char *s, char *o);
void pro_decode(char *s, unsigned char *o);
void pro_encrypt(uint64_t off, int len, unsigned char *buf);
#endif

/* template file format */
#define ZIP_DATA_STORE      0
#define ZIP_DATA_DEFLATE    8
#define ZIP_DATA_ZSTD       93
#define ZIP_LOCAL_MAGIC     0x04034b50
#define ZIP_CENTRAL_MAGIC   0x02014b50
#define ZIP_EOCD32_MAGIC    0x06054b50
typedef struct {
    uint32_t    magic;
    uint16_t    version;
    uint16_t    flags;
    uint16_t    method;
    uint32_t    mtime;
    uint32_t    crc32;
    uint32_t    comp;
    uint32_t    orig;
    uint16_t    fnlen;
    uint16_t    extlen;
} __attribute__((packed)) zip_local_t;
typedef struct {
    uint16_t    magic;
    uint16_t    size;
    uint64_t    orig;
    uint64_t    comp;
} __attribute__((packed)) zip_ext64_t;
typedef struct {
    uint32_t    magic;
    uint16_t    madeby;
    uint16_t    version;
    uint16_t    flags;
    uint16_t    method;
    uint32_t    mtime;
    uint32_t    crc32;
    uint32_t    comp;
    uint32_t    orig;
    uint16_t    fnlen;
    uint16_t    extlen;
    uint16_t    comlen;
    uint16_t    disk;
    uint16_t    iattr;
    uint32_t    eattr;
    uint32_t    offs;
    char *fn;
} __attribute__((packed)) zip_central_t;
typedef struct {
    uint32_t    magic;
    uint16_t    numdisk;
    uint16_t    strtdisk;
    uint16_t    totdisk;
    uint16_t    nument;
    uint32_t    cdsize;
    uint32_t    totoffs;
    uint16_t    comlen;
} __attribute__((packed)) zip_eocd32_t;

/**
 * Helper, natural comparition
 */
#define  pr_isdigit( x )   ( ( (unsigned)(x) - '0' ) < 10U )
int strnatcmp(const void *a, const void *b)
{
    const char *ca, *cb;
    int result = 0;
    for(ca = *((const char**)a), cb = *((const char**)b); *ca && *cb; ca++, cb++) {
        if(pr_isdigit(*ca) && pr_isdigit(*cb)) {
            for(result = 0; pr_isdigit(*ca) && pr_isdigit(*cb); ca++, cb++)
                if(!result) result = *ca - *cb;
            if(!pr_isdigit(*ca) && pr_isdigit(*cb)) return -1;
            if(!pr_isdigit(*cb) && pr_isdigit(*ca)) return +1;
            if(result || (!*ca && !*cb)) return result;
        }
        result = strncasecmp(ca, cb, 1);
        if(result) return result;
    }
    return *ca - *cb;
}

/**
 * Check if a project exists
 */
int project_exists(char *id)
{
    if(id != projdir && id != projfn) strcpy(projfn, id);
#ifdef __WIN32__
    MultiByteToWideChar(CP_UTF8, 0, projdir, -1, szFull, PATH_MAX);
    return !_wstat64(szFull, &projst);
#else
    return !stat(projdir, &projst);
#endif
}

/**
 * File abstraction for seeking in big files
 */
void project_fseek(FILE *f, int64_t offs)
{
#ifdef __WIN32__
    fpos_t pos = (fpos_t)offs;
#else
    fpos_t pos = {0};
    pos.__pos = offs;
#endif
    if(f) fsetpos(f, &pos);
}

/**
 * Get asset size (can be max 4G)
 */
uint32_t project_filesize(char *path)
{
    projst.st_size = 0;
#ifdef __WIN32__
    MultiByteToWideChar(CP_UTF8, 0, path, -1, szFull, PATH_MAX);
    _wstat64(szFull, &projst);
#else
    stat(path, &projst);
#endif
    return (uint32_t)projst.st_size;
}

/**
 * Get file sizes
 */
uint64_t project_getsizes(char *path, char *ext)
{
#ifdef __WIN32__
    _WDIR *dir;
    struct _wdirent *ent;
#else
    DIR *dir;
    struct dirent *ent;
#endif
    int l, e = ext ? strlen(ext) : 0;
    char *fn, *fn2;
    uint64_t ret = 0;

    l = strlen(path);
    fn = (char*)malloc(l + 2 + FILENAME_MAX);
    if(!fn) return 0;
    memcpy(fn, path, l); fn2 = fn + l;
    if(fn2[-1] != SEP[0]) *fn2++ = SEP[0];
#ifdef __WIN32__
    MultiByteToWideChar(CP_UTF8, 0, path, -1, szFull, PATH_MAX);
    if((dir = _wopendir(szFull)) != NULL) {
        while ((ent = _wreaddir(dir)) != NULL) {
            if(ent->d_name[0] == L'.') continue;
            wdname[WideCharToMultiByte(CP_UTF8, 0, ent->d_name, wcslen(ent->d_name), wdname, FILENAME_MAX, NULL, NULL)] = 0;
            strcpy(fn2, wdname);
            MultiByteToWideChar(CP_UTF8, 0, fn, -1, szFull, PATH_MAX);
            if(_wstat64(szFull, &projst)) continue;
#else
    if((dir = opendir(path)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if(ent->d_name[0] == '.') continue;
            strcpy(fn2, ent->d_name);
            if(stat(fn, &projst)) continue;
#endif
            if((!ext && !S_ISDIR(projst.st_mode))) continue;
            l = strlen(dname);
            if(ext && e && strcmp(dname + l - e, ext)) continue;
            ret += (uint64_t)projst.st_size;
        }
#ifdef __WIN32__
        _wclosedir(dir);
#else
        closedir(dir);
#endif
    }
    free(fn);
    return ret;
}

/**
 * Read directories for projects
 */
char **project_getdir(char *path, char *ext, int *num)
{
#ifdef __WIN32__
    _WDIR *dir;
    struct _wdirent *ent;
#else
    DIR *dir;
    struct dirent *ent;
#endif
    FILE *f;
    int n = 0, l, e = ext ? strlen(ext) : 0;
    char **ret, *fn, *fn2, data[256], *c;

    if(num) *num = 0;
    ret = (char**)malloc(sizeof(char*));
    if(!ret) return NULL;
    ret[0] = NULL;
    l = strlen(path);
    fn = (char*)malloc(l + 2 + FILENAME_MAX);
    if(!fn) { free(ret); return NULL; }
    memcpy(fn, path, l); fn2 = fn + l;
    if(fn2[-1] != SEP[0]) *fn2++ = SEP[0];
#ifdef __WIN32__
    MultiByteToWideChar(CP_UTF8, 0, path, -1, szFull, PATH_MAX);
    if((dir = _wopendir(szFull)) != NULL) {
        while ((ent = _wreaddir(dir)) != NULL) {
            if(ent->d_name[0] == L'.') continue;
            wdname[WideCharToMultiByte(CP_UTF8, 0, ent->d_name, wcslen(ent->d_name), wdname, FILENAME_MAX, NULL, NULL)] = 0;
            strcpy(fn2, wdname);
            MultiByteToWideChar(CP_UTF8, 0, fn, -1, szFull, PATH_MAX);
            if(_wstat64(szFull, &projst)) continue;
#else
    if((dir = opendir(path)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if(ent->d_name[0] == '.') continue;
            strcpy(fn2, ent->d_name);
            if(stat(fn, &projst)) continue;
#endif
            if((!ext && !S_ISDIR(projst.st_mode))) continue;
            l = strlen(dname);
            if(ext && e && ext[0] != '@' && strcmp(dname + l - e, ext)) continue;
            ret = (char**)realloc(ret, (n+2)*sizeof(char*));
            if(!ret) {
#ifdef __WIN32__
                _wclosedir(dir);
#else
                closedir(dir);
#endif
                free(fn); return NULL;
            }
            ret[n] = ret[n + 1] = NULL;
            if(ext && e && ext[0] == '@') {
                strcpy(fn2 + l, SEP "config");
                memset(data, 0, sizeof(data));
                f = project_fopen(fn, "rb");
                if(f) {
                    l = fread(data, 256, 1, f);
                    fclose(f);
                }
                if(memcmp(data, PROJMAGIC_CONFIG, 16)) continue;
                ret[n] = malloc(196);
                if(ret[n]) {
                    memset(ret[n], 0, 196);
                    ret[n][154] = atoi(data + 16);
                    for(c = data; *c && *c != '\r' && *c != '\n'; c++);
                    for(; *c == '\r' || *c == '\n'; c++);
                    for(l = 0; l < 127 && c[l] != '\r' && c[l] != '\n'; l++);
                    memcpy(ret[n], c, l); ret[n][127] = 0;
                    memcpy(ret[n] + 128, dname, 16);
                    while(c[l] != '\r' && c[l] != '\n') l++;
                    while(c[l] == '\r' || c[l] == '\n') l++;
                    strncpy(ret[n] + 156, c + l, 39);
                    ret[++n] = NULL;
                }
            } else {
                ret[n] = malloc(l + 1);
                if(ret[n]) { strcpy(ret[n], dname); ret[n][l - e] = 0; ret[++n] = NULL; }
            }
        }
#ifdef __WIN32__
        _wclosedir(dir);
#else
        closedir(dir);
#endif
        qsort(ret, n, sizeof(char*), strnatcmp);
        if(num) *num = n;
    }
    free(fn);
    return ret;
}

/**
 * Free a directory list
 */
void project_freedir(char ***list, int *num)
{
    int i;

    if(list && *list) {
        for(i = 0; (!num || i < *num) && (*list)[i]; i++) free((*list)[i]);
        free(*list);
        *list = NULL;
    }
    if(num) *num = 0;
}

/**
 * Create a windows shortcut
 */
void project_mklnk(char *fn, char *arg)
{
    /* disgusting? Absolutely yes, thank you Microsoft! */
    char hdr[0x4c+1] = "\x4c\x0\x0\x0\x1\x14\x2\x0\x0\x0\x0\x0\xc0\x0\x0\x0\x0\x0\x0\x46\xc5\x1\x0\x0"
    "\x20\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0"
    "\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x01\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0"
    "\x0\x0\x0\x0";
    char idlist[0x165+1] = "\x63\x1\x14\x0\x1f\x50\xe0\x4f\xd0\x20\xea\x3a\x69\x10\xa2\xd8\x8\x0\x2b\x30"
    "\x30\x9d\x19\x0/C:\\\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0"
    "\x0\x0\x0\x88\x0\x31\x0\x0\x0\x0\x0\x89\x53\xfa\x34\x11\x0\x50\x52\x4f\x47\x52\x41\x7e"
    "\x31\x0\x0\x70\x0\x8\x0\x4\x0\xef\xbe\xee\x3a\x85\x1a\x89\x53\xfa\x34\x2a\x0\x0\x0\x3c"
    "\x0\x0\x0\x0\x0\x1\x0\x0\x0\x0\x0\x0\x0\x0\x0\x46\x0\x0\x0\x0\x0\x50\x0\x72"
    "\x0\x6f\x0\x67\x0\x72\x0\x61\x0\x6d\x0\x20\x0\x46\x0\x69\x0\x6c\x0\x65\x0\x73\x0\x0"
    "\x0\x40\x0\x73\x0\x68\x0\x65\x0\x6c\x0\x6c\x0\x33\x0\x32\x0\x2e\x0\x64\x0\x6c\x0\x6c"
    "\x0\x2c\x0\x2d\x0\x32\x0\x31\x0\x37\x0\x38\x0\x31\x0\x0\x0\x18\x0\x56\x0\x31\x0\x0"
    "\x0\x0\x0\x89\x53\xfa\x34\x10\x0\x54\x69\x72\x4e\x61\x6e\x6f\x47\x0\x0\x3e\x0\x8\x0\x4"
    "\x0\xef\xbe\x89\x53\xfa\x34\x89\x53\xfa\x34\x2a\x0\x0\x0\x97\x2e\x1\x0\x0\x0\x10\x0\x0"
    "\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x54\x0\x69\x0\x72\x0\x4e\x0\x61\x0\x6e"
    "\x0\x6f\x0\x47\x0\x0\x0\x18\x0\x56\x0\x32\x0\xcb\x18\xa7\x0\xbb\x52\x2d\x3c\x20\x0\x74"
    "\x6e\x67\x65\x2e\x65\x78\x65\x0\x0\x3e\x0\x8\x0\x4\x0\xef\xbe\xbb\x52\x18\x1f\xbb\x62\x18"
    "\x1f\x2a\x0\x0\x0\x7e\xe7\x0\x0\x0\x0\x7\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0"
    "\x0\x0\x0\x74\x0\x6e\x0\x67\x0\x65\x0\x2e\x0\x65\x0\x78\x0\x65\x0\x0\x0\x18\x0\x0\x0";
    char icon[0x4A+1] = "\x22\x0\x43\x0\x3a\x0\x5c\x0\x50\x0\x72"
    "\x0\x6f\x0\x67\x0\x72\x0\x61\x0\x6d\x0\x20\x0\x46\x0\x69\x0\x6c\x0\x65\x0\x73\x0\x5c"
    "\x0\x54\x0\x69\x0\x72\x0\x4e\x0\x61\x0\x6e\x0\x6f\x0\x47\x0\x5c\x0\x74\x0\x6e\x0\x67"
    "\x0\x65\x0\x2e\x0\x65\x0\x78\x0\x65\x0\x0\x0\x0\x0";
    char *s;
    uint16_t par[512] = { 0 }, *d;
    FILE *f = project_fopen(fn, "wb+");

    if(!f) return;
    if(arg) {
        hdr[0x14] |= 0x20;
        idlist[0x11e] = idlist[0x155] = icon[0x3c] = 'p';
    }
    fwrite(hdr, 0x4c, 1, f);
    fwrite(idlist, 0x165, 1, f);
    for(s = arg ? project.name : "TirNanoG Editor", d = par + 1; *s && par[0] < 256; d++) {
        par[0]++;
        if((*s & 128) != 0) {
            if(!(*s & 32)) { *d = ((*s & 0x1F)<<6)|(*(s+1) & 0x3F); s++; } else
            if(!(*s & 16)) { *d = ((*s & 0xF)<<12)|((*(s+1) & 0x3F)<<6)|(*(s+2) & 0x3F); s += 2; } else
            if(!(*s & 8)) { *d = ((*s & 0x7)<<18)|((*(s+1) & 0x3F)<<12)|((*(s+2) & 0x3F)<<6)|(*(s+3) & 0x3F); s += 3; }
            else break;
        } else
            *d = *s++;
    }
    fwrite(par, (par[0] + 1) * 2, 1, f);
    if(arg) {
        for(s = arg, d = par + 1; *s && par[0] < 256; d++) {
            par[0]++;
            if((*s & 128) != 0) {
                if(!(*s & 32)) { *d = ((*s & 0x1F)<<6)|(*(s+1) & 0x3F); s++; } else
                if(!(*s & 16)) { *d = ((*s & 0xF)<<12)|((*(s+1) & 0x3F)<<6)|(*(s+2) & 0x3F); s += 2; } else
                if(!(*s & 8)) { *d = ((*s & 0x7)<<18)|((*(s+1) & 0x3F)<<12)|((*(s+2) & 0x3F)<<6)|(*(s+3) & 0x3F); s += 3; }
                else break;
            } else
                *d = *s++;
        }
        fwrite(par, (par[0] + 1) * 2, 1, f);
    }
    fwrite(icon, 0x4A, 1, f);
    fclose(f);
}

/**
 * Create a Linux desktop file
 */
void project_mkdsk(char *fn, char *arg)
{
    FILE *f = project_fopen(fn, "wb+");

    if(!f) return;
    fprintf(f, "[Desktop Entry]\n"
        "Version=1.0\n"
        "Type=Application\n"
        "Name=%s\n"
        "Exec=/usr/bin/tngp%s%s\n"
        "Icon=tngp\n"
        "Terminal=false\n"
        "StartupNotify=false\n"
        "Categories=Games;\n", project.name, arg ? " " : "", arg ? arg : "");
    fclose(f);
}

/**
 * Initialize directory paths
 */
void project_initdir(int menu)
{
#ifdef __WIN32__
    wchar_t *tmp = NULL, path[PATH_MAX];
    int wlen;
#else
    const char *globaldir = "/usr/share/applications/TirNanoG.desktop";
    char *tmp;
    FILE *f;
#endif
    int i;
    char *s, *d;

    memset(fileops_path, 0, PATH_MAX + FILENAME_MAX);
    strcpy(fileops_path, "." SEP);
#ifdef __WIN32__
    strcpy(ffmpegexe, FFMPEG);
    MultiByteToWideChar(CP_UTF8, 0, FFMPEG, -1, szFull, PATH_MAX);
    media_ffmpeg = !_wstat64(szFull, &projst);
    if(!media_ffmpeg && GetEnvironmentVariableW(L"PATH", path, PATH_MAX)) {
        ffmpegexe[0] = 0; tmp = path;
        while(!media_ffmpeg && tmp[0]) {
            if(tmp[0] == L';') tmp++;
            for(i = 0; tmp[0] && tmp[0] != L';'; i++, tmp++) szFull[i] = tmp[0];
            memcpy(&szFull[i], L"\\ffmpeg.exe", 24);
            media_ffmpeg = !_wstat64(szFull, &projst);
        }
    }
    tmp = (wchar_t*)main_alloc(PATH_MAX + FILENAME_MAX);
    if(!SHGetFolderPathW(HWND_DESKTOP, CSIDL_PROFILE, NULL, 0, tmp)) {
        wlen = WideCharToMultiByte(CP_UTF8, 0, tmp, wcslen(tmp), fileops_path, PATH_MAX, NULL, NULL);
        if(wlen && !projdir) {
            projdir = (char*)main_alloc(PATH_MAX + FILENAME_MAX);
            strcpy(projdir, fileops_path);
            projfn = projdir + strlen(projdir);
            if(menu) {
                strcpy(projfn, "\\AppData"); project_mkdir(projdir);
                strcat(projfn, "\\Roaming"); project_mkdir(projdir);
                strcat(projfn, "\\Microsoft"); project_mkdir(projdir);
                strcat(projfn, "\\Windows"); project_mkdir(projdir);
                strcat(projfn, "\\Start Menu"); project_mkdir(projdir);
                strcat(projfn, "\\Programs"); project_mkdir(projdir);
                strcat(projfn, "\\TirNanoG.lnk");
                project_mklnk(projdir, NULL);
            }
            strcpy(projfn, "\\TirNanoG\\");
        }
    }
    free(tmp);
#else
    media_ffmpeg = !stat(FFMPEG, &projst);
    if(!media_ffmpeg && (tmp = getenv("PATH"))) {
        while(!media_ffmpeg && tmp[0]) {
            if(tmp[0] == ':') tmp++;
            for(i = 0; tmp[0] && tmp[0] != ':'; i++, tmp++) full[i] = tmp[0];
            strcpy(full + i, "/ffmpeg");
            media_ffmpeg = !stat(full, &projst);
        }
    }
    tmp = getenv("HOME");
    if(tmp) {
        strcpy(fileops_path, tmp);
        if(!projdir) {
            projdir = (char*)main_alloc(PATH_MAX + FILENAME_MAX);
            strcpy(projdir, tmp);
            projfn = projdir + strlen(projdir);
#if _Linux_
            if(menu) {
                f = fopen(globaldir, "rb");
                if(f) fclose(f);
                else {
                    f = fopen(globaldir, "wb+");
                    if(!f) {
                        strcpy(projfn, "/.local"); mkdir(projdir, 0755);
                        strcat(projfn, "/share"); mkdir(projdir, 0755);
                        strcat(projfn, "/applications"); mkdir(projdir, 0755);
                        strcat(projfn, "/TirNanoG.desktop");
                        f = fopen(projdir, "wb+");
                    }
                    if(f) {
                        fwrite(&binary_tnge_desktop, sizeof(binary_tnge_desktop), 1, f);
                        fclose(f);
                    }
                }
            }
#else
            (void)f;
#endif
            strcpy(projfn, "/TirNanoG/");
        }
    }
#endif
    /* should never happen */
    if(!projdir) {
        printf("tnge: no projdir???\n");
        exit(1);
    }
    projfn = projdir + strlen(projdir);
    project_mkdir(NULL);
    memset(&project, 0, sizeof(project_t));
    /* not really dir related, but no better place for initialize these */
    project_behs[0] = lang[CMD_BEH_ATTACK];
    project_behs[1] = lang[CMD_BEH_NEUTRAL];
    project_behs[2] = lang[CMD_BEH_FLEE];
    project_trns[0] = lang[CMD_TRN_WALK];
    project_trns[1] = lang[CMD_TRN_SWIM];
    project_trns[2] = lang[CMD_TRN_FLY];
    for(i = 0; i < 24; i++) {
        project_clock[i] = d = &project_times[i * 8];
        for(s = lang[LANG_TIMEFMT]; *s; s++) {
            if(*s == '%') {
                s++;
                switch(*s) {
                    case 'h': d += sprintf(d, "%2u", i == 0 || i == 12 ? 12 : i % 12); break;
                    case 'H': d += sprintf(d, "%02u", i); break;
                    case 'p': *d++ = (i < 12 ? 'a' : 'p'); *d++ = 'm'; break;
                }
            } else
                *d++ = *s;
        }
        *d = 0;
    }
}

/**
 * Make a directory
 */
void project_mkdir(char *path)
{
    char *s, *d;

    if(path != projfn && path != projdir) {
        *projfn = 0;
        for(s = path, d = projfn; s && *s; s++) {
#ifdef __WIN32__
            *d++ = (*s == '/' ? '\\' : *s);
#else
            *d++ = (*s == '\\' ? '/' : *s);
#endif
        }
        if(d > projfn && d[-1] == SEP[0]) d--;
        *d = 0;
    }
#ifdef __WIN32__
    MultiByteToWideChar(CP_UTF8, 0, projdir, -1, szFull, PATH_MAX);
    _wmkdir(szFull);
#else
    mkdir(projdir, 0755);
#endif
}

/**
 * Open a file
 */
FILE *project_fopen(const char *fn, const char *mode)
{
    FILE *f = NULL;
#ifdef __WIN32__
    wchar_t wmode[8];

    memset(wmode, 0, sizeof(wmode));
    MultiByteToWideChar(CP_UTF8, 0, mode, -1, wmode, 8);
    MultiByteToWideChar(CP_UTF8, 0, fn, -1, szFull, PATH_MAX);
    f = _wfopen(szFull, wmode);
#else
    f = fopen(fn, mode);
#endif
    return f;
}

/**
 * Delete a file
 */
void project_rm(char *path)
{
#ifdef __WIN32__
    MultiByteToWideChar(CP_UTF8, 0, path, -1, szFull, PATH_MAX);
    _wremove(szFull);
    /* workaround a bug in mingw. remove() should remove directories too according to POSIX */
    _wrmdir(szFull);
#else
    remove(path);
#endif
}

/**
 * Recursively delete a directory
 */
void project_rmdir(char *path, int parent)
{
    /* this is recursive, so do not allocate big arrays on its local stack */
#ifdef __WIN32__
    _WDIR *dir;
    struct _wdirent *ent;
#else
    DIR *dir;
    struct dirent *ent;
#endif
    int i;

    if(!parent) {
        i = strlen(path);
        if(!i) return;
        strcpy(full, path);
        if(full[i - 1] == SEP[0])
            i--;
        full[i] = 0;
        skipbytes = i + 1;
    } else
        i = strlen(full);
#ifdef __WIN32__
    MultiByteToWideChar(CP_UTF8, 0, full, -1, szFull, PATH_MAX);
    if ((dir = _wopendir(szFull)) != NULL) {
        while ((ent = _wreaddir(dir)) != NULL) {
            wdname[WideCharToMultiByte(CP_UTF8, 0, ent->d_name, wcslen(ent->d_name), wdname, FILENAME_MAX, NULL, NULL)] = 0;
#else
    if ((dir = opendir(full)) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
#endif
            if(!strcmp(dname, ".") || !strcmp(dname, "..")) continue;
            strncpy(full + i, SEP, sizeof(full) - i - 1);
            strncpy(full + i + 1, dname, sizeof(full) - i - 2);
#ifdef __WIN32__
            MultiByteToWideChar(CP_UTF8, 0, full, -1, szFull, PATH_MAX);
            if(_wstat64(szFull, &projst)) continue;
#else
            if(stat(full, &projst)) continue;
#endif
            if(S_ISDIR(projst.st_mode))
                project_rmdir(NULL, parent + 1);
            project_rm(full);
            full[i] = 0;
        }
#ifdef __WIN32__
        _wclosedir(dir);
#else
        closedir(dir);
#endif
    }
    if(!parent)
        project_rm(full);
}

/**
 * Load a project configuration
 */
void project_load(char *id)
{
    FILE *f;
    unsigned int i;
    unsigned char buf[256], *s, *b;
    char **list;
    memset(buf, 0, sizeof(buf));

    strcpy(projfn, id);
    strcat(projfn, SEP "config");
    if(verbose) printf("project_load: reading %s\n", projdir);
    f = project_fopen(projdir, "rb");
    if(f) {
        i = fread(buf, sizeof(buf) - 1, 1, f);
        fclose(f);
    }
    if(!memcmp(buf, PROJMAGIC_CONFIG, 16)) {
        for(b = buf + 16; *b == ' '; b++);
        project.rev = atoi((char*)b);
        if(project.rev > PROJ_FORMATREV) {
            if(verbose) printf("project_load: unsupported project format (revision %u > %u)\n", project.rev, PROJ_FORMATREV);
            goto err;
        }
        /* if needed, use switch(project.rev) throughout the code to keep backward compatibility with older project formats */
        for(; *b && *b != '\r' && *b != '\n'; b++);
        if(!*b) goto err;
        for(; *b == '\r' || *b == '\n'; b++);
        if(!*b) goto err;
        for(s = b; *s && *s != '\r' && *s != '\n'; s++);
        if(!*s) goto err;
        i = s - b;
        memcpy(&project.name, b, i > PROJ_TITLEMAX ? PROJ_TITLEMAX : i);
        while(*s && (*s == '\r' || *s == '\n')) s++;
        if(!*s) goto err;
        if(!memcmp(s, "iso", 3)) project.type = TNG_TYPE_ISO; else
        if(!memcmp(s, "hexv", 4)) project.type = TNG_TYPE_HEXV; else
        if(!memcmp(s, "hexh", 4)) project.type = TNG_TYPE_HEXH; else
        if(!memcmp(s, "3d", 2)) project.type = TNG_TYPE_3D; else
            project.type = TNG_TYPE_ORTHO;
        while(*s && *s != ' ') { s++; } while(*s == ' ') { s++; } project.tilew = atoi((char*)s);
        if(project.tilew < 8) project.tilew = 8;
        if(project.tilew > 512) project.tilew = 512;
        while(*s && *s != ' ') { s++; } while(*s == ' ') { s++; } project.tileh = atoi((char*)s);
        if(project.tileh < 8) project.tileh = 8;
        if(project.tileh > 512) project.tileh = 512;
        while(*s && *s != ' ') { s++; } while(*s == ' ') { s++; } project.mapsize = atoi((char*)s);
        if(!project.mapsize) project.mapsize = PROJ_MAPSIZE;
        /* make sure map size is power of two, 8 to 256 */
        for(i = 3; (1 << i) < project.mapsize && i < 8; i++);
        project.mapsize = (1 << i);
        memset(buf, 0, sizeof(buf));
        if(renderer) ui_title(project.name);
        maps_tbl.val = maps_tbl.clk = -1;
    } else {
err:    if(verbose) printf("project_load: unable to load %s\n", projdir);
        ui_status(1, lang[ERR_LOADCFG]);
        return;
    }
    project_loadlicense(id);
    ui_progressbar(0, 0, 0, 100, LANG_LOADING);
    project_loadlangs();
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_FONTS]);
    project.fonts = project_getdir(projdir, ".sfn", &project.numfont);
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_UI]);
    project.choosers = project_getdir(projdir, ".chr", &project.numchoosers);
    project.cuts = project_getdir(projdir, ".cut", &project.numcuts);
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_GAME]);
    project.attrs = project_getdir(projdir, ".atr", &project.numattrs);
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_GAME]);
    project.chars = project_getdir(projdir, ".opt", &project.numchars);
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_DIALOGS]);
    project.dialogs = project_getdir(projdir, ".dlg", &project.numdialogs);
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_DIALOGS]);
    project.crafts = project_getdir(projdir, ".cft", &project.numcrafts);
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_MAPS]);
    project.maps = project_getdir(projdir, ".tmx", &project.nummaps);
    project.mapidx = (int*)main_alloc((project.nummaps + 1) * sizeof(int));
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_QUESTS]);
    project.quests = project_getdir(projdir, ".qst", &project.numquests);
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_MEDIA + 0]);
    project.music = project_getdir(projdir, ".ogg", &project.nummusic);
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_MEDIA + 1]);
    project.sounds = project_getdir(projdir, ".ogg", &project.numsounds);
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_MEDIA + 2]);
    sprintf((char*)buf, "_%c%c.ogg", lang[-1][0], lang[-1][1]);
    project.speech = project_getdir(projdir, (char*)buf, &project.numspeech);
    sprintf(projfn, "%s" SEP "%s", id, project_dirs[PROJDIRS_MEDIA + 3]);
    project.movies = project_getdir(projdir, ".ogv", &project.nummovies);
    spr_init(id);
    alerts_load();
    /* we don't need the list, but this also marks object sprites */
    list = project_loadobjs(NULL);
    project_freedir(&list, NULL);

    memset(buf, 0, sizeof(buf));
    strcpy(projfn, id);
    strcat(projfn, SEP "game.tng");
    f = project_fopen(projdir, "rb");
    if(f) { i = fread(buf, 15, 1, f); fclose(f); }
    project.hasGame = !memcmp(buf, "#!/usr/bin/tngp", 15);
    actions_load(1);
    project_loadactions();
}

/**
 * Load the editor license
 */
void project_loadlicense(char *id)
{
#ifdef _TNGE_PRO_
    FILE *f;
    unsigned char buf[2048];

    strcpy(projfn, id);
    strcat(projfn, SEP "license.txt");
    f = project_fopen(projdir, "rb");
    if(f) {
        memset(buf, 0, sizeof(buf));
        if(fread(buf, 1, sizeof(buf) - 1, f))
            pro_license(id, buf);
        fclose(f);
    }
#endif
    strncpy(project.id, id, 16);
}

/**
 * Load project's translations list
 */
void project_loadlangs()
{
    FILE *f;
    unsigned char buf[2048], *s, *b, *d;
    int i;

    project_freedir(&project.languages, &project.numlang);
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_LANG]);
    project.languages = project_getdir(projdir, ".po", NULL);
    project.langw = ui_menu_langminw;
    project.numlang = 0;
    if(project.languages) {
        for(project.numlang = 0; project.languages[project.numlang]; project.numlang++) {
            sprintf(projfn, "%s" SEP "%s" SEP "%s.po", project.id, project_dirs[PROJDIRS_LANG],
                project.languages[project.numlang]);
            f = project_fopen(projdir, "rb");
            if(f) {
                memset(buf, 0, sizeof(buf));
                i = fread(buf, sizeof(buf) - 1, 1, f);
                fclose(f);
                for(s = buf; *s; s++)
                    if(!memcmp(s, "msgstr", 6)) {
                        while(*s && *s != '\"') s++;
                        if(!*s) break;
                        s++; for(b = s; *b && *b != '\"' && b - s < 32; b++);
                        project.languages[project.numlang] = (char*)realloc(project.languages[project.numlang], b - s + 4);
                        if(project.languages[project.numlang]) {
                            project.languages[project.numlang][2] = ' ';
                            for(d = (unsigned char*)project.languages[project.numlang] + 3; *s && s < b; s++, d++) {
                                if(*s == '\\') s++;
                                *d = *s;
                            }
                            *d = 0;
                        }
                        break;
                    }
                i = ui_textwidth(project.languages[project.numlang]) + 40;
                if(project.langw < i) project.langw = i;
            }
        }
    }
}

/**
 * Load actions list
 */
void project_loadactions()
{
    project_actions[0] = lang[CHARS_ANIM_IDLE];
    project_actions[1] = lang[CHARS_ANIM_WALK];
    project_actions[2] = lang[CHARS_ANIM_CLIMB];
    project_actions[3] = lang[CHARS_ANIM_JUMP];
    project_actions[4] = lang[CHARS_ANIM_RUN];
    project_actions[5] = lang[CHARS_ANIM_SWIM];
    project_actions[6] = lang[CHARS_ANIM_FLY];
    project_actions[7] = lang[CHARS_ANIM_BLOCK];
    project_actions[8] = lang[CHARS_ANIM_HURT];
    project_actions[9] = lang[CHARS_ANIM_DIE];
    project_actions[10] = actions_name[0][0] ? actions_name[0] : lang[CHARS_ANIM_A1];
    project_actions[11] = actions_name[1][0] ? actions_name[1] : lang[CHARS_ANIM_A2];
    project_actions[12] = actions_name[2][0] ? actions_name[2] : lang[CHARS_ANIM_A3];
    project_actions[13] = actions_name[3][0] ? actions_name[3] : lang[CHARS_ANIM_A4];
    project_actions[14] = actions_name[4][0] ? actions_name[4] : lang[CHARS_ANIM_A5];
    project_actions[15] = actions_name[5][0] ? actions_name[5] : lang[CHARS_ANIM_A6];
    project_actions[16] = actions_name[6][0] ? actions_name[6] : lang[CHARS_ANIM_A7];
    project_actions[17] = actions_name[7][0] ? actions_name[7] : lang[CHARS_ANIM_A8];
    project_actions[18] = actions_name[8][0] ? actions_name[8] : lang[CHARS_ANIM_A9];
}

/**
 * Load objects and mark object sprites which have meta data
 */
char **project_loadobjs(int *num)
{
    FILE *f;
    char **list = NULL, tmp[32];
    int i, j, k;

    for(i = 0; i < project.spritenum[1]; i++)
        project.sprites[1][i].y &= 0xFFFF;
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_OBJECTS]);
    list = project_getdir(projdir, ".obj", num);
    if(list) {
        for(j = 0; list[j]; j++) {
            for(i = 0; i < project.spritenum[1] && strcmp(list[j], project.sprites[1][i].name); i++);
            if(i < project.spritenum[1]) {
                sprintf(projfn, "%s" SEP "%s" SEP "%s.obj", project.id, project_dirs[PROJDIRS_OBJECTS], list[j]);
                f = project_fopen(projdir, "rb");
                if(f) {
                    memset(tmp, 0, 32);
                    if(fread(tmp, 1, 32, f) > 0 && !memcmp(tmp, PROJMAGIC_OBJECT, 15)) {
                        for(k = 15; k < 30 && (tmp[k] == '\r' || tmp[k] == '\n'); k++);
                        if(tmp[k] != '-' || (tmp[k + 1] != '\r' && tmp[k] != '\n')) project.sprites[1][i].y |= 0x10000;
                    }
                    fclose(f);
                }
            }
        }
    }
    return list;
}

/**
 * Save project configuration
 */
void project_save()
{
    FILE *f;

    if(!project.id[0]) return;
    strcpy(projfn, project.id);
    project_mkdir(projfn);
    strcat(projfn, SEP "config");
    if(verbose) printf("project_save: saving %s\n", projdir);
    f = project_fopen(projdir, "wb+");
    if(f) {
        fprintf(f, PROJMAGIC_CONFIG " %u\r\n%s\r\n%s %u %u %u\r\n", PROJ_FORMATREV, project.name,
            project.type == TNG_TYPE_HEXH ? "hexh" : (project.type == TNG_TYPE_HEXH ? "hexv" :
            (project.type == TNG_TYPE_ISO ? "iso" : (project.type == TNG_TYPE_3D ? "3d" : "ortho")) ),
            project.tilew, project.tileh, project.mapsize);
        fclose(f);
        if(renderer) ui_title(project.name);
    } else {
        if(verbose) printf("project_save: unable to save %s\n", projdir);
        ui_status(1, lang[ERR_SAVECFG]);
    }
}

/**
 * Free project
 */
void project_free()
{
    maps_free();
    spr_free();
    project_freedir(&project.languages, &project.numlang);
    project_freedir(&project.fonts, &project.numfont);
    project_freedir(&project.choosers, &project.numchoosers);
    project_freedir(&project.cuts, &project.numcuts);
    project_freedir(&project.attrs, &project.numattrs);
    project_freedir(&project.chars, &project.numchars);
    project_freedir(&project.dialogs, &project.numdialogs);
    project_freedir(&project.crafts, &project.numcrafts);
    project_freedir(&project.maps, &project.nummaps);
    project_freedir(&project.quests, &project.numquests);
    project_freedir(&project.music, &project.nummusic);
    project_freedir(&project.sounds, &project.numsounds);
    project_freedir(&project.speech, &project.numspeech);
    project_freedir(&project.movies, &project.nummovies);
    if(project.mapidx) free(project.mapidx);
    memset(&project, 0, sizeof(project_t));
    if(renderer) ui_title(NULL);
}

/**
 * Duplicate string into one which has attribute references parsed
 * {} = username
 * {attribute} = attribute value
 * {9attribute} = attribute value padded to the right
 */
char *project_parsestr(char *str)
{
    char *ret, *s, *d;
    int l;

    if(!str || !*str) return NULL;
    l = strlen(str);
    for(s = str; *s; s++)
        if(*s == '{') l += (s[1] == '}' ? 256 : 10);
    s = str;
    ret = d = (char*)main_alloc(l + 1);
    while(*s) {
        if(*s == '{') {
            s++;
            if(*s == '}') {
                l = strlen(lang[MAINMENU_MERLIN]);
                memcpy(d, lang[MAINMENU_MERLIN], l);
                d += l;
                s++;
                continue;
            }
            if(*s >= '1' && *s <= '9') {
                l = *s - '1';
                while(l--) *d++ = ' ';
                s++;
            }
            *d++ = '0';
            while(*s && *s != '}') s++;
            s++;
        } else
            *d++ = *s++;
    }
    return ret;
}

/**
 * Load a file
 */
char *project_loadfile(const char *dir, char *fn, char *ext, const char *magic, const char *func)
{
    FILE *f;
    char *data;
    long int len;
    int l;

    if(!dir || !*dir || !fn || !*fn || !ext || !*ext) return NULL;
    if(*dir == '.')
        sprintf(projfn, "%s" SEP "%s.%s", dir, fn, ext);
    else
        sprintf(projfn, "%s" SEP "%s" SEP "%s.%s", project.id, dir, fn, ext);
    if(func && verbose) printf("%s: loading %s\n", func, projdir);
    f = project_fopen(projdir, "rb");
    if(!f) return NULL;
    fseek(f, 0, SEEK_END);
    len = (long int)ftell(f);
    fseek(f, 0, SEEK_SET);
    data = (char*)main_alloc(len + 1);
    l = fread(data, len, 1, f);
    fclose(f);
    if(magic && *magic) {
        l = strlen(magic);
        if(memcmp(data, magic, l) || (data[l] != '\r' && data[l] != '\n')) { free(data); return NULL; }
    }
    return data;
}

/**
 * Save a file
 */
FILE *project_savefile(int confirm, const char *dir, char *fn, char *ext, const char *magic, const char *func)
{
    FILE *f;
    int i = 0;

    if(!dir || !*dir || !fn || !*fn || !ext || !*ext) return NULL;
    if(*dir == '.') {
        strcpy(projfn, dir);
        project_mkdir(projdir);
        sprintf(projfn, "%s" SEP "%s.%s", dir, fn, ext);
    } else {
        sprintf(projfn, "%s" SEP "%s", project.id, dir);
        project_mkdir(projdir);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.%s", project.id, dir, fn, ext);
    }
    if(confirm) {
        f = project_fopen(projdir, "rb");
        if(f) { i = 1; fclose(f); }
        if(i && !ui_modal(ICON_WARN, ERR_EXISTS, projfn)) return NULL;
    }
    if(func && verbose) printf("%s: saving %s\n", func, projdir);
    f = project_fopen(projdir, "wb");
    if(!f) return NULL;
    if(magic && *magic) fprintf(f, "%s\r\n", magic);
    return f;
}

/**
 * Skip to newline
 */
char *project_skipnl(char *str)
{
    if(str && *str) {
        while(*str && *str != '\n') str++;
        while(*str && (*str == '\r' || *str == '\n')) str++;
    }
    return str;
}

/**
 * Skip cnt arguments
 */
char *project_skiparg(char *str, int cnt)
{
    if(!str || !*str || cnt < 1) return str;
    while(*str == ' ') str++;
    if(!*str || *str == '\r' || *str == '\n') return project_skipnl(str);
    while(cnt--) {
        while(*str && *str != ' ' && *str != '\r' && *str != '\n') str++;
        while(*str == ' ') str++;
    }
    return str;
}

/**
 * Skip a script
 */
char *project_skipscript(char *str)
{
    int i = 1;

    if(!str || !*str) return str;
    while(*str && *str != '{' && *str != '\r' && *str != '\n') str++;
    if(*str == '{') {
        for(str++; *str && i > 0; str++) {
            if(*str == '{') i++;
            if(*str == '}') i--;
        }
        if(*str == '}') str++;
    }
    return str;
}

/**
 * Read an integer
 */
char *project_getint(char *str, int *ret, int min, int max)
{
    if(!str || !*str) return str;
    while(*str && *str == ' ') str++;
    if(ret) {
        *ret = atoi(str);
        if(*ret < min) *ret = min;
        if(*ret > max) *ret = max;
    }
    if(*str == '-') str++;
    while(*str && (*str == '.' || (*str >= '0' && *str <= '9'))) str++;
    return str;
}

/**
 * Return an array index
 */
char *project_getidx(char *str, int *ret, char **opts, int def)
{
    int i, l;

    *ret = def;
    if(!opts || !str || !*str) return str;
    while(*str && *str == ' ') str++;
    if(*str == '-') return str + 1;
    for(i = 0; opts[i]; i++) {
        l = strlen(opts[i]);
        if(!memcmp(str, opts[i], l) && (!str[l] || ISOP(str[l]) || str[l] == ',' || str[l] == ')' || str[l] == '\"' ||
           str[l] == ' ' || str[l] == '\r' || str[l] == '\n')) {
            *ret = i;
            return str + l;
        }
    }
    while(*str && *str != ' ' && !ISOP(*str) && *str != ',' && *str != ')' && *str != '\"' && *str != '\r' && *str != '\n')
        str++;
    if(*str == '\"' || *str == '/') str++;
    return str;
}

/**
 * Return an object sprite index, only for objects with meta data and name
 */
char *project_getobj(char *str, int *ret, int def)
{
    int i, l;

    if(!def) {
        for(i = 0; i < project.spritenum[1] && !(project.sprites[1][i].y & 0x10000); i++);
        *ret = i >= project.spritenum[1] ? -1 : i;
    } else
        *ret = def;
    if(!str || !*str) return str;
    while(*str && *str == ' ') str++;
    if(*str == '-') return str + 1;
    for(i = 0; i < project.spritenum[1]; i++) {
        if(!(project.sprites[1][i].y & 0x10000) || !project.sprites[1][i].name) continue;
        l = strlen(project.sprites[1][i].name);
        if(!memcmp(str, project.sprites[1][i].name, l) && (!str[l] || ISOP(str[l]) || str[l] == ',' || str[l] == ')' ||
          str[l] == '\"' || str[l] == ' ' || str[l] == '\r' || str[l] == '\n')) {
            *ret = i;
            return str + l;
        }
    }
    while(*str && *str != ' ' && !ISOP(*str) && *str != ',' && *str != ')' && *str != '\"' && *str != '\r' && *str != '\n')
        str++;
    if(*str == '\"' || *str == '/') str++;
    return str;
}

/**
 * Get a relation
 */
char *project_getrel(char *str, int *ret)
{
    int i, l;

    *ret = 0;
    if(!str || !*str) return str;
    while(*str && *str == ' ') str++;
    if(*str == '-') return str + 1;
    for(i = 0; project_rels[i]; i++) {
        l = strlen(project_rels[i]);
        if(!memcmp(str, project_rels[i], l)) {
            *ret = i;
            return str + l;
        }
    }
    while(*str && *str != ' ' && *str != '\r' && *str != '\n') str++;
    return str;
}

/**
 * Return a string
 * type 0 read until whitespace
 * type 1 read until space
 * type 2 read until newline
 * type 3 read valid id characters
 * type 4 read until quote, interpret some escape sequences
 * type 5 read until comma or closing parenthesis
 */
char *project_getstr(char *str, char **ret, int type, int maxlen)
{
    char *s, *d;
    int l;

    *ret = NULL;
    if(!str || !*str) return str;
    while(*str && (*str == ' ' || (type == 4 && *str != '\"' && *str != '\r' && *str != '\n'))) str++;
    if(!*str) return str;
    if(type == 4 && *str == '\"' && str[1] == '\"') return str + 2;
    if(*str == '-') return str + 1;
    s = str;
    switch(type & 0x7F) {
        case 5:
            for(l = 0; *s && *s != '\r' && *s != '\n'; s++) {
                if(!l && (*s == ',' || *s == ')')) break;
                if(*s == '(') l++;
                if(*s == ')') l--;
            }
        break;
        case 4:
            if(maxlen < 1) return s;
            *ret = d = (char*)main_alloc(maxlen + 1);
            if(*s == '\"') { s++; str++; }
            for(; *s && *s != '\"' && *s != '\r' && *s != '\n'; s++) {
                if(*s == '\\') {
                    s++;
                    if((int)((uintptr_t)s - (uintptr_t)str) < maxlen) {
                        switch(*s) {
                            case '\"': *d++ = '\"'; break;
                            case '\'': *d++ = '\''; break;
                            case 'n': *d++ = '\n'; break;
                        }
                    }
                } else
                if((int)((uintptr_t)s - (uintptr_t)str) < maxlen)
                    *d++ = *s;
            }
            if(*s == '\"') s++;
            *d = 0;
            return s;
        break;
        case 3:
            while(*s && ((*s >= 'a' && *s <= 'z') || (*s >= 'A' && *s <= 'Z') || (*s >= '0' && *s <= '9') || *s == '.' ||
                *s == '_')) s++;
        break;
        case 2: while(*s && *s != '\r' && *s != '\n') s++; break;
        case 1: while(*s && *s != ' ') s++; break;
        default: while(*s && *s != ' ' && *s != '\r' && *s != '\n') s++; break;
    }
    if(s > str) {
        l = s - str;
        if(maxlen > 0 && maxlen + 1 < l) l = maxlen - 1;
        *ret = (char*)main_alloc(l + 1);
        memcpy(*ret, str, l);
        (*ret)[l] = 0;
    }
    return s;
}
char *project_getstr2(char *str, char *ret, int type, int maxlen)
{
    char *s = NULL;

    str = project_getstr(str, &s, type, maxlen);
    *ret = 0;
    if(s) { strcpy(ret, s); free(s); }
    return str;
}

/**
 * Get string for translation
 */
char *project_gettrstr(char *str, int type, int maxlen)
{
    char *s = NULL;

    if(!str || !*str) return str;
    str = project_getstr(str, &s, type, maxlen);
    translate_addstr(s, maxlen);
    return str;
}

/**
 * Get font specification from file
 */
char *project_getfont(char *str, int *style, int *size, int *family)
{
    if(!str || !*str) return str;
    while(*str && *str == ' ') str++;
    *style = 0;
    if(*str != '.') *style |= SSFN_STYLE_BOLD;
    str++; if(!*str) return str;
    if(*str != '.') *style |= SSFN_STYLE_ITALIC;
    str++; if(!*str) return str;
    str = project_getint(str, size, 8, 192);
    str = project_getidx(str, family, project.fonts, -1);
    return str;
}

/**
 * Get a color from file
 */
char *project_getcolor(char *str, uint32_t *color)
{
    if(!str || !*str) return str;
    while(*str && *str == ' ') str++;
    *color = gethex(str, 8);
    while(*str && ((*str >= '0' && *str <= '9') || (*str >= 'a' && *str <= 'f') || (*str >= 'A' && *str <= 'F'))) str++;
    if(*str == ' ') str++;
    return str;
}

/**
 * Get a sprite from file
 */
char *project_getsprite(char *str, ui_sprsel_t *spr)
{
    int i, l;

    if(!str || !*str) return str;
    while(*str && *str == ' ') str++;
    spr->val = -1;
    if(*str == '-') { return str + 1; }
    for(i = 0; i < project.spritenum[spr->cat]; i++) {
        if(!project.sprites[spr->cat][i].name) continue;
        l = strlen(project.sprites[spr->cat][i].name);
        if(!memcmp(str, project.sprites[spr->cat][i].name, l) && (!str[l] || str[l] == '\"' || str[l] == ',' || str[l] == ')' ||
          str[l] == ' ' || str[l] == '\r' || str[l] == '\n')) {
            spr->val = i;
            return str + l;
        }
    }
    while(*str && *str != ' ' && *str != ',' && *str != ')' && *str != '\"' && *str != '\r' && *str != '\n') str++;
    if(*str == '\"') str++;
    return str;
}

/**
 * Get a script from file
 */
char *project_getscript(char *str, ui_cmd_t *cmd)
{
    if(!str || !*str) return str;
    while(*str && *str != '\r' && *str != '\n' && *str != '{') str++;
    ui_cmd_free(cmd);
    if(*str == '{' && str[1] != '}') {
        str = ui_cmd_deserialize(str, &cmd->root, &cmd->err);
        ui_cmd_resize(&cmd->root);
    } else
        str += 2;
    return str;
}

/**
 * Write an index to file
 */
void project_wridx(FILE *f, int idx, char **opts)
{
    fprintf(f, "%s", !opts || idx < 0 ? "-" : opts[idx]);
}

/**
 * Write a relation to file
 */
void project_wrrel(FILE *f, int idx)
{
    fprintf(f, "%s", idx < 0 ? "-" : project_rels[idx]);
}

/**
 * Write font specification to file
 */
void project_wrfont(FILE *f, int style, int size, int family)
{
    fprintf(f, "%c%c %d %s", style & SSFN_STYLE_BOLD ? 'B' : '.', style & SSFN_STYLE_ITALIC ? 'I' : '.', size,
        family < 0 || family >= project.numfont ? "-" : project.fonts[family]);
}

/**
 * Write a sprite to file
 */
void project_wrsprite(FILE *f, ui_sprsel_t *spr)
{
    fprintf(f, "%s", !spr || spr->val < 0 || spr->val >= project.spritenum[spr->cat] ? "-" :
        project.sprites[spr->cat][spr->val].name);
}
void project_wrsprite2(FILE *f, int val, int cat)
{
    fprintf(f, "%s", val < 0 || val >= project.spritenum[cat] ? "-" : project.sprites[cat][val].name);
}

/**
 * Write a script to file
 */
void project_wrscript(FILE *f, ui_cmd_t *cmd)
{
    if(cmd && cmd->root.len > 0) {
        cmd->err = 0;
        ui_cmd_serialize(f, &cmd->root, 0, &cmd->err);
    } else
        fprintf(f, "{}\r\n");
}

/**
 * Escape string
 */
void project_escapestr(char *s, char *d)
{
    while(*s == ' ') s++;
    while(*s) {
        if(*s >= 0 && *s < ' ') { *d++ = ' '; s++; continue; }
        if(*s == '\"' || *s == '\'' || *s == '\\') *d++ = '\\';
        *d++ = *s++;
    }
    *d = 0;
}

/**
 * Write a translation to file
 */
void project_wrmsg(FILE *f, char *id, char *str, int len)
{
    char *d = NULL;

    if(!id || !*id || !str || len < 1) return;
    fprintf(f, "\r\n");
    d = (char*)main_alloc(2 * len + 1);
    project_escapestr(id, d);
    fprintf(f, "msgid \"%s\"\r\n", d);
    project_escapestr(str, d);
    fprintf(f, "msgstr \"%s\"\r\n", d);
    free(d);
}

/**
 * Write escaped string to file
 */
void project_wrstr(FILE *f, char *str)
{
    char *d = NULL;

    if(!str) return;
    d = (char*)main_alloc(2 * strlen(str) + 1);
    project_escapestr(str, d);
    fprintf(f, "\"%s\"", d);
    free(d);
}

/**
 * Call ffmpeg executable to convert video and parse its output
 */
int project_ffmpeg(char *from, char *to)
{
/*
 * I've tried to integrate libavcodec, but that's the worst documented piece of software in history, with non-functional and
 * non-working transcoding.c example. Ffmpeg is written by complete morons who think it is fun to change the API every month
 * (I'm not kidding, even their very own latest tools are throwing several pages of deprecated warnings during compilation).
 * There's no way I will integrate such a jerk and unstable API, so even though it hurts, calling the ffmpeg executable it is...
 * At least the command line options are stable, hopefully the stdout is somewhat too (rely on it as little as possible).
 */
    int ret = 0;
    int64_t dur = 0, cur = 0;
    char buf[1024], c, *s, fq[16], vq[16], aq[16];
#ifdef __WIN32__
    char *cmdline;
    DWORD len;
    wchar_t *szCmdline;
    PROCESS_INFORMATION processInfo;
    STARTUPINFOW startupInfo;
    SECURITY_ATTRIBUTES saAttr;
    HANDLE stdoutReadHandle = NULL;
    HANDLE stdoutWriteHandle = NULL;
#else
    int fd[2];
#endif

    project_rm(to);
    if(!media_ffmpeg) goto end;
    sprintf(fq, "%d", FREQ);
    sprintf(vq, "%d", VIDEO_QUALITY);
    sprintf(aq, "%d", AUDIO_QUALITY);
#ifdef __WIN32__
    cmdline = (char*)main_alloc(256 + strlen(from) + strlen(to));
    /* ffmpeg.exe maybe not in path. In that case ffmpegexe variable holds the full path */
    s = cmdline + sprintf(cmdline, "\"%s\" -y -i \"", ffmpegexe[0] ? ffmpegexe : "ffmpeg.exe");
    project_escapestr(from, s);
    sprintf(s + strlen(s), "\" -q:v %s -r 30 -q:a %s -ar %s -ac 2 \"", vq, aq, fq);
    s += strlen(s);
    project_escapestr(to, s);
    strcat(s, "\"");
    len = (DWORD)strlen(cmdline);
    szCmdline = (wchar_t*)main_alloc((len + 1) * sizeof(wchar_t));
    MultiByteToWideChar(CP_UTF8, 0, cmdline, -1, szCmdline, len);
    memset(&saAttr, 0, sizeof(saAttr));
    saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
    saAttr.bInheritHandle = TRUE;
    saAttr.lpSecurityDescriptor = NULL;
    if(!CreatePipe(&stdoutReadHandle, &stdoutWriteHandle, &saAttr, 5000) ||
      !SetHandleInformation(stdoutReadHandle, HANDLE_FLAG_INHERIT, 0)) {
        if(verbose) printf("project_ffmpeg: unable to create pipe\n");
        goto end;
    }
    memset(&startupInfo, 0, sizeof(startupInfo));
    startupInfo.cb = sizeof(startupInfo);
    startupInfo.hStdError = stdoutWriteHandle;
    startupInfo.hStdOutput = stdoutWriteHandle;
    startupInfo.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
    startupInfo.dwFlags |= STARTF_USESTDHANDLES;
    if(!CreateProcessW(NULL, szCmdline, NULL, NULL, TRUE, CREATE_NO_WINDOW | CREATE_UNICODE_ENVIRONMENT, NULL, 0,
      &startupInfo, &processInfo)) {
        CloseHandle(stdoutReadHandle);
        CloseHandle(stdoutWriteHandle);
        if(verbose) printf("project_ffmpeg: unable to create child process\n");
        goto end;
    }
    CloseHandle(stdoutWriteHandle);
    s = buf;
    while(ReadFile(stdoutReadHandle, &c, 1, &len, NULL) && len == 1) {
#else
    if(pipe(fd) == -1) {
        if(verbose) printf("project_ffmpeg: unable to create pipe\n");
        goto end;
    }
    switch(fork()) {
        case -1:
            close(fd[0]);
            close(fd[1]);
            if(verbose) printf("project_ffmpeg: unable to fork\n");
            goto end;
        break;
        case 0:
            /* DO NOT close stdin because ffmpeg will crash */
            dup2(fd[1], 1);
            dup2(fd[1], 2);
            close(fd[0]);
            execlp("ffmpeg", "ffmpeg", "-y", "-i", from, "-q:v", vq, "-r", "30", "-q:a", aq, "-ar", fq, "-ac", "2", to, NULL);
        break;
    }
    close(fd[1]);
    s = buf;
    while(read(fd[0], &c, 1)) {
#endif
        if(c == '\r' || c == '\n') {
            *s = 0;
            if(!dur) {
                for(s = buf; *s == ' '; s++);
                if(!memcmp(s, "Duration: ", 10))
                    dur = atoi(s + 10) * 360000 + atoi(s + 13) * 6000 + atoi(s + 16) * 100 + atoi(s + 19);
            }
            if(!memcmp(buf, "frame=", 6)) {
                for(s = buf + 6; *s && memcmp(s, "time=", 5); s++);
                if(*s) {
                    if(verbose) { printf("%s\r", buf); fflush(stdout); }
                    cur = atoi(s + 5) * 360000 + atoi(s + 8) * 6000 + atoi(s + 11) * 100 + atoi(s + 14);
                    ui_progressbar(0, 0, cur, dur, MEDIA_IMPORT);
                }
            }
            s = buf;
        } else
            *s++ = c;
    }
    if(verbose) printf("\n");
    if(memcmp(buf, "Conversion failed", 17)) {
        ui_progressbar(0, 0, 0, 0, MEDIA_IMPORT);
        ret = 1;
    }
#ifdef __WIN32__
    WaitForSingleObject(processInfo.hProcess, INFINITE);
    CloseHandle(processInfo.hProcess);
    CloseHandle(processInfo.hThread);
    CloseHandle(stdoutReadHandle);
    free(szCmdline);
    free(cmdline);
#else
    close(fd[0]);
#endif
end:
    if(!ret) ui_status(1, lang[ERR_SAVEMOV]);
    return ret;
}

/**
 * Open user's manual
 */
void project_help()
{
    char url[256], fn[16];

    /* first look for local file */
    sprintf(fn, "manual_%c%c.html", lang[-1][0], lang[-1][1]);
    sprintf(url, "file://" DOCDIR SEP "%s", fn);
    if(verbose) printf("project_help: looking for %s\n", url);
    /* use online manual if not found */
#ifdef __WIN32__
    MultiByteToWideChar(CP_UTF8, 0, url + 7, -1, szFull, PATH_MAX);
    if(_wstat64(szFull, &projst))
#else
    if(stat(url + 7, &projst))
#endif
        sprintf(url, "%s/%s", site_url, fn);
    sprintf(url + strlen(url), "#tab%u", ui_tab);
    main_open_url(url);
    /* starting browser asynchronuously, let the user notice something is happening */
    SDL_SetCursor(cursors[CURSOR_LOADING]);
    SDL_Delay(250);
    SDL_SetCursor(cursors[CURSOR_PTR]);
}

/**
 * Play the game
 */
void project_play()
{
#ifdef __WIN32__
    wchar_t szTNGP[256];
    MultiByteToWideChar(CP_UTF8, 0, TNGP, -1, szTNGP, 256);
#endif
    if(!project.id[0] ||
#ifdef __WIN32__
      _wstat64(szTNGP, &projst)
#else
      stat(TNGP, &projst)
#endif
      ) {
        ui_status(1, lang[ERR_PLAYER]);
        return;
    }
    strcpy(projfn, project.id);
    strcat(projfn, SEP "game.tng");
    if(!project_exists(projdir) || !project.hasGame)
        project_togame(0);
    if(verbose) printf("project_play: running %s\n", projdir);
#ifdef __WIN32__
    MultiByteToWideChar(CP_UTF8, 0, projdir, -1, szFull, PATH_MAX);
    ShellExecuteW(0, L"open", szTNGP, szFull, 0, SW_SHOW);
#else
    if(!fork()) {
        fclose(stdin);
        fclose(stdout);
        fclose(stderr);
        execl(TNGP, "tngp", projdir, NULL);
        /* should never reached */
        exit(1);
    }
#endif
}

/**
 * Return true for valid directory names
 */
int project_isvaliddir(char *s)
{
    int i, l;
    for(i = 0; project_dirs[i]; i++) {
        l = strlen(project_dirs[i]);
        if(!memcmp(s, project_dirs[i], l) && s[l] == '/') return 1;
    }
    return 0;
}

/**
 * Delete a project
 */
void project_delete(char *id)
{
    if(!strcmp(project.id, id))
        project_free();
    strcpy(projfn, id);
    if(verbose) printf("project_delete: removing %s\n", projdir);
    project_rmdir(projdir, 0);
}

/**
 * Get mask base
 */
void project_encbase(unsigned char *out)
{
    memset(out, 0, 4);
#ifdef _TNGE_PRO_
    pro_encbase(out);
#endif
}

/**
 * Binary to text
 */
char *project_encode(unsigned char *s)
{
    char *o = malloc(16);

    if(!o) return NULL;
    memset(o, 0, 16);
#ifdef _TNGE_PRO_
    pro_encode(s, o);
#else
    (void)s;
#endif
    return o;
}

/**
 * Text to binary
 */
void project_decode(char *s, unsigned char *o)
{
    memset(o, 0, 8);
#ifdef _TNGE_PRO_
    pro_decode(s, o);
#else
    (void)s;
#endif
}

/**
 * Create project from template
 */
int project_fromtemplate(char *id, char *zip)
{
    FILE *f, *o;
    zip_local_t loc;
    z_stream zstrm;
    ZSTD_DCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;
    uint8_t data[65536], buf[65536], *e;
    uint64_t size = 0, len, l = 0, total = 0, cur = 0;
    int fnlen = 0, skip = 0, ret, cl = strlen(COMMONDIR) + 1, tl = strlen(CREDITSFN) + 1, newproj;
    char *od, *fn, *s, *d;

    if(verbose) printf("project_fromtemplate: loading template %s\n", zip);
    f = project_fopen(zip, "rb");
    newproj = !project_exists(id);
    if(newproj) project_mkdir(id);
    od = projfn + strlen(id);
    *od++ = SEP[0]; *od = 0;
    if(f) {
        while(!feof(f)) {
            if(!fread((uint8_t*)&loc, sizeof(zip_local_t), 1, f) || loc.magic != ZIP_LOCAL_MAGIC) break;
            if(loc.fnlen > fnlen) fnlen = loc.fnlen;
            fseek(f, loc.fnlen, SEEK_CUR);
            size = loc.comp;
            if(loc.extlen) {
                memset(data, 0, sizeof(data));
                if(fread(data, loc.extlen, 1, f))
                    for(e = data; e - data < loc.extlen && *((uint16_t*)(e + 2)) > 0; e += *((uint16_t*)(e + 2)) + 4)
                        if(*((uint16_t*)e) == 1) { size = ((zip_ext64_t*)e)->comp; break; }
            }
            fseek(f, size, SEEK_CUR);
            total += loc.fnlen + size;
        }
        fseek(f, 0, SEEK_SET);
        fn = (char*)malloc(fnlen + FILENAME_MAX);
        if(!fn) { fclose(f); ui_status(1, lang[ERR_MEM]); return 0; }
        while(!feof(f)) {
            if(!fread((uint8_t*)&loc, sizeof(zip_local_t), 1, f) || loc.magic != ZIP_LOCAL_MAGIC ||
              !fread(fn, loc.fnlen, 1, f)) break;
            fn[loc.fnlen] = 0;
            size = loc.comp;
            if(loc.extlen) {
                memset(data, 0, sizeof(data));
                if(fread(data, loc.extlen, 1, f))
                    for(e = data; e - data < loc.extlen && *((uint16_t*)(e + 2)) > 0; e += *((uint16_t*)(e + 2)) + 4)
                        if(*((uint16_t*)e) == 1) { size = ((zip_ext64_t*)e)->comp; break; }
            }
            if(!cur && fn[loc.fnlen - 1] == '/' && memcmp(fn, COMMONDIR "/", cl) && !project_isvaliddir(fn)) {
                if(verbose) printf("project_fromtemplate: skipping directory part '%s'\n", fn);
                fseek(f, size, SEEK_CUR); skip = loc.fnlen; cur += loc.fnlen + size; continue;
            }
            cur += loc.fnlen;
            s = fn + skip;
            if((!newproj || memcmp(s, "config", 7)) && memcmp(s, CREDITSFN, tl) && memcmp(s, COMMONDIR "/", cl) &&
              !project_isvaliddir(s)) {
                if(verbose) printf("project_fromtemplate: bad file in template %s\n", fn);
                fseek(f, size, SEEK_CUR);
            } else {
                l = !newproj && !memcmp(s, CREDITSFN, tl);
                d = od;
                if(!memcmp(s, COMMONDIR "/", cl)) { d = projfn; *d++ = '.'; } else { strcpy(projfn, id); d[-1] = SEP[0]; }
                while(*s) {
#ifdef __WIN32__
                    *d++ = (*s == '/' ? '\\' : *s);
#else
                    *d++ = (*s == '\\' ? '/' : *s);
#endif
                    s++;
                }
                *d = 0;
                if(fn[loc.fnlen - 1] == '/') {
                    d[-1] = 0;
                    if(verbose) printf("project_fromtemplate: mkdir %s\n", projdir);
                    project_mkdir(projdir);
                    fseek(f, size, SEEK_CUR);
                } else {
                    o = project_fopen(projdir, l ? "ab+" : "wb+");
                    if(o) {
                        if(l) fwrite("\r\n", 2, 1, o);
                        if(verbose) printf("project_fromtemplate: %s %s (%ld bytes)\n", l ? "append" : "write", projdir,
                            (long int)size);
                        l = 0;
                        switch(loc.method) {
                            case ZIP_DATA_STORE:
                                for(len = size; len > 0; len -= l) {
                                    l = len > sizeof(data) ? sizeof(data) : len;
                                    l = fread(data, 1, l, f);
                                    if(l < 1) break;
                                    fwrite(data, l, 1, o);
                                }
                            break;
                            case ZIP_DATA_DEFLATE:
                                memset(&zstrm, 0, sizeof(zstrm));
                                if((inflateInit2(&zstrm, -MAX_WBITS)) != Z_OK) {
                                    fclose(o); fclose(f); ui_status(1, lang[ERR_MEM]); return 0;
                                }
                                len = size;
                                do {
                                    zstrm.avail_in = len > sizeof(data) ? sizeof(data) : len; len -= zstrm.avail_in;
                                    if(!fread(data, zstrm.avail_in, 1, f)) break;
                                    zstrm.next_in = data;
                                    do {
                                        zstrm.avail_out = sizeof(buf);
                                        zstrm.next_out = buf;
                                        ret = inflate(&zstrm, Z_NO_FLUSH);
                                        if(ret == Z_STREAM_ERROR || ret == Z_NEED_DICT || ret == Z_DATA_ERROR ||
                                            ret == Z_MEM_ERROR || !fwrite(buf, sizeof(buf) - zstrm.avail_out, 1, o)) {
                                            if(verbose) printf("project_fromtemplate: uncompress error %d\n", ret);
                                            inflateEnd(&zstrm); fclose(f); fclose(o); ui_status(1, lang[ERR_UNCOMP]); return 0;
                                        }
                                    } while(zstrm.avail_out == 0);
                                    ui_progressbar(0, 0, cur + size - len, total, LOADPROJ_LOADTEMPL);
                                } while(ret != Z_STREAM_END || len < 1);
                                inflateEnd(&zstrm);
                            break;
                            case ZIP_DATA_ZSTD:
                                if(!(zcmp = ZSTD_createDCtx())) {
                                    fclose(o); fclose(f); ui_status(1, lang[ERR_MEM]); return 0;
                                }
                                len = size;
                                do {
                                    zi.size = len > sizeof(data) ? sizeof(data) : len; len -= zi.size;
                                    if(!fread(data, zi.size, 1, f)) break;
                                    zi.src = data;
                                    zi.pos = zo.pos = 0;
                                    do {
                                        zo.size = sizeof(buf);
                                        zo.dst = buf;
                                        ret = (int)ZSTD_decompressStream(zcmp, &zo, &zi);
                                        if(ZSTD_isError(ret) || !fwrite(buf, zo.size, 1, o)) {
                                            if(verbose) printf("project_fromtemplate: zstd uncompress error %d\n", ret);
                                            ZSTD_freeDCtx(zcmp); fclose(f); fclose(o); ui_status(1, lang[ERR_UNCOMP]); return 0;
                                        }
                                    } while(zo.pos < zo.size);
                                    ui_progressbar(0, 0, cur + size - len, total, LOADPROJ_LOADTEMPL);
                                } while(ret > 0 || len < 1);
                                ZSTD_freeDCtx(zcmp);
                                break;
                        }
                        fclose(o);
                    } else {
                        if(verbose) printf("project_fromtemplate: unable to write %s (%ld bytes)\n", projdir, (long int)size);
                        fseek(f, size, SEEK_CUR);
                    }
                }
            }
            ui_progressbar(0, 0, cur, total, LOADPROJ_LOADTEMPL);
            cur += size;
        }
        free(fn);
        fclose(f);
        ui_progressbar(0, 0, 0, 0, 0);
        return 1;
    } else
        ui_status(1, lang[ERR_TEMPL]);
    return 0;
}

static zip_central_t *zip;
static zip_local_t loc;
static zip_eocd32_t eoc;
static uint32_t total;
#define CMPBUFSIZ 1024*1024
/**
 * Add a file to template
 */
uint32_t project_addfile(FILE *f, char *name, uint32_t curr)
{
    FILE *g;
    int i, flush;
    z_stream strm;
    uint32_t have;
    uint8_t *in, *out;

    in = main_alloc(CMPBUFSIZ);
    out = main_alloc(CMPBUFSIZ);

    memset(&strm, 0, sizeof(strm));
    g = project_fopen(projdir, "rb");
    if(g) {
        zip[eoc.nument].offs = (uint32_t)ftell(f);
        loc.orig = zip[eoc.nument].orig = (uint32_t)projst.st_size;
        loc.comp = zip[eoc.nument].comp = loc.crc32 = 0;
        zip[eoc.nument].eattr = 0;
        zip[eoc.nument].magic = ZIP_CENTRAL_MAGIC;
        zip[eoc.nument].madeby = zip[0].madeby;
        zip[eoc.nument].version = zip[0].version;
        zip[eoc.nument].mtime = zip[0].mtime;
        zip[eoc.nument].fnlen = loc.fnlen = strlen(name);
        zip[eoc.nument].fn = main_alloc(loc.fnlen + 1);
        for(i = 0; name[i]; i++)
            zip[eoc.nument].fn[i] = name[i] == '\\' ? '/' : name[i];
        zip[eoc.nument].fn[i] = 0;
        fwrite(&loc, sizeof(zip_local_t), 1, f);
        fwrite(zip[eoc.nument].fn, zip[eoc.nument].fnlen, 1, f);
        deflateInit2(&strm, 9, Z_DEFLATED, -MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY);
        do {
            strm.avail_in = fread(in, 1, CMPBUFSIZ, g);
            strm.next_in = in;
            loc.crc32 = crc32(loc.crc32, in, strm.avail_in);
            curr += strm.avail_in;
            ui_progressbar(0, 0, curr, total, LOADPROJ_SAVETEMPL);
            flush = feof(g) ? Z_FINISH : Z_NO_FLUSH;
            do {
                strm.avail_out = CMPBUFSIZ;
                strm.next_out = out;
                deflate(&strm, flush);
                have = CMPBUFSIZ - strm.avail_out;
                if(loc.orig < CMPBUFSIZ && loc.orig < have) {
                    loc.comp = loc.orig;
                    fwrite(in, loc.orig, 1, f);
                    flush = Z_FINISH;
                    break;
                }
                fwrite(out, have, 1, f);
                loc.comp += have;
                loc.method = ZIP_DATA_DEFLATE;
            } while(strm.avail_out == 0);
        } while (flush != Z_FINISH);
        fclose(g);
        deflateEnd(&strm);
        zip[eoc.nument].method = loc.method;
        zip[eoc.nument].comp = loc.comp;
        zip[eoc.nument].crc32 = loc.crc32;
        project_fseek(f, zip[eoc.nument].offs);
        fwrite(&loc, sizeof(zip_local_t), 1, f);
        fseek(f, 0, SEEK_END);
        eoc.nument++;
    }
    free(in);
    free(out);
    loc.method = ZIP_DATA_STORE;
    loc.crc32 = 0;
    loc.comp = loc.orig = 0;
    return curr;
}

/**
 * Create template from project
 */
void project_totemplate(int menu)
{
    FILE *f;
    char *path, *fn, *fn2, **lytlist, **gpllist, **sprlist, tmp[1024];
    int i = 1, l;
    time_t t;
    struct tm *tm;
    uint32_t mtime = 0, curr = 0;
#ifdef __WIN32__
    _WDIR *dir, *dir2;
    struct _wdirent *ent, *ent2;
#else
    DIR *dir, *dir2;
    struct dirent *ent, *ent2;
#endif

    (void)menu;
    if(!project.id[0]) return;
    l = strlen(project.id);
    path = projfn + l;
    strcpy(projfn, project.id);
    strcat(projfn, ".zip");
    if(verbose) printf("project_totemplate: creating template %s\n", projdir);
    f = project_fopen(projdir, "rb");
    if(f) {
        fclose(f);
        i = ui_modal(ICON_WARN, ERR_EXISTS, projfn);
    }
    if(!i) return;
    f = project_fopen(projdir, "wb+");
    if(!f) { ui_status(1, lang[ERR_WRTEMPL]); return; }
    sprintf(full, "%s %s", projdir, lang[LANG_SAVED]);
    SDL_SetCursor(cursors[CURSOR_LOADING]);
    ui_progressbar(0, 0, 0, 100, LOADPROJ_SAVETEMPL);
    strcpy(projfn, "." COMMONDIR);
    curr = 5;
    lytlist = project_getdir(projdir, ".lyt", &i); curr += i;
    gpllist = project_getdir(projdir, ".gpl", &i); curr += i;
    sprlist = project_getdir(projdir, ".png", &i); curr += i;
    strcpy(projfn, project.id);
    *path++ = SEP[0];
    total = 0;
    /* count total size */
    for(i = 0; project_dirs[i]; i++) {
        strcpy(path, project_dirs[i]);
        fn = path + strlen(project_dirs[i]);
#ifdef __WIN32__
        MultiByteToWideChar(CP_UTF8, 0, projdir, -1, szFull, PATH_MAX);
        if((dir = _wopendir(szFull)) != NULL) {
#else
        if((dir = opendir(projdir)) != NULL) {
#endif
            curr++;
            *fn++ = SEP[0];
#ifdef __WIN32__
            while((ent = _wreaddir(dir)) != NULL) {
                if(ent->d_name[0] == L'.') continue;
                wdname[WideCharToMultiByte(CP_UTF8, 0, ent->d_name, wcslen(ent->d_name), wdname, FILENAME_MAX, NULL, NULL)] = 0;
#else
            while((ent = readdir(dir)) != NULL) {
                if(ent->d_name[0] == '.') continue;
#endif
                strcpy(fn, dname);
                if(!project_exists(projdir)) continue;
                if(S_ISREG(projst.st_mode)) {
                    curr++;
                    total += (uint32_t)projst.st_size;
                } else
                if(S_ISDIR(projst.st_mode)) {
                    fn2 = fn + strlen(fn);
                    dir2 = NULL;
#ifdef __WIN32__
                    MultiByteToWideChar(CP_UTF8, 0, projdir, -1, szFull, PATH_MAX);
                    if((dir2 = _wopendir(szFull)) != NULL) {
#else
                    if((dir2 = opendir(projdir)) != NULL) {
#endif
                        curr++;
                        *fn2++ = SEP[0];
#ifdef __WIN32__
                        while ((ent = _wreaddir(dir2)) != NULL) {
                            if(ent->d_name[0] == L'.') continue;
                            wdname[WideCharToMultiByte(CP_UTF8, 0, ent->d_name, wcslen(ent->d_name), wdname, FILENAME_MAX,
                                NULL, NULL)] = 0;
#else
                        while ((ent = readdir(dir2)) != NULL) {
                            if(ent->d_name[0] == '.') continue;
#endif
                            strcpy(fn2, dname);
                            if(!project_exists(projdir)) continue;
                            if(S_ISREG(projst.st_mode)) {
                                curr++;
                                total += (uint32_t)projst.st_size;
                            }
                        }
#ifdef __WIN32__
                        _wclosedir(dir2);
#else
                        closedir(dir2);
#endif
                    }
                }
            }
#ifdef __WIN32__
            _wclosedir(dir);
#else
            closedir(dir);
#endif
        }
    }
    if(verbose > 1) printf("project_totemplate: compressing %u bytes in total\n", total);
    time(&t);
    tm = gmtime(&t);
    if(tm) {
        mtime = (((tm->tm_year - 80) & 0x7f) << 25) | ((tm->tm_mon + 1) << 21) |
            (tm->tm_mday << 16) | (tm->tm_hour << 11) | (tm->tm_min << 5) | (tm->tm_sec >> 1);
    }
    zip = (zip_central_t*)main_alloc(curr * sizeof(zip_central_t));
    memset(&eoc, 0, sizeof(eoc));
    eoc.magic = ZIP_EOCD32_MAGIC;
    eoc.totdisk = 1;
    memset(&loc, 0, sizeof(zip_local_t));
    memset(zip, 0, curr * sizeof(zip_central_t));
    curr = 0;
    /* write main directory */
    loc.magic = ZIP_LOCAL_MAGIC;
    zip[0].magic = ZIP_CENTRAL_MAGIC;
    zip[0].madeby = 0x31e;
    loc.version = zip[0].version = 10;
    loc.mtime = zip[0].mtime = mtime;
    loc.fnlen = zip[0].fnlen = l + 1;
    zip[0].fn = main_alloc(loc.fnlen + 1);
    strcpy(zip[0].fn, project.id);
    strcat(zip[0].fn, "/");
    fwrite(&loc, sizeof(zip_local_t), 1, f);
    fwrite(zip[0].fn, zip[0].fnlen, 1, f);
    zip[0].eattr = 0x10;
    eoc.nument++;
    /* add config file */
    strcpy(path, "config");
    if(project_exists(projdir))
        curr = project_addfile(f, projfn, curr);
    /* add credits file if exists */
    strcpy(path, CREDITSFN);
    if(project_exists(projdir))
        curr = project_addfile(f, projfn, curr);
    /* add directories */
    for(i = 0; project_dirs[i]; i++) {
        strcpy(path, project_dirs[i]);
        fn = path + strlen(project_dirs[i]);
#ifdef __WIN32__
        MultiByteToWideChar(CP_UTF8, 0, projdir, -1, szFull, PATH_MAX);
        if((dir = _wopendir(szFull)) != NULL) {
#else
        if((dir = opendir(projdir)) != NULL) {
#endif
            zip[eoc.nument].magic = ZIP_CENTRAL_MAGIC;
            zip[eoc.nument].madeby = zip[0].madeby;
            zip[eoc.nument].version = zip[0].version;
            zip[eoc.nument].mtime = mtime;
            zip[eoc.nument].eattr = 0x10;
            zip[eoc.nument].offs = (uint32_t)ftell(f);
            loc.fnlen = zip[eoc.nument].fnlen = l + strlen(project_dirs[i]) + 2;
            zip[eoc.nument].fn = main_alloc(loc.fnlen + 1);
            strcpy(zip[eoc.nument].fn, project.id);
            strcat(zip[eoc.nument].fn, "/");
            strcat(zip[eoc.nument].fn, project_dirs[i]);
            strcat(zip[eoc.nument].fn, "/");
            fwrite(&loc, sizeof(zip_local_t), 1, f);
            fwrite(zip[eoc.nument].fn, zip[eoc.nument].fnlen, 1, f);
            eoc.nument++;
            *fn++ = SEP[0];
#ifdef __WIN32__
            while((ent = _wreaddir(dir)) != NULL) {
                if(ent->d_name[0] == L'.') continue;
                wdname[WideCharToMultiByte(CP_UTF8, 0, ent->d_name, wcslen(ent->d_name), wdname, FILENAME_MAX, NULL, NULL)] = 0;
#else
            while((ent = readdir(dir)) != NULL) {
                if(ent->d_name[0] == '.') continue;
#endif
                strcpy(fn, dname);
                if(!project_exists(projdir)) continue;
                if(S_ISREG(projst.st_mode))
                    curr = project_addfile(f, projfn, curr);
                else
                if(S_ISDIR(projst.st_mode)) {
                    fn2 = fn + strlen(fn);
                    dir2 = NULL;
#ifdef __WIN32__
                    MultiByteToWideChar(CP_UTF8, 0, projdir, -1, szFull, PATH_MAX);
                    if((dir2 = _wopendir(szFull)) != NULL) {
#else
                    if((dir2 = opendir(projdir)) != NULL) {
#endif
                        zip[eoc.nument].magic = ZIP_CENTRAL_MAGIC;
                        zip[eoc.nument].madeby = zip[0].madeby;
                        zip[eoc.nument].version = zip[0].version;
                        zip[eoc.nument].mtime = mtime;
                        zip[eoc.nument].eattr = 0x10;
                        zip[eoc.nument].offs = (uint32_t)ftell(f);
                        loc.fnlen = zip[eoc.nument].fnlen = strlen(projfn);
                        zip[eoc.nument].fn = main_alloc(loc.fnlen + 1);
                        strcpy(zip[eoc.nument].fn, project.id);
                        strcat(zip[eoc.nument].fn, "/");
                        strcat(zip[eoc.nument].fn, project_dirs[i]);
                        strcat(zip[eoc.nument].fn, "/");
                        strcat(zip[eoc.nument].fn, dname);
                        strcat(zip[eoc.nument].fn, "/");
                        fwrite(&loc, sizeof(zip_local_t), 1, f);
                        fwrite(zip[eoc.nument].fn, zip[eoc.nument].fnlen, 1, f);
                        eoc.nument++;
                        *fn2++ = SEP[0];
#ifdef __WIN32__
                        while ((ent2 = _wreaddir(dir2)) != NULL) {
                            if(ent2->d_name[0] == L'.') continue;
                            wdname[WideCharToMultiByte(CP_UTF8, 0, ent2->d_name, wcslen(ent2->d_name), wdname, FILENAME_MAX,
                                NULL, NULL)] = 0;
                            strcpy(fn2, wdname);
#else
                        while ((ent2 = readdir(dir2)) != NULL) {
                            if(ent2->d_name[0] == '.') continue;
                            strcpy(fn2, ent2->d_name);
#endif
                            if(!project_exists(projdir)) continue;
                            if(S_ISREG(projst.st_mode))
                                curr = project_addfile(f, projfn, curr);
                        }
#ifdef __WIN32__
                        _wclosedir(dir2);
#else
                        closedir(dir2);
#endif
                    }
                }
            }
#ifdef __WIN32__
            _wclosedir(dir);
#else
            closedir(dir);
#endif
        }
    }
    if(verbose > 1) printf("project_totemplate: adding common assets\n");
    /* add common palettes, layouts and sprite generator templates */
    if(lytlist || gpllist || sprlist) {
        zip[eoc.nument].magic = ZIP_CENTRAL_MAGIC;
        zip[eoc.nument].madeby = zip[0].madeby;
        zip[eoc.nument].version = zip[0].version;
        zip[eoc.nument].mtime = mtime;
        zip[eoc.nument].eattr = 0x10;
        zip[eoc.nument].offs = (uint32_t)ftell(f);
        loc.fnlen = zip[eoc.nument].fnlen = l + strlen(COMMONDIR) + 2;
        zip[eoc.nument].fn = main_alloc(loc.fnlen + 1);
        strcpy(zip[eoc.nument].fn, project.id);
        strcat(zip[eoc.nument].fn, "/" COMMONDIR "/");
        fwrite(&loc, sizeof(zip_local_t), 1, f);
        fwrite(zip[eoc.nument].fn, zip[eoc.nument].fnlen, 1, f);
        eoc.nument++;
        if(lytlist) {
            for(i = 0; lytlist[i]; i++) {
                strcpy(projfn, "." COMMONDIR SEP);
                strcat(projfn, lytlist[i]);
                strcat(projfn, ".lyt");
                if(project_exists(projdir)) {
                    strcpy(tmp, project.id);
                    strcat(tmp, "/" COMMONDIR "/");
                    strcat(tmp, lytlist[i]);
                    strcat(tmp, ".lyt");
                    curr = project_addfile(f, tmp, curr);
                }
                free(lytlist[i]);
            }
            free(lytlist);
        }
        if(gpllist) {
            for(i = 0; gpllist[i]; i++) {
                strcpy(projfn, "." COMMONDIR SEP);
                strcat(projfn, gpllist[i]);
                strcat(projfn, ".gpl");
                if(project_exists(projdir)) {
                    strcpy(tmp, project.id);
                    strcat(tmp, "/" COMMONDIR "/");
                    strcat(tmp, gpllist[i]);
                    strcat(tmp, ".gpl");
                    curr = project_addfile(f, tmp, curr);
                }
                free(gpllist[i]);
            }
            free(gpllist);
        }
        if(sprlist) {
            for(i = 0; sprlist[i]; i++) {
                strcpy(projfn, "." COMMONDIR SEP);
                strcat(projfn, sprlist[i]);
                strcat(projfn, ".png");
                if(project_exists(projdir)) {
                    strcpy(tmp, project.id);
                    strcat(tmp, "/" COMMONDIR "/");
                    strcat(tmp, sprlist[i]);
                    strcat(tmp, ".png");
                    curr = project_addfile(f, tmp, curr);
                }
                free(sprlist[i]);
            }
            free(sprlist);
        }
    }
    /* write central */
    if(verbose > 1) printf("project_totemplate: writing central directory, %u entries\n", eoc.nument);
    eoc.totoffs = (uint32_t)ftell(f);
    for(i = 0; (uint32_t)i < eoc.nument; i++) {
        fwrite(&zip[i], 46, 1, f);
        fwrite(zip[i].fn, zip[i].fnlen, 1, f);
        free(zip[i].fn);
        eoc.cdsize += 46 + zip[i].fnlen;
    }
    fwrite(&eoc, sizeof(eoc), 1, f);
    fclose(f);
    SDL_SetCursor(cursors[CURSOR_PTR]);
    free(zip);
    ui_status(0, full);
}

/**
 * Save as distributable game file
 */
void project_togame(int menu)
{
    (void)menu;
    if(!project.id[0]) return;
    /* save a game extension with default name and all the data */
    if(project_totng("game", TNG_FLAG_ENCRYPT, NULL, NULL, NULL))
        project.hasGame = 1;
}

/**
 * Import project from game file
 * returns true on success
 */
int project_fromtng(char *fn)
{
    tngctx_t ctx;
    tng_asset_desc_t *desc;
    unsigned char *buf, *comp;
    char tmp[24];
    uint32_t chk1, chk2;
    int i, j, len;
    FILE *f;

    if(!fn || !*fn) return 0;
    if(verbose) printf("project_fromtng: importing %s\n", fn);
    memset(&ctx, 0, sizeof(ctx));
    f = project_fopen(fn, "rb");
    if(f) {
        /* read header */
        len = (int)fread(&ctx.hdr, 1, sizeof(tng_hdr_t), f);
        if(!len || memcmp(&ctx.hdr.magic, TNG_MAGIC, 16) || ctx.hdr.enc) {
            if(verbose) printf("project_fromtng: bad magic or encrypted file\n");
            fclose(f);
            return 0;
        }
        chk1 = ctx.hdr.crc; ctx.hdr.crc = 0;
        chk2 = crc32(0, (const uint8_t*)&ctx.hdr, sizeof(tng_hdr_t));
        buf = (unsigned char*)main_alloc(65536);
        while(!feof(f)) {
            len = (int)fread(buf, 1, 65536, f);
            chk2 = crc32(chk2, buf, len);
        }
        free(buf);
        if(chk1 != chk2) {
            if(verbose) printf("project_fromtng: bad checksum\n");
            fclose(f);
            return 0;
        }
        project_fseek(f, sizeof(tng_hdr_t));
        /* read and uncompress section table */
        chk1 = 0xFFFFFFFF;
        if(!fread(&chk1, 1, 4, f) || (chk1 & 0x80000000) || chk1 < 8) {
            if(verbose) printf("project_fromtng: compressed sections table too big\n");
            fclose(f);
            return 0;
        }
        comp = (unsigned char*)main_alloc(chk1);
        if(!fread(comp, 1, chk1, f)) {
rderr:      if(verbose) printf("project_fromtng: error reading section table\n");
            fclose(f);
            free(comp);
            return 0;
        }
        chk2 = crc32(0, comp, chk1 - 4);
        if(memcmp(comp + chk1 - 4, &chk2, 4)) goto rderr;
        len = 0;
        buf = (uint8_t*)stbi_zlib_decode_malloc_guesssize_headerflag((const char*)comp, (int)chk1 - 4, 4096, &len, 1);
        free(comp);
        if(!buf || len < 1) {
            if(buf) free(buf);
            if(verbose) printf("project_fromtng: uncompression error\n");
            fclose(f);
            return 0;
        }
        ctx.f = f;
        ctx.offs = 68 + chk1;
        ctx.tbl = (tng_section_t*)buf;
        ctx.sec = buf;
        ctx.lensec = len;
        ctx.sts = buf + ctx.tbl[0].offs;
        ctx.numtbl = ctx.tbl[0].offs / sizeof(tng_section_t);
        memcpy(tmp, ctx.hdr.id, 16); tmp[16] = 0;
        if(project_exists(tmp)) {
            if(!ui_modal(ICON_WARN, ERR_EXISTS, projfn)) { fclose(f); return 0; }
            project_delete(tmp);
        }
        project_mkdir(projdir);
        project_free();
        memcpy(project.id, ctx.hdr.id, 16);
        project.type = ctx.hdr.type & 0x0F;
        project.tilew = ctx.hdr.tilew | (((ctx.hdr.type >> 4) & 3) << 8);
        project.tileh = ctx.hdr.tileh | (((ctx.hdr.type >> 6) & 3) << 8);
        project.mapsize = 1 << ctx.hdr.mapsize;
        /* count the number of total assets for the progressbar */
        for(i = 0; i < ctx.numtbl; i++) {
            switch(TNG_SECTION_TYPE(&ctx.tbl[i])) {
                case TNG_SECTION_STRINGS:
                case TNG_SECTION_SPRITEATLAS:
                case TNG_SECTION_EQUIPSLOTS: break;
                case TNG_SECTION_TRANSLATION:
                case TNG_SECTION_FONTS:
                case TNG_SECTION_MUSIC:
                case TNG_SECTION_SOUNDS:
                case TNG_SECTION_MOVIES:
                case TNG_SECTION_SPRITE_BACKGROUND:
                case TNG_SECTION_CHOOSERS:
                case TNG_SECTION_CUTSCENES:
                case TNG_SECTION_CHARS:
                case TNG_SECTION_NPCS:
                case TNG_SECTION_SPAWNERS:
                case TNG_SECTION_DIALOGS:
                case TNG_SECTION_OBJECTS:
                case TNG_SECTION_CRAFTS:
                case TNG_SECTION_QUESTS:
                case TNG_SECTION_MAPS:
                    ctx.total += TNG_SECTION_SIZE(&ctx.tbl[i]) / sizeof(tng_asset_desc_t);
                break;
                case TNG_SECTION_SPRITE_UI:
                case TNG_SECTION_SPRITE_TILE:
                case TNG_SECTION_SPRITE_CHARACTER:
                case TNG_SECTION_SPRITE_PORTRAIT:
                    desc = (tng_asset_desc_t*)(ctx.sec + ctx.tbl[i].offs);
                    len = TNG_SECTION_SIZE(&ctx.tbl[i]) / sizeof(tng_asset_desc_t);
                    for(j = 0; j < len; j++, desc++)
                        ctx.total += desc->name >> 24;
                break;
                default: ctx.total++; break;
            }
        }
        /* load translations, this must be the first so that others can reference texts */
        translate_fromtng(&ctx);
        /* read and save inlined assets */
        fonts_fromtng(&ctx);
        media_fromtng(&ctx);
        spr_fromtng(&ctx);
        /* user interface */
        elements_fromtng(&ctx);
        mainmenu_fromtng(&ctx);
        hud_fromtng(&ctx);
        alerts_fromtng(&ctx);
        credits_fromtng(&ctx);
        choosers_fromtng(&ctx);
        cutscn_fromtng(&ctx);
        /* game logic */
        startup_fromtng(&ctx);
        attrs_fromtng(&ctx);
        actions_fromtng(&ctx);
        chars_fromtng(&ctx);
        npcs_fromtng(&ctx);
        spawners_fromtng(&ctx);
        dialogs_fromtng(&ctx);
        objects_fromtng(&ctx);
        crafts_fromtng(&ctx);
        tiles_fromtng(&ctx);
        quests_fromtng(&ctx);
        maps_fromtng(&ctx);
        fclose(f);
        free(ctx.tbl);
        translate_freestrings();
        /* save project config */
        project_save();
        /* reload project config and all lists. This will set error if anything went wrong with the extraction */
        project_free();
        project_load(tmp);
        return 1;
    }
    return 0;
}

/**
 * Save as distributable game file
 * returns true on success
 */
int project_totng(char *name, int flags, char *opts, char *langs, char *maps)
{
    mbedtls_sha256_context sha;
    z_stream strm;
    tngctx_t ctx;
    uint64_t off;
    uint32_t siz, crc = 0, crc2;
    uint8_t *sec = NULL;
    int i;
    char path[128];
    FILE *f, *g;

    /* generate sections and collect list of assets */
    mbedtls_sha256_init(&sha);
    mbedtls_sha256_starts_ret(&sha, 0);
    memset(&ctx, 0, sizeof(tngctx_t));
    ctx.flags = flags;
    ctx.opts = opts;
    ctx.langs = langs;
    ctx.maps = maps;
    ui_progressbar(1, 2, 0, 100, LANG_COLLECT);
#ifdef _TNGE_PRO_
    if(flags & TNG_FLAG_ENCRYPT)
        pro_encmask();
    else
        project.chk = 0;
#endif
    if(!project.id[0] || !project.name[0] || !name || !*name || !tng_sections(&ctx) || !ctx.tbl || !ctx.sts || !ctx.sec)
        goto err;
    sprintf(projfn, "%s" SEP "%s.tng", project.id, name);
    if(verbose) printf("project_totng: saving %s\n", projdir);
    ui_progressbar(2, 2, 0, 100, LANG_WRITING);
    /* compress sections */
    siz = compressBound(ctx.numtbl * sizeof(tng_section_t) + ctx.offstr + ctx.lensec) + 256;
    sec = (uint8_t*)main_alloc(siz);
    memset(&strm, 0, sizeof(strm));
    deflateInit(&strm, 9);
    strm.avail_out = siz;
    strm.next_out = sec;
    strm.avail_in = ctx.numtbl * sizeof(tng_section_t);
    strm.next_in = (unsigned char *)ctx.tbl;
    deflate(&strm, Z_NO_FLUSH);
    strm.avail_in = ctx.offstr;
    strm.next_in = ctx.sts;
    deflate(&strm, Z_NO_FLUSH);
    strm.avail_in = ctx.lensec;
    strm.next_in = ctx.sec;
    deflate(&strm, Z_FINISH);
    siz = strm.total_out;
    deflateEnd(&strm);
    if(siz < 4) goto err;
    /* add a compressed section crc, because calculating crc on the entire file could be time consuming */
    crc = crc32_z(0, sec, siz);
    memcpy(sec + siz, &crc, 4);
    siz += 4;
    /* write tng file */
    f = project_fopen(projdir, "wb+");
    if(f) {
        /* write header */
        fwrite(&ctx.hdr, 1, sizeof(tng_hdr_t), f);
        mbedtls_sha256_update_ret(&sha, (const uint8_t*)&ctx.hdr, sizeof(tng_hdr_t));
        mbedtls_sha256_update_ret(&sha, sec, siz);
        off = sizeof(tng_hdr_t) + 4 + siz;
        ctx.total += off;
        /* write sections */
        crc2 = siz;
#ifdef _TNGE_PRO_
        pro_encrypt(64, 4, (unsigned char*)&crc2);
        pro_encrypt(68, siz, sec);
#endif
        fwrite(&crc2, 1, 4, f);
        fwrite(sec, 1, siz, f);
        crc = crc32_z(0, (unsigned char*)&crc2, 4);
        crc = crc32_z(crc, sec, siz);
        sec = (uint8_t*)realloc(sec, 1024*1024);
        if(!sec) main_error(ERR_MEM);
        ui_progressbar(2, 2, off, ctx.total, LANG_WRITING);
        /* write assets */
        for(i = 0; i < ctx.numassets; i++) {
            if(ctx.assets[i].external) {
                if(ctx.assets[i].size < 1) continue;
                if(verbose) printf("project_totng: adding asset %s (%u bytes)\n", (char*)ctx.assets[i].data, ctx.assets[i].size);
                g = project_fopen((char*)ctx.assets[i].data, "rb");
                if(g) {
                    do {
                        siz = fread(sec, 1, 1024*1024, g);
                        mbedtls_sha256_update_ret(&sha, sec, siz);
#ifdef _TNGE_PRO_
                        pro_encrypt(off, siz, sec);
#endif
                        fwrite(sec, 1, siz, f);
                        crc = crc32_z(crc, sec, siz);
                        off += siz;
                        ui_progressbar(2, 2, off, ctx.total, LANG_WRITING);
                    } while(siz == 1024*1024);
                    fclose(g);
                }
            } else {
                if(verbose) printf("project_totng: adding generated asset #%u (%u bytes)\n", i, ctx.assets[i].size);
                mbedtls_sha256_update_ret(&sha, ctx.assets[i].data, ctx.assets[i].size);
#ifdef _TNGE_PRO_
                pro_encrypt(off, ctx.assets[i].size, ctx.assets[i].data);
#endif
                fwrite(ctx.assets[i].data, 1, ctx.assets[i].size, f);
                crc = crc32_z(crc, ctx.assets[i].data, ctx.assets[i].size);
                off += ctx.assets[i].size;
                ui_progressbar(2, 2, off, ctx.total, LANG_WRITING);
            }
        }
        free(sec); sec = NULL;
        /* update with checksums and write the header again */
        mbedtls_sha256_finish_ret(&sha, (unsigned char*)&path);
        mbedtls_sha256_free(&sha);
        memcpy(&ctx.hdr.unique, path, sizeof(ctx.hdr.unique));
        ctx.hdr.enc = project.chk;
        crc2 = crc32_z(0, (unsigned char*)&ctx.hdr, sizeof(tng_hdr_t));
        ctx.hdr.crc = crc32_combine(crc2, crc, off - sizeof(tng_hdr_t));
        project_fseek(f, 0);
        fwrite(&ctx.hdr, 1, sizeof(tng_hdr_t), f);
        fclose(f);
        tng_free(&ctx);
        /* create launchers */
        sprintf(projfn, "%s" SEP "%s.lnk", project.id, project.name);
        sprintf(path, "C:\\Program Files\\%s\\game.tng", project.id);
        project_mklnk(projdir, strcmp(name, "game") && project.url[0] ? project.url : path);
        sprintf(projfn, "%s" SEP "%s.desktop", project.id, project.name);
        sprintf(path, "/usr/share/games/%s/game.tng", project.id);
        project_mkdsk(projdir, strcmp(name, "game") && project.url[0] ? project.url : path);
        sprintf(projfn, "%s" SEP "%s.tng", project.id, name);
        ui_status(0, lang[LANG_SAVED]);
        if(verbose) printf("project_totng: %s saved.\n", projfn);
        return 1;
    } else {
        ui_status(1, lang[ERR_SAVETNG]);
err:    if(sec) free(sec);
        if(ctx.err) {
            if(verbose) printf("project_totng: section size exceeds maximum\n");
            ui_status(1, lang[ERR_SAVETNG]);
        }
        tng_free(&ctx);
    }
    return 0;
}
