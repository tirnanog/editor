/*
 * tnge/main.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Main header
 *
 */

/* set static limits and constants */
#define PROJ_FORMATREV      0
#define PROJ_NAMEMAX        32
#define PROJ_TITLEMAX       128
#define PROJ_DESCMAX        1024
#define PROJ_EXPRMAX        128
#define PROJ_ATTRMAX        99999
#define PROJ_MAPSIZE        32

#include <stdint.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <time.h>
#include <limits.h>
#include <SDL.h>
#include <SDL_mixer.h>
#include "zlib.h"
#include "tng.h"
#include "ui.h"
#include "project.h"
#include "lang.h"
#include "data.h"

#ifdef __WIN32__
#define SEP "\\"
#define TNGP "C:\\Program Files\\TirNanoG\\tngp.exe"
#define FFMPEG "C:\\Program Files\\TirNanoG\\ffmpeg.exe"
#define DOCDIR "C:\\Program Files\\TirNanoG\\doc"
#define ASSIMP "assimp32.dll"
#else
#define SEP "/"
#define TNGP "/usr/bin/tngp"
#define FFMPEG "/usr/bin/ffmpeg"
#define DOCDIR "/usr/share/doc/tnge"
#define ASSIMP "libassimp.so"
#endif

#ifndef PATH_MAX
# ifdef MAX_PATH
#  define PATH_MAX MAX_PATH
# else
#  define PATH_MAX 4096
# endif
#endif
#ifndef FILENAME_MAX
# define FILENAME_MAX 256
#endif

#ifndef PRIu64
#if __WORDSIZE == 64
#define PRIu64 "lu"
#define PRId64 "ld"
#else
#define PRIu64 "llu"
#define PRId64 "lld"
#endif
#endif

#define DEFLANG "en"
#define FREQ 44100
#define VIDEO_QUALITY 5
#define AUDIO_QUALITY 3


/* these are not only used by chars.c, but also by npcs.c and objects.c for preview */
typedef struct {
    int rel;                        /* is required relation (or -1 for provided attribute) */
    int attr;                       /* attribute index */
    int val;                        /* value */
} chars_attrs_t;

typedef struct {
    int qty;                        /* number of objects */
    int obj;                        /* object index */
} chars_inv_t;

typedef struct {
    char opt[PROJ_NAMEMAX + 1];     /* option's name */
    char desc[PROJ_DESCMAX + 1];    /* option's description */
    int numattrs, numinvs, numsprs; /* number of attributes, inventory items and sprites */
    chars_attrs_t *attrs;           /* attributes */
    chars_inv_t *invs;              /* inventory items */
    int *sprs;                      /* sprite indeces */
} chars_opt_t;

/* likewise not only used by hud.c, but also by objects.c for preview */
typedef struct {
    char name[PROJ_NAMEMAX + 1];    /* unique equipment slot name */
    int left;                       /* left margin */
    int top;                        /* top margin */
} hud_equip_t;

/* not only by translate.c, but also by tng.c */
typedef struct {
    char *id;
    char *str;
    int len;
} trn_t;

char *stbi_zlib_decode_malloc_guesssize_headerflag(const char *buffer, int len, int initial_size, int *outlen, int parse_header);
extern char tngever[], site_url[];
extern int verbose;

void main_error(int msg);
void *main_alloc(size_t size);
void main_open_url(char *url);
