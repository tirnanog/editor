/*
 * tnge/lang_ru.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Russian dictionary for TirNanoG Editor
 *
 */

"ru",

"Не удалось инициализировать SDL",
"Ошибка выделения памяти",
"Файл уже существует. Переписать?",
"Ошибка запуска плеера",
"Ошибка чтения конфигурации",
"Ошибка записи конфигурации",
"Ошибка записи endusers.txt",
"Ошибка написания макета",
"Ошибка записи спрайта",
"Ошибка при загрузке шаблона",
"Ошибка написания шаблона",
"Ошибка при загрузке архива TNG",
"Ошибка записи архива TNG",
"Ошибка распаковки",
"Ошибка загрузки палитры из файла",
"Ошибка загрузки изображения",
"Не удалось загрузить макет",
"Не могу добавить спрайт другого размера",
"Не могу добавить, слишком много кадров",
"Имя спрайта не указано",
"У какого-то спрайта нет имени, мне использовать числа?",
"Имя спрайта не уникально",
"Ошибка инициализации звука",
"Ошибка чтения аудио",
"Ошибка записи аудио",
"Ошибка чтения фильма",
"Ошибка записи фильма",
"Ошибка чтения выбора",
"Ошибка при написании выбора",
"Ошибка чтения ролика",
"Ошибка записи ролика",
"Ошибка чтения конфигурации пользовательского интерфейса",
"Ошибка записи конфигурации пользовательского интерфейса",
"Ошибка чтения сценария запуска",
"Ошибка написания скрипта запуска",
"Ошибка чтения конфигурации действия",
"Ошибка записи конфигурации действия",
"Ошибка чтения атрибутов",
"Ошибка записи атрибутов",
"Ошибка в выражении",
"Ошибка в скрипте, см. блоки, отмеченные красным",
"Сценарий запуска отсутствует",
"Ошибка чтения параметров символов",
"Ошибка при записи параметров символов",
"Ошибка чтения NPC",
"Ошибка записи NPC",
"Ошибка чтения спавнера",
"Ошибка записи спавнера",
"Ошибка чтения диалога",
"Ошибка записи диалога",
"Ошибка чтения объекта",
"Ошибка записи объекта",
"Ошибка чтения диалога",
"Ошибка записи диалога",
"Ошибка чтения информации о плитке",
"Ошибка при записи информации о тайле",
"Ошибка чтения карты",
"Ошибка записи карты",
"Ошибка чтения конфигурации квеста",
"Ошибка записи конфигурации квеста",
"Ошибка чтения квеста",
"Ошибка написания квеста",
"Ошибка чтения перевода",
"Ошибка написания перевода",
"Не могу перевести на базовый язык",

"Сохраните и начните игру",
"Откройте руководство пользователя",
"Проект",
"Ресурсы",
"Интерфейс",
"Игра",
"Переводы",

"Новый спрайт",
"Новый язык",

"Новый проект",
"Загрузить проект",
"Импорт игры",
"Импорт шаблона",
"Экспорт игры",
"Расширение экспорта",
"Экспорт шаблона",
"Лицензионные ключи",
"Насчет",
"Покидать",

"Шрифты",
"Спрайты",
"Генератор",
"Музыка",
"Звуки",
"Речь",
"Кино",

"Элементы",
"Главное меню",
"HUD",
"Оповещения",
"Кредиты",
"Выбирающие",
"Катсцены",

"Запускать",
"Атрибуты",
"Действия",
"Характер",
"NPC",
"Спавнеры",
"Диалоги",
"Объекты",
"Крафт",
"плитка",
"Карты",
"Квесты",

"Редактор ролевых игр",
"Эта программа является бесплатным программным обеспечением; вы можете\nраспространять его и/или модифицировать в соответствии с условиями\nСтандартной общественной лицензии GNU; либо версия 3 Лицензии, либо\n(на ваш выбор) любая более поздняя версия.",
"Эта программа распространяется в расчете на то, что она будет полезна,\nно БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ; даже без подразумеваемой гарантии\nКОММЕРЧЕСКОЙ ПРИГОДНОСТИ или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННОЙ ЦЕЛИ.\nПодробнее см. в Стандартной общественной лицензии GNU.",
"редактор бесплатный и с открытым исходным кодом",
"с ним можно сохранять только бесплатные игры",
"лицензия на файл коммерческой игры",
"Лучше всего с активами от",

"Недавно использованные папки",
"Показать все файлы",
"Имя",
"Размер",
"Изменено",
"Размер растра",
"Диапазон",
"Максимальное количество цветов",
"Кадров в секунду",
"Облицовка",
"Руководство",
"Вручную выберите кадры действий",
"Импортировать",
"Нагрузка",
"Воскресенье",
"понедельник",
"вторник",
"среда",
"Четверг",
"Пятница",
"Суббота",
"Вчерашний день",
"Прямо сейчас",
"%d минут назад",
"%d часов назад",
"час назад",

"Идентификатор игры",
"Заголовок",
"Шаблон",
"плитка",
"Размер карты",
"Скачать больше",
"Создать новый проект",
"UNIX-имя проекта",
"Переводимый, понятный человеку заголовок",
"Проект будет создан с этим скелетом",
"Размер карты в тайлах",
"Произведение размера карты и фрагмента превышает 4096. Это может быть проблематично.",

"Загрузить выбранный проект",
"проекты не найдены",
"Добавить новый проект",
"Удалить выбранный проект",
"Импорт шаблона проекта",
"Экспорт шаблона проекта",

"Имя расширения",
"Дистанционный пульт",
"URL-адрес сервера для MMORPG",
"Шифровать",
"Опции",
"Переводы",
"Выбрать все",
"нет переводов",

"ID лицензии",
"Владелец",
"Лицензия этого редактора дает вам разрешение на создание файла коммерческой игры.",
"Ключи дешифрования конечного пользователя",
"нет лицензий конечного пользователя",
"Всего",
"Создать",
"Скопировать лицензионный ключ в буфер обмена",
"Лицензия скопирована в буфер обмена",

"Шрифт",
"Предварительный просмотр",
"вектор",
"шрифты не найдены",
"Добавить новый шрифт",
"Удалить шрифт",
"Цвет шрифта",
"Сделать жирным",
"Сделать курсивом",
"Размер в пикселях",
"Семейство шрифтов",
"Импорт шрифта",

"Спрайты не найдены. Сначала импортируйте.",
"Сохранение спрайтов...",
"Добавить новые спрайты из изображений",
"Создание спрайтов из шаблонов",
"Вернуться к списку спрайтов",
"Удалить спрайт",
"Очистить таблицу спрайтов",
"Перейти в другую категорию",
"Спрайты пользовательского интерфейса",
"Тайловые спрайты",
"Спрайты объектов",
"Спрайты персонажей",
"Портретные спрайты",
"Фоны",
"кадр(ы)",
"Загрузить изображение или модель в виде таблицы спрайтов",
"Преобразовать в палитру",
"Преобразование...",
"Импорт палитры",
"Удалить палитру",
"Поменяйте местами синий и красный каналы",
"Используйте метод CIE76 вместо расстояния sRGB при поиске наилучшего соответствия",
"Уменьшить до половины",
"Вырасти вдвое",
"Отразить изображение по вертикали",
"Перевернуть изображение по горизонтали",
"Используйте сетку для чтения нескольких спрайтов",
"Ширина спрайта",
"Высота спрайта",
"Маржа слева",
"Верхний край",
"Вертикальное заполнение",
"Отступ горизонтальный",
"Использовать существующий макет",
"Сохранить макет",
"Название группы спрайтов",
"Удалить спрайт из списка",
"Имя спрайта",
"Играть один раз",
"Играть вперед и назад",
"Воспроизведение в цикле",
"юг",
"Юго-запад",
"Запад",
"Северо-Запад",
"север",
"К северо-востоку",
"Восток",
"Юго-восток",
"Ожидание загрузки модели assimp",
"Для рендеринга потребуется более 1G RAM!",
"Вы уверены что хотите продолжить?",
"Рендеринг модели",
"Генерация спрайтов",
"Начальный кадр",
"Количество кадров для добавления",
"Добавить кадры как спрайт",
"Опции",
"Поиск среди вариантов",
"Выберите вариант цвета",

"Инструментальная музыка",
"Звуковые эффекты",
"Переводимые записи речи",
"Кинофильм",
"Продолжительность",
"Название носителя",
"медиа не найдено",
"Добавить новые медиафайлы",
"Удалить медиа",
"Воспроизвести медиа",
"Импорт медиафайлов...",

"Курсоры",
"Входы",
"Окна",
"Диалоги",
"Кнопки",
"Шрифты",
"Нормальный",
"Выбрано",
"Нажатый",
"Неактивный",
"Чат",
"Заголовок окна",
"Неактивный заголовок",
"Название диалога",
"Заполнение верхней части вкладки окна",
"Неактивное заполнение верхней части табуляции",
"Верхний отступ заголовка диалогового окна",
"Верхнее заполнение содержимого окна",
"Содержимое окна слева отступа",
"Верхняя обивка пуговиц",
"Удалить конфигурацию интерфейса",
"Предварительный просмотр пользовательского интерфейса",
"На выбранном звуке",
"Нажатый звук",
"Звук ошибки",
"Указатель курсора",
"Курсор нажат",
"Курсор запрещен",
"Действие/использование курсора",
"Загрузка курсора",
"Флажок снят",
"Флажок установлен",
"Предыдущий вариант",
"Нажата предыдущая опция",
"Фон списка опций",
"Следующий вариант",
"Нажата следующая опция",
"Ползунок влево",
"Кнопка слайдера",
"Фон слайдера",
"Ползунок вправо",
"Полоса прокрутки слева",
"Горизонтальный фон полосы прокрутки",
"Полоса прокрутки справа",
"Полоса прокрутки вверх",
"Вертикальный фон полосы прокрутки",
"Полоса прокрутки вниз",
"Кнопка полосы прокрутки",
"Вкладка нормальная левая",
"Вкладка обычный фон",
"Вкладка обычная справа",
"Неактивная вкладка слева",
"Фон неактивной вкладки",
"Неактивная вкладка справа",
"Верхняя левая граница окна",
"Верхняя граница окна",
"Верхняя правая граница окна",
"Левая граница окна",
"Фон окна",
"Правая граница окна",
"Нижняя левая граница окна",
"Нижняя граница окна",
"Нижняя правая граница окна",
"Название диалога слева",
"Фон заголовка диалогового окна",
"Название диалога справа",
"Вариант диалога слева",
"Фон диалогового окна",
"Вариант диалога справа",
"Верхняя левая граница диалогового окна",
"Верхняя граница диалогового окна",
"Верхняя правая граница диалогового окна",
"Левая граница диалогового окна",
"Фон диалогового окна",
"Правая граница диалога",
"Нижняя левая граница диалогового окна",
"Нижняя граница диалогового окна",
"Нижняя правая граница диалогового окна",
"Кнопка нормальная левая",
"Кнопка нормальный фон",
"Кнопка нормальная правая",
"Кнопка выбрана слева",
"Кнопка выбранный фон",
"Кнопка выбрана правильно",
"Кнопка нажата влево",
"Кнопка нажата фон",
"Кнопка нажата вправо",
"Кнопка неактивна слева",
"Кнопка неактивный фон",
"Кнопка неактивна справа",
"Вход в чат слева",
"Фон ввода чата",
"Ввод символов справа",
"Значок приложения",
"Пример флажка",
"Вариант",
"Слайдер",
"Актер",
"Вопрос?",
"Ответ 1",
"Ответ 2",
"Ответ 3",

"вступление",
"Кат-сцена, показанная перед первым появлением главного меню",
"главное меню включено",
"Задний план",
"Фоновая музыка",
"Фоновый фильм",
"Фоновая картинка",
"Не обрезать фон",
"Веб-сайт",
"URL-адрес, который будет открыт при нажатии на заголовок",
"Горизонтальный",
"Вертикальный",
"Интервал",
"Параллакс",
"Заголовок",
"Заглавное изображение",
"Цвет заголовка",
"Цвет тени",
"Кнопки",
"Верхняя обивка пуговиц",
"Ввод имени",
"Главное меню предварительного просмотра",
"Генерация превью персонажа",
"График",
"Цвет сетки графика",
"Цвет графика",
"Удалить конфигурацию главного меню",
"Мерлин",
"Мужской",
"Эльф",
"Маг",
"Сила",
"Ловкость",
"Конституция",
"Интеллект",
"Жизнеспособность",

"Элементы управления",
"Панель элементов",
"Статус бар",
"в сочетании с панелью элементов",
"Прогрессбары",
"Направление",
"Ценность",
"Максимум",
"Изображение",
"индикаторы прогресса не указаны",
"Изображение слишком широкое (максимум 500 пикселей)",
"Фоновая картинка",
"Дополнительный фон",
"Переднее изображение",
"Дополнительная передняя крышка",
"Количество элементов на панели элементов",
"Ширина элемента",
"Высота предмета",
"Позиция элемента относительно панели элементов",
"Вертикальный зазор между элементами",
"Дополнительный зазор между предметами и портативным предметом",
"Круг навигатора для сенсорных экранов",
"Кнопки действий для сенсорных экранов",
"Стереть выбранный индикатор выполнения",
"Позиция относительно строки состояния/панели элементов",
"Никакая позиция не дает временного эффекта, индикатор прогресса",
"Получить координаты из изображения бара, подсказка: сначала выберите изображение индикатора выполнения",
"Добавить новый индикатор выполнения в список",
"Прогрессбар слева направо",
"Прогрессбар снизу вверх",
"Альфа-канал",
"полоса здоровья врага",
"Инвентарь",
"Значок инвентаря",
"Иконка инвентаря нажата",
"Ширина предмета в слоте",
"Высота предмета в слоте",
"Положение предмета относительно слота",
"Количество элементов шрифта",
"Фон слота",
"Выбранный фон слота",
"Изображение слотов снаряжения",
"слоты оборудования не определены",
"Название слота снаряжения",
"Стереть выбранный слот снаряжения",
"Положение слота снаряжения",
"Уникальное название слота снаряжения",
"Добавить новый слот оборудования в список",
"Имя слота должно быть уникальным",
"Удалить конфигурацию HUD",
"Предварительный просмотр интерфейса HUD",

"Шрифт",
"Продолжительность",
"Новый квест",
"Завершено",
"Не удалось",
"Удалить конфигурацию оповещения о квесте",
"Предварительный просмотр квеста",

"Задний план",
"Фоновая музыка",
"Фоновая картинка",
"Заголовок",
"Шрифт",
"Категория",
"Авторы",
"в этой категории нет авторов",
"Удалить автора из списка",
"Имя автора",
"Добавить автора в категорию",
"Удалить конфигурацию кредитов",
"Предварительный просмотр кредитов",

"нет выбора",
"Удалить выбранный селектор",
"Предварительный просмотр выбора маршрута",
"нет выбора",
"Левое и правое поле",
"Верхнее и нижнее поле",
"Горизонтальное заполнение",
"Вертикальное заполнение",
"Должность",
"Изображение",
"Обычный значок",
"Выбранный значок",
"Значок нажатия",
"Прямоугольник вверху слева в процентах экрана",
"Прямоугольник внизу справа в процентах экрана",
"Помощник по подбору координат",
"Требует",
"Обеспечивает",
"и",
"Держите выбор в рабочем состоянии",
"Удалить опцию выбора",
"Добавить вариант выбора",
"Возвращает",

"нет роликов",
"Удалить выбранный ролик",
"Предварительный ролик",
"Название ролика",
"Фоновая музыка / киноканал",
"Канал слайд-шоу",
"Речевой канал",
"Канал субтитров",
"Канал сценария",
"Фоновый фильм",
"Фоновая музыка",
"Сначала выберите фоновый фильм или музыку",
"Выберите интервал для редактирования",
"Слайд-шоу",
"Цвет слайд-шоу",
"Положение текста на экране в процентах",
"Выровнять по левому краю",
"Выровнять по центру",
"Выровнять вправо",
"Воспроизвести выбранный интервал музыки",
"Команды скрипта",
"Играть один раз или сначала",
"Воспроизведение в цикле, пока пользователь не прервет",
"Удалить слайд(ы) с временной шкалы",
"Удалить записи речи с временной шкалы",
"Удалить субтитры с временной шкалы",
"Удалить субтитры",
"Дублировать субтитры",
"без субтитров",
"Исчезать и исчезать",

"Удалить сценарии запуска и таймера",
"Скрипты, работающие через равные промежутки времени",

"нет атрибутов",
"Удалить выбранный атрибут",
"Заголовок",
"Начальный",
"Переменная",
"Рассчитано",
"Глобальная переменная",
"Глобальный расчет",
"Значение должно быть установлено в меню «Символ».",
"Настройка атрибутов",
"Тип атрибута",
"Имя атрибута",
"Название атрибута, оставьте поле пустым для скрытого атрибута",
"Выражение для расчета",
"Значение по умолчанию",
"Всего баллов в режиме свободной формы",
"Максимум. за атрибут",
"Атрибут скорости",
"Атрибут веса",
"Радиус действия",
"Очки повышения уровня",

"Пользовательское действие",
"Атрибуты дальности для дальних атак",

"нет групп опций",
"Удалить выбранную группу опций",
"Предварительный просмотр объединенных слоев анимации",
"Группы опций",
"база берегись",
"Уровень земли от вертикального центра",
"Палитра цветовых вариаций",
"Опции",
"нет вариантов",
"Атрибуты",
"нет атрибутов",
"Название группы опций",
"Удалить определение атрибута",
"Установить атрибут или отношение для требуемого атрибута",
"Добавить новое определение атрибута",
"Инвентарь",
"нет товаров",
"Удалить предмет из стартового инвентаря",
"Количество",
"Объект",
"Добавить предмет в стартовый инвентарь",
"Удалить вариант",
"Вариант",
"Добавить новую опцию",
"нет слоев анимации",
"Удалить слой",
"Копировать слои в буфер обмена",
"Скопируйте и очистите слои",
"Вставить слои из буфера обмена",
"Переместить слой вверх",
"Переместить слой вниз",
"Добавить новый слой",
"Праздный",
"Ходить",
"Взбираться",
"Прыгать",
"Бежать",
"Плавать",
"Летать",
"Блокировка",
"Повредить",
"Умри",
"Действие 1",
"Действие 2",
"Действие 3",
"Действие 4",
"Действие 5",
"Действие 6",
"Действие 7",
"Действие 8",
"Действие 9",

"нет NPC",
"Удалить выбранного неигрового персонажа",
"Предварительный просмотр объединенных слоев анимации NPC",
"Движение по умолчанию",
"стоя",
"блуждающий",
"патруль",
"Скорость",
"Изменить курс",
"Дорожка",
"Процент от нормальной скорости",
"Случайное изменение направления каждые несколько секунд",
"Расстояние в единицах карты",
"На мероприятии",
"Заголовок",
"ул",
"Портрет",
"Шанс",
"Кол-во",
"Предмет инвентаря",
"Частота",
"один раз",
"Цена продажи",
"Цена покупки",
"Конфигурация NPC",
"Инвентарь NPC, лут",
"Внутреннее имя неигрового персонажа",
"Удалить предмет из инвентаря",
"Шанс добавить предмет в инвентарь",
"Количество товара",
"Пополняйте инвентарь каждые несколько минут",
"Добавить предмет в инвентарь",
"Рассчитать цену в переменной 'a'",

"нет спаунеров",
"Удалить выбранный спавнер",
"Уникальное имя спавнера",
"Количество макс. NPC",
"Частота появления",
"нет NPC",
"Удалить NPC из списка",
"Требует",
"Обеспечивает",
"вид NPC",
"Анимация появления",
"Добавить NPC в список",
"Условно спаун",
"Установить атрибут NPC",

"нет диалогов",
"Диалоговое окно удаления",
"Диалоговое окно предварительного просмотра",
"Заголовок. Используйте {} для имени и портрета игрока",
"портрет слева",
"Сообщение",
"Варианты ответа",
"нет вариантов ответа",
"Имя диалога",
"Сообщение произнесено",
"Выбрать портрет для диалога",
"Удалить вариант ответа",
"Переместить вариант ответа вверх",
"Переместить вариант ответа вниз",
"Добавить новый вариант ответа",
"Условный вариант ответа требует",
"Предоставляет, когда выбран этот вариант",

"Мета объекта",
"нет объекта мета",
"Выбрать объект по картинке",
"Удалить выбранный объект",
"Предварительный просмотр экипированного предмета",
"Предварительный просмотр предмета в инвентаре",
"Дублирующий объект",
"На карте",
"В инвентаре",
"К какой группе действий относится",
"При оснащении",
"Заголовок",
"Слоты для снаряжения",
"навык",
"Обязательные атрибуты",
"Модификаторы атрибутов",
"Спрайт-снаряд",
"Продолжительность анимации снаряда",
"Объект боеприпасов",
"Порядок слоев",
"Слой переднего плана",
"Фоновый слой",
"Установить специальный спрайт в инвентаре",
"Звуковой эффект при помещении в инвентарь",
"Базисная цена",
"Свободно выбираемая категория по цене",
"Модификатор влияет, только если объект экипирован",
"Используйте процент для изменения атрибута",
"Удалить обязательный атрибут",
"Добавить обязательный атрибут",
"Удалить модификатор атрибута",
"Добавить модификатор атрибута",
"транспорт",

"порядок не имеет значения",
"Ингредиенты",
"нет рецептов",
"Название диалогового окна крафта",
"Заголовок",
"Созданный звуковой эффект",
"Удалить рецепт",
"Добавить новый рецепт",
"Товар",

"Плитки Мета",
"нет меты плитки",
"Удалить выбранную плитку",
"Выбрать плитку по картинке",
"Правила автокарты",
"Комбинируйте с другой плиткой",
"Скорость",
"Звуковой эффект",
"На прогулке",
"В плавании",
"На лету",
"нет правил автокарты",
"Удалить выбранное правило автосопоставления",
"Добавить правило автосопоставления в список",

"нет карт",
"Удалить выбранную карту",
"Предварительный просмотр карты",
"Карта поиска",
"Заголовок",
"Дневной свет",
"Земля 1",
"Земля 2",
"Земля 3",
"Земля 4",
"Объекты",
"NPC",
"Спавнеры",
"Крыша 1",
"Крыша 2",
"Крыша 3",
"Крыша 4",
"Столкновение",
"Патрульные пути",
"Скопировать выделение в буфер обмена",
"Скопировать выделение в буфер обмена и стереть с карты",
"Вставить выделение из буфера обмена на карту",
"Повернуть карту",
"Метаинформация карты",
"Выберите слой и измените видимость",
"Изменить предмет для рисования",
"Перейти в режим ластика",
"Выбрать объект с карты",
"Краска с предметом",
"Залить выделенную область элементом",
"Выберите прямоугольную область",
"Выберите многоугольник произвольной формы",
"Выберите непрерывную область одного и того же элемента",
"Добавить к выбору",
"Пересечение с существующим выделением",
"Рисовать в линию",
"Отразить буфер обмена по горизонтали",
"Отразить буфер обмена по вертикали",
"Изменить направление маршрута патрулирования",
"Внутреннее имя карты",
"Название карты",
"Продолжительность дня в минутах",
"Палитра, используемая для указания времени суток",
"нет параллаксов",
"Удалить выбранный параллакс",
"Параллакс над слоями",
"Параллакс за слоями",
"Укажите движение параллакса по оси X",
"Укажите движение параллакса по оси Y",
"Укажите дельта-движение параллакса относительно игрока по оси X",
"Укажите дельта-движение параллакса относительно игрока по оси Y",
"Интервал движения параллакса",
"Выберите спрайт параллакса",
"Добавить параллакс",
"Соедините южную сторону карты с другой картой",
"Соедините юго-западную сторону карты с другой картой.",
"Соедините западную сторону карты с другой картой",
"Соедините северо-западную сторону карты с другой картой.",
"Соедините северную сторону карты с другой картой",
"Соедините северо-восточную сторону карты с другой картой.",
"Соедините восточную сторону карты с другой картой",
"Соедините юго-восточную сторону карты с другой картой.",

"нет квестов",
"Удалить выбранный квест",
"Предварительный квест",
"Заголовок",
"Описание",
"только один игрок может завершить",
"По завершении",
"Внутреннее название квеста",

"нет голосовых записей",
"Оригинал",
"Переведено",
"Код языка ISO-639-1",
"Родное название языка",
"Удалить перевод",

"НЕТ",
"Комментарий",
"Итерация",
"Условные ветки",
"Присвоить значение переменной",
"Завершить игру, вернуться в главное меню",
"Отложить выполнение",
"Изменить фоновую музыку",
"Воспроизвести звуковой эффект",
"Воспроизвести переведенную речь",
"Выбор маршрута запуска",
"Воспроизвести кат-сцену",
"Воспроизвести диалог",
"Перенести персонажа в локацию",
"Добавить квест или отметить как выполненный",
"Воспроизвести анимацию персонажа",
"Воспроизвести объектную анимацию",
"Разместите NPC",
"Выберите NPC",
"Изменить поведение NPC",
"Открыть инвентарь",
"Повернуть / переместить NPC на юг",
"Повернуть / переместить NPC на юго-запад",
"Повернуть / переместить NPC на запад",
"Повернуть / переместить NPC на северо-запад",
"Повернуть / переместить NPC на север",
"Повернуть / переместить NPC на северо-восток",
"Повернуть / переместить NPC на восток",
"Повернуть / переместить NPC на юго-восток",
"Удалить объект с карты",
"Разместить объект на карте",
"Удалить объект с карты",
"Разместить объект на карте",
"Заменить объект на карте",
"Отдать предмет герою",
"Забрать предмет у героя",
"Выполнить обработчик событий клика другого объекта",
"Изменить вид транспорта",
"Установите сцену на определенную карту и время",
"Подождите, пока актер закончит задание",
"Открыть диалог крафта",
"Посмотрите, есть ли объект в заданном месте на карте",
"Показать предупреждающее сообщение",
"Изменить высоту",
"Атака",
"Нейтральный",
"Бежать",
"Гулять пешком",
"Морской",
"Аэро",
"При запуске",
"Каждую 1/10 сек.",
"Каждую 1 сек.",
"Каждые 10 секунд",
"По изменению",
"По щелчку",
"На ощупь",
"В отпуске",
"На подходе",
"О действии №1",
"О действии №2",
"О действии №3",
"О действии №4",
"О действии № 5",
"О действии № 6",
"О действии №7",
"О действии №8",
"О действии №9",
"При использовании #1",
"При использовании #2",
"При использовании #3",
"Каждую",
"Повторить, если выражение верно",
"Добавить новую ветку",
"Удалить ветку",
"Выражение для выбора ветки",
"Средства массовой информации",
"Селектор",
"Катсцена",
"Анимация",
"NPC",
"Диалог",
"Объект",
"Правила крафта",
"Координаты карты",
"Направление или просто вид",
"Квест",
"Отметить квест как завершенный",
"Поведение",
"Тип транспорта",
"Дневное время в часах",
"Анимация действий",
"Отображать так долго",
"Играть так долго",
"Подождите так долго",
"Переместите столько плиток",
"Количество",

"Уникальное имя",
"Ценность",
"Загрузка...",
"Извлечение...",
"Сбор информации...",
"Сжатие изображений атласа...",
"Письмо...",
"Наглядная форма",
"Вы уверены, что хотите удалить?",
"Вы можете использовать ссылки {атрибут} и {} для имени игрока.",
"никто",
"дефолт",
"набор",
"да",
"Нет",
"В ПОРЯДКЕ",
"Сохранять",
"сохранен.",
"Приблизить",
"Уменьшить",
"Увеличить до нормального",
"Маска столкновения",
"Переключить маску события",
"Изменить видимость",
"%H:00",

"Загрузить игру",
"Новая игра",
"Конфигурация",
"Выход",
"Генерация персонажа",
"Имя пользователя",
"Пароль",
"Оставшийся",
"Создавать",
"Назад",
"Игрок с таким именем уже существует",
"В ПОРЯДКЕ",
"Сохраненные игры",
"Нагрузка",
"Войти на сервер",
"Авторизоваться",
"Неверное имя пользователя или пароль",
"Чат",
"Сохранить игру",
"Назад к меню",
"Инвентарь",
"Навыки и умения",
"Квесты",
"Продать",
"Купить",
"Без модификаторов",
"Требует",
"Обеспечивает",
"Боеприпасы",
"Дизайн игры и сюжет",
"Концептуальное искусство",
"Графический дизайн",
"Аниматоры",
"Шрифты",
"Музыка",
"Звуковые эффекты",
"Актеры озвучивания",
"Переводчики",
"Программирование"
