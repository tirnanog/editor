/*
 * libsfn/libsfn.h
 *
 * Copyright (C) 2020 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief SSFN Utility functions
 *
 */

#ifndef LIBSFN_H
#define LIBSFN_H

#include <stdint.h>
#include "ssfn.h"
#include "lang.h"

#define iswhitespace(x) ((x)==0x20||(x)==0xA0)

/**
 * Messages for progress bar and progress bar callback prototype
 */
enum {
    PBAR_NONE = 0,
    PBAR_MEASURE = FONTS_IMPORT,
    PBAR_OUTLINE = FONTS_IMPORT,
    PBAR_GETKERN = FONTS_IMPORT,
    PBAR_QUANT = FONTS_IMPORT,
    PBAR_RDFILE = FONTS_IMPORT,
    PBAR_BITMAP = FONTS_IMPORT,
    PBAR_TALLPIX = FONTS_IMPORT,
    PBAR_WIDEPIX = FONTS_IMPORT,
    PBAR_GENFRAG = FONTS_IMPORT,
    PBAR_COMPFRAG = FONTS_IMPORT,
    PBAR_SERFRAG = FONTS_IMPORT,
    PBAR_WRTCHARS = FONTS_IMPORT,
    PBAR_WRTFILE = FONTS_IMPORT,
    PBAR_RASTERIZE = FONTS_IMPORT,
    PBAR_VECTORIZE = FONTS_IMPORT,
    PBAR_COPY = FONTS_IMPORT
};
typedef void (*sfnprogressbar_t)(int step, int numstep, int64_t curr, int64_t total, int msg);

/**
 * Glyph cache structure, private (same as ssfn_glyph_t but with larger bit widths)
 */
typedef struct {
    uint16_t p;                         /* data buffer pitch, bytes per line */
    uint16_t h;                         /* data buffer height */
    uint16_t o;                         /* overlap of glyph, scaled to size */
    uint16_t x;                         /* advance x, scaled to size */
    uint16_t y;                         /* advance y, scaled to size */
    uint16_t a;                         /* ascender, scaled to size */
    uint16_t d;                         /* descender, scaled to size */
    uint8_t data[(260 + 260 / SSFN_ITALIC_DIV) << 8];        /* data buffer */
} sfngc_t;

/**
 * Fragments structure, private
 */
typedef struct {
    unsigned char type;
    int w, h;
    int pos;
    int len;
    int cnt;
    int idx;
    unsigned char *data;
} sfnfrag_t;

/**
 * Kerning group, private
 */
typedef struct {
    int first;
    int last;
    int idx;
} sfnkgrp_t;

/**
 * Kerning position, private
 */
typedef struct {
    int idx;
    int pos;
    int len;
    unsigned char *data;
} sfnkpos_t;


/**
 * Contour command
 */
typedef struct {
    unsigned char type;         /* one of SSFN_CONTOUR_x defines */
    unsigned char px;           /* on curve point coordinates */
    unsigned char py;
    unsigned char c1x;          /* control point #1 coordinates */
    unsigned char c1y;
    unsigned char c2x;          /* control point #2 coordinates */
    unsigned char c2y;
} sfncont_t;

/**
 * One glyph layer
 */
typedef struct {
    unsigned char type;         /* one of SSFN_FRAG_x defines */
    unsigned char color;        /* color palette index, 0xFE foreground, 0xFF background */
    int len, miny, minx;        /* private properties, len = sfncont_t array length */
    unsigned char *data;        /* either color index map, or sfncont_t array */
} sfnlayer_t;

/**
 * One kerning relation
 */
typedef struct {
    int n;                      /* next code point in relation */
    char x;                     /* relative horizontal offset */
    char y;                     /* relative vertical offset */
} sfnkern_t;

/**
 * One glyph
 */
typedef struct {
    unsigned char width;        /* glyph width */
    unsigned char height;       /* glyph height */
    unsigned char ovl_x;        /* overlay x */
    unsigned char adv_x;        /* horizontal advance */
    unsigned char adv_y;        /* vertical advance */
    unsigned char numlayer;     /* number of layers */
    sfnlayer_t *layers;         /* the layers */
    int numkern, kgrp;          /* private, number of kerning relations and groups */
    sfnkern_t *kern;            /* kerning relations array */
    unsigned char rtl, hintv[33], hinth[33]; /* right-toleft flag and hinting */
    int numfrag;                /* private, used by sfn_save() */
    int *frags;
} sfnglyph_t;

/**
 * In-memory font structure
 */
typedef struct {
    unsigned char family;       /* font family type, see sfn_setfamilytype() */
    unsigned char style;        /* ORd SSFN_STYLE_x defines */
    unsigned char width;        /* overall font width */
    unsigned char height;       /* overall font height */
    unsigned char baseline;     /* baseline (ascent) */
    unsigned char underline;    /* underline position */
    char *filename;             /* filename */
    char *name;                 /* unique name of the font */
    char *familyname;           /* font family name */
    char *subname;              /* font family sub-name */
    char *revision;             /* font revision / version string */
    char *manufacturer;         /* font designer / creator / foundry */
    char *license;              /* font license */
    sfnglyph_t glyphs[0x110000];/* glyphs array */
    int numcpal;                /* number of color palette entries */
    unsigned char cpal[1024];   /* color palette */
    char *ligatures[SSFN_LIG_LAST-SSFN_LIG_FIRST+1]; /* ligatures, UTF-8 strings for U+F000 .. U+F8FF */
    int numskip;                /* code points to skip on load, see sfn_skipadd() */
    int *skip;
    int numfrags;               /* private, used by sfn_save() and sfn_glyph() */
    sfnfrag_t *frags;
    int numkpos;
    sfnkpos_t *kpos;
    long int total;
    uint16_t *p;
    int np, ap, mx, my, lx, ly;
} sfnctx_t;
/**
 * The global font context
 */
extern sfnctx_t ctx;
/**
 * Progress bar callback
 */
extern sfnprogressbar_t pbar;

/*** arguments ***/
extern int rs, re, replace, skipundef, skipcode, hinting, adv, relul, rasterize, dump, origwh;
extern int unicode, lastuni, quiet, dorounderr;

/*** sfn.c ***/
void sfn_init(sfnprogressbar_t pb);
void sfn_setfamilytype(int t);
void sfn_setstr(char **s, char *n, int len);
void sfn_chardel(int unicode);
int sfn_charadd(int unicode, int w, int h, int ax, int ay, int ox);
sfnlayer_t *sfn_layeradd(int unicode, int t, int x, int y, int w, int h, int c, unsigned char *data);
void sfn_layerdel(int unicode, int idx);
int sfn_contadd(sfnlayer_t *lyr, int t, int px, int py, int c1x, int c1y, int c2x, int c2y);
int sfn_kernadd(int unicode, int next, int x, int y);
void sfn_hintgen(int unicode);
unsigned char sfn_cpaladd(int r, int g, int b, int a);
void sfn_skipadd(int unicode);
int sfn_load(char *filename);
int sfn_save(char *filename);
void sfn_sanitize(int unicode);
int sfn_glyph(int size, int unicode, int layer, int postproc, sfngc_t *g);
void sfn_rasterize(int size);
void sfn_vectorize();
void sfn_coverage();
void sfn_free();

unsigned int gethex(char *ptr, int len);

#endif
