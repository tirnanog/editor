ZStandard
=========

This is a heavily stripped down version of [zstd](https://github.com/facebook/zstd) with only the decompressor.

Compilation
-----------

```
make libzstd.a ZSTD_LIB_COMPRESSION=0 ZSTD_LEGACY_SUPPORT=0
```
