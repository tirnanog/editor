/*
 * tnge/license.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Generate end user licenses for games
 *
 */

#include <time.h>
#include "main.h"

void license_copy(void *data);
void license_generate(void *data);
ui_box_t license_box1, license_box2;
char license_id[64], license_total[256];
void license_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

ui_tablehdr_t license_table_hdr[] = {
    { -1, 60, 0, 0 },
    { LIC_ENDUSER, 0, 0, 0 },
    { 0 }
};
ui_table_t license_table = { license_table_hdr, LIC_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), license_drawcell,
    NULL, NULL, NULL };

/**
 * The form
 */
ui_form_t license_form[] = {
    { FORM_BOX, 10, 30, 0, 42, 0, 0, &license_box1, NULL },
    { FORM_BOX, 11, 31, 300, 20, 0, 0, &license_box2, NULL },
    { FORM_BOX, 311, 31, 0, 20, 0, 0, &license_box2, NULL },
    { FORM_COMMENT, 18, 31, 300, 16, 0, 0, NULL, NULL },
    { FORM_COMMENT, 319, 31, 0, 16, 0, 0, NULL, NULL },
    { FORM_TEXT, 15, 51, 300, 16, 0, 0, license_id, NULL },
    { FORM_TEXT, 316, 51, 0, 16, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 74, 0, 16, 0, 0, NULL, NULL },
    { FORM_TABLE, 10, 99, 0, 0, 0, 0, &license_table, license_copy },
    { FORM_BUTTONICN, 24, 0, 20, 20, 0, LIC_COPY, (void*)ICON_COPY, license_copy },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, license_generate },
    { FORM_TEXT, 0, 0, 0, 16, 0, 0, license_total, NULL },
    { FORM_LAST }
};

/**
 * Draw table cell
 */
void license_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    char tmp[16];
    char *str = *((char**)data);
    (void)sel; (void)w; (void)h;
    sprintf(tmp, "%6d.", idx + 1);
    ui_text(dst, x + 4, y, tmp);
    ui_text(dst, x + 4 + hdr[0].w, y, str);
}

/**
 * Exit window
 */
void license_exit(int tab)
{
    (void)tab;
    project_freedir((char***)&license_table.data, &license_table.num);
    license_table.val = license_table.scr = 0;
    license_table.clk = -1;
}

/**
 * Enter window
 */
void license_init(int tab)
{
    FILE *f;
    unsigned char key[8], key2[8];
    char tmp[256], **liclist = NULL;
    int i, failedkeys = 0;

    license_exit(tab);
    if(!project.author[0]) return;
    project_encbase(key);
    strcpy(projfn, project.id); strcat(projfn, SEP "endusers.txt");
    f = project_fopen(projdir, "r");
    if(f) {
        while(!feof(f)) {
            memset(tmp, 0, sizeof(tmp));
            if(fgets(tmp, sizeof(tmp)-1, f)) {
                project_decode(tmp, key2);
                if(!memcmp(key, key2, 4)) {
                    liclist = (char**)realloc(liclist, (license_table.num + 2) * sizeof(char*));
                    if(!liclist) main_error(ERR_MEM);
                    liclist[license_table.num] = (char*)main_alloc(16);
                    memcpy(liclist[license_table.num++], tmp, 14);
                    liclist[license_table.num] = NULL;
                } else
                    failedkeys++;
            }
        }
        fclose(f);
    }
    if(failedkeys) {
        f = project_fopen(projdir, "wb+");
        if(f) {
            for(i = 0; i < license_table.num; i++)
                fprintf(f, "%s\r\n", liclist[i]);
            fclose(f);
        }
    }
    license_table.data = liclist;
    if(!lang) return;
    license_box1.l = theme[THEME_DARKER]; license_box1.b = theme[THEME_INPBG]; license_box1.d = theme[THEME_LIGHTER];
    license_box2.l = theme[THEME_LIGHTER]; license_box2.b = theme[THEME_BG]; license_box2.d = theme[THEME_DARKER];
    license_form[3].param = lang[LIC_ID];
    license_form[4].param = lang[LIC_OWNER];
    license_form[6].param = project.author;
    license_form[7].param = lang[LIC_COMMENT];
    license_form[7].w = ui_textwidth(lang[LIC_COMMENT]);
    license_form[10].param = lang[LIC_GENERATE];
    license_form[10].w = ui_textwidth(lang[LIC_GENERATE]) + 40;
    if(license_form[10].w < 200) license_form[10].w = 200;
    sprintf(license_id, "%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X%02X%02X", (uint8_t)project.uuid[0],
        (uint8_t)project.uuid[1], (uint8_t)project.uuid[2], (uint8_t)project.uuid[3], (uint8_t)project.uuid[4],
        (uint8_t)project.uuid[5], (uint8_t)project.uuid[6], (uint8_t)project.uuid[7], (uint8_t)project.uuid[8],
        (uint8_t)project.uuid[9], (uint8_t)project.uuid[10], (uint8_t)project.uuid[11], (uint8_t)project.uuid[12],
        (uint8_t)project.uuid[13], (uint8_t)project.uuid[14], (uint8_t)project.uuid[15]);
    sprintf(license_total, "%s: %d", lang[LIC_TOTAL], license_table.num);
    license_form[11].w = ui_textwidth(license_total) + 10;
}

/**
 * Resize the view
 */
void license_resize(int tab, int w, int h)
{
    (void)tab;
    license_form[0].w = w - 20;
    license_form[2].w = license_form[4].w = license_form[6].w = w - 322;
    license_form[7].x = (w - license_form[7].w) / 2;
    ui_table_resize(&license_form[8], w - 20, h - 159);
    license_form[9].y = license_form[10].y = license_form[11].y = h - 48;
    license_form[10].x = w - 20 - license_form[10].w;
    license_form[11].x = (w - license_form[11].w) / 2;
}

/**
 * View layer
 */
void license_redraw(int tab)
{
    (void)tab;
    ui_form = license_form;
    license_form[9].inactive = license_table.val < 0;
}

/**
 * Copy license key to clipboard
 */
void license_copy(void *data)
{
    char **liclist = (char**)license_table.data;

    (void)data;
    SDL_SetClipboardText(liclist[license_table.val]);
    ui_status(0, lang[LIC_COPIED]);
}

/**
 * Generate end user license keys
 */
void license_generate(void *data)
{
    char **liclist = (char**)license_table.data, *tmp, stat[256];
    unsigned char key[8];
    int i;
    FILE *f;

    (void)data;
    if(!project.author[0]) return;
    strcpy(projfn, project.id); strcat(projfn, SEP "endusers.txt");
    srand(time(NULL));
    f = project_fopen(projdir, "ab+");
    if(!f) {
        if(lang) ui_status(1, lang[ERR_SAVELIC]);
    } else {
        project_encbase(key);
        while(1) {
            tmp = project_encode(key);
            if(!tmp) main_error(ERR_MEM);
            for(i = 0; i < license_table.num && memcmp(liclist[i], tmp, 14); i++);
            if(i >= license_table.num) {
                if(lang) {
                    SDL_SetClipboardText(tmp);
                    if(verbose) printf("license_generate: generated end user decryption key: %s\r\n", tmp);
                } else
                    printf("%s\n", tmp);
                liclist = (char**)realloc(liclist, (license_table.num + 1) * sizeof(char*));
                if(!liclist) main_error(ERR_MEM);
                license_table.data = liclist;
                license_table.val = license_table.num;
                liclist[license_table.num++] = tmp;
                fwrite(tmp, strlen(tmp), 1, f);
                fwrite("\r\n", 2, 1, f);
                break;
            }
            free(tmp);
        }
        fclose(f);
        if(lang) {
            sprintf(license_total, "%s: %d", lang[LIC_TOTAL], license_table.num);
            sprintf(stat, "%s %s", projfn, lang[LANG_SAVED]);
            ui_status(0, stat);
        }
    }
}
