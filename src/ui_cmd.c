/*
 * tnge/ui_cmd.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Commands, scripting widget
 *
 */

#include "main.h"

#define CMD_TIMEMAX     6000
#define CMD_NUMMAX      99999
#define CMD_NUMALERT    15

/* command palettes */
int ui_cmd_start[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 42, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
    43, 31, 32, 34, 35, 36, 37, 38, 39, 40, 41 };
int ui_cmd_timer[] = { 1, 2, 3, 4, 6, 8, 9, 18, 15, 31, 32, 16 };
int ui_cmd_cutscene[] = { 1, 6, 37, 8, 12, 36, 17, 18, 15, 21, 22, 23, 24, 25, 26, 27, 28, 31, 32, 16, 41 };
int ui_cmd_action[] = { 1, 2, 3, 4, 5, 6, 8, 9, 15, 42 };
int ui_cmd_attr[] = { 1, 2, 3, 4, 5, 6, 8, 9, 11, 15, 42 };
int ui_cmd_objevt[] = { 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 42, 13, 15, 16, 20, 40, 29, 30, 33, 34, 35, 36, 37, 31, 32, 41, 43 };
int ui_cmd_npcevt[] = { 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 42, 13, 14, 16, 17, 18, 19, 20, 40, 31, 32, 33, 34, 35, 41 };
int ui_cmd_price[] = { 1, 2, 3, 4 };
int ui_cmd_quest[] = { 1, 2, 3, 4, 10, 11, 12, 42, 13, 31, 32, 33, 16, 34, 35, 36, 41 };

typedef struct {
    int icon;
    char *name;
    int par[4];
    int stat[3];
} cmdtypes_t;

/**
 * Command types. The first 5 are special: control structures, different colors or unusal argument order.
 * Others only depend on definitions in this list only, and there are a few rules:
 * FORM_EXPR, FORM_MAPSEL can only be the first argument
 * select options (FORM_SNDSEL, FORM_CUTSEL etc.) can only be the second
 * FORM_NUM and FORM_TIME only the third
 */
cmdtypes_t cmd_types[] = {
    /* 0 - 4, special hardcoded, do not change */
    { 0 }, /* NOP */
    { ICON_INSCR,   "//",       { FORM_TEXT, 0, 0, 0 }, { 0, 0, 0 } },                          /* CMD_CMNT */
    { ICON_LOOP,    "while",    { FORM_EXPR, 0, 0, 0 }, { 0, 0, 0 } },                          /* CMD_ITER */
    { ICON_SWITCH,  "switch",   { FORM_EXPR, 0, 0, 0 }, { 0, 0, 0 } },                          /* CMD_COND */
    { ICON_LET,     "=",        { FORM_EXPR, FORM_CMDVAR, 0, 0 }, { 0, 0, 0 } },                /* CMD_LET */
    /* 5 */
    { ICON_END,     "exit",     { 0, 0, 0, 0 }, { 0, 0, 0 } },                                  /* CMD_END */
    { ICON_DELAY,   "delay",    { 0, 0, FORM_TIME, 0 }, { 0, 0, CMD_STAT_DLY } },               /* CMD_DELAY */
    { ICON_MUS,     "music",    { 0, FORM_MUSSEL, 0, 0 }, { 0, 0, 0 } },                        /* CMD_MUS */
    { ICON_SND,     "sound",    { 0, FORM_SNDSEL, 0, 0 }, { 0, 0, 0 } },                        /* CMD_SND */
    { ICON_SPCH,    "speak",    { 0, FORM_SPCSEL, 0, 0 }, { 0, 0, 0 } },                        /* CMD_SPC */
    /* 10 */
    { ICON_CHSR,    "chooser",  { 0, FORM_CHRSEL, 0, 0 }, { 0, 0, 0 } },                        /* CMD_CHR */
    { ICON_CUTSC,   "cutscene", { 0, FORM_CUTSEL, 0, 0 }, { 0, 0, 0 } },                        /* CMD_CUT */
    { ICON_DLG,     "dialog",   { 0, FORM_DLGSEL, FORM_TIME, 0 }, { 0, 0, CMD_STAT_TOUT } },    /* CMD_DLG */
    { ICON_MAP,     "location", { FORM_MAPSEL, FORM_DIRSEL, 0, 0 }, { 0, 0, 0 } },              /* CMD_MAP */
    { ICON_QUEST,   "quest",    { FORM_BOOL, FORM_QSTSEL, 0, 0 }, { CMD_STAT_QST2, 0, 0 } },    /* CMD_QUEST */
    { ICON_PLAYC,   "canim",    { 0, FORM_ACNSEL, FORM_TIME, 0 }, { 0, 0, CMD_STAT_PLAY } },    /* CMD_CANIM character anim */
    { ICON_PLAYO,   "oanim",    { 0, FORM_SPRITE, FORM_TIME, 1 }, { 0, 0, CMD_STAT_PLAY } },    /* CMD_OANIM object anim */
    /* 17 */
    { ICON_NPCMAP,  "npc",      { FORM_MAPSEL, FORM_NPCSEL, 0, 0 }, { 0, 0, 0 } },              /* CMD_NPCADD */
    { ICON_NPC,     "actor",    { 0, FORM_NPCSEL, 0, 0 }, { 0, 0, 0 } },                        /* CMD_NPCSEL */
    { ICON_BATTLE,  "behave",   { 0, FORM_BEHSEL, 0, 0 }, { 0, 0, 0 } },                        /* CMD_NPCBEH */
    { ICON_MARKET,  "market",   { 0, 0, 0, 0 }, { 0, 0, 0 } },                                  /* CMD_NPCINV */
    /* 21 */
    { ICON_S,       "south",    { 0, 0, FORM_NUM, 0 }, { 0, 0, CMD_STAT_MOVE } },               /* CMD_NPCS */
    { ICON_SW,      "southwest",{ 0, 0, FORM_NUM, 0 }, { 0, 0, CMD_STAT_MOVE } },               /* CMD_NPCSW */
    { ICON_W,       "west",     { 0, 0, FORM_NUM, 0 }, { 0, 0, CMD_STAT_MOVE } },               /* CMD_NPCW */
    { ICON_NW,      "northwest",{ 0, 0, FORM_NUM, 0 }, { 0, 0, CMD_STAT_MOVE } },               /* CMD_NPCNW */
    { ICON_N,       "north",    { 0, 0, FORM_NUM, 0 }, { 0, 0, CMD_STAT_MOVE } },               /* CMD_NPCN */
    { ICON_NE,      "northeast",{ 0, 0, FORM_NUM, 0 }, { 0, 0, CMD_STAT_MOVE } },               /* CMD_NPCNE */
    { ICON_E,       "east",     { 0, 0, FORM_NUM, 0 }, { 0, 0, CMD_STAT_MOVE } },               /* CMD_NPCE */
    { ICON_SE,      "southeast",{ 0, 0, FORM_NUM, 0 }, { 0, 0, CMD_STAT_MOVE } },               /* CMD_NPCSE */
    /* 29 */
    { ICON_OBJREM,  "remove",   { 0, 0, 0, 0 }, { 0, 0, 0 } },                                  /* CMD_REMOVE */
    { ICON_OBJADD,  "drop",     { 0, FORM_OBJSEL, 0, 0 }, { 0, 0, 0 } },                        /* CMD_DROP */
    { ICON_OBJREM,  "delete",   { FORM_MAPSEL, 0, 0, 0 }, { 0, 0, 0 } },                        /* CMD_DELETE */
    { ICON_OBJADD,  "add",      { FORM_MAPSEL, FORM_SPRITE, 0, 1 }, { 0, CMD_STAT_OBJ, 0 } },   /* CMD_ADD */
    { ICON_OBJREPL, "replace",  { 0, FORM_OBJSEL, 0, 2 }, { 0, 0, 0 } },                        /* CMD_REPLACE */
    { ICON_GIVE,    "give",     { 0, FORM_OBJSEL, FORM_NUM, 1 }, { 0, 0, CMD_STAT_AMOUNT } },   /* CMD_GIVE */
    { ICON_TAKE,    "take",     { 0, FORM_OBJSEL, FORM_NUM, 1 }, { 0, 0, CMD_STAT_AMOUNT } },   /* CMD_TAKE */
    { ICON_EVENT,   "event",    { 0, FORM_OBJSEL, 0, 0 }, { 0, 0, 0 } },                        /* CMD_EVENT */
    /* 37 */
    { ICON_WALK,    "transport",{ 0, FORM_TRNSEL, 0, 0 }, { 0, 0, 0 } },                        /* CMD_TRANSPORT */
    { ICON_MAP,     "scene",    { FORM_MAPSEL, FORM_CLKSEL, 0, 0 }, { 0, 0, 0 } },              /* CMD_SCENE */
    { ICON_WAITC,   "waitnpc",  { 0, 0, 0, 0 }, { 0, 0, 0 } },                                  /* CMD_WAITNPC */
    { ICON_CRAFT,   "craft",    { 0, FORM_CFTSEL, 0, 0 }, { 0, 0, 0 } },                        /* CMD_CRAFT */
    { ICON_QMARK,   "match",    { FORM_MAPSEL, FORM_SPRITE, 0, 1 }, { 0, CMD_STAT_OBJ, 0 } },   /* CMD_MATCH */
    { ICON_ALERT,   "alert",    { 0, FORM_ALERT, 0, 0 }, { 0, 0, 0 } },                         /* CMD_ALERT */
    /* 43 */
    { ICON_ALTIT,   "altitude", { 0, 0, FORM_ALTITUDE, 0 }, { 0, 0, 0 } }                       /* CMD_ALTITUDE */
};

/* local variables */
char *ui_cmd_locals[] = { " ", "@", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q",
    "r", "s", "t", "u", "v", "w", "x", "y", "z", NULL };

/* directions */
char *ui_cmd_dirs[] = { "\x11", "\x12", "\x13", "\x14", "\x15", "\x16", "\x17", "\x18", "\xc2\x9f", NULL };

ui_select_t ui_cmd_sel = { 0 };
ui_input_t ui_cmd_inp = { 0 };
ui_sprsel_t ui_cmd_spr = { 0 };
ui_map_t ui_cmd_map = { 0 };
uint32_t ui_cmd_color;
int ui_cmd_lastmap;

/**
 * Dump the command list tree, only for debugging
 */
void ui_cmd_dump(cmd_t *parent, cmd_t *node, int level)
{
    int i;

    for(i = 0; i < level; i++) printf("  ");
    printf("%s%p type %d w %d h %d aw[%d,%d,%d] err %d numchildren %d\n",parent == node ? ">" : " ", (void*)parent,
        parent->type,parent->w,parent->h,parent->aw[0],parent->aw[1],parent->aw[2],parent->err,parent->len);
    for(i = 0; i < parent->len; i++)
        ui_cmd_dump(&parent->children[i], node, level + 1);
}

/**
 * Translate palette index and get color
 */
int ui_cmdpal_get(int type, int i, uint32_t *l, uint32_t *b, uint32_t *d)
{
    switch(type) {
        case CMDPAL_START: i = ui_cmd_start[i]; break;
        case CMDPAL_TIMER: i = ui_cmd_timer[i]; break;
        case CMDPAL_ATTR: i = ui_cmd_attr[i]; break;
        case CMDPAL_ACTION: i = ui_cmd_action[i]; break;
        case CMDPAL_CUTSCENE: i = ui_cmd_cutscene[i]; break;
        case CMDPAL_OBJEVT: i = ui_cmd_objevt[i]; break;
        case CMDPAL_NPCEVT: i = ui_cmd_npcevt[i]; break;
        case CMDPAL_PRICE: i = ui_cmd_price[i]; break;
        case CMDPAL_QUEST: i = ui_cmd_quest[i]; break;
    }
    if(b) *b = theme[i > 5 ? THEME_FUNC : THEME_CMNT + i - 1];
    if(l && b) *l = (*b + 0x101010) & 0x7F7F7F;
    if(d && b) *d = (*b - 0x101010) & 0x7F7F7F;
    return i;
}

/**
 * Display a command palette
 */
void ui_cmdpal(SDL_Texture *dst, int x, int y, int h, ui_cmdpal_t *pal, int pressed)
{
    uint32_t l, b, d;
    int i, o = ui_clip.h, idx;

    switch(pal->type) {
        case CMDPAL_START: pal->num = (int)sizeof(ui_cmd_start)/(int)sizeof(int); break;
        case CMDPAL_TIMER: pal->num = (int)sizeof(ui_cmd_timer)/(int)sizeof(int); break;
        case CMDPAL_ATTR: pal->num = (int)sizeof(ui_cmd_attr)/(int)sizeof(int); break;
        case CMDPAL_ACTION: pal->num = (int)sizeof(ui_cmd_action)/(int)sizeof(int); break;
        case CMDPAL_CUTSCENE: pal->num = (int)sizeof(ui_cmd_cutscene)/(int)sizeof(int); break;
        case CMDPAL_OBJEVT: pal->num = (int)sizeof(ui_cmd_objevt)/(int)sizeof(int); break;
        case CMDPAL_NPCEVT: pal->num = (int)sizeof(ui_cmd_npcevt)/(int)sizeof(int); break;
        case CMDPAL_PRICE: pal->num = (int)sizeof(ui_cmd_price)/(int)sizeof(int); break;
        case CMDPAL_QUEST: pal->num = (int)sizeof(ui_cmd_quest)/(int)sizeof(int); break;
        default: pal->num = (int)sizeof(cmd_types)/(int)sizeof(cmd_types[0]); break;
    }
    ui_box2(dst, x, y, 24 + 2 + 12, h, theme[THEME_DARKER], theme[THEME_INPBG], theme[THEME_LIGHTER], 0, pal->scr, 24, 0);
    ui_vscrbar(dst, x + 24 + 1, y + 1, 12, h - 2, pal->scr, (h - 2) / 24, pal->num, pressed);
    y += 3; ui_clip.h = y + h - 5;
    for(i = pal->scr; i < pal->num && y < ui_clip.h; i++, y += 24) {
        idx = ui_cmdpal_get(pal->type, i, &l, &b, &d);
        ui_box(dst, x + 3, y, 20, 20, l, b, d);
        ui_icon(dst, x + 5, y + 2, cmd_types[idx].icon);
    }
    ui_clip.h = o;
}

/**
 * Command palette mouse down handler
 */
void ui_cmdpal_drag(ui_form_t *elem, int x, int y)
{
    uint32_t l, b, d;
    ui_cmdpal_t *pal;
    int i;

    if(!elem || !elem->param || x < 0 || y < 0) return;
    pal = elem->param;
    i = (y - 1) / 24 + pal->scr;
    if(i > pal->num - 1) return;
    i = ui_cmdpal_get(pal->type, i, &l, &b, &d);
    ui_dnd_drag(i, NULL, cmd_types[i].icon, l, b, d);
}

/**
 * Display a variable selector
 */
void ui_cmdvar(SDL_Texture *dst, int x, int y, int w, ui_cmdvar_t *var, int pressed)
{
    uint32_t f = ssfn_dst.fg;
    int p;

    if(pressed == -1)
        ui_box(dst, x, y, 16, 18, theme[THEME_INA], theme[THEME_INA], theme[THEME_INA]);
    else
        ui_rect(dst, x, y, 16, 18, theme[pressed == 1 ? THEME_DARKER : THEME_LIGHTER],
            theme[pressed == 1 ? THEME_LIGHTER : THEME_DARKER]);
    ssfn_dst.w = ui_clip.x + ui_clip.w;
    ssfn_dst.h = ui_clip.y + ui_clip.h;
    if(dst == screen) { p = scr_p; ssfn_dst.ptr = (unsigned char *)scr_data; }
    else SDL_LockTexture(dst, NULL, (void**)&ssfn_dst.ptr, &p);
    ssfn_dst.p = p;
    ssfn_dst.x = x + 4;
    ssfn_dst.y = y + (pressed == 1 ? 1 : 0);
    ssfn_dst.fg = theme[pressed == -1 ? THEME_LIGHT : THEME_FG];
    if(ssfn_dst.x >= ui_clip.x)
        ssfn_putc(ui_cmd_locals[var->type][0]);
    ssfn_dst.fg = f;
    if(dst != screen) SDL_UnlockTexture(dst);
    if(pressed == -1 || var->type > 2) return;
    p = ui_clip.w; ui_clip.w = x + w - 2;
    ui_text(dst, x + 20, y + (pressed == 2 ? 1 : 0), project.attrs[var->attr]);
    ui_clip.w = p;
    ui_rect(dst, x + 18, y, w - 18, 18, theme[pressed == 2 ? THEME_DARKER : THEME_LIGHTER],
            theme[pressed == 2 ? THEME_LIGHTER : THEME_DARKER]);
}

/**
 * Variable selector mouse down handler
 */
int ui_cmdvar_click(ui_form_t *elem, int x, int y)
{
    ui_cmdvar_t *var;

    if(!elem || !elem->param || x < 0 || y < 0) return 0;
    var = elem->param;
    if(x < 17) {
        var->sel.opts = ui_cmd_locals;
        var->sel.val = var->type;
        ui_popup_page = 1;
    } else
    if(x > 17 && var->type < 2) {
        var->sel.opts = project.attrs;
        var->sel.val = var->attr;
        ui_popup_page = 2;
    }
    return 0;
}

/**
 * Recursively display commands in script
 */
void _ui_cmd(SDL_Texture *dst, int x, int y, cmd_t *node, cmd_t *clknode, int clk)
{
    uint32_t l, b, d, cl, cd;
    char tmp[16];
    int i, p, a = 0, w;

    if(!dst || !node) return;

    if(node->err)
        b = theme[THEME_ERR];
    else
        switch(node->type) {
            case 1: b = theme[THEME_CMNT]; break;
            case 2: b = theme[THEME_LOOP]; break;
            case 3: b = theme[THEME_SWITCH]; break;
            case 4: b = theme[THEME_LET]; break;
            default:b = theme[THEME_FUNC]; break;
        }
    l = (b + 0x101010) & 0x7F7F7F;
    d = (b - 0x101010) & 0x7F7F7F;

    switch(node->type) {
        case 0:     /* block */
            if(node->children) {
                for(i = 0, a = 0; i < node->len; i++) {
                    _ui_cmd(dst, x, y + a, &node->children[i], clknode, clk);
                    a += node->children[i].h;
                }
            }
        break;
        case 1:     /* comment */
            ui_box(dst, x + 1, y + 1, node->w - 2, 21, b, b, b);
            ui_rect(dst, x, y, node->w, node->h, l, d);
            ui_icon(dst, x + 2, y + 3, cmd_types[1].icon);
            if(node->err) ui_error(x + 2, y + 3);
            if(clknode == node && clk) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
            ui_text(dst, x + 22, y + p, node->arg1);
            ui_rect(dst, x + 20, y + 2, node->w - 22, 18, cl, cd);
        break;
        case 2:     /* loop */
            ui_box(dst, x + 1, y + 1, node->w - 2, 21, b, b, b);
            ui_rect(dst, x, y, node->w, node->h, l, d);
            ui_icon(dst, x + 2, y + 3, cmd_types[2].icon);
            if(node->err) ui_error(x + 2, y + 3);
            if(clknode == node && clk) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
            ui_text(dst, x + 22, y + p, node->arg1);
            ui_rect(dst, x + 20, y + 2, node->w - 22, 18, cl, cd);
            if(node->children) {
                for(i = 0, a = 24; i < node->len; i++) {
                    _ui_cmd(dst, x + 3, y + a, &node->children[i], clknode, clk);
                    a += node->children[i].h;
                }
            }
        break;
        case 3:     /* switch */
            ui_box(dst, x + 1, y + 1, node->w - 2, 23, b, b, b);
            for(i = a = 0; i < node->len; i++) {
                sprintf(tmp, "%u", node->len - i - 1);
                ui_number(dst, x + a + 2 + node->children[i].w / 2, y + 18, tmp, theme[THEME_FG]);
                ui_rect(dst, x + a, y + 23, node->children[i].w + 6, node->h - 23, b, b);
                _ui_cmd(dst, x + a + 3, y + 24, &node->children[i], clknode, clk);
                a += node->children[i].w + 5;
            }
            ui_rect(dst, x, y, node->w, node->h, l, d);
            ui_icon(dst, x + 2, y + 2, cmd_types[3].icon);
            if(node->err) ui_error(x + 2, y + 2);
            if(clknode == node && clk == 1) { cl = d; cd = l; p = 1; } else { cl = l; cd = d; p = 0; }
            ssfn_dst.fg = l; ui_text(dst, x + 24, y + p + 1, "+");
            ssfn_dst.fg = d; ui_text(dst, x + 23, y + p, "+");
            ui_rect(dst, x + 20, y + 2, 16, 16, cl, cd);
            if(node->len > 2) {
                if(clknode == node && clk == 2) { cl = d; cd = l; p = 1; } else { cl = l; cd = d; p = 0; }
                ssfn_dst.fg = l; ui_text(dst, x + 45, y + p + 1, "-");
            } else {
                cl = cd = d; p = 0;
            }
            ssfn_dst.fg = d; ui_text(dst, x + 44, y + p, "-");
            ssfn_dst.fg = theme[THEME_FG];
            ui_rect(dst, x + 40, y + 2, 16, 16, cl, cd);
            if(clknode == node && clk == 3) { cl = d; cd = l; p = 2; } else { cl = l; cd = d; p = 1; }
            ui_text(dst, x + 62, y + p, node->arg1);
            ui_rect(dst, x + 60, y + 2, node->w - 62, 16, cl, cd);
        break;
        case 4:     /* let */
            ui_box(dst, x, y, node->w, 22, l, b, d);
            if(clknode == node && clk == 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
            ui_text(dst, x + 4, y + p, ui_cmd_locals[node->arg2]);
            ui_rect(dst, x + 2, y + 2, 12, 18, cl, cd);
            if(node->arg2 < 2 && project.attrs) {
                if(clknode == node && clk == 2) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                ui_text(dst, x + 15, y + p, project.attrs[node->arg3]);
                ui_rect(dst, x + 14, y + 2, node->aw[1], 18, cl, cd);
                a = node->aw[1];
            } else
                a = 0;
            ui_icon(dst, x + 13 + a, y + 3, cmd_types[4].icon);
            if(node->err) ui_error(x + 13 + a, y + 3);
            if(clknode == node && clk == 3) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
            ui_text(dst, x + 32 + a, y + p, node->arg1);
            ui_rect(dst, x + 30 + a, y + 2, node->w - 32 - a, 18, cl, cd);
        break;
        default:    /* everything else is the same */
            ui_box(dst, x, y, node->w, 22, l, b, d);
            ui_icon(dst, x + 2, y + 3, cmd_types[node->type].icon);
            if(node->err) ui_error(x + 2, y + 3);
            ui_input_bg = b;
            for(i = 0, a = 20; i < 3; i++) {
                switch(cmd_types[node->type].par[i]) {
                    case FORM_EXPR:
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        ui_text(dst, x + a + 2, y + p, node->arg1);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_MUSSEL:
                        if(node->arg2 >= project.nummusic) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        if(project.music) ui_text(dst, x + a + 2, y + p, project.music[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_SNDSEL:
                        if(node->arg2 >= project.numsounds) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        if(project.sounds) ui_text(dst, x + a + 2, y + p, project.sounds[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_SPCSEL:
                        if(node->arg2 >= project.numspeech) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        if(project.speech) ui_text(dst, x + a + 2, y + p, project.speech[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_CHRSEL:
                        if(node->arg2 >= project.numchoosers) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        if(project.choosers) ui_text(dst, x + a + 2, y + p, project.choosers[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_CUTSEL:
                        if(node->arg2 >= project.numcuts) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        if(project.cuts) ui_text(dst, x + a + 2, y + p, project.cuts[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_NPCSEL:
                        if(node->arg2 >= project.spritenum[PROJ_NUMSPRCATS - 2]) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        if(project.sprites[PROJ_NUMSPRCATS - 2])
                            ui_text(dst, x + a + 2, y + p, project.sprites[PROJ_NUMSPRCATS - 2][node->arg2].name);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_DLGSEL:
                        if(node->arg2 >= project.numdialogs) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        if(project.dialogs) ui_text(dst, x + a + 2, y + p, project.dialogs[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_OBJSEL:
                        if(node->arg2 < 0 || node->arg2 >= project.spritenum[1] ||
                          (project.sprites[1] && !(project.sprites[1][node->arg2].y & 0x10000))) {
                            for(p = 0; project.sprites[1] && p < project.spritenum[1] && !(project.sprites[1][p].y & 0x10000); p++);
                            node->arg2 = p >= project.spritenum[1] ? 0 : p;
                        }
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        if(project.sprites[1]) ui_text(dst, x + a + 2, y + p, project.sprites[1][node->arg2].name);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_CFTSEL:
                        if(node->arg2 >= project.numcrafts) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        if(project.crafts) ui_text(dst, x + a + 2, y + p, project.crafts[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_MAPSEL:
                        if(node->arg4 >= project.nummaps) node->arg4 = 0;
                        if(cmd_types[node->type].icon == ICON_NPCMAP)
                            node->arg4 = ui_cmd_lastmap;
                        else
                            ui_cmd_lastmap = node->arg4;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        w = ui_clip.w; ui_clip.w = x + a + node->aw[i] - 2 - ui_clip.x;
                        if(project.maps) ui_text(dst, x + a + 2, y + p, project.maps[node->arg4]);
                        ui_clip.w = w;
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_DIRSEL:
                        if(node->arg2 >= 9) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        ui_text(dst, x + a + 2, y + p, ui_cmd_dirs[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_QSTSEL:
                        if(node->arg2 >= project.numquests) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        if(project.quests) ui_text(dst, x + a + 2, y + p, project.quests[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_BEHSEL:
                        if(node->arg2 > 2) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        ui_text(dst, x + a + 2, y + p, project_behs[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_TRNSEL:
                        if(node->arg2 > 2) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        ui_text(dst, x + a + 2, y + p, project_trns[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_CLKSEL:
                        if(node->arg2 > 23) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        ui_text(dst, x + a + 2, y + p, project_clock[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_ACNSEL:
                        if(node->arg2 > 15) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        ui_text(dst, x + a + 2, y + p, project_actions[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_SPRITE:
                        if(node->arg2 >= project.spritenum[cmd_types[node->type].par[3]]) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        if(project.sprites[cmd_types[node->type].par[3]])
                            ui_text(dst, x + a + 2, y + p, project.sprites[cmd_types[node->type].par[3]][node->arg2].name);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_BOOL:
                        ui_bool(dst, x + a + 4, y + 1, node->aw[i], node->arg3, NULL);
                    break;
                    case FORM_TIME:
                        if(node->arg3 < 1) node->arg3 = 1;
                        if(node->arg3 > CMD_TIMEMAX) node->arg3 = CMD_TIMEMAX;
                        ui_time(dst, x + a, y + 1, node->aw[i], node->arg3, CMD_TIMEMAX,
                            clknode == node && clk == 3 ? 1 : (clknode == node && clk == 4 ? 2 : 0));
                    break;
                    case FORM_NUM:
                        if(node->arg3 < cmd_types[node->type].par[3]) node->arg3 = cmd_types[node->type].par[3];
                        if(node->arg3 > CMD_NUMMAX) node->arg3 = CMD_NUMMAX;
                        ui_num(dst, x + a, y + 1, node->aw[i], node->arg3,
                            clknode == node && clk == 3 ? 1 : (clknode == node && clk == 4 ? 2 : 0));
                    break;
                    case FORM_ALERT:
                        if(node->arg2 < 0 || node->arg2 > CMD_NUMALERT - 2) node->arg2 = 0;
                        if(clknode == node && clk == i + 1) { cl = d; cd = l; p = 3; } else { cl = l; cd = d; p = 2; }
                        ui_text(dst, x + a + 2, y + p, alerts_opts[node->arg2]);
                        ui_rect(dst, x + a, y + 2, node->aw[i], 18, cl, cd);
                    break;
                    case FORM_ALTITUDE:
                        if(node->arg3 < -128) node->arg3 = -128;
                        if(node->arg3 > 127) node->arg3 = 127;
                        ui_num(dst, x + a, y + 1, node->aw[i], node->arg3,
                            clknode == node && clk == 3 ? 1 : (clknode == node && clk == 4 ? 2 : 0));
                    break;
                }
                a += node->aw[i] + (node->aw[i] ? 4 : 0);
            }
            ui_input_bg = theme[THEME_INPBG];
        break;
    }
}

/**
 * Display a command script
 */
void ui_cmd(SDL_Texture *dst, int x, int y, int w, int h, int label, ui_cmd_t *cmd, int pressed)
{
    SDL_Rect save;

    ui_box2(dst, x, y, w, h, theme[THEME_DARKER], theme[THEME_INPBG], theme[THEME_LIGHTER], 20, cmd->scry, 24, 0);
    ui_box(dst, x + 1, y + 1, w - 2, 20, theme[THEME_LIGHT], theme[THEME_BG], theme[THEME_DARKER]);
    if(label)
        ui_text2(dst, x + 8, y + 2, lang[label], theme[THEME_DARKER], theme[THEME_LIGHTER]);
    ui_vscrbar(dst, x + w - 13, y + 21, 12, h - 34, cmd->scry, (h - 35) / 24, cmd->root.h / 24, pressed && (ui_popup_page == -1));
    ui_hscrbar(dst, x + 1, y + h - 13, w - 13, 12, cmd->scrx, (w - 14) / 24, cmd->root.w / 24, pressed && (ui_popup_page == -2));
    memcpy(&save, &ui_clip, sizeof(ui_clip));
    ui_clip.x = x + 1; ui_clip.y = y + 21; ui_clip.w = w - 14; ui_clip.h = h - 35;
    ui_cmd_lastmap = 0;
    _ui_cmd(dst, x + 1 - cmd->scrx * 24, y + 21 - cmd->scry * 24, &cmd->root, cmd->clknode, cmd->clk);
    if(cmd->clknode && ui_cmd_inp.str) {
        ui_input_bg = ui_cmd_color;
        ui_input(dst, cmd->clkx - 1, cmd->clky - 1, x + w - 14 - cmd->clkx, ui_input_buf, 1);
        ui_input_bg = theme[THEME_INPBG];
    }
    memcpy(&ui_clip, &save, sizeof(ui_clip));
}

/**
 * Recursively get status in script
 */
int _ui_cmd_status(int x, int y, cmd_t *node)
{
    int i, a = 0, ret = 0;

    if(!node) return 0;

    switch(node->type) {
        case 0:     /* block */
            if(node->children)
                for(i = 0, a = 0; !ret && i < node->len; i++) {
                    ret = _ui_cmd_status(x, y + a, &node->children[i]);
                    a += node->children[i].h;
                }
        break;
        case 1:     /* comment */
            if(ui_cury > y && ui_cury < y + 24 && ui_curx > x && ui_curx < x + node->w)
                return CMD_CMNT;
        break;
        case 2:     /* loop */
            if(ui_cury > y && ui_cury < y + 24 && ui_curx > x && ui_curx < x + node->w) {
                if(ui_curx > x + 20 && ui_curx < x + node->w - 2) return CMD_STAT_ITER;
                return CMD_ITER;
            }
            if(node->children) {
                for(i = 0, a = 24; !ret && i < node->len; i++) {
                    ret = _ui_cmd_status(x + 3, y + a, &node->children[i]);
                    a += node->children[i].h;
                }
            }
        break;
        case 3:     /* switch */
            if(ui_cury > y && ui_cury < y + 24 && ui_curx > x && ui_curx < x + node->w) {
                if(ui_curx > x + 20 && ui_curx < x + 36) return CMD_STAT_SWPLUS;
                if(ui_curx > x + 40 && ui_curx < x + 56) return CMD_STAT_SWMINUS;
                if(ui_curx > x + 60 && ui_curx < x + node->w - 2) return CMD_STAT_SWEXPR;
                return CMD_COND;
            }
            for(i = a = 0; !ret && i < node->len; i++) {
                ret = _ui_cmd_status(x + a + 3, y + 24, &node->children[i]);
                a += node->children[i].w + 5;
            }
        break;
        case 4:     /* let */
            if(ui_cury > y && ui_cury < y + 24 && ui_curx > x && ui_curx < x + node->w)
                return CMD_LET;
        break;
        default:    /* everything else is the same */
            if(ui_cury > y && ui_cury < y + 24 && ui_curx > x && ui_curx < x + node->w) {
                if(ui_curx < x + 20) return CMD_NOP + node->type;
                for(i = 0, a = 20; i < 3 && ui_curx >= x + a; i++) {
                    if(ui_curx > x + a && ui_curx < x + a + node->aw[i] + 4) {
                        if(cmd_types[node->type].stat[i]) return cmd_types[node->type].stat[i];
                        switch(cmd_types[node->type].par[i]) {
                            case FORM_MUSSEL:
                            case FORM_SNDSEL:
                            case FORM_SPCSEL: return CMD_STAT_MEDIA;
                            case FORM_CHRSEL: return CMD_STAT_CHOOSER;
                            case FORM_CUTSEL: return CMD_STAT_CUTSCN;
                            case FORM_SPRITE: return CMD_STAT_ANIM;
                            case FORM_NPCSEL: return CMD_STAT_NPC;
                            case FORM_DLGSEL: return CMD_STAT_DLG;
                            case FORM_OBJSEL: return CMD_STAT_OBJ;
                            case FORM_CFTSEL: return CMD_STAT_CFT;
                            case FORM_MAPSEL: return CMD_STAT_MAP;
                            case FORM_DIRSEL: return CMD_STAT_DIR;
                            case FORM_QSTSEL: return CMD_STAT_QST;
                            case FORM_BEHSEL: return CMD_STAT_BEH;
                            case FORM_TRNSEL: return CMD_STAT_TRN;
                            case FORM_CLKSEL: return CMD_STAT_CLK;
                            case FORM_ACNSEL: return CMD_STAT_ACN;
                        }
                    }
                    a += node->aw[i] + (node->aw[i] ? 4 : 0);
                }
                return 0;
            }
        break;
    }
    return ret;
}

/**
 * Get status for command arguments
 */
void ui_cmd_status(int x, int y, ui_cmd_t *cmd)
{
    int ret = _ui_cmd_status(x + 1 - cmd->scrx * 24, y + 21 - cmd->scry * 24, &cmd->root);
    if(ret)
        ui_status(0, lang[ret]);
}

/**
 * Recursively free script nodes
 */
void _ui_cmd_free(cmd_t *node)
{
    int i;

    if(node) {
        if(node->children) {
            for(i = 0; i < node->len; i++)
                _ui_cmd_free(&node->children[i]);
            free(node->children);
            node->children = NULL;
        }
        node->len = node->err = 0;
    }
}

/**
 * Free command script
 */
void ui_cmd_free(ui_cmd_t *cmd)
{
    cmd->scrx = cmd->scry = cmd->err = cmd->clk = 0;
    cmd->clknode = NULL;
    _ui_cmd_free(&cmd->root);
}

/**
 * Walk the node tree
 */
cmd_t *ui_cmd_walk(cmd_t *node, int x, int y, int sx, int sy, int *rx, int *ry, int *ri, cmd_t **parent)
{
    int i, a = 0;

    if(!node) return NULL;
    if(node->type != 1 && node->type < 4 && (!node->type || y >= sy + 24)) {
        if(parent)
            *parent = node;
        if(node->children) {
            if(node->type == 3) {
                for(i = a = 0; i < node->len; i++)
                    if(x >= sx + a && x < sx + a + node->children[i].w + 5)
                        return ui_cmd_walk(&node->children[i], x, y, sx + a + 1, sy + 24, rx, ry, ri, parent);
                    else
                        a += node->children[i].w + 5;
                return NULL;
            } else {
                for(i = 0, a = node->type ? 24 : 0; i < node->len; i++)
                    if(y >= sy + a && y < sy + a + node->children[i].h)
                        return ui_cmd_walk(&node->children[i], x, y, sx + 1, sy + a, rx, ry, ri, parent);
                    else
                        a += node->children[i].h;
            }
        }
    }
    if(rx) *rx = sx;
    if(ry) *ry = sy;
    if(ri) *ri = x - sx;
    return node;
}

/**
 * Resize script nodes recursively
 */
void ui_cmd_resize(cmd_t *node)
{
    int i, j, w;

    if(!node) return;
    node->w = (node->type == 3 ? 58 : 18); node->h = 0;
    for(i = 0; i < 3; i++) {
        node->aw[i] = 0;
        switch(cmd_types[node->type].par[i]) {
            case FORM_TEXT:
            case FORM_EXPR:
                node->aw[i] = ui_textwidth(node->arg1) + 4;
                if(node->aw[i] < 8) node->aw[i] = 8;
            break;
            case FORM_CMDVAR:
                node->aw[i] = (node->arg2 < 2 && node->arg3 >= 0 && project.attrs ? ui_textwidth(project.attrs[node->arg3]) + 4 : 0);
                node->w += 12 - (node->arg2 < 2 ? 4 : 0);
            break;
            case FORM_MUSSEL:
                node->aw[i] = (node->arg2 >= 0 && project.music ? ui_textwidth(project.music[node->arg2]) : 0) + 4;
            break;
            case FORM_SNDSEL:
                node->aw[i] = (node->arg2 >= 0 && project.sounds ? ui_textwidth(project.sounds[node->arg2]) : 0) + 4;
            break;
            case FORM_SPCSEL:
                node->aw[i] = (node->arg2 >= 0 && project.speech ? ui_textwidth(project.speech[node->arg2]) : 0) + 4;
            break;
            case FORM_CHRSEL:
                node->aw[i] = (node->arg2 >= 0 && project.choosers ? ui_textwidth(project.choosers[node->arg2]) : 0) + 4;
            break;
            case FORM_CUTSEL:
                node->aw[i] = (node->arg2 >= 0 && project.cuts ? ui_textwidth(project.cuts[node->arg2]) : 0) + 4;
            break;
            case FORM_NPCSEL:
                node->aw[i] = (node->arg2 >= 0 && project.sprites[PROJ_NUMSPRCATS - 2] ?
                    ui_textwidth(project.sprites[PROJ_NUMSPRCATS - 2][node->arg2].name) : 0) + 4;
            break;
            case FORM_DLGSEL:
                node->aw[i] = (node->arg2 >= 0 && project.dialogs ? ui_textwidth(project.dialogs[node->arg2]) : 0) + 4;
            break;
            case FORM_OBJSEL:
                node->aw[i] = (node->arg2 >= 0 && project.sprites[1] ? ui_textwidth(project.sprites[1][node->arg2].name) : 0) + 4;
            break;
            case FORM_CFTSEL:
                node->aw[i] = (node->arg2 >= 0 && project.crafts ? ui_textwidth(project.crafts[node->arg2]) : 0) + 4;
            break;
            case FORM_QSTSEL:
                node->aw[i] = (node->arg2 >= 0 && project.quests ? ui_textwidth(project.quests[node->arg2]) : 0) + 4;
            break;
            case FORM_BEHSEL: node->aw[i] = (node->arg2 >= 0 ? ui_textwidth(project_behs[node->arg2]) : 0) + 4; break;
            case FORM_TRNSEL: node->aw[i] = (node->arg2 >= 0 ? ui_textwidth(project_trns[node->arg2]) : 0) + 4; break;
            case FORM_CLKSEL: node->aw[i] = (node->arg2 >= 0 ? ui_textwidth(project_clock[node->arg2]) : 0) + 4; break;
            case FORM_ACNSEL: node->aw[i] = (node->arg2 >= 0 ? ui_textwidth(project_actions[node->arg2]) : 0) + 4; break;
            case FORM_MAPSEL:
                node->aw[i] = (node->arg4 >= 0 && project.maps ? ui_textwidth(project.maps[node->arg4]) : 0) + 4;
            break;
            case FORM_DIRSEL: node->aw[i] = 20; break;
            case FORM_SPRITE:
                node->aw[i] = (node->arg2 >= 0 && project.sprites[cmd_types[node->type].par[3]] ?
                    ui_textwidth(project.sprites[cmd_types[node->type].par[3]][node->arg2].name) : 0) + 4;
            break;
            case FORM_BOOL: node->aw[i] = 16; break;
            case FORM_TIME: node->aw[i] = (CMD_TIMEMAX > 6000 ? 80 : 56); break;
            case FORM_NUM: node->aw[i] = (CMD_NUMMAX > 99999 ? 80 : 56); break;
            case FORM_ALERT:
                node->aw[i] = (node->arg2 >= 0 && node->arg2 <= CMD_NUMALERT ? ui_textwidth(alerts_opts[node->arg2]) : 0) + 4;
            break;
            case FORM_ALTITUDE: node->aw[i] = 56; break;
        }
        node->w += node->aw[i] + (node->aw[i] ? 4 : 0);
    }
    if(!node->aw[0] && !node->aw[1] && !node->aw[2]) node->w += 2;
    if(node->children) {
        for(i = w = 0; i < node->len; i++) {
            ui_cmd_resize(&node->children[i]);
            if(node->type == 3) {
                w += node->children[i].w + 5;
                if(node->children[i].h > node->h)
                    node->h = node->children[i].h;
            } else {
                node->h += node->children[i].h;
                if(node->children[i].w > w)
                    w = node->children[i].w;
            }
        }
        if(node->type == 3) {
            if(w > node->w)
                node->w = w;
            else {
                j = (node->w - w) / node->len;
                for(i = w = 0; i < node->len - 1; i++) {
                    node->children[i].w += j;
                    w += node->children[i].w + 5;
                }
                node->children[i].w = (node->w - w - 5);
            }
        } else
            if(w + 5 > node->w)
                node->w = w + 5;
    }
    if(node->type && node->h < 24) node->h = 24;
    switch(node->type) {
        case 0: node->h += 24; break;
        case 2: node->w += 1; node->h += (node->len ? 48 : 24); break;
        case 3: node->w += 1; node->h += 24; break;
    }
}

/**
 * Command scripts mouse drag handler
 */
int ui_cmd_drag(ui_form_t *elem, int x, int y)
{
    uint32_t l, b, d;
    ui_cmd_t *cmd;
    cmd_t *parent = NULL, *node;
    int i = 0;

    if(!elem || !elem->param || x < 0 || y < 0) return 0;
    cmd = elem->param;
    cmd->clknode = NULL; cmd->clk = 0;
    node = ui_cmd_walk(&cmd->root, x + cmd->scrx * 24, y + cmd->scry * 24, 0, 0, NULL, NULL, NULL, &parent);
    if(!node || !node->type) return 0;
    for(i = 0; i < parent->len && &parent->children[i] != node; i++);
    node = main_alloc(sizeof(cmd_t));
    memcpy(node, &parent->children[i], sizeof(cmd_t));
    memmove(&parent->children[i], &parent->children[i + 1], (parent->len - i - 1) * sizeof(cmd_t));
    parent->len--;
    if(parent->len)
        parent->children = (cmd_t*)realloc(parent->children, parent->len * sizeof(cmd_t));
    else {
        free(parent->children);
        parent->children = NULL;
    }
    ui_cmd_resize(&cmd->root);
    i = ui_cmdpal_get(CMDPAL_ALL, node->type, &l, &b, &d);
    ui_dnd_drag(-1, node, cmd_types[i].icon, l, b, d);
    return 1;
}

/**
 * Command scripts mouse drop handler
 */
int ui_cmd_drop(ui_form_t *elem, int x, int y)
{
    ui_cmd_t *cmd;
    cmd_t *parent = NULL, *node;
    int i = 0;

    if(!elem || !elem->param || x < 0 || x >= elem->w - 13 || y < 0 || y >= elem->h - 13 || (ui_dnd_type == -1 && !ui_dnd_param))
        return 0;
    cmd = elem->param;
    node = ui_cmd_walk(&cmd->root, x + cmd->scrx * 24, y + cmd->scry * 24, 0, 0, NULL, NULL, NULL, &parent);
    if(!node) return 0;
    for(i = 0; i < parent->len && &parent->children[i] != node; i++);
    parent->children = (cmd_t*)realloc(parent->children, (parent->len + 1) * sizeof(cmd_t));
    if(parent->children) {
        if(i < parent->len)
            memmove(&parent->children[i + 1], &parent->children[i], (parent->len - i) * sizeof(cmd_t));
        parent->len++;
        if(ui_dnd_param) {
            memcpy(&parent->children[i], ui_dnd_param, sizeof(cmd_t));
            free(ui_dnd_param);
            ui_dnd_param = NULL;
        } else {
            memset(&parent->children[i], 0, sizeof(cmd_t));
            parent->children[i].type = ui_dnd_type;
            switch(ui_dnd_type) {
                case 3:
                    parent->children[i].arg1[0] = 'a';
                    parent->children[i].children = (cmd_t*)main_alloc(2 * sizeof(cmd_t));
                    parent->children[i].len = 2;
                break;
                case 4: case 42: parent->children[i].arg2 = 2; break;
            }
        }
    } else
        parent->len = 0;
    ui_cmd_resize(&cmd->root);
    return 1;
}

/**
 * Free dragged script
 */
void ui_cmd_dropfree()
{
    ui_dnd_type = -1;
    if(!ui_dnd_param) return;
    _ui_cmd_free(ui_dnd_param);
    ui_dnd_param = NULL;
}

/**
 * Command scripts mouse down handler
 */
int ui_cmd_click(ui_form_t *elem, int x, int y)
{
    ui_cmd_t *cmd;
    cmd_t *node;
    int i = 0, a, X = 0, sx;

    ui_cmd_inp.str = NULL;
    ui_cmd_sel.opts = NULL;
    ui_cmd_map.val = NULL;
    ui_sprite_ptr = NULL;
    if(!elem || !elem->param || x < 0 || y < 0) return 0;
    cmd = elem->param;
    cmd->clknode = NULL; cmd->clk = 0;
    node = ui_cmd_walk(&cmd->root, x + cmd->scrx * 24, y + cmd->scry * 24, 0, 0, &sx, &cmd->clky, &X, NULL);
    if(!node || !node->type) return 0;
    sx -= cmd->scrx * 24; cmd->clky -= cmd->scry * 24;
    node->err = 0;
    switch(node->type) {
        case 1:
        case 2:
            if(X >= 20 && X < node->w - 2 && y < cmd->clky + 24) { cmd->clknode = node; cmd->clk = 1; cmd->clkx = sx + 20; }
        break;
        case 3:
            if(X >= 20 && X < 36) { cmd->clknode = node; cmd->clk = 1; cmd->clkx = sx + 20; } else
            if(X >= 40 && X < 56) { cmd->clknode = node; cmd->clk = 2; cmd->clkx = sx + 40; } else
            if(X >= 60 && X < node->w - 2) { cmd->clknode = node; cmd->clk = 3; cmd->clkx = sx + 60; }
        break;
        case 4:
            i = (node->arg2 < 2 ? node->aw[1]: 0);
            if(X >= 2 && X < 12) { cmd->clknode = node; cmd->clk = 1; cmd->clkx = sx + 2; } else
            if(X >= 16 && X < 16 + i) { cmd->clknode = node; cmd->clk = 2; cmd->clkx = sx + 16; } else
            if(X >= 30 + i && X < node->w - 2) { cmd->clknode = node; cmd->clk = 3; cmd->clkx = sx + 30 + i; }
        break;
        default:
            for(i = 0, a = 20; i < 3; i++) {
                if(X >= a && X < a + node->aw[i]) {
                    cmd->clknode = node; cmd->clk = i + 1; cmd->clkx = sx + a;
                    switch(cmd_types[node->type].par[i]) {
                        case FORM_BOOL:
                            node->arg3 ^= 1;
                            cmd->clknode = NULL; cmd->clk = 0;
                        break;
                        case FORM_TIME:
                        case FORM_NUM:
                        case FORM_ALTITUDE:
                            if(y >= cmd->clky + 12) cmd->clk = i + 2;
                        break;
                    }
                }
                a += node->aw[i] + (node->aw[i] ? 4 : 0);
            }
        break;
    }
    cmd->clkx += elem->x + 1 - cmd->scrx; cmd->clky += elem->y + 23 - cmd->scry;
    return 1;
}

/**
 * Command scripts mouse release handler
 */
int ui_cmd_release(ui_form_t *elem)
{
    ui_cmd_t *cmd;
    int i, x;

    ui_cmd_inp.maxlen = PROJ_EXPRMAX;
    ui_cmd_inp.str = NULL;
    ui_cmd_sel.opts = NULL;
    ui_cmd_map.val = NULL;
    ui_sprite_ptr = NULL;
    if(!elem || !elem->param) return 0;
    cmd = elem->param;
    if(!cmd->clknode || !cmd->clk) return 0;
    switch(cmd->clknode->type) {
        case 1:
            ui_cmd_color = theme[THEME_CMNT];
            ui_cmd_inp.type = INP_TEXT;
            ui_cmd_inp.str = cmd->clknode->arg1;
        break;
        case 2:
            ui_cmd_color = theme[THEME_LOOP];
            ui_cmd_inp.type = INP_EXPR;
            ui_cmd_inp.str = cmd->clknode->arg1;
        break;
        case 3:
            ui_cmd_color = theme[THEME_SWITCH];
            switch(cmd->clk) {
                case 1: /* add case */
                    cmd->clknode->children = (cmd_t*)realloc(cmd->clknode->children, (cmd->clknode->len + 1) * sizeof(cmd_t));
                    if(cmd->clknode->children) {
                        memmove(&cmd->clknode->children[1], cmd->clknode->children, cmd->clknode->len * sizeof(cmd_t));
                        memset(cmd->clknode->children, 0, sizeof(cmd_t));
                        cmd->clknode->len++;
                    } else
                        cmd->clknode->len = 0;
                    ui_cmd_resize(&cmd->root);
                    cmd->clknode = NULL; cmd->clk = 0;
                    return 0;
                break;
                case 2: /* del case */
                    if(cmd->clknode->len > 2) {
                        _ui_cmd_free(&cmd->clknode->children[0]);
                        cmd->clknode->len--;
                        memmove(cmd->clknode->children, &cmd->clknode->children[1], cmd->clknode->len * sizeof(cmd_t));
                        cmd->clknode->children = (cmd_t*)realloc(cmd->clknode->children, cmd->clknode->len * sizeof(cmd_t));
                        if(!cmd->clknode->children)
                            cmd->clknode->len = 0;
                    }
                    ui_cmd_resize(&cmd->root);
                    cmd->clknode = NULL; cmd->clk = 0;
                    return 0;
                break;
                case 3:
                    ui_cmd_inp.type = INP_EXPR;
                    ui_cmd_inp.str = cmd->clknode->arg1;
                break;
            }
        break;
        case 4:
            ui_cmd_color = theme[THEME_LET];
            switch(cmd->clk) {
                case 1: ui_cmd_sel.opts = ui_cmd_locals; ui_cmd_sel.val = cmd->clknode->arg2; break;
                case 2: ui_cmd_sel.opts = project.attrs; ui_cmd_sel.val = cmd->clknode->arg3; break;
                case 3: ui_cmd_inp.type = INP_EXPR; ui_cmd_inp.str = cmd->clknode->arg1; break;
            }
        break;
        default:
            ui_cmd_color = theme[THEME_FUNC];
            if(cmd_types[cmd->clknode->type].par[0] == FORM_EXPR && cmd->clk == 1) {
                ui_cmd_inp.type = INP_EXPR;
                ui_cmd_inp.str = cmd->clknode->arg1;
            } else
            if(cmd_types[cmd->clknode->type].par[0] == FORM_MAPSEL && cmd->clk == 1) {
                ui_cmd_map.fix = cmd_types[cmd->clknode->type].icon == ICON_NPCMAP;
                ui_cmd_map.crd = &cmd->clknode->arg5; ui_cmd_map.val = &cmd->clknode->arg4;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_DIRSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = ui_cmd_dirs; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_MUSSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project.music; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_SNDSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project.sounds; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_SPCSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project.speech; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_CHRSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project.choosers; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_CUTSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project.cuts; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_NPCSEL && cmd->clk == 2) {
                ui_cmd_spr.cat = PROJ_NUMSPRCATS - 2;
                ui_cmd_spr.unique = 1;
                ui_sprite_ptr = &cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_DLGSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project.dialogs; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_OBJSEL && cmd->clk == 2) {
                ui_cmd_spr.cat = 1;
                ui_cmd_spr.unique = 3;
                ui_sprite_ptr = &cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_CFTSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project.crafts; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_QSTSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project.quests; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_BEHSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project_behs; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_TRNSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project_trns; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_CLKSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project_clock; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_ACNSEL && cmd->clk == 2) {
                ui_cmd_sel.opts = project_actions; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_ALERT && cmd->clk == 2) {
                ui_cmd_sel.opts = alerts_opts; ui_cmd_sel.val = cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[1] == FORM_SPRITE && cmd->clk == 2) {
                ui_cmd_spr.cat = cmd_types[cmd->clknode->type].par[3];
                ui_cmd_spr.unique = 1;
                ui_sprite_ptr = &cmd->clknode->arg2;
            } else
            if(cmd_types[cmd->clknode->type].par[2] == FORM_TIME && cmd->clk >= 3) {
                if(cmd->clk == 3 && cmd->clknode->arg3 < CMD_TIMEMAX) cmd->clknode->arg3++;
                if(cmd->clk == 4 && cmd->clknode->arg3 > 1) cmd->clknode->arg3--;
                cmd->clknode = NULL; cmd->clk = 0;
                return 0;
            } else
            if(cmd_types[cmd->clknode->type].par[2] == FORM_NUM && cmd->clk >= 3) {
                if(cmd->clk == 3 && cmd->clknode->arg3 < CMD_NUMMAX) cmd->clknode->arg3++;
                if(cmd->clk == 4 && cmd->clknode->arg3 > cmd_types[cmd->clknode->type].par[3]) cmd->clknode->arg3--;
                cmd->clknode = NULL; cmd->clk = 0;
                return 0;
            } else
            if(cmd_types[cmd->clknode->type].par[2] == FORM_ALTITUDE && cmd->clk >= 3) {
                if(cmd->clk == 3 && cmd->clknode->arg3 < 127) cmd->clknode->arg3++;
                if(cmd->clk == 4 && cmd->clknode->arg3 > -128) cmd->clknode->arg3--;
                cmd->clknode = NULL; cmd->clk = 0;
                return 0;
            }
        break;
    }
    if(ui_cmd_inp.str) {
        ui_input_start(&ui_cmd_inp, NULL);
    } else
    if(ui_cmd_sel.opts) {
        ui_popup_type = FORM_SELECT;
        for(ui_popup_max = x = 0; ui_cmd_sel.opts[ui_popup_max]; ui_popup_max++) {
            i = ui_textwidth(ui_cmd_sel.opts[ui_popup_max]) + 10;
            if(i > x) x = i;
        }
        ui_popup_min = 0;
        ui_popup_page = (ui_popup_max - ui_popup_min);
        if(cmd->clky + 18 + (ui_popup_page * 18 + 4) > scr_h) {
            ui_popup_page = (scr_h - cmd->clky - 18) / 18 - 1;
            if(ui_popup_page < 9)
                ui_popup_page = (ui_popup_max - ui_popup_min) > 9 ? 9 : (ui_popup_max - ui_popup_min);
        }
        ui_popup_scr = ui_popup_val = ui_cmd_sel.val;
        if(ui_popup_scr + ui_popup_page > ui_popup_max)
            ui_popup_scr = ui_popup_max - ui_popup_page;
        if(ui_popup_scr < ui_popup_min)
            ui_popup_scr = ui_popup_min;
        ui_popup(cmd->clkx, cmd->clky + 18, x, (ui_popup_page * 18 + 4), 3);
    } else
    if(ui_sprite_ptr) {
        ui_sprite_picker(cmd->clkx, cmd->clky, &ui_cmd_spr);
    } else
    if(ui_cmd_map.val) {
        ui_map_picker(cmd->clkx, cmd->clky, &ui_cmd_map);
    }
    return 1;
}

/**
 * Command scripts scroll handler
 */
void ui_cmd_wheel(ui_form_t *elem, int x, int y)
{
    ui_cmd_t *cmd;
    cmd_t *node;
    int i = 0, a, X = 0;

    ui_cmd_inp.str = NULL;
    ui_cmd_sel.opts = NULL;
    ui_sprite_ptr = NULL;
    if(!elem || !elem->param || (!x && !y)) return;
    cmd = elem->param;
    cmd->clknode = NULL; cmd->clk = 0;
    if(x != 0) {
        if(x > 0 && cmd->scrx > 0) cmd->scrx--;
        if(x < 0 && cmd->scrx < cmd->root.w / 24 - (elem->w - 14) / 24) cmd->scrx++;
        return;
    }
    node = ui_cmd_walk(&cmd->root, ui_curx - elem->x - 1 + cmd->scrx * 24, ui_cury - elem->y - 23 + cmd->scry * 24,
        0, 0, NULL, NULL, &X, NULL);
    if(node) {
        if(node->type == 5 && X > 120 && X < 162) {
            if(y > 0 && (node->arg3 & 0xff) < 192) node->arg3++;
            if(y < 0 && (node->arg3 & 0xff) > 16) node->arg3--;
            return;
        }
        if(node->type > 5)
            for(i = 0, a = 20; i < 3; i++) {
                if(X >= a && X < a + node->aw[i]) {
                    switch(cmd_types[node->type].par[i]) {
                        case FORM_TIME:
                            if(y > 0 && node->arg3 < CMD_TIMEMAX) node->arg3++;
                            if(y < 0 && node->arg3 > 1) node->arg3--;
                            return;
                        break;
                        case FORM_NUM:
                            if(y > 0 && node->arg3 < CMD_NUMMAX) node->arg3++;
                            if(y < 0 && node->arg3 > cmd_types[node->type].par[3]) node->arg3--;
                            return;
                        break;
                        case FORM_ALTITUDE:
                            if(y > 0 && node->arg3 < 127) node->arg3++;
                            if(y < 0 && node->arg3 > -128) node->arg3--;
                            return;
                        break;
                    }
                }
                a += node->aw[i] + (node->aw[i] ? 4 : 0);
            }
    }
    if(y > 0 && cmd->scry > 0) cmd->scry--;
    if(y < 0 && cmd->scry < cmd->root.h / 24 - (elem->h - 35) / 24) cmd->scry++;
}

/**
 * Command scripts set a node's value
 */
void ui_cmd_set(ui_cmd_t *cmd, int i)
{
    if(!cmd || !cmd->clknode) return;
    if(i != -2) {
        if(i == -1 && !ui_sprite_ptr && !ui_cmd_map.val && cmd->clknode->type != 1 && !ui_cmd_validexpr(cmd->clknode->arg1)) {
            cmd->clknode->err = 1;
            ui_status(1, lang[ERR_EXPR]);
        } else
        if(i >= 0)
            switch(cmd->clknode->type) {
                case 1: break;
                case 4:
                    switch(cmd->clk) {
                        case 1: cmd->clknode->arg2 = i; break;
                        case 2: cmd->clknode->arg3 = i; break;
                    }
                break;
                default: cmd->clknode->arg2 = i; break;
            }
    }
    cmd->clknode = NULL; cmd->clk = 0;
    ui_cmd_resize(&cmd->root);
}

/**
 * Check an expression
 */
int ui_cmd_validexpr(char *str)
{
    ui_bc_t bc;
    int ret = ui_cmd_expr(NULL, &bc, str);
    if(bc.data) free(bc.data);
    return ret;
}

/**
 * Serialize script node tree into a file
 */
void ui_cmd_serialize(FILE *f, cmd_t *node, int lvl, int *e)
{
    int i, j;

    if(!f || !node || lvl < 0) return;
    if(!node->type) { fprintf(f, "{\r\n"); }
    else for(i = 0; i < lvl; i++) fprintf(f, "  ");
    switch(node->type) {
        case 0:
            for(i = 0; i < node->len; i++)
                ui_cmd_serialize(f, &node->children[i], lvl + 1, e);
        break;
        case 1:
            fprintf(f, "%s %s", cmd_types[1].name, node->arg1);
        break;
        case 2:
            node->err = !ui_cmd_validexpr(node->arg1); *e |= node->err;
            fprintf(f, "%s(%s) {\r\n", cmd_types[2].name, node->arg1);
            for(i = 0; i < node->len; i++)
                ui_cmd_serialize(f, &node->children[i], lvl + 1, e);
            for(i = 0; i < lvl; i++) fprintf(f, "  ");
            fprintf(f, "}");
        break;
        case 3:
            node->err = !ui_cmd_validexpr(node->arg1); *e |= node->err;
            fprintf(f, "%s(%s) {\r\n", cmd_types[3].name, node->arg1);
            for(i = 0; i < node->len; i++) {
                for(j = 0; j < lvl + 1; j++) fprintf(f, "  ");
                fprintf(f, "case %d ", node->len - i - 1);
                ui_cmd_serialize(f, &node->children[i], lvl + 1, e);
            }
            for(i = 0; i < lvl; i++) fprintf(f, "  ");
            fprintf(f, "}");
        break;
        case 4:
            node->err = !ui_cmd_validexpr(node->arg1); *e |= node->err;
            if(node->arg2 > 1)
                fprintf(f, "%s", ui_cmd_locals[node->arg2]);
            else
                fprintf(f, "%s%s", node->arg2 == 1 ? "@" : "", project.attrs[node->arg3]);
            fprintf(f, "=%s", node->arg1);
        break;
        default:
            fprintf(f, "%s(", cmd_types[node->type].name);
            for(i = j = 0; i < 3; i++) {
                if(cmd_types[node->type].par[i]) {
                    if(j) fprintf(f, ",");
                    j++;
                }
                switch(cmd_types[node->type].par[i]) {
                    case FORM_EXPR:
                        node->err = !ui_cmd_validexpr(node->arg1); *e |= node->err;
                        fprintf(f, "%s", node->arg1);
                    break;
                    case FORM_MUSSEL: fprintf(f, "%s", project.music[node->arg2]); break;
                    case FORM_SNDSEL: fprintf(f, "%s", project.sounds[node->arg2]); break;
                    case FORM_SPCSEL: fprintf(f, "%s", project.speech[node->arg2]); break;
                    case FORM_CHRSEL: fprintf(f, "%s", project.choosers[node->arg2]); break;
                    case FORM_CUTSEL: fprintf(f, "%s", project.cuts[node->arg2]); break;
                    case FORM_NPCSEL: project_wrsprite2(f, node->arg2, PROJ_NUMSPRCATS - 2); break;
                    case FORM_DLGSEL: fprintf(f, "%s", project.dialogs[node->arg2]); break;
                    case FORM_OBJSEL: project_wrsprite2(f, node->arg2, 1); break;
                    case FORM_CFTSEL: fprintf(f, "%s", project.crafts[node->arg2]); break;
                    case FORM_QSTSEL: fprintf(f, "%s", project.quests[node->arg2]); break;
                    case FORM_BEHSEL: fprintf(f, "%s", project_behs[node->arg2]); break;
                    case FORM_TRNSEL: fprintf(f, "%s", project_trns[node->arg2]); break;
                    case FORM_CLKSEL: fprintf(f, "%u", node->arg2); break;
                    case FORM_ACNSEL: fprintf(f, "%s", project_actions[node->arg2]); break;
                    case FORM_MAPSEL: fprintf(f, "%s,%u,%u", project.maps[node->arg4], node->arg5 & 0x7fff,
                        (node->arg5 >> 16) & 0x7fff); break;
                    case FORM_DIRSEL: fprintf(f, "%u", node->arg2); break;
                    case FORM_ALERT: fprintf(f, "%u", node->arg2 + 2); break;
                    case FORM_SPRITE: project_wrsprite2(f, node->arg2, cmd_types[node->type].par[3]); break;
                    case FORM_BOOL: case FORM_TIME: case FORM_NUM: fprintf(f, "%u", node->arg3); break;
                    case FORM_ALTITUDE: fprintf(f, "%d", node->arg3); break;
                }
            }
            fprintf(f, ")");
        break;
    }
    if(!node->type) {
        for(i = 0; i < lvl; i++) fprintf(f, "  ");
        fprintf(f, "}");
    }
    fprintf(f, "\r\n");
}

/**
 * Add a child node to node
 */
int ui_cmd_add(cmd_t *node, int type, char *arg1, int arg2, int arg3, int arg4, int arg5, int err)
{
    int i;

    if(!node) return -1;
    i = node->len++;
    node->children = (cmd_t*)realloc(node->children, node->len * sizeof(cmd_t));
    if(!node->children) { node->len = 0; return -1; }
    memset(&node->children[i], 0, sizeof(cmd_t));
    node->children[i].type = type;
    if(arg1)
        strncpy(node->children[i].arg1, arg1, PROJ_EXPRMAX);
    node->children[i].arg2 = arg2;
    node->children[i].arg3 = arg3;
    node->children[i].arg4 = arg4;
    node->children[i].arg5 = arg5;
    node->children[i].err = err;
    return i;
}

/**
 * Deserialize script string into a node tree
 */
char *ui_cmd_deserialize(char *str, cmd_t *node, int *e)
{
    char *s, *arg1, *last;
    int type, arg2, arg3, arg4, arg5, i, l, err = 0;

    if(!str || !*str) return str;
    while(*str == ' ' || *str == '\r' || *str == '\n') str++;
    if(*str != '{') return str;
    last = str; str++;
    while(*str && *str != '}') {
        while(*str == ' ' || *str == '\r' || *str == '\n') str++;
        /* failsafe */
        if(last == str) break;
        last = str;
        /* comment */
        if(str[0] == cmd_types[1].name[0] && (!cmd_types[1].name[1] || cmd_types[1].name[1] == str[1])) {
            for(str += strlen(cmd_types[1].name); *str == ' '; str++);
            str = project_getstr(str, &arg1, 2, PROJ_EXPRMAX);
            if(arg1) {
                i = ui_cmd_add(node, 1, arg1, 0, 0, 0, 0, 0);
                free(arg1);
            }
            continue;
        }
        /* detect let */
        arg1 = NULL; arg2 = arg3 = -1;
        if(*str >= 'a' && *str <= 'z' && str[1] == '=') {
            arg2 = *str - 'a' + 2;
            arg3 = 0;
            str += 2;
        } else {
            if(*str == '@') { arg2 = 1; str++; }
            s = project_getidx(str, &arg3, project.attrs, -1);
            if(*s == '=' && arg3 >= 0) {
                if(arg2 == -1) arg2++;
                str = s + 1;
            }
        }
        if(arg2 >= 0 && arg3 >= 0) {
            str = project_getstr(str, &arg1, 2, PROJ_EXPRMAX);
            err = !ui_cmd_validexpr(arg1); *e |= err;
            i = ui_cmd_add(node, 4, arg1, arg2, arg3, 0, 0, err);
            if(arg1) free(arg1);
            continue;
        }
        /* detect keywords */
        if(!memcmp(str, "case ", 5))
            type = 0;
        else
            for(type = 2; type < (int)(sizeof(cmd_types)/sizeof(cmd_types[0])); type++) {
                if(type == 4) continue;
                l = strlen(cmd_types[type].name);
                if(!memcmp(str, cmd_types[type].name, l) && str[l] == '(') { str += l + 1; break; }
            }
        if(type < (int)(sizeof(cmd_types)/sizeof(cmd_types[0]))) {
            arg1 = NULL; arg2 = arg3 = arg5 = err = 0;
            for(i = 0; *str && i < 3; i++) {
                if(*str == ',') str++;
                switch(cmd_types[type].par[i]) {
                    case FORM_EXPR:
                        str = project_getstr(str, &arg1, 5, PROJ_EXPRMAX);
                        err = !ui_cmd_validexpr(arg1); *e |= err;
                    break;
                    case FORM_MUSSEL: str = project_getidx(str, &arg2, project.music, 0); break;
                    case FORM_SNDSEL: str = project_getidx(str, &arg2, project.sounds, 0); break;
                    case FORM_SPCSEL: str = project_getidx(str, &arg2, project.speech, 0); break;
                    case FORM_CHRSEL: str = project_getidx(str, &arg2, project.choosers, 0); break;
                    case FORM_CUTSEL: str = project_getidx(str, &arg2, project.cuts, 0); break;
                    case FORM_NPCSEL:
                        ui_cmd_spr.cat = PROJ_NUMSPRCATS - 2;
                        str = project_getsprite(str, &ui_cmd_spr);
                        arg2 = ui_cmd_spr.val < 0 ? 0 : ui_cmd_spr.val;
                    break;
                    case FORM_DLGSEL: str = project_getidx(str, &arg2, project.dialogs, 0); break;
                    case FORM_OBJSEL: str = project_getobj(str, &arg2, 0); break;
                    case FORM_CFTSEL: str = project_getidx(str, &arg2, project.crafts, 0); break;
                    case FORM_QSTSEL: str = project_getidx(str, &arg2, project.quests, 0); break;
                    case FORM_BEHSEL: str = project_getidx(str, &arg2, project_behs, 1); break;
                    case FORM_TRNSEL: str = project_getidx(str, &arg2, project_trns, 0); break;
                    case FORM_CLKSEL: str = project_getint(str, &arg2, 0, 23); break;
                    case FORM_ACNSEL: str = project_getidx(str, &arg2, project_actions, 0); break;
                    case FORM_MAPSEL:
                        str = project_getidx(str, &arg4, project.maps, 0);
                        if(*str == ',') {
                            str++;
                            arg5 = 0;
                            str = project_getint(str, &l, 0, 0x7fff);
                            if(*str == ',') {
                                str++;
                                str = project_getint(str, &arg5, 0, 0x7fff);
                            }
                            arg5 <<= 16; arg5 |= l;
                        }
                    break;
                    case FORM_DIRSEL: str = project_getint(str, &arg2, 0, 8); break;
                    case FORM_SPRITE:
                        ui_cmd_spr.cat = cmd_types[type].par[3];
                        str = project_getsprite(str, &ui_cmd_spr);
                        arg2 = ui_cmd_spr.val < 0 ? 0 : ui_cmd_spr.val;
                    break;
                    case FORM_BOOL: str = project_getint(str, &arg3, 0, 1); break;
                    case FORM_TIME: str = project_getint(str, &arg3, 1, CMD_TIMEMAX); break;
                    case FORM_NUM: str = project_getint(str, &arg3, cmd_types[type].par[3], CMD_NUMMAX); break;
                    case FORM_ALERT: str = project_getint(str, &arg2, 2, CMD_NUMALERT); arg2 -= 2; break;
                    case FORM_ALTITUDE: str = project_getint(str, &arg3, -128, 127); break;
                }
            }
            while(*str && *str != '{' && *str != ')' && *str != '\r' && *str != '\n') str++;
            if(*str == ')') str++;
            i = ui_cmd_add(node, type, arg1, arg2, arg3, arg4, arg5, err);
            if(arg1) free(arg1);
            if(i >= 0 && type != 1 && type < 4)
                str = ui_cmd_deserialize(str, &node->children[i], e);
            continue;
        }
        /* unknown keyword, skip line */
        while(*str && *str != '{' && *str != '}' && *str != '\r' && *str != '\n') str++;
    }
    if(*str == '}') str++;
    return str;
}

/**
 * Expand bytecode buffer
 */
int ui_cmd_bcalloc(ui_bc_t *bc, int size)
{
    if(!bc->data || bc->len + size >= bc->alloc) {
        bc->alloc += 1024;
        bc->data = (uint8_t*)realloc(bc->data, bc->alloc);
        if(!bc->data) { bc->alloc = 0; return 0; }
    }
    return 1;
}

/**
 * Add to bytecode
 */
void ui_cmd_bc(ui_bc_t *bc, uint8_t *data, int len)
{
    if(ui_cmd_bcalloc(bc, len)) {
        memcpy(bc->data + bc->len, data, len);
        bc->len += len;
    }
}
/**
 * Convenient functions to add bytecode
 */
void ui_cmd_bc8 (ui_bc_t *bc, uint8_t  data) { ui_cmd_bc(bc, (uint8_t*)&data, 1); }
void ui_cmd_bc16(ui_bc_t *bc, uint16_t data) { ui_cmd_bc(bc, (uint8_t*)&data, 2); }
void ui_cmd_bc24(ui_bc_t *bc, uint32_t data) { ui_cmd_bc(bc, (uint8_t*)&data, 3); }
void ui_cmd_bc32(ui_bc_t *bc, uint32_t data) { ui_cmd_bc(bc, (uint8_t*)&data, 4); }

/**
 * Convert expresion to bytecode
 */
int ui_cmd_expr(tngctx_t *ctx, ui_bc_t *bc, char *data)
{
    ui_bc_t sub;
    int64_t n;
    int i, l, p, top = -1;
    char *s, *e, o = 0, stk[PROJ_EXPRMAX], unary = 1;

    if(!bc) return 0;
    memset(bc, 0, sizeof(ui_bc_t));
    if(!data || !*data) return 0;
    for(s = data; *s; s++) {
        if(*s == '(') {
            stk[++top] = '(';
            unary = 1;
        } else
        if(*s == ')') {
            if(top < 0) goto err;
            while(top >= 0 && stk[top] != '(')
                ui_cmd_bc8(bc, stk[top--]);
            if(stk[top] != '(') goto err;
            top--;
            unary = 0;
        } else
        if(*s == ',') {
            while(top >= 0) {
                if(stk[top] == '(') goto err;
                else ui_cmd_bc8(bc, stk[top--]);
            }
            unary = 1;
        } else
        if(!memcmp(s, "min(", 4) || !memcmp(s, "max(", 4)) {
            for(e = s + 4, p = l = 1; *e; e++) {
                if(*e == '(') p++;
                if(*e == ',') l++;
                if(*e == ')') { p--; if(!p) break; }
            }
            if(!*e || p || l > 255) goto err;
            *e = 0;
            if(!ui_cmd_expr(ctx, &sub, s + 4) || !sub.data) {
                *e = ')';
                if(sub.data) free(sub.data);
                goto err;
            }
            *e = ')';
            ui_cmd_bc(bc, sub.data, sub.len);
            free(sub.data);
            if(l > 1) {
                ui_cmd_bc8(bc, s[1] == 'i' ? TNG_BC_MIN : TNG_BC_MAX);
                ui_cmd_bc8(bc, l);
            }
            s = e;
            unary = 0;
        } else
        if(!memcmp(s, "rnd(", 4)) {
            s += 4; n = atol(s);
            ui_cmd_bc8(bc, TNG_BC_RND);
            ui_cmd_bc32(bc, n > 0xffffffff ? 0xffffffff : n);
            while(*s && *s != ')') s++;
            if(!*s) goto err;
            unary = 0;
        } else
        if(!memcmp(s, "sum(", 4) || !memcmp(s, "@sum(", 5)) {
            s += *s == '@' ? 5 : 4;
            if(!project.attrs) goto err;
            for(i = l = 0; i < project.numattrs; i++) {
                l = strlen(project.attrs[i]);
                if(!memcmp(s, project.attrs[i], l) && s[l] == ')') break;
            }
            if(i >= project.numattrs) goto err;
            ui_cmd_bc8(bc, s[-5] == '@' ? TNG_BC_SUMO : TNG_BC_SUM);
            ui_cmd_bc24(bc, i);
            s += l;
            unary = 0;
        } else
        if(!memcmp(s, "cnt(", 4) || !memcmp(s, "@cnt(", 5)) {
            s += *s == '@' ? 5 : 4;
            if(*s == ')')
                ui_cmd_bc8(bc, s[-5] == '@' ? TNG_BC_CNTO0 : TNG_BC_CNT0);
            else {
                if(!project.sprites[1]) goto err;
                for(i = l = 0; i < project.spritenum[1]; i++) {
                    l = strlen(project.sprites[1][i].name);
                    if(!memcmp(s, project.sprites[1][i].name, l) && s[l] == ')') break;
                }
                if(i >= project.spritenum[1]) goto err;
                ui_cmd_bc8(bc, s[-5] == '@' ? TNG_BC_CNTO1 : TNG_BC_CNT1);
                ui_cmd_bc16(bc, i);
                s += l;
            }
            unary = 0;
        } else
        if((unary && *s == '-') || (*s >= '0' && *s <= '9')) {
            n = atol(s);
            if(n > -128 && n < 127) {
                ui_cmd_bc8(bc, TNG_BC_PUSH8);
                ui_cmd_bc8(bc, n);
            } else
            if(n > -32768 && n < 32767) {
                ui_cmd_bc8(bc, TNG_BC_PUSH16);
                ui_cmd_bc16(bc, n);
            } else
            if(n > -16777216 && n < 16777215) {
                ui_cmd_bc8(bc, TNG_BC_PUSH24);
                ui_cmd_bc24(bc, n);
            } else {
                if(n & ~0xffffffff) goto err;
                ui_cmd_bc8(bc, TNG_BC_PUSH32);
                ui_cmd_bc32(bc, n);
            }
            if(*s == '-') s++;
            while(*s && *s >= '0' && *s <= '9') s++;
            s--;
            unary = 0;
        } else
        if(strchr("+-*/%=<>!|&", *s)) {
            switch(*s) {
                case '+': o = TNG_BC_ADD; break;
                case '-': o = TNG_BC_SUB; break;
                case '*': o = TNG_BC_MUL; break;
                case '/': o = TNG_BC_DIV; break;
                case '%': o = TNG_BC_MOD; break;
                case '=': o = TNG_BC_EQ; break;
                case '|': o = TNG_BC_OR; break;
                case '&': o = TNG_BC_AND; break;
                case '!': if(s[1] == '=') { s++; o = TNG_BC_NE; } else o = TNG_BC_NOT; break;
                case '<': if(s[1] == '=') { s++; o = TNG_BC_LE; } else o = TNG_BC_LT; break;
                case '>': if(s[1] == '=') { s++; o = TNG_BC_GE; } else o = TNG_BC_GT; break;
            }
            /* we were smart, and assigned the bytecodes in operand's precedence order in the first place */
            while(top >= 0 && stk[top] != '(' && o <= stk[top])
                ui_cmd_bc8(bc, stk[top--]);
            stk[++top] = o;
            unary = 1;
        } else
        if((*s >= 'a' && *s <= 'z') && s[1] != '_' && s[1] != '.' && !(s[1] >= 'a' && s[1] <= 'z') &&
          !(s[1] >= '0' && s[1] <= '9')) {
            ui_cmd_bc8(bc, TNG_BC_PUSH);
            ui_cmd_bc8(bc, *s - 'a');
            unary = 0;
        } else {
            if(*s == '@') {
                ui_cmd_bc8(bc, TNG_BC_PUSHO);
                s++;
            } else
                ui_cmd_bc8(bc, TNG_BC_PUSHA);
            if(!project.attrs) goto err;
            for(i = l = 0; i < project.numattrs; i++) {
                l = strlen(project.attrs[i]);
                if(!memcmp(s, project.attrs[i], l) && (!s[l] || ISOP(s[l]) || s[l] == ',' || s[l] == ')')) break;
            }
            if(i >= project.numattrs) goto err;
            ui_cmd_bc24(bc, ctx ? tng_str(ctx, project.attrs[i]) : 0);
            s += l - 1;
            unary = 0;
        }
    }
    while(top >= 0) {
        if(stk[top] == '(') goto err;
        else ui_cmd_bc8(bc, stk[top--]);
    }
    return bc->len && bc->data ? 1 : 0;
err:
    if(bc->data) free(bc->data);
    bc->data = NULL;
    bc->len = 0;
    return 0;
}

/**
 * Serialize script node tree into a bytecode string
 */
void ui_cmd_tobc(tngctx_t *ctx, ui_bc_t *bc, cmd_t *node, int lvl, int *e)
{
    ui_bc_t expr;
    int i, j, k, *jmps;

    if(!ctx || !bc || !node || lvl < 0) return;
    switch(node->type) {
        case 0:
            for(i = 0; i < node->len; i++)
                ui_cmd_tobc(ctx, bc, &node->children[i], lvl + 1, e);
        break;
        case 1: /* comments not stored in final bytecode */ break;
        case 2:
            node->err = !ui_cmd_expr(ctx, &expr, node->arg1); *e |= node->err;
            if(node->err) { if(expr.data) free(expr.data); return; }
            j = bc->len;
            ui_cmd_bc(bc, expr.data, expr.len);
            free(expr.data);
            ui_cmd_bc8(bc, TNG_BC_JZ);
            k = bc->len;
            ui_cmd_bc24(bc, 0);
            for(i = 0; i < node->len; i++)
                ui_cmd_tobc(ctx, bc, &node->children[i], lvl + 1, e);
            ui_cmd_bc8(bc, TNG_BC_JMP);
            ui_cmd_bc24(bc, j);
            memcpy(bc->data + k, &bc->len, 3);
        break;
        case 3:
            node->err = !ui_cmd_expr(ctx, &expr, node->arg1); *e |= node->err;
            if(node->err) { if(expr.data) free(expr.data); return; }
            ui_cmd_bc(bc, expr.data, expr.len);
            free(expr.data);
            ui_cmd_bc8(bc, TNG_BC_SWITCH);
            ui_cmd_bc8(bc, node->len - 1);
            k = bc->len;
            for(i = 1; i < node->len; i++)
                ui_cmd_bc24(bc, 0);
            jmps = (int*)main_alloc(node->len * sizeof(int));
            for(i = 0; i < node->len; i++) {
                if(i) memcpy(bc->data + k + (i - 1) * 3, &bc->len, 3);
                ui_cmd_tobc(ctx, bc,&node->children[i], lvl + 1, e);
                if(i + 1 < node->len) {
                    ui_cmd_bc8(bc, TNG_BC_JMP);
                    jmps[i] = bc->len;
                    ui_cmd_bc24(bc, 0);
                }
            }
            for(i = 0; i < node->len - 1; i++)
                memcpy(bc->data + jmps[i], &bc->len, 3);
            free(jmps);
        break;
        case 4:
            node->err = !ui_cmd_expr(ctx, &expr, node->arg1); *e |= node->err;
            if(node->err) { if(expr.data) free(expr.data); return; }
            ui_cmd_bc(bc, expr.data, expr.len);
            free(expr.data);
            if(node->arg2 > 1) {
                ui_cmd_bc8(bc, TNG_BC_POP);
                ui_cmd_bc8(bc, node->arg2 - 2);
            } else {
                ui_cmd_bc8(bc, node->arg2 == 1 ? TNG_BC_POPO : TNG_BC_POPA);
                ui_cmd_bc24(bc, tng_str(ctx, project.attrs[node->arg3]));
            }
        break;
        default:
            if(node->type > ctx->hdr.maxbc) ctx->hdr.maxbc = node->type;
            for(j = 0, i = 2; i >= 0; i--) {
                if(cmd_types[node->type].par[i]) j++;
                switch(cmd_types[node->type].par[i]) {
                    case 0: break;
                    case FORM_EXPR:
                        node->err = !ui_cmd_expr(ctx, &expr, node->arg1); *e |= node->err;
                        if(node->err) { if(expr.data) free(expr.data); return; }
                        ui_cmd_bc(bc, expr.data, expr.len);
                        free(expr.data);
                    break;
                    case FORM_MAPSEL:
                        ui_cmd_bc8(bc, TNG_BC_PUSHMAP);
                        ui_cmd_bc24(bc, tng_str(ctx, project.maps[node->arg4]));
                        ui_cmd_bc32(bc, node->arg5 & 0x7fff7fff);
                    break;
                    case FORM_MUSSEL:
                        ui_cmd_bc8(bc, TNG_BC_PUSHMUS);
                        ui_cmd_bc24(bc, tng_str(ctx, project.music[node->arg2]));
                    break;
                    case FORM_SNDSEL:
                        ui_cmd_bc8(bc, TNG_BC_PUSHSND);
                        ui_cmd_bc24(bc, tng_str(ctx, project.sounds[node->arg2]));
                    break;
                    case FORM_SPCSEL:
                        ui_cmd_bc8(bc, TNG_BC_PUSHSPC);
                        ui_cmd_bc24(bc, tng_str(ctx, project.speech[node->arg2]));
                    break;
                    case FORM_CHRSEL:
                        ui_cmd_bc8(bc, TNG_BC_PUSHCHR);
                        ui_cmd_bc24(bc, tng_str(ctx, project.choosers[node->arg2]));
                    break;
                    case FORM_CUTSEL:
                        ui_cmd_bc8(bc, TNG_BC_PUSHCUT);
                        ui_cmd_bc24(bc, tng_str(ctx, project.cuts[node->arg2]));
                    break;
                    case FORM_DLGSEL:
                        ui_cmd_bc8(bc, TNG_BC_PUSHDLG);
                        ui_cmd_bc24(bc, tng_str(ctx, project.dialogs[node->arg2]));
                    break;
                    case FORM_OBJSEL:
                        ui_cmd_bc8(bc, TNG_BC_PUSHSPR);
                        ui_cmd_bc8(bc, 1);
                        ui_cmd_bc16(bc, project.sprites[1][node->arg2].idx);
                    break;
                    case FORM_CFTSEL:
                        ui_cmd_bc8(bc, TNG_BC_PUSHCFT);
                        ui_cmd_bc24(bc, tng_str(ctx, project.crafts[node->arg2]));
                    break;
                    case FORM_QSTSEL:
                        ui_cmd_bc8(bc, TNG_BC_PUSHQST);
                        ui_cmd_bc24(bc, tng_str(ctx, project.quests[node->arg2]));
                    break;
                    case FORM_NPCSEL:
                        ui_cmd_bc8(bc, TNG_BC_PUSHSPR);
                        ui_cmd_bc8(bc, PROJ_NUMSPRCATS - 2);
                        ui_cmd_bc16(bc, project.sprites[PROJ_NUMSPRCATS - 2][node->arg2].idx);
                    break;
                    case FORM_SPRITE:
                        ui_cmd_bc8(bc, TNG_BC_PUSHSPR);
                        ui_cmd_bc8(bc, cmd_types[node->type].par[3]);
                        ui_cmd_bc16(bc, project.sprites[cmd_types[node->type].par[3]][node->arg2].idx);
                    break;
                    case FORM_BEHSEL: case FORM_TRNSEL: case FORM_ACNSEL: case FORM_CLKSEL:
                    case FORM_DIRSEL: ui_cmd_bc8(bc, TNG_BC_PUSH8); ui_cmd_bc8(bc, node->arg2); break;
                    case FORM_ALERT: ui_cmd_bc8(bc, TNG_BC_PUSH8); ui_cmd_bc8(bc, node->arg2 + 2); break;
                    case FORM_BOOL: ui_cmd_bc8(bc, TNG_BC_PUSH8); ui_cmd_bc8(bc, node->arg3); break;
                    case FORM_TIME: case FORM_NUM: ui_cmd_bc8(bc, TNG_BC_PUSH24); ui_cmd_bc24(bc, node->arg3); break;
                    case FORM_ALTITUDE: ui_cmd_bc8(bc, TNG_BC_PUSH8); ui_cmd_bc8(bc, node->arg3); break;
                }
            }
            ui_cmd_bc8(bc, TNG_BC_FNC0 + j);
            ui_cmd_bc8(bc, node->type);
        break;
    }
}

/**
 * Get references from expression
 */
void ui_cmd_refexpr(tngctx_t *ctx, char *str)
{
    int i, l;
    char *s;

    if(!str || !*str) return;
    for(s = str; *s; s++) {
        if(!((*s >= '0' && *s <= '9') || *s == '(' || *s == ')' || *s == '<' || *s == '>' || *s == ',' || ISOP(*s))) {
            /* check one letter local variable names */
            if(*s == '@' || (*s >= 'a' && *s <= 'z' && (!s[1] || s[1] < 'a' || s[1] > 'z'))) {} else
            /* check functions */
            if(!memcmp(s, "min(", 4)) s += 2; else      /* skip minimum */
            if(!memcmp(s, "max(", 4)) s += 2; else      /* skip maximum */
            if(!memcmp(s, "rnd(", 4)) s += 2; else      /* skip random */
            if(!memcmp(s, "sum(", 4)) s += 2; else      /* skip total of attribute of items in inventory */
            if(!memcmp(s, "cnt(", 4)) {                 /* number of specific items in inventory */
                s += 4;
                if(*s != ')') {
                    if(!project.sprites[1]) return;
                    for(i = l = 0; i < project.spritenum[1]; i++) {
                        l = strlen(project.sprites[1][i].name);
                        if(!memcmp(s, project.sprites[1][i].name, l) && s[l] == ')') break;
                    }
                    if(i >= project.spritenum[1]) return;
                    objects_ref(ctx, i);
                    s += l;
                }
            } else {
                /* check attributes */
                if(!project.attrs) return;
                for(i = l = 0; i < project.numattrs; i++) {
                    l = strlen(project.attrs[i]);
                    if(!memcmp(s, project.attrs[i], l) && (!s[l] || ISOP(s[l]) || s[l] == ',' || s[l] == ')')) break;
                }
                if(i >= project.numattrs) return;
                attrs_ref(ctx, i);
                s += l - 1;
            }
        }
    }
}

/**
 * Get references from script node tree
 */
void ui_cmd_ref(tngctx_t *ctx, cmd_t *node, int lvl)
{
    int i;

    if(!ctx || !node || lvl < 0) return;
    switch(node->type) {
        case 0:
            for(i = 0; i < node->len; i++)
                ui_cmd_ref(ctx, &node->children[i], lvl + 1);
        break;
        case 1: break;
        case 2:
        case 3:
            ui_cmd_refexpr(ctx, node->arg1);
            for(i = 0; i < node->len; i++)
                ui_cmd_ref(ctx, &node->children[i], lvl + 1);
        break;
        case 4:
            ui_cmd_refexpr(ctx, node->arg1);
            if(node->arg2 < 2)
                attrs_ref(ctx, node->arg3);
        break;
        default:
            for(i = 2; i >= 0; i--) {
                switch(cmd_types[node->type].par[i]) {
                    case FORM_EXPR: ui_cmd_refexpr(ctx, node->arg1); break;
                    case FORM_MUSSEL: tng_ref(ctx, TNG_IDX_MUS, node->arg2); break;
                    case FORM_SNDSEL: tng_ref(ctx, TNG_IDX_SND, node->arg2); break;
                    case FORM_SPCSEL: tng_ref(ctx, TNG_IDX_SPC, node->arg2); break;
                    case FORM_CHRSEL: choosers_ref(ctx, node->arg2); break;
                    case FORM_CUTSEL: cutscn_ref(ctx, node->arg2); break;
                    case FORM_DLGSEL: dialogs_ref(ctx, node->arg2); break;
                    case FORM_OBJSEL: objects_ref(ctx, node->arg2); break;
                    case FORM_CFTSEL: crafts_ref(ctx, node->arg2); break;
                    case FORM_QSTSEL: quests_ref(ctx, node->arg2); break;
                    case FORM_NPCSEL: npcs_ref(ctx, node->arg2); break;
                    case FORM_SPRITE: tng_sprref(ctx, cmd_types[node->type].par[3], node->arg2); break;
                }
            }
        break;
    }
}

/**
 * Convert from bytecode to expression
 */
void ui_cmd_exprbc(tngctx_t *ctx, ui_bc_t *bc, char *out)
{
    int n;

    if(!ctx || !bc || !out) return;
    memset(out, 0, PROJ_EXPRMAX);
    /* TODO: postfix bytecode to infix string */
    switch(bc->data[0]) {
        case TNG_BC_PUSH8: sprintf(out, "%d", (int8_t)bc->data[1]); break;
        case TNG_BC_PUSH16: sprintf(out, "%d", *((int16_t*)&bc->data[1])); break;
        case TNG_BC_PUSH32: sprintf(out, "%d", *((int32_t*)&bc->data[1])); break;
        case TNG_BC_PUSH24:
            n = 0; memcpy(&n, bc->data + 1, 3); if(n & 0x800000) n |= 0xff000000;
            sprintf(out, "%d", n); break;
        break;
    }
}

/**
 * Convert from bytecode to a string
 */
void ui_cmd_frombc(tngctx_t *ctx, ui_bc_t *bc, int lvl, FILE *f)
{
    uint8_t *ptr, *end;
    int i, t = 1, n;

    if(!ctx || !bc || lvl < 0 || !f) return;
    /* TODO: convert bytecode to script, not just disassemble */
    ptr = bc->data;
    end = bc->data + bc->len;
    while(ptr < end && t) {
        for(i = 0; i <= lvl; i++) fprintf(f, "  ");
        fprintf(f, "%s %06x: ", cmd_types[1].name, (int)((uintptr_t)ptr - (uintptr_t)bc->data));
        t = *ptr++;
        switch(t) {
            case TNG_BC_END:    fprintf(f, "END"); break;
            case TNG_BC_SWITCH:
                fprintf(f, "SWITCH ");
                n = *ptr++;
                for(i = 0; i < n; i++, ptr += 3) fprintf(f, " %02x%02x%02x", ptr[2], ptr[1], ptr[0]);
            break;
            case TNG_BC_JMP:    fprintf(f, "JMP     %02x%02x%02x", ptr[2], ptr[1], ptr[0]); ptr += 3; break;
            case TNG_BC_JZ:     fprintf(f, "JZ      %02x%02x%02x", ptr[2], ptr[1], ptr[0]); ptr += 3; break;
            case TNG_BC_FNC0:
            case TNG_BC_FNC1:
            case TNG_BC_FNC2:
            case TNG_BC_FNC3:   fprintf(f, "CALL    %s(%d)", ptr[0] < 5 || ptr[0] >= (sizeof(cmd_types)/sizeof(cmd_types[0])) ?
                "???" : cmd_types[ptr[0]].name, t - TNG_BC_FNC0); ptr++; break;
            case TNG_BC_CNT0:   fprintf(f, "CNT0"); break;
            case TNG_BC_CNT1:   fprintf(f, "CNT1    1.%02X%02X", ptr[1], ptr[0]); ptr += 2; break;
            case TNG_BC_SUM:    fprintf(f, "SUM     "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_CNTO0:  fprintf(f, "CNTO0"); break;
            case TNG_BC_CNTO1:  fprintf(f, "CNTO1   1.%02X%02X", ptr[1], ptr[0]); ptr += 2; break;
            case TNG_BC_SUMO:   fprintf(f, "SUMO    "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_RND:    fprintf(f, "RND     %u", *((uint32_t*)ptr)); ptr += 4; break;
            case TNG_BC_MIN:    fprintf(f, "MIN     (%u)", ptr[0]); ptr++; break;
            case TNG_BC_MAX:    fprintf(f, "MAX     (%u)", ptr[0]); ptr++; break;
            case TNG_BC_ADD:    fprintf(f, "ADD     (2)"); break;
            case TNG_BC_SUB:    fprintf(f, "SUB     (2)"); break;
            case TNG_BC_MUL:    fprintf(f, "MUL     (2)"); break;
            case TNG_BC_DIV:    fprintf(f, "DIV     (2)"); break;
            case TNG_BC_MOD:    fprintf(f, "MOD     (2)"); break;
            case TNG_BC_EQ:     fprintf(f, "EQ      (2)"); break;
            case TNG_BC_NE:     fprintf(f, "NE      (2)"); break;
            case TNG_BC_GE:     fprintf(f, "GE      (2)"); break;
            case TNG_BC_GT:     fprintf(f, "GT      (2)"); break;
            case TNG_BC_LE:     fprintf(f, "LE      (2)"); break;
            case TNG_BC_LT:     fprintf(f, "LT      (2)"); break;
            case TNG_BC_NOT:    fprintf(f, "NOT     (1)"); break;
            case TNG_BC_OR:     fprintf(f, "OR      (2)"); break;
            case TNG_BC_AND:    fprintf(f, "AND     (2)"); break;
            case TNG_BC_POP:    fprintf(f, "POP     %c", ptr[0] < 26 ? ptr[0] + 'a' : '?'); ptr++; break;
            case TNG_BC_POPA:   fprintf(f, "POPA    "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_POPO:   fprintf(f, "POPO    "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_PUSH:   fprintf(f, "PUSH    %c", ptr[0] < 26 ? ptr[0] + 'a' : '?'); ptr++; break;
            case TNG_BC_PUSHA:  fprintf(f, "PUSHA   "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_PUSHO:  fprintf(f, "PUSHO   "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_PUSH8:  fprintf(f, "PUSH8   %u", ptr[0]); ptr++; break;
            case TNG_BC_PUSH16: fprintf(f, "PUSH16  %u", *((uint16_t*)ptr)); ptr += 2; break;
            case TNG_BC_PUSH24: fprintf(f, "PUSH24  %u", *((uint16_t*)ptr) | (ptr[2] << 16)); ptr += 3; break;
            case TNG_BC_PUSH32: fprintf(f, "PUSH32  %u", *((uint32_t*)ptr)); ptr += 4; break;
            case TNG_BC_PUSHMAP:
                fprintf(f, "PUSHMAP "); ptr = tng_wridx(ctx, ptr, f);
                fprintf(f, ",%u,%u", (ptr[1] << 8) | ptr[0], (ptr[3] << 8) | ptr[2]); ptr += 4;
            break;
            case TNG_BC_PUSHSPR:fprintf(f, "PUSHSPR %u.%02X%02X", ptr[0], ptr[2], ptr[1]); ptr += 3; break;
            case TNG_BC_PUSHMUS:fprintf(f, "PUSHMUS "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_PUSHSND:fprintf(f, "PUSHSND "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_PUSHSPC:fprintf(f, "PUSHSPC "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_PUSHCHR:fprintf(f, "PUSHCHR "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_PUSHCUT:fprintf(f, "PUSHCUT "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_PUSHDLG:fprintf(f, "PUSHDLG "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_PUSHCFT:fprintf(f, "PUSHCFT "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_PUSHQST:fprintf(f, "PUSHQST "); ptr = tng_wridx(ctx, ptr, f); break;
            case TNG_BC_PUSHTXT:fprintf(f, "PUSHTXT "); ptr = tng_wrtext(ctx, ptr, f); break;
            default: fprintf(f, "unknown bytecode %02x aborting disassembly", t); t = 0; break;
        }
        fprintf(f, "\r\n");
    }
}
