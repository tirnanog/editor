# WebP Codec

NOTE by bzt: I had to completely rewrite the build system and rewrote some
static functions in headers (!) as macros (fuck it, if you want to inline
those so desparately, just use a macro for Crist's sake, if inlining fails
then your static functions are polluting all the object files including your
headers, and will make static linking fail due to duplicated functions).
At least to say it is big pile of shit and that's only if I want to be polite.
You just can't compile the decoder without the encoder (which is just HUGE,
bigger than TNGE, and we don't need it anyway), talking about poorly designed
software...

Google, it is highly time to fire all your programmers and hire someone who
actually knows what he is doing and isn't just some copy'n'paste kid stealing
from other libraries...

Original README.md follows:

------------------------------------------------------------------------------

```
      __   __  ____  ____  ____
     /  \\/  \/  _ \/  _ )/  _ \
     \       /   __/  _  \   __/
      \__\__/\____/\_____/__/ ____  ___
            / _/ /    \    \ /  _ \/ _/
           /  \_/   / /   \ \   __/  \__
           \____/____/\_____/_____/____/v1.2.4
```

WebP codec is a library to encode and decode images in WebP format. This package
contains the library that can be used in other programs to add WebP support, as
well as the command line tools 'cwebp' and 'dwebp' to compress and decompress
images respectively.

See https://developers.google.com/speed/webp for details on the image format.

The latest source tree is available at
https://chromium.googlesource.com/webm/libwebp

It is released under the same license as the WebM project. See
https://www.webmproject.org/license/software/ or the "COPYING" file for details.
An additional intellectual property rights grant can be found in the file
PATENTS.

## Building

See the [building documentation](doc/building.md).

## Encoding and Decoding Tools

The examples/ directory contains tools to encode and decode images and
animations, view information about WebP images, and more. See the
[tools documentation](doc/tools.md).

## APIs

See the [APIs documentation](doc/api.md), and API usage examples in the
`examples/` directory.

## Bugs

Please report all bugs to the issue tracker: https://bugs.chromium.org/p/webp

Patches welcome! See [how to contribute](CONTRIBUTING.md).

## Discuss

Email: webp-discuss@webmproject.org

Web: https://groups.google.com/a/webmproject.org/group/webp-discuss
