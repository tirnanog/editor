#ifndef WEBP_WEBP_MUX_TYPES_H_
#define WEBP_WEBP_MUX_TYPES_H_

#include <string.h>
#include "./types.h"

#ifdef __cplusplus
extern "C" {
#endif
typedef struct WebPData WebPData;

typedef enum WebPFeatureFlags {
  ANIMATION_FLAG  = 0x00000002,
  XMP_FLAG        = 0x00000004,
  EXIF_FLAG       = 0x00000008,
  ALPHA_FLAG      = 0x00000010,
  ICCP_FLAG       = 0x00000020,

  ALL_VALID_FLAGS = 0x0000003e
} WebPFeatureFlags;
typedef enum WebPMuxAnimDispose {
  WEBP_MUX_DISPOSE_NONE,
  WEBP_MUX_DISPOSE_BACKGROUND
} WebPMuxAnimDispose;
typedef enum WebPMuxAnimBlend {
  WEBP_MUX_BLEND,
  WEBP_MUX_NO_BLEND
} WebPMuxAnimBlend;
struct WebPData {
  const uint8_t* bytes;
  size_t size;
};
/* whoever put these function (!) in a PUBLIC HEADER file, should be hang and rotten in Hell in eternity */
void WebPDataInit(WebPData* webp_data);
void WebPDataClear(WebPData* webp_data);
int WebPDataCopy(const WebPData* src, WebPData* dst);

#ifdef __cplusplus
}
#endif

#endif
