/*
 * tnge/chars.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Player character options window
 *
 */

#include "main.h"

void chars_load(void *data);
void chars_new(void *data);
void chars_save(void *data);
void chars_delete(void *data);
void chars_preview(void *data);
void chars_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void chars_optdel(void *data);
void chars_optadd(void *data);
void chars_drawopt(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void chars_attrdel(void *data);
void chars_attradd(void *data);
void chars_drawattr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void chars_invdel(void *data);
void chars_invadd(void *data);
void chars_drawinv(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void chars_lyrdel(void *data);
void chars_lyradd(void *data);
void chars_lyrclk(void *data);
void chars_lyrup(void *data);
void chars_lyrdown(void *data);
void chars_lyrcopy(void *data);
void chars_lyrcut(void *data);
void chars_lyrpaste(void *data);
void chars_drawspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void chars_renderspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

SDL_Texture *chars_anim = NULL;
char chars_name[PROJ_NAMEMAX] = { 0 }, chars_base[PROJ_NAMEMAX] = { 0 }, chars_optname[PROJ_NAMEMAX] = { 0 };
char chars_opt[PROJ_NAMEMAX] = { 0 };
int chars_baseopt = 0, chars_err = 0;
ui_input_t chars_nameinp = { INP_ID, sizeof(chars_name), chars_name };
ui_input_t chars_optinp = { INP_NAME, sizeof(chars_opt), chars_opt };
ui_input_t chars_descinp = { INP_TEXT, PROJ_DESCMAX, NULL };
ui_select_t chars_attr = { 0, 0, NULL };
ui_select_t chars_rel = { -1, LANG_SET, project_rels };
ui_num_t chars_val = { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 };
ui_sprsel_t chars_spr_hidden = { -1, 2, 25, 0 };
ui_sprsel_t chars_spr_hidden2 = { -1, 3, 186, 1 };
ui_num_t chars_qty = { 1, 1, 99, 1 };
ui_sprsel_t chars_obj = { -1, 1, 158, 3 };
ui_num_t chars_deltay = { 0, 0, 255, 1 };
ui_sprsel_t chars_pal = { -1, 0, 897, 2 };

ui_tablehdr_t chars_hdr[] = {
    { CHARS_LIST, 0, 0, 0 },
    { 0 }
};
ui_table_t chars_tbl = { chars_hdr, CHARS_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), chars_drawcell, NULL,
    NULL, NULL };

ui_tablehdr_t chars_optshdr[] = {
    { CHARS_OPTIONS, 0, 0, 0 },
    { 0 }
};
ui_table_t chars_opts = { chars_optshdr, CHARS_NOOPT, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(chars_opt_t), chars_drawopt,
    NULL, NULL, NULL };

ui_tablehdr_t chars_attrshdr[] = {
    { -1, 20, 0, 0 },
    { CHARS_ATTRS, 0, 0, 0 },
    { LANG_VALUE, 82, 0, 0 },
    { 0 }
};
ui_table_t chars_attrs = { chars_attrshdr, CHARS_NOATTR, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(chars_attrs_t), chars_drawattr,
    NULL, NULL, NULL };

ui_tablehdr_t chars_invshdr[] = {
    { -1, 20, 0, 0 },
    { CHARS_INVENTORY, 0, 0, 0 },
    { 0 }
};
ui_table_t chars_invs = { chars_invshdr, CHARS_NOINV, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(chars_inv_t), chars_drawinv,
    NULL, NULL, NULL };

ui_tablehdr_t chars_sprshdr[] = {
    { NUMTEXTS + ICON_IDLE, 20, CHARS_ANIM_IDLE, 0 },
    { NUMTEXTS + ICON_WALK, 20, CHARS_ANIM_WALK, 0 },
    { NUMTEXTS + ICON_CLIMB, 20, CHARS_ANIM_CLIMB, 0 },
    { NUMTEXTS + ICON_JUMP, 20, CHARS_ANIM_JUMP, 0 },
    { NUMTEXTS + ICON_RUN, 20, CHARS_ANIM_RUN, 0 },
    { NUMTEXTS + ICON_SWIM, 20, CHARS_ANIM_SWIM, 0 },
    { NUMTEXTS + ICON_FLY, 20, CHARS_ANIM_FLY, 0 },
    { NUMTEXTS + ICON_SHIELD, 20, CHARS_ANIM_BLOCK, 0 },
    { NUMTEXTS + ICON_HURT, 20, CHARS_ANIM_HURT, 0 },
    { NUMTEXTS + ICON_DEATH, 20, CHARS_ANIM_DIE, 0 },
    { NUMTEXTS + ICON_NUMACT + 1, 20, CHARS_ANIM_A1, 0 },
    { NUMTEXTS + ICON_NUMACT + 2, 20, CHARS_ANIM_A2, 0 },
    { NUMTEXTS + ICON_NUMACT + 3, 20, CHARS_ANIM_A3, 0 },
    { NUMTEXTS + ICON_NUMACT + 4, 20, CHARS_ANIM_A4, 0 },
    { NUMTEXTS + ICON_NUMACT + 5, 20, CHARS_ANIM_A5, 0 },
    { NUMTEXTS + ICON_NUMACT + 6, 20, CHARS_ANIM_A6, 0 },
    { NUMTEXTS + ICON_NUMACT + 7, 20, CHARS_ANIM_A7, 0 },
    { NUMTEXTS + ICON_NUMACT + 8, 20, CHARS_ANIM_A8, 0 },
    { NUMTEXTS + ICON_NUMACT + 9, 20, CHARS_ANIM_A9, 0 },
    { NUMTEXTS + ICON_PORT, 20, NPCS_PORTRAIT, 0 },
    { 0 }
};
ui_table_t chars_sprs = { chars_sprshdr, CHARS_NOLYR, 20, 20, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(int), chars_drawspr,
    chars_renderspr, NULL, NULL };

/**
 * The form
 */
ui_form_t chars_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, CHARS_DELETE, (void*)ICON_REMOVE, chars_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, chars_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, CHARS_PREVIEW, (void*)ICON_PVIEW, chars_preview },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, chars_new },
    { FORM_INPUT, 0, 30, 200, 18, 0, CHARS_NAME, &chars_nameinp, NULL },
    { FORM_TABLE, 10, 30, 200, 0, 0, 0, &chars_tbl, chars_load },
    /* 6 */
    { FORM_TABLE, 0, 54, 0, 0, 0, 0, &chars_opts, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_DELOPT, (void*)ICON_REMOVE, chars_optdel },
    { FORM_INPUT, 0, 0, 0, 20, 0, CHARS_OPT, &chars_optinp, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_ADDOPT, (void*)ICON_ADD, chars_optadd },
    /* 10 */
    { FORM_TABLE, 0, 54, 415, 100, 0, 0, &chars_attrs, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_DELATTR, (void*)ICON_REMOVE, chars_attrdel },
    { FORM_SELECT, 0, 0, 0, 20, 0, 0, &chars_attr, NULL },
    { FORM_SELECT, 0, 0, 40, 20, 0, CHARS_RELATTR, &chars_rel, NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, 0, &chars_val, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_ADDATTR, (void*)ICON_ADD, chars_attradd },
    /* 16 */
    { FORM_TABLE, 0, 54, 415, 100, 0, 0, &chars_invs, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_DELINV, (void*)ICON_REMOVE, chars_invdel },
    { FORM_NUM, 0, 0, 48, 20, 0, 0, &chars_qty, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, 0, &chars_obj, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_ADDINV, (void*)ICON_ADD, chars_invadd },
    /* 21 */
    { FORM_TABLE, 420, 154, 415, 158, 0, 0, &chars_sprs, chars_lyrclk },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_DELLYR, (void*)ICON_REMOVE, chars_lyrdel },
    { FORM_CHARBTN, 0, 0, 20, 20, 0, CHARS_UPLYR, (void*)11014, chars_lyrup },
    { FORM_CHARBTN, 0, 0, 20, 20, 0, CHARS_DWNLYR, (void*)11015, chars_lyrdown },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_ADDLYR, (void*)ICON_ADD, chars_lyradd },
    /* 26 */
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_PSTLYR, (void*)ICON_PASTE, chars_lyrpaste },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_CUTLYR, (void*)ICON_CUT, chars_lyrcut },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHARS_CPYLYR, (void*)ICON_COPY, chars_lyrcopy },
    /* 29 */
    { FORM_BOOL, 0, 31, 136, 20, 0, CHARS_BASE, &chars_baseopt, NULL },
    { FORM_NUM, 0, 31, 56, 20, 0, CHARS_BASELINE, &chars_deltay, NULL },
    { FORM_SPRITE, 0, 31, 20, 20, 0, CHARS_PALETTE, &chars_pal, NULL },
    { FORM_INPUT, 0, 0, 415, 18, 0, QUESTS_DESC, &chars_descinp, NULL },

    { FORM_LAST }
};

/**
 * Draw table cell
 */
void chars_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    char **opt = (char**)data;

    (void)idx; (void)sel; (void)w; (void)h; (void)hdr;
    if(!data) return;
    if(!strcmp(chars_base, *opt))
        ui_icon(dst, x + 4, y + 1, ICON_CHAR);
    ui_text(dst, x + 24, y + 1, *opt);
}

/**
 * Draw table cell
 */
void chars_drawopt(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    chars_opt_t *opt = (chars_opt_t*)data;

    (void)idx; (void)sel; (void)w; (void)h; (void)hdr;
    if(!data) return;
    ui_text(dst, x + 4, y + 1, opt->opt);
}

/**
 * Draw table cell
 */
void chars_drawattr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    chars_attrs_t *attr = (chars_attrs_t*)data;
    char tmp[16];

    (void)idx; (void)sel; (void)w; (void)h;
    if(!data) return;
    if(attr->rel >= 0)
        ui_icon(dst, x + 4, y + 1, ICON_QMARK);
    ui_text(dst, x + 4 + hdr[0].w, y + 1, project.attrs[attr->attr]);
    if(attr->rel >= 0)
        ui_text(dst, x + 4 + hdr[0].w + hdr[1].w, y + 1, project_rels[attr->rel]);
    sprintf(tmp, "%6d", attr->val);
    ui_text(dst, x + 20 + hdr[0].w + hdr[1].w, y + 1, tmp);
}

/**
 * Draw table cell
 */
void chars_drawinv(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    chars_inv_t *inv = (chars_inv_t*)data;
    char tmp[16];

    (void)idx; (void)sel; (void)w; (void)h;
    if(!data) return;
    sprintf(tmp, "%2d", inv->qty);
    ui_text(dst, x + 4 + hdr[0].w, y + 1, tmp);
    ui_text(dst, x + 24 + hdr[0].w, y + 1, project.sprites[1][inv->obj].name);
}

/**
 * Draw table cell
 */
void chars_drawspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    ui_sprite_t *s;
    SDL_Rect rect;

    (void)hdr;
    if(!data) return;
    s = spr_getidx((idx % SPRPERLYR) == SPRPERLYR - 1 ? chars_spr_hidden2.cat : chars_spr_hidden.cat, *((int*)data), -1, 0);
    if(!s)
        ui_icon(dst, x + 2, y, ICON_INADD);
    else
    if(idx != sel) {
        ui_fit(w, h, s->w, s->h, &rect.w, &rect.h);
        rect.x = x + (w - rect.w) / 2; rect.y = y + h - rect.h;
        spr_blit(s, 160, dst, &rect);
    } else
        spr_texture(s, &chars_anim);
}

/**
 * Draw table cell
 */
void chars_renderspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    SDL_Rect rect;

    (void)dst; (void)hdr;
    if(!data || !chars_anim || *((int*)data) < 0 || idx != sel) return;
    rect.x = x; rect.y = y; rect.w = w; rect.h = h;
    spr_render(spr_getidx((idx % SPRPERLYR) == SPRPERLYR - 1 ? chars_spr_hidden2.cat : chars_spr_hidden.cat,*((int*)data), -1, 1),
        -1, chars_anim, &rect);
}

/**
 * Free all resources
 */
void chars_free()
{
    chars_new(NULL);
    chars_exit(SUBMENU_CHAR);
}

/**
 * Exit character options window
 */
void chars_exit(int tab)
{
    (void)tab;
    chars_tbl.val = chars_tbl.clk = -1;
    if(chars_anim) SDL_DestroyTexture(chars_anim);
    chars_anim = NULL;
}

/**
 * Enter character options window
 */
void chars_init(int tab)
{
    chars_exit(tab);
    chars_form[1].param = lang[LANG_SAVE];
    chars_form[1].w = ui_textwidth(chars_form[1].param) + 40;
    if(chars_form[1].w < 200) chars_form[1].w = 200;
    chars_form[6].x = chars_form[7].x = chars_form[29].x = chars_form[5].x + chars_form[5].w + 10;
    chars_form[29].w = ui_textwidth(lang[CHARS_BASE]) + 20;
    chars_form[30].x = chars_form[29].x + chars_form[29].w + 4;
    chars_form[31].x = chars_form[30].x + chars_form[30].w + 8;
    project_freedir(&project.chars, &project.numchars);
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_GAME]);
    project.chars = project_getdir(projdir, ".opt", &project.numchars);
    chars_tbl.data = project.chars;
    chars_tbl.num = project.numchars;
    if(chars_tbl.val >= chars_tbl.num) chars_tbl.val = chars_tbl.num - 1;
    if(!chars_name[0]) chars_tbl.val = -1;
    if(!chars_opts.num) chars_opts.val = -1;
    chars_attr.opts = project.attrs;
    chars_loadcfg(NULL);
}

/**
 * Resize the view
 */
void chars_resize(int tab, int w, int h)
{
    (void)tab;
    chars_form[0].y = chars_form[1].y = chars_form[2].y = chars_form[3].y = h - 48;
    chars_form[1].x = w - 20 - chars_form[1].w;
    chars_form[2].x = chars_form[1].x - 52;
    ui_table_resize(&chars_form[5], chars_form[5].w, h - 89);
    chars_form[3].x = chars_form[5].x + chars_form[5].w + 20;
    chars_form[4].x = w - 10 - chars_form[4].w;
    chars_form[10].x = chars_form[11].x = chars_form[16].x = chars_form[17].x = w - 10 - chars_form[10].w;
    ui_table_resize(&chars_form[6], chars_form[10].x - 10 - chars_form[6].x, h - 82 - chars_form[6].y);
    ui_table_resize(&chars_form[10], chars_form[10].w, (h - 82 - 60 - 24 - chars_form[21].h - chars_form[6].y) / 2);
    ui_table_resize(&chars_form[16], chars_form[16].w, (h - 82 - 60 - 24 - chars_form[21].h - chars_form[6].y) / 2);
    chars_form[8].x = chars_form[7].x + chars_form[7].w + 10;
    chars_form[9].x = chars_form[6].x + chars_form[6].w - chars_form[9].w;
    chars_form[8].w = chars_form[9].x - 4 - chars_form[8].x;
    chars_form[7].y = chars_form[8].y = chars_form[9].y = chars_form[32].y = h - 48 - 30;
    chars_form[22].y = chars_form[23].y = chars_form[24].y =
        chars_form[25].y = chars_form[26].y = chars_form[27].y = chars_form[28].y = h - 48 - 30 - 24;
    chars_form[11].y = chars_form[12].y = chars_form[13].y = chars_form[14].y = chars_form[15].y =
        chars_form[10].y + chars_form[10].h + 2;
    chars_form[12].x = chars_form[11].x + chars_form[11].w + 10;
    chars_form[15].x = w - 10 - chars_form[15].w;
    chars_form[14].x = chars_form[15].x - 4 - chars_form[14].w;
    chars_form[13].x = chars_form[14].x - 4 - chars_form[13].w;
    chars_form[12].w = chars_form[13].x - 4 - chars_form[12].x;
    chars_form[16].y = chars_form[11].y + 28;
    chars_form[17].y = chars_form[18].y = chars_form[19].y = chars_form[20].y = chars_form[16].y + chars_form[16].h + 2;
    chars_form[18].x = chars_form[17].x + chars_form[17].w + 10;
    chars_form[19].x = chars_form[18].x + chars_form[18].w + 4;
    chars_form[20].x = w - 10 - chars_form[20].w;
    chars_form[19].w = chars_form[20].x - 4 - chars_form[19].x;
    chars_form[21].x = chars_form[22].x = chars_form[32].x = w - 10 - chars_form[21].w;
    chars_form[21].y = h - 82 - chars_form[21].h - 24;
    ui_table_resize(&chars_form[21], chars_form[21].w, chars_form[21].h);
    chars_form[25].x = w - 10 - chars_form[25].w;
    chars_form[24].x = chars_form[25].x - 10 - chars_form[24].w;
    chars_form[23].x = chars_form[24].x - 4 - chars_form[23].w;
    chars_form[26].x = chars_form[23].x - 100 - chars_form[26].w;
    chars_form[27].x = chars_form[26].x - 4 - chars_form[27].w;
    chars_form[28].x = chars_form[27].x - 4 - chars_form[28].w;
}

/**
 * View layer
 */
void chars_redraw(int tab)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    int i;

    (void)tab;
    ui_form = chars_form;
    ui_form[0].inactive = (chars_tbl.val < 0);
    ui_form[1].inactive = ui_form[24].inactive = (!chars_name[0] || !chars_opts.num);
    ui_form[2].inactive = ui_form[31].inactive = (!chars_name[0] || !chars_opts.num ||
        chars_opts.val < 0 || !opts || !opts[chars_opts.val].numsprs);
    for(i = 6; i < 10; i++)
        ui_form[i].inactive = (!chars_name[0]);
    for(i = 10; i < 29; i++)
        ui_form[i].inactive = (!chars_name[0] || chars_opts.val < 0);
    ui_form[20].inactive = (!chars_name[0] || chars_opts.val < 0 || chars_obj.val < 0);
    ui_form[30].inactive = !chars_baseopt;
    ui_form[32].inactive = (!chars_name[0] || chars_opts.val < 0);
    if(opts && chars_opts.val >= 0 && chars_opts.val < chars_opts.num) {
        chars_attrs.data = opts[chars_opts.val].attrs;
        chars_attrs.num = opts[chars_opts.val].numattrs;
        chars_invs.data = opts[chars_opts.val].invs;
        chars_invs.num = opts[chars_opts.val].numinvs;
        chars_sprs.data = opts[chars_opts.val].sprs;
        chars_sprs.num = opts[chars_opts.val].numsprs;
        chars_descinp.str = opts[chars_opts.val].desc;
    }
    if(!chars_attrs.num) chars_attrs.val = -1;
    if(!chars_invs.num) chars_invs.val = -1;
    if(!chars_sprs.num) chars_sprs.val = chars_sprs.clk = -1;
    if(chars_sprs.num && chars_sprs.val == -1) chars_sprs.val = chars_sprs.clk = 0;
    if(chars_name[0]) {
        ui_form[7].inactive = (chars_opts.val < 0);
        ui_form[9].inactive = (!chars_opt[0]);
        if(chars_opts.val >= 0) {
            ui_form[11].inactive = (chars_attrs.val < 0);
            ui_form[17].inactive = (chars_invs.val < 0);
            ui_form[22].inactive = (chars_sprs.val < 0);
            ui_form[23].inactive = (chars_sprs.val < SPRPERLYR);
            ui_form[24].inactive = (chars_sprs.val < 0 || chars_sprs.val >= chars_sprs.num - SPRPERLYR);
            ui_form[26].inactive = (!ui_sprite_clpbrd || !ui_sprite_clpbrd_num);
            ui_form[27].inactive = ui_form[28].inactive = (!chars_sprs.num);
        }
    }
}

/**
 * Erase a character option
 */
void chars_optdel(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num) return;
    if(opts[chars_opts.val].attrs) free(opts[chars_opts.val].attrs);
    if(opts[chars_opts.val].invs) free(opts[chars_opts.val].invs);
    if(opts[chars_opts.val].sprs) free(opts[chars_opts.val].sprs);
    memcpy(&opts[chars_opts.val], &opts[chars_opts.val + 1], (chars_opts.num - chars_opts.val) *
        sizeof(chars_opt_t));
    chars_opts.num--;
    if(chars_opts.val >= chars_opts.num)
        chars_opts.val = chars_opts.num - 1;
    if(!chars_opts.num) { free(opts); chars_opts.data = NULL; }
}

/**
 * Helper to sort options
 */
int chars_optcmp(const void *a, const void *b)
{
    return strcasecmp(((chars_opt_t*)a)->opt, ((chars_opt_t*)b)->opt);
}

/**
 * Add a new character option
 */
void chars_optadd(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    int i;

    (void)data;
    if(!chars_opt[0]) return;
    opts = (chars_opt_t*)realloc(opts, (chars_opts.num + 1) * sizeof(chars_opt_t));
    chars_opts.data = opts;
    if(!opts) main_error(ERR_MEM);
    i = chars_opts.num++;
    memset(&opts[i], 0, sizeof(chars_opt_t));
    strcpy(opts[i].opt, chars_opt);
    qsort(opts, chars_opts.num, sizeof(chars_opt_t), chars_optcmp);
    for(chars_opts.val = 0; chars_opts.val < chars_opts.num && strcmp(opts[chars_opts.val].opt, chars_opt); chars_opts.val++);
    chars_opt[0] = 0;
}

/**
 * Erase an attribute from character option
 */
void chars_attrdel(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    chars_attrs_t *attrs;

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num ||
      chars_attrs.val < 0 || chars_attrs.val >= chars_attrs.num) return;
    attrs = opts[chars_opts.val].attrs;
    memcpy(&attrs[chars_attrs.val], &attrs[chars_attrs.val + 1], (opts[chars_opts.val].numattrs - chars_attrs.val) *
        sizeof(chars_attrs_t));
    opts[chars_opts.val].numattrs--;
    if(chars_attrs.val >= opts[chars_opts.val].numattrs)
        chars_attrs.val = opts[chars_opts.val].numattrs - 1;
    if(!opts[chars_opts.val].numattrs) { free(attrs); opts[chars_opts.val].attrs = NULL; }
}

/**
 * Add a new attribute to character option
 */
void chars_attradd(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    chars_attrs_t *attrs;
    int i;

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num) return;
    attrs = opts[chars_opts.val].attrs;
    for(i = 0; attrs && i < opts[chars_opts.val].numattrs; i++)
        if(attrs[i].attr == chars_attr.val) {
            chars_attrs.val = i;
            attrs[i].rel = chars_rel.val;
            attrs[i].val = chars_val.val;
            return;
        }
    attrs = (chars_attrs_t*)realloc(attrs, (opts[chars_opts.val].numattrs + 1) * sizeof(chars_attrs_t));
    opts[chars_opts.val].attrs = attrs;
    if(!attrs) main_error(ERR_MEM);
    i = chars_attrs.val = opts[chars_opts.val].numattrs++;
    attrs[i].rel = chars_rel.val;
    attrs[i].val = chars_val.val;
    attrs[i].attr = chars_attr.val;
}

/**
 * Erase an inventory item from character option
 */
void chars_invdel(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    chars_inv_t *invs;

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num ||
      chars_invs.val < 0 || chars_invs.val >= chars_invs.num) return;
    invs = opts[chars_opts.val].invs;
    memcpy(&invs[chars_invs.val], &invs[chars_invs.val + 1], (opts[chars_opts.val].numinvs - chars_invs.val) *
        sizeof(chars_inv_t));
    opts[chars_opts.val].numinvs--;
    if(chars_invs.val >= opts[chars_opts.val].numinvs)
        chars_invs.val = opts[chars_opts.val].numinvs - 1;
    if(!opts[chars_opts.val].numinvs) { free(invs); opts[chars_opts.val].invs = NULL; }
}

/**
 * Add a new inventory item to character option
 */
void chars_invadd(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    chars_inv_t *invs;
    int i;

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num || chars_obj.val < 0) return;
    invs = opts[chars_opts.val].invs;
    for(i = 0; invs && i < opts[chars_opts.val].numinvs; i++)
        if(invs[i].obj == chars_obj.val) {
            chars_invs.val = i;
            invs[i].qty = chars_qty.val;
            return;
        }
    invs = (chars_inv_t*)realloc(invs, (opts[chars_opts.val].numinvs + 1) * sizeof(chars_inv_t));
    opts[chars_opts.val].invs = invs;
    if(!invs) main_error(ERR_MEM);
    i = chars_invs.val = opts[chars_opts.val].numinvs++;
    invs[i].qty = chars_qty.val;
    invs[i].obj = chars_obj.val;
}

/**
 * Erase a sprite layer from character option
 */
void chars_lyrdel(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    int *lyrs, i;

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num ||
      chars_sprs.val < 0 || chars_sprs.val >= chars_sprs.num) return;
    lyrs = opts[chars_opts.val].sprs;
    i = chars_sprs.val - (chars_sprs.val % SPRPERLYR);
    memcpy(&lyrs[i], &lyrs[i + SPRPERLYR], (opts[chars_opts.val].numsprs - i - SPRPERLYR) * sizeof(int));
    opts[chars_opts.val].numsprs -= SPRPERLYR;
    if(chars_sprs.val >= opts[chars_opts.val].numsprs)
        chars_sprs.val -= SPRPERLYR;
    if(!opts[chars_opts.val].numsprs) { free(lyrs); opts[chars_opts.val].sprs = NULL; }
}

/**
 * Add a new sprite layer to character option
 */
void chars_lyradd(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    int i;

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num) return;
    opts[chars_opts.val].sprs = (int*)realloc(opts[chars_opts.val].sprs, (opts[chars_opts.val].numsprs + SPRPERLYR)*sizeof(int));
    if(!opts) main_error(ERR_MEM);
    chars_sprs.val = opts[chars_opts.val].numsprs;
    for(i = 0; i < SPRPERLYR; i++) opts[chars_opts.val].sprs[chars_sprs.val + i] = -1;
    opts[chars_opts.val].numsprs += SPRPERLYR;
}

/**
 * Click on a sprite on a layer, pop up sprite picker
 */
void chars_lyrclk(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num ||
      chars_sprs.val < 0 || chars_sprs.val >= chars_sprs.num) return;
    if(ui_curbtn == 1) {
        ui_pressed = -1;
        ui_sprite_ptr = &opts[chars_opts.val].sprs[chars_sprs.val];
        ui_sprite_picker(scr_w, scr_h, (chars_sprs.val % SPRPERLYR) == SPRPERLYR - 1 ? &chars_spr_hidden2 : &chars_spr_hidden);
    } else
        opts[chars_opts.val].sprs[chars_sprs.val] = -1;
}

/**
 * Move sprite layer up
 */
void chars_lyrup(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    int *lyrs, i, tmp[SPRPERLYR];

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num ||
      chars_sprs.val < SPRPERLYR || chars_sprs.val >= chars_sprs.num) return;
    lyrs = opts[chars_opts.val].sprs;
    i = chars_sprs.val - (chars_sprs.val % SPRPERLYR);
    memcpy(&tmp, &lyrs[i - SPRPERLYR], SPRPERLYR * sizeof(int));
    memcpy(&lyrs[i - SPRPERLYR], &lyrs[i], SPRPERLYR * sizeof(int));
    memcpy(&lyrs[i], &tmp, SPRPERLYR * sizeof(int));
    chars_sprs.val -= SPRPERLYR;
}

/**
 * Move sprite layer down
 */
void chars_lyrdown(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    int *lyrs, i, tmp[SPRPERLYR];

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num ||
      chars_sprs.val < 0 || chars_sprs.val >= chars_sprs.num - SPRPERLYR) return;
    lyrs = opts[chars_opts.val].sprs;
    i = chars_sprs.val - (chars_sprs.val % SPRPERLYR);
    memcpy(&tmp, &lyrs[i + SPRPERLYR], SPRPERLYR * sizeof(int));
    memcpy(&lyrs[i + SPRPERLYR], &lyrs[i], SPRPERLYR * sizeof(int));
    memcpy(&lyrs[i], &tmp, SPRPERLYR * sizeof(int));
    chars_sprs.val += SPRPERLYR;
}

/**
 * Copy sprite layers to clipboard
 */
void chars_lyrcopy(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num || !opts[chars_opts.val].numsprs) return;
    ui_sprite_clpbrd_num = opts[chars_opts.val].numsprs;
    ui_sprite_clpbrd = (int*)realloc(ui_sprite_clpbrd, ui_sprite_clpbrd_num * sizeof(int));
    if(ui_sprite_clpbrd) memcpy(ui_sprite_clpbrd, opts[chars_opts.val].sprs, ui_sprite_clpbrd_num * sizeof(int));
}

/**
 * Cut sprite layers to clipboard
 */
void chars_lyrcut(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num || !opts[chars_opts.val].numsprs) return;
    if(ui_sprite_clpbrd) free(ui_sprite_clpbrd);
    ui_sprite_clpbrd = opts[chars_opts.val].sprs;
    opts[chars_opts.val].sprs = NULL;
    ui_sprite_clpbrd_num = opts[chars_opts.val].numsprs;
    opts[chars_opts.val].numsprs = 0;
}

/**
 * Paste sprite layers from clipboard
 */
void chars_lyrpaste(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;

    (void)data;
    if(!opts || chars_opts.val < 0 || chars_opts.val >= chars_opts.num || !ui_sprite_clpbrd || !ui_sprite_clpbrd_num) return;
    opts[chars_opts.val].sprs = (int*)realloc(opts[chars_opts.val].sprs,
        (opts[chars_opts.val].numsprs + ui_sprite_clpbrd_num) * sizeof(int));
    if(opts[chars_opts.val].sprs) {
        memcpy(opts[chars_opts.val].sprs + opts[chars_opts.val].numsprs, ui_sprite_clpbrd, ui_sprite_clpbrd_num * sizeof(int));
        chars_sprs.val = opts[chars_opts.val].numsprs;
        opts[chars_opts.val].numsprs += ui_sprite_clpbrd_num;
    } else
        opts[chars_opts.val].numsprs = 0;
}

/**
 * Clear form, new character option list
 */
void chars_new(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    int i;

    (void)data;
    chars_name[0] = chars_opt[0] = 0;
    chars_rel.val = chars_obj.val = chars_pal.val = -1;
    chars_val.val = chars_attr.val = 0;
    chars_qty.val = 1;
    if(opts) {
        for(i = 0; i < chars_opts.num; i++) {
            if(opts[i].attrs) free(opts[i].attrs);
            if(opts[i].invs) free(opts[i].invs);
            if(opts[i].sprs) free(opts[i].sprs);
        }
        free(opts); chars_opts.data = NULL;
    }
    chars_opts.num = chars_attrs.num = chars_invs.num = chars_sprs.num = 0;
    chars_opts.clk = chars_opts.val = chars_attrs.clk = chars_attrs.val = chars_invs.clk = chars_invs.val =
        chars_sprs.clk = chars_sprs.val = -1;
}

/**
 * Delete character option list
 */
void chars_delete(void *data)
{
    char **list = (char**)chars_tbl.data;

    (void)data;
    if(chars_tbl.val < 0 || !list) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.opt", project.id, project_dirs[PROJDIRS_GAME], list[chars_tbl.val]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("chars_delete: removing %s\n", projdir);
        remove(projdir);
        chars_new(NULL);
        chars_init(SUBMENU_CHAR);
    }
}

/**
 * Save character option list
 */
void chars_save(void *data)
{
    char **list = (char**)chars_tbl.data;
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    FILE *f;
    int i, j;

    (void)data;
    if(!chars_name[0]) return;
    /* save base character configuration */
    if(chars_baseopt) strcpy(chars_base, chars_name);
    else if(!strcmp(chars_name, chars_base)) chars_base[0] = 0;
    f = project_savefile(0, project_dirs[PROJDIRS_GAME], "character", "cfg", PROJMAGIC_CHARCONF, "chars_save");
    if(f) {
        fprintf(f, "%u %s\r\n", chars_deltay.val, chars_base);
        fclose(f);
    }
    /* save character options */
    f = project_savefile(!list || chars_tbl.val < 0 || strcmp(list[chars_tbl.val], chars_name),
        project_dirs[PROJDIRS_GAME], chars_name, "opt", PROJMAGIC_CHAROPT, "chars_save");
    if(f) {
        fprintf(f, "%s\r\n", chars_optname[0] ? chars_optname : "-");
        if(chars_pal.val != -1) {
            fprintf(f, "p ");
            project_wrsprite(f, &chars_pal);
            fprintf(f, "\r\n");
        }
        for(i = 0; i < chars_opts.num; i++) {
            fprintf(f, "o %s\r\n", opts[i].opt);
            if(opts[i].desc[0])
                fprintf(f, " d %s\r\n", opts[i].desc);
            for(j = 0; j < opts[i].numattrs; j++)
                if(opts[i].attrs[j].rel < 0)
                    fprintf(f, " a %s %d\r\n", project.attrs[opts[i].attrs[j].attr], opts[i].attrs[j].val);
                else
                    fprintf(f, " r %s%s%d\r\n", project.attrs[opts[i].attrs[j].attr], project_rels[opts[i].attrs[j].rel],
                        opts[i].attrs[j].val);
            for(j = 0; j < opts[i].numinvs; j++) {
                fprintf(f, " i %u ", opts[i].invs[j].qty);
                project_wrsprite2(f, opts[i].invs[j].obj, 2);
                fprintf(f, "\r\n");
            }
            for(j = 0; j < opts[i].numsprs; j++) {
                if(!(j % SPRPERLYR)) fprintf(f, "%s l ", j ? "\r\n" : "");
                else fprintf(f, " ");
                project_wrsprite2(f, opts[i].sprs[j], (j % SPRPERLYR) == SPRPERLYR - 1 ? chars_spr_hidden2.cat :
                    chars_spr_hidden.cat);
            }
            fprintf(f, "\r\n");
        }
        fclose(f);
        chars_init(SUBMENU_CHAR);
        list = (char**)chars_tbl.data;
        for(chars_tbl.val = 0; chars_tbl.val < chars_tbl.num && list &&
            strcmp(chars_name, list[chars_tbl.val]); chars_tbl.val++);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.opt", project.id, project_dirs[PROJDIRS_GAME], chars_name);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVECHAR]);
}

/**
 * Load character option list
 */
void chars_load(void *data)
{
    char **list = (char**)chars_tbl.data;

    (void)data;
    if(chars_tbl.val < 0 || !list) return;
    if(!chars_loadopt(list[chars_tbl.val]))
        ui_status(1, lang[ERR_LOADCHAR]);
    else
        chars_baseopt = !strcmp(chars_base, list[chars_tbl.val]);
}

/**
 * Load character option list
 */
int chars_loadopt(char *name)
{
    char *str, *s, t;
    chars_opt_t *opts = NULL;
    int i;

    if(!name || !*name) return 1;
    str = s = project_loadfile(project_dirs[PROJDIRS_GAME], name, "opt", PROJMAGIC_CHAROPT, "chars_load");
    if(str) {
        chars_new(NULL);
        s = project_skipnl(s);
        s = project_getstr2(s, chars_optname, 2, sizeof(chars_optname));
        while(*s) {
            s = project_skipnl(s);
            while(*s == ' ') s++;
            if(!*s || s[1] != ' ') break;
            t = *s; do { s++; } while(*s == ' ');
            switch(t) {
                case 'p':
                    s = project_getsprite(s, &chars_pal);
                break;
                case 'o':
                    s = project_getstr2(s, chars_opt, 2, sizeof(chars_opt));
                    chars_optadd(NULL);
                    opts = (chars_opt_t*)chars_opts.data;
                    chars_attrs.clk = chars_attrs.val = chars_sprs.clk = chars_sprs.val = -1;
                break;
                case 'd':
                    opts = (chars_opt_t*)chars_opts.data;
                    if(opts) s = project_getstr2(s, opts[chars_opts.num - 1].desc, 2, PROJ_DESCMAX);
                    else s = project_skipnl(s);
                break;
                case 'a':
                    chars_rel.val = -1;
                    s = project_getidx(s, &chars_attr.val, project.attrs, 0);
                    s = project_getint(s, &chars_val.val, chars_val.min, chars_val.max);
                    chars_attradd(NULL);
                break;
                case 'r':
                    s = project_getidx(s, &chars_attr.val, project.attrs, 0);
                    s = project_getrel(s, &chars_rel.val);
                    s = project_getint(s, &chars_val.val, chars_val.min, chars_val.max);
                    chars_attradd(NULL);
                break;
                case 'i':
                    s = project_getint(s, &chars_qty.val, chars_qty.min, chars_qty.max);
                    s = project_getobj(s, &chars_obj.val, 0);
                    chars_invadd(NULL);
                break;
                case 'l':
                    chars_lyradd(NULL);
                    if(opts && chars_opts.val >= 0 && chars_opts.val < chars_opts.num && opts[chars_opts.val].numsprs > 0)
                        for(i = opts[chars_opts.val].numsprs - SPRPERLYR; *s && i < opts[chars_opts.val].numsprs; i++) {
                            s = project_getsprite(s, (i % SPRPERLYR) == SPRPERLYR - 1 ? &chars_spr_hidden2 : &chars_spr_hidden);
                            opts[chars_opts.val].sprs[i] = (i % SPRPERLYR) == SPRPERLYR - 1 ? chars_spr_hidden2.val :
                                chars_spr_hidden.val;
                        }
                break;
            }
        }
        chars_attr.val = chars_val.val = 0; chars_qty.val = 1;
        chars_rel.val = chars_spr_hidden.val = chars_spr_hidden2.val = chars_opts.clk = chars_opts.val = chars_attrs.clk =
            chars_attrs.val = chars_invs.clk = chars_invs.val = chars_sprs.clk = chars_sprs.val = chars_obj.val = -1;
        free(str);
        strncpy(chars_name, name, sizeof(chars_name) - 1);
        return 1;
    }
    return 0;
}

/**
 * Load character base configuration
 */
char *chars_loadcfg(uint8_t *deltay)
{
    char *str, *s;

    chars_base[0] = 0; chars_deltay.val = chars_deltay.min;
    str = project_loadfile(project_dirs[PROJDIRS_GAME], "character", "cfg", PROJMAGIC_CHARCONF, "chars_loadcfg");
    if(str) {
        s = project_skipnl(str);
        s = project_getint(s, &chars_deltay.val, chars_deltay.min, chars_deltay.max);
        s = project_getstr2(s, chars_base, 0, sizeof(chars_base));
        free(str);
    }
    if(deltay) *deltay = (uint8_t)chars_deltay.val;
    return chars_base;
}

/**
 * Load strings for translation
 */
void chars_loadstr()
{
    char **list, *str, *s;
    int i, num;

    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_GAME]);
    list = project_getdir(projdir, ".opt", &num);
    for(i = 0; i < num; i++) {
        str = s = project_loadfile(project_dirs[PROJDIRS_GAME], list[i], "opt", PROJMAGIC_CHAROPT, "chars_loadstr");
        if(str) {
            s = project_skipnl(s);
            s = project_gettrstr(s, 2, sizeof(chars_optname));
            while(*s) {
                s = project_skipnl(s);
                while(*s == ' ') s++;
                if(*s == 'o') {
                    do { s++; } while(*s == ' ');
                    s = project_gettrstr(s, 2, sizeof(chars_opt));
                } else
                if(*s == 'd') {
                    do { s++; } while(*s == ' ');
                    s = project_gettrstr(s, 2, PROJ_DESCMAX);
                }
            }
            free(str);
        }
    }
    project_freedir((char***)&list, &num);
}

/**
 * Preview character option
 */
void chars_preview(void *data)
{
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;

    (void)data;
    if(verbose) printf("chars_preview: started\n");
    chars_sprites((opts && chars_opts.val >= 0 && chars_opts.val < chars_opts.num) ? opts[chars_opts.val].sprs : NULL,
        (opts && chars_opts.val >= 0 && chars_opts.val < chars_opts.num) ? opts[chars_opts.val].numsprs : 0,
        chars_spr_hidden.cat, chars_sprs.val % SPRPERLYR);
    if(verbose) printf("chars_preview: stopped\n");
}

/**
 * Preview sprite animations (common to characters, NPCs etc.)
 */
void chars_sprites(int *sprs, int num, int cat, int sel)
{
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *texture = NULL, *txts[8] = { 0 };
    SDL_Rect src, dst, rect[8];
    int wf = SDL_GetWindowFlags(window), i, k, p, bw, dw, dh, mw, mh, mf;
    char tmp[128], *txt;
    uint8_t dy;
    uint32_t *pixels;
    ui_sprite_t sprites[8];

    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

    bw = ((dm.h - 96) / 3) & ~3;
    for(i = 0; i < 8; i++) rect[i].w = rect[i].h = bw;
    rect[0].x = (dm.w - bw) / 2;            rect[0].y = 24 + 2 * (24 + bw);
    rect[1].x = (dm.w - bw) / 2 - 24 - bw;  rect[1].y = 24 + 2 * (24 + bw);
    rect[2].x = (dm.w - bw) / 2 - 24 - bw;  rect[2].y = 24 + 24 + bw;
    rect[3].x = (dm.w - bw) / 2 - 24 - bw;  rect[3].y = 24;
    rect[4].x = (dm.w - bw) / 2;            rect[4].y = 24;
    rect[5].x = (dm.w - bw) / 2 + 24 + bw;  rect[5].y = 24;
    rect[6].x = (dm.w - bw) / 2 + 24 + bw;  rect[6].y = 24 + 24 + bw;
    rect[7].x = (dm.w - bw) / 2 + 24 + bw;  rect[7].y = 24 + 2 * (24 + bw);

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);

    if(chars_baseopt) dy = chars_deltay.val; else chars_loadcfg(&dy);
    memset(sprites, 0, sizeof(sprites));
refresh:
    /* merge sprites, because separate SDL_RenderCopy calls might place pixels at different positions depending on rounding */
    spr_merge(sprites, &mw, &mh, &mf, sprs, 8, 2, num, (sel % SPRPERLYR) == SPRPERLYR - 1 ? 3 : cat, sel, SPRPERLYR, dy);
    ui_clip.x = ui_clip.h = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;
    for(i = 0; i < 8; i++)
        spr_texture(&sprites[i], &txts[i]);

    /* there's a rounding bug in SDL RenderCopy for src.x: 3 % 64 != 67 % 64... this causes jumping
     * pixels in up-scaled animations, workaround by using exact multiples of source size */
    if(mw > bw || mh > bw)
        ui_fit(bw, bw, mw, mh, &dw, &dh);
    else {
        if(mw > 0 && mh > 0) {
            for(k = 1; mw * (k + 1) < bw && mh * (k + 1) < bw; k++);
            dw = mw * k; dh = mh * k;
        } else
            dw = dh = bw;
    }

    if(texture) {
        SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
        memset(pixels, 0, p * dm.h);
        SDL_UnlockTexture(texture);
        ui_icon(texture, dm.w / 2 - 8, dm.h / 2 - 16 - 16,
            sel < 10 ? chars_sprshdr[sel % SPRPERLYR].label - NUMTEXTS : ICON_ACTION);
        sprintf(tmp, "%u x %u", mw, mh);
        ui_text(texture, (dm.w - ui_textwidth(tmp)) / 2, dm.h / 2 - 16, tmp);
        sprintf(tmp, "%u %s", mf, lang[SPRITES_FRAMES]);
        ui_text(texture, (dm.w - ui_textwidth(tmp)) / 2, dm.h / 2, tmp);
        if(sel == SPRPERLYR - 1)
            txt = lang[NPCS_PORTRAIT];
        else
            txt = project_actions[sel % SPRPERLYR];
        ui_text(texture, (dm.w - ui_textwidth(txt)) / 2, dm.h / 2 + 16, txt);
        ssfn_dst.fg = theme[THEME_ERR];
        switch(chars_err) {
            case 2:
                ui_icon(texture, dm.w / 2 - 60, dm.h / 2 + 40, ICON_WARN);
                ui_text(texture, dm.w / 2 - 44, dm.h / 2 + 40, "nframe");
                ui_icon(texture, dm.w / 2 + 20, dm.h / 2 + 40, ICON_WARN);
                ui_text(texture, dm.w / 2 + 36, dm.h / 2 + 40, "type");
            break;
            case 3:
                ui_icon(texture, dm.w / 2 - 24, dm.h / 2 + 40, ICON_WARN);
                ui_text(texture, dm.w / 2 - 8, dm.h / 2 + 40, "type");
            break;
            case 1:
                ui_icon(texture, (dm.w - 16 - 6 * 8) / 2, dm.h / 2 + 40, ICON_WARN);
                ui_text(texture, (dm.w - 16 - 6 * 8) / 2 + 16, dm.h / 2 + 40, "nframe");
            break;
        }
        ssfn_dst.fg = theme[THEME_FG];
        tmp[1] = 0;
        for(i = 0; i < 8; i++) {
            tmp[0] = 17 + i;
            ui_text(texture, rect[i].x + rect[i].w / 2 - 8, rect[i].y + rect[i].h + 2, tmp);
        }
    }

    while(1) {
        /* events */
        SDL_WaitEvent(&event);
        if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
            SDL_PushEvent(&event); goto cleanup;
        }
        switch(event.type) {
            case SDL_KEYUP:
                switch(event.key.keysym.sym) {
                    case SDLK_LEFT:
                        if(sel > 0) sel--; else sel = SPRPERLYR - 1;
                        goto refresh;
                    case SDLK_RIGHT:
                        if(sel < SPRPERLYR - 1) sel++; else sel = 0;
                        goto refresh;
                }
                goto cleanup;
            break;
            case SDL_MOUSEBUTTONUP: goto cleanup; break;
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, theme[THEME_BG] & 0xff, (theme[THEME_BG] >> 8) & 0xff,(theme[THEME_BG] >> 16) & 0xff, 0);
        SDL_RenderClear(renderer);
        for(i = 0; i < 8; i++) {
            p = i;
            if(!txts[p] && i && txts[i - 1]) p = i - 1;
            if(!txts[p] && txts[0]) p = 0;
            if(txts[p] && !(sel == SPRPERLYR - 1 && i)) {
                dst.w = dw; dst.h = dh;
                dst.x = rect[i].x + (rect[i].w - dst.w) / 2; dst.y = rect[i].y + rect[i].h - dst.h;
                spr_render(&sprites[p], -1, txts[p], &dst);
                if(sprites[p].type & 0x80 && (ui_anim_cnt / 3) & 1) {
                    src.x = (ICON_WARN & 7) << 4; src.y = (ICON_WARN & ~7) << 1;
                    src.w = dst.w = src.h = dst.h = 16;
                    SDL_RenderCopy(renderer, icons, &src, &dst);
                }
            } else
            if(spr_none)
                SDL_RenderCopy(renderer, spr_none, NULL, &rect[i]);
        }
        if(texture) SDL_RenderCopy(renderer, texture, NULL, NULL);
        SDL_RenderPresent(renderer);
    }

cleanup:
    for(i = 0; i < 8; i++) {
        if(txts[i]) SDL_DestroyTexture(txts[i]);
        if(sprites[i].data) free(sprites[i].data);
    }
    if(texture) SDL_DestroyTexture(texture);
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Get references
 */
void chars_ref(tngctx_t *ctx)
{
    char **list, *str, *s, t;
    int i, j, num;

    if(!ctx) return;
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_GAME]);
    list = project_getdir(projdir, ".opt", &num);
    for(i = 0; i < num; i++) {
        str = s = project_loadfile(project_dirs[PROJDIRS_GAME], list[i], "opt", PROJMAGIC_CHAROPT, "chars_ref");
        if(str) {
            s = project_skipnl(s);
            while(*s) {
                s = project_skipnl(s);
                while(*s == ' ') s++;
                if(!*s || s[1] != ' ') break;
                t = *s; do { s++; } while(*s == ' ');
                switch(t) {
                    case 'a':
                    case 'r':
                        s = project_getidx(s, &j, project.attrs, 0);
                        attrs_ref(ctx, j);
                    break;
                    case 'i':
                        s = project_skiparg(s, 1);
                        s = project_getobj(s, &j, -1);
                        objects_ref(ctx, j);
                    break;
                }
            }
            free(str);
        }
    }
    project_freedir((char***)&list, &num);
}

/**
 * Save tng
 */
int chars_totng(tngctx_t *ctx)
{
    chars_opt_t *opts;
    uLongf cl;
    uint8_t *buf, *ptr, *comp;
    int i, j, k, l, sprids[SPRPERLYR];

    if(!ctx || !project.chars || !project.numchars) return 1;
    tng_section(ctx, TNG_SECTION_CHARS);
    for(j = 0; j < project.numchars; j++) {
        if(!chars_loadopt(project.chars[j])) {
            ui_switchtab(SUBMENU_CHAR);
            chars_tbl.val = j;
            ui_status(1, lang[ERR_LOADCHAR]);
            return 0;
        }
        opts = (chars_opt_t*)chars_opts.data;
        for(i = 0, l = 7; i < chars_opts.num; i++)
            l += 9 + opts[i].numattrs * 8 + opts[i].numinvs * 4 + opts[i].numsprs * 3;
        buf = ptr = (uint8_t*)main_alloc(l);
        ptr = tng_asset_text(ptr, ctx, chars_optname);
        ptr = tng_asset_sprite(ptr, ctx, chars_pal.cat, chars_pal.val);
        *ptr++ = chars_opts.num;
        for(i = 0; i < chars_opts.num; i++) {
            ptr = tng_asset_text(ptr, ctx, opts[i].opt);
            ptr = tng_asset_text(ptr, ctx, opts[i].desc);
            *ptr++ = opts[i].numattrs;
            *ptr++ = opts[i].numinvs;
            *ptr++ = SPRPERLYR;
            for(k = 0; k < opts[i].numattrs; k++) {
                *ptr++ = opts[i].attrs[k].rel < 0 ? 0xff : opts[i].attrs[k].rel;
                ptr = tng_asset_idx(ptr, ctx, project.attrs, TNG_IDX_ATR, opts[i].attrs[k].attr);
                memcpy(ptr, &opts[i].attrs[k].val, 4); ptr += 4;
            }
            for(k = 0; k < opts[i].numinvs; k++) {
                *ptr++ = opts[i].invs[k].qty;
                ptr = tng_asset_sprite(ptr, ctx, 2, opts[i].invs[k].obj);
            }
            spr_temp(sprids, opts[i].sprs, opts[i].numsprs, chars_spr_hidden.cat, SPRPERLYR);
            for(k = 0; k < SPRPERLYR; k++)
                ptr = tng_asset_sprite(ptr, ctx, k == SPRPERLYR - 1 ? chars_spr_hidden2.cat : chars_spr_hidden.cat, sprids[k]);
        }
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        if(cl) {
            tng_desc(ctx, project.chars[j], cl);
            tng_asset_int(ctx, comp, cl);
        } else
            free(comp);
        free(buf);
        /* free resources */
        chars_new(NULL);
    }
    return 1;
}

/**
 * Read from tng
 */
int chars_fromtng(tngctx_t *ctx)
{
    FILE *f;
    tng_asset_desc_t *asset;
    uint8_t *buf, *ptr, *end;
    int i, j, l, len, na, ni, ns, r;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_CHARS; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    f = project_savefile(0, project_dirs[PROJDIRS_GAME], "character", "cfg", PROJMAGIC_CHARCONF, "chars_fromtng");
    if(f) {
        fprintf(f, "%u %s\r\n", ctx->hdr.deltay, (char*)ctx->sts + asset->name);
        fclose(f);
    }
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;
    for(j = 0; j < len; j++, asset++) {
        ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
        buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &l);
        if(buf) {
            if(!asset->name || asset->name >= TNG_SECTION_SIZE(&ctx->tbl[0]) || l < 1) { free(buf); continue; }
            end = buf + l;
            f = project_savefile(0, project_dirs[PROJDIRS_GAME], (char*)ctx->sts + asset->name, "opt", PROJMAGIC_CHAROPT,
                "chars_fromtng");
            if(f) {
                ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                if(ptr[1] == 0xff && ptr[2] == 0xff)
                    ptr += 3;
                else {
                    fprintf(f, "p "); ptr = tng_wrsprite(ctx, ptr, f); fprintf(f, "\r\n");
                }
                l = *ptr++;
                for(i = 0; i < l && ptr < end; i++) {
                    fprintf(f, "o ");
                    ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                    if(ptr[0] == 0xff && ptr[1] == 0xff && ptr[2] == 0xff)
                        ptr += 3;
                    else {
                        fprintf(f, " d "); ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                    }
                    na = *ptr++;
                    ni = *ptr++;
                    ns = *ptr++;
                    for(j = 0; j < na && ptr < end; j++) {
                        r = *ptr++;
                        if(r == 0xff) {
                            fprintf(f, " a ");
                            ptr = tng_wridx(ctx, ptr, f); fprintf(f, " ");
                        } else {
                            fprintf(f, " r ");
                            ptr = tng_wridx(ctx, ptr, f);
                            fprintf(f, "%s", project_rels[r]);
                        }
                        ptr = tng_wrnum(ctx, ptr, 4, f);  fprintf(f, "\r\n");
                    }
                    for(j = 0; j < ni && ptr < end; j++) {
                        fprintf(f, " i %u ", (int)*ptr++);
                        ptr = tng_wrsprite(ctx, ptr, f); fprintf(f, "\r\n");
                    }
                    for(j = 0; j < ns && ptr < end; j++) {
                        if(!(j % SPRPERLYR)) fprintf(f, "%s l ", j ? "\r\n" : "");
                        else fprintf(f, " ");
                        ptr = tng_wrsprite(ctx, ptr, f);
                    }
                    fprintf(f, "\r\n");
                }
                fclose(f);
            }
            free(buf);
        }
    }
    return 1;
}
