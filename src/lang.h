/*
 * tnge/lang.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Multilanguage support, dictionary keywords
 *
 */
#ifndef LANG_H
#define LANG_H
enum {
    ERR_DISPLAY,        /*** for practical reasons, line numbers must  ***/
    ERR_MEM,            /*** match with the strings in lang_*.h files! ***/
    ERR_EXISTS,
    ERR_PLAYER,
    ERR_LOADCFG,
    ERR_SAVECFG,
    ERR_SAVELIC,
    ERR_SAVELYT,
    ERR_SAVESPR,
    ERR_TEMPL,
    ERR_WRTEMPL,
    ERR_LOADTNG,
    ERR_SAVETNG,
    ERR_UNCOMP,
    ERR_PALETTE,
    ERR_IMAGE,
    ERR_LOADLYT,
    ERR_SPRSIZE,
    ERR_SPRLEN,
    ERR_SPRNAME,
    ERR_SPRNAMENUM,
    ERR_SPRNAMEUNIQ,
    ERR_AUDIO,
    ERR_LOADSND,
    ERR_SAVESND,
    ERR_LOADMOV,
    ERR_SAVEMOV,
    ERR_LOADCHR,
    ERR_SAVECHR,
    ERR_LOADCUT,
    ERR_SAVECUT,
    ERR_LOADUI,
    ERR_SAVEUI,
    ERR_LOADSTRT,
    ERR_SAVESTRT,
    ERR_LOADACT,
    ERR_SAVEACT,
    ERR_LOADATTR,
    ERR_SAVEATTR,
    ERR_EXPR,
    ERR_SCRIPT,
    ERR_NOSTART,
    ERR_LOADCHAR,
    ERR_SAVECHAR,
    ERR_LOADNPC,
    ERR_SAVENPC,
    ERR_LOADSWR,
    ERR_SAVESWR,
    ERR_LOADDLG,
    ERR_SAVEDLG,
    ERR_LOADOBJ,
    ERR_SAVEOBJ,
    ERR_LOADCFT,
    ERR_SAVECFT,
    ERR_LOADTILE,
    ERR_SAVETILE,
    ERR_LOADMAP,
    ERR_SAVEMAP,
    ERR_LOADQCFG,
    ERR_SAVEQCFG,
    ERR_LOADQUEST,
    ERR_SAVEQUEST,
    ERR_LOADPO,
    ERR_SAVEPO,
    ERR_LANG,

    MENU_PLAYSTAT,
    MENU_HELPSTAT,
    MENU_PROJECT,
    MENU_ASSETS,
    MENU_UI,
    MENU_GAME,
    MENU_TRANS,

    SUBMENU_NEWSPRITE,
    SUBMENU_NEWLANG,

    SUBMENU_NEWPROJ,
    SUBMENU_LOADPROJ,
    SUBMENU_IMPORT,
    SUBMENU_IMPZIP,
    SUBMENU_EXPTNG,
    SUBMENU_EXPEXT,
    SUBMENU_EXPTMPL,
    SUBMENU_LICENSES,
    SUBMENU_ABOUT,
    SUBMENU_QUIT,

    SUBMENU_FONTS,
    SUBMENU_SPRITES,
    SUBMENU_GENERATOR,
    SUBMENU_MUSIC,
    SUBMENU_SOUNDS,
    SUBMENU_SPEECH,
    SUBMENU_MOVIES,

    SUBMENU_ELEMENTS,
    SUBMENU_MAINMENU,
    SUBMENU_HUD,
    SUBMENU_ALERTS,
    SUBMENU_CREDITS,
    SUBMENU_CHOOSERS,
    SUBMENU_CUTSCENES,

    SUBMENU_START,
    SUBMENU_ATTRS,
    SUBMENU_ACTIONS,
    SUBMENU_CHAR,
    SUBMENU_NPCS,
    SUBMENU_SPAWNERS,
    SUBMENU_DIALOGS,
    SUBMENU_OBJECTS,
    SUBMENU_CRAFTS,
    SUBMENU_TILES,
    SUBMENU_MAPS,
    SUBMENU_QUESTS,

    ABOUT_TNGE,
    ABOUT_LICENSE1,
    ABOUT_LICENSE2,
    ABOUT_GPL,
    ABOUT_CCBYNCSA,
    ABOUT_COMMERCIAL,
    ABOUT_OGA,

    FILEOP_HIST,
    FILEOP_ALL,
    FILEOP_NAME,
    FILEOP_SIZE,
    FILEOP_TIME,
    FILEOP_RAST,
    FILEOP_RANGE,
    FILEOP_COLORS,
    FILEOP_NUMFPS,
    FILEOP_FACING,
    FILEOP_MANUAL,
    FILEOP_SELFRM,
    FILEOP_IMPORT,
    FILEOP_LOAD,
    FILEOP_WDAY0,
    FILEOP_WDAY1,
    FILEOP_WDAY2,
    FILEOP_WDAY3,
    FILEOP_WDAY4,
    FILEOP_WDAY5,
    FILEOP_WDAY6,
    FILEOP_YESTERDAY,
    FILEOP_NOW,
    FILEOP_MSAGO,
    FILEOP_HSAGO,
    FILEOP_HAGO,

    NEWPROJ_GAMEID,
    NEWPROJ_TITLE,
    NEWPROJ_TEMPLATE,
    NEWPROJ_TILES,
    NEWPROJ_MAPSIZE,
    NEWPROJ_DOWNLOAD,
    NEWPROJ_CREATE,
    NEWPROJ_IDSTAT,
    NEWPROJ_TITSTAT,
    NEWPROJ_TMPSTAT,
    NEWPROJ_MAPSTAT,
    NEWPROJ_TOOBIG,

    LOADPROJ_LOAD,
    LOADPROJ_NONE,
    LOADPROJ_ADD,
    LOADPROJ_DELETE,
    LOADPROJ_LOADTEMPL,
    LOADPROJ_SAVETEMPL,

    SAVEEXT_NAME,
    SAVEEXT_REMOTE,
    SAVEEXT_STREMOTE,
    SAVEEXT_ENCRYPT,
    SAVEEXT_OPTS,
    SAVEEXT_LANGS,
    SAVEEXT_SELALL,
    SAVEEXT_NOLANGS,

    LIC_ID,
    LIC_OWNER,
    LIC_COMMENT,
    LIC_ENDUSER,
    LIC_NONE,
    LIC_TOTAL,
    LIC_GENERATE,
    LIC_COPY,
    LIC_COPIED,

    FONTS_NAME,
    FONTS_PREVIEW,
    FONTS_VECTOR,
    FONTS_NONE,
    FONTS_ADD,
    FONTS_DELETE,
    FONTS_COLOR,
    FONTS_BOLD,
    FONTS_ITALIC,
    FONTS_SIZE,
    FONTS_FAMILY,
    FONTS_IMPORT,

    SPRITES_NONE,
    SPRITES_SAVING,
    SPRITES_ADD,
    SPRITES_GEN,
    SPRITES_LIST,
    SPRITES_DELETE,
    SPRITES_CLEAR,
    SPRITES_MOVE,
    SPRITES_CAT_UI,
    SPRITES_CAT_TILE,
    SPRITES_CAT_OBJ,
    SPRITES_CAT_CHAR,
    SPRITES_CAT_PORT,
    SPRITES_CAT_BACKS,
    SPRITES_FRAMES,
    SPRITES_LOAD,
    SPRITES_PAL,
    SPRITES_CONV,
    SPRITES_IMPPAL,
    SPRITES_DELPAL,
    SPRITES_SWAPRB,
    SPRITES_CIE76,
    SPRITES_HALF,
    SPRITES_DOUBLE,
    SPRITES_VFLIP,
    SPRITES_HFLIP,
    SPRITES_GRID,
    SPRITES_WIDTH,
    SPRITES_HEIGHT,
    SPRITES_LEFT,
    SPRITES_TOP,
    SPRITES_VERT,
    SPRITES_HORIZ,
    SPRITES_LAYOUT,
    SPRITES_SAVELYT,
    SPRITES_GRPNAME,
    SPRITES_ERASE,
    SPRITES_NAME,
    SPRITES_ANIMONCE,
    SPRITES_ANIMFB,
    SPRITES_ANIMLOOP,
    SPRITES_TYPESO,
    SPRITES_TYPESW,
    SPRITES_TYPEWE,
    SPRITES_TYPENW,
    SPRITES_TYPENO,
    SPRITES_TYPENE,
    SPRITES_TYPEEA,
    SPRITES_TYPESE,
    SPRITES_LOADMODEL,
    SPRITES_TOOMUCHRAM,
    SPRITES_CONTINUE,
    SPRITES_RENDER,
    SPRITES_GENERATE,
    SPRITES_START,
    SPRITES_COUNT,
    SPRITES_ADDFRAMES,
    SPRITES_GENOPTS,
    SPRITES_GENSRCH,
    SPRITES_CHOOSEVAR,

    MEDIA_CAT_MUSIC,
    MEDIA_CAT_SOUND,
    MEDIA_CAT_SPEECH,
    MEDIA_CAT_MOVIE,
    MEDIA_TIME,
    MEDIA_NAME,
    MEDIA_NONE,
    MEDIA_ADD,
    MEDIA_DELETE,
    MEDIA_PLAY,
    MEDIA_IMPORT,

    ELEMENTS_CURSORS,
    ELEMENTS_INPUTS,
    ELEMENTS_WINDOW,
    ELEMENTS_DIALOGS,
    ELEMENTS_BUTTONS,
    ELEMENTS_FONTS,
    ELEMENTS_NORMAL,
    ELEMENTS_SELECTED,
    ELEMENTS_PRESSED,
    ELEMENTS_INACTIVE,
    ELEMENTS_CHAT,
    ELEMENTS_WINTITLE,
    ELEMENTS_INATITLE,
    ELEMENTS_DLGTITLE,
    ELEMENTS_TITPAD,
    ELEMENTS_SELPAD,
    ELEMENTS_DLGPAD,
    ELEMENTS_WINTPAD,
    ELEMENTS_WINLPAD,
    ELEMENTS_BTNPPAD,
    ELEMENTS_DELETE,
    ELEMENTS_PREVIEW,
    ELEMENTS_ONSEL,
    ELEMENTS_ONCLK,
    ELEMENTS_ONERR,
    ELEMENTS_SPR_CSR_PTR,
    ELEMENTS_SPR_CSR_CLK,
    ELEMENTS_SPR_CSR_NA,
    ELEMENTS_SPR_CSR_USE,
    ELEMENTS_SPR_CSR_LOADING,
    ELEMENTS_SPR_CHK_0,
    ELEMENTS_SPR_CHK_1,
    ELEMENTS_SPR_OPT_NP,
    ELEMENTS_SPR_OPT_PP,
    ELEMENTS_SPR_OPT_BG,
    ELEMENTS_SPR_OPT_NN,
    ELEMENTS_SPR_OPT_PN,
    ELEMENTS_SPR_SLDR_L,
    ELEMENTS_SPR_SLDR_BTN,
    ELEMENTS_SPR_SLDR_B,
    ELEMENTS_SPR_SLDR_R,
    ELEMENTS_SPR_SCR_L,
    ELEMENTS_SPR_SCR_HB,
    ELEMENTS_SPR_SCR_R,
    ELEMENTS_SPR_SCR_U,
    ELEMENTS_SPR_SCR_VB,
    ELEMENTS_SPR_SCR_D,
    ELEMENTS_SPR_SCR_B,
    ELEMENTS_SPR_TAB_NL,
    ELEMENTS_SPR_TAB_NB,
    ELEMENTS_SPR_TAB_NR,
    ELEMENTS_SPR_TAB_SL,
    ELEMENTS_SPR_TAB_SB,
    ELEMENTS_SPR_TAB_SR,
    ELEMENTS_SPR_WIN_TL,
    ELEMENTS_SPR_WIN_T,
    ELEMENTS_SPR_WIN_TR,
    ELEMENTS_SPR_WIN_L,
    ELEMENTS_SPR_WIN_BG,
    ELEMENTS_SPR_WIN_R,
    ELEMENTS_SPR_WIN_BL,
    ELEMENTS_SPR_WIN_B,
    ELEMENTS_SPR_WIN_BR,
    ELEMENTS_SPR_DLG_T_L,
    ELEMENTS_SPR_DLG_T_B,
    ELEMENTS_SPR_DLG_T_R,
    ELEMENTS_SPR_DLG_O_L,
    ELEMENTS_SPR_DLG_O_B,
    ELEMENTS_SPR_DLG_O_R,
    ELEMENTS_SPR_DLG_TL,
    ELEMENTS_SPR_DLG_T,
    ELEMENTS_SPR_DLG_TR,
    ELEMENTS_SPR_DLG_L,
    ELEMENTS_SPR_DLG_BG,
    ELEMENTS_SPR_DLG_R,
    ELEMENTS_SPR_DLG_BL,
    ELEMENTS_SPR_DLG_B,
    ELEMENTS_SPR_DLG_BR,
    ELEMENTS_SPR_BTN_NL,
    ELEMENTS_SPR_BTN_NB,
    ELEMENTS_SPR_BTN_NR,
    ELEMENTS_SPR_BTN_SL,
    ELEMENTS_SPR_BTN_SB,
    ELEMENTS_SPR_BTN_SR,
    ELEMENTS_SPR_BTN_PL,
    ELEMENTS_SPR_BTN_PB,
    ELEMENTS_SPR_BTN_PR,
    ELEMENTS_SPR_BTN_IL,
    ELEMENTS_SPR_BTN_IB,
    ELEMENTS_SPR_BTN_IR,
    ELEMENTS_SPR_BTN_CL,
    ELEMENTS_SPR_BTN_CB,
    ELEMENTS_SPR_BTN_CR,
    ELEMENTS_SPR_APPICO,
    ELEMENTS_WINCHK,
    ELEMENTS_OPTION,
    ELEMENTS_SLIDER,
    ELEMENTS_DLGNAME,
    ELEMENTS_DLGTXT,
    ELEMENTS_DLGOPT1,
    ELEMENTS_DLGOPT2,
    ELEMENTS_DLGOPT3,

    MAINMENU_INTRO,
    MAINMENU_SINTRO,
    MAINMENU_ENABLED,
    MAINMENU_BACK,
    MAINMENU_BACKMUS,
    MAINMENU_BACKMOV,
    MAINMENU_BACKIMG,
    MAINMENU_NOCROP,
    MAINMENU_WEBSITE,
    MAINMENU_URLSTAT,
    MAINMENU_DX,
    MAINMENU_DY,
    MAINMENU_INTERVAL,
    MAINMENU_PARALLAX,
    MAINMENU_TITLE,
    MAINMENU_HEADER,
    MAINMENU_TITCOLOR,
    MAINMENU_TITSHADOW,
    MAINMENU_BUTTONS,
    MAINMENU_BTNPAD,
    MAINMENU_CHARNAME,
    MAINMENU_VIEWMENU,
    MAINMENU_VIEWCHRG,
    MAINMENU_GRAPH,
    MAINMENU_GRAPHB,
    MAINMENU_GRAPHC,
    MAINMENU_DELETE,
    MAINMENU_MERLIN,
    MAINMENU_OPT1,
    MAINMENU_OPT2,
    MAINMENU_OPT3,
    MAINMENU_ATTR1,
    MAINMENU_ATTR2,
    MAINMENU_ATTR3,
    MAINMENU_ATTR4,
    MAINMENU_ATTR5,

    HUD_CONTROLS,
    HUD_ITEMBAR,
    HUD_STATUSBAR,
    HUD_COMBINED,
    HUD_PROGBARS,
    HUD_DIRECTION,
    HUD_VALUE,
    HUD_MAXIMUM,
    HUD_IMAGE,
    HUD_NONE,
    HUD_TOOWIDE,
    HUD_BACK,
    HUD_BACKOPT,
    HUD_FRONT,
    HUD_FRONTOPT,
    HUD_NUM,
    HUD_WIDTH,
    HUD_HEIGHT,
    HUD_POS,
    HUD_VGAP,
    HUD_HAND,
    HUD_NAV,
    HUD_BTNS,
    HUD_ERASE,
    HUD_PPOS,
    HUD_EFFBAR,
    HUD_PPOSHINT,
    HUD_ADD,
    HUD_DIR_0,
    HUD_DIR_1,
    HUD_DIR_2,
    HUD_DIR_3,
    HUD_INVENTORY,
    HUD_INVICON,
    HUD_INVPRESSED,
    HUD_INVWIDTH,
    HUD_INVHEIGHT,
    HUD_INVPOS,
    HUD_INVNUM,
    HUD_INVITEM,
    HUD_INVSEL,
    HUD_INVEQUIP,
    HUD_INVNONE,
    HUD_INVSLOT,
    HUD_INVERASE,
    HUD_INVEPOS,
    HUD_INVNAME,
    HUD_INVADD,
    HUD_INVNOTUNIQUE,
    HUD_DELETE,
    HUD_PREVIEW,

    ALERT_FONT,
    ALERT_DUR,
    ALERT_NEW,
    ALERT_COMPLETED,
    ALERT_FAILED,
    ALERT_DELETE,
    ALERT_PREVIEW,

    CREDITS_BACK,
    CREDITS_BACKMUS,
    CREDITS_BACKIMG,
    CREDITS_HEADER,
    CREDITS_FONT,
    CREDITS_CATEGORY,
    CREDITS_AUTHORS,
    CREDITS_NONE,
    CREDITS_ERASE,
    CREDITS_ANAME,
    CREDITS_ADD,
    CREDITS_DELETE,
    CREDITS_PREVIEW,

    CHOOSERS_NONE,
    CHOOSERS_DELETE,
    CHOOSERS_PREVIEW,
    CHOOSERS_NOITEM,
    CHOOSERS_MARH,
    CHOOSERS_MARV,
    CHOOSERS_PADH,
    CHOOSERS_PADV,
    CHOOSERS_POS,
    CHOOSERS_IMAGE,
    CHOOSERS_NORMAL,
    CHOOSERS_SELECTED,
    CHOOSERS_PRESSED,
    CHOOSERS_STRPOS,
    CHOOSERS_ENDPOS,
    CHOOSERS_HELPER,
    CHOOSERS_REQUIRE,
    CHOOSERS_PROVIDE,
    CHOOSERS_AND,
    CHOOSERS_KEEP,
    CHOOSERS_ERASE,
    CHOOSERS_ADD,
    CHOOSERS_RETURN,

    CUTSCN_NONE,
    CUTSCN_DELETE,
    CUTSCN_PREVIEW,
    CUTSCN_NAME,
    CUTSCN_CH_MOV,
    CUTSCN_CH_IMG,
    CUTSCN_CH_SPH,
    CUTSCN_CH_TXT,
    CUTSCN_CH_SCR,
    CUTSCN_BGMOV,
    CUTSCN_BGMUS,
    CUTSCN_NOBG,
    CUTSCN_NOSEL,
    CUTSCN_SLIDE,
    CUTSCN_COLOR,
    CUTSCN_POS,
    CUTSCN_ALN_LEFT,
    CUTSCN_ALN_CENTRE,
    CUTSCN_ALN_RIGHT,
    CUTSCN_PLAY,
    CUTSCN_SCRIPT,
    CUTSCN_ONCE,
    CUTSCN_LOOP,
    CUTSCN_DELSLD,
    CUTSCN_DELSPC,
    CUTSCN_DELSUB,
    CUTSCN_REMSUB,
    CUTSCN_ADDSUB,
    CUTSCN_NOSUB,
    CUTSCN_FADE,

    STARTUP_DELETE,
    STARTUP_TIMERS,

    ATTRS_NONE,
    ATTRS_DELETE,
    ATTRS_TITLE,
    ATTRS_TYPE_PRI,
    ATTRS_TYPE_VAR,
    ATTRS_TYPE_CALC,
    ATTRS_TYPE_GLOBALVAR,
    ATTRS_TYPE_GLOBALCALC,
    ATTRS_VALCHAR,
    ATTRS_SETUP,
    ATTRS_TYPE,
    ATTRS_NAME,
    ATTRS_STITLE,
    ATTRS_CALC,
    ATTRS_DEF,
    ATTRS_FREETOTAL,
    ATTRS_FREEATTR,
    ATTRS_SPEED,
    ATTRS_WEIGHT,
    ATTRS_LIGHT,
    ATTRS_LEVELUP,

    ACTIONS_USERDEF,
    ACTIONS_RANGED,

    CHARS_NONE,
    CHARS_DELETE,
    CHARS_PREVIEW,
    CHARS_LIST,
    CHARS_BASE,
    CHARS_BASELINE,
    CHARS_PALETTE,
    CHARS_OPTIONS,
    CHARS_NOOPT,
    CHARS_ATTRS,
    CHARS_NOATTR,
    CHARS_NAME,
    CHARS_DELATTR,
    CHARS_RELATTR,
    CHARS_ADDATTR,
    CHARS_INVENTORY,
    CHARS_NOINV,
    CHARS_DELINV,
    CHARS_QTY,
    CHARS_OBJECT,
    CHARS_ADDINV,
    CHARS_DELOPT,
    CHARS_OPT,
    CHARS_ADDOPT,
    CHARS_NOLYR,
    CHARS_DELLYR,
    CHARS_CPYLYR,
    CHARS_CUTLYR,
    CHARS_PSTLYR,
    CHARS_UPLYR,
    CHARS_DWNLYR,
    CHARS_ADDLYR,
    CHARS_ANIM_IDLE,
    CHARS_ANIM_WALK,
    CHARS_ANIM_CLIMB,
    CHARS_ANIM_JUMP,
    CHARS_ANIM_RUN,
    CHARS_ANIM_SWIM,
    CHARS_ANIM_FLY,
    CHARS_ANIM_BLOCK,
    CHARS_ANIM_HURT,
    CHARS_ANIM_DIE,
    CHARS_ANIM_A1,
    CHARS_ANIM_A2,
    CHARS_ANIM_A3,
    CHARS_ANIM_A4,
    CHARS_ANIM_A5,
    CHARS_ANIM_A6,
    CHARS_ANIM_A7,
    CHARS_ANIM_A8,
    CHARS_ANIM_A9,

    NPCS_NONE,
    NPCS_DELETE,
    NPCS_PREVIEW,
    NPCS_MOVE,
    NPCS_MOVE_STAND,
    NPCS_MOVE_RANDOM,
    NPCS_MOVE_PATROL,
    NPCS_SPEED,
    NPCS_CHANGE,
    NPCS_PATH,
    NPCS_SSPEED,
    NPCS_SCHANGE,
    NPCS_SDIST,
    NPCS_ONEVENT,
    NPCS_TITLE,
    NPCS_TITLESTR,
    NPCS_PORTRAIT,
    NPCS_CHANCE,
    NPCS_QTY,
    NPCS_ITEMS,
    NPCS_FREQ,
    NPCS_ONETIME,
    NPCS_SELL,
    NPCS_BUY,
    NPCS_CONFIG,
    NPCS_INV,
    NPCS_NAME,
    NPCS_DELINV,
    NPCS_PERINV,
    NPCS_QTYINV,
    NPCS_FRQINV,
    NPCS_ADDINV,
    NPCS_CALCPRICE,

    SPAWNERS_NONE,
    SPAWNERS_DELETE,
    SPAWNERS_NAME,
    SPAWNERS_NUMNPC,
    SPAWNERS_FREQ,
    SPAWNERS_NONPC,
    SPAWNERS_ERASE,
    SPAWNERS_REQUIRE,
    SPAWNERS_PROVIDE,
    SPAWNERS_NPCKIND,
    SPAWNERS_ANIM,
    SPAWNERS_ADD,
    SPAWNERS_SREQUIRE,
    SPAWNERS_SPROVIDE,

    DIALOGS_NONE,
    DIALOGS_DELETE,
    DIALOGS_PREVIEW,
    DIALOGS_TITLE,
    DIALOGS_LEFT,
    DIALOGS_DESC,
    DIALOGS_OPTS,
    DIALOGS_NOOPTS,
    DIALOGS_NAME,
    DIALOGS_SPCH,
    DIALOGS_PORT,
    DIALOGS_ERASE,
    DIALOGS_UP,
    DIALOGS_DOWN,
    DIALOGS_ADD,
    DIALOGS_REQUIRE,
    DIALOGS_PROVIDE,

    OBJECTS_META,
    OBJECTS_NONE,
    OBJECTS_BYPIC,
    OBJECTS_DELETE,
    OBJECTS_PVIEWEQP,
    OBJECTS_PVIEWINV,
    OBJECTS_DUPLICATE,
    OBJECTS_ONMAP,
    OBJECTS_ININV,
    OBJECTS_ACTIONGRP,
    OBJECTS_EQUIPBY,
    OBJECTS_TITLE,
    OBJECTS_EQUIPSLOTS,
    OBJECTS_SKILL,
    OBJECTS_REQATTRS,
    OBJECTS_MODATTRS,
    OBJECTS_PROJSPR,
    OBJECTS_PROJTIME,
    OBJECTS_PROJUNIT,
    OBJECTS_LAYERORD,
    OBJECTS_FORE,
    OBJECTS_BACK,
    OBJECTS_INVSPRITE,
    OBJECTS_SOUND,
    OBJECTS_PRICE,
    OBJECTS_PRICECAT,
    OBJECTS_MODEQ,
    OBJECTS_MODPER,
    OBJECTS_DELRQ,
    OBJECTS_ADDRQ,
    OBJECTS_ADDATR,
    OBJECTS_DELATR,
    OBJECTS_TRANSPORT,

    CRAFTS_UNORDERED,
    CRAFTS_OPTS,
    CRAFTS_NOOPTS,
    CRAFTS_NAME,
    CRAFTS_TITLE,
    CRAFTS_SND,
    CRAFTS_ERASE,
    CRAFTS_ADD,
    CRAFTS_PROD,

    TILES_META,
    TILES_NONE,
    TILES_DELETE,
    TILES_BYPIC,
    TILES_AUTOMAP,
    TILES_COMBINE,
    TILES_SPEED,
    TILES_SOUND,
    TILES_ONWALK,
    TILES_ONSWIM,
    TILES_ONFLY,
    TILES_NOAUTO,
    TILES_DELAUTO,
    TILES_ADDAUTO,

    MAPS_NONE,
    MAPS_DELETE,
    MAPS_PREVIEW,
    MAPS_SEARCH,
    MAPS_TITLE,
    MAPS_DAYLIGHT,
    MAPS_LYR_GRD1,
    MAPS_LYR_GRD2,
    MAPS_LYR_GRD3,
    MAPS_LYR_GRD4,
    MAPS_LYR_OBJECTS,
    MAPS_LYR_NPCS,
    MAPS_LYR_SPWN,
    MAPS_LYR_ROOF1,
    MAPS_LYR_ROOF2,
    MAPS_LYR_ROOF3,
    MAPS_LYR_ROOF4,
    MAPS_LYR_COLL,
    MAPS_LYR_PATHS,
    MAPS_COPY,
    MAPS_CUT,
    MAPS_PASTE,
    MAPS_MOVE,
    MAPS_INFO,
    MAPS_STLAYERS,
    MAPS_ITEM,
    MAPS_ERASE,
    MAPS_PIPETTE,
    MAPS_PAINT,
    MAPS_FILL,
    MAPS_SELBOX,
    MAPS_SELPLY,
    MAPS_SELFZY,
    MAPS_SELADD,
    MAPS_SELSUB,
    MAPS_LINE,
    MAPS_HFLIP,
    MAPS_VFLIP,
    MAPS_FLIPDIR,
    MAPS_NAME,
    MAPS_STTITLE,
    MAPS_DAYLEN,
    MAPS_DAYSPR,
    MAPS_NOPAR,
    MAPS_PARERASE,
    MAPS_PARFG,
    MAPS_PARBG,
    MAPS_PARSX,
    MAPS_PARSY,
    MAPS_PARDX,
    MAPS_PARDY,
    MAPS_PARTM,
    MAPS_PARSPR,
    MAPS_PARADD,
    MAPS_LINKSO,
    MAPS_LINKSW,
    MAPS_LINKWE,
    MAPS_LINKNW,
    MAPS_LINKNO,
    MAPS_LINKNE,
    MAPS_LINKEA,
    MAPS_LINKSE,

    QUESTS_NONE,
    QUESTS_DELETE,
    QUESTS_PREVIEW,
    QUESTS_TITLE,
    QUESTS_DESC,
    QUESTS_UNIQUE,
    QUESTS_ONCOMPLETE,
    QUESTS_NAME,

    TRANSLATE_NOSPC,
    TRANSLATE_ORIG,
    TRANSLATE_TRANS,
    TRANSLATE_CODE,
    TRANSLATE_NAME,
    TRANSLATE_DELETE,

    CMD_NOP,
    CMD_CMNT,
    CMD_ITER,
    CMD_COND,
    CMD_LET,
    CMD_END,
    CMD_DELAY,
    CMD_MUS,
    CMD_SND,
    CMD_SPC,
    CMD_CHR,
    CMD_CUT,
    CMD_DLG,
    CMD_MAP,
    CMD_QUEST,
    CMD_CANIM,
    CMD_OANIM,
    CMD_NPCADD,
    CMD_NPCSEL,
    CMD_NPCBEH,
    CMD_NPCINV,
    CMD_NPCS,
    CMD_NPCSW,
    CMD_NPCW,
    CMD_NPCNW,
    CMD_NPCN,
    CMD_NPCNE,
    CMD_NPCE,
    CMD_NPCSE,
    CMD_REMOVE,
    CMD_DROP,
    CMD_DELETE,
    CMD_ADD,
    CMD_REPLACE,
    CMD_GIVE,
    CMD_TAKE,
    CMD_EVENT,
    CMD_TRANSPORT,
    CMD_SCENE,
    CMD_WAITNPC,
    CMD_CRAFT,
    CMD_MATCH,
    CMD_ALERT,
    CMD_ALTITUDE,
    CMD_BEH_ATTACK,
    CMD_BEH_NEUTRAL,
    CMD_BEH_FLEE,
    CMD_TRN_WALK,
    CMD_TRN_SWIM,
    CMD_TRN_FLY,
    CMD_EVT_ONSTART,
    CMD_EVT_ONTIMER110,
    CMD_EVT_ONTIMER1,
    CMD_EVT_ONTIMER10,
    CMD_EVT_ONCHANGE,
    CMD_EVT_ONCLICK,
    CMD_EVT_ONTOUCH,
    CMD_EVT_ONLEAVE,
    CMD_EVT_ONAPPROACH,
    CMD_EVT_ONACTION1,
    CMD_EVT_ONACTION2,
    CMD_EVT_ONACTION3,
    CMD_EVT_ONACTION4,
    CMD_EVT_ONACTION5,
    CMD_EVT_ONACTION6,
    CMD_EVT_ONACTION7,
    CMD_EVT_ONACTION8,
    CMD_EVT_ONACTION9,
    CMD_EVT_ONOBJ1,
    CMD_EVT_ONOBJ2,
    CMD_EVT_ONOBJ3,
    CMD_EVT_ONTIMER,
    CMD_STAT_ITER,
    CMD_STAT_SWPLUS,
    CMD_STAT_SWMINUS,
    CMD_STAT_SWEXPR,
    CMD_STAT_MEDIA,
    CMD_STAT_CHOOSER,
    CMD_STAT_CUTSCN,
    CMD_STAT_ANIM,
    CMD_STAT_NPC,
    CMD_STAT_DLG,
    CMD_STAT_OBJ,
    CMD_STAT_CFT,
    CMD_STAT_MAP,
    CMD_STAT_DIR,
    CMD_STAT_QST,
    CMD_STAT_QST2,
    CMD_STAT_BEH,
    CMD_STAT_TRN,
    CMD_STAT_CLK,
    CMD_STAT_ACN,
    CMD_STAT_TOUT,
    CMD_STAT_PLAY,
    CMD_STAT_DLY,
    CMD_STAT_MOVE,
    CMD_STAT_AMOUNT,

    LANG_NAME,
    LANG_VALUE,
    LANG_LOADING,
    LANG_EXTRACTING,
    LANG_COLLECT,
    LANG_COMPRESS,
    LANG_WRITING,
    LANG_CLEARFORM,
    LANG_SURE,
    LANG_ATTRSTR,
    LANG_NONE,
    LANG_DEF,
    LANG_SET,
    LANG_YES,
    LANG_NO,
    LANG_OK,
    LANG_SAVE,
    LANG_SAVED,
    LANG_ZOOMIN,
    LANG_ZOOMOUT,
    LANG_ZOOMNORM,
    LANG_COLLMASK,
    LANG_EVTMASK,
    LANG_VISIB,
    LANG_TIMEFMT,

    GAME_LOADGAME,
    GAME_NEWGAME,
    GAME_CONFIG,
    GAME_EXIT,
    GAME_CHARGEN,
    GAME_USERNAME,
    GAME_PASSWORD,
    GAME_REMAIN,
    GAME_CREATE,
    GAME_BACK,
    GAME_EXISTS,
    GAME_OKAY,
    GAME_SAVEDGAMES,
    GAME_LOAD,
    GAME_LOGTOSERVER,
    GAME_LOGIN,
    GAME_BADUSER,
    GAME_CHAT,
    GAME_SAVE,
    GAME_MENU,
    GAME_INV,
    GAME_SKILLS,
    GAME_QUESTS,
    GAME_SELL,
    GAME_BUY,
    GAME_NOMODIFY,
    GAME_REQUIRES,
    GAME_PROVIDES,
    GAME_AMMO,
    GAME_CRD_GAME,
    GAME_CRD_ART,
    GAME_CRD_DESIGN,
    GAME_CRD_ANIMS,
    GAME_CRD_FONTS,
    GAME_CRD_MUSIC,
    GAME_CRD_SOUND,
    GAME_CRD_VOICE,
    GAME_CRD_TR,
    GAME_CRD_PROG,

    /* must be the last */
    NUMTEXTS
};

#define NUMLANGS         9

extern char **lang;

#endif
