TinyGL
======

Originally by Fabrice Bellard, but this is a version modified by Gek, C-Chads from [this repo](https://github.com/C-Chads/tinygl).
I've made a lot of modifications though:

1. cleaned up the from compiler warnings with -Wall and -pedantic
2. removed the font and text renderer because it generated dozens of overflow warnings and we don't need it anyway
3. removed function implementations from header files
4. fixed the broken memory allocator selection
5. converted to ANSI C
6. fixed everything so that it compiles with -ansi -Wextra
7. added GL_RGBA textures
8. added glOrtho
9. removed that terrible cmake stuff and simplified the Makefile
10. added alpha channel to rendering, glClear should clear to 0 alpha (so only rendered pixels will have the alpha channel set)
11. added maxy to zbuffer, which is set to the bottom most rendered pixel's y

Still not perfect, lot of things left to clean up.

bzt
