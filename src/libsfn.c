/*
 * libsfn/sfn.c
 *
 * Copyright (C) 2020 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief File functions: import, modify and save
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "zlib.h"
#include "stb_image.h"
#include "libsfn.h"

/* png load replaced with tnge's image_load */
unsigned char *image_load(char *fn, int *w, int *h);
FILE *project_fopen(const char *fn, const char *mode);

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_OUTLINE_H
#include FT_IMAGE_H
#include FT_BBOX_H
#include FT_SFNT_NAMES_H

#define FT_SIZE 1024

FT_Library ft = NULL;
FT_Face face;
FT_BBox *bbox;

typedef struct {
    int unicode;
    FT_BBox bbox;
} ftchr_t;
ftchr_t *ftchars;

void hex(char *ptr, int size);          /* GNU unifont */
void bdf(char *ptr, int size);          /* X11 Bitmap Distribution Format or FontForge's SplineFontDB with bitmap font*/
void pcf(unsigned char *ptr, int size); /* X11 Portable Compiled Font */
void psf(unsigned char *ptr, int size); /* PC Screen Font (Linux Console) */
void pff(unsigned char *ptr, int size); /* GRUB's Font */
void fnt(unsigned char *ptr, int size); /* Windows Font */
void png(unsigned char *ptr, int size); /* PNG Pixmap */
void tga(unsigned char *ptr, int size); /* TGA Pixmap */
void sfd(char *data, int size);                 /* FontForge's SplineFontDB with SplineSets */
int  ft2_read(unsigned char *data, int size);   /* Read in and check if FreeType2 supports this font */
void ft2_parse();                               /* Parse a font with FreeType2 */

int rs = 0, re = 0x10FFFF, replace = 0, skipundef = 0, skipcode = 0, hinting = 0, adv = 0, relul = 0;
int rasterize = 0, origwh = 0, lastuni = -1, *fidx, dorounderr = 0;
sfnctx_t ctx;
sfnprogressbar_t pbar = NULL;
extern int verbose;

/**
 * Convert hex string to binary number. Use this lightning fast implementation
 * instead of the unbeliveably crap, slower than a pregnant snail sscanf...
 */
unsigned int gethex(char *ptr, int len)
{
    unsigned int ret = 0;
    for(;len--;ptr++) {
        if(*ptr>='0' && *ptr<='9') {          ret <<= 4; ret += (unsigned int)(*ptr-'0'); }
        else if(*ptr >= 'a' && *ptr <= 'f') { ret <<= 4; ret += (unsigned int)(*ptr-'a'+10); }
        else if(*ptr >= 'A' && *ptr <= 'F') { ret <<= 4; ret += (unsigned int)(*ptr-'A'+10); }
        else break;
    }
    return ret;
}

/**
 * Turn a decimal or hex string into binary number
 */
unsigned int getnum(char *s)
{
    if(!s || !*s) return 0;
    if(*s=='\'') { s++; return ssfn_utf8(&s); }
    if((*s=='0' && s[1]=='x') || (*s=='U' && s[1]=='+')) return gethex(s+2,8);
    return atoi(s);
}

/**
 * Encode run-length bytes
 */
unsigned char *rle_enc(unsigned char *inbuff, int inlen, int *outlen)
{
    int i, k, l, o;
    unsigned char *outbuff;

    if(!inbuff || inlen < 1 || !outlen) return NULL;

    /* allocate memory for the worst case scenario */
    outbuff = (unsigned char *)realloc(NULL, 2 * inlen + 1);
    if(!outbuff) return NULL;

    k = o = 0; outbuff[o++] = 0;
    for(i = 0; i < inlen; i++) {
        for(l = 1; l < 128 && i + l < inlen && inbuff[i] == inbuff[i + l]; l++);
        if(l > 1) {
            l--; if(outbuff[k]) { outbuff[k]--; outbuff[o++] = 0x80 | l; } else outbuff[k] = 0x80 | l;
            outbuff[o++] = inbuff[i]; k = o; outbuff[o++] = 0; i += l; continue;
        }
        outbuff[k]++; outbuff[o++] = inbuff[i];
        if(outbuff[k] > 127) { outbuff[k]--; k = o; outbuff[o++] = 0; }
    }
    if(!(outbuff[k] & 0x80)) { if(outbuff[k]) outbuff[k]--; else o--; }
    *outlen = o;
    outbuff = (unsigned char *)realloc(outbuff, o);
    return outbuff;
}

/**
 * Decode run-length encoded bytes
 */
unsigned char *rle_dec(unsigned char *inbuff, int inlen, int *outlen)
{
    int l, o = 0, s = 0;
    unsigned char *end = inbuff + inlen, *outbuff = NULL;

    if(!inbuff || inlen < 2 || !outlen) return NULL;

    if(*outlen) {
        s = *outlen;
        outbuff = (unsigned char*)realloc(outbuff, s);
        if(!outbuff) return NULL;
    }
    while(inbuff < end) {
        l = ((*inbuff++) & 0x7F) + 1;
        /* if we don't know the required buffer size in advance, allocate memory in 4k blocks */
        if(o + l + 1 > s) {
            s += 4096;
            outbuff = (unsigned char*)realloc(outbuff, s);
            if(!outbuff) return NULL;
        }
        if(inbuff[-1] & 0x80) {
            while(l--) outbuff[o++] = *inbuff;
            inbuff++;
        } else while(l--) outbuff[o++] = *inbuff++;
    }
    *outlen = o;
    outbuff = (unsigned char *)realloc(outbuff, o);
    return outbuff;
}

/**
 * Check if a row is background color only
 */
int isempty(int len, unsigned char *data)
{
    int i;
    if(!data || len < 1) return 1;
    for(i = 0; i < len; i++)
        if(data[i] != 0xFF) return 0;
    return 1;
}

/**
 * Sort layers by type and scanline
 */
int lyrsrt(const void *a, const void *b)
{
    /* pixmap, bitmap first, then contours bigger area first */
    return ((sfnlayer_t*)a)->type != ((sfnlayer_t*)b)->type ?
        ((sfnlayer_t*)b)->type - ((sfnlayer_t*)a)->type :
        (((sfnlayer_t*)a)->miny != ((sfnlayer_t*)b)->miny ?
        ((sfnlayer_t*)a)->miny - ((sfnlayer_t*)b)->miny :
        ((sfnlayer_t*)a)->minx - ((sfnlayer_t*)b)->minx);
}

/**
 * Sort kerning pairs by next character's code point
 */
int krnsrt(const void *a, const void *b)
{
    return ((sfnkern_t*)a)->n - ((sfnkern_t*)b)->n;
}

/**
 * Sort kerning positions by list size
 */
int possrt(const void *a, const void *b)
{
    return ((sfnkpos_t*)b)->len - ((sfnkpos_t*)a)->len;
}

/**
 * Sort fragments by number of reference and type
 */
int frgsrt(const void *a, const void *b)
{
    return ((sfnfrag_t*)a)->cnt != ((sfnfrag_t*)b)->cnt ?
        ((sfnfrag_t*)b)->cnt - ((sfnfrag_t*)a)->cnt :
        (((sfnfrag_t*)a)->type != ((sfnfrag_t*)b)->type ?
        ((sfnfrag_t*)a)->type - ((sfnfrag_t*)b)->type :
        ((sfnfrag_t*)a)->w - ((sfnfrag_t*)b)->w);
}

/**
 * Sort fragment descriptors by type and scanline
 */
int frdsrt(const void *a, const void *b)
{
    return ctx.frags[fidx[((int*)a)[0]]].type != ctx.frags[fidx[((int*)b)[0]]].type ?
        ctx.frags[fidx[((int*)b)[0]]].type - ctx.frags[fidx[((int*)a)[0]]].type :
        (((int*)a)[2] != ((int*)b)[2] ? ((int*)a)[2] - ((int*)b)[2] : ((int*)a)[1] - ((int*)b)[1]);
}

/**
 * Compare two normalized contour fragments, allowing rounding errors in coordinates
 */
int frgcmp(sfncont_t *a, sfncont_t *b, int l)
{
    int i;
    if(l<1) return 0;
    for(; l; l--, a++, b++) {
        if(a->type != b->type) return a->type - b->type;
        i = a->px - b->px; if((i < 0 ? -i : i) > 1) return i;
        i = a->c1x - b->c1x; if((i < 0 ? -i : i) > 1) return i;
        i = a->c2x - b->c2x; if((i < 0 ? -i : i) > 1) return i;
    }
    return 0;
}

/**
 * Parse compressed SSFN font format (binary)
 *
 * @param ptr pointer to buffer
 * @param size size of the buffer
 */
void sfn(unsigned char *ptr, int size)
{
    ssfn_font_t *font = (ssfn_font_t*)ptr;
    sfnlayer_t *currlayer;
    unsigned char *ptr2, color, *frg, *cmd, *bitmap = NULL;
    int f, i, j, k, l, m, n, u, x, y, unicode = 0;

    /* sanity checks */
    if((unsigned int)size != font->size || memcmp((unsigned char*)font + font->size - 4, SSFN_ENDMAGIC, 4))
        { if(verbose > 1) { fprintf(stderr, "libsfn: missing end magic or incorrect font size\n"); } return; }
    if(!font->fragments_offs) { if(verbose > 1) { fprintf(stderr, "libsfn: missing fragments table\n"); } return; }
    if(!font->characters_offs) { if(verbose > 1) { fprintf(stderr, "libsfn: missing characters table\n"); } return; }
    if(font->characters_offs <= font->fragments_offs) {
        if(verbose > 1) { fprintf(stderr, "libsfn: incorrect characters table offset\n"); } return; }
    if(font->kerning_offs && (font->kerning_offs <= font->characters_offs || (font->ligature_offs &&
        font->kerning_offs <= font->ligature_offs)))
        { if(verbose > 1) { fprintf(stderr, "libsfn: incorrect kerning table offset\n"); } return; }
    if(font->ligature_offs && font->ligature_offs <= font->characters_offs)
        { if(verbose > 1) { fprintf(stderr, "libsfn: incorrect ligature table offset\n"); } return; }
    if(font->cmap_offs && ((font->size - font->cmap_offs) & 3))
        { if(verbose > 1) { fprintf(stderr, "libsfn: incorrect cmap table offset\n"); } return; }

    /* header */
    ctx.family = SSFN_TYPE_FAMILY(font->type);
    ctx.style = SSFN_TYPE_STYLE(font->type);
    ctx.width = font->width;
    ctx.height = font->height;
    ctx.baseline = font->baseline;
    ctx.underline = font->underline;

    /* string table */
    ptr = (unsigned char *)font + sizeof(ssfn_font_t);
    for(i = -6; ptr < (unsigned char *)font + font->fragments_offs; i++) {
        switch(i) {
            case -6: sfn_setstr(&ctx.name, (char*)ptr, 0); break;
            case -5: sfn_setstr(&ctx.familyname, (char*)ptr, 0); break;
            case -4: sfn_setstr(&ctx.subname, (char*)ptr, 0); break;
            case -3: sfn_setstr(&ctx.revision, (char*)ptr, 0); break;
            case -2: sfn_setstr(&ctx.manufacturer, (char*)ptr, 0); break;
            case -1: sfn_setstr(&ctx.license, (char*)ptr, 0); break;
            default: sfn_setstr(&ctx.ligatures[i], (char*)ptr, 0); break;
        }
        ptr += strlen((char*)ptr) + 1;
    }

    /* character mappings */
    ptr = (unsigned char *)font + font->characters_offs;
    ptr2 = (unsigned char *)font + (font->ligature_offs ? font->ligature_offs : (font->kerning_offs ?
        font->kerning_offs : (font->cmap_offs ? font->cmap_offs : font->size - 4)));
    for(unicode = 0; ptr < ptr2;) {
        if(ptr[0] == 0xFF) { unicode += 65536; ptr++; }
        else if((ptr[0] & 0xC0) == 0xC0) { k = (((ptr[0] & 0x3F) << 8) | ptr[1]) + 1; unicode += k; ptr += 2; }
        else if((ptr[0] & 0xC0) == 0x80) { k = (ptr[0] & 0x3F) + 1; unicode += k; ptr++; }
        else {
            if(pbar) (*pbar)(0, 0, unicode, 0x10FFFF, PBAR_RDFILE);
            n = ptr[1]; k = ptr[0];
            u = unicode >= rs && unicode <= re ? sfn_charadd(unicode, ptr[2], ptr[3], ptr[4], ptr[5], ptr[0] & 0x3F) : 0;
            ptr += 6; color = 0xFE;
            for(f = 0; f < n; f++) {
                if(ptr[0] == 255 && ptr[1] == 255) {
                    color = ptr[2]; ptr += k & 0x40 ? 6 : 5;
                } else {
                    x = ptr[0]; y = ptr[1];
                    if(k & 0x40) { m = (ptr[5] << 24) | (ptr[4] << 16) | (ptr[3] << 8) | ptr[2]; ptr += 6; }
                    else { m = (ptr[4] << 16) | (ptr[3] << 8) | ptr[2]; ptr += 5; }
                    if(u) {
                        if(m < font->fragments_offs || (unsigned int)m >= font->characters_offs) {
                            if(verbose > 1) fprintf(stderr,"libsfn: incorrect fragment offset %x\n",m);
                            return;
                        }
                        frg = (unsigned char*)font + m;
                        if(!(frg[0] & 0x80)) {
                            /* contour */
                            currlayer = sfn_layeradd(unicode, SSFN_FRAG_CONTOUR, 0, 0, 0, 0, color, NULL);
                            if(!currlayer) return;
                            j = (frg[0] & 0x3F);
                            if(frg[0] & 0x40) { j <<= 8; j |= frg[1]; frg++; }
                            j++; frg++;
                            cmd = frg; frg += (j+3)/4;
                            for(i = 0; i < j; i++) {
                                switch((cmd[i / 4] >> ((i & 3) * 2)) & 3) {
                                    case SSFN_CONTOUR_MOVE:
                                        sfn_contadd(currlayer, SSFN_CONTOUR_MOVE, frg[0]+x, frg[1]+y, 0,0, 0,0);
                                        frg += 2;
                                    break;
                                    case SSFN_CONTOUR_LINE:
                                        sfn_contadd(currlayer, SSFN_CONTOUR_LINE, frg[0]+x, frg[1]+y, 0,0, 0,0);
                                        frg += 2;
                                    break;
                                    case SSFN_CONTOUR_QUAD:
                                        sfn_contadd(currlayer, SSFN_CONTOUR_QUAD, frg[0]+x, frg[1]+y, frg[2]+x, frg[3]+y, 0,0);
                                        frg += 4;
                                    break;
                                    case SSFN_CONTOUR_CUBIC:
                                        sfn_contadd(currlayer, SSFN_CONTOUR_CUBIC, frg[0]+x, frg[1]+y, frg[2]+x, frg[3]+y,
                                            frg[4]+x, frg[5]+y);
                                        frg += 6;
                                    break;
                                }
                            }
                        } else if((frg[0] & 0x60) == 0x00) {
                            /* bitmap */
                            m = ((frg[0] & 0x1F) + 1) << 3;
                            bitmap = (unsigned char*)malloc(m * (frg[1] + 1));
                            if(!bitmap) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
                            memset(bitmap, 0xFF, m * (frg[1] + 1));
                            cmd = frg + 2;
                            for(i=l=0;i<(int)((frg[0] & 0x3F) + 1) * (frg[1] + 1);i++) {
                                for(j=1;j<=0x80;j<<=1) bitmap[l++] = (cmd[i] & j) ? 0xFE : 0xFF;
                            }
                            sfn_layeradd(unicode, SSFN_FRAG_BITMAP, x, y, m, frg[1] + 1, color, bitmap);
                            free(bitmap);
                        } else if((frg[0] & 0x60) == 0x20) {
                            /* pixel map */
                            j = (frg[2] + 1) * (frg[3] + 1);
                            bitmap = rle_dec(frg + 4, (((frg[0] & 0x1F) << 8) | frg[1]) + 1, &j);
                            if(!bitmap) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
                            sfn_layeradd(unicode, SSFN_FRAG_PIXMAP, x, y, frg[2] + 1, frg[3] + 1, 0xFE, bitmap);
                            free(bitmap);
                        } else if((frg[0] & 0x60) == 0x40) {
                            /* kerning relation */
                            j = (((frg[0] & 0x3) << 8) | frg[1]) + 1;
                            for(i = 0, frg += 2; i < j; i++, frg += 8) {
                                y = ((frg[2] & 0xF) << 16) | (frg[1] << 8) | frg[0];
                                m = ((frg[5] & 0xF) << 16) | (frg[4] << 8) | frg[3];
                                cmd = (unsigned char*)font + font->kerning_offs + ((((frg[2] >> 4) & 0xF) << 24) |
                                    (((frg[5] >> 4) & 0xF) << 16) | (frg[7] << 8) | frg[6]);
                                while(y <= m) {
                                    l = ((*cmd++) & 0x7F) + 1;
                                    if(cmd[-1] & 0x80) {
                                        if(cmd[0])
                                            while(l-- && y <= m)
                                                sfn_kernadd(unicode, y++, x ? (char)cmd[0] : 0, x ? 0 : (char)cmd[0]);
                                        else
                                            y += l;
                                        cmd++;
                                    } else while(l-- && y <= m) {
                                        if(cmd[0])
                                            sfn_kernadd(unicode, y, x ? (char)cmd[0] : 0, x ? 0 : (char)cmd[0]);
                                        y++;
                                        cmd++;
                                    }
                                }
                            }
                        } else {
                            /* hinting grid */
                            j = frg[0] & 31; frg++;
                            memset(ctx.glyphs[unicode].hintv, 0, 33);
                            memset(ctx.glyphs[unicode].hinth, 0, 33);
                            if(x > 0) {
                                ctx.glyphs[unicode].hintv[0] = j + 1;
                                ctx.glyphs[unicode].hintv[1] = x - 1;
                                for(l = 2; j && l < 33; l++, frg++, j--)
                                    ctx.glyphs[unicode].hintv[l] = ctx.glyphs[unicode].hintv[l - 1] + frg[0];
                            } else
                            if(y > 0) {
                                ctx.glyphs[unicode].hinth[0] = j + 1;
                                ctx.glyphs[unicode].hinth[1] = y - 1;
                                for(l = 2; j && l < 33; l++, frg++, j--)
                                    ctx.glyphs[unicode].hinth[l] = ctx.glyphs[unicode].hinth[l - 1] + frg[0];
                            }
                        }
                        color = 0xFE;
                    }
                }
            }
            unicode++;
        }
    }

    /* color map */
    if(font->cmap_offs) {
        memcpy(ctx.cpal, (uint8_t*)font + font->cmap_offs, font->size - font->cmap_offs - 4);
        ctx.numcpal = (font->size - font->cmap_offs - 4) / 4;
    }
}

/**
 * Parse SSFN ASCII font format (text)
 *
 * @param ptr pointer to zero terminated UTF-8 string
 * @param size size of the buffer
 */
void asc(char *ptr, int size)
{
    int x, y, w = 0, h = 0, o, i, par[6], unicode = -1, nc = 0, numchars, len = 0, line = 1;
    char *end = ptr + size-4, *e;
    unsigned char *bitmap = NULL, color = 0xFE;
    sfnlayer_t *currlayer = NULL;

    for(numchars = 0,e=ptr++;e < end && *e;e++)
        if(e[0]=='\n' && e[1]=='=' && e[2]=='=' && e[3]=='=') numchars++;

    while(ptr < end && *ptr) {
        /* end marker */
        if(ptr[-1] == '\n' && ptr[0] == '#' && ptr[1] == ' ' && ptr[2] == 'E') break;
        /* properties */
        if(ptr[-1] == '\n' && ptr[0] == '$' && ptr[1] >= 'a' && ptr[1] <= 'z') {
            for(e = ptr; e < end && *e && *e != ' ' && *e != '\r' && *e != '\n'; e++);
            while(*e == ' ') e++;
            switch(ptr[1]) {
                case 't': sfn_setfamilytype(atoi(e)); break;
                case 's':
                    if(ptr[2]=='u' && *e == '\"') { e++; sfn_setstr(&ctx.subname, e, 0); } else
                    if(ptr[2]=='t') {
                        for(; *e && *e != '\r' && *e != '\n'; e++)
                            if(e[-1] == ' ') {
                                if(*e == 'b') ctx.style |= SSFN_STYLE_BOLD;
                                if(*e == 'i') ctx.style |= SSFN_STYLE_ITALIC;
                                if(*e == '1') ctx.style |= SSFN_STYLE_USRDEF1;
                                if(*e == '2') ctx.style |= SSFN_STYLE_USRDEF2;
                            }
                    }
                break;
                case 'b': ctx.baseline = atoi(e); break;
                case 'u': ctx.underline = atoi(e); break;
                case 'n': if(*e == '\"') { e++; sfn_setstr(&ctx.name, e, 0); } break;
                case 'f': if(*e == '\"') { e++; sfn_setstr(&ctx.familyname, e, 0); } break;
                case 'r': if(*e == '\"') { e++; sfn_setstr(&ctx.revision, e, 0); } break;
                case 'm': if(*e == '\"') { e++; sfn_setstr(&ctx.manufacturer, e, 0); } break;
                case 'l': if(*e == '\"') { e++; sfn_setstr(&ctx.license, e, 0); } break;
                default: if(verbose > 1) { fprintf(stderr,"libsfn: line %d: unknown property\n",line); } break;
            }
        }
        if(unicode != -1) {
            /* foreground color command */
            if(*ptr == 'f') {
                color = *ptr=='-' ? 0xFE : sfn_cpaladd(
                    gethex((char*)ptr+2, 2), gethex((char*)ptr+4, 2),
                    gethex((char*)ptr+6, 2), gethex((char*)ptr, 2));
            } else
            /* bitmap layer */
            if(*ptr == '.' || *ptr == 'X' || *ptr == 'x') {
                bitmap = realloc(bitmap, len);
                if(!bitmap) { if(verbose > 1) { fprintf(stderr,"libsfn: line %d: memory allocation error\n",line); } return; }
                for(i = 0; i < len && ptr < end && *ptr && (*ptr == '.' || *ptr == 'X' || *ptr == 'x' ||
                    *ptr == '\r' || *ptr == '\n'); ptr++) {
                    switch(*ptr) {
                        case '\n': line++; break;
                        case 'x':
                        case 'X': bitmap[i++] = 0xFE; break;
                        case '.': bitmap[i++] = 0XFF; break;
                    }
                }
                ptr--;
                sfn_layeradd(unicode, SSFN_FRAG_BITMAP, 0, 0, w, h, color, bitmap);
                currlayer = NULL;
            } else
            /* pixmap layer */
            if((*ptr == '-' || (*ptr >= '0' && *ptr <= '9') || (*ptr >= 'A' && *ptr <= 'F') || (*ptr >= 'a' && *ptr <= 'f')) &&
                ptr[1] != ' ') {
                bitmap = realloc(bitmap, len);
                if(!bitmap) { if(verbose > 1) { fprintf(stderr,"libsfn: line %d: memory allocation error\n",line); } return; }
                for(i = 0; i < len && ptr < end && *ptr && (*ptr == ' ' || *ptr == '-' || (*ptr >= '0' && *ptr <= '9') ||
                    (*ptr >= 'A' && *ptr <= 'F') || (*ptr >= 'a' && *ptr <= 'f') || *ptr == '\r' || *ptr == '\n'); ptr++) {
                        if(*ptr == '\n') line++; else
                        if(*ptr == '-') { bitmap[i++] = 0xFF; while(*ptr == '-') ptr++; } else
                        if((*ptr >= '0' && *ptr <= '9') || (*ptr >= 'A' && *ptr <= 'F') || (*ptr >= 'a' && *ptr <= 'f')) {
                            bitmap[i++] = sfn_cpaladd(gethex(ptr+2, 2), gethex(ptr+4, 2), gethex(ptr+6,2), gethex(ptr,2));
                            ptr += 8;
                        }
                }
                ptr--;
                sfn_layeradd(unicode, SSFN_FRAG_PIXMAP, 0, 0, w, h, color, bitmap);
                currlayer = NULL;
            } else
            /* horizontal hinting grid */
            if(*ptr == 'H') {
                for(i = 0, ptr += 2; i < 32 && ptr < end && *ptr && *ptr != '\r' && *ptr != '\n';) {
                    while(*ptr && (*ptr < '0' || *ptr > '9')) ptr++;
                    ctx.glyphs[unicode].hinth[1+i++] = atoi(ptr);
                    while(*ptr >= '0' && *ptr <= '9') ptr++;
                }
                ctx.glyphs[unicode].hinth[0] = i;
                currlayer = NULL;
            } else
            /* vertical hinting grid */
            if(*ptr == 'V') {
                for(i = 0, ptr += 2; i < 32 && ptr < end && *ptr && *ptr != '\r' && *ptr != '\n';) {
                    while(*ptr && (*ptr < '0' || *ptr > '9')) ptr++;
                    ctx.glyphs[unicode].hintv[1+i++] = atoi(ptr);
                    while(*ptr >= '0' && *ptr <= '9') ptr++;
                }
                ctx.glyphs[unicode].hintv[0] = i;
                currlayer = NULL;
            } else
            /* kerning info */
            if(*ptr == 'k') {
                for(ptr+=2, i=0; ptr < end && *ptr && ptr[1] != '\r' && ptr[1] != '\n'; i++) {
                    par[1] = par[2] = 0;
                    if((ptr[0] == 'U' || ptr[0] == 'u') && ptr[1] == '+') {
                        ptr += 2;
                        par[0] = gethex(ptr, 6);
                        while((*ptr >= '0' && *ptr <= '9') || (*ptr >= 'A' && *ptr <= 'F') || (*ptr >= 'a' && *ptr <= 'f'))
                            ptr++;
                    } else
                        par[0] = ssfn_utf8(&ptr);
                    while(*ptr == ' ') ptr++;
                    par[1] = atoi(ptr);
                    while(*ptr == '-' || (*ptr >= '0' && *ptr <= '9')) ptr++;
                    if(*ptr == ' ') {
                        while(*ptr == ' ') ptr++;
                        par[2] = atoi(ptr);
                        while(*ptr == '-' || (*ptr >= '0' && *ptr <= '9')) ptr++;
                    }
                    if(*ptr == ',') ptr++;
                    while(*ptr == ' ') ptr++;
                    if(i >= ctx.glyphs[unicode].numkern || ctx.glyphs[unicode].kern) {
                        ctx.glyphs[unicode].numkern += 512;
                        ctx.glyphs[unicode].kern = (sfnkern_t*)realloc(ctx.glyphs[unicode].kern,
                            ctx.glyphs[unicode].numkern * sizeof(sfnkern_t));
                        if(!ctx.glyphs[unicode].kern) {
                            if(verbose > 1) { fprintf(stderr,"libsfn: line %d: memory allocation error\n",line); }
                            return;
                        }
                    }
                    ctx.glyphs[unicode].kern[i].n = par[0];
                    ctx.glyphs[unicode].kern[i].x = par[1];
                    ctx.glyphs[unicode].kern[i].y = par[2];
                }
                if(!i && ctx.glyphs[unicode].kern) {
                    free(ctx.glyphs[unicode].kern);
                    ctx.glyphs[unicode].kern = NULL;
                }
                ctx.glyphs[unicode].numkern = i;
                currlayer = NULL;
            } else
            /* contour */
            if(*ptr == 'm' || *ptr == 'l' || *ptr == 'q' || *ptr == 'c') {
                e = ptr; par[0]=par[1]=par[2]=par[3]=par[4]=par[5]=0;
                for(ptr+=2, i=0; ptr < end && *ptr && *ptr != '\r' && *ptr != '\n' && i < 6; ptr++) {
                    par[i++] = atoi(ptr);
                    while(ptr < end && *ptr!=' ' && *ptr!=',' && ptr[1] && ptr[1] != '\r' && ptr[1] != '\n') ptr++;
                }
                ptr--;
                if(*e == 'm') {
                    if(i<2) {if(verbose > 1){fprintf(stderr,"libsfn: line %d: too few move arguments in U+%06X\n",line,unicode);}return;}
                    currlayer = sfn_layeradd(unicode, SSFN_FRAG_CONTOUR, 0, 0, 0, 0, color, NULL);
                    sfn_contadd(currlayer, SSFN_CONTOUR_MOVE, par[0], par[1], 0,0, 0,0);
                } else if(currlayer) {
                    switch(*e) {
                        case 'l':
                            if(i<2) { if(verbose > 1) fprintf(stderr,"libsfn: line %d: too few line arguments in U+%06X\n",line,unicode); }
                            else sfn_contadd(currlayer, SSFN_CONTOUR_LINE, par[0], par[1], 0,0, 0,0);
                        break;

                        case 'q':
                            if(i<4) { if(verbose > 1) fprintf(stderr,"libsfn: line %d: too few quadratic curve arguments in U+%06X\n",line,unicode); }
                            else sfn_contadd(currlayer, SSFN_CONTOUR_QUAD, par[0], par[1], par[2], par[3], 0,0);
                        break;

                        case 'c':
                            if(i<6) { if(verbose > 1) fprintf(stderr,"libsfn: line %d: too few bezier curve arguments in U+%06X\n",line,unicode); }
                            else sfn_contadd(currlayer, SSFN_CONTOUR_CUBIC, par[0], par[1], par[2], par[3], par[4], par[5]);
                        break;
                    }
                } else {
                    if(verbose > 1) fprintf(stderr,"libsfn: line %d: contour path does not start with a 'move to' command in U+%06X\n",line,
                        unicode);
                    break;
                }
            }
        }
        /* characters */
        if(ptr[-1] == '\n' && ptr[0] == '=' && ptr[1] == '=' && ptr[2] == '=') {
            if(pbar) (*pbar)(0, 0, ++nc, numchars, PBAR_RDFILE);
            ptr +=  5; unicode = gethex(ptr, 6);
            ptr += 10; w = atoi(ptr); while(*ptr && *ptr != '\r' && *ptr != '\n' && *ptr != '=') ptr++;
            ptr +=  2; h = atoi(ptr); while(*ptr && *ptr != '\r' && *ptr != '\n' && *ptr != '=') ptr++;
            ptr +=  2; x = atoi(ptr); while(*ptr && *ptr != '\r' && *ptr != '\n' && *ptr != '=') ptr++;
            ptr +=  2; y = atoi(ptr); while(*ptr && *ptr != '\r' && *ptr != '\n' && *ptr != '=') ptr++;
            ptr +=  2; o = atoi(ptr); while(*ptr && *ptr != '\r' && *ptr != '\n' && *ptr != '\"') ptr++;
            if(unicode >= SSFN_LIG_FIRST && unicode <= SSFN_LIG_LAST && *ptr == '\"')
                sfn_setstr(&ctx.ligatures[unicode-SSFN_LIG_FIRST], ptr + 1, 0);
            if(w > 255) { w = 255; } if(w < 0) w = 0;
            if(h > 255) { h = 255; } if(h < 0) h = 0;
            if(x > 254) { x = 254; } if(x < 0) x = 0;
            if(y > 254) { y = 254; } if(y < 0) y = 0;
            if(o > 63) { o = 63; } if(o < 0) o = 0;
            len = (w + 1) * (h + 1);
            if(unicode < rs || unicode > re || unicode < 0 || unicode > 0x10FFFF || !sfn_charadd(unicode, w, h, x, y, o))
                unicode = -1;
            color = 0xFE;
        }
        /* go to next line */
        while(ptr < end && *ptr && *ptr != '\r' && *ptr != '\n') ptr++;
        while(*ptr == '\r' || *ptr == '\n') {
            if(*ptr == '\n') line++;
            ptr++;
        }
    }
    if(bitmap) free(bitmap);
}

/**
 * Initialize SSFN context
 *
 * @param pb progressbar callback
 */
void sfn_init(sfnprogressbar_t pb)
{
    memset(&ctx, 0, sizeof(ctx));
    pbar = pb;
}

/**
 * Free SSFN context
 */
void sfn_free()
{
    int i;

    if(ctx.name) free(ctx.name);
    if(ctx.familyname) free(ctx.familyname);
    if(ctx.subname) free(ctx.subname);
    if(ctx.revision) free(ctx.revision);
    if(ctx.manufacturer) free(ctx.manufacturer);
    if(ctx.license) free(ctx.license);
    for(i = 0; i < 0x110000; i++)
        sfn_chardel(i);
    memset(&ctx, 0, sizeof(ctx));
}

/**
 * Remove a character by deleting all of its layers
 *
 * @param unicode character to delete
 */
void sfn_chardel(int unicode)
{
    int i;

    if(unicode < 0 || unicode>0x10FFFF) return;
    if(ctx.glyphs[unicode].layers) {
        for(i = 0; i < ctx.glyphs[unicode].numlayer; i++)
            if(ctx.glyphs[unicode].layers[i].data)
                free(ctx.glyphs[unicode].layers[i].data);
        free(ctx.glyphs[unicode].layers);
    }
    if(ctx.glyphs[unicode].kern)
        free(ctx.glyphs[unicode].kern);
    memset(&ctx.glyphs[unicode], 0, sizeof(sfnglyph_t));
}

/**
 * Add a character to font
 *
 * @param unicode character to add
 * @param w width
 * @param h height
 * @param ax advance x
 * @param ay advance y
 * @param ox overlap x
 * @return true on success
 */
int sfn_charadd(int unicode, int w, int h, int ax, int ay, int ox)
{
    int i;

    if(unicode < rs || unicode > re || unicode < 0 || unicode > 0x10FFFF || (ctx.glyphs[unicode].layers && !replace)) return 0;
    for(i = 0; i < ctx.numskip; i++)
        if(ctx.skip[i] == unicode) return 0;
    if(ctx.glyphs[unicode].layers && replace) sfn_chardel(unicode);
    else memset(&ctx.glyphs[unicode], 0, sizeof(sfnglyph_t));
    if(ax) ay = 0;
    ctx.glyphs[unicode].width = w;
    ctx.glyphs[unicode].height = h;
    ctx.glyphs[unicode].adv_x = ax + (ax ? adv : 0);
    ctx.glyphs[unicode].adv_y = ay + (!ax ? adv: 0);
    ctx.glyphs[unicode].ovl_x = ox;
    return 1;
}

/**
 * Add a layer to character
 *
 * @param unicode character to add layer to
 * @param t type of the layer (SSFN_FRAG_x)
 * @param x offset x
 * @param y offset y
 * @param w width
 * @param h height
 * @param c color index, 254 foreground
 * @param data pointer to data buffer
 * @return pointer to a layer struct or NULL on error
 */
sfnlayer_t *sfn_layeradd(int unicode, int t, int x, int y, int w, int h, int c, unsigned char *data)
{
    sfnlayer_t *lyr = NULL;
    unsigned char *data2;
    int i, j, l;

    if(unicode < rs || unicode > re || unicode < 0 || unicode > 0x10FFFF) return NULL;
    if(t != SSFN_FRAG_CONTOUR && !iswhitespace(unicode) && (!data || isempty(w * h, data))) return NULL;
    if(ctx.glyphs[unicode].numlayer >= 255) {
        if(verbose > 1) fprintf(stderr, "libsfn: too many layers in U+%06x character's glyph.\n", unicode);
        return NULL;
    }
    if(t != SSFN_FRAG_CONTOUR)
        for(i = 0; i < ctx.glyphs[unicode].numlayer; i++)
            if(ctx.glyphs[unicode].layers[i].type == t && (t != SSFN_FRAG_BITMAP || ctx.glyphs[unicode].layers[i].color == c))
                { lyr = &ctx.glyphs[unicode].layers[i]; break; }
    if(x < 0) {
        if(verbose > 1) fprintf(stderr, "libsfn: negative x position in U+%06x character's glyph.\n", unicode);
        x = 0;
    }
    if(x + w > ctx.glyphs[unicode].width) {
        if(t != SSFN_FRAG_CONTOUR && lyr && lyr->data) {
            l = (x + w) * ctx.glyphs[unicode].height;
            data2 = (unsigned char*)malloc(l + 1);
            if(!data2) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return NULL; }
            memset(data2, 0xFF, l + 1);
            for(j = 0; j < ctx.glyphs[unicode].height; j++)
                for(i = 0; i < ctx.glyphs[unicode].width; i++)
                    data2[j * (x + w) + i] = lyr->data[ctx.glyphs[unicode].width * j + i];
            free(lyr->data);
            lyr->data = data2;
        }
        ctx.glyphs[unicode].width = x + w;
    }
    if(y < 0) {
        if(verbose > 1) fprintf(stderr, "libsfn: negative y position in U+%06x character's glyph.\n", unicode);
        y = 0;
    }
    if(y + h > ctx.glyphs[unicode].height) {
        if(t != SSFN_FRAG_CONTOUR && lyr && lyr->data) {
            l = ctx.glyphs[unicode].width * (y + h);
            lyr->data = (unsigned char*)realloc(lyr->data, l + 1);
            if(!lyr->data) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return NULL; }
            memset(lyr->data + ctx.glyphs[unicode].width * ctx.glyphs[unicode].height, 0,
                ctx.glyphs[unicode].width * (y + h - ctx.glyphs[unicode].height));
        }
        ctx.glyphs[unicode].height = y + h;
    }
    if(!lyr) {
        ctx.glyphs[unicode].layers = (sfnlayer_t*)realloc(ctx.glyphs[unicode].layers,
            (ctx.glyphs[unicode].numlayer + 1) * sizeof(sfnlayer_t));
        if(!ctx.glyphs[unicode].layers) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return NULL; }
        lyr = &ctx.glyphs[unicode].layers[ctx.glyphs[unicode].numlayer++];
        memset(lyr, 0, sizeof(sfnlayer_t));
        lyr->type = t;
        lyr->color = c;
        if(t != SSFN_FRAG_CONTOUR) {
            l = ctx.glyphs[unicode].width * ctx.glyphs[unicode].height;
            lyr->data = (unsigned char*)malloc(l + 1);
            if(!lyr->data) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return NULL; }
            memset(lyr->data, 0xFF, l + 1);
        }
    }
    if(t != SSFN_FRAG_CONTOUR && data) {
        for(j = 0; j < h; j++)
            for(i = 0; i < w; i++)
                lyr->data[(y + j) * ctx.glyphs[unicode].width + (x + i)] = data[j * w + i];
        if(!ctx.glyphs[unicode].adv_x && !ctx.glyphs[unicode].adv_y) {
            if(ctx.family == SSFN_FAMILY_MONOSPACE)
                ctx.glyphs[unicode].adv_x = ctx.glyphs[unicode].width + 1 + adv;
            else {
                for(y = l = 0; y < ctx.glyphs[unicode].height; y++)
                    for(j = ctx.glyphs[unicode].width; (unsigned int)j > (unsigned int)l; j--)
                        if(lyr->data[y * ctx.glyphs[unicode].width + j]) l = j;
                ctx.glyphs[unicode].adv_x = (iswhitespace(unicode) ? ctx.glyphs[unicode].width : l) + 1 + adv;
            }
        }
    }
    ctx.lx = ctx.ly = 0;
    return lyr;
}

/**
 * Delete a layer from character
 *
 * @param unicode character to remove layer from
 * @param idx layer
 */
void sfn_layerdel(int unicode, int idx)
{
    if(unicode < 0 || unicode > 0x10FFFF || ctx.glyphs[unicode].numlayer < 1 || idx >= ctx.glyphs[unicode].numlayer) return;
    if(ctx.glyphs[unicode].layers[idx].data) free(ctx.glyphs[unicode].layers[idx].data);
    ctx.glyphs[unicode].layers[idx].data = NULL;
    ctx.glyphs[unicode].numlayer--;
    memcpy(&ctx.glyphs[unicode].layers[idx], &ctx.glyphs[unicode].layers[idx+1],
        (ctx.glyphs[unicode].numlayer - idx) * sizeof(sfnlayer_t));
}

/**
 * Add a contour command to a layer
 *
 * @param lyr pointer to layer
 * @param t type (SSFN_CONT_x)
 * @param px next point x
 * @param py next point y
 * @param c1x control point #1 x
 * @param c1y control point #1 y
 * @param c2x control point #2 x
 * @param c2y control point #2 y
 * @return true on success
 */
int sfn_contadd(sfnlayer_t *lyr, int t, int px, int py, int c1x, int c1y, int c2x, int c2y)
{
    sfncont_t *cont;
    int cx, cy;

    if(!lyr || lyr->type != SSFN_FRAG_CONTOUR) return 0;
    if(lyr->len >= 32767) {
        if(verbose > 1) fprintf(stderr, "libsfn: too many points in contour in U+%06x character's glyph.\n", unicode);
        return 0;
    }
    lyr->data = (unsigned char*)realloc(lyr->data, (lyr->len + 1) * sizeof(sfncont_t));
    if(!lyr->data) { lyr->len = 0; return 0; }
    /* clamp coordinates to prevent overflow */
    if(px<0 || px>254 || py<0 || py>254 || c1x<0 || c1x>254 || c1y<0 || c1y>254 || c2x<0 || c2x>254 || c2y<0 || c2y>254) {
        /* should never happen */
        if(verbose && lastuni != unicode)
            fprintf(stderr,"\rlibsfn: scaling error U+%06x px %d py %d c1x %d c1y %d c2x %d c2y %d\n", unicode, px, py, c1x, c1y,
                c2x, c2y);
        lastuni = unicode;
        if(px<0) { px = 0; } if(px>254) px = 254;
        if(py<0) { py = 0; } if(py>254) py = 254;
        if(c1x<0) { c1x = 0; } if(c1x>254) c1x = 254;
        if(c1y<0) { c1y = 0; } if(c1y>254) c1y = 254;
        if(c2x<0) { c2x = 0; } if(c2x>254) c2x = 254;
        if(c2y<0) { c2y = 0; } if(c2y>254) c2y = 254;
    }
    /* convert trivial cubic curves to quadratic ones, requires less storage space in fonts */
    if(t == SSFN_CONTOUR_CUBIC) {
        if((c1x >> 1) == (c2x >> 1) && (c1y >> 1) == (c2y >> 1)) { t = SSFN_CONTOUR_QUAD; c2x = c2y = 0; } else
        if(ctx.lx > 0 && ctx.ly > 0) {
            cx = ((c1x - ctx.lx) / 2) + ctx.lx;
            cy = ((c1y - ctx.ly) / 2) + ctx.ly;
            if(((((c2x - cx) / 2) + cx) >> 1) == (px >> 1) && ((((c2y - cy) / 2) + cy) >> 1) == (py >> 1)) {
                t = SSFN_CONTOUR_QUAD; c1x = cx; c1y = cy; c2x = c2y = 0;
            }
        }
    }
    cont = &((sfncont_t *)(lyr->data))[lyr->len++];
    cont->type = t & 0xFF;
    cont->px = px; if(px + 1 > ctx.glyphs[unicode].width) ctx.glyphs[unicode].width = px + 1;
    cont->py = py; if(py + 1 > ctx.glyphs[unicode].height) ctx.glyphs[unicode].height = py + 1;
    cont->c1x = c1x; if(c1x + 1 > ctx.glyphs[unicode].width) ctx.glyphs[unicode].width = c1x + 1;
    cont->c1y = c1y; if(c1y + 1 > ctx.glyphs[unicode].height) ctx.glyphs[unicode].height = c1y + 1;
    cont->c2x = c2x; if(c2x + 1 > ctx.glyphs[unicode].width) ctx.glyphs[unicode].width = c2x + 1;
    cont->c2y = c2y; if(c2y + 1 > ctx.glyphs[unicode].height) ctx.glyphs[unicode].height = c2y + 1;
    ctx.lx = px; ctx.ly = py;
    return 1;
}

/**
 * Add a kerning relation
 *
 * @param unicode previous code point in relation
 * @param next next unicode in relation
 * @param x kerning offset x
 * @param y kerning offset y
 * @return true on success
 */
int sfn_kernadd(int unicode, int next, int x, int y)
{
    int i;

    if(unicode < rs || unicode > re || unicode < 33 || unicode > 0x10FFFF || next < rs || next > re ||
        next < 33 || next > 0x10FFFFF) return 0;
    if(x < -128) x = -128;
    if(x > 127) x = 127;
    if(y < -128) y = -128;
    if(y > 127) y = 127;
    for(i = 0; i < ctx.glyphs[unicode].numkern; i++)
        if(ctx.glyphs[unicode].kern[i].n == next) {
            if(!x && !y) {
                memcpy(&ctx.glyphs[unicode].kern[i], &ctx.glyphs[unicode].kern[i+1], (ctx.glyphs[unicode].numkern - i) *
                    sizeof(sfnkern_t));
                ctx.glyphs[unicode].numkern--;
            } else {
                ctx.glyphs[unicode].kern[i].x = x;
                ctx.glyphs[unicode].kern[i].y = y;
            }
            return 1;
        }
    if(!x && !y) return 0;
    if(ctx.glyphs[unicode].numkern >= 32767) {
        if(verbose > 1) fprintf(stderr,"libsfn: too many kerning pairs for U+%06x, truncated to 32767\n", unicode);
        return 1;
    }
    i = ctx.glyphs[unicode].numkern++;
    ctx.glyphs[unicode].kern = (sfnkern_t*)realloc(ctx.glyphs[unicode].kern, ctx.glyphs[unicode].numkern * sizeof(sfnkern_t));
    if(!ctx.glyphs[unicode].kern) { ctx.glyphs[unicode].numkern = 0; return 0; }
    while(i > 0 && ctx.glyphs[unicode].kern[i-1].n > next) {
        ctx.glyphs[unicode].kern[i].n = ctx.glyphs[unicode].kern[i-1].n;
        ctx.glyphs[unicode].kern[i].x = ctx.glyphs[unicode].kern[i-1].x;
        ctx.glyphs[unicode].kern[i].y = ctx.glyphs[unicode].kern[i-1].y;
        i--;
    }
    ctx.glyphs[unicode].kern[i].n = next;
    ctx.glyphs[unicode].kern[i].x = x;
    ctx.glyphs[unicode].kern[i].y = y;
    return 1;
}

/**
 * Calculate hinting grid
 *
 * @param unicode character to calculate hints to
 */
void sfn_hintgen(int unicode)
{
    int i, j, x, y, h[256], v[256], mx = 0, my = 0, limit = 3;
    sfncont_t *cont;

    if(unicode < 0 || unicode > 0x10FFFF) return;
    memset(ctx.glyphs[unicode].hintv, 0, 33);
    memset(ctx.glyphs[unicode].hinth, 0, 33);
    if(!ctx.glyphs[unicode].layers) return;
    /* look for vertical or horizontal lines in contour paths */
    memset(h, 0, sizeof(h)); memset(v, 0, sizeof(v));
    for(i = 0; i < ctx.glyphs[unicode].numlayer; i++)
        if(ctx.glyphs[unicode].layers[i].type == SSFN_FRAG_CONTOUR) {
            cont = (sfncont_t*)ctx.glyphs[unicode].layers[i].data;
            x = cont->px; y = cont->py;
            for(j = 0; j < ctx.glyphs[unicode].layers[i].len; j++, cont++) {
                if(cont->type == SSFN_CONTOUR_LINE) {
                    if(x != cont->px && y == cont->py) v[y] += x > cont->px ? x - cont->px : cont->px - x;
                    if(x == cont->px && y != cont->py) h[x] += y > cont->py ? y - cont->py : cont->py - y;
                }
                x = cont->px; y = cont->py;
                if(x > mx) mx = x;
                if(y > my) my = y;
            }
        }
    /* now lets see which coordinates have more points than the limit, those will be the grid lines */
    mx /= limit; my /= limit;
    for(i = 0; i < 256; i++) {
        if(h[i] > my && ctx.glyphs[unicode].hintv[0] < 32)
            ctx.glyphs[unicode].hintv[1 + ctx.glyphs[unicode].hintv[0]++] = i;
        if(v[i] > mx && ctx.glyphs[unicode].hinth[0] < 32)
            ctx.glyphs[unicode].hinth[1 + ctx.glyphs[unicode].hinth[0]++] = i;
    }
}

/**
 * Add a kerning position list
 *
 * @param data pointer to buffer
 * @param len length of buffer
 * @return kpos index or -1 on error
 */
int sfn_kposadd(char *data, int len)
{
    unsigned char *comp;
    int i;

    if(!data || len < 1) return -1;
    comp = rle_enc((unsigned char*)data, len, &len);
    for(i = 0; i < ctx.numkpos; i++)
        if(len == ctx.kpos[i].len && !memcmp(comp, ctx.kpos[i].data, len)) {
            free(comp);
            return i;
        }
    i = ctx.numkpos++;
    ctx.kpos = (sfnkpos_t*)realloc(ctx.kpos, ctx.numkpos * sizeof(sfnkpos_t));
    ctx.kpos[i].idx = i;
    ctx.kpos[i].len = len;
    ctx.kpos[i].data = comp;
    return i;
}

/**
 * Add a fragment to the global list
 *
 * @param type type of the fragment (SSFN_FRAG_x)
 * @param w width
 * @param h height
 * @param data pointer to buffer
 * @return fragment index or -1 on error
 */
int sfn_fragadd(int type, int w, int h, void *data)
{
    int i, l = w * h;
    unsigned char *data2;

    if(w < 1 || h < 0 || !data)
        return -1;
    if(type == SSFN_FRAG_CONTOUR) { l = w * sizeof(sfncont_t); h = 0; } else
    if(type == SSFN_FRAG_KERNING) { l = w * sizeof(sfnkgrp_t); h = 0; } else
    if(type == SSFN_FRAG_HINTING) { l = w; h = 0; }
    for(i = 0; i < ctx.numfrags; i++)
        if(ctx.frags[i].type == type && ctx.frags[i].w == w && ctx.frags[i].h == h && ctx.frags[i].len == l &&
            (type == SSFN_FRAG_CONTOUR && dorounderr ? !frgcmp((sfncont_t*)ctx.frags[i].data, (sfncont_t*)data, w) :
            !memcmp(ctx.frags[i].data, data, l))) {
            ctx.frags[i].cnt++;
            return i;
        }
    if(type != SSFN_FRAG_HINTING || l > 0) {
        data2 = (unsigned char*)malloc(l);
        if(!data2) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return -1; }
        memcpy(data2, data, l);
    } else
        data2 = NULL;
    i = ctx.numfrags++;
    ctx.frags = (sfnfrag_t*)realloc(ctx.frags, ctx.numfrags * sizeof(sfnfrag_t));
    if(!ctx.frags) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return -1; }
    ctx.frags[i].idx = i;
    ctx.frags[i].pos = 0;
    ctx.frags[i].cnt = 1;
    ctx.frags[i].type = type;
    ctx.frags[i].w = w;
    ctx.frags[i].h = h;
    ctx.frags[i].len = l;
    ctx.frags[i].data = data2;
    return i;
}

/**
 * Add a fragment to a character
 *
 * @param unicode character to add to
 * @param type type of the fragment (SSFN_FRAG_x)
 * @param w width
 * @param h height
 * @param x offset x
 * @param y offset y
 * @param data pointer to buffer
 * @return true on success
 */
int sfn_fragchr(int unicode, int type, int w, int h, int x, int y, void *data)
{
    int i;

    if(type == SSFN_FRAG_KERNING && w > 1024) {
        if(verbose > 1) fprintf(stderr, "libsfn: too many kerning groups for U+%06X, truncated to 1024.\n", unicode);
        w = 1024;
    }
    if(ctx.glyphs[unicode].numfrag >= 255) {
        if(verbose > 1) fprintf(stderr, "libsfn: too many fragments for U+%06X, truncated to 255.\n", unicode);
        return 1;
    }
    i = sfn_fragadd(type, w, h, data);
    if(i != -1) {
        ctx.glyphs[unicode].frags = (int*)realloc(ctx.glyphs[unicode].frags,
            (ctx.glyphs[unicode].numfrag + 1) * 3 * sizeof(int));
        if(!ctx.glyphs[unicode].frags) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
        ctx.glyphs[unicode].frags[ctx.glyphs[unicode].numfrag*3+0] = i;
        ctx.glyphs[unicode].frags[ctx.glyphs[unicode].numfrag*3+1] = x;
        ctx.glyphs[unicode].frags[ctx.glyphs[unicode].numfrag*3+2] = y;
        ctx.glyphs[unicode].numfrag++;
        return 1;
    }
    return 0;
}

/**
 * Add a color map entry
 *
 * @param r red
 * @param g green
 * @param b blue
 * @param a alpha
 * @return color map index
 */
unsigned char sfn_cpaladd(int r, int g, int b, int a)
{
    int i, dr, dg, db, m, q;
    int64_t d, dm;

    if(!r && g<2 && !b && (!a || a==0xFF)) return 0xFF;                 /* background */
    if(r == 0xFF && g == 0xFF && b == 0xFF && a == 0xFF) return 0xFE;   /* foreground */
    for(q=1; q<=8; q++) {
        m=-1; dm=256;
        for(i=0; i<ctx.numcpal && (ctx.cpal[i*4+0] || ctx.cpal[i*4+1] || ctx.cpal[i*4+2]); i++) {
            if(a==ctx.cpal[i*4+3] && b==ctx.cpal[i*4+0] && g==ctx.cpal[i*4+1] && r==ctx.cpal[i*4+2]) return i;
            if(b>>q==ctx.cpal[i*4+0]>>q && g>>q==ctx.cpal[i*4+1]>>q && r>>q==ctx.cpal[i*4+2]>>q) {
                dr = r > ctx.cpal[i*4+2] ? r - ctx.cpal[i*4+2] : ctx.cpal[i*4+2] - r;
                dg = g > ctx.cpal[i*4+1] ? g - ctx.cpal[i*4+1] : ctx.cpal[i*4+1] - g;
                db = b > ctx.cpal[i*4+0] ? b - ctx.cpal[i*4+0] : ctx.cpal[i*4+0] - b;
                d = dr*dr + dg*dg + db*db;
                if(d < dm) { dm = d; m = i; }
                if(!dm) break;
            }
        }
        if(dm>9+9+9 && i<254) {
            ctx.cpal[i*4+3] = a;
            ctx.cpal[i*4+2] = r;
            ctx.cpal[i*4+1] = g;
            ctx.cpal[i*4+0] = b;
            ctx.numcpal++;
            return i;
        }
        if(m>=0) {
            ctx.cpal[m*4+3] = ((ctx.cpal[m*4+3] + a) >> 1);
            ctx.cpal[m*4+2] = ((ctx.cpal[m*4+2] + r) >> 1);
            ctx.cpal[m*4+1] = ((ctx.cpal[m*4+1] + g) >> 1);
            ctx.cpal[m*4+0] = ((ctx.cpal[m*4+0] + b) >> 1);
            return m;
        }
    }
    if(verbose > 1) fprintf(stderr,"libsfn: unable to add color to color map, should never happen\n");
    return 0xFE;                                                        /* fallback to foreground */
}

/**
 * Add a UNICODE code point to skip from output
 *
 * @param unicode code point to skip
 */
void sfn_skipadd(int unicode)
{
    int i = ctx.numskip++;
    ctx.skip = (int*)realloc(ctx.skip, ctx.numskip * sizeof(int));
    if(!ctx.skip) ctx.numskip = 0;
    else ctx.skip[i] = unicode;
}

/**
 * Load / import a font into SSFN context
 *
 * @param filename file to load
 * @param dump dump level (0 no dump)
 * @return true on success
 */
int sfn_load(char *filename)
{
    unsigned char *data = NULL, *data2 = NULL, *o, c;
    int r, size, w, h;
    FILE *f;

    f = project_fopen(filename, "rb");
    if(!f) return 0;
    fseek(f, 0, SEEK_END);
    size = (int)ftell(f);
    fseek(f, 0, SEEK_SET);
    if(!size) { fclose(f); return 0; }
    data = (unsigned char*)malloc(size + 1);
    if(!data) { fclose(f); return 0; }
    if(!fread(data, size, 1, f)) { free(data); fclose(f); return 0; }
    fclose(f);
    ctx.total += (long int)size;
    ctx.filename = filename;

    if(data[0] == 0x1f && data[1] == 0x8b) {
        o = data; data += 2;
        if(*data++ != 8 || !size) { free(o); return 0; }
        c = *data++; data += 6;
        if(c & 4) { r = *data++; r += (*data++ << 8); data += r; }
        if(c & 8) { while(*data++ != 0); }
        if(c & 16) { while(*data++ != 0); }
        data2 = (unsigned char*)stbi_zlib_decode_malloc_guesssize_headerflag((const char*)data, size, 4096, &size, 0);
        free(o);
        if(!data2) return 0;
        data2 = realloc(data2, size + 1);
        if(!data2) return 0;
        data = data2;
    }
    data[size] = 0;

    r = 1;
    if(data[0]=='S' && data[1]=='F' && data[2]=='N' && data[3]=='C') {
        if(verbose > 1) fprintf(stderr, "libsfn: file '%s' is a collection with multiple fonts. Extract fonts first.\n", filename);
        r = 0;
    } else if(data[0]=='S' && data[1]=='F' && data[2]=='N' && data[3]=='2') {
        if(verbose > 1) printf("Loaded '%s' (SSFN BIN, %X - %X)\n", filename, rs, re);
        sfn(data, size);
    } else if(data[0]=='#' && data[1]==' ' && data[2]=='S' && data[3]=='c') {
        if(verbose > 1) printf("Loaded '%s' (SSFN ASCII, %X - %X)\n", filename, rs, re);
        asc((char*)data, size);
    } else if(data[0]==0x72 && data[1]==0xB5 && data[2]==0x4A && data[3]==0x86) {
        if(verbose > 1) printf("Loaded '%s' (PSF2, %X - %X)\n", filename, rs, re);
        psf(data, size);
    } else if(data[8]=='P' && data[9]=='F' && data[10]=='F' && data[11]=='2') {
        if(verbose > 1) printf("Loaded '%s' (PFF2, %X - %X)\n", filename, rs, re);
        pff(data, size);
    } else if((data[0]=='M' && data[1]=='Z') || (!data[0] && (data[1] == 2 || data[1] == 3) && !data[5] && (data[6] > ' ' &&
        data[6] < 127))) {
        if(verbose > 1) printf("Loaded '%s' (WinFNT, %X - %X)\n", filename, rs, re);
        fnt(data, size);
    } else if(data[0]=='S' && data[1]=='T' && data[2]=='A' && data[3]=='R') {
        if(verbose > 1) printf("Loaded '%s' (X11 BDF, %X - %X)\n", filename, rs, re);
        bdf((char*)data, size);
    } else if(data[0]==1 && data[1]=='f' && data[2]=='c' && data[3]=='p') {
        if(verbose > 1) printf("Loaded '%s' (X11 PCF, %X - %X)\n", filename, rs, re);
        pcf(data, size);
    } else if(data[0]=='S' && data[1]=='p' && data[2]=='l' && data[3]=='i' && data[4]=='n') {
        if(verbose > 1) printf("Loaded '%s' (SplineFontDB, %X - %X)\n", filename, rs, re);
        sfd((char*)data, size);
    } else if(data[0]==0x89 && data[1]=='P' && data[2]=='N' && data[3]=='G') {
        if(verbose > 1) printf("Loaded '%s' (PNG, %X - %X)\n", filename, rs, re);
        png((unsigned char*)filename, size);
    } else if(data[0]==0 && (data[1]==0 || data[1]==1) &&
        (data[2]==1 || data[2]==2 || data[2]==9 || data[2]==10) &&
        (data[16]==8 || data[16]==24 || data[16]==32)) {
            if(verbose > 1) printf("Loaded '%s' (TARGA, %X - %X)\n", filename, rs, re);
            tga(data, size);
    } else if((data[0]>='0' && data[0]<='9') || (data[0]>='A' && data[0]<='F')) {
        if(verbose > 1) printf("Loaded '%s' (GNU unifont hex, %X - %X)\n", filename, rs, re);
        hex((char*)data, size);
    } else if(ft2_read(data, size)) {
        if(verbose > 1) printf("Loaded '%s' (FreeType2, %X - %X)\n", filename, rs, re);
        ft2_parse();
    } else if((data2 = image_load(filename, &w, &h)) && w > 0 && h > 0) {
        if(verbose > 1) printf("Loaded '%s' (Image, %X - %X)\n", filename, rs, re);
        free(data2);
        png((unsigned char*)filename, size);
    } else {
        if(verbose > 1) fprintf(stderr, "libsfn: unknown format '%s'\n", filename);
        r = 0;
    }

    free(data);
    rs = 0; re = 0x10FFFF;
    return r;
}

/**
 * Serialize an SSFN context into a file
 *
 * @param filename file to save to
 * @param ascii true if saving to ASC
 * @param comp true if file should be gzipped
 * @return true on success
 */
int sfn_save(char *filename)
{
    int unicode, i, j, k, l, o, x, y, h, nc = 0, mc = 0, ml = 0, fs = 0, cs = 0, ks = 0, ls = 0, comp = 1;
    unsigned char p[32], *frg = NULL, *chr = NULL, *krn = NULL, *tmp, *gz;
    unsigned short int lig[SSFN_LIG_LAST-SSFN_LIG_FIRST+1];
    unsigned long int gzs;
    char *strs = NULL, *crd = NULL;
    sfnkgrp_t kgrp, *kgrpf = NULL;
    ssfn_font_t *hdr;
    sfncont_t *cont, *norm;
    FILE *f;
    uint32_t crc;
    z_stream stream;

    for(i = 0; i < 0x110000; i++) {
        ml += ctx.glyphs[i].numlayer;
        if((iswhitespace(i) && (ctx.glyphs[i].adv_x || ctx.glyphs[i].adv_y)) || ctx.glyphs[i].layers) mc++;
    }
    if(!mc || !ml) {
        if(verbose > 1) fprintf(stderr, "libsfn: no layers in font???\n");
        return 0;
    }
    if(verbose > 1) printf("Saving '%s'\n Numchars: %d, Numlayers: %d\n", filename, mc, ml);

        /* ----------------------------- output to optionally gzip compressed binary ----------------------------- */
        ctx.numfrags = 0;
        if(ctx.frags) { free(ctx.frags); ctx.frags = NULL; }
        /* generate fragments */
        for(unicode = 0; unicode <= 0x10FFFF; unicode++) {
            if(ctx.glyphs[unicode].layers) {
                /* hints first (if exists) */
                if(hinting) {
                    if(!ctx.glyphs[unicode].hintv[0] && !ctx.glyphs[unicode].hinth[0]) sfn_hintgen(unicode);
                    if(ctx.glyphs[unicode].hintv[0]) {
                        memset(p, 0, 32);
                        x = ctx.glyphs[unicode].hintv[1];
                        for(i = 1; i < ctx.glyphs[unicode].hintv[0] && ctx.glyphs[unicode].hintv[1+i]; i++)
                            p[i-1] = ctx.glyphs[unicode].hintv[1+i] - ctx.glyphs[unicode].hintv[i];
                        sfn_fragchr(unicode, SSFN_FRAG_HINTING, ctx.glyphs[unicode].hintv[0] - 1, 0, x + 1, 0, p);
                    }
                    if(ctx.glyphs[unicode].hinth[0]) {
                        memset(p, 0, 32);
                        y = ctx.glyphs[unicode].hinth[1];
                        for(i = 1; i < ctx.glyphs[unicode].hinth[0] && ctx.glyphs[unicode].hinth[1+i]; i++)
                            p[i-1] = ctx.glyphs[unicode].hinth[1+i] - ctx.glyphs[unicode].hinth[i];
                        sfn_fragchr(unicode, SSFN_FRAG_HINTING, ctx.glyphs[unicode].hinth[0] - 1, 0, 0, y + 1, p);
                    }
                }
                /* then kerning (if exists) */
                if(ctx.glyphs[unicode].kern) {
                    /* vertical kerning */
                    memset(&kgrp, 0, sizeof(sfnkgrp_t));
                    l = o = 0; kgrpf = NULL;
                    for(i = 0; i < ctx.glyphs[unicode].numkern; i++) {
                        if(ctx.glyphs[unicode].kern[i].x) {
                            if(!kgrp.first) {
                                kgrp.first = kgrp.last = ctx.glyphs[unicode].kern[i].n;
                                l = 1;
                                crd = realloc(crd, l);
                                if(!crd) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                                crd[0] = ctx.glyphs[unicode].kern[i].x;
                            } else {
                                if(kgrp.last + 127 < ctx.glyphs[unicode].kern[i].n) {
                                    kgrp.idx = sfn_kposadd(crd, l);
                                    kgrpf = (sfnkgrp_t*)realloc(kgrpf, (o+1)*sizeof(sfnkgrp_t));
                                    if(!kgrpf) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                                    memcpy(&kgrpf[o++], &kgrp, sizeof(sfnkgrp_t));
                                    kgrp.first = kgrp.last = ctx.glyphs[unicode].kern[i].n;
                                    l = 1;
                                    crd = realloc(crd, l);
                                    if(!crd) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                                    crd[0] = ctx.glyphs[unicode].kern[i].x;
                                } else {
                                    crd = realloc(crd, l + ctx.glyphs[unicode].kern[i].n - kgrp.last);
                                    if(!crd) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                                    for(k = 0; k < ctx.glyphs[unicode].kern[i].n - kgrp.last - 1; k++, l++)
                                        crd[l] = 0;
                                    crd[l++] = ctx.glyphs[unicode].kern[i].x;
                                    kgrp.last = ctx.glyphs[unicode].kern[i].n;
                                }
                            }
                        }
                    }
                    if(kgrp.first) {
                        kgrp.idx = sfn_kposadd(crd, l);
                        kgrpf = (sfnkgrp_t*)realloc(kgrpf, (o+1)*sizeof(sfnkgrp_t));
                        if(!kgrpf) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                        memcpy(&kgrpf[o++], &kgrp, sizeof(sfnkgrp_t));
                    }
                    if(crd) { free(crd); crd = NULL; }
                    if(o && kgrpf) {
                        if(!sfn_fragchr(unicode, SSFN_FRAG_KERNING, o, 0, 1, 0, kgrpf)) return 0;
                        free(kgrpf);
                    }
                    /* horizontal kerning */
                    memset(&kgrp, 0, sizeof(sfnkgrp_t));
                    l = o = 0; kgrpf = NULL;
                    for(i = 0; i < ctx.glyphs[unicode].numkern; i++) {
                        if(ctx.glyphs[unicode].kern[i].y) {
                            if(!kgrp.first) {
                                kgrp.first = kgrp.last = ctx.glyphs[unicode].kern[i].n;
                                l = 1;
                                crd = realloc(crd, l);
                                if(!crd) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                                crd[0] = ctx.glyphs[unicode].kern[i].y;
                            } else {
                                if(kgrp.last + 127 < ctx.glyphs[unicode].kern[i].n) {
                                    kgrp.idx = sfn_kposadd(crd, l);
                                    kgrpf = (sfnkgrp_t*)realloc(kgrpf, (o+1)*sizeof(sfnkgrp_t));
                                    if(!kgrpf) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                                    memcpy(&kgrpf[o++], &kgrp, sizeof(sfnkgrp_t));
                                    kgrp.first = kgrp.last = ctx.glyphs[unicode].kern[i].n;
                                    l = 1;
                                    crd = realloc(crd, l);
                                    if(!crd) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                                    crd[0] = ctx.glyphs[unicode].kern[i].y;
                                } else {
                                    crd = realloc(crd, l + ctx.glyphs[unicode].kern[i].n - kgrp.last);
                                    if(!crd) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                                    for(k = 0; k < ctx.glyphs[unicode].kern[i].n - kgrp.last - 1; k++, l++)
                                        crd[l] = 0;
                                    crd[l++] = ctx.glyphs[unicode].kern[i].y;
                                    kgrp.last = ctx.glyphs[unicode].kern[i].n;
                                }
                            }
                        }
                    }
                    if(kgrp.first) {
                        kgrp.idx = sfn_kposadd(crd, l);
                        kgrpf = (sfnkgrp_t*)realloc(kgrpf, (o+1)*sizeof(sfnkgrp_t));
                        if(!kgrpf) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                        memcpy(&kgrpf[o++], &kgrp, sizeof(sfnkgrp_t));
                    }
                    if(crd) { free(crd); crd = NULL; }
                    if(o && kgrpf) {
                        if(!sfn_fragchr(unicode, SSFN_FRAG_KERNING, o, 0, 0, 1, kgrpf)) return 0;
                        free(kgrpf);
                    }
                }
                /* layers to fragments */
                for(j = 0; j < ctx.glyphs[unicode].numlayer; j++) {
                    if(pbar) (*pbar)(1, 5, ++nc, ml, PBAR_GENFRAG);
                    if(ctx.glyphs[unicode].layers[j].color < 0xFE) {
                            if(ctx.glyphs[unicode].numfrag == 255) {
                                if(verbose > 1) fprintf(stderr, "libsfn: too many fragments for U+%06X, truncated to 255.\n", unicode);
                            } else {
                                ctx.glyphs[unicode].frags = (int*)realloc(ctx.glyphs[unicode].frags,
                                    (ctx.glyphs[unicode].numfrag + 1) * 3 * sizeof(int));
                                if(!ctx.glyphs[unicode].frags) {
                                    if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); }
                                    return 0;
                                }
                                ctx.glyphs[unicode].frags[ctx.glyphs[unicode].numfrag*3+0] =
                                    ctx.glyphs[unicode].layers[j].color;
                                ctx.glyphs[unicode].frags[ctx.glyphs[unicode].numfrag*3+1] = 255;
                                ctx.glyphs[unicode].frags[ctx.glyphs[unicode].numfrag*3+2] = 255;
                                ctx.glyphs[unicode].numfrag++;
                            }
                    }
                    switch(ctx.glyphs[unicode].layers[j].type) {
                        case SSFN_FRAG_CONTOUR:
                            /* normalize coordinates */
                            cont = (sfncont_t*)ctx.glyphs[unicode].layers[j].data;
                            for(i = 0, x = y = 256; i < ctx.glyphs[unicode].layers[j].len; i++, cont++) {
                                if(x > cont->px) x = cont->px;
                                if(y > cont->py) y = cont->py;
                                if(cont->type >= SSFN_CONTOUR_QUAD) {
                                    if(x > cont->c1x) x = cont->c1x;
                                    if(y > cont->c1y) y = cont->c1y;
                                    if(cont->type >= SSFN_CONTOUR_CUBIC) {
                                        if(x > cont->c2x) x = cont->c2x;
                                        if(y > cont->c2y) y = cont->c2y;
                                    }
                                }
                            }
                            if(x > 254) x = 254;
                            norm = (sfncont_t*)malloc(ctx.glyphs[unicode].layers[j].len * sizeof(sfncont_t));
                            if(!norm) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                            memset(norm, 0, ctx.glyphs[unicode].layers[j].len * sizeof(sfncont_t));
                            /* add to fragments list */
                            cont = (sfncont_t*)ctx.glyphs[unicode].layers[j].data;
                            for(i = 0; i < ctx.glyphs[unicode].layers[j].len; i++, cont++) {
                                norm[i].type = cont->type;
                                norm[i].px = cont->px - x;
                                norm[i].py = cont->py - y;
                                if(cont->type >= SSFN_CONTOUR_QUAD) {
                                    norm[i].c1x = cont->c1x - x;
                                    norm[i].c1y = cont->c1y - y;
                                    if(cont->type >= SSFN_CONTOUR_CUBIC) {
                                        norm[i].c2x = cont->c2x - x;
                                        norm[i].c2y = cont->c2y - y;
                                    }
                                }
                            }
                            if(!sfn_fragchr(unicode, SSFN_FRAG_CONTOUR, ctx.glyphs[unicode].layers[j].len, 0, x, y, norm))
                                return 0;
                            free(norm);
                        break;
                        case SSFN_FRAG_BITMAP:
                        case SSFN_FRAG_PIXMAP:
                            h = ctx.glyphs[unicode].height; x = 1;
                            for(y = 0; y < h; y += x) {
                                for(;y < h && isempty(ctx.glyphs[unicode].width,
                                    ctx.glyphs[unicode].layers[j].data + (h - 1) * ctx.glyphs[unicode].width); h--);
                                for(;y < h && isempty(ctx.glyphs[unicode].width,
                                    ctx.glyphs[unicode].layers[j].data + y * ctx.glyphs[unicode].width); y++);
                                for(x = 0; (y + x) < h && !isempty(ctx.glyphs[unicode].width,
                                    ctx.glyphs[unicode].layers[j].data + (y + x) * ctx.glyphs[unicode].width); x++);
                                if(x > 0 && !sfn_fragchr(unicode, ctx.glyphs[unicode].layers[j].type, ctx.glyphs[unicode].width,
                                    x, 0, y, ctx.glyphs[unicode].layers[j].data + y * ctx.glyphs[unicode].width))
                                        return 0;
                            }
                        break;
                    }
                }
                /* should never reached, just to be on the safe side */
                if(ctx.glyphs[unicode].numfrag > 255) {
                    if(verbose > 1) fprintf(stderr, "libsfn: too many fragments for U+%06X, truncated to 255.\n", unicode);
                    ctx.glyphs[unicode].numfrag = 255;
                }
            }
        }

        /* serialize kerning positions */
        qsort(ctx.kpos, ctx.numkpos, sizeof(sfnkpos_t), possrt);
        for(i = ks = 0; i < ctx.numkpos; i++) {
            ctx.kpos[i].pos = -1;
            for(j = 0; krn && j < ks - ctx.kpos[i].len; j++)
                if(!memcmp(krn, ctx.kpos[i].data, ctx.kpos[i].len)) { ctx.kpos[i].pos = j; break; }
            if(ctx.kpos[i].pos == -1) {
                ctx.kpos[i].pos = ks;
                krn = (unsigned char *)realloc(krn, ks + ctx.kpos[i].len);
                if(!krn) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                memcpy(krn + ks, ctx.kpos[i].data, ctx.kpos[i].len);
                ks += ctx.kpos[i].len;
            }
        }

        /* generate header and string table */
        memset(lig, 0, sizeof(lig));
        o = y = sizeof(ssfn_font_t) + (ctx.name ? strlen(ctx.name) : 0) + 1 + (ctx.familyname ? strlen(ctx.familyname) : 0) + 1 +
            (ctx.subname ? strlen(ctx.subname) : 0) + 1 + (ctx.revision ? strlen(ctx.revision) : 0) + 1 +
            (ctx.manufacturer ? strlen(ctx.manufacturer) : 0) + 1 + (ctx.license ? strlen(ctx.license) : 0) + 1;
        for(ls = 0; ls < SSFN_LIG_LAST-SSFN_LIG_FIRST+1 && ctx.ligatures[ls] && ctx.ligatures[ls][0]; ls++) {
            if(o + strlen(ctx.ligatures[ls]) + 1 > 65535) break;
            o += strlen(ctx.ligatures[ls]) + 1;
        }
        hdr = (ssfn_font_t*)malloc(o);
        if(!hdr) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
        memset(hdr, 0, o);
        memcpy(hdr->magic, SSFN_MAGIC, 4);
        hdr->size = o + 4;
        hdr->type = (ctx.family & 0x0F) | ((ctx.style & 0x0F) << 4);
        hdr->width = ctx.width;
        hdr->height = ctx.height;
        hdr->baseline = ctx.baseline;
        hdr->underline = ctx.underline;
        strs = ((char*)hdr) + sizeof(ssfn_font_t);
        if(ctx.name) { j = strlen(ctx.name) + 1; memcpy(strs, ctx.name, j); strs += j; } else strs++;
        if(ctx.familyname) { j = strlen(ctx.familyname) + 1; memcpy(strs, ctx.familyname, j); strs += j; } else strs++;
        if(ctx.subname) { j = strlen(ctx.subname) + 1; memcpy(strs, ctx.subname, j); strs += j; } else strs++;
        if(ctx.revision) { j = strlen(ctx.revision) + 1; memcpy(strs, ctx.revision, j); strs += j; } else strs++;
        if(ctx.manufacturer) { j = strlen(ctx.manufacturer) + 1; memcpy(strs, ctx.manufacturer, j); strs += j; } else strs++;
        if(ctx.license) { j = strlen(ctx.license) + 1; memcpy(strs, ctx.license, j); strs += j; } else strs++;
        for(j = 0; j < ls; j++) {
            lig[j] = y;
            x = strlen(ctx.ligatures[j]) + 1;
            memcpy(strs, ctx.ligatures[j], x);
            strs += x; y += x;
        }

        /* compress fragments */
        for(i = 0; i < ctx.numfrags; i++) {
            if(pbar) (*pbar)(2, 5, i, ctx.numfrags, PBAR_COMPFRAG);
        }

        /* serialize fragments */
        fidx = (int*)malloc(ctx.numfrags * sizeof(int));
        qsort(ctx.frags, ctx.numfrags, sizeof(sfnfrag_t), frgsrt);
        for(i = 0; i < ctx.numfrags; i++)
            fidx[ctx.frags[i].idx] = i;
        hdr->fragments_offs = o;
        for(i = 0; i < ctx.numfrags; i++) {
            if(pbar) (*pbar)(3, 5, i, ctx.numfrags, PBAR_SERFRAG);
            ctx.frags[i].pos = o + fs;
            frg = (unsigned char*)realloc(frg, fs + 5 + ctx.frags[i].len);
            if(!frg) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
            switch(ctx.frags[i].type) {
                case SSFN_FRAG_CONTOUR:
                    if(ctx.frags[i].w>64)
                        frg[fs++] = (((ctx.frags[i].w - 1) >> 8) & 0x3F) | 0x40;
                    frg[fs++] = (ctx.frags[i].w - 1) & 0xFF;
                    k = fs; fs += (ctx.frags[i].w + 3) / 4;
                    memset(frg + k, 0, fs - k);
                    for(j = 0, cont = (sfncont_t*)ctx.frags[i].data; j < ctx.frags[i].w; j++, cont++) {
                        frg[k + (j / 4)] |= cont->type << ((j & 3) * 2);
                        frg[fs++] = cont->px;
                        frg[fs++] = cont->py;
                        if(cont->type >= SSFN_CONTOUR_QUAD) {
                            frg[fs++] = cont->c1x;
                            frg[fs++] = cont->c1y;
                            if(cont->type >= SSFN_CONTOUR_CUBIC) {
                                frg[fs++] = cont->c2x;
                                frg[fs++] = cont->c2y;
                            }
                        }
                    }
                break;
                case SSFN_FRAG_BITMAP:
                    k = ((ctx.frags[i].w + 7) >> 3) & 0x1F;
                    frg[fs++] = 0x80 | (k - 1);
                    frg[fs++] = ctx.frags[i].h - 1;
                    for(y = 0; y < ctx.frags[i].h; y++) {
                        memset(frg + fs, 0, k);
                        for(x = 0; x < ctx.frags[i].w; x++) {
                            if(ctx.frags[i].data[y * ctx.frags[i].w + x] != 0xFF)
                                frg[fs + (x >> 3)] |= 1 << (x & 7);
                        }
                        fs += k;
                    }
                break;
                case SSFN_FRAG_PIXMAP:
                    tmp = rle_enc(ctx.frags[i].data, ctx.frags[i].w * ctx.frags[i].h, &k);
                    if(!tmp || k < 2) return 0;
                    frg = (unsigned char*)realloc(frg, fs + 5 + k);
                    if(!frg) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                    frg[fs++] = 0xA0 | (((k - 1) >> 8) & 0x1F);
                    frg[fs++] = (k - 1) & 0xFF;
                    frg[fs++] = ctx.frags[i].w - 1;
                    frg[fs++] = ctx.frags[i].h - 1;
                    memcpy(frg + fs, tmp, k);
                    fs += k;
                    free(tmp);
                break;
                case SSFN_FRAG_KERNING:
                    frg[fs++] = (((ctx.frags[i].w - 1) >> 8) & 0x3) | 0xC0 | /*kerning context*/((0 & 7) << 2);
                    frg[fs++] = (ctx.frags[i].w - 1) & 0xFF;
                    for(j = 0, kgrpf = (sfnkgrp_t*)ctx.frags[i].data; j < ctx.frags[i].w; j++, kgrpf++) {
                        /* this is not efficient, but fast enough */
                        for(k = x = 0; k < ctx.numkpos; k++)
                            if(ctx.kpos[k].idx == kgrpf->idx) { x = ctx.kpos[k].pos; break; }
                        frg[fs++] = kgrpf->first & 0xFF;
                        frg[fs++] = (kgrpf->first >> 8) & 0xFF;
                        frg[fs++] = ((kgrpf->first >> 16) & 0x0F) | ((x >> 20) & 0xF0);
                        frg[fs++] = kgrpf->last & 0xFF;
                        frg[fs++] = (kgrpf->last >> 8) & 0xFF;
                        frg[fs++] = ((kgrpf->last >> 16) & 0x0F) | ((x >> 12) & 0xF0);
                        frg[fs++] = x & 0xFF;
                        frg[fs++] = (x >> 8) & 0xFF;
                    }
                break;
                case SSFN_FRAG_HINTING:
                    if(ctx.frags[i].w && ctx.frags[i].data) {
                        frg[fs++] = ((ctx.frags[i].w & 31) | 0xE0);
                        memcpy(frg + fs, ctx.frags[i].data, ctx.frags[i].w);
                        fs += ctx.frags[i].w;
                    } else
                        frg[fs++] = 0xE0;
                break;
            }
        }
        hdr->size += fs;
        o += fs;

        /* serialize character map */
        hdr->characters_offs = o;
        unicode = -1;
        for(i = nc = 0; i <= 0x10FFFF; i++) {
            if((iswhitespace(i) && (ctx.glyphs[i].adv_x || ctx.glyphs[i].adv_y)) ||
                ctx.glyphs[i].layers) {
                if(pbar) (*pbar)(4, 5, ++nc, mc, PBAR_WRTCHARS);
                j = i - unicode - 1;
                chr = (unsigned char*)realloc(chr, cs+256+ctx.glyphs[i].numfrag*6);
                if(!chr) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
                while(j > 0) {
                    if(j <= 64) { chr[cs++] = ((j-1) & 0x3F) | 0x80; break; }
                    else {
                        while(j > 65536) { chr[cs++] = 0xFF; j -= 65536; }
                        while(j > 16128) { chr[cs++] = 0xFE; chr[cs++] = 0xFF; j -= 16128; }
                        if(j > 64) { chr[cs++] = (((j-1) >> 8) & 0x3F) | 0xC0; chr[cs++] = (j-1) & 0xFF; break; }
                    }
                }
                k = (ctx.glyphs[i].ovl_x & 0x3F);
                for(j = 0; j < ctx.glyphs[i].numfrag; j++)
                    if(ctx.frags[fidx[ctx.glyphs[i].frags[j*3]]].pos > 0xFFFFFF) { k |= 0x40; break; }
                qsort(ctx.glyphs[i].frags, ctx.glyphs[i].numfrag, 3*sizeof(int), frdsrt);
                chr[cs++] = k;
                chr[cs++] = ctx.glyphs[i].numfrag;
                chr[cs++] = ctx.glyphs[i].width;
                chr[cs++] = ctx.glyphs[i].height;
                chr[cs++] = ctx.glyphs[i].adv_x;
                chr[cs++] = ctx.glyphs[i].adv_y;
                for(j = 0; j < ctx.glyphs[i].numfrag; j++) {
                    chr[cs++] = ctx.glyphs[i].frags[j*3+1];
                    if(ctx.glyphs[i].frags[j*3+1] == 255) {
                        memcpy(chr + cs, &ctx.glyphs[i].frags[j*3+0], 4);
                        cs += 4;
                        if(k & 0x40)
                            chr[cs++] = 0;
                    } else {
                        chr[cs++] = ctx.glyphs[i].frags[j*3+2];
                        x = ctx.frags[fidx[ctx.glyphs[i].frags[j*3+0]]].pos;
                        chr[cs++] = x & 0xFF;
                        chr[cs++] = (x >> 8) & 0xFF;
                        chr[cs++] = (x >> 16) & 0xFF;
                        if(k & 0x40)
                            chr[cs++] = (x >> 24) & 0xFF;
                    }
                }
                unicode = i;
            }
        }
        free(fidx);
        j = 0x110000 - unicode;
        chr = (unsigned char*)realloc(chr, cs+256);
        if(!chr) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
        while(j > 0) {
            if(j <= 64) { chr[cs++] = ((j-1) & 0x3F) | 0x80; break; }
            else {
                while(j > 65536) { chr[cs++] = 0xFF; j -= 65536; }
                while(j > 16128) { chr[cs++] = 0xFE; chr[cs++] = 0xFF; j -= 16128; }
                if(j > 64) { chr[cs++] = (((j-1) >> 8) & 0x3F) | 0xC0; chr[cs++] = (j-1) & 0xFF; break; }
            }
        }
        hdr->size += cs;
        o += cs;

        /* ligatures */
        if(ls) {
            hdr->ligature_offs = o;
            hdr->size += ls*2;
            o += ls*2;
        }

        /* kerning table */
        if(ks) {
            hdr->kerning_offs = o;
            hdr->size += ks;
            o += ks;
        }

        /* color map */
        if(ctx.numcpal) {
            hdr->cmap_offs = o;
            hdr->size += ctx.numcpal * 4;
        }

        /* write out file */
        x = 0;
        if(comp) {
            gzs = hdr->fragments_offs + fs + cs + ls*2 + ks + ctx.numcpal*4 + 4;
            stream.avail_out = compressBound(gzs) + 16;
            gz = malloc(stream.avail_out);
            if(!gz) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return 0; }
            stream.zalloc = (alloc_func)0;
            stream.zfree = (free_func)0;
            stream.opaque = (voidpf)0;

            if(deflateInit(&stream, 9) != Z_OK) { if(verbose > 1) { fprintf(stderr,"libsfn: deflate error\n"); } return 0; }

            stream.next_out = (z_const Bytef *)gz + 8;

            if(pbar) (*pbar)(5, 5, 0, 5, PBAR_WRTFILE);
            stream.avail_in = hdr->fragments_offs;
            stream.next_in = (z_const Bytef *)hdr;
            crc = crc32(0, stream.next_in, stream.avail_in);
            deflate(&stream, Z_NO_FLUSH);

            if(pbar) (*pbar)(5, 5, 1, 5, PBAR_WRTFILE);
            stream.avail_in = fs;
            stream.next_in = (z_const Bytef *)frg;
            crc = crc32(crc, stream.next_in, stream.avail_in);
            deflate(&stream, Z_NO_FLUSH);

            if(pbar) (*pbar)(5, 5, 2, 5, PBAR_WRTFILE);
            stream.avail_in = cs;
            stream.next_in = (z_const Bytef *)chr;
            crc = crc32(crc, stream.next_in, stream.avail_in);
            deflate(&stream, Z_NO_FLUSH);

            if(ls) {
                if(pbar) (*pbar)(5, 5, 3, 5, PBAR_WRTFILE);
                stream.avail_in = ls*2;
                stream.next_in = (z_const Bytef *)lig;
                crc = crc32(crc, stream.next_in, stream.avail_in);
                deflate(&stream, Z_NO_FLUSH);
            }
            if(ks) {
                if(pbar) (*pbar)(5, 5, 4, 5, PBAR_WRTFILE);
                stream.avail_in = ks;
                stream.next_in = (z_const Bytef *)krn;
                crc = crc32(crc, stream.next_in, stream.avail_in);
                deflate(&stream, Z_NO_FLUSH);
            }
            if(ctx.numcpal) {
                if(pbar) (*pbar)(5, 5, 5, 5, PBAR_WRTFILE);
                stream.avail_in = ctx.numcpal*4;
                stream.next_in = (z_const Bytef *)ctx.cpal;
                crc = crc32(crc, stream.next_in, stream.avail_in);
                deflate(&stream, Z_NO_FLUSH);
            }
            stream.avail_in = 4;
            stream.next_in = (z_const Bytef *)SSFN_ENDMAGIC;
            crc = crc32(crc, stream.next_in, stream.avail_in);
            deflate(&stream, Z_FINISH);

            memset(gz, 0, 10);
            gz[0] = 0x1f; gz[1] = 0x8b; gz[2] = 0x8; gz[9] = 3;

            f = project_fopen(filename, "wb+");
            if(f) {
                ctx.filename = filename;
                fwrite(gz, stream.total_out + 4, 1, f);
                fwrite(&crc, 4, 1, f);
                fwrite(&gzs, 4, 1, f);
                fclose(f);
                x = stream.total_out + 12;
            } else {
                if(verbose > 1) fprintf(stderr, "libsfn: unable to write '%s'\n", filename);
            }
            free(gz);
        } else {
            f = project_fopen(filename, "wb+");
            if(f) {
                ctx.filename = filename;
                if(pbar) (*pbar)(5, 5, 0, 5, PBAR_WRTFILE);
                fwrite(hdr, hdr->fragments_offs, 1, f);
                if(pbar) (*pbar)(5, 5, 1, 5, PBAR_WRTFILE);
                if(fs) fwrite(frg, fs, 1, f);
                if(pbar) (*pbar)(5, 5, 2, 5, PBAR_WRTFILE);
                if(cs) fwrite(chr, cs, 1, f);
                if(pbar) (*pbar)(5, 5, 3, 5, PBAR_WRTFILE);
                if(ls) fwrite(lig, ls*2, 1, f);
                if(pbar) (*pbar)(5, 5, 4, 5, PBAR_WRTFILE);
                if(ks) fwrite(krn, ks, 1, f);
                if(pbar) (*pbar)(5, 5, 5, 5, PBAR_WRTFILE);
                if(ctx.numcpal) fwrite(ctx.cpal, ctx.numcpal*4, 1, f);
                fwrite(SSFN_ENDMAGIC, 4, 1, f);
                fclose(f);
                x = hdr->size;
            } else {
                if(verbose > 1) fprintf(stderr, "libsfn: unable to write '%s'\n", filename);
            }
        }

        /* free resources */
        for(unicode = 0; unicode <= 0x10FFFF; unicode++) {
            if(ctx.glyphs[unicode].frags) {
                free(ctx.glyphs[unicode].frags);
                ctx.glyphs[unicode].frags = NULL;
            }
            ctx.glyphs[unicode].numfrag = 0;
        }
        if(ctx.frags) {
            for(i = 0; i < ctx.numfrags; i++)
                free(ctx.frags[i].data);
            free(ctx.frags);
            ctx.frags = NULL;
        }
        if(ctx.kpos) {
            for(i = 0; i < ctx.numkpos; i++)
                free(ctx.kpos[i].data);
            free(ctx.kpos);
            ctx.kpos = NULL;
        }
        if(frg) free(frg);
        if(krn) free(krn);
        if(chr) free(chr);
        free(hdr);
        ctx.numfrags = 0;
    return x;
}

/**
 * Set the family type
 *
 * @param t family type
 */
void sfn_setfamilytype(int t)
{
    if(t >= 0 && t <= SSFN_FAMILY_HAND) ctx.family = t;
}

/**
 * Set one of the string attributes
 *
 * @param s pointer to a string pointer
 * @param n new string
 * @param len length of the string if not zero terminated
 */
void sfn_setstr(char **s, char *n, int len)
{
    int i, l = len;

    if(*s) { free(*s); *s = NULL; }
    if(!n) return;
    while(*n == ' ' || *n == '\t') { n++; if(len) l--; }
    if(len && !l) return;
    for(i = 0; (!len || i < l) && n[i] && n[i] != '\"' && n[i] != '\r' && n[i] != '\n'; i++);
    while(i && (n[i-1] == ' ' || n[i-1] == '\t')) i--;
    if(!i) return;
    *s = malloc(i + 1);
    if(!*s) return;
    memcpy(*s, n, i);
    *(*s + i) = 0;
}

/**
 * Sanitize context, make sure dimensions and positions are valid
 */
void sfn_sanitize(int unicode)
{
    sfncont_t *cont;
    int i, j, k, l, m = 0, h, s, e;

    if(unicode == -1) { s = 0; e = 0x110000; } else { s = unicode; e = unicode + 1; }
    for(i = s; i < e; i++) {
        if(ctx.glyphs[i].layers) {
            h = 0;
            for(j = 0; j < ctx.glyphs[i].numlayer; j++)
                if(ctx.glyphs[i].layers[j].type == SSFN_FRAG_CONTOUR) {
                    ctx.glyphs[i].width = ctx.glyphs[i].height = 0;
                    break;
                }
            for(j = 0; j < ctx.glyphs[i].numlayer; j++) {
                if(ctx.glyphs[i].layers[j].type == SSFN_FRAG_CONTOUR) {
                    ctx.glyphs[i].layers[j].minx = ctx.glyphs[i].layers[j].miny = 256;
                    for(k = 0, cont = (sfncont_t*)ctx.glyphs[i].layers[j].data; k < ctx.glyphs[i].layers[j].len; k++, cont++) {
                        if(cont->px > m) m = cont->px;
                        if(cont->px + 1 > ctx.glyphs[i].width) ctx.glyphs[i].width = cont->px + 1;
                        if(cont->py + 1 > ctx.glyphs[i].height) ctx.glyphs[i].height = cont->py + 1;
                        if(cont->px < ctx.glyphs[i].layers[j].minx) ctx.glyphs[i].layers[j].minx = cont->px;
                        if(cont->py < ctx.glyphs[i].layers[j].miny) ctx.glyphs[i].layers[j].miny = cont->py;
                        if(cont->type >= SSFN_CONTOUR_QUAD) {
                            if(cont->c1x > m) m = cont->c1x;
                            if(cont->c1x + 1 > ctx.glyphs[i].width) ctx.glyphs[i].width = cont->c1x + 1;
                            if(cont->c1y + 1 > ctx.glyphs[i].height) ctx.glyphs[i].height = cont->c1y + 1;
                            if(cont->c1x < ctx.glyphs[i].layers[j].minx) ctx.glyphs[i].layers[j].minx = cont->c1x;
                            if(cont->c1y < ctx.glyphs[i].layers[j].miny) ctx.glyphs[i].layers[j].miny = cont->c1y;
                            if(cont->type >= SSFN_CONTOUR_CUBIC) {
                                if(cont->c2x > m) m = cont->c2x;
                                if(cont->c2x + 1 > ctx.glyphs[i].width) ctx.glyphs[i].width = cont->c2x + 1;
                                if(cont->c2y + 1 > ctx.glyphs[i].height) ctx.glyphs[i].height = cont->c2y + 1;
                                if(cont->c2x < ctx.glyphs[i].layers[j].minx) ctx.glyphs[i].layers[j].minx = cont->c2x;
                                if(cont->c2y < ctx.glyphs[i].layers[j].miny) ctx.glyphs[i].layers[j].miny = cont->c2y;
                            }
                        }
                    }
                } else {
                    ctx.glyphs[i].layers[j].minx = ctx.glyphs[i].layers[j].miny = 256;
                    for(l = 0; l < ctx.glyphs[i].height; l++)
                        for(k = 0; k < ctx.glyphs[i].width; k++)
                            if(ctx.glyphs[i].layers[j].data[l * ctx.glyphs[i].width + k] != 0xFF) {
                                if(k > m) m = k;
                                if(k < ctx.glyphs[i].layers[j].minx) ctx.glyphs[i].layers[j].minx = k;
                                if(l < ctx.glyphs[i].layers[j].miny) ctx.glyphs[i].layers[j].miny = l;
                            }
                }
                if(ctx.glyphs[i].layers[j].color >= ctx.numcpal || ctx.glyphs[i].layers[j].type == SSFN_FRAG_PIXMAP)
                    ctx.glyphs[i].layers[j].color = 0xFE;
                /* try to autodetect base line */
                if(!ctx.baseline && (ctx.glyphs[i].layers[j].type == SSFN_FRAG_BITMAP || ctx.glyphs[i].layers[j].type ==
                    SSFN_FRAG_PIXMAP) && ((i >= '0' && i <= '9') || (i >= 'A' && i < 'Q') || (i >= 'a' && i < 'g') ||
                    i == 'h' || i == 'i' || (i >= 'k' && i < 'p') || (i >= 'r' && i < 'y') || i == 'z')) {
                        if(ctx.glyphs[i].layers[j].type == SSFN_FRAG_CONTOUR) {
                            if(ctx.glyphs[i].height > h + 1) h = ctx.glyphs[i].height - 1;
                        } else {
                            l = ctx.glyphs[i].height - 1;
                            for(;0 < l && isempty(ctx.glyphs[i].width,
                                ctx.glyphs[i].layers[j].data + l * ctx.glyphs[i].width); l--);
                            if(l > h) h = l;
                        }
                    }
            }
            if(!ctx.baseline) ctx.baseline = h;
            qsort(ctx.glyphs[i].layers, ctx.glyphs[i].numlayer, sizeof(sfnlayer_t), lyrsrt);
        }
        if(ctx.glyphs[i].kern)
            qsort(ctx.glyphs[i].kern, ctx.glyphs[i].numkern, sizeof(sfnkern_t), krnsrt);
        if(ctx.glyphs[i].ovl_x > 63) ctx.glyphs[i].ovl_x = 63;
        if(!ctx.glyphs[i].adv_x && !ctx.glyphs[i].adv_y && m)
            ctx.glyphs[i].adv_x = m + 2 + adv;
        if(ctx.glyphs[i].adv_x) ctx.glyphs[i].adv_y = 0;
        if(ctx.family == SSFN_FAMILY_MONOSPACE) {
            if(ctx.glyphs[i].adv_x) ctx.glyphs[i].adv_x = ((ctx.glyphs[i].width + 7) & ~7) + adv;
            else ctx.glyphs[i].adv_y = ctx.height + adv;
        }
    }
    ctx.width = ctx.height = 0;
    for(i = 0; i < 0x110000; i++) {
        if(ctx.glyphs[i].width > ctx.width) ctx.width = ctx.glyphs[i].width;
        if(ctx.glyphs[i].height > ctx.height) ctx.height = ctx.glyphs[i].height;
    }
    if(!ctx.baseline || ctx.baseline > ctx.height) ctx.baseline = ctx.height * 80 / 100;
    if(relul) ctx.underline = ctx.baseline + relul;
    if(ctx.underline <= ctx.baseline || ctx.underline > ctx.height)
        ctx.underline = ctx.baseline + (ctx.height - ctx.baseline - 1) / 2;
    if(ctx.family > SSFN_FAMILY_HAND) ctx.family = SSFN_FAMILY_HAND;
}

/* add a line to contour */
static void _sfn_l(int p, int h, int x, int y)
{
    if(x < 0 || y < 0 || x >= p || y >= h || (
        ((ctx.lx + (1 << (SSFN_PREC-1))) >> SSFN_PREC) == ((x + (1 << (SSFN_PREC-1))) >> SSFN_PREC) &&
        ((ctx.ly + (1 << (SSFN_PREC-1))) >> SSFN_PREC) == ((y + (1 << (SSFN_PREC-1))) >> SSFN_PREC))) return;
    if(ctx.ap <= ctx.np) {
        ctx.ap = ctx.np + 512;
        ctx.p = (uint16_t*)realloc(ctx.p, ctx.ap * sizeof(uint16_t));
        if(!ctx.p) { ctx.ap = ctx.np = 0; return; }
    }
    if(!ctx.np) {
        ctx.p[0] = ctx.mx;
        ctx.p[1] = ctx.my;
        ctx.np += 2;
    }
    ctx.p[ctx.np+0] = x;
    ctx.p[ctx.np+1] = y;
    ctx.np += 2;
    ctx.lx = x; ctx.ly = y;
}

/* add a Bezier curve to contour */
static void _sfn_b(int p,int h, int x0,int y0, int x1,int y1, int x2,int y2, int x3,int y3, int l)
{
    int m0x, m0y, m1x, m1y, m2x, m2y, m3x, m3y, m4x, m4y,m5x, m5y;
    if(l<4 && (x0!=x3 || y0!=y3)) {
        m0x = ((x1-x0)/2) + x0;     m0y = ((y1-y0)/2) + y0;
        m1x = ((x2-x1)/2) + x1;     m1y = ((y2-y1)/2) + y1;
        m2x = ((x3-x2)/2) + x2;     m2y = ((y3-y2)/2) + y2;
        m3x = ((m1x-m0x)/2) + m0x;  m3y = ((m1y-m0y)/2) + m0y;
        m4x = ((m2x-m1x)/2) + m1x;  m4y = ((m2y-m1y)/2) + m1y;
        m5x = ((m4x-m3x)/2) + m3x;  m5y = ((m4y-m3y)/2) + m3y;
        _sfn_b(p,h, x0,y0, m0x,m0y, m3x,m3y, m5x,m5y, l+1);
        _sfn_b(p,h, m5x,m5y, m4x,m4y, m2x,m2y, x3,y3, l+1);
    } else
    if(l) _sfn_l(p,h, x3, y3);
}

/**
 * Rasterize a layer or glyph
 *
 * @param size size
 * @param unicode code point
 * @param layer layer index or -1 for all
 * @param postproc do postprocess for bitmaps
 * @param g glyph data to return
 */
int sfn_glyph(int size, int unicode, int layer, int postproc, sfngc_t *g)
{
    uint8_t ci = 0, cb = 0;
    uint16_t r[640];
    int i, j, k, l, p, m, n, o, w, h, a, A, b, B, nr, x;
    sfncont_t *cont;

    if(unicode < 0 || unicode > 0x10FFFF || !ctx.glyphs[unicode].numlayer || layer >= ctx.glyphs[unicode].numlayer ||
        ctx.height < 1) return 0;
    h = (size > ctx.height ? (size + 4) & ~3 : ctx.height);
    w = ctx.glyphs[unicode].width * h / ctx.height;
    p = w + (ci ? h / SSFN_ITALIC_DIV : 0) + cb;
    g->p = p;
    g->h = h;
    if(p * h >= (260 + 260 / SSFN_ITALIC_DIV) << 8) return 0;
    g->x = ctx.glyphs[unicode].adv_x * h / ctx.height;
    g->y = ctx.glyphs[unicode].adv_y * h / ctx.height;
    g->o = ctx.glyphs[unicode].ovl_x * h / ctx.height;
    memset(&g->data, 0xFF, p * h);
    ctx.lx = ctx.ly = 0;
    for(n = (layer == -1 ? 0 : layer); n < (layer == -1 ? ctx.glyphs[unicode].numlayer : layer + 1); n++) {
        switch(ctx.glyphs[unicode].layers[n].type) {
            case SSFN_FRAG_CONTOUR:
                for(i = 0, ctx.np = 0, cont = (sfncont_t*)ctx.glyphs[unicode].layers[n].data;
                    i < ctx.glyphs[unicode].layers[n].len; i++, cont++) {
                        k = (cont->px << SSFN_PREC) * h / ctx.height; m = (cont->py << SSFN_PREC) * h / ctx.height;
                        switch(cont->type) {
                            case SSFN_CONTOUR_MOVE: ctx.mx = ctx.lx = k; ctx.my = ctx.ly = m; break;
                            case SSFN_CONTOUR_LINE: _sfn_l(p << SSFN_PREC, h << SSFN_PREC, k, m); break;
                            case SSFN_CONTOUR_QUAD:
                                a = (cont->c1x << SSFN_PREC) * h / ctx.height;
                                A = (cont->c1y << SSFN_PREC) * h / ctx.height;
                                _sfn_b(p << SSFN_PREC,h << SSFN_PREC, ctx.lx,ctx.ly, ((a-ctx.lx)/2)+ctx.lx,
                                    ((A-ctx.ly)/2)+ctx.ly, ((k-a)/2)+a,((m-A)/2)+A, k,m, 0);
                            break;
                            case SSFN_CONTOUR_CUBIC:
                                a = (cont->c1x << SSFN_PREC) * h / ctx.height;
                                A = (cont->c1y << SSFN_PREC) * h / ctx.height;
                                b = (cont->c2x << SSFN_PREC) * h / ctx.height;
                                B = (cont->c2y << SSFN_PREC) * h / ctx.height;
                                _sfn_b(p << SSFN_PREC,h << SSFN_PREC, ctx.lx,ctx.ly, a,A, b,B, k,m, 0);
                            break;
                        }
                }
                if(ctx.mx != ctx.lx || ctx.my != ctx.ly) _sfn_l(p << SSFN_PREC, h << SSFN_PREC, ctx.mx, ctx.my);
                if(ctx.np > 4) {
                    for(b = A = B = o = 0; b < h; b++, B += p) {
                        a = b << SSFN_PREC;
                        for(nr = 0, i = 0; i < ctx.np - 3; i += 2) {
                            if( (ctx.p[i+1] < a && ctx.p[i+3] >= a) ||
                                (ctx.p[i+3] < a && ctx.p[i+1] >= a)) {
                                    if((ctx.p[i+1] >> SSFN_PREC) == (ctx.p[i+3] >> SSFN_PREC))
                                        x = (((int)ctx.p[i]+(int)ctx.p[i+2])>>1);
                                    else
                                        x = ((int)ctx.p[i]) + ((a - (int)ctx.p[i+1])*
                                            ((int)ctx.p[i+2] - (int)ctx.p[i])/
                                            ((int)ctx.p[i+3] - (int)ctx.p[i+1]));
                                    x >>= SSFN_PREC;
                                    if(ci) x += (h - b) / SSFN_ITALIC_DIV;
                                    if(cb && !o) {
                                        if(g->data[B + x] == 0xFF) { o = -cb; A = cb; }
                                        else { o = cb; A = -cb; }
                                    }
                                    for(k = 0; k < nr && x > r[k]; k++);
                                    for(l = nr; l > k; l--) r[l] = r[l-1];
                                    r[k] = x;
                                    nr++;
                            }
                        }
                        if(nr > 1 && nr & 1) { r[nr - 2] = r[nr - 1]; nr--; }
                        if(nr) {
                            for(i = 0; i < nr - 1; i += 2) {
                                l = r[i] + o; m = r[i + 1] + A;
                                if(l < 0) l = 0;
                                if(m > p) m = p;
                                if(i > 0 && l < r[i - 1] + A) l = r[i - 1] + A;
                                for(; l < m; l++)
                                    g->data[B + l] = g->data[B + l] == ctx.glyphs[unicode].layers[n].color ?
                                        0xFF : ctx.glyphs[unicode].layers[n].color;
                            }
                        }
                    }
                }
            break;
            case SSFN_FRAG_BITMAP:
            case SSFN_FRAG_PIXMAP:
                B = ctx.glyphs[unicode].width; A = ctx.glyphs[unicode].height;
                b = B * h / ctx.height; a = A * h / ctx.height;
                for(j = 0; j < a; j++) {
                    k = j * A / a * B;
                    for(i = 0; i < b; i++) {
                        l = ctx.glyphs[unicode].layers[n].data[k + i * B / b];
                        if(l != 0xFF)
                            g->data[j * p + i] =
                                ctx.glyphs[unicode].layers[n].type == SSFN_FRAG_BITMAP ? ctx.glyphs[unicode].layers[n].color : l;
                    }
                }
                if(postproc && ctx.glyphs[unicode].layers[n].type == SSFN_FRAG_BITMAP) {
                    B = ctx.glyphs[unicode].layers[n].color;
                    m = B == 0xFD ? 0xFC : 0xFD;
                    for(k = h; k > ctx.height + 4; k -= 2*ctx.height) {
                        for(j = 1; j < a - 1; j++)
                            for(i = 1; i < b - 1; i++) {
                                l = j * p + i;
                                if(g->data[l] == 0xFF && (g->data[l - p] == B ||
                                    g->data[l + p] == B) && (g->data[l - 1] == B ||
                                    g->data[l + 1] == B)) g->data[l] = m;
                            }
                        for(j = 1; j < a - 1; j++)
                            for(i = 1; i < b - 1; i++) {
                                l = j * p + i;
                                if(g->data[l] == m) g->data[l] = B;
                            }
                    }
                }
            break;
        }
    }
    return 1;
}

/**
 * Convert vector layers into bitmaps of size SIZE
 *
 * @param size size in pixels to rasterize to
 */
void sfn_rasterize(int size)
{
    uint32_t P;
    unsigned long int sA;
    int i, j, k, m, n, w, x, y, y0, y1, Y0, Y1, x0, x1, X0, X1, X2, xs, ys, yp, pc, nc = 0, numchars = 0;
    sfngc_t g;

    if(size < 8) size = 8;
    if(size > 255) size = 255;
    if(ctx.height < 1) ctx.height = 1;

    for(i = 0; i < 0x110000; i++)
        if(ctx.glyphs[i].numlayer) numchars++;

    for(i = 0; i < 0x110000; i++) {
        /* we must rescale characters without glyphs too, like white spaces */
        ctx.glyphs[i].adv_x = ctx.glyphs[i].adv_x * size / ctx.height;
        ctx.glyphs[i].adv_y = ctx.glyphs[i].adv_y * size / ctx.height;
        ctx.glyphs[i].ovl_x = ctx.glyphs[i].ovl_x * size / ctx.height;
        if(!ctx.glyphs[i].numlayer) {
            ctx.glyphs[i].width = ctx.glyphs[i].width * size / ctx.height;
            ctx.glyphs[i].height = ctx.glyphs[i].height * size / ctx.height;
            continue;
        }
        if(pbar) (*pbar)(0, 0, ++nc, numchars, PBAR_RASTERIZE);
        n = sfn_glyph(size, i, -1, 0, &g);
        for(j = 0; j < ctx.glyphs[i].numlayer; j++)
            if(ctx.glyphs[i].layers[j].data)
                free(ctx.glyphs[i].layers[j].data);
        for(j = 0; j < ctx.glyphs[i].numkern; j++) {
            ctx.glyphs[i].kern[j].x = ctx.glyphs[i].kern[j].x * size / ctx.height;
            ctx.glyphs[i].kern[j].y = ctx.glyphs[i].kern[j].y * size / ctx.height;
        }
        ctx.glyphs[i].numlayer = ctx.glyphs[i].width = ctx.glyphs[i].height = 0;
        if(n) {
            w = g.p * size / g.h;
            n = size > 16 ? 2 : 1;
            if(w < n) w = n;
            ctx.glyphs[i].layers = realloc(ctx.glyphs[i].layers, sizeof(sfnlayer_t));
            if(!ctx.glyphs[i].layers) continue;
            memset(ctx.glyphs[i].layers, 0, sizeof(sfnlayer_t));
            ctx.glyphs[i].layers[0].data = malloc(w * size);
            if(!ctx.glyphs[i].layers[0].data) { sfn_chardel(i); continue; }
            ctx.glyphs[i].numlayer = 1;
            ctx.glyphs[i].width = w;
            ctx.glyphs[i].height = size;
            ctx.glyphs[i].layers[0].type = SSFN_FRAG_BITMAP;
            ctx.glyphs[i].layers[0].len = w * size;
            ctx.glyphs[i].layers[0].color = 0xFE;
            for (y = j = 0; y < size; y++) {
                y0 = (y << 8) * g.h / size; Y0 = y0 >> 8; y1 = ((y + 1) << 8) * g.h / size; Y1 = y1 >> 8;
                for (x = 0; x < w; x++, j++) {
                    m = 0; sA = 0;
                    x0 = (x << 8) * g.p / w; X0 = x0 >> 8; x1 = ((x + 1) << 8) * g.p / w; X1 = x1 >> 8;
                    for(ys = y0; ys < y1; ys += 256) {
                        if(ys >> 8 == Y0) { yp = 256 - (ys & 0xFF); ys &= ~0xFF; if(yp > y1 - y0) yp = y1 - y0; }
                        else if(ys >> 8 == Y1) yp = y1 & 0xFF;
                        else yp = 256;
                        X2 = (ys >> 8) * g.p;
                        for(xs = x0; xs < x1; xs += 256) {
                            if (xs >> 8 == X0) {
                                k = 256 - (xs & 0xFF); xs &= ~0xFF; if(k > x1 - x0) k = x1 - x0;
                                pc = k == 256 ? yp : (k * yp) >> 8;
                            } else
                            if (xs >> 8 == X1) { k = x1 & 0xFF; pc = k == 256 ? yp : (k * yp) >> 8; }
                            else pc = yp;
                            m += pc;
                            k = g.data[X2 + (xs >> 8)];
                            if(k == 0xFF) continue;
                            P = *((uint32_t*)(ctx.cpal + (k << 2)));
                            sA += (k == 0xFE || !P ? 255 : ((P >> 24) & 0xFF)) * pc;
                        }
                    }
                    if(m) sA /= m; else sA >>= 8;
                    ctx.glyphs[i].layers[0].data[j] = sA > 64 ? 0xFE : 0xFF;
                }
            }
        }
    }
    ctx.width = ctx.height = ctx.baseline = ctx.underline = 0;
    sfn_sanitize(-1);
}

int min_y, max_y, min_x, max_x, max_b, max_s, max_w, max_h, unicode;
sfnlayer_t *currlayer;

/**
 * Parse FontForge's SplineFontDb
 */
void sfd(char *ptr, int size)
{
    int i, j, w = -1, h = 0, b = 0, unicode = 0, nc = 0, numchars = 0, ps = 0, p[7], mx, xx, my, xy, f = 0;
    long int avg_w = 0, avg_h = 0, avg_n = 0;
    char *end = ptr + size, *endprop, *face, *name = NULL;
#define scx(x) (((x - mx) * 255 + max_s / 2) / max_s)
#define scy(y) (((max_b - y) * 255 + max_s / 2) / max_s)

    for(face = ptr; face + 11 < end && *face; face++) {
        if(!memcmp(face, "StartChar:", 10)) numchars++;
        if(!memcmp(face, "BitmapFont:", 11)) {
            /* Ooops, we have a bitmap font */
            bdf(face, end - face);
            return;
        }
    }
    face = NULL;

    while(ptr < end && *ptr) {
        if(!memcmp(ptr, "FullName: ", 10) && !face) { ptr += 10; face = ptr; }
        if(!memcmp(ptr, "Version: ", 9) && !ctx.revision) { ptr += 9; sfn_setstr(&ctx.revision, ptr, 0); }
        if(!memcmp(ptr, "FontName: ", 10) && !ctx.name && !ctx.subname) {
            ptr += 10; while(*ptr && *ptr!='-' && *ptr!='\n') { ptr++; }
            if(*ptr == '-') { ptr++; sfn_setstr(&ctx.subname, ptr, 0); }
        }
        if(!memcmp(ptr, "FamilyName: ", 12) && !ctx.manufacturer) { ptr += 12; sfn_setstr(&ctx.manufacturer, ptr, 0); }
        if(!memcmp(ptr, "FamilyName: ", 12) && !ctx.familyname) { ptr += 12; sfn_setstr(&ctx.familyname, ptr, 0); }
        if(!memcmp(ptr, "Copyright: ", 11) && !ctx.license) { ptr += 11; sfn_setstr(&ctx.license, ptr, 0); }
        if(!memcmp(ptr, "Ascent: ", 8)) {
            ptr += 8; while(*ptr == ' ') ptr++;
            b = atoi(ptr); }
        if(!memcmp(ptr, "Descent: ", 9)) {
            ptr += 9; while(*ptr == ' ') ptr++;
            ps = atoi(ptr); }
        if(!memcmp(ptr, "UnderlinePosition: ", 19)) {
            ptr += 19; while(*ptr == ' ') ptr++;
            relul = atoi(ptr); if(relul < 0) relul = -relul; }
        if(!memcmp(ptr, "ItalicAngle: ", 13)) {
            ptr += 13; while(*ptr == ' ') ptr++;
            if(ptr[0] != '0') ctx.style |= SSFN_STYLE_ITALIC; }
        if(!memcmp(ptr, "Weight: Bold", 12)) ctx.style |= SSFN_STYLE_BOLD;
        if(!memcmp(ptr, "BeginChars:", 11)) break;
        while(*ptr && *ptr!='\n') ptr++;
        while(*ptr=='\n') ptr++;
    }
    ps += b;
    if(!ctx.name) {
        if(!face) face = ctx.familyname;
        if(face && ctx.subname && ctx.subname[0]) {
            for(i = 0; face[i] && face[i] != '\"' && face[i] != '\r' && face[i] != '\n'; i++);
            j = strlen(ctx.subname);
            name = malloc(i + j + 2);
            if(!name) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
            memcpy(name, face, i);
            name[i] = ' ';
            memcpy(name + i + 1, ctx.subname, j);
            name[i + j + 1] = 0;
            sfn_setstr(&ctx.name, name, 0);
            free(name);
        } else
            sfn_setstr(&ctx.name, ctx.familyname, 0);
    }
    if(verbose > 1) printf("\r  Name '%s' num_glyphs: %d, ascender: %d, underline: %d, height: %d\n", ctx.name, numchars, b, b + relul, ps);
    if(!ps) return;
    min_x = min_y = 2*ps; max_x = max_y = max_b = max_w = max_h = -2*ps; numchars = 0;
    endprop = ptr;
    while(ptr < end && *ptr) {
        if(!memcmp(ptr, "Encoding: ", 10)) {
            ptr += 10; while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
            unicode = atoi(ptr); while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
            if(*ptr == '0' && (ptr[1] == '\r' || ptr[1] == '\n')) unicode = 0;
            f = 0;
        }
        if(!memcmp(ptr, "Fore", 4))  f = 1;
        if(!memcmp(ptr, "Back", 4))  f = 0;
        if(f && !memcmp(ptr, "SplineSet", 9)) {
            ptr += 9; while(*ptr && *ptr!='\n') { ptr++; } while(*ptr=='\n') ptr++;
            for(j = 0; j < ctx.numskip && ctx.skip[j] != unicode; j++);
            if(unicode >= 0 && unicode >= rs && unicode <= re && j >= ctx.numskip) {
                mx = my = 2*ps; xx = xy = -2*ps; numchars++;
                while(ptr < end && *ptr && memcmp(ptr, "EndSplineSet", 12)) {
                    for(j = 0; j < 7; j++) {
                        while(*ptr == ' ') ptr++;
                        if(!memcmp(ptr, "Spiro", 5)) while(ptr < end && *ptr && memcmp(ptr - 9, "EndSpiro", 8)) ptr++;
                        if(j >=6 || (*ptr != ' ' && *ptr != '-' && !(*ptr >= '0' && *ptr <= '9'))) break;
                        p[j] = atoi(ptr); while(*ptr && *ptr != ' ') ptr++;
                        if(j & 1) {
                            if(p[j] > max_b) max_b = p[j];
                            if(p[j] < min_y) min_y = p[j];
                            if(p[j] > max_y) max_y = p[j];
                            if(p[j] < my) my = p[j];
                            if(p[j] > xy) xy = p[j];
                        } else {
                            if(p[j] < min_x) min_x = p[j];
                            if(p[j] > max_x) max_x = p[j];
                            if(p[j] < mx) mx = p[j];
                            if(p[j] > xx) xx = p[j];
                        }
                    }
                    if(ptr[0] != 'm' && ptr[0] != 'l' && ptr[0] != 'c') {
                        if(verbose > 1)
                            fprintf(stderr, "\rlibsfn: bad font, unknown spline command '%c' for U+%06X    \n", ptr[0], unicode);
                        return;
                    }
                    while(*ptr && *ptr!='\n') ptr++;
                    while(*ptr=='\n') ptr++;
                }
                if(mx < xx && my < xy) {
                    xx -= mx - 1; xy -= my - 1;
                    if(xx > max_w) max_w = xx;
                    if(xy > max_h) max_h = xy;
                    avg_w += xx;
                    avg_h += xy;
                    avg_n++;
                }
            }
            while(ptr < end && *ptr && memcmp(ptr, "EndChar", 7)) ptr++;
        }
        while(*ptr && *ptr!='\n') ptr++;
        while(*ptr=='\n') ptr++;
    }
    if(avg_n > 0) {
        avg_w /= avg_n;
        avg_h /= avg_n;
    }
    if(verbose && b != max_b) {
        fprintf(stderr, "\rlibsfn: inconsistent font: ascender %d != max(yMax) %d    \n", b, max_b);
    }
    if(verbose > 1) printf("\r  Numchars: %d, Bounding box: (%d, %d), (%d, %d) dx %d dy %d, w: %d, h: %d, baseline: %d\n", numchars,
        min_x, min_y, max_x, max_y, max_x - min_x, max_y - min_y, max_w, max_h, max_b);
    max_s = max_w > max_h ? max_w : max_h;
    if(max_s > 0) {
        if(verbose > 1) printf("  Scaling to %d x %d, average: %ld x %ld\n", max_s, max_s, avg_w, avg_h);
        ptr = endprop;
        while(ptr < end && *ptr) {
            if(!memcmp(ptr, "Encoding: ", 10)) {
                ptr += 10; while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
                unicode = atoi(ptr); while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
                if(*ptr == '0' && (ptr[1] == '\r' || ptr[1] == '\n')) unicode = 0;
                w = -1; h = f = 0;
            }
            if(!memcmp(ptr, "Fore", 4))  f = 1;
            if(!memcmp(ptr, "Back", 4))  f = 0;
            if(!memcmp(ptr, "Width: ", 7)) { ptr += 7; w = atoi(ptr);  }
            if(!memcmp(ptr, "VWidth: ", 8)) { ptr += 8; h = atoi(ptr);  }
            if(!memcmp(ptr, "EndChar", 7) && iswhitespace(unicode) && (w || h))
                sfn_charadd(unicode, 0, 0, w, h, 0);
            if(f && !memcmp(ptr, "SplineSet", 9)) {
                ptr += 9; while(*ptr && *ptr!='\n') { ptr++; } while(*ptr=='\n') ptr++;
                for(j = 0; j < ctx.numskip && ctx.skip[j] != unicode; j++);
                if(unicode >= 0 && unicode >= rs && unicode <= re && j >= ctx.numskip) {
                    if(pbar) (*pbar)(0, 0, ++nc, numchars, PBAR_OUTLINE);
                    mx = my = 2*ps; xx = xy = -2*ps; endprop = ptr;
                    /* sfd does not store the glyph metrics */
                    while(ptr < end && *ptr && memcmp(ptr, "EndSplineSet", 12)) {
                        for(j = 0; j < 7; j++) {
                            while(*ptr == ' ') ptr++;
                            if(!memcmp(ptr, "Spiro", 5)) while(ptr < end && *ptr && memcmp(ptr - 9, "EndSpiro", 8)) ptr++;
                            if(j >=6 || (*ptr != ' ' && *ptr != '-' && !(*ptr >= '0' && *ptr <= '9'))) break;
                            p[j] = atoi(ptr); while(*ptr && *ptr != ' ') ptr++;
                            if(j & 1) {
                                if(p[j] < my) my = p[j];
                                if(p[j] > xy) xy = p[j];
                            } else {
                                if(p[j] < mx) mx = p[j];
                                if(p[j] > xx) xx = p[j];
                            }
                        }
                        while(*ptr && *ptr!='\n') ptr++;
                        while(*ptr=='\n') ptr++;
                    }
                    if(mx < xx && my < xy) {
                        xx -= mx - 1; xy -= my - 1;
                        if(verbose && (xx >= 2*avg_w+avg_w/2 || xy >= 2*avg_h+avg_h/2)) {
                            fprintf(stderr, "\rlibsfn: irregular dimensions in font: U+%06X", unicode);
                            if(xx >= 2*avg_w+avg_w/2) {
                                fprintf(stderr, ", width: %d ", xx);
                                if(xx >= 3*avg_w)
                                    fprintf(stderr, "(%ld times the average, can't be right!!!)", xx / avg_w);
                            }
                            if(xy >= 2*avg_h+avg_h/2) {
                                fprintf(stderr, ", height: %d ", xy);
                                if(xy >= 3*avg_h)
                                    fprintf(stderr, "(%ld times the average, can't be right!!!)", xy / avg_h);
                            }
                            fprintf(stderr, "\n");
                        }
                        /* now that we know the actual dimensions of the glyph, go once again... */
                        if(h > 0) w = 0;
                        if(w < 0) w = xx > 0 ? xx : 0;
                        if(sfn_charadd(unicode, 0, 0, w, h, !h && mx < 0 ? -mx * 255 / max_s : 0)) {
                            ptr = endprop;
                            while(ptr < end && *ptr && memcmp(ptr, "EndSplineSet", 12)) {
                                for(j = 0; j < 7; j++) {
                                    while(*ptr == ' ') ptr++;
                                    if(!memcmp(ptr, "Spiro", 5)) while(ptr < end && *ptr && memcmp(ptr - 9, "EndSpiro", 8)) ptr++;
                                    if(j >=6 || (*ptr != ' ' && *ptr != '-' && !(*ptr >= '0' && *ptr <= '9'))) break;
                                    p[j] = atoi(ptr); while(*ptr && *ptr != ' ') ptr++;
                                }
                                switch(ptr[0]) {
                                    case 'm':
                                        currlayer = sfn_layeradd(unicode, SSFN_FRAG_CONTOUR, 0, 0, 0, 0, 0xFE, NULL);
                                        sfn_contadd(currlayer, SSFN_CONTOUR_MOVE, scx(p[0]), scy(p[1]), 0,0, 0,0);
                                    break;
                                    case 'l': sfn_contadd(currlayer, SSFN_CONTOUR_LINE, scx(p[0]), scy(p[1]), 0,0, 0,0); break;
                                    case 'c':
                                        sfn_contadd(currlayer, SSFN_CONTOUR_CUBIC, scx(p[4]), scy(p[5]), scx(p[0]), scy(p[1]),
                                            scx(p[2]),scy(p[3]));
                                    break;
                                }
                                while(*ptr && *ptr!='\n') ptr++;
                                while(*ptr=='\n') ptr++;
                            }
                        }
                    }
                }
                while(ptr < end && *ptr && memcmp(ptr, "EndChar", 7)) ptr++;
            }
            while(*ptr && *ptr!='\n') ptr++;
            while(*ptr=='\n') ptr++;
        }
    } /* max_s != 0 */
    if(!ctx.baseline && max_b) ctx.baseline = max_b;
}

/**
 * Transform and scale coordinates
 */
int sx(int i)
{
    return ((i - bbox->xMin) * 255 + max_s / 2) / max_s;
}

int sy(int i)
{
    return ((max_b - i) * 255 + max_s / 2) / max_s;
}

/*** contour command callbacks for FT2 ***/

int move(const FT_Vector *to, void *ptr)
{
    (void)ptr;
    currlayer = sfn_layeradd(unicode, SSFN_FRAG_CONTOUR, 0, 0, 0, 0, 0xFE, NULL);
    sfn_contadd(currlayer, SSFN_CONTOUR_MOVE, sx(to->x), sy(to->y), 0,0, 0,0);
    return 0;
}

int line(const FT_Vector *to, void *ptr)
{
    (void)ptr;
    sfn_contadd(currlayer, SSFN_CONTOUR_LINE, sx(to->x), sy(to->y), 0,0, 0,0);
    return 0;
}

int quad(const FT_Vector *control, const FT_Vector *to, void *ptr)
{
    (void)ptr;
    sfn_contadd(currlayer, SSFN_CONTOUR_QUAD, sx(to->x), sy(to->y), sx(control->x), sy(control->y), 0,0);
    return 0;
}

int cubic(const FT_Vector *control1, const FT_Vector *control2, const FT_Vector *to, void *ptr)
{
    (void)ptr;
    sfn_contadd(currlayer, SSFN_CONTOUR_CUBIC, sx(to->x), sy(to->y), sx(control1->x), sy(control1->y),
        sx(control2->x), sy(control2->y));
    return 0;
}

FT_Outline_Funcs funcs = { move, line, quad, cubic, 0, 0 };

/**
 * Parse a TTF (or any other vector font that FT2 supports)
 */
void ttf()
{
    FT_UInt agi;
    FT_ULong cp;
    FT_Vector v;
    FT_Error error;
    int i, j, n, x, y, numchars = 0;
    long int avg_w = 0, avg_h = 0, avg_n = 0;
    ftchr_t *c;

    /* unbeliveable, FT2 lib does not return the number of characters in the font... */
    for(cp = FT_Get_First_Char(face, &agi); agi; cp = FT_Get_Next_Char(face, cp, &agi)) numchars++;

    c = ftchars = (ftchr_t*)malloc((numchars + 1) * sizeof(ftchr_t));
    if(!ftchars) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
    memset(ftchars, 0, (numchars + 1) * sizeof(ftchr_t));

    /* ...and it returns incorrect bounding box. Never trust FT2's scaling. */
    min_x = min_y = face->units_per_EM*2; max_x = max_y = max_b = max_w = max_h = -face->units_per_EM*2;
    for(cp = FT_Get_First_Char(face, &agi), i = 0; i <= numchars && agi; i++) {
        if(pbar) (*pbar)(1, 3, i, numchars, PBAR_MEASURE);
        for(j = 0; j < ctx.numskip && ctx.skip[j] != (int)cp; j++);
        if(j >= ctx.numskip) {
            c->unicode = (int)cp;
            c->bbox.xMin = c->bbox.xMax = c->bbox.yMin = c->bbox.yMax = 0;
            if(FT_Load_Glyph(face, FT_Get_Char_Index(face, cp), FT_LOAD_NO_SCALE) ||
                (i && !face->glyph->glyph_index)) continue;
            if(!origwh) {
                FT_Outline_Get_BBox(&face->glyph->outline, &c->bbox);
            } else {
                if(!face->glyph->metrics.vertBearingX && !face->glyph->metrics.vertBearingY) {
                    c->bbox.xMin = face->glyph->metrics.horiBearingX;
                    c->bbox.xMax = face->glyph->metrics.horiBearingX + face->glyph->metrics.width;
                    c->bbox.yMin = face->glyph->metrics.horiBearingY - face->glyph->metrics.height;
                    c->bbox.yMax = face->glyph->metrics.horiBearingY;
                } else {
                    c->bbox.xMin = face->glyph->metrics.vertBearingX;
                    c->bbox.xMax = face->glyph->metrics.vertBearingX + face->glyph->metrics.width;
                    c->bbox.yMin = face->glyph->metrics.vertBearingY - face->glyph->metrics.height;
                    c->bbox.yMax = face->glyph->metrics.vertBearingY;
                }
            }
            if(face->glyph->metrics.width > max_w) max_w = face->glyph->metrics.width;
            if(face->glyph->metrics.height > max_h) max_h = face->glyph->metrics.height;
            if(c->bbox.yMax > max_b) max_b = c->bbox.yMax;
            if(c->bbox.xMin < min_x) min_x = c->bbox.xMin;
            if(c->bbox.xMax > max_x) max_x = c->bbox.xMax;
            if(c->bbox.yMin < min_y) min_y = c->bbox.yMin;
            if(c->bbox.yMax > max_y) max_y = c->bbox.yMax;
            avg_w += face->glyph->metrics.width;
            avg_h += face->glyph->metrics.height;
            avg_n++;
/*
if(cp==0x21) {
    printf("\n%ld UUUUU Bounding box (%ld, %ld), (%ld, %ld)\n", cp,c->bbox.xMin, c->bbox.yMin, c->bbox.xMax, c->bbox.yMax);
    printf("metrics w %ld h %ld horiAdv %ld vertAdv %ld hbear %ld %ld vbear %ld %ld\n", face->glyph->metrics.width, face->glyph->metrics.height,
        face->glyph->metrics.horiAdvance, face->glyph->metrics.vertAdvance, face->glyph->metrics.horiBearingX, face->glyph->metrics.horiBearingY,
        face->glyph->metrics.vertBearingX, face->glyph->metrics.vertBearingY);
}
*/
            if(verbose && (c->bbox.xMin != face->glyph->metrics.horiBearingX ||
                c->bbox.yMin != face->glyph->metrics.horiBearingY - face->glyph->metrics.height ||
                c->bbox.xMax != c->bbox.xMin + face->glyph->metrics.width ||
                c->bbox.yMax != c->bbox.yMin + face->glyph->metrics.height)) {
                    fprintf(stderr, "\rlibsfn: inconsistent font: U+%06lX", cp);
                    if(c->bbox.xMin != face->glyph->metrics.horiBearingX)
                        fprintf(stderr, ", xMin %ld != hBX %ld", c->bbox.xMin, face->glyph->metrics.horiBearingX);
                    if(c->bbox.yMin != face->glyph->metrics.horiBearingY - face->glyph->metrics.height)
                        fprintf(stderr, ", yMin %ld != hBY-h %ld", c->bbox.yMin,
                            face->glyph->metrics.horiBearingY - face->glyph->metrics.height);
                    if(c->bbox.xMax != c->bbox.xMin + face->glyph->metrics.width)
                        fprintf(stderr, ", xMax %ld != xMin+w %ld", c->bbox.xMax,
                            c->bbox.xMin + face->glyph->metrics.width);
                    if(c->bbox.yMax != c->bbox.yMin + face->glyph->metrics.height)
                        fprintf(stderr, ", yMax %ld != yMin+h %ld", c->bbox.yMax,
                            c->bbox.yMin + face->glyph->metrics.height);
                    fprintf(stderr, "    \n");
            }
            c++;
        }
        cp = FT_Get_Next_Char(face, cp, &agi);
    }
    if(avg_n > 0) {
        avg_w /= avg_n;
        avg_h /= avg_n;
    }
    /* due to optionally skipped characters */
    numchars = (int)((unsigned long int)c - (unsigned long int)ftchars) / sizeof(ftchr_t);
    if(min_x >= max_x || min_y >= max_y) min_x = max_x = min_y = max_y = max_w = max_h = max_b = 0;

    if(verbose && face->ascender != max_b) {
        fprintf(stderr, "\rlibsfn: inconsistent font: ascender %d != max(yMax) %d    \n", face->ascender, max_b);
    }
    if(origwh) {
        if(max_x - min_x > max_w) max_w = max_x - min_x;
        if(max_y - min_y > max_h) max_h = max_y - min_y;
    }
    if(verbose > 1) printf("\r  Numchars: %d, Bounding box: (%d, %d), (%d, %d) dx %d dy %d, w: %d, h: %d, baseline: %d\n", numchars,
        min_x, min_y, max_x, max_y, max_x - min_x, max_y - min_y, max_w, max_h, max_b);
    max_s = max_w > max_h ? max_w : max_h;
    if(max_s > 0) {
        if(verbose > 1) printf("  Scaling to %d x %d, average: %ld x %ld\n", max_s, max_s, avg_w, avg_h);

        /* get font style */
        if(face->style_flags & FT_STYLE_FLAG_BOLD) ctx.style |= SSFN_STYLE_BOLD;
        if(face->style_flags & FT_STYLE_FLAG_ITALIC) ctx.style |= SSFN_STYLE_ITALIC;

        /* baseline and underline */
        ctx.baseline = (max_b * 255 + max_s - 1) / max_s;
        ctx.underline = ((max_b - face->underline_position) * 255 + max_s - 1) / max_s;

        /* iterate on characters and contour outlines */
        lastuni = -1;
        for(i = 0; i < numchars; i++) {
            unicode = ftchars[i].unicode;
            if(pbar) (*pbar)(2, 3, i, numchars, PBAR_OUTLINE);
            if(FT_Load_Glyph(face, FT_Get_Char_Index(face, unicode), FT_LOAD_NO_SCALE) ||
                (i && !face->glyph->glyph_index)) continue;
            bbox = &ftchars[i].bbox;
            if(face->glyph->metrics.horiAdvance /* !face->glyph->metrics.vertBearingX && !face->glyph->metrics.vertBearingY*/) {
                x = face->glyph->metrics.horiAdvance - bbox->xMin + 1; y = 0;
                if(x < 0) x = 1;
                x = (x * 255 + max_s - 1) / max_s;
            } else {
                x = 0; y = face->glyph->metrics.vertAdvance - bbox->yMin + 1;
                if(y < 0) y = 1;
                y = (y * 255 + max_s - 1) / max_s;
            }
            if(verbose && (face->glyph->metrics.width >= 2*avg_w+avg_w/2 || face->glyph->metrics.height >= 2*avg_h+avg_h/2)) {
                fprintf(stderr, "\rlibsfn: irregular dimensions in font: U+%06X", unicode);
                if(face->glyph->metrics.width >= 2*avg_w+avg_w/2) {
                    fprintf(stderr, ", width: %ld ", face->glyph->metrics.width);
                    if(face->glyph->metrics.width >= 3*avg_w)
                        fprintf(stderr, "(%ld times the average, can't be right!!!)", face->glyph->metrics.width / avg_w);
                }
                if(face->glyph->metrics.height >= 2*avg_h+avg_h/2) {
                    fprintf(stderr, ", height: %ld ", face->glyph->metrics.height);
                    if(face->glyph->metrics.height >= 3*avg_h)
                        fprintf(stderr, "(%ld times the average, can't be right!!!)", face->glyph->metrics.height / avg_h);
                }
                fprintf(stderr, "\n");
            }
            /* don't trust FT2's width and height, we'll recalculate them from the coordinates */
            if(sfn_charadd(unicode, 0, 0, x, y, !y && bbox->xMin < 0 ? (-bbox->xMin * 255 + max_s - 1) / max_s : 0)) {
                error = FT_Outline_Decompose(&face->glyph->outline, &funcs, NULL);
                if(error && verbose) { fprintf(stderr, "libsfn: FreeType2 decompose error %x\n", error); }
            }
        }

        /* get kerning information */
        if(face->face_flags & FT_FACE_FLAG_KERNING) {
            for(i = n = 0; i < numchars; i++) {
                if(pbar) (*pbar)(3, 3, i, numchars, PBAR_GETKERN);
                for(j = 0; j < numchars; j++) {
                    if(ftchars[i].unicode<33 || ftchars[j].unicode<33) continue;
                    v.x = v.y = 0;
                    FT_Get_Kerning(face, ftchars[i].unicode, ftchars[j].unicode, FT_KERNING_UNSCALED, &v);
                    if(v.x) { v.x = (v.x * 255 + max_s - 1) / max_s; v.y = 0; }
                    else { v.x = 0; v.y = (v.y * 255 + max_s - 1) / max_s; }
                    if(v.x >= -1 && v.x <= 1 && v.y >= -1 && v.x <= 1) continue;
                    sfn_kernadd(ftchars[i].unicode, ftchars[j].unicode, v.x, v.y);
                    n++;
                }
            }
            if(verbose > 1) printf("\r\x1b[K  Kerning %d pairs\n", n);
        } else
            if(verbose > 1) printf("\r\x1b[K  No kerning information\n");
    }
    free(ftchars);
}

/**
 * Parse bitmap font (any format that FT2 supports)
 * I know, this is not vector font related, but since all the other FT2 functions are here I didn't wanted
 * to move this function with all the includes into bitmap.c
 */
void ft2()
{
    int i, j, k, l, n, a = 0, w = 8, h = 16, nc = 0, numchars = 0, ft2bug = 0;
    unsigned char *glyph, *bitmap;
    FT_UInt agi;
    FT_ULong cp;

    /* unbeliveable, FT2 lib can't iterate on characters in the font if there's no encoding... */
    for(cp = FT_Get_First_Char(face, &agi); agi; cp = FT_Get_Next_Char(face, cp, &agi)) numchars++;

    for(i = 0; i < face->num_fixed_sizes; i++)
        if(face->available_sizes[i].height > h) {
            h = face->available_sizes[i].height;
            w = face->available_sizes[i].width;
        }
    if(verbose > 1) printf("\r  Numchars: %d, num_fixed_sizes: %d, selected: %d x %d\n", numchars, face->num_fixed_sizes, w, h);
    if(!numchars) {
        if(verbose > 1) printf("  FreeType2 bug, FT_Get_Next_char didn't work, trying to render ALL code points one-by-one...\n");
        ft2bug = 1; numchars = 0x10FFFF;
    }
    FT_Set_Pixel_Sizes(face, w, h);
    n = 2*h*h;
    bitmap = (unsigned char*)malloc(n);
    if(!bitmap) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }

    if(ft2bug) { cp = 0; agi = 1; } else cp = FT_Get_First_Char(face, &agi);
    while(agi) {
        if(pbar) (*pbar)(0, 0, ++nc, numchars, PBAR_BITMAP);
        for(j = 0; j < ctx.numskip && ctx.skip[j] != (int)cp; j++);
        if(j < ctx.numskip) goto cont;
        if(cp < (FT_ULong)rs || cp > (FT_ULong)re || FT_Load_Glyph(face, FT_Get_Char_Index(face, cp), FT_LOAD_NO_SCALE) ||
            (cp && !face->glyph->glyph_index)) goto cont;
        FT_Render_Glyph(face->glyph, FT_RENDER_MODE_MONO);
        if(face->glyph->bitmap.pixel_mode != FT_PIXEL_MODE_MONO) goto cont;
        memset(bitmap, 0, n);
        glyph = face->glyph->bitmap.buffer;
        w = ((int)(face->glyph->bitmap.width+7) & ~7);
        for(i = k = 0; i < (int)face->glyph->bitmap.rows; i++) {
            for(j = 0; j < (int)(face->glyph->bitmap.width+7)/8; j++)
                for(l=0x80;l && k < n;l>>=1)
                    bitmap[k++] = (glyph[j] & l) ? 0xFE : 0xFF;
            glyph += face->glyph->bitmap.pitch;
        }
        a += face->glyph->bitmap_top;
        if(sfn_charadd(cp, w, face->glyph->bitmap.rows, face->glyph->advance.x >> 6, face->glyph->advance.y >> 6,
            face->glyph->bitmap_left))
                sfn_layeradd(cp, SSFN_FRAG_BITMAP, 0, 0, w, face->glyph->bitmap.rows, 0xFE, bitmap);
cont:   if(ft2bug) { cp++; if(cp > 0x10FFFF) break; } else cp = FT_Get_Next_Char(face, cp, &agi);
    }

    ctx.baseline = numchars ? a / numchars : a;
    ctx.underline = ctx.baseline + (h - ctx.baseline) / 2;

    free(bitmap);
}

/**
 * Check if FT2 can read the font
 */
int ft2_read(unsigned char *data, int size)
{
    if(!ft && FT_Init_FreeType(&ft)) { if(verbose > 1) { fprintf(stderr, "libsfn: unable to initialize FreeType2\n"); } return 0; }
    face = NULL;
    return (!FT_New_Memory_Face(ft, data, size, 0, &face) && face) ? 1 : 0;
}

/**
 * Get a string from ft2
 * for the sfnt string ids, see https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=IWS-Chapter08#3054f18b
 */
int ft2_str(int id, FT_SfntName *name)
{
    uint8_t *src, *dst, *end, *old;
    uint16_t chr;
    int i;

    if(!name || !face) return 0;
    /* NOTE: see freetype2/src/base/ftsnames.c line 73, it is a real mine-field:
     * 1. it does not check if the name entry for an idx is valid or not
     * 2. if it happens to have bad values, or otherwise unable to read from the stream, it will still return FT_Err_OK.
     * Also, see freetype2/src/sfnt/ttload.c line 947, it skips empty and bad records, which means FT_Get_Sfnt_Name's idx isn't
     * necessarily aligned with sfnt ids any more, it could be just a total arbitrary ft2 internal id.
     * So we have to do it the hard way...
     */
    for(i = 0; i < (int)FT_Get_Sfnt_Name_Count(face); i++)
        if(!FT_Get_Sfnt_Name(face, i, name) && name->string != NULL && name->string_len > 0 && name->name_id == id) {
            if(!name->encoding_id && name->string[0]) return 1;
            /* TTF isn't really documented, but ft2 seems to think this is UTF16-BE when it gets face->family_name */
            src = old = name->string; end = name->string + name->string_len;
            dst = name->string = (uint8_t*)malloc(name->string_len * 2);
            if(!dst) { name->string = old; return 0; }
            for(; src < end; src += 2) {
                chr = (src[0] << 8) | src[1];
                if(!chr) break;
                if(chr < 0x80) *dst++ = chr;
                else if(chr < 0x800) { *dst++ = ((chr>>6)&0x1F)|0xC0; *dst++ = (chr&0x3F)|0x80; }
                else { *dst++ = ((chr>>12)&0x0F)|0xE0; *dst++ = ((chr>>6)&0x3F)|0x80; *dst++ = (chr&0x3F)|0x80; }
            }
            name->encoding_id = 1;
            name->string_len = (uintptr_t)dst - (uintptr_t)name->string;
            free(old);
            return 1;
        }
    return 0;
}

/**
 * Parse a FT2 font
 */
void ft2_parse()
{
    FT_SfntName name;
    if(!ft || !face) return;

    FT_Select_Charmap(face, FT_ENCODING_UNICODE);
    if(verbose > 1) printf("\r  Name '%s' num_glyphs: %ld, units_per_EM: %d, ascender: %d, underline: %d, %s\n",
        face->family_name, face->num_glyphs, face->units_per_EM, face->ascender, face->underline_position,
        face->face_flags & FT_FACE_FLAG_SCALABLE ? "vector" : "bitmap");

    if(face->face_flags & FT_FACE_FLAG_SCALABLE) ttf(); else ft2();

    /* unique font name */
    if(!ctx.name) {
        name.string = NULL;
        if(ft2_str(4, &name)) sfn_setstr(&ctx.name, (char*)name.string, name.string_len); else
        if(ft2_str(3, &name)) sfn_setstr(&ctx.name, (char*)name.string, name.string_len); else
        if(ft2_str(6, &name)) sfn_setstr(&ctx.name, (char*)name.string, name.string_len); else
        if(ft2_str(20, &name)) sfn_setstr(&ctx.name, (char*)name.string, name.string_len); else
        if(ft2_str(18, &name)) sfn_setstr(&ctx.name, (char*)name.string, name.string_len);
    }
    /* fallback */
    if(!ctx.name) sfn_setstr(&ctx.name, face->family_name, 0);

    /* family name */
    if(!ctx.familyname) {
        name.string = NULL;
        if(ft2_str(1, &name)) sfn_setstr(&ctx.familyname, (char*)name.string, name.string_len); else
        if(ft2_str(16, &name)) sfn_setstr(&ctx.familyname, (char*)name.string, name.string_len);
    }
    /* fallback */
    if(!ctx.familyname) sfn_setstr(&ctx.familyname, face->family_name, 0);

    /* subfamily name */
    if(!ctx.subname) {
        name.string = NULL;
        if(ft2_str(2, &name)) sfn_setstr(&ctx.subname, (char*)name.string, name.string_len); else
        if(ft2_str(17, &name)) sfn_setstr(&ctx.subname, (char*)name.string, name.string_len);
    }
    /* fallback */
    if(!ctx.subname) sfn_setstr(&ctx.subname, face->style_name, 0);

    /* version / revision */
    name.string = NULL;
    if(!ctx.revision && ft2_str(5, &name)) sfn_setstr(&ctx.revision, (char*)name.string, name.string_len);

    /* manufacturer */
    if(!ctx.manufacturer) {
        name.string = NULL;
        if(ft2_str(8, &name)) sfn_setstr(&ctx.manufacturer, (char*)name.string, name.string_len); else
        if(ft2_str(9, &name)) sfn_setstr(&ctx.manufacturer, (char*)name.string, name.string_len);
    }

    /* copyright */
    if(!ctx.license) {
        name.string = NULL;
        if(ft2_str(13, &name)) sfn_setstr(&ctx.license, (char*)name.string, name.string_len); else
        if(ft2_str(0, &name)) sfn_setstr(&ctx.license, (char*)name.string, name.string_len); else
        if(ft2_str(7, &name)) sfn_setstr(&ctx.license, (char*)name.string, name.string_len);
    }

    FT_Done_Face(face); face = NULL;
    FT_Done_FreeType(ft); ft = NULL;
}

typedef struct {
    unsigned int magic;
    unsigned int version;
    unsigned int headersize;
    unsigned int flags;
    unsigned int numglyph;
    unsigned int bytesperglyph;
    unsigned int height;
    unsigned int width;
    unsigned char glyphs;
} __attribute__((packed)) psf_t;

/****************************** file format parsers ******************************/

/**
 * Parse PSF2 font (binary)
 */
void psf(unsigned char *ptr, int size)
{
    psf_t *psf = (psf_t*)ptr;
    uint16_t *utbl = NULL;
    uint32_t c, g=0, unicode, nc = 0, numchars = psf->numglyph;
    unsigned char *s, *e, *glyph, *bitmap;
    int i, j, k, l, n;

    s=(unsigned char*)(ptr + psf->headersize + psf->numglyph*psf->bytesperglyph);
    e=ptr + size;
    /* try to fix bad fonts */
    if(s > e) {
        for(s = e; s + 1 > ptr + psf->headersize && (s[-1] || s[0] == 0xFF); s--);
        psf->numglyph = ((int)(s - ptr) - psf->headersize) / psf->bytesperglyph;
    }
    if(s < e) {
        numchars = 0;
        utbl = (uint16_t*)malloc(0x110000*sizeof(uint16_t));
        if(!utbl) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
        memset(utbl, 0, 0x110000*sizeof(uint16_t));
        while(s<e && g<psf->numglyph) {
            c = (uint16_t)((uint8_t)s[0]);
            if(c == 0xFF) { g++; } else {
                if((c & 128) != 0) {
                    if((c & 32) == 0 ) { c=((s[0] & 0x1F)<<6)+(s[1] & 0x3F); s++; } else
                    if((c & 16) == 0 ) { c=((((s[0] & 0xF)<<6)+(s[1] & 0x3F))<<6)+(s[2] & 0x3F); s+=2; } else
                    if((c & 8) == 0 ) { c=((((((s[0] & 0x7)<<6)+(s[1] & 0x3F))<<6)+(s[2] & 0x3F))<<6)+(s[3] & 0x3F); s+=3;}
                    else c=0;
                }
                if(c<0x110000) {
                    utbl[c] = g;
                    numchars++;
                }
            }
            s++;
        }
    }
    if((psf->flags >> 24) && !ctx.baseline) ctx.baseline = (psf->flags >> 24);
    if(psf->width < 1) psf->width = 1;
    if(psf->height < 1) psf->height = 1;
    l = ((psf->width + 7) & ~7) * (psf->height + 1);
    bitmap = (unsigned char*)malloc(l);
    if(!bitmap) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
    for(unicode=rs;unicode<=(uint32_t)re;unicode++) {
        g = utbl? utbl[unicode] : unicode;
        if((!g && unicode && !iswhitespace(unicode)) || g >= psf->numglyph) continue;
        glyph = ptr + psf->headersize + g*psf->bytesperglyph;
        memset(bitmap, 0, l);
        for(i=k=0;i<(int)psf->bytesperglyph;) {
            for(n = 0; n < (int)psf->width; i++)
                for(j=0x80;j && n < (int)psf->width;j>>=1,n++)
                    bitmap[k+n] = (glyph[i] & j) ? 0xFE : 0xFF;
            k += psf->width;
        }
        if(sfn_charadd(unicode, psf->width, psf->height, 0, 0, 0))
            sfn_layeradd(unicode, SSFN_FRAG_BITMAP, 0, 0, psf->width, psf->height, 0xFE, bitmap);
        if(pbar) (*pbar)(0, 0, ++nc, numchars, PBAR_BITMAP);
    }
    free(bitmap);
    free(utbl);
}

/**
 * Parse GRUB's PFF2 font (binary)
 * yet another messy, resource wasteful, extremely badly documented format...
 */
void pff(unsigned char *ptr, int size)
{
    uint32_t len = 0, unicode, nc = 0, numchars = 0;
    int16_t w = 0, h = 0, p, n, a /*, x, y, mix, miy, max, may*/;
    int i, j, k, m;
    unsigned char *end = ptr + size, *section, *data = ptr, *bitmap;

    ptr += 12;
    while(ptr < end && len < (uint32_t)size) {
        len = (ptr[4] << 24) | (ptr[5] << 16) | (ptr[6] << 8) | ptr[7];
        section = ptr + 8;
        if(!memcmp(ptr, "NAME", 4)) sfn_setstr(&ctx.name, (char*)section, len); else
        if(!memcmp(ptr, "FAMI", 4)) sfn_setstr(&ctx.familyname, (char*)section, len); else
        if(!memcmp(ptr, "WEIG", 4) && section[0]=='b') ctx.style |= SSFN_STYLE_BOLD; else
        if(!memcmp(ptr, "SLAN", 4) && section[0]=='i') ctx.style |= SSFN_STYLE_ITALIC; else
        if(!memcmp(ptr, "MAXW", 4)) w = (section[0] << 8) | section[1]; else
        if(!memcmp(ptr, "MAXH", 4)) h = (section[0] << 8) | section[1]; else
        if(!memcmp(ptr, "ASCE", 4)) ctx.baseline = (section[0] << 8) | section[1]; else
        if(!memcmp(ptr, "CHIX", 4)) {
            /*mix = miy = max = may = 0;*/
            for(end = section + len, ptr = section; ptr < end; ptr += 9) {
                if(!ptr[4]) {
                    n = (data[((ptr[5] << 24) | (ptr[6] << 16) | (ptr[7] << 8) | ptr[8]) + 0] << 8) |
                        data[((ptr[5] << 24) | (ptr[6] << 16) | (ptr[7] << 8) | ptr[8]) + 1];
                    if(n > w) w = n;
                    n = (data[((ptr[5] << 24) | (ptr[6] << 16) | (ptr[7] << 8) | ptr[8]) + 2] << 8) |
                        data[((ptr[5] << 24) | (ptr[6] << 16) | (ptr[7] << 8) | ptr[8]) + 3];
                    if(n > h) h = n;
/*
                    n = (data[((ptr[5] << 24) | (ptr[6] << 16) | (ptr[7] << 8) | ptr[8]) + 4] << 8) |
                        data[((ptr[5] << 24) | (ptr[6] << 16) | (ptr[7] << 8) | ptr[8]) + 5];
                    if(n < mix) { mix = n; } if(n > max) max = n;
                    n = (data[((ptr[5] << 24) | (ptr[6] << 16) | (ptr[7] << 8) | ptr[8]) + 6] << 8) |
                        data[((ptr[5] << 24) | (ptr[6] << 16) | (ptr[7] << 8) | ptr[8]) + 7];
                    if(n < miy) { miy = n; } if(n > may) may = n;
*/
                    numchars++;
                }
            }
            ctx.width = w/* - mix + max*/;
            ctx.height = h/* - miy + may*/;
/*            ctx.baseline -= miy; */
            if(verbose > 1) printf("\r  Name '%s' num_glyphs: %d, ascender: %d, width: %d, height: %d\n", ctx.name, numchars, ctx.baseline,
                ctx.width, ctx.height);
            n = ctx.width * ctx.height;
            bitmap = (unsigned char*)malloc(n);
            if(!bitmap) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
            for(end = section + len; section < end; section += 9) {
                /* undocumented: section[4] supposed to indicate compression of some sort? grub-mkfont.c always writes 0 */
                if(!section[4]) {
                    /* undocumented: section[0] holds left and right joins (or both), not sure what to do with those */
                    unicode = (section[1] << 16) | (section[2] << 8) | section[3];
                    ptr = data + ((section[5] << 24) | (section[6] << 16) | (section[7] << 8) | section[8]);
                    memset(bitmap, 0xFF, n);
                    w = (ptr[0] << 8) | ptr[1]; h = (ptr[2] << 8) | ptr[3];
/*                    x = (ptr[4] << 8) | ptr[5]; y = (ptr[6] << 8) | ptr[7];*/
                    a = (ptr[8] << 8) | ptr[9];
                    p = w; /* + (x < 0 ? 0 : x); h -= y;*/
                    ptr += 10; /*k = (y - miy) * p;*/
                    for(j = k = 0, m = 0x80; j < h; j++, k += p)
                        for(i = 0; i < w; i++, m >>= 1) {
                            if(!m) { m = 0x80; ptr++; }
                            if(ptr[0] & m) bitmap[k + i/* + (x < 0 ? 0 : x)*/] = 0xFE;
                        }
                    if(sfn_charadd(unicode, p, h, a, 0, /*x < 0 ? -x :*/ 0))
                        sfn_layeradd(unicode, SSFN_FRAG_BITMAP, 0, 0, p, h, 0xFE, bitmap);
                    if(pbar) (*pbar)(0, 0, ++nc, numchars, PBAR_BITMAP);
                }
            }
            free(bitmap);
            break;
        }
        ptr += 8 + len;
    }
}

/**
 * Parse fucked up Windows Console Font (binary)
 */
void fnt(unsigned char *ptr, int size)
{
    int i, j, k, l, m, w, h, p, mn, mx, defchar;
    unsigned char *data = ptr, *bit, map[8192], *bitmap = NULL, ver;
    /* skip over executable header... */
    if(ptr[0] == 'M' && ptr[1] == 'Z') {
        ptr += ((ptr[0x3D] << 8) | ptr[0x3C]); if(ptr[0] != 'N' || ptr[1] != 'E') return;
        ptr += ((ptr[37] << 8) | ptr[36]); j = ((ptr[1] << 8) | ptr[0]); ptr += 2; if(j > 16) return;
        for(i = 0; i < 16 && (ptr[0] || ptr[1]); i++) {
            if(ptr[0] == 0x08 && ptr[1] == 0x80) {
                if(((ptr[3] << 8) | ptr[2]) < 1) { return; } ptr += 8; ptr = data + (((ptr[1] << 8) | ptr[0]) << j); break;
            } else ptr += ((ptr[3] << 8) | ptr[2]) * 12 + 8;
        }
    }
    /* parse Windows resource file */
    if(ptr[0] || (ptr[1] != 2 && ptr[1] != 3) || ((ptr[4] << 16) | (ptr[3] << 8) | ptr[2]) > size ||
        (ptr[66] & 1)) return;
    sfn_setstr(&ctx.name, (char*)(ptr + ((ptr[107] << 16) | (ptr[106] << 8) | ptr[105])), 0);
    sfn_setstr(&ctx.license, (char*)(ptr + 6), 0);
    h = ((ptr[89] << 8) | ptr[88]);
    mn = ptr[95]; mx = ptr[96]; defchar = ptr[97]; ctx.baseline = ((ptr[75] << 8) | ptr[74]);
    if(ptr[80]) ctx.style |= SSFN_STYLE_ITALIC;
    if(((ptr[84] << 8) | ptr[83]) > 400) ctx.style |= SSFN_STYLE_BOLD;
    switch(ptr[90] >> 4) {
        case 2: ctx.family = SSFN_FAMILY_SANS; break;
        case 3: ctx.family = SSFN_FAMILY_MONOSPACE; break;
        case 4: ctx.family = SSFN_FAMILY_HAND; break;
        case 5: ctx.family = SSFN_FAMILY_DECOR; break;
        default: ctx.family = SSFN_FAMILY_SERIF; break;
    }
    if(verbose > 1) printf("\r  Name '%s' num_glyphs: %d, ascender: %d, height: %d\n", ctx.name, mx - mn + 1, ctx.baseline, h);
    if(!h || mn >= mx) return;
    ver = ptr[1]; data = ptr; ptr += (ptr[1] == 3 ? 148 : 118);

    /* get bitmaps */
    for(unicode = mn; unicode <= mx; unicode++, ptr += ver == 3 ? 6 : 4) {
        w = ptr[0]; bit = data + ((ver == 3 ? (ptr[4] << 16) : 0) | (ptr[3] << 8) | ptr[2]);
        p = ((w - 1) >> 3) + 1;
        if(p * h > (int)sizeof(map)) continue;
        bitmap = realloc(bitmap, w * h);
        if(!bitmap) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
        /* I'm fed up. I can't properly get the bytes, so lets copy them into correct order before I get crazy. Ugly, but works */
        for(j = 0; j < p; j++)
            for(k = 0; k < h; k++)
                map[k * p + j] = *bit++;
        for(i = k = l = 0; k < h; k++)
            for(j = 0, l = k * p, m = 0x80; j < w; j++, m >>= 1) {
                if(!m) { m = 0x80; l++; }
                bitmap[i++] = map[l] & m ? 0xFE : 0xFF;
            }
        if(sfn_charadd(unicode, w, h, 0, 0, 0))
            sfn_layeradd(unicode, SSFN_FRAG_BITMAP, 0, 0, w, h, 0xFE, bitmap);
        if(defchar && defchar >= mn && unicode == defchar) {
            sfn_chardel(0);
            if(sfn_charadd(0, w, h, 0, 0, 0))
                sfn_layeradd(0, SSFN_FRAG_BITMAP, 0, 0, w, h, 0xFE, bitmap);
        }
    }
    free(bitmap);
}


/**
 * Parse GNU unifont hex format (text)
 */
void hex(char *ptr, int size)
{
    uint32_t i, j, c, unicode, nc = 0, numchars;
    int w;
    char *end = ptr + size;
    unsigned char p[256];

    numchars = 0;
    for(numchars=0;ptr < end && *ptr;ptr++) if(*ptr=='\n') numchars++;
    ptr = end - size;
    while(ptr < end && *ptr) {
        unicode = gethex(ptr, 6);
        while(*ptr && *ptr!=':') ptr++;
        if(!*ptr) return;
        ptr++;
        while(*ptr && (*ptr==' '||*ptr=='\t')) ptr++;
        if(!*ptr) return;
        memset(p, 0, 256);
        for(i = 0;i<256 && *ptr && *ptr!='\n' && *ptr!='\r';ptr += 2) {
            c = gethex(ptr, 2);
            for(j=0x80;j;j>>=1) p[i++] = (c & j) ? 0xFE : 0xFF;
        }
        while(*ptr && *ptr!='\n' && *ptr!='\r') ptr++;
        while(*ptr && (*ptr=='\n' || *ptr=='\r')) ptr++;
        w = i > 128 ? 16 : 8;
        if(sfn_charadd(unicode, w, 16, 0, 0, 0))
            sfn_layeradd(unicode, SSFN_FRAG_BITMAP, 0, 0, w, 16, 0xFE, p);
        if(pbar) (*pbar)(0, 0, ++nc, numchars, PBAR_BITMAP);
    }
}

/**
 * Parse X11 BDF font format and FontForge's SplineFontDB with bitmaps (text)
 */
void bdf(char *ptr, int size)
{
    uint32_t c;
    int w = 0, h = 0, l = 0, t = 0, i, j, a, b = 0, unicode = 0, nc = 0, numchars = 0, defchar = 0, ps = 0, mx, xx, my, xy, k;
    char *end = ptr + size, *face, *name = NULL, *style = NULL, *manu = NULL;
    unsigned char *bitmap = NULL, sfd = 0, dec[4];

    for(face = ptr; face + 12 < end && *face; face++) {
        if(!memcmp(face, "ENCODING ", 9)) numchars++;
        if(!memcmp(face, "BDFChar:", 8)) { numchars++; sfd = 1; }
        if(!memcmp(face, "BitmapFont: ", 12)) {
            ptr += 12; while(*ptr == ' ') ptr++;
            ps = atoi(ptr); while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
            while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
            b = atoi(ptr); if(!ctx.baseline) ctx.baseline = b;
            sfd = 1;
        }
    }
    face = NULL;

    while(ptr < end && *ptr) {
        if(!memcmp(ptr, "FACE_NAME ", 10) && !face) {
            ptr += 10; while(*ptr && *ptr!='\"') { ptr++; } ptr++; face = ptr;
        }
        if(!memcmp(ptr, "FONT_NAME ", 10) && !face) {
            ptr += 10; while(*ptr && *ptr!='\"') { ptr++; } ptr++; face = ptr;
        }
        if(!memcmp(ptr, "FONT_VERSION ", 13) && !ctx.revision) {
            ptr += 13; while(*ptr && *ptr!='\"') { ptr++; } ptr++;
            sfn_setstr(&ctx.revision, ptr, 0);
        }
        if(!memcmp(ptr, "ADD_STYLE_NAME ", 15) && !ctx.name && !style) {
            ptr += 15; while(*ptr && *ptr!='\"') { ptr++; } ptr++; style = ptr;
        }
        if(!memcmp(ptr, "FOUNDRY ", 8) && !ctx.manufacturer) {
            ptr += 8; while(*ptr && *ptr!='\"') { ptr++; } ptr++; manu = ptr;
        }
        if(!memcmp(ptr, "HOMEPAGE ", 9) && !ctx.manufacturer && !manu) {
            ptr += 9; while(*ptr && *ptr!='\"') { ptr++; } ptr++; manu = ptr;
        }
        if(!memcmp(ptr, "FAMILY_NAME ", 12) && !ctx.familyname) {
            ptr += 12; while(*ptr && *ptr!='\"') { ptr++; } ptr++;
            sfn_setstr(&ctx.familyname, ptr, 0);
        }
        if(!memcmp(ptr, "WEIGHT_NAME ", 12) && !ctx.subname) {
            ptr += 12; while(*ptr && *ptr!='\"') { ptr++; } ptr++;
            sfn_setstr(&ctx.subname, ptr, 0);
        }
        if(!memcmp(ptr, "COPYRIGHT ", 10) && !ctx.license) {
            ptr += 10; while(*ptr && *ptr!='\"') { ptr++; } ptr++;
            sfn_setstr(&ctx.license, ptr, 0);
        }
        if(!memcmp(ptr, "FONT_ASCENT ", 12)) {
            ptr += 12; if(sfd) { while(*ptr && *ptr != ' ') ptr++; } while(*ptr == ' ') ptr++;
            b = atoi(ptr); if(!ctx.baseline) ctx.baseline = b; }
        if(!memcmp(ptr, "UNDERLINE_POSITION ", 19)) {
            ptr += 19; if(sfd) { while(*ptr && *ptr != ' ') ptr++; } while(*ptr == ' ') ptr++;
            relul = atoi(ptr); if(relul < 0) relul = -relul; }
        if(!memcmp(ptr, "DEFAULT_CHAR ", 13)) {
            ptr += 13; if(sfd) { while(*ptr && *ptr != ' ') ptr++; } while(*ptr == ' ') ptr++;
            defchar = atoi(ptr); }
        if(!memcmp(ptr, "PIXEL_SIZE ", 11) && !ps) {
            ptr += 11; if(sfd) { while(*ptr && *ptr != ' ') ptr++; } while(*ptr == ' ') ptr++;
            ps = atoi(ptr); }
        if(!memcmp(ptr, "ENDPROPERTIES", 13) || !memcmp(ptr, "BDFEndProperties", 16)) break;
        while(*ptr && *ptr!='\n') ptr++;
        while(*ptr=='\n') ptr++;
    }
    if(!ctx.name) {
        if(!face) face = ctx.familyname;
        if(face && style && style[0]) {
            for(i = 0; face[i] && face[i] != '\"' && face[i] != '\r' && face[i] != '\n'; i++);
            for(j = 0; style[j] && style[i] != '\"' && style[i] != '\r' && style[i] != '\n'; j++);
            name = malloc(i + j + 2);
            if(!name) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
            memcpy(name, face, i);
            name[i] = ' ';
            memcpy(name + i + 1, style, j);
            name[i + j + 1] = 0;
            sfn_setstr(&ctx.name, name, 0);
            free(name);
        } else
            sfn_setstr(&ctx.name, ctx.familyname, 0);
    }
    if(!ctx.manufacturer && manu)
        sfn_setstr(&ctx.manufacturer, manu, 0);
    if(verbose > 1)
        printf("\r  Name '%s' num_glyphs: %d, ascender: %d, underline: %d, height: %d\n", ctx.name, numchars, b, b + relul, ps);

    while(ptr < end && *ptr) {
        if(!sfd) {
            if(!memcmp(ptr, "ENCODING ", 9)) { ptr += 9; unicode = atoi(ptr); }
            if(!memcmp(ptr, "BBX ", 4)) {
                ptr += 4; w = atoi(ptr);
                while(*ptr && *ptr!=' ') ptr++;
                ptr++; h = atoi(ptr);
                while(*ptr && *ptr!=' ') ptr++;
                ptr++; l = atoi(ptr);
                while(*ptr && *ptr!=' ') ptr++;
                ptr++; t = atoi(ptr);
            }
            if(!memcmp(ptr, "BITMAP", 6)) {
                ptr += 6; while(*ptr && *ptr!='\n') ptr++;
                ptr++;
                if(skipcode && !memcmp(ptr,"0000\n7FFE", 9) && !memcmp(ptr + 35,"7FFE\n7FFE", 9)) ptr += 16*5;
                else if(w > 0 && h > 0 && w * h <= 65536) {
                    bitmap = realloc(bitmap, ((w + 7) & ~7) * (h + 1));
                    if(!bitmap) { if(verbose > 1) fprintf(stderr,"libsfn: memory allocation error\n"); }
                    else {
                        for(i = 0;i < w * h && *ptr; ptr += 2) {
                            while(*ptr=='\n' || *ptr=='\r') ptr++;
                            c = gethex(ptr, 2);
                            for(j=0x80,k=0;j && k < w && i < w * h;k++,j>>=1) bitmap[i++] = c & j ? 0xFE : 0xFF;
                        }
                        while(i < ((w + 7) & ~7) * (h + 1)) bitmap[i++] = 0xFF;
                        if(!skipcode && unicode == defchar) { sfn_chardel(0); unicode = 0; }
                        if(sfn_charadd(unicode, w, h, 0, 0, 0))
                            sfn_layeradd(unicode, SSFN_FRAG_BITMAP, l, ctx.baseline - t - h, w, h, 0xFE, bitmap);
                    }
                }
                if(pbar) (*pbar)(0, 0, ++nc, numchars, PBAR_BITMAP);
                w = h = l = t = 0;
            }
        } else
        if(!memcmp(ptr, "BDFChar:", 8)) {
            ptr += 8; while(*ptr == ' ') ptr++;
            while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
            unicode = atoi(ptr); while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
            w = atoi(ptr); while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
            mx = atoi(ptr); while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
            xx = atoi(ptr); while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
            my = atoi(ptr); while(*ptr && *ptr != ' ') { ptr++; } while(*ptr == ' ') ptr++;
            xy = atoi(ptr); while(*ptr && *ptr != ' ' && *ptr != '\n') { ptr++; } while(*ptr == ' ') ptr++;
            if(*ptr != '\n') a = atoi(ptr); else a = w;
            while(*ptr && *ptr != '\n') { ptr++; } ptr++;
            h = ps; xx -= mx - 1; xy -= my - 1;
            xx = (xx + 7) & ~7;
            if(xx * xy < 65536) {
                bitmap = realloc(bitmap, xx * xy);
                if(!bitmap) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
                for(i = 0, k = 4; i < xx * xy;) {
                    if(k > 3) {
                        if(!*ptr || *ptr == '\r' || *ptr == '\n') break;
                        k = 0;
                        if(*ptr == 'z') { dec[0] = dec[1] = dec[2] = dec[3] = 0; ptr++; }
                        else {
                            c = ((((ptr[0]-'!')*85 + ptr[1]-'!')*85 + ptr[2]-'!')*85 + ptr[3]-'!')*85 + ptr[4]-'!';
                            dec[0] = (c >> 24) & 0xFF; dec[1] = (c >> 16) & 0xFF; dec[2] = (c >> 8) & 0xFF; dec[3] = c & 0xFF;
                            ptr += 5;
                        }
                    }
                    c = dec[k++];
                    for(j = 0x80; j; j >>= 1) bitmap[i++] = c & j ? 0xFE : 0xFF;
                }
                while(i < xx * xy) bitmap[i++] = 0xFF;
                if(!skipcode && unicode == defchar) { sfn_chardel(0); unicode = 0; }
                if(sfn_charadd(unicode, w, h, a, 0, mx < 0 ? -mx : 0))
                    sfn_layeradd(unicode, SSFN_FRAG_BITMAP, 0, 0, xx, xy, 0xFE, bitmap);
            }
            if(pbar) (*pbar)(0, 0, ++nc, numchars, PBAR_BITMAP);
        }
        while(*ptr && *ptr!='\n') ptr++;
        while(*ptr=='\n') ptr++;
    }
    if(bitmap) free(bitmap);
}

/**
 * Parse X11 PCF font format (binary)
 */
void pcf(unsigned char *ptr, int size)
{
    uint32_t i, j, n, *iptr=(uint32_t*)ptr, fmt, mf=0, bf=0, bs=0, ef=0, offs, mn=0, mx=0, mg=0, siz;
    uint32_t boffs = 0, bitmaps = 0, metrics = 0, encodings = 0;
    unsigned char *bitmap = NULL, *bm;
    char *face = NULL, *name = NULL, *style = NULL, *manu = NULL;
    char *str, *s, *v;
    int x, y, o, a, b = 0, k, w, h = 0, p = 1, r, sx = 1, m, defchar = 0;
#define pcf32(f,o) (f&(1<<2)? (ptr[o+0]<<24)|(ptr[o+1]<<16)|(ptr[o+2]<<8)|ptr[o+3] : \
    (ptr[o+3]<<24)|(ptr[o+2]<<16)|(ptr[o+1]<<8)|ptr[o+0])
#define pcf16(f,o) (f&(1<<2)? (ptr[o+0]<<8)|ptr[o+1] : (ptr[o+1]<<8)|ptr[o+0])

    /* parse tables */
    for(i = 0; i < iptr[1]; i++) {
        fmt = iptr[i*4+3]; offs = iptr[i*4+5];
        if(offs + iptr[i*4+4] >= (uint32_t)size) continue;
        switch(iptr[i*4+2]) {
            case (1<<0): /* PCF_PROPERTIES */
                n = pcf32(fmt,offs+4); str = (char*)ptr + offs + ((n * 9 + 3)/4 + 3)*4;
                for(j = 0; j < n; j++) {
                    s = str + pcf32(fmt,offs + 8 + j * 9); v = str + pcf32(fmt,offs + 13 + j * 9);
                    if(!strcmp(s, "FACE_NAME") && !face) face = v; else
                    if(!strcmp(s, "FONT_NAME") && !face) face = v; else
                    if(!strcmp(s, "FONT_VERSION") && !ctx.revision) sfn_setstr(&ctx.revision, v, 0); else
                    if(!strcmp(s, "ADD_STYLE_NAME") && !ctx.name && !style) style = v; else
                    if(!strcmp(s, "FOUNDRY") && !ctx.manufacturer) manu = v; else
                    if(!strcmp(s, "HOMEPAGE") && !ctx.manufacturer && !manu) manu = v; else
                    if(!strcmp(s, "FAMILY_NAME") && !ctx.familyname) sfn_setstr(&ctx.subname, v, 0); else
                    if(!strcmp(s, "WEIGHT_NAME") && !ctx.subname) sfn_setstr(&ctx.subname, v, 0); else
                    if(!strcmp(s, "COPYRIGHT") && !ctx.license) sfn_setstr(&ctx.license, v, 0); else
                    if(!strcmp(s, "PIXEL_SIZE")) { k = pcf32(fmt,offs + 13 + j * 9); if(k > h) h = k; } else
                    if(!strcmp(s, "UNDERLINE_POSITION")) {
                        relul = (int)pcf32(fmt,offs + 13 + j * 9); if(relul < 0) relul = -relul;
                    }
                }
            break;
            case (1<<2): /* PCF_METRICS */
                metrics = offs; mf = fmt; b = 0;
                if(fmt & 0x100) {
                    n = pcf16(fmt,offs + 4);
                    for(j=0; j<n; j++) { k = (int)ptr[offs+3+6+j*5]-0x80; if(k > b) b = k; }
                    for(j=0; j<n; j++) { k = (int)ptr[offs+4+6+j*5]-0x80 + b; if(k > h) h = k; }
                } else {
                    n = pcf32(fmt,offs + 4);
                    for(j = 0; j < n; j++) { k = pcf16(fmt,offs+6+8+j*12); if(k > b) b = k; }
                    for(j = 0; j < n; j++) { k = pcf16(fmt,offs+8+8+j*12) + b; if(k > h) h = k; }
                }
                if(!mn && !mx && n) mx = n - 1;
                if(!mg || n < mg) mg = n;
            break;
            case (1<<3): /* PCF_BITMAPS */
                boffs = offs + 8; bf = fmt; n = pcf32(fmt,offs+4);
                bitmaps = boffs + n * 4 + 16; bs = iptr[i*4+4] + offs - bitmaps;
                p = 1 << (fmt & 3); sx = fmt & (1 << 2) ? 1 : -1;
                if(!mg || n < mg) mg = n;
            break;
            case (1<<5): /* PCF_BDF_ENCODINGS */
                encodings = offs + 14; ef = fmt;
                mn = (pcf16(fmt, offs + 8)<<8) | pcf16(fmt, offs + 4);
                mx = (pcf16(fmt, offs + 10)<<8) | pcf16(fmt, offs + 6);
                defchar = pcf16(fmt, offs + 12);
            break;
        }
    }
    if(!ctx.name) {
        if(!face) face = ctx.familyname;
        if(face && style && style[0]) {
            i = strlen(face); j = strlen(style);
            name = malloc(i + j + 2);
            if(!name) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
            memcpy(name, face, i);
            name[i] = ' ';
            memcpy(name + i + 1, style, j);
            name[i + j + 1] = 0;
            sfn_setstr(&ctx.name, name, 0);
            free(name);
        } else
            sfn_setstr(&ctx.name, ctx.familyname, 0);
    }
    if(!ctx.manufacturer && manu)
        sfn_setstr(&ctx.manufacturer, manu, 0);
    if(verbose > 1) printf("\r  Name '%s' num_glyphs: %d, ascender: %d, underline: %d, height: %d\n", face, mg, b, b + relul, h);
    if(!b || !h || !mg || !mx || !bitmaps) return;
    if(mg > 65534) mg = 65534;
    ctx.baseline = b;
    ctx.underline = b + relul;
    ctx.height = h;

    /* parse bitmaps and add glyphs. Encoding table must be handled as optional */
    for(unicode = mn; (uint32_t)unicode <= mx; unicode++) {
        if(pbar) (*pbar)(0, 0, unicode, mx - mn + 1, PBAR_BITMAP);
        i = encodings ? pcf16(ef, encodings + unicode * 2) : unicode;
        if(i >= mg) continue;
        offs = pcf32(bf, boffs + i * 4); siz = (i >= mg - 1 ? bs : (uint32_t)pcf32(bf, boffs + i * 4 + 4)) - offs;
        if(mf & 0x100) {
            x = (int)ptr[metrics+6+i*5]-0x80; a = (int)ptr[metrics+3+6+i*5]-0x80;
            w = (int)ptr[metrics+2+6+i*5]-0x80; r = (int)ptr[metrics+1+6+i*5]-0x80;
        } else {
            x = (int16_t)pcf16(mf,metrics+8+i*12); a = pcf16(mf,metrics+6+8+i*12);
            w = pcf16(mf,metrics+4+8+i*12); r = pcf16(mf,metrics+2+8+i*12);
        }
        /* do some heuristics and validation because PCF fonts are *usually* buggy... */
        if(x < 0) { o = -x; x = 0; } else o = 0;
        if(w < r) r = w;
        if(p < 1) p = 1;
        n = (siz / p); y = b - a;
        if(n > (uint32_t)h) n = h;
        if(y < 0) y = 0;
        if(y + n > (uint32_t)h) y = h - n;
        k = n * r; if(k < 1 || k > 65536) continue;
        bitmap = realloc(bitmap, k);
        if(!bitmap) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
        memset(bitmap, 0xFF, k);
        if(siz > n * p) siz = n * p;
        for(j = 0; siz; siz -= p, offs += p) {
            /* seriously, who have thought even for a moment that messing with both byte and bit endianess is sane? */
            bm = ptr + bitmaps + offs + (sx < 0 ? p - 1 : 0); m = bf & (1 << 3) ? 1 << 7 : 1;
            for(k = 0; k < r; k++) {
                bitmap[j++] = bm[0] & m ? 0xFE : 0xFF;
                if(bf & (1 << 3)) { m >>= 1; if(!m) { m = 1 << 7; bm += sx; } }
                else { m <<= 1; if(m > 0x80) { m = 1; bm += sx; } }
            }
        }
        if(unicode == defchar) {
            sfn_chardel(0);
            j = 0;
        } else j = unicode;
        if(sfn_charadd(j, w, h, x+r, 0, o))
            sfn_layeradd(j, SSFN_FRAG_BITMAP, x, y, r, n, 0xFE, bitmap);
    }
    if(bitmap) free(bitmap);
}

/**
 * Parse a pixel map in memory
 */
void pixmap_parse(unsigned char *data, int w, int h)
{
    unsigned char *data2;
    unsigned int i, j, k, x, y, m, o;
    int unicode;

    if(rs==0 && re==0x10FFFF)
        re = h > w ? h / w : w / h;
    if(h > w ) {
        m = h / (re - rs + 1);
        if(m < 8 || w < 8) { if(verbose > 1) { fprintf(stderr, "libsfn: unable to determine glyph size\n"); } return; }
        for(unicode=rs, i=0; unicode<=re; unicode++, i += w*m) {
            for(y=k=0;y<m;y++)
                for(j=w-1;j>k;j--)
                    if(data[i+y*w+j] < 0xFE) k=j;
            if(sfn_charadd(unicode, w, m, k+1, 0, 0))
                sfn_layeradd(unicode, SSFN_FRAG_PIXMAP, 0, 0, w, m, 0xFE, data + i);
            if(pbar) (*pbar)(0, 0, unicode, re - rs, PBAR_TALLPIX);
        }
    } else {
        m = w / (re - rs + 1);
        if(m < 8 || h < 8) { if(verbose > 1) { fprintf(stderr, "libsfn: unable to determine glyph size\n"); } return; }
        data2 = (unsigned char*)malloc(m*h);
        if(!data2) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
        for(unicode=rs; unicode<=re; unicode++) {
            for(y=o=k=0;y<(unsigned int)h;y++) {
                i = y*w + (unicode-rs)*m;
                for(x=0;x<m;x++) {
                    if(data[i] < 0xFE && k < x) k = x;
                    data2[o++] = data[i++];
                }
            }
            if(sfn_charadd(unicode, m, h, k+1, 0, 0))
                sfn_layeradd(unicode, SSFN_FRAG_PIXMAP, 0, 0, m, h, 0xFE, data2);
            if(pbar) (*pbar)(0, 0, unicode, re - rs, PBAR_WIDEPIX);
        }
        free(data2);
    }
}

/**
 * Parse PNG format for pixel fonts (binary)
 */
void png(unsigned char *ptr, int size)
{
    unsigned char *data, *data2;
    unsigned int i, w, h;

    w = h = size = 0;
    data = (uint8_t*)image_load((char*)ptr, (int*)&w, (int*)&h);
    if(!data || w < 1 || h < 1)
        { if(verbose > 1) { fprintf(stderr,"libsfn: unsupported PNG format\n"); } return; }
    data2 = (unsigned char*)malloc(w * h);
    if(!data2) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
    if(pbar) (*pbar)(0, 0, 0, 1, PBAR_QUANT);
    for(i = 0; i < w * h; i++) {
        if(pbar) (*pbar)(0, 0, i, w * h, PBAR_QUANT);
        data2[i] = sfn_cpaladd(data[i*4], data[i*4+1], data[i*4+2], data[i*4+3]);
    }
    free(data);
    pixmap_parse(data2, w, h);
    free(data2);
}

/**
 * Parse TGA format for pixel fonts (binary)
 */
void tga(unsigned char *ptr, int size)
{
    unsigned char *data;
    int i, j, k, x, y, w, h, o, m;

    o = (ptr[11] << 8) + ptr[10];
    w = (ptr[13] << 8) + ptr[12];
    h = (ptr[15] << 8) + ptr[14];
    if(w<1 || h<1) {
tgaerr: if(verbose > 1) fprintf(stderr,"libsfn: unsupported TGA file format\n");
        return;
    }
    m = ((ptr[1]? (ptr[7]>>3)*ptr[5] : 0) + 18);
    data = (unsigned char*)malloc(w*h);
    if(!data) { if(verbose > 1) { fprintf(stderr,"libsfn: memory allocation error\n"); } return; }
    switch(ptr[2]) {
        case 1:
            if(ptr[6]!=0 || ptr[4]!=0 || ptr[3]!=0 || (ptr[7]!=24 && ptr[7]!=32)) goto tgaerr;
            for(y=i=0; y<h; y++) {
                k = ((!o?h-y-1:y)*w);
                for(x=0; x<w; x++) {
                    j = ptr[m + k++]*(ptr[7]>>3) + 18;
                    data[i++] = sfn_cpaladd(ptr[j+2], ptr[j+1], ptr[j], ptr[7]==32?ptr[j+3]:0xFF);
                }
            }
        break;

        case 2:
            if(ptr[5]!=0 || ptr[6]!=0 || ptr[1]!=0 || (ptr[16]!=24 && ptr[16]!=32)) goto tgaerr;
            for(y=i=0; y<h; y++) {
                j = ((!o?h-y-1:y)*w*(ptr[16]>>3));
                for(x=0; x<w; x++) {
                    data[i++] = sfn_cpaladd(ptr[j+2], ptr[j+1], ptr[j], ptr[16]==32?ptr[j+3]:0xFF);
                    j += ptr[16]>>3;
                }
            }
        break;

        case 9:
            if(ptr[6]!=0 || ptr[4]!=0 || ptr[3]!=0 || (ptr[7]!=24 && ptr[7]!=32)) goto tgaerr;
            y = i = 0;
            for(x=0; x<w*h && m<size;) {
                k = ptr[m++];
                if(k > 127) {
                    k -= 127; x += k;
                    j = ptr[m++]*(ptr[7]>>3) + 18;
                    while(k--) {
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = sfn_cpaladd(ptr[j+2], ptr[j+1], ptr[j], ptr[7]==32?ptr[j+3]:0xFF);
                    }
                } else {
                    k++; x += k;
                    while(k--) {
                        j = ptr[m++]*(ptr[7]>>3) + 18;
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = sfn_cpaladd(ptr[j+2], ptr[j+1], ptr[j], ptr[7]==32?ptr[j+3]:0xFF);
                    }
                }
            }
        break;

        case 10:
            if(ptr[5]!=0 || ptr[6]!=0 || ptr[1]!=0 || (ptr[16]!=24 && ptr[16]!=32)) goto tgaerr;
            y = i = 0;
            for(x=0; x<w*h && m<size;) {
                k = ptr[m++];
                if(k > 127) {
                    k -= 127; x += k;
                    while(k--) {
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = sfn_cpaladd(ptr[m+2], ptr[m+1], ptr[m], ptr[16]==32?ptr[m+3]:0xFF);
                    }
                    m += ptr[16]>>3;
                } else {
                    k++; x += k;
                    while(k--) {
                        if(!(i%w)) { i=((!o?h-y-1:y)*w); y++; }
                        data[i++] = sfn_cpaladd(ptr[m+2], ptr[m+1], ptr[m], ptr[16]==32?ptr[m+3]:0xFF);
                        m += ptr[16]>>3;
                    }
                }
            }
        break;
    }
    pixmap_parse(data, w, h);
    free(data);
}
