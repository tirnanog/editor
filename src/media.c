/*
 * tnge/media.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Media files window
 *
 */

#include "main.h"
#include "SDL2_mixer/src/music.h"
#include "SDL_thread.h"
#include "vorbis/vorbisenc.h"
#include "theora.h"

#define AUBUFF 1024*1024

/* when importing translated speech */
extern char translate_code[], *translate_fn;

SDL_Thread *media_th = NULL;
int media_tab, *media_durs = NULL, media_ffmpeg = 0;
char media_dir[64], media_pbar = 0;
void media_new(void *data);
void media_delete(void *data);
void media_playhook(void *data);
void media_changecat(void *data);
void media_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

ui_icongrpelem_t media_catelems[] = {
    { ICON_MUS, MEDIA_CAT_MUSIC },
    { ICON_SND, MEDIA_CAT_SOUND },
    { ICON_SPCH, MEDIA_CAT_SPEECH },
    { ICON_MOV, MEDIA_CAT_MOVIE }
};
ui_icongrp_t media_cats = { &media_tab, 0, 4, media_catelems };

ui_tablehdr_t media_table_hdr[] = {
    { MEDIA_TIME, 96, 0, 0 },
    { MEDIA_NAME, 0, 0, 0 },
    { 0 }
};

ui_table_t media_table = { media_table_hdr, MEDIA_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0,
    sizeof(char*), media_drawcell, NULL, NULL, NULL };

/**
 * The form
 */
ui_form_t media_form[] = {
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, MEDIA_ADD, (void*)ICON_ADD, media_new },
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, MEDIA_DELETE, (void*)ICON_REMOVE, media_delete },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, MEDIA_PLAY, (void*)ICON_PLAY, media_playhook },
    { FORM_TABLE, 10, 29, 0, 0, 0, 0, &media_table, media_playhook },
    { FORM_ICONGRP, 20, 0, 4*24-4, 20, 0, 0, &media_cats, media_changecat },
    { FORM_LAST }
};

/**
 * Initialize media libs
 */
void media_initlibs()
{
    Mix_Init(MIX_INIT_MOD|MIX_INIT_MP3|MIX_INIT_OGG|MIX_INIT_FLAC|MIX_INIT_OPUS);
}

/**
 * Free media libs
 */
void media_freelibs()
{
    Mix_Quit();
}

/**
 * Get formatted media duration (100 hsec = 1 sec)
 */
void media_dur(int hsec, char *str)
{
    sprintf(str, "%02u:%02u.%02u", hsec / 6000, (hsec / 100) % 60, hsec % 100);
}

/**
 * Get media filename
 */
void media_getfn(char *fn)
{
    if(media_tab == SUBMENU_SPEECH - SUBMENU_MUSIC)
        sprintf(projfn, "%s" SEP "%s_%c%c.ogg", media_dir, fn, lang[-1][0], lang[-1][1]);
    else
        sprintf(projfn, "%s" SEP "%s.og%c", media_dir, fn, media_tab == SUBMENU_MOVIES - SUBMENU_MUSIC ? 'v' : 'g');
}

/**
 * Draw table cell
 */
void media_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    char tmp[16];

    (void)idx; (void)sel; (void)w; (void)h;
    if(media_durs && media_durs[idx] != 0) {
        media_dur(media_durs[idx], tmp);
        ui_text(dst, x + 4, y, tmp);
    } else
        ui_text(dst, x + 4, y, "--:--.--");
    ui_text(dst, x + 4 + hdr[0].w, y, *((char**)data));
}

/**
 * Add new media file
 */
void media_new(void *data) {
    (void)data;
    fileops_type = media_tab == SUBMENU_MOVIES - SUBMENU_MUSIC ? FILEOPS_MOVIE : FILEOPS_AUDIO;
    ui_switchtab(SUBMENU_IMPORT);
}

/**
 * Delete a media file
 */
void media_delete(void *data) {
    char **files = (char**)media_table.data;
    int i;

    (void)data;
    if(!media_table.data || media_table.val < 0) return;
    if(ui_modal(ICON_WARN, LANG_SURE, NULL)) {
        media_getfn(files[media_table.val]);
        if(verbose) printf("media_delete: removing %s\n", projdir);
        remove(projdir);
        if(media_tab == SUBMENU_SPEECH - SUBMENU_MUSIC)
            for(i = 0; i < project.numlang; i++) {
                sprintf(projfn, "%s" SEP "%s_%c%c.ogg", media_dir, files[media_table.val],
                    project.languages[i][0], project.languages[i][1]);
                if(verbose) printf("media_delete: removing %s\n", projdir);
                remove(projdir);
            }
    }
    *projfn = 0;
}

/**
 * Media file click callback
 */
void media_playhook(void *data)
{
    char **files = (char**)media_table.data;

    (void)data;
    if(!files || !media_table.num || media_table.val < 0) return;
    media_form[2].inactive = 1;
    ui_redraw(2, 2);
    media_getfn(files[media_table.val]);
    media_pbar = 1;
    media_play(projdir);
    media_pbar = 0;
    media_table.clk = -1;
}

/**
 * Change media category
 */
void media_changecat(void *data)
{
    (void)data;
    ui_switchtab(media_tab + SUBMENU_MUSIC);
}

/**
 * Exit media files window
 */
void media_exit(int tab)
{
    int i;

    (void)tab;
    media_table.clk = -1;
    media_table.num = 0; /* <-- this will make the worker thread to quit */
    if(media_th) { SDL_WaitThread(media_th, &i); media_th = NULL; }
    if(media_durs) { free(media_durs); media_durs = NULL; }
}

/**
 * Load media durations
 */
int media_worker(void *data)
{
    char **files = (char**)media_table.data;
    int i;
    SDL_Event event;

    (void)data;
    event.type = SDL_WINDOWEVENT;
    event.window.event = SDL_WINDOWEVENT_EXPOSED;
    event.window.windowID = SDL_GetWindowID(window);
    for(i = 0; i < media_table.num; i++) {
        media_getfn(files[i]);
        media_durs[i] = theora_getduration(projdir);
        SDL_PushEvent(&event);
    }
    return 0;
}

/**
 * Enter media files window
 */
void media_init(int tab)
{
    char tmp[16];

    media_tab = tab - SUBMENU_MUSIC;
    media_exit(tab);

    strcpy(media_dir, project.id);
    strcat(media_dir, SEP);
    strcat(media_dir, project_dirs[PROJDIRS_MEDIA + media_tab]);
    strcpy(projfn, media_dir);
    switch(tab) {
        case SUBMENU_MUSIC:
            project_freedir(&project.music, &project.nummusic);
            project.music = project_getdir(projdir, ".ogg", &project.nummusic);
            media_table.data = project.music;
            media_table.num = project.nummusic;
        break;
        case SUBMENU_SOUNDS:
            project_freedir(&project.sounds, &project.numsounds);
            project.sounds = project_getdir(projdir, ".ogg", &project.numsounds);
            media_table.data = project.sounds;
            media_table.num = project.numsounds;
        break;
        case SUBMENU_SPEECH:
            sprintf(tmp, "_%c%c.ogg", lang[-1][0], lang[-1][1]);
            project_freedir(&project.speech, &project.numspeech);
            project.speech = project_getdir(projdir, tmp, &project.numspeech);
            media_table.data = project.speech;
            media_table.num = project.numspeech;
        break;
        case SUBMENU_MOVIES:
            project_freedir(&project.movies, &project.nummovies);
            project.movies = project_getdir(projdir, ".ogv", &project.nummovies);
            media_table.data = project.movies;
            media_table.num = project.nummovies;
        break;
    }
    if(media_table.num) {
        media_durs = (int*)main_alloc(media_table.num * sizeof(int));
        media_th = SDL_CreateThread(media_worker, "worker", NULL);
    }
}

/**
 * Resize the view
 */
void media_resize(int tab, int w, int h)
{
    (void)tab;
    media_form[0].y = media_form[1].y = media_form[2].y = media_form[4].y = h - 48;
    media_form[0].x = w - 40;
    media_form[2].x = w - 104;
    ui_table_resize(&media_form[3], w - 20, h - 89);
    media_form[4].x = w / 4 - media_form[4].w / 2;
}

/**
 * View layer
 */
void media_redraw(int tab)
{
    (void)tab;
    ui_form = media_form;
    media_form[1].inactive = media_form[2].inactive = media_table.val < 0;
}

/**
 * Import an media file
 * Original code from libvorbis/examples/encoder_example.c
 */
int media_import(char *path)
{
    Mix_Music *music = NULL;
    char *s, *lng;
    int i, j, siz, ret, numchannel, dur;
    signed char *buf, hdr[4];
    FILE *f, *g;
    float **buffer;
    size_t pos = 0, size, len;
    vorbis_info      vi; /* struct that stores all the static vorbis bitstream settings */
    vorbis_dsp_state vd; /* central working state for the packet->PCM decoder */
    vorbis_block     vb; /* local working space for packet->PCM decode */
    vorbis_comment   vc; /* struct that stores all the user comments */
    ogg_stream_state os; /* take physical pages, weld into a logical stream of packets */
    ogg_page         og; /* one Ogg bitstream page.  Vorbis packets are inside */
    ogg_packet       op; /* one raw packet of data for decode */
    ogg_packet       header;
    ogg_packet       header_comm;
    ogg_packet       header_code;

    if(media_th) { SDL_WaitThread(media_th, &i); media_th = NULL; }

    /* generate media filename with path */
    if(fileops_type == FILEOPS_TRANSLATION) {
        numchannel = 1; media_tab = 2;
        s = translate_fn;
        lng = translate_code;
    } else {
        numchannel = (!media_tab || media_tab == 3 ? 2 : 1);
        s = strrchr(path, SEP[0]);
        if(!s) s = path; else s++;
        lng = lang[-1];
    }
    if(!s) return 0;
    strcpy(projfn, project.id);
    strcat(projfn, SEP);
    strcat(projfn, project_dirs[PROJDIRS_MEDIA + media_tab]);
    project_mkdir(projfn);
    strcat(projfn, SEP);
    strcat(projfn, s);
    s = strrchr(projfn, '.');
    if(!s) s = projfn + strlen(projfn);
    *s = 0;
    if(media_tab == 2) { *s++ = '_'; *s++ = lng[0]; *s++ = lng[1]; *s = 0; }
    strcat(s, media_tab == 3 ? ".ogv" : ".ogg");
    if(verbose) printf("media_import: importing %s to %s\n", path, projdir);

    if(media_tab == 3) {
    /*** convert movie ***/
        f = project_fopen(path, "rb");
        if(f) {
            memset(hdr, 0, sizeof(hdr));
            if(!fread(hdr, 1, 4, f) || memcmp(hdr, "OggS", 4)) {
                if(media_ffmpeg) {
                    fclose(f);
                    return project_ffmpeg(path, projdir);
                } else
                    goto moverr;
            }
            fseek(f, 0UL, SEEK_END);
            size = (size_t)ftell(f);
            fseek(f, 0UL, SEEK_SET);
            g = project_fopen(projdir, "wb+");
            if(g) {
                buf = (signed char*)main_alloc(1024*1024);
                while(pos < size) {
                    len = fread(buf, 1, AUBUFF, f);
                    if(!len) break;
                    fwrite(buf, 1, AUBUFF, g);
                    ui_progressbar(0, 0, pos, size, MEDIA_IMPORT);
                    pos += len;
                }
                free(buf);
                fclose(g);
                fclose(f);
                ui_progressbar(0, 0, 0, 0, MEDIA_IMPORT);
            } else {
moverr:         fclose(f);
                ui_status(1, lang[ERR_SAVEMOV]);
                return 0;
            }
        } else {
            ui_status(1, lang[ERR_LOADMOV]);
            *projfn = 0;
            return 0;
        }
        ret = 1;
    } else {
    /*** convert audio ***/
        vorbis_info_init(&vi);
        if(vorbis_encode_init_vbr(&vi, numchannel, FREQ, numchannel == 1 ? 0.5 : 0.3)) {
            ui_status(1, lang[ERR_SAVESND]);
            vorbis_info_clear(&vi);
            *projfn = 0;
            return 0;
        }

        if(Mix_OpenAudio(FREQ, AUDIO_S16LSB, numchannel, 4096) < 0) {
            if(verbose) printf("media_import: Mix_OpenAudio failed\n");
            ui_status(1, lang[ERR_AUDIO]);
            vorbis_info_clear(&vi);
            *projfn = 0;
            return 0;
        }
        if(!(music = Mix_LoadMUS(path))) {
            if(verbose) printf("media_import: Mix_LoadMUS failed\n");
            ui_status(1, lang[ERR_LOADSND]);
            Mix_CloseAudio();
            vorbis_info_clear(&vi);
            *projfn = 0;
            return 0;
        }
        f = project_fopen(projdir, "wb+");
        if(!f) {
            ui_status(1, lang[ERR_SAVESND]);
            Mix_FreeMusic(music);
            Mix_CloseAudio();
            vorbis_info_clear(&vi);
            *projfn = 0;
            return 0;
        }
        Mix_VolumeMusic(MIX_MAX_VOLUME);
#ifndef USE_DYNLIBS
        /* only the latest development SDL2_mixer has Mix_MusicDuration function, not the shared lib ones */
        dur = (int)(Mix_MusicDuration(music) * 100.0);
        if(verbose) printf("media_import: length %u\n", dur);
#else
        dur = 0;
#endif

        vorbis_analysis_init(&vd, &vi);
        vorbis_block_init(&vd, &vb);
        vorbis_comment_init(&vc);
        vorbis_analysis_headerout(&vd, &vc, &header, &header_comm, &header_code);
        ogg_stream_init(&os, SDL_GetTicks());
        ogg_stream_packetin(&os, &header);
        ogg_stream_packetin(&os, &header_comm);
        ogg_stream_packetin(&os, &header_code);

        /* This ensures the actual
         * audio data will start on a new page, as per spec
         */
        while(ogg_stream_flush(&os, &og)) {
          fwrite(og.header, 1, og.header_len, f);
          fwrite(og.body, 1, og.body_len, f);
        }

        /* get raw PCM data from dragonland */
        if(music->context && music->interface && music->interface->GetAudio) {
            if(verbose) {
                printf("media_import: got interface and GetAudio\n");
                if(!music->interface->Play) printf("media_import: interface Play missing\n");
            }
            if(music->interface->Play) music->interface->Play(music->context, 1);
            buf = (signed char *)main_alloc(AUBUFF);
            do {
                memset(buf, 0, AUBUFF);
                ret = music->interface->GetAudio(music->context, buf, AUBUFF);
                if(ret < 0) ret = 0;
                else ret = AUBUFF - ret;
                /* output vorbis stream */
                if(ret) {
                    siz = ret / numchannel / sizeof(int16_t);
                    buffer = vorbis_analysis_buffer(&vd, AUBUFF / numchannel / sizeof(int16_t));
                    /* uninterleave samples */
                    for(i = 0; i < siz; i++)
                        for(j = 0; j < numchannel; j++)
                            buffer[j][i] = ((buf[(i * numchannel + j) * sizeof(int16_t) + 1] << 8) |
                                (0x00ff & (int)buf[(i * numchannel + j) * sizeof(int16_t) + 0])) / 32768.f;
                    (void)buffer;
                    vorbis_analysis_wrote(&vd, siz);
                } else
                    vorbis_analysis_wrote(&vd, 0);
                while(vorbis_analysis_blockout(&vd, &vb) == 1) {
                    vorbis_analysis(&vb, NULL);
                    vorbis_bitrate_addblock(&vb);
                    while(vorbis_bitrate_flushpacket(&vd, &op)) {
                        ogg_stream_packetin(&os, &op);
                        while(ogg_stream_pageout(&os, &og)){
                            fwrite(og.header, 1, og.header_len, f);
                            fwrite(og.body, 1, og.body_len, f);
                            if(!ogg_page_eos(&og)) break;
                        }
                    }
                }
                ui_progressbar(0, 0,
#ifndef USE_DYNLIBS
                    /* only the latest development SDL2_mixer has Mix_GetMusicPosition function, not the shared lib ones */
                    (long int)(Mix_GetMusicPosition(music) * 100.0)
#else
                    1
#endif
                    , dur, MEDIA_IMPORT);
            } while(ret == AUBUFF);
            if(music->interface->Stop) music->interface->Stop(music->context);
            free(buf);
            ui_progressbar(0, 0, 0, 0, MEDIA_IMPORT);
            ret = 1;
        } else {
            ui_status(1, lang[ERR_LOADSND]);
            ret = 0;
        }
        while(ogg_stream_flush(&os, &og)) {
          fwrite(og.header, 1, og.header_len, f);
          fwrite(og.body, 1, og.body_len, f);
        }
        ogg_stream_clear(&os);
        vorbis_block_clear(&vb);
        vorbis_dsp_clear(&vd);
        vorbis_comment_clear(&vc);
        vorbis_info_clear(&vi);
        fclose(f);
        Mix_FreeMusic(music);
        Mix_CloseAudio();
        /* verify conversion */
        dur = theora_getduration(projdir);
        if(verbose) printf("media_import: verify length %u\n", dur);
        if(dur < 1) {
            ui_status(1, lang[ERR_LOADSND]);
            remove(projdir);
            ret = 0;
        }
    }
    *projfn = 0;
    return ret;
}

/**
 * Callback for SDL_mixer
 */
void media_callback(int channel)
{
    Mix_Chunk *audio;
    if(!channel) {
        audio = theora_audio(&theora_ctx);
        if(audio)
            Mix_PlayChannelTimed(channel, audio, 0, -1);
    }
}

/**
 * Play media file
 */
void media_play(char *path)
{
    char tmp[16];
    void *pixels;
    int wf = SDL_GetWindowFlags(window), w, h, i, pitch;
    Uint32 end = 0, now, *p;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *texture = NULL, *tm = NULL;
    SDL_Rect dstv, tmdst;
    Mix_Music *music = NULL;

    if(!path) return;
    if(verbose) printf("media_play: playing %s\n", path);

    if(Mix_OpenAudio(FREQ, AUDIO_S16SYS, 2, 4096) < 0) { ui_status(1, lang[ERR_AUDIO]); return; }
    end = theora_getduration(path) * 10;
    if(end < 1000 && (music = Mix_LoadMUS(path))) {
        Mix_VolumeMusic(MIX_MAX_VOLUME);
        Mix_PlayMusic(music, 0);
        while(Mix_PlayingMusic()) {
            SDL_PollEvent(&event);
            if(event.type==SDL_QUIT || (event.type==SDL_WINDOWEVENT && event.window.event==SDL_WINDOWEVENT_CLOSE)) {
                SDL_PushEvent(&event); break;
            }
            if(event.type == SDL_KEYUP || event.type == SDL_MOUSEBUTTONUP) break;
            SDL_Delay(10);
        }
        Mix_FreeMusic(music);
        Mix_CloseAudio();
        return;
    }

    theora_start(&theora_ctx, project_fopen(path, "rb"), NULL);
    if(theora_ctx.hasVideo) {
        SDL_GetDesktopDisplayMode(0, &dm);
        SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
        SDL_ShowCursor(SDL_DISABLE);
        SDL_RenderClear(renderer);
        SDL_RenderPresent(renderer);
        texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, theora_ctx.w, theora_ctx.h);
        if(!texture) goto cleanup;
        ui_fit(dm.w, dm.h, theora_ctx.w, theora_ctx.h, &dstv.w, &dstv.h);
        dstv.x = (dm.w - dstv.w) / 2; dstv.y = (dm.h - dstv.h) / 2;
        tmdst.x = dm.w - 282; tmdst.y = dm.h - 20;
    } else {
        tmdst.x = scr_w - 128 - 282; tmdst.y = scr_h - 48;
    }
    if(media_pbar || theora_ctx.hasVideo) {
        tm = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, 282, 20);
        if(!tm) goto cleanup;
        tmdst.w = 282, tmdst.h = 20;
    }

    /* audio */
    if(theora_ctx.hasAudio) {
        Mix_ChannelFinished(media_callback);
        media_callback(0);
    }

    theora_ctx.baseticks = SDL_GetTicks();
    while (theora_playing(&theora_ctx)) {
        now = SDL_GetTicks() - theora_ctx.baseticks;

        /* video */
        theora_video(&theora_ctx, texture);

        /* events */
        while (SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
                SDL_PushEvent(&event); goto cleanup;
            }
            if(event.type == SDL_KEYUP || event.type == SDL_MOUSEBUTTONDOWN) goto cleanup;
        }
        /* progressbar */
        if(tm) {
            SDL_LockTexture(tm, NULL, &pixels, &pitch);
            for(i = 0, p = pixels; i < tmdst.w * tmdst.h; i++) p[i] = theme[THEME_BG];
            w = pitch/4 * 4 + 76;
            for(h = 0; h < 12; h++)
                for(i = 0; i < 202; i++)
                    p[w + i + pitch / 4 * h] = theme[h == 0 || i == 0 ? THEME_DARKER : (h == 11 || i == 201 ? THEME_LIGHTER :
                        (!end || i <= (int)(202 * now / end) ? THEME_FG : THEME_INPBG))];
            SDL_UnlockTexture(tm);
            media_dur(now / 10, tmp);
            ui_text(tm, 4, 1, tmp);
        }
        /* render */
        if(texture) {
            SDL_RenderClear(renderer);
            SDL_RenderCopy(renderer, texture, NULL, &dstv);
        } else {
            ui_render();
        }
        if(tm) SDL_RenderCopy(renderer, tm, NULL, &tmdst);
        SDL_RenderPresent(renderer);
    }
cleanup:
    theora_ctx.stop = 1; /* do not allow any theora_audio calls after this point */
    SDL_ShowCursor(SDL_ENABLE);
    if(Mix_Playing(-1)) {
        Mix_ChannelFinished(NULL);
        Mix_HaltChannel(-1);
    }
    SDL_Delay(10);
    if(verbose) printf("media_play: stopped\n");
    theora_stop(&theora_ctx);
    if(tm) SDL_DestroyTexture(tm);
    if(texture) SDL_DestroyTexture(texture);
    Mix_CloseAudio();
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Save tng
 */
int media_totng(tngctx_t *ctx)
{
    uint32_t cl;
    int i;

    /* speech recordings are saved in translate_totng() because they are localized */
    if(!ctx) return 1;
    /* check if there's at least one music referenced */
    for(i = 0; i < project.nummusic && !ctx->idx[TNG_IDX_MUS].ref[i]; i++);
    if(project.music && i < project.nummusic) {
        tng_section(ctx, TNG_SECTION_MUSIC);
        for(i = 0; i < project.nummusic; i++) {
            if(!ctx->idx[TNG_IDX_MUS].ref[i]) continue;
            sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA], project.music[i]);
            cl = project_filesize(projdir);
            if(!cl) {
                ui_switchtab(SUBMENU_MUSIC);
                media_table.val = i;
                ui_status(1, lang[ERR_LOADSND]);
                return 0;
            }
            tng_desc(ctx, project.music[i], cl);
            tng_asset_ext(ctx, projdir, cl);
        }
    }
    /* check if there's at least one sound effect referenced */
    for(i = 0; i < project.numsounds && !ctx->idx[TNG_IDX_SND].ref[i]; i++);
    if(project.sounds && i < project.numsounds) {
        tng_section(ctx, TNG_SECTION_SOUNDS);
        for(i = 0; i < project.numsounds; i++) {
            if(!ctx->idx[TNG_IDX_SND].ref[i]) continue;
            sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 1], project.sounds[i]);
            cl = project_filesize(projdir);
            if(!cl) {
                ui_switchtab(SUBMENU_SOUNDS);
                media_table.val = i;
                ui_status(1, lang[ERR_LOADSND]);
                return 0;
            }
            tng_desc(ctx, project.sounds[i], cl);
            tng_asset_ext(ctx, projdir, cl);
        }
    }
    /* check if there's at least one movie referenced */
    for(i = 0; i < project.nummovies && !ctx->idx[TNG_IDX_MOV].ref[i]; i++);
    if(project.movies && i < project.nummovies) {
        tng_section(ctx, TNG_SECTION_MOVIES);
        for(i = 0; i < project.nummovies; i++) {
            if(!ctx->idx[TNG_IDX_MOV].ref[i]) continue;
            sprintf(projfn, "%s" SEP "%s" SEP "%s.ogv", project.id, project_dirs[PROJDIRS_MEDIA + 3], project.movies[i]);
            cl = project_filesize(projdir);
            if(!cl) {
                ui_switchtab(SUBMENU_MOVIES);
                media_table.val = i;
                ui_status(1, lang[ERR_LOADMOV]);
                return 0;
            }
            tng_desc(ctx, project.movies[i], cl);
            tng_asset_ext(ctx, projdir, cl);
        }
    }
    return 1;
}

/**
 * Read from tng
 */
int media_fromtng(tngctx_t *ctx)
{
    tng_asset_desc_t *asset;
    int i, j, len, l;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(j = 0; j < ctx->numtbl; j++)
        switch(TNG_SECTION_TYPE(&ctx->tbl[j])) {
            case TNG_SECTION_MUSIC:
                asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[j].offs);
                project.nummusic = len = TNG_SECTION_SIZE(&ctx->tbl[j]) / sizeof(tng_asset_desc_t);
                sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_MEDIA]);
                project_mkdir(projdir);
                project.music = (char**)main_alloc((len + 1) * sizeof(char*));
                for(i = 0; i < len; i++, asset++) {
                    l = strlen((char*)ctx->sts + asset->name);
                    project.music[i] = (char*)main_alloc(l + 1);
                    memcpy(project.music[i], ctx->sts + asset->name, l);
                    sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA], ctx->sts + asset->name);
                    if(verbose) printf("media_fromtng: saving %s\n", projdir);
                    tng_save_asset(ctx, asset->offs, asset->size, projdir);
                    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
                }
            break;
            case TNG_SECTION_SOUNDS:
                asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[j].offs);
                len = TNG_SECTION_SIZE(&ctx->tbl[j]) / sizeof(tng_asset_desc_t);
                sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_MEDIA + 1]);
                project_mkdir(projdir);
                for(i = 0; i < len; i++, asset++) {
                    sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 1],
                        ctx->sts + asset->name);
                    if(verbose) printf("media_fromtng: saving %s\n", projdir);
                    tng_save_asset(ctx, asset->offs, asset->size, projdir);
                    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
                }
            break;
            case TNG_SECTION_MOVIES:
                asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[j].offs);
                len = TNG_SECTION_SIZE(&ctx->tbl[j]) / sizeof(tng_asset_desc_t);
                sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_MEDIA + 3]);
                project_mkdir(projdir);
                for(i = 0; i < len; i++, asset++) {
                    sprintf(projfn, "%s" SEP "%s" SEP "%s.ogv", project.id, project_dirs[PROJDIRS_MEDIA + 3],
                        ctx->sts + asset->name);
                    if(verbose) printf("media_fromtng: saving %s\n", projdir);
                    tng_save_asset(ctx, asset->offs, asset->size, projdir);
                    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
                }
            break;
        }
    return 1;
}
