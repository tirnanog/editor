/*
 * tnge/newproj.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief New project window
 *
 */

#include "main.h"

void newproj_download(void *data);
void newproj_create(void *data);
void newproj_chksize(void *data);

char *projopts[] = {
    "\001  16 x  16",
    "\001  32 x  32",
    "\001  64 x  64",
    "\001 128 x 128",
    "\001 256 x 256",
    "\002  32 x  16",
    "\002  64 x  32",
    "\002 128 x  64",
    "\002 256 x 128",
/*
    "\003  16 x  16",
    "\003  32 x  32",
    "\003  64 x  64",
    "\003 128 x 128",
    "\003 256 x 256",
    "\004  16 x  16",
    "\004  32 x  32",
    "\004  64 x  64",
    "\004 128 x 128",
    "\004 256 x 256",
    "\005 3D",
*/
    NULL };
char *projmapsize[] = {
    "  8",
    " 16",
    " 32",
    " 64",
    "128",
    NULL };
char projid[17], projname[PROJ_TITLEMAX];

ui_input_t newproj_id = { INP_ID, 17, projid };
ui_input_t newproj_name = { INP_NAME, PROJ_TITLEMAX, projname };
ui_select_t newproj_temps = { 0, LANG_NONE, NULL };
ui_select_t newproj_tiles = { 0, 0, projopts };
ui_select_t newproj_maps = { 2, 0, projmapsize };

/**
 * The form
 */
ui_form_t newproj_form[] = {
    { FORM_TEXT, 20, 40, 0, 16, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 40, 160, 20, 0, NEWPROJ_IDSTAT, &newproj_id, NULL },
    { FORM_COMMENT, 0, 40, 280, 16, 0, 0, "(max 16, 0-9 a-z A-Z _ .)", NULL },
    { FORM_TEXT, 20, 70, 0, 16, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 70, 00, 20, 0, NEWPROJ_TITSTAT, &newproj_name, NULL },
    { FORM_TEXT, 20, 100, 0, 16, 0, 0, NULL, NULL },
    { FORM_SELECT, 0, 100, 100, 20, 0, NEWPROJ_TMPSTAT, &newproj_temps, NULL },
    { FORM_BUTTON, 0, 100, 0, 20, 0, 0, NULL, newproj_download },
    { FORM_TEXT, 20, 130, 0, 16, 0, 0, NULL, NULL },
    { FORM_SELECT, 0, 130, 122, 18, 0, 0, &newproj_tiles, newproj_chksize },
    { FORM_TEXT, 20, 154, 0, 16, 0, 0, NULL, NULL },
    { FORM_SELECT, 0, 154, 48, 18, 0, NEWPROJ_MAPSTAT, &newproj_maps, newproj_chksize },
    { FORM_BUTTON, 0, 0, 200, 20, 0, 0, NULL, newproj_create },
    { FORM_LAST }
};

/**
 * Check map and tile size
 */
void newproj_chksize(void *data)
{
    int i, j, k;

    (void)data;
    i = atoi(projopts[newproj_tiles.val] + 2);
    j = atoi(projopts[newproj_tiles.val] + 8);
    k = atoi(projmapsize[newproj_maps.val]);
    if(i * k > 4096 || j *k > 4096)
        ui_status(1, lang[NEWPROJ_TOOBIG]);
}

/**
 * Exit new project window
 */
void newproj_exit(int tab)
{
    (void)tab;
    project_freedir(&newproj_temps.opts, NULL);
}

/**
 * Enter new project window
 */
void newproj_init(int tab)
{
    int i, j, l;

    memset(projid, 0, sizeof(projid));
    memset(projname, 0, sizeof(projname));
    newproj_temps.val = -1; newproj_tiles.val = 1;
    newproj_exit(tab);
    *projfn = 0;
    newproj_temps.opts = project_getdir(projdir, ".zip", NULL);
    newproj_form[0].param = lang[NEWPROJ_GAMEID];
    newproj_form[3].param = lang[NEWPROJ_TITLE];
    newproj_form[5].param = lang[NEWPROJ_TEMPLATE];
    newproj_form[7].param = lang[NEWPROJ_DOWNLOAD];
    newproj_form[8].param = lang[NEWPROJ_TILES];
    newproj_form[10].param = lang[NEWPROJ_MAPSIZE];
    newproj_form[12].param = lang[NEWPROJ_CREATE];
    for(j = 0, i = NEWPROJ_GAMEID; i < NEWPROJ_DOWNLOAD; i++) {
        l = ui_textwidth(lang[i]) + 10;
        if(l > j) j = l;
    }
    newproj_form[1].x = newproj_form[4].x = newproj_form[6].x = newproj_form[9].x = newproj_form[11].x = 20 + j;
    newproj_form[6].w = 122;
    for(i = 0; newproj_temps.opts && newproj_temps.opts[i]; i++) {
        j = ui_textwidth(newproj_temps.opts[i]) + 24;
        if(j > newproj_form[6].w) newproj_form[6].w = j;
    }
}

/**
 * Resize the view
 */
void newproj_resize(int tab, int w, int h)
{
    (void)tab;
    newproj_form[2].x = newproj_form[1].x + newproj_form[1].w + 4;
    newproj_form[4].w = w - newproj_form[4].x - 20;
    newproj_form[7].x = newproj_form[6].x + newproj_form[6].w + 8;
    newproj_form[7].w = ui_textwidth(lang[NEWPROJ_DOWNLOAD]) + 16;
    newproj_form[12].w = ui_textwidth(lang[NEWPROJ_CREATE]) + 40;
    newproj_form[12].x = w - newproj_form[12].w - 10;
    newproj_form[12].y = h - newproj_form[12].h - 32;
}

/**
 * View layer
 */
void newproj_redraw(int tab)
{
    (void)tab;
    ui_form = newproj_form;
    newproj_form[9].inactive = newproj_temps.val != -1 && newproj_temps.opts;
    newproj_form[12].inactive = !projid[0] || !projname[0];
}

/**
 * Download button handler
 */
void newproj_download(void *data)
{
    char url[256];

    (void)data;
    sprintf(url, "%s#templates", site_url);
    main_open_url(url);
    /* browser starts asynchornuosly, so keep the button pressed for a while */
    SDL_Delay(250);
}

/**
 * Create button handler
 */
void newproj_create(void *data)
{
    (void)data;
    project_free();
    if(project_exists(projid)) {
        if(!ui_modal(ICON_WARN, ERR_EXISTS, projid)) return;
        project_delete(projid);
    }
    if(newproj_temps.val != -1 && newproj_temps.opts) {
        strcpy(projfn, newproj_temps.opts[newproj_temps.val]);
        strcat(projfn, ".zip");
        if(!project_fromtemplate(projid, projdir)) return;
        project_load(projid);
    } else {
        project.type = projopts[newproj_tiles.val][0] - 1;
        project.tilew = atoi(projopts[newproj_tiles.val] + 2);
        project.tileh = atoi(projopts[newproj_tiles.val] + 8);
    }
    project.mapsize = atoi(projmapsize[newproj_maps.val]);
    if(project.mapsize < 8 || project.mapsize > 512) project.mapsize = PROJ_MAPSIZE;
    memcpy(&project.id, &projid, 16);
    strncpy(project.name, projname, PROJ_TITLEMAX);
    project_save();
    ui_switchtab(SUBMENU_MAPS);
}

