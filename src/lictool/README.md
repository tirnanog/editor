TirNanoG Editor License Tool
============================

This small tool can generate `license.txt` files for TirNanoG Editor. Don't get your hopes up, you can't
compile it (there's no pro.c), and even if you could, it is absolutely useless in lack of my RSA keys.
