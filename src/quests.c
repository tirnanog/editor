/*
 * tnge/quests.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief quests window
 *
 */

#include "main.h"

void quests_load(void *data);
void quests_new(void *data);
void quests_save(void *data);
void quests_delete(void *data);
void quests_preview(void *data);

char quests_name[PROJ_NAMEMAX], quests_title[PROJ_TITLEMAX], quests_desc[PROJ_DESCMAX];
int quests_tab, quests_unique = 0;
ui_input_t quests_nameinp = { INP_ID, sizeof(quests_name), quests_name };
ui_input_t quests_titinp = { INP_NAME, sizeof(quests_title), quests_title };
ui_input_t quests_descinp = { INP_NAME, sizeof(quests_desc), quests_desc };
ui_cmdpal_t quests_pal = { CMDPAL_QUEST, 0, 0 };
ui_cmd_t quests_cmd = { 0 };

ui_tablehdr_t quests_hdr[] = {
    { SUBMENU_QUESTS, 0, 0, 0 },
    { 0 }
};
ui_table_t quests_tbl = { quests_hdr, QUESTS_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), NULL, NULL,
    NULL, NULL };

/**
 * The form
 */
ui_form_t quests_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, QUESTS_DELETE, (void*)ICON_REMOVE, quests_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, quests_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, QUESTS_PREVIEW, (void*)ICON_PVIEW, quests_preview },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, quests_new },
    { FORM_INPUT, 0, 30, 200, 18, 0, QUESTS_NAME, &quests_nameinp, NULL },
    { FORM_TABLE, 10, 30, 200, 0, 0, 0, &quests_tbl, quests_load },
    /* 6 */
    { FORM_BOOL, 0, 30, 0, 18, 0, QUESTS_UNIQUE, &quests_unique, NULL },
    /* 7 */
    { FORM_TEXT, 0, 55, 0, 18, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 54, 0, 18, 0, QUESTS_TITLE, &quests_titinp, NULL },
    /* 9 */
    { FORM_TEXT, 0, 79, 0, 18, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 78, 0, 18, 0, LANG_ATTRSTR, &quests_descinp, NULL },
    /* 11 */
    { FORM_COMMANDS, 0, 102, 0, 0, 0, QUESTS_ONCOMPLETE, &quests_cmd, NULL },
    { FORM_CMDPAL, 0, 102, 40, 0, 0, 0, &quests_pal, NULL },

    { FORM_LAST }
};

/**
 * Exit quests window
 */
void quests_exit(int tab)
{
    (void)tab;
    quests_tbl.clk = -1;
}

/**
 * Enter quests window
 */
void quests_init(int tab)
{
    int i;

    quests_exit(tab);
    quests_form[1].param = lang[LANG_SAVE];
    quests_form[1].w = ui_textwidth(quests_form[1].param) + 40;
    if(quests_form[1].w < 200) quests_form[1].w = 200;
    quests_form[7].param = lang[QUESTS_TITLE];
    i = quests_tab = ui_textwidth(quests_form[7].param); quests_form[7].w = i;
    quests_form[9].param = lang[QUESTS_DESC];
    i = ui_textwidth(quests_form[9].param); quests_form[9].w = i;
    if(i > quests_tab) quests_tab = i;
    quests_form[6].x = quests_form[7].x = quests_form[9].x = quests_form[11].x = quests_form[5].x + quests_form[5].w + 10;
    quests_form[8].x = quests_form[10].x = quests_form[6].x + quests_tab + 4;

    project_freedir(&project.quests, &project.numquests);
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_QUESTS]);
    project.quests = project_getdir(projdir, ".qst", &project.numquests);
    if(!quests_name[0]) quests_tbl.val = -1;
    quests_tbl.data = project.quests;
    quests_tbl.num = project.numquests;
    if(quests_tbl.val >= quests_tbl.num) quests_tbl.val = quests_tbl.num - 1;
}

/**
 * Resize the view
 */
void quests_resize(int tab, int w, int h)
{
    (void)tab;
    quests_form[0].y = quests_form[1].y = quests_form[2].y = quests_form[3].y = h - 48;
    quests_form[1].x = w - 20 - quests_form[1].w;
    quests_form[2].x = quests_form[1].x - 52;
    ui_table_resize(&quests_form[5], quests_form[5].w, h - 89);
    quests_form[3].x = quests_form[5].x + quests_form[5].w + 20;
    quests_form[4].x = w - 10 - quests_form[4].w;
    quests_form[6].w = quests_form[1].x - quests_form[6].x;
    quests_form[8].w = w - 10 - quests_form[8].x;
    quests_form[10].w = w - 10 - quests_form[10].x;
    quests_form[11].w = w - 52 - quests_form[11].x;
    quests_form[12].x = w - 48;
    quests_form[11].h = quests_form[12].h = h - 59 - quests_form[11].y;
}

/**
 * View layer
 */
void quests_redraw(int tab)
{
    (void)tab;
    ui_form = quests_form;
    ui_form[0].inactive = (quests_tbl.val < 0);
    ui_form[1].inactive = (!quests_name[0] || !quests_title[0]);
    ui_form[2].inactive = (!quests_title[0]);
}

/**
 * Clear form, new quest
 */
void quests_new(void *data)
{
    (void)data;
    quests_name[0] = quests_title[0] = quests_desc[0] = 0;
    quests_unique = 0;
}

/**
 * Delete quest
 */
void quests_delete(void *data)
{
    char **list = (char**)quests_tbl.data;

    (void)data;
    if(quests_tbl.val < 0 || !list) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.qst", project.id, project_dirs[PROJDIRS_QUESTS], list[quests_tbl.val]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("quests_delete: removing %s\n", projdir);
        remove(projdir);
        quests_new(NULL);
        quests_init(SUBMENU_QUESTS);
    }
}

/**
 * Save quest
 */
void quests_save(void *data)
{
    char **list = (char**)quests_tbl.data;
    FILE *f;

    (void)data;
    if(!quests_name[0] || !quests_title[0]) return;
    f = project_savefile(!list || quests_tbl.val < 0 || strcmp(list[quests_tbl.val], quests_name),
        project_dirs[PROJDIRS_QUESTS], quests_name, "qst", PROJMAGIC_QUEST, "quests_save");
    if(f) {
        fprintf(f, "%u\r\n%s\r\n%s\r\n", quests_unique, quests_title, quests_desc[0] ? quests_desc : "-");
        fprintf(f, "e complete ");
        project_wrscript(f, &quests_cmd);
        fclose(f);
        quests_init(SUBMENU_QUESTS);
        for(quests_tbl.val = 0; quests_tbl.val < quests_tbl.num &&
            strcmp(quests_name, project.quests[quests_tbl.val]); quests_tbl.val++);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.qst", project.id, project_dirs[PROJDIRS_QUESTS], quests_name);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVEQUEST]);
}

/**
 * Load quest
 */
int quests_loadqst(char *fn)
{
    char *str, *s;

    if(!fn || !*fn) return 0;
    str = project_loadfile(project_dirs[PROJDIRS_QUESTS], fn, "qst", PROJMAGIC_QUEST, "quests_loadqst");
    if(str) {
        s = project_skipnl(str);
        s = project_getint(s, &quests_unique, 0, 1);
        s = project_skipnl(s);
        s = project_getstr2(s, quests_title, 2, sizeof(quests_title));
        s = project_skipnl(s);
        s = project_getstr2(s, quests_desc, 2, sizeof(quests_desc));
        s = project_skipnl(s);
        s = project_getscript(s, &quests_cmd);
        free(str);
        strncpy(quests_name, fn, sizeof(quests_name) - 1);
        return 1;
    }
    return 0;
}

/**
 * Load quest
 */
void quests_load(void *data)
{
    char **list = (char**)quests_tbl.data;

    (void)data;
    if(quests_tbl.val < 0 || !list) return;
    if(!quests_loadqst(list[quests_tbl.val]))
        ui_status(1, lang[ERR_LOADQUEST]);
}

/**
 * Load strings for translation
 */
void quests_loadstr()
{
    char *str, *s;
    int i;

    for(i = 0; i < project.numquests; i++) {
        str = project_loadfile(project_dirs[PROJDIRS_QUESTS], project.quests[i], "qst", PROJMAGIC_QUEST, "quests_loadstr");
        if(str) {
            s = project_skipnl(str);
            s = project_skipnl(s);
            s = project_gettrstr(s, 2, sizeof(quests_title));
            s = project_skipnl(s);
            s = project_gettrstr(s, 2, sizeof(quests_desc));
            free(str);
        }
    }
}

/**
 * Preview quest
 */
void quests_preview(void *data)
{
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *bg = NULL, *texture = NULL;
    SDL_Rect win, dlg;
    char *str;
    int wf = SDL_GetWindowFlags(window), i, j, k, x, y, p, nl, right;
    uint32_t *pixels;
    ui_tablehdr_t hdr[] = {
        { GAME_INV, 0, 0, 0 },
        { GAME_SKILLS, 0, 0, 0 },
        { GAME_QUESTS, 0, 0, 0 },
        { 0 }
    };

    (void)data;
    if(verbose) printf("quests_preview: started\n");

    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

    elements_load();
    elements_loadfonts();
    elements_winsizes(hdr, 2, &win.w, &win.y, &right, &win.h, NULL);
    if(win.w < dm.w / 3) win.w = dm.w / 3;
    win.x = dm.w - win.w - right - 2; win.w -= 10;
    win.y += 32;
    win.h = dm.h * 3 / 4 - win.y - win.h;

    bg = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
    if(bg) {
        SDL_LockTexture(bg, NULL, (void**)&pixels, &p);
        /* checker */
        for(j = k = 0; j < dm.h; j++, k += p/4)
            for(i = 0; i < dm.w ; i++) {
                x = (i >> 5); y = (j >> 5);
                pixels[k + i] = theme[(y & 1) ^ (x & 1) ? THEME_LIGHT : THEME_DARK];
            }
        SDL_UnlockTexture(bg);
        elements_window(bg, dm.w, dm.h, win.x, win.y, win.w, win.h, hdr, 2);
        i = elements_checkbox(bg, win.x + elements_pad[4].val, win.y + elements_pad[3].val, 1);
        elements_text(bg, dm.w, dm.h, win.x + elements_pad[4].val + i + 8, win.y + elements_pad[3].val,
            win.w - 2 * elements_pad[4].val - i - 8, i + 8, quests_title, 3);
        elements_vscr(bg, dm.w - right - elements_pad[4].val, win.y + elements_pad[3].val,
            win.h - 2 * elements_pad[3].val, 1);
    }
    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
    if(texture) {
        SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
        memset(pixels, 0, p * dm.h);
        SDL_UnlockTexture(texture);
        str = elements_dlgstr(quests_desc, dm.w * 3 / 4, 0, 3, &nl);
        elements_dlgsize(dm.w, dm.h, nl, 0, &dlg.x, &dlg.y, &dlg.w, &dlg.h);
        elements_dialog(texture, dm.w, dm.h, dlg.x, dlg.y, dlg.w, dlg.h, quests_title, str, 0);
        if(str) free(str);
    }
    elements_freefonts();

    while(1) {
        /* events */
        SDL_WaitEvent(&event);
        if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
            SDL_PushEvent(&event); goto cleanup;
        }
        switch(event.type) {
            case SDL_KEYUP: goto cleanup; break;
            case SDL_MOUSEBUTTONUP: goto cleanup; break;
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        if(bg) SDL_RenderCopy(renderer, bg, NULL, NULL);
        if(texture) SDL_RenderCopy(renderer, texture, NULL, NULL);
        SDL_RenderPresent(renderer);
    }

cleanup:
    if(bg) SDL_DestroyTexture(bg);
    if(texture) SDL_DestroyTexture(texture);
    if(verbose) printf("quests_preview: stopped\n");
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Get references
 */
void quests_ref(tngctx_t *ctx, int idx)
{
    ui_cmd_t cmd = { 0 };
    char *str, *s;

    if(!tng_ref(ctx, TNG_IDX_QST, idx)) return;
    str = project_loadfile(project_dirs[PROJDIRS_QUESTS], project.quests[idx], "qst", PROJMAGIC_QUEST, "quests_ref");
    if(str) {
        s = project_skipnl(str);
        s = project_skipnl(s);
        s = project_skipnl(s);
        s = project_skipnl(s);
        s = project_getscript(s, &cmd);
        ui_cmd_ref(ctx, &cmd.root, 0);
        ui_cmd_free(&cmd);
        free(str);
    }
}

/**
 * Save tng
 */
int quests_totng(tngctx_t *ctx)
{
    ui_bc_t bc;
    uLongf cl;
    uint8_t *buf, *ptr, *comp;
    int j, l, err;

    if(!ctx || !project.quests || !project.numquests) return 1;
    /* check if there's at least one quest referenced */
    for(j = 0; j < project.numquests && !ctx->idx[TNG_IDX_QST].ref[j]; j++);
    if(j >= project.numquests) return 1;
    /* add quests to output */
    tng_section(ctx, TNG_SECTION_QUESTS);
    for(j = 0; j < project.numquests; j++) {
        if(!ctx->idx[TNG_IDX_QST].ref[j]) continue;
        if(!quests_loadqst(project.quests[j])) {
            ui_switchtab(SUBMENU_QUESTS);
            quests_tbl.val = j;
            ui_status(1, lang[ERR_LOADQUEST]);
            return 0;
        }
        memset(&bc, 0, sizeof(bc)); err = 0;
        if(quests_cmd.root.len) {
            ui_cmd_tobc(ctx, &bc, &quests_cmd.root, 0, &err);
            if(err) {
                if(bc.data) free(bc.data);
                ui_switchtab(SUBMENU_QUESTS);
                quests_tbl.val = j;
                ui_status(1, lang[ERR_SCRIPT]);
                return 0;
            }
        }
        l = 10 + bc.len;
        buf = ptr = (uint8_t*)main_alloc(l);
        *ptr++ = quests_unique;
        ptr = tng_asset_text(ptr, ctx, quests_title);
        ptr = tng_asset_text(ptr, ctx, quests_desc);
        memcpy(ptr, &bc.len, 3); ptr += 3;
        if(bc.data && bc.len) {
            memcpy(ptr, bc.data, bc.len);
            free(bc.data);
        }
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        if(cl) {
            tng_desc(ctx, project.quests[j], cl);
            tng_asset_int(ctx, comp, cl);
        } else
            free(comp);
        free(buf);
        /* free resources */
        quests_new(NULL);
    }
    return 1;
}

/**
 * Read from tng
 */
int quests_fromtng(tngctx_t *ctx)
{
    FILE *f;
    tng_asset_desc_t *asset;
    uint8_t *buf, *ptr;
    int i, j, l, len;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_QUESTS; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;
    for(j = 0; j < len; j++, asset++) {
        ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
        buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &l);
        if(buf) {
            if(!asset->name || asset->name >= TNG_SECTION_SIZE(&ctx->tbl[0]) || l < 10) { free(buf); continue; }
            f = project_savefile(0, project_dirs[PROJDIRS_QUESTS], (char*)ctx->sts + asset->name, "qst", PROJMAGIC_QUEST,
                "quests_fromtng");
            if(f) {
                fprintf(f, "%u\r\n", *ptr++);
                ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                l = 0; memcpy(&l, ptr, 3); ptr += 3;
                fprintf(f, "e complete ");
                tng_wrscript(ctx, buf, l, f);
                fclose(f);
            }
            free(buf);
        }
    }
    return 1;
}
