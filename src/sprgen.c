/*
 * tnge/sprgen.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Sprite Generator window
 *
 */

#include "main.h"

typedef struct {
    int idx, pal, enabled, w, h, o;
    int deplyr, depidx;
    char name[FILENAME_MAX];
    uint8_t *data;
    uint8_t *back;
} sprgen_lyropt_t;

void sprgen_new(void *data);
void sprgen_save(void *data);
void sprgen_sel(void *data);
void sprgen_search(void *data);
void sprgen_cellclk(void *data);
void sprgen_regen(void *data);
void sprgen_rotcw(void *data);
void sprgen_rotccw(void *data);
void sprgen_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

/* things from newsprite.c */
void newsprite_tngbase(SDL_Texture *src, int w, int h, int sp);
void newsprite_savesheet(void *data);
extern char newsprite_spr[FILENAME_MAX];
extern ui_table_t newsprite_table;
extern ui_input_t newsprite_sprinp;
int strnatcmp(const void *a, const void *b);

ui_tablehdr_t sprgen_hdr[] = {
    { -1, 20, 0, 0 },
    { SPRITES_GENOPTS, 0, 0, 0 },
    { 0 }
};
ui_table_t sprgen_tbl = { sprgen_hdr, LANG_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(sprgen_lyropt_t),
    sprgen_drawcell, NULL, NULL, NULL };

char sprgen_srch[PROJ_NAMEMAX] = { 0 };
SDL_Texture *sprgen_layer = NULL, *sprgen_portrait = NULL;
int sprgen_lyr, sprgen_gridsize, sprgen_numena, sprgen_rot;
ui_sprgrp_t sprgen_layers = { &sprgen_lyr, 0, 0, NULL };
ui_input_t sprgen_srchinp = { INP_ID, sizeof(sprgen_srch), sprgen_srch };
ui_clrver_t sprgen_clrver = { 0 };

/**
 * The form
 */
ui_form_t sprgen_form[] = {
    { FORM_INPUT, 0, 30, 200, 18, 0, SPRITES_NAME, &newsprite_sprinp, NULL },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, sprgen_save },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, sprgen_new },
    { FORM_SPRGRP, 0, 30, 0, 20, 0, 0, &sprgen_layers, sprgen_sel },
    { FORM_TABLE, 10, 54, 200, 0, 0, 0, &sprgen_tbl, sprgen_cellclk },
    { FORM_ICON, 26, 32, 16, 16, 0, 0, (void*)ICON_SRCH, NULL },
    { FORM_INPUT, 46, 30, 164, 20, 0, SPRITES_GENSRCH, &sprgen_srchinp, sprgen_search },
    { FORM_CLRVER, 14, 0, 15*12+10, 10, 0, SPRITES_CHOOSEVAR, &sprgen_clrver, sprgen_regen },
    { FORM_CHARBTN, 0, 0, 20, 20, 0, 0, (void*)10558, sprgen_rotcw },
    { FORM_CHARBTN, 0, 0, 20, 20, 0, 0, (void*)10559, sprgen_rotccw },

    { FORM_LAST }
};

/**
 * Draw (render) table cell
 */
void sprgen_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    sprgen_lyropt_t *opt = (sprgen_lyropt_t*)data, *opts = (sprgen_lyropt_t*)sprgen_tbl.data;

    (void)idx;
    (void)sel;
    (void)hdr;
    (void)w;
    (void)h;
    if(!data) return;
    ui_bool(dst, x + 6, y, 16, opts[opt->idx].enabled, NULL);
    ui_text(dst, x + 24, y + 1, opts[opt->idx].name);
}

/**
 * Click on a table cell
 */
void sprgen_cellclk(void *data)
{
    ui_table_t *tbl = (ui_table_t*)data;
    sprgen_lyropt_t *opt;
    int i, j, l;

    if(!data || !tbl->data || tbl->val < 0) return;
    opt = (sprgen_lyropt_t*)tbl->data;
    opt[opt[tbl->val].idx].enabled ^= 1;
    do {
        sprgen_numena = l = 0;
        for(j = 0; j < sprgen_layers.num; j++) {
            opt = sprgen_layers.elems[j].opts;
            for(i = 0; opt && i < sprgen_layers.elems[j].num; i++)
                if(opt[i].enabled) {
                    sprgen_numena++;
                    if(opt[i].depidx != -1 && !((sprgen_lyropt_t*)sprgen_layers.elems[opt[i].deplyr].opts)[opt[i].depidx].enabled) {
                        opt[i].enabled = 0; j = sprgen_layers.num; l++; break;
                    }
                }
        }
    } while(l);
    strcpy(ui_input_buf, sprgen_srch);
    sprgen_search(NULL);
    sprgen_regen(NULL);
}

/**
 * Regenerate textures
 */
void sprgen_regen(void *data)
{
    uint32_t *lyr, *prt, *cur;
    uint8_t *pal;
    int i, j, k, l, o, pl, pp;
    sprgen_lyropt_t *opt = (sprgen_lyropt_t*)sprgen_tbl.data;

    (void)data;
    if(!opt) return;
    ui_clip.x = ui_clip.y = 0;
    ui_clip.w = sprgen_gridsize * 18;
    ui_clip.h = sprgen_gridsize * 16;
    /* redraw character layer and portrait */
    if(sprgen_layer) {
        SDL_LockTexture(sprgen_layer, NULL, (void**)&lyr, &pl);
        memset(lyr, 0, pl * sprgen_gridsize * 16);
    }
    if(sprgen_portrait) {
        SDL_LockTexture(sprgen_portrait, NULL, (void**)&prt, &pp);
        memset(prt, 0, pp * sprgen_gridsize * 5);
    }
    /* first copy tools background layer, then everything else, lastly tools foreground */
    for(o = 0; o < 10; o++)
        for(j = -1; j < sprgen_layers.num + 1; j++) {
            if(j == 2) continue;
            l = j == sprgen_layers.num || j == -1 ? 2 : j;
            opt = sprgen_layers.elems[l].opts;
            for(i = 0; opt && i < sprgen_layers.elems[l].num; i++)
                if(opt[i].enabled && opt[i].o == o) {
                    cur = (uint32_t*)(j == -1 ? opt[i].back : opt[i].data);
                    if(cur) {
                        /* take the palette from the foreground if exists */
                        pal = (opt[i].data ? opt[i].data : opt[i].back) +
                            (opt[i].h - 16 + opt[i].pal) * opt[i].w * 4 + (opt[i].w - 8) * 4;
                        if(opt[i].h == sprgen_gridsize * 16) {
                            ui_combinewp(lyr, 0, 0, sprgen_gridsize * 18, sprgen_gridsize * 16, pl, cur, 0, 0,
                                sprgen_gridsize * 18, sprgen_gridsize * 16, sprgen_gridsize * 18 * 4, pal);
                            for(k = 0; k < 4; k++)
                                ui_combinewp(prt, k * sprgen_gridsize * 4, 0, sprgen_gridsize * 4, sprgen_gridsize * 5, pp,
                                    cur, sprgen_gridsize * 12, sprgen_gridsize * 11,
                                    sprgen_gridsize * 4, sprgen_gridsize * 5, sprgen_gridsize * 18 * 4, pal);
                        } else {
                            ui_combinewp(prt, 0, 0, sprgen_gridsize * 16, sprgen_gridsize * 5, pp, cur, 0, 0,
                                sprgen_gridsize * 16, sprgen_gridsize * 5, sprgen_gridsize * 16 * 4, pal);
                        }
                    }
                }
        }
    if(sprgen_layer) SDL_UnlockTexture(sprgen_layer);
    if(sprgen_portrait) {
        /* clear optional palette in bottom right corner */
        prt += (sprgen_gridsize * 5 - 16) * pp / 4 + sprgen_gridsize * 16 - 8;
        for(i = 0; i < 16; i++, prt += pp / 4)
            memset(prt, 0, 8 * 4);
        SDL_UnlockTexture(sprgen_portrait);
    }
}

/**
 * Option search box callback
 */
void sprgen_search(void *data)
{
    sprgen_lyropt_t *opt = (sprgen_lyropt_t*)sprgen_tbl.data;
    int i, j, l, k;

    (void)data;
    sprgen_tbl.num = 0;
    if(!ui_input_buf[0]) {
        for(i = j = 0; i < sprgen_layers.elems[sprgen_lyr].num; i++) {
            if(opt[i].depidx == -1 || ((sprgen_lyropt_t*)sprgen_layers.elems[opt[i].deplyr].opts)[opt[i].depidx].enabled)
                opt[sprgen_tbl.num++].idx = i;
        }
    } else {
        sprgen_tbl.val = sprgen_tbl.scr = 0; l = strlen(ui_input_buf);
        for(j = 0; j < sprgen_layers.elems[sprgen_lyr].num; j++) {
            if(opt[j].depidx != -1 && !((sprgen_lyropt_t*)sprgen_layers.elems[opt[j].deplyr].opts)[opt[j].depidx].enabled)
                continue;
            k = strlen(opt[j].name);
            if(k < l) continue;
            k -= l;
            for(i = 0; i <= k && strncasecmp(opt[j].name + i, ui_input_buf, l); i++);
            if(i > k) continue;
            opt[sprgen_tbl.num++].idx = j;
        }
    }
    if(sprgen_tbl.val >= sprgen_tbl.num) sprgen_tbl.val = sprgen_tbl.num - 1;
}

/**
 * Exit sprite generator window
 */
void sprgen_exit(int tab)
{
    sprgen_lyropt_t *opt;
    int i, j;

    (void)tab;
    if(sprgen_layers.elems) {
        for(i = 0; i < sprgen_layers.num; i++)
            if(sprgen_layers.elems[i].opts) {
                opt = sprgen_layers.elems[i].opts;
                for(j = 0; j < sprgen_layers.elems[i].num; j++) {
                    if(opt[j].data) free(opt[j].data);
                    if(opt[j].back) free(opt[j].back);
                }
                free(sprgen_layers.elems[i].opts);
            }
        free(sprgen_layers.elems); sprgen_layers.elems = NULL;
    }
    sprgen_layers.num = 0;
    if(sprgen_layer) { SDL_DestroyTexture(sprgen_layer); sprgen_layer = NULL; }
    if(sprgen_portrait) { SDL_DestroyTexture(sprgen_portrait); sprgen_portrait = NULL; }
}

/**
 * Enter sprite generator window
 */
void sprgen_init(int tab)
{
    sprgen_lyropt_t *opt, *opt2;
    char **files, *fn, *c, *e;
    int i, j, k, l = 0, n, o, w, h, num;
    uint8_t *data;

    sprgen_exit(tab);
    sprgen_form[1].param = lang[FILEOP_IMPORT];
    sprgen_form[1].w = ui_textwidth(sprgen_form[1].param) + 40;
    if(sprgen_form[1].w < 200) sprgen_form[1].w = 200;
    sprgen_layers.elems = (ui_sprgrpelem_t*)main_alloc(4 * sizeof(ui_sprgrpelem_t));
    strcpy(sprgen_layers.elems[0].name, "body"); sprgen_layers.elems[0].icon = ICON_BODY;
    strcpy(sprgen_layers.elems[1].name, "head"); sprgen_layers.elems[1].icon = ICON_HEAD;
    strcpy(sprgen_layers.elems[2].name, "toolf"); sprgen_layers.elems[2].icon = ICON_TOOLS;
    sprgen_layers.num = 3;
    sprgen_gridsize = 0;
    strcpy(projfn, "." COMMONDIR);
    files = project_getdir(projdir, ".png", &num);
    fn = projfn + strlen(COMMONDIR) + 1;
    *fn++ = SEP[0];
    /* quick fix, foreground tools must be read first, background layers only afterwards */
    for(i = 0; files && files[i] && i < num; i++)
        if(!memcmp(files[i], "toolb_", 6)) files[i][4] = 'g';
    qsort(files, num, sizeof(char*), strnatcmp);
    for(i = 0; files && files[i] && i < num; i++)
        if(!memcmp(files[i], "toolg_", 6)) files[i][4] = 'b';

    /* read in images */
    for(i = 0; files && files[i] && i < num; i++) {
        ui_progressbar(0, 0, i, num, LANG_LOADING);
        strcpy(fn, files[i]); strcat(fn, ".png");
        if(verbose > 1) printf("sprgen_init: loading %s\r\n", projdir);
        data = image_load(projdir, &w, &h);
        if(data) {
            if(w == 16 && h == 16) {
                /* layer icon */
                for(j = 0; j < sprgen_layers.num && strcmp(sprgen_layers.elems[j].name, files[i]); j++);
                if(j >= sprgen_layers.num && memcmp(files[i], "toolb", 5)) {
                    j = sprgen_layers.num++;
                    sprgen_layers.elems = (ui_sprgrpelem_t*)realloc(sprgen_layers.elems,
                        sprgen_layers.num * sizeof(ui_sprgrpelem_t));
                    if(!sprgen_layers.elems) main_error(ERR_MEM);
                    memset(&sprgen_layers.elems[j], 0, sizeof(ui_sprgrpelem_t));
                    strcpy(sprgen_layers.elems[j].name, files[i]);
                    memcpy(sprgen_layers.elems[j].spr, data, 256 * sizeof(uint32_t));
                }
                free(data);
            } else {
                /* character layer or portrait layer */
                if(!memcmp(files[i], "toolb_", 6) || !memcmp(files[i], "toolf_", 6)) {
                    j = 2; l = 6;
                } else {
                    for(j = 0; j < sprgen_layers.num; j++) {
                        l = strlen(sprgen_layers.elems[j].name);
                        if(!memcmp(sprgen_layers.elems[j].name, files[i], l) && files[i][l] == '_') { l++; break; }
                    }
                }
                if(files[i][l] >= '0' && files[i][l] <= '9') { o = files[i][l] - '0'; l++; } else o = 0;
                if(files[i][l] == '_') l++;
                if(j < sprgen_layers.num) {
                    k = sprgen_layers.elems[j].num; opt = NULL;
                    if(j == 2 && files[i][4] != 'f') {
                        opt = sprgen_layers.elems[2].opts;
                        c = strrchr(files[i] + l, '(');
                        if(c) n = c - files[i] - l - (c[-1] == '_' ? 1 : 0); else n = strlen(files[i] + l);
                        for(k = 0; opt && k < sprgen_layers.elems[2].num &&
                            (memcmp(opt[k].name, files[i] + l, n) || opt[k].name[n]); k++);
                        if(k < sprgen_layers.elems[j].num) {
                            opt[k].back = data;
                            continue;
                        }
                    }
                    k = sprgen_layers.elems[j].num++;
                    sprgen_layers.elems[j].opts = opt = (sprgen_lyropt_t*)realloc(sprgen_layers.elems[j].opts,
                        sprgen_layers.elems[j].num * sizeof(sprgen_lyropt_t));
                    if(opt) {
                        memset(&opt[k], 0, sizeof(sprgen_lyropt_t));
                        strcpy(opt[k].name, files[i] + l);
                        opt[k].idx = k;
                        opt[k].w = w;
                        opt[k].h = h;
                        opt[k].o = o;
                        opt[k].deplyr = opt[k].depidx = -1;
                        if(j == 2 && files[i][4] != 'f')
                            opt[k].back = data;
                        else
                            opt[k].data = data;
                        if(!j && !sprgen_gridsize) {
                            sprgen_gridsize = h / 16;
                            if(sprgen_gridsize < 8) sprgen_gridsize = 8;
                        }
                    } else {
                        sprgen_layers.elems[j].num = 0;
                        free(data);
                    }
                } else
                    free(data);
            }
        }
    }
    /* parse dependencies */
    for(j = 0; j < sprgen_layers.num; j++) {
        opt = sprgen_layers.elems[j].opts;
        for(i = 0; opt && i < sprgen_layers.elems[j].num; i++) {
            c = strrchr(opt[i].name, '(');
            e = strrchr(opt[i].name, ')');
            if(c && e && c < e) {
                if(c > opt[i].name && c[-1] == '_') c[-1] = 0;
                *c++ = 0; *e = 0; n = e - c;
                for(l = 0; l < sprgen_layers.num; l++) {
                    opt2 = sprgen_layers.elems[l].opts;
                    for(k = 0; opt2 && k < sprgen_layers.elems[l].num; k++) {
                        if(!memcmp(c, opt2[k].name, n) && (!opt2[k].name[n] ||
                          (opt2[k].name[n] == '_' && opt2[k].name[n + 1] == '('))) {
                            opt[i].deplyr = l; opt[i].depidx = k;
                            l = sprgen_layers.num; break;
                        }
                    }
                }
            }
        }
    }
    /* create output textures */
    if(sprgen_gridsize > 0) {
        w = sprgen_gridsize * 18;
        h = sprgen_gridsize * 16;
        sprgen_layer = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, w, h);
        if(sprgen_layer) SDL_SetTextureBlendMode(sprgen_layer, SDL_BLENDMODE_BLEND);
        w = sprgen_gridsize * 16;
        h = sprgen_gridsize * 5;
        sprgen_portrait = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, w, h);
        if(sprgen_portrait) SDL_SetTextureBlendMode(sprgen_portrait, SDL_BLENDMODE_BLEND);
    }
    sprgen_srch[0] = ui_input_buf[0] = 0;
    sprgen_new(NULL);
    sprgen_search(NULL);
    ui_progressbar(0, 0, 0, 0, LANG_LOADING);
    project_freedir(&files, NULL);
    sprgen_lyr = 0;
    sprgen_sel(NULL);
    ui_anim_cnt = 0;
}

/**
 * Select a layer
 */
void sprgen_sel(void *data)
{
    (void)data;
    if(sprgen_lyr < sprgen_layers.num) {
        sprgen_tbl.num = sprgen_layers.elems[sprgen_lyr].num;
        sprgen_tbl.data = sprgen_layers.elems[sprgen_lyr].opts;
    } else {
        sprgen_tbl.num = 0;
        sprgen_tbl.data = NULL;
    }
    sprgen_tbl.clk = sprgen_tbl.val = sprgen_tbl.scr = 0;
    sprgen_srch[0] = ui_input_buf[0] = 0;
    sprgen_search(NULL);
}

/**
 * Clear form, new sprite
 */
void sprgen_new(void *data)
{
    sprgen_lyropt_t *opt;
    int i, j;

    for(j = 0; j < sprgen_layers.num; j++) {
        opt = sprgen_layers.elems[j].opts;
        for(i = 0; opt && i < sprgen_layers.elems[j].num; i++)
            opt[i].enabled = opt[i].pal = 0;
    }
    sprgen_numena = sprgen_rot = sprgen_lyr = 0;
    sprgen_sel(NULL);
    memset(newsprite_spr, 0, sizeof(newsprite_spr));
    if(sprgen_layer) {
        SDL_LockTexture(sprgen_layer, NULL, (void**)&data, &i);
        memset(data, 0, i * sprgen_gridsize * 16);
        SDL_UnlockTexture(sprgen_layer);
    }
    if(sprgen_portrait) {
        SDL_LockTexture(sprgen_portrait, NULL, (void**)&data, &i);
        memset(data, 0, i * sprgen_gridsize * 5);
        SDL_UnlockTexture(sprgen_portrait);
    }
}

/**
 * Rotate preview clock-wise
 */
void sprgen_rotcw(void *data)
{
    (void)data;
    if(sprgen_rot < 3) sprgen_rot++; else sprgen_rot = 0;
}

/**
 * Rotate preview counter clock-wise
 */
void sprgen_rotccw(void *data)
{
    (void)data;
    if(sprgen_rot > 0) sprgen_rot--; else sprgen_rot = 3;
}

/**
 * Save sprites
 */
void sprgen_save(void *data)
{
    FILE *f;
    uint8_t *src, *dst, *s, *d;
    char *prtsfx[] = { "", "_happy", "_sad", "_angry" }, fn[FILENAME_MAX + PROJ_NAMEMAX + 12], *e;
    int i, j, p, p2 = sprgen_gridsize * 4 * 4, l = sprgen_gridsize * 4 * sprgen_gridsize * 5;

    (void)data;
    ui_progressbar(0, 0, 0, 100, SPRITES_SAVING);
    /* save portraits */
    if(sprgen_portrait) {
        dst = (uint8_t*)main_alloc(l * 4);
        SDL_LockTexture(sprgen_portrait, NULL, (void**)&src, &p);
        for(i = 0; i < 4; i++) {
            for(j = 0, d = dst, s = src + i * p2; j < sprgen_gridsize * 5; j++, s += p, d += p2)
                memcpy(d, s, p2);
            for(j = 0; j < l && !dst[j * 4 + 3]; j++);
            if(j >= l) continue;
            if(i) {
                for(j = 0, d = dst, s = src; j < sprgen_gridsize * 5 && !memcmp(s, d, p2); j++, s += p, d += p2);
                if(j >= sprgen_gridsize * 5) continue;
            }
            e = fn + sprintf(fn, "%s%s", newsprite_spr, prtsfx[i]);
            strcat(fn, "_soo0");
            f = project_savefile(0, project_dirs[PROJDIRS_SPRITES + 3], fn, "png", NULL, "sprgen_save");
            *e = 0;
            if(f) {
                if(image_save(f, sprgen_gridsize * 4, sprgen_gridsize * 5, p2, dst))
                    spr_add(3, fn, 0, 0, 1, sprgen_gridsize * 4, sprgen_gridsize * 5, p2, dst);
                fclose(f);
            }
        }
        free(dst);
        SDL_UnlockTexture(sprgen_portrait);
    }
    /* save character animations */
    if(sprgen_layer) {
        newsprite_tngbase(sprgen_layer, sprgen_gridsize * 18, sprgen_gridsize * 16, 0);
        newsprite_savesheet(NULL);
        newsprite_empty();
    }
    ui_progressbar(0, 0, 0, 0, SPRITES_SAVING);
    strcpy(projfn, newsprite_spr);
    ui_status(0, lang[LANG_SAVED]);
    newsprite_spr[0] = 0;
}

/**
 * Resize the view
 */
void sprgen_resize(int tab, int w, int h)
{
    (void)tab;

    sprgen_form[0].x = w - 10 - sprgen_form[0].w;
    sprgen_form[1].y = sprgen_form[2].y = sprgen_form[7].y = sprgen_form[8].y = sprgen_form[9].y = h - 48;
    sprgen_form[1].x = w - 20 - sprgen_form[1].w;
    ui_table_resize(&sprgen_form[4], sprgen_form[4].w, h - 89 - 24);
    sprgen_form[2].x = sprgen_form[3].x = sprgen_form[4].x + sprgen_form[4].w + 20;
    sprgen_form[8].x = sprgen_form[2].x + (sprgen_form[0].x - sprgen_form[2].x) / 2 - sprgen_form[8].w - 10;
    sprgen_form[9].x = sprgen_form[2].x + (sprgen_form[0].x - sprgen_form[2].x) / 2 + 10;
    sprgen_form[3].w = sprgen_layers.num * 24 - 4;
    if(sprgen_form[3].x + sprgen_form[3].w > sprgen_form[0].x - 10)
        sprgen_form[3].w = sprgen_form[0].x - 10 - sprgen_form[3].x;
}

/**
 * View layer
 */
void sprgen_redraw(int tab)
{
    int i;
    sprgen_lyropt_t *opt;

    (void)tab;
    ui_form = sprgen_form;
    ui_form[1].inactive = (!newsprite_spr[0] || sprgen_numena < 1 || !sprgen_layer || !sprgen_portrait);
    ui_form[7].inactive = 1; memset(&sprgen_clrver, 0, sizeof(sprgen_clrver));
    if(sprgen_tbl.val >= 0 && sprgen_tbl.data) {
        opt = (sprgen_lyropt_t *)sprgen_tbl.data;
        opt = &opt[opt[sprgen_tbl.val].idx];
        i = (((opt->h - 16) * opt->w + (opt->w - 8))) * 4;
        if(opt->enabled && opt->data && opt->w > 8 && opt->h > 16 && opt->data[i + 3] && (*((uint32_t*)&opt->data[i]) & 0xffffff)) {
            sprgen_clrver.val = &opt->pal;
            sprgen_clrver.pal = (uint32_t*)&opt->data[i];
            sprgen_clrver.p = opt->w * 4;
            ui_form[7].inactive = 0;
        }
    }
}

/**
 * Render additional view
 */
void sprgen_render(int tab)
{
    int h;
    SDL_Rect src, rect;

    (void)tab;
    if(sprgen_layer) {
        src.w = src.h = sprgen_gridsize;
        rect.x = sprgen_form[3].x; rect.y = sprgen_form[3].y + 30;
        ui_fit((scr_w - 10 - rect.x - 5 * 8) / 6, (scr_h / 2 - rect.y - 16) / 2, sprgen_gridsize, sprgen_gridsize,
            &rect.w, &rect.h);
        if(rect.w > sprgen_gridsize) rect.w = rect.h = sprgen_gridsize;
        /* first row: in every sheet in original LPC, all these anims should work for all assets */
        /* die */
        src.y = 10 * sprgen_gridsize;
        src.x = 12 * sprgen_gridsize + ui_anim(0, 6, -1) * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);
        rect.x += rect.w + 8;
        /* walk */
        src.x = ui_anim(2, 6, -1) * sprgen_gridsize;
        src.y = (sprgen_rot + 4) * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);
        rect.x += rect.w + 8;
        /* slash */
        src.x += 6 * sprgen_gridsize;
        src.y = sprgen_rot * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);
        rect.x += rect.w + 8;
        /* thrust */
        src.y += 4 * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);
        rect.x += rect.w + 8;
        /* bowing */
        src.y += 4 * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);
        rect.x += rect.w + 8;
        /* cast a spell */
        src.y += 4 * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);

        /* second row: not in every original LPC, probably most assets broken and need manual fixing */
        rect.x = sprgen_form[3].x; rect.y += 8 + rect.h;
        /* spawn */
        src.y = 9 * sprgen_gridsize;
        src.x = 12 * sprgen_gridsize + ui_anim(0, 6, -1) * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);
        rect.x += rect.w + 8;
        /* climb */
        src.y = 8 * sprgen_gridsize;
        src.x = 12 * sprgen_gridsize + ui_anim(2, 6, -1) * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);
        rect.x += rect.w + 8;
        /* push */
        src.y = sprgen_rot * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);
        rect.x += rect.w + 8;
        /* carry */
        src.y += 4 * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);
        rect.x += rect.w + 8;
        /* run */
        src.x = ui_anim(2, 6, -1) * sprgen_gridsize;
        src.y = (sprgen_rot + 8) * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);
        rect.x += rect.w + 8;
        /* jump */
        src.y += 4 * sprgen_gridsize;
        SDL_RenderCopy(renderer, sprgen_layer, &src, &rect);
    }
    if(sprgen_portrait) {
        rect.x = sprgen_form[3].x;
        h = (scr_h / 2 - 60); if(h > sprgen_gridsize * 5) h = sprgen_gridsize * 5;
        ui_fit(scr_w - 10 - rect.x, h, sprgen_gridsize * 16, sprgen_gridsize * 5, &rect.w, &rect.h);
        rect.y = scr_h - 64 - rect.h;
        SDL_RenderCopy(renderer, sprgen_portrait, NULL, &rect);
    }
}

/**
 * Controller
 */
int sprgen_ctrl(int tab)
{
    (void)tab;
    switch(event.type) {
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym) {
                case SDLK_LEFT: sprgen_rotcw(NULL); break;
                case SDLK_RIGHT: sprgen_rotccw(NULL); break;
            }
        break;
        case SDL_MOUSEBUTTONDOWN:
            if(event.button.x >= sprgen_form[3].x &&  event.button.y >= sprgen_form[3].y + 30 && event.button.y < scr_h / 2) {
                if(sprgen_rot > 0) sprgen_rot--; else sprgen_rot = 3;
            }
        break;
    }
    return 0;
}
