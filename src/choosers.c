/*
 * tnge/choosers.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Route choosers window
 *
 */

#include "main.h"

#define REQLEN 64

typedef struct {
    int pos;
    int x1, y1, x2, y2;
    int rtype[2], rattr[2], rval[2];
    int keep;
    int ptype, pattr, pval;
    SDL_Rect d;
} choosers_opt_t;

void choosers_load(void *data);
void choosers_clk(void *data);
void choosers_up(void *data);
void choosers_down(void *data);
void choosers_erase(void *data);
void choosers_add(void *data);
void choosers_new(void *data);
void choosers_save(void *data);
void choosers_delete(void *data);
void choosers_preview(void *data);
void choosers_helper(void *data);
void choosers_drawopt(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void choosers_renderopt(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

SDL_Texture *choosers_anims[3] = { 0 };
int choosers_keep = 0;
char choosers_name[PROJ_NAMEMAX];
ui_input_t choosers_nameinp = { INP_ID, sizeof(choosers_name), choosers_name };
char *choosers_posopts[] = { "\xc2\xa3", "\xc2\xa4", "\xc2\xa5", "\xc2\xa6", "\xc2\xa7", "\xc2\xa8", "\xc2\xa9",
    "\xc2\xaa", "\xc2\xab", "\xc2\xac", "\xc2\xad", "\xc2\xae", "\xc2\xaf", "\x19", NULL };
ui_select_t choosers_possel = { 0, 0, choosers_posopts };
ui_num_t choosers_x1 = { 0, 0, 99, 1 };
ui_num_t choosers_y1 = { 0, 0, 99, 1 };
ui_num_t choosers_x2 = { 1, 1, 100, 1 };
ui_num_t choosers_y2 = { 1, 1, 100, 1 };
ui_sprsel_t choosers_imgn = { -1, 0, 25, 1 };
ui_sprsel_t choosers_imgs = { -1, 0, 25, 1 };
ui_sprsel_t choosers_imgp = { -1, 0, 25, 1 };
ui_cmdvar_t choosers_req[] = { { 2, 0, { 0, 0, NULL } }, { 2, 0, { 0, 0, NULL } } };
ui_cmdvar_t choosers_prov = { 2, 0, { 0, 0, NULL } };
ui_num_t choosers_rval[] = { { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 }, { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 } };
ui_num_t choosers_pval = { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 };
ui_num_t choosers_numl = { 0, 0, 512, 1 };
ui_num_t choosers_numt = { 0, 0, 512, 1 };
ui_num_t choosers_numH = { 0, 0, 512, 1 };
ui_num_t choosers_numV = { 0, 0, 512, 1 };
ui_sprsel_t choosers_help = { -1, 5, 25, 1 };

ui_tablehdr_t choosers_hdr[] = {
    { SUBMENU_CHOOSERS, 0, 0, 0 },
    { 0 }
};
ui_table_t choosers_tbl = { choosers_hdr, CHOOSERS_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), NULL, NULL,
    NULL, NULL };

ui_tablehdr_t choosers_opthdr[] = {
    { CHOOSERS_POS, 80, 0, 0 },
    { CHOOSERS_IMAGE, 110, 0, 0 },
    { CHOOSERS_REQUIRE, 0, 0, 0 },
    { CHOOSERS_PROVIDE, 0, 0, 0 },
    { 0 }
};
ui_table_t choosers_opts = { choosers_opthdr, CHOOSERS_NOITEM, 36, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(choosers_opt_t),
    choosers_drawopt, choosers_renderopt, NULL, NULL };

/**
 * The form
 */
ui_form_t choosers_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, CHOOSERS_DELETE, (void*)ICON_REMOVE, choosers_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, choosers_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, CHOOSERS_PREVIEW, (void*)ICON_PVIEW, choosers_preview },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, choosers_new },
    { FORM_INPUT, 0, 30, 200, 18, 0, LANG_NAME, &choosers_nameinp, NULL },
    { FORM_TABLE, 10, 30, 200, 0, 0, 0, &choosers_tbl, choosers_load },
    /* 6 */
    { FORM_TABLE, 0, 54, 0, 0, 0, 0, &choosers_opts, choosers_clk },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHOOSERS_ERASE, (void*)ICON_REMOVE, choosers_erase },
    { FORM_SELECT, 0, 0, 40, 20, 0, CHOOSERS_POS, &choosers_possel, NULL },
    /* 9 */
    { FORM_NUM, 0, 0, 42, 20, 0, CHOOSERS_STRPOS, &choosers_x1, NULL },
    { FORM_TEXT, 0, 0, 8, 20, 0, 0, "%", NULL },
    { FORM_NUM, 0, 0, 42, 20, 0, CHOOSERS_STRPOS, &choosers_y1, NULL },
    { FORM_TEXT, 0, 0, 32, 20, 0, 0, "% - ", NULL },
    { FORM_NUM, 0, 0, 42, 20, 0, CHOOSERS_ENDPOS, &choosers_x2, NULL },
    { FORM_TEXT, 0, 0, 8, 20, 0, 0, "%", NULL },
    { FORM_NUM, 0, 0, 42, 20, 0, CHOOSERS_ENDPOS, &choosers_y2, NULL },
    { FORM_TEXT, 0, 0, 16, 20, 0, 0, "%", NULL },
    { FORM_CHARBTN, 0, 0, 20, 20, 0, CHOOSERS_HELPER, (void*)162, choosers_helper },
    { FORM_SPRITE, 0, 0, 20, 20, 0, CHOOSERS_HELPER, &choosers_help, NULL },
    /* 19 */
    { FORM_SPRITE, 0, 0, 0, 20, 0, CHOOSERS_NORMAL, &choosers_imgn, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, CHOOSERS_SELECTED, &choosers_imgs, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, CHOOSERS_PRESSED, &choosers_imgp, NULL },
    { FORM_CMDVAR, 0, 0, 18, 18, 0, CHOOSERS_REQUIRE, &choosers_req[0], NULL },
    { FORM_TEXT, 0, 0, 8, 20, 0, CHOOSERS_REQUIRE, "=", NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, CHOOSERS_REQUIRE, &choosers_rval[0], NULL },
    { FORM_TEXT, 0, 0, 8, 20, 0, CHOOSERS_REQUIRE, "&", NULL },
    { FORM_CMDVAR, 0, 0, 18, 18, 0, CHOOSERS_REQUIRE, &choosers_req[1], NULL },
    { FORM_TEXT, 0, 0, 8, 20, 0, CHOOSERS_REQUIRE, ">", NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, CHOOSERS_REQUIRE, &choosers_rval[1], NULL },
    /* 29 */
    { FORM_BOOL, 0, 0, 16, 18, 0, CHOOSERS_KEEP, &choosers_keep, NULL },
    { FORM_CMDVAR, 0, 0, 18, 18, 0, CHOOSERS_PROVIDE, &choosers_prov, NULL },
    { FORM_TEXT, 0, 0, 16, 20, 0, CHOOSERS_PROVIDE, ":=", NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, CHOOSERS_PROVIDE, &choosers_pval, NULL },
    /* 33 */
    { FORM_CHARBTN, 0, 0, 20, 20, 0, 0, (void*)11014, choosers_up },
    { FORM_CHARBTN, 0, 0, 20, 20, 0, 0, (void*)11015, choosers_down },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CHOOSERS_ADD, (void*)ICON_ADD, choosers_add },
    /* 36 */
    { FORM_TEXT, 0, 31, 16, 16, 0, 0, "\x1C", NULL },
    { FORM_NUM, 0, 29, 40, 18, 0, CHOOSERS_MARH, &choosers_numl, NULL },
    { FORM_TEXT, 0, 31, 16, 16, 0, 0, "\x1D", NULL },
    { FORM_NUM, 0, 29, 40, 18, 0, CHOOSERS_MARV, &choosers_numt, NULL },
    { FORM_TEXT, 0, 31, 16, 16, 0, 0, "\x1E", NULL },
    { FORM_NUM, 0, 29, 40, 18, 0, CHOOSERS_PADH, &choosers_numH, NULL },
    { FORM_TEXT, 0, 31, 16, 16, 0, 0, "\x1F", NULL },
    { FORM_NUM, 0, 29, 40, 18, 0, CHOOSERS_PADV, &choosers_numV, NULL },

    { FORM_LAST }
};

/**
 * Draw table cell
 */
void choosers_drawopt(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    choosers_opt_t *opt = (choosers_opt_t*)data;
    ui_sprite_t *s;
    SDL_Rect rect;
    char tmp[128];
    int t = ui_clip.w, i, j;

    if(!data) return;
    ui_text(dst, x + 20, y + (h - 16) / 2, choosers_posopts[opt->pos]);
    if(opt->pos == 13) {
        sprintf(tmp, "(%u%%,%u%%) - (%u%%,%u%%)", opt->x1, opt->y1, opt->x2, opt->y2);
        ui_clip.w = x + hdr[0].w + hdr[1].w;
        ui_text(dst, x + hdr[0].w + 4, y + (h - 16) / 2, tmp);
    } else {
        rect.x = x + hdr[0].w + 4; rect.y = y + 1;
        j = (hdr[1].w) / 3 - 4;
        s = spr_getidx(choosers_imgn.cat, opt->x1, 0, 1);
        if(idx != sel) {
            ui_fit(j, h - 4, s->w, s->h, &rect.w, &rect.h);
            spr_blit(s, 160, dst, &rect);
        } else
            spr_texture(s, &choosers_anims[0]);
        rect.x += 4 + j;
        s = spr_getidx(choosers_imgs.cat, opt->y1, 0, 0);
        if(s && idx != sel) {
            ui_fit(j, h - 4, s->w, s->h, &rect.w, &rect.h);
            spr_blit(s, 160, dst, &rect);
        } else
            spr_texture(s, &choosers_anims[1]);
        rect.x += 4 + j;
        s = spr_getidx(choosers_imgp.cat, opt->x2, 0, 0);
        if(s && idx != sel) {
            ui_fit(j, h - 4, s->w, s->h, &rect.w, &rect.h);
            spr_blit(s, 160, dst, &rect);
        } else
            spr_texture(s, &choosers_anims[2]);
        if(opt->rtype[0] != 2) {
            ui_clip.w = hdr[0].w + hdr[1].w + hdr[2].w;
            i = opt->rtype[1] != 2 ? 32 : 16;
            if(opt->rtype[0] > 2)
                sprintf(tmp, "%s=%u %s", ui_cmd_locals[opt->rtype[0]], opt->rval[0], opt->rtype[1] != 2 ? lang[CHOOSERS_AND] : "");
            else
                sprintf(tmp, "%s=%u %s", project.attrs[opt->rattr[0]], opt->rval[0], opt->rtype[1] != 2 ? lang[CHOOSERS_AND] : "");
            ui_text(dst, x + hdr[0].w + hdr[1].w + 4, y + (h - i) / 2, tmp);
            if(opt->rtype[1] != 2) {
                if(opt->rtype[1] > 2)
                    sprintf(tmp, "%s>%u", ui_cmd_locals[opt->rtype[1]], opt->rval[1]);
                else
                    sprintf(tmp, "%s>%u", project.attrs[opt->rattr[1]], opt->rval[1]);
                ui_text(dst, x + hdr[0].w + hdr[1].w + 4, y + h / 2, tmp);
            }
        }
    }
    ui_clip.w = x + w - 12;
    x += hdr[0].w + hdr[1].w + hdr[2].w + 2;
    if(opt->keep)
        ui_icon(dst, x, y + (h - 16) / 2, ICON_HIST);
    x += 18;
    if(opt->ptype == 2) {
        sprintf(tmp, "a:=%u", idx + 1);
        ui_text(dst, x, y + (h - 16) / 2, tmp);
    } else {
        sprintf(tmp, "a:=%u", idx + 1);
        ui_text(dst, x, y + h / 2 - 16, tmp);
        if(opt->ptype > 2)
            sprintf(tmp, "%s:=%u", ui_cmd_locals[opt->ptype], opt->pval);
        else
            sprintf(tmp, "%s:=%u", project.attrs[opt->pattr], opt->pval);
        ui_text(dst, x, y + h / 2, tmp);
    }
    ui_clip.w = t;
}

/**
 * Render table cell
 */
void choosers_renderopt(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    choosers_opt_t *opt = (choosers_opt_t*)data;
    SDL_Rect rect;
    ui_sprite_t *s;

    (void)dst; (void)w;
    if(!data || opt->pos >= 13 || idx != sel) return;
    rect.x = x + hdr[0].w + 4; rect.y = y + 1; rect.h = h - 4;
    rect.w = (hdr[1].w) / 3 - 4;
    s = spr_getidx(choosers_imgn.cat, opt->x1, 0, 1);
    spr_render(s, -1, choosers_anims[0], &rect);
    rect.x += 4 + rect.w;
    s = spr_getidx(choosers_imgs.cat, opt->y1, 0, 0);
    spr_render(s, -1, choosers_anims[1], &rect);
    rect.x += 4 + rect.w;
    s = spr_getidx(choosers_imgp.cat, opt->x2, 0, 0);
    spr_render(s, -1, choosers_anims[2], &rect);
}

/**
 * Exit choosers window
 */
void choosers_exit(int tab)
{
    int i;

    (void)tab;
    choosers_tbl.clk = choosers_opts.clk = -1;
    for(i = 0; i < 3; i++)
        if(choosers_anims[i]) { SDL_DestroyTexture(choosers_anims[i]); choosers_anims[i] = NULL; }
}

/**
 * Enter choosers window
 */
void choosers_init(int tab)
{
    choosers_exit(tab);
    choosers_form[1].param = lang[LANG_SAVE];
    choosers_form[1].w = ui_textwidth(choosers_form[1].param) + 40;
    if(choosers_form[1].w < 200) choosers_form[1].w = 200;
    project_freedir(&project.choosers, &project.numchoosers);
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_UI]);
    project.choosers = project_getdir(projdir, ".chr", &project.numchoosers);
    if(!choosers_name[0]) choosers_tbl.val = -1;
    choosers_tbl.data = project.choosers;
    choosers_tbl.num = project.numchoosers;
    if(choosers_tbl.val >= choosers_tbl.num) choosers_tbl.val = choosers_tbl.num - 1;
    if(choosers_opts.val >= choosers_opts.num) choosers_opts.val = choosers_opts.num - 1;
}

/**
 * Resize the view
 */
void choosers_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    choosers_form[0].y = choosers_form[1].y = choosers_form[2].y = choosers_form[3].y = h - 48;
    choosers_form[1].x = w - 20 - choosers_form[1].w;
    choosers_form[2].x = choosers_form[1].x - 52;
    ui_table_resize(&choosers_form[5], choosers_form[5].w, h - 89);
    choosers_form[3].x = choosers_form[5].x + choosers_form[5].w + 20;
    choosers_form[6].x = choosers_form[7].x = choosers_form[36].x = choosers_form[5].x + choosers_form[5].w + 10;
    choosers_form[4].x = w - 10 - choosers_form[4].w;
    ui_table_resize(&choosers_form[6], w - 10 - choosers_form[6].x, h - 82 - choosers_form[6].y);
    for(i = 37; i < 44; i++)
        choosers_form[i].x = choosers_form[i - 1].x + choosers_form[i - 1].w + 2;
    for(i = 7; i < 36; i++)
        choosers_form[i].y = h - 48 - 30;
    choosers_form[8].x = choosers_form[7].x + choosers_form[7].w + 10;
    choosers_form[9].x = choosers_form[8].x + choosers_form[8].w + 4;
    for(i = 10; i < 19; i++)
        choosers_form[i].x = choosers_form[i - 1].x + choosers_form[i - 1].w + 2;
    for(i = 19; i < 22; i++) {
        choosers_form[i].x = choosers_form[i == 19 ? 8 : i - 1].x + choosers_form[i == 19 ? 8 : i - 1].w + 4;
        choosers_form[i].w = (choosers_opts.hdr[1].w - 8) / 3;
    }
    choosers_form[22].x = choosers_form[6].x + choosers_opts.hdr[0].w + choosers_opts.hdr[1].w + 4;
    choosers_form[22].w = choosers_req[0].type > 1 ? 18 : 24 + ui_textwidth(project.attrs[choosers_req[0].attr]);
    choosers_form[24].w = choosers_form[28].w = choosers_form[32].w = 62;
    i = (choosers_opts.hdr[2].w - 24 - 8 - 2 * 62) / 2;
    if(i < 32) {
        choosers_form[24].w = choosers_form[28].w = choosers_form[32].w = 42;
        i = (choosers_opts.hdr[2].w - 24 - 8 - 2 * 42) / 2;
    }
    if(choosers_form[22].w > i) choosers_form[22].w = i;
    choosers_form[23].x = choosers_form[22].x + choosers_form[22].w;
    choosers_form[24].x = choosers_form[23].x + choosers_form[23].w + 2;
    choosers_form[25].x = choosers_form[24].x + choosers_form[24].w;
    choosers_form[26].x = choosers_form[25].x + choosers_form[25].w + 2;
    choosers_form[26].w = choosers_req[1].type > 1 ? 18 : 24 + ui_textwidth(project.attrs[choosers_req[1].attr]);
    if(choosers_form[26].w > i) choosers_form[26].w = i;
    choosers_form[27].x = choosers_form[26].x + choosers_form[26].w;
    choosers_form[28].x = choosers_form[27].x + choosers_form[27].w + 2;

    choosers_form[29].x = choosers_form[6].x + choosers_opts.hdr[0].w + choosers_opts.hdr[1].w + choosers_opts.hdr[2].w + 4;
    choosers_form[30].x = choosers_form[29].x + choosers_form[29].w;
    choosers_form[30].w = choosers_prov.type > 1 ? 18 : 24 + ui_textwidth(project.attrs[choosers_prov.attr]);
    if(choosers_form[30].x + choosers_form[30].w > scr_w - 136) choosers_form[30].w = scr_w - 136 - choosers_form[30].x;
    choosers_form[31].x = choosers_form[30].x + choosers_form[30].w;
    choosers_form[32].x = choosers_form[31].x + choosers_form[31].w + 2;
    choosers_form[35].x = w - 30;
    choosers_form[34].x = choosers_form[35].x - choosers_form[34].w - 2;
    choosers_form[33].x = choosers_form[34].x - choosers_form[33].w - 2;
}

/**
 * View layer
 */
void choosers_redraw(int tab)
{
    int i;

    (void)tab;
    ui_form = choosers_form;
    ui_form[0].inactive = (choosers_tbl.val < 0);
    ui_form[1].inactive = (!choosers_name[0]);
    ui_form[2].inactive = (!choosers_opts.num);
    ui_form[7].inactive = (choosers_opts.val < 0);
    for(i = 9; i < 19; i++)
        choosers_form[i].inactive = choosers_possel.val < 13 ? 2 : 0;
    for(i = 19; i < 29; i++)
        choosers_form[i].inactive = choosers_possel.val < 13 ? 0 : 2;

    if(choosers_req[0].type == 1) choosers_req[0].type = 0;
    if(!choosers_form[22].inactive) {
        choosers_form[23].inactive = choosers_form[24].inactive = choosers_form[25].inactive = choosers_form[26].inactive =
            choosers_form[27].inactive = choosers_form[28].inactive = (choosers_req[0].type == 2);
        if(!choosers_form[26].inactive)
            choosers_form[27].inactive = choosers_form[28].inactive = (choosers_req[1].type == 2);
    }
    choosers_form[22].w = choosers_req[0].type > 1 ? 18 : 24 + ui_textwidth(project.attrs[choosers_req[0].attr]);
    choosers_form[24].w = choosers_form[28].w = choosers_form[32].w = 62;
    i = (choosers_opts.hdr[2].w - 24 - 8 - 2 * 62) / 2;
    if(i < 32) {
        choosers_form[24].w = choosers_form[28].w = choosers_form[32].w = 42;
        i = (choosers_opts.hdr[2].w - 24 - 8 - 2 * 42) / 2;
    }
    if(choosers_form[22].w > i) choosers_form[22].w = i;
    choosers_form[23].x = choosers_form[22].x + choosers_form[22].w;
    choosers_form[24].x = choosers_form[23].x + choosers_form[23].w + 2;
    choosers_form[25].x = choosers_form[24].x + choosers_form[24].w;
    choosers_form[26].x = choosers_form[25].x + choosers_form[25].w + 2;
    choosers_form[26].w = choosers_req[1].type > 1 ? 18 : 24 + ui_textwidth(project.attrs[choosers_req[1].attr]);
    if(choosers_form[26].w > i) choosers_form[26].w = i;
    choosers_form[27].x = choosers_form[26].x + choosers_form[26].w;
    choosers_form[28].x = choosers_form[27].x + choosers_form[27].w + 2;

    if(choosers_prov.type == 1) choosers_prov.type = 0;
    choosers_form[31].inactive = choosers_form[32].inactive = (choosers_prov.type == 2);
    choosers_form[30].w = choosers_prov.type > 1 ? 18 : 24 + ui_textwidth(project.attrs[choosers_prov.attr]);
    if(choosers_form[30].x + choosers_form[30].w > scr_w - 136) choosers_form[30].w = scr_w - 136 - choosers_form[30].x;
    choosers_form[31].x = choosers_form[30].x + choosers_form[30].w;
    choosers_form[32].x = choosers_form[31].x + choosers_form[31].w + 2;
    ui_form[33].inactive = (choosers_opts.val < 1);
    ui_form[34].inactive = (choosers_opts.val < 0 || choosers_opts.val + 1 >= choosers_opts.num);
    ui_form[35].inactive = (choosers_possel.val < 13 && choosers_imgn.val < 0);
}

/**
 * Click on chooser options table
 */
void choosers_clk(void *data)
{
    choosers_opt_t *list = (choosers_opt_t*)choosers_opts.data;

    (void)data;
    if(!list || choosers_opts.val < 0 || choosers_opts.val >= choosers_opts.num || list[choosers_opts.val].pos == 13) return;
    if(ui_curx > ui_form[6].x && ui_curx < ui_form[6].x + choosers_opts.hdr[0].w) {
        if(ui_curbtn == 1) {
            if(list[choosers_opts.val].pos < 12) list[choosers_opts.val].pos++; else list[choosers_opts.val].pos = 0;
        }
        if(ui_curbtn != 1) {
            if(list[choosers_opts.val].pos > 0) list[choosers_opts.val].pos--; else list[choosers_opts.val].pos = 12;
        }
    }
}

/**
 * Move chooser option up
 */
void choosers_up(void *data)
{
    choosers_opt_t *list = (choosers_opt_t*)choosers_opts.data, tmp;

    (void)data;
    if(!list || choosers_opts.val < 1 || choosers_opts.val >= choosers_opts.num) return;
    memcpy(&tmp, &list[choosers_opts.val - 1], sizeof(choosers_opt_t));
    memcpy(&list[choosers_opts.val - 1], &list[choosers_opts.val], sizeof(choosers_opt_t));
    memcpy(&list[choosers_opts.val], &tmp, sizeof(choosers_opt_t));
    choosers_opts.val--;
}

/**
 * Move chooser option down
 */
void choosers_down(void *data)
{
    choosers_opt_t *list = (choosers_opt_t*)choosers_opts.data, tmp;

    (void)data;
    if(!list || choosers_opts.val < 0 || choosers_opts.val + 1 >= choosers_opts.num) return;
    memcpy(&tmp, &list[choosers_opts.val + 1], sizeof(choosers_opt_t));
    memcpy(&list[choosers_opts.val + 1], &list[choosers_opts.val], sizeof(choosers_opt_t));
    memcpy(&list[choosers_opts.val], &tmp, sizeof(choosers_opt_t));
    choosers_opts.val++;
}

/**
 * Erase chooser option
 */
void choosers_erase(void *data)
{
    choosers_opt_t *list = (choosers_opt_t*)choosers_opts.data;

    (void)data;
    if(!list || choosers_opts.val < 0 || choosers_opts.val >= choosers_opts.num) return;
    memcpy(&list[choosers_opts.val], &list[choosers_opts.val + 1], (choosers_opts.num - choosers_opts.val) *
        sizeof(choosers_opt_t));
    choosers_opts.num--;
    if(choosers_opts.val >= choosers_opts.num)
        choosers_opts.val = choosers_opts.num - 1;
    if(!choosers_opts.num) { free(list); choosers_opts.data = NULL; }
}

/**
 * Add chooser option
 */
void choosers_add(void *data)
{
    choosers_opt_t *list = (choosers_opt_t*)choosers_opts.data;
    int i, j;

    (void)data;
    if(choosers_possel.val < 13 && choosers_imgn.val < 0) return;
    list = (choosers_opt_t*)realloc(list, (choosers_opts.num + 1) * sizeof(choosers_opt_t));
    choosers_opts.data = list;
    if(!list) main_error(ERR_MEM);
    i = choosers_opts.num++;
    list[i].pos = choosers_possel.val;
    if(choosers_possel.val < 13) {
        list[i].x1 = choosers_imgn.val;
        list[i].y1 = choosers_imgs.val;
        list[i].x2 = choosers_imgp.val;
    } else {
        if(choosers_x1.val > choosers_x2.val) { j = choosers_x1.val; choosers_x1.val = choosers_x2.val; choosers_x2.val = j; }
        if(choosers_x1.val == choosers_x2.val) { if(choosers_x2.val == 100) choosers_x1.val--; else choosers_x2.val++; }
        if(choosers_y1.val > choosers_y2.val) { j = choosers_y1.val; choosers_y1.val = choosers_y2.val; choosers_y2.val = j; }
        if(choosers_y1.val == choosers_y2.val) { if(choosers_y2.val == 100) choosers_y1.val--; else choosers_y2.val++; }
        list[i].x1 = choosers_x1.val;
        list[i].y1 = choosers_y1.val;
        list[i].x2 = choosers_x2.val;
        list[i].y2 = choosers_y2.val;
    }
    list[i].rtype[0] = choosers_req[0].type;
    list[i].rattr[0] = choosers_req[0].attr;
    list[i].rval[0] = choosers_rval[0].val;
    list[i].rtype[1] = choosers_req[1].type;
    list[i].rattr[1] = choosers_req[1].attr;
    list[i].rval[1] = choosers_rval[1].val;
    list[i].keep = choosers_keep;
    list[i].ptype = choosers_prov.type;
    list[i].pattr = choosers_prov.attr;
    list[i].pval = choosers_pval.val;
}

/**
 * Clear form, new chooser
 */
void choosers_new(void *data)
{
    (void)data;
    choosers_name[0] = 0;
    choosers_possel.val = choosers_opts.num = choosers_keep = choosers_rval[0].val = choosers_rval[1].val = choosers_pval.val =
        choosers_req[0].attr = choosers_req[1].attr = choosers_prov.attr = choosers_numl.val = choosers_numt.val =
        choosers_numH.val = choosers_numV.val = 0;
    choosers_req[0].type = choosers_req[1].type = choosers_prov.type = 2;
    choosers_imgn.val = choosers_imgs.val = choosers_imgp.val = choosers_opts.val = -1;
    if(choosers_opts.data) { free(choosers_opts.data); choosers_opts.data = NULL; }
}

/**
 * Delete chooser
 */
void choosers_delete(void *data)
{
    char **list = (char**)choosers_tbl.data;

    (void)data;
    if(choosers_tbl.val < 0 || !list) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.chr", project.id, project_dirs[PROJDIRS_UI], list[choosers_tbl.val]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("choosers_delete: removing %s\n", projdir);
        remove(projdir);
        choosers_new(NULL);
        choosers_init(SUBMENU_CHOOSERS);
    }
}

/**
 * Save chooser
 */
void choosers_save(void *data)
{
    choosers_opt_t *list = (choosers_opt_t*)choosers_opts.data;
    char **chrs = (char**)choosers_tbl.data;
    FILE *f;
    int i;

    (void)data;
    if(!choosers_name[0]) return;
    f = project_savefile(!chrs || choosers_tbl.val < 0 || strcmp(chrs[choosers_tbl.val], choosers_name),
        project_dirs[PROJDIRS_UI], choosers_name, "chr", PROJMAGIC_CHOOSER, "choosers_save");
    if(f) {
        fprintf(f, "%u %u %u %u\r\n", choosers_numl.val, choosers_numt.val, choosers_numH.val, choosers_numV.val);
        for(i = 0; list && i < choosers_opts.num; i++) {
            fprintf(f, "%u %u ", list[i].pos, list[i].keep);
            if(list[i].pos >= 13)
                fprintf(f, "%u %u %u %u ", list[i].x1, list[i].y1, list[i].x2, list[i].y2);
            else {
                project_wrsprite2(f, list[i].x1, choosers_imgn.cat); fprintf(f, " ");
                project_wrsprite2(f, list[i].y1, choosers_imgn.cat); fprintf(f, " ");
                project_wrsprite2(f, list[i].x2, choosers_imgn.cat); fprintf(f, " ");
                fprintf(f, "0 ");
            }
            if(list[i].rtype[0] == 2 || list[i].pos >= 13)
                fprintf(f, "- ");
            else {
                if(list[i].rtype[0] > 2)
                    fprintf(f, "%s=%u", ui_cmd_locals[list[i].rtype[0]], list[i].rval[0]);
                else
                    fprintf(f, "%s=%u", project.attrs[list[i].rattr[0]], list[i].rval[0]);
                if(list[i].rtype[1] == 2)
                    fprintf(f, " ");
                else
                if(list[i].rtype[1] > 2)
                    fprintf(f, "&%s>%u ", ui_cmd_locals[list[i].rtype[1]], list[i].rval[1]);
                else
                    fprintf(f, "&%s>%u ", project.attrs[list[i].rattr[1]], list[i].rval[1]);
            }
            if(list[i].ptype == 2)
                fprintf(f, "-\r\n");
            else
            if(list[i].ptype > 2)
                fprintf(f, "%s=%u\r\n", ui_cmd_locals[list[i].ptype], list[i].pval);
            else
                fprintf(f, "%s=%u\r\n", project.attrs[list[i].pattr], list[i].pval);
        }
        fclose(f);
        choosers_init(SUBMENU_CHOOSERS);
        for(choosers_tbl.val = 0; choosers_tbl.val < choosers_tbl.num &&
            strcmp(choosers_name, project.choosers[choosers_tbl.val]); choosers_tbl.val++);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.chr", project.id, project_dirs[PROJDIRS_UI], choosers_name);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVECHR]);
}

/**
 * Load chooser
 */
int choosers_loadchr(char *fn)
{
    char *str, *s;

    str = project_loadfile(project_dirs[PROJDIRS_UI], fn, "chr", PROJMAGIC_CHOOSER, "choosers_loadchr");
    if(str) {
        s = project_skipnl(str);
        s = project_getint(s, &choosers_numl.val, choosers_numl.min, choosers_numl.max);
        s = project_getint(s, &choosers_numt.val, choosers_numt.min, choosers_numt.max);
        s = project_getint(s, &choosers_numH.val, choosers_numH.min, choosers_numH.max);
        s = project_getint(s, &choosers_numV.val, choosers_numV.min, choosers_numV.max);
        if(choosers_opts.data) { free(choosers_opts.data); choosers_opts.data = NULL; }
        choosers_opts.num = 0;
        while(*s) {
            s = project_skipnl(s);
            if(!*s) break;
            s = project_getint(s, &choosers_possel.val, 0, 13);
            s = project_getint(s, &choosers_keep, 0, 1);
            if(choosers_possel.val >= 13) {
                s = project_getint(s, &choosers_x1.val, choosers_x1.min, choosers_x1.max);
                s = project_getint(s, &choosers_y1.val, choosers_y1.min, choosers_y1.max);
                s = project_getint(s, &choosers_x2.val, choosers_x2.min, choosers_x2.max);
            } else {
                s = project_getsprite(s, &choosers_imgn);
                s = project_getsprite(s, &choosers_imgs);
                s = project_getsprite(s, &choosers_imgp);
            }
            s = project_getint(s, &choosers_y2.val, choosers_y2.min, choosers_y2.max);
            while(*s == ' ') s++;
            choosers_req[0].attr = choosers_req[1].attr = choosers_rval[0].val = choosers_rval[1].val = 0;
            choosers_req[0].type = choosers_req[1].type = 2;
            if(*s != '-') {
                if(*s >= 'b' && *s <= 'z' && s[1] == '=')
                    s = project_getidx(s, &choosers_req[0].type, ui_cmd_locals, 0);
                else
                    s = project_getidx(s, &choosers_req[0].attr, project.attrs, 0);
                if(*s == '=')
                    s = project_getint(s + 1, &choosers_rval[0].val, choosers_rval[0].min, choosers_rval[0].max);
                if(*s == '&') {
                    s++;
                    if(*s >= 'b' && *s <= 'z' && s[1] == '>')
                        s = project_getidx(s, &choosers_req[1].type, ui_cmd_locals, 0);
                    else
                        s = project_getidx(s, &choosers_req[1].attr, project.attrs, 0);
                    if(*s == '>')
                        s = project_getint(s + 1, &choosers_rval[1].val, choosers_rval[1].min, choosers_rval[1].max);
                }
            } else s++;
            while(*s == ' ') s++;
            choosers_prov.attr = choosers_pval.val = 0; choosers_prov.type = 2;
            if(*s != '-') {
                if(*s >= 'b' && *s <= 'z' && s[1] == '=')
                    s = project_getidx(s, &choosers_prov.type, ui_cmd_locals, 0);
                else
                    s = project_getidx(s, &choosers_prov.attr, project.attrs, 0);
                if(*s == '=')
                    s = project_getint(s + 1, &choosers_pval.val, choosers_pval.min, choosers_pval.max);
            }
            choosers_add(NULL);
        }
        free(str);
        choosers_imgn.val = choosers_imgs.val = choosers_imgp.val = -1;
        choosers_x1.val = choosers_y1.val = 0; choosers_x2.val = choosers_y2.val = 1;
        choosers_possel.val = choosers_keep = choosers_prov.attr = choosers_pval.val = choosers_req[0].attr =
            choosers_req[1].attr = choosers_rval[0].val = choosers_rval[1].val =0;
        choosers_prov.type = choosers_req[0].type = choosers_req[1].type = 2;
        strncpy(choosers_name, fn, sizeof(choosers_name) - 1);
        return 1;
    }
    return 0;
}

/**
 * Load chooser
 */
void choosers_load(void *data)
{
    char **list = (char**)choosers_tbl.data;

    (void)data;
    if(choosers_tbl.val < 0 || !list) return;
    if(!choosers_loadchr(list[choosers_tbl.val]))
        ui_status(1, lang[ERR_LOADCHR]);
}

/**
 * Coordinate picker helper
 */
void choosers_helper(void *data)
{
    (void)data;
    ui_coord_helper(&choosers_help, &choosers_x1.val, &choosers_y1.val, &choosers_x2.val, &choosers_y2.val);
}

/**
 * Preview chooser
 */
void choosers_preview(void *data)
{
    choosers_opt_t *list = (choosers_opt_t*)choosers_opts.data;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *texture = NULL, **txts = NULL;
    SDL_Rect dst[13];
    int wf = SDL_GetWindowFlags(window);
    int i, j, k, x, y, p, results = 0, pressed = 0, over = -1, last = -1, *vars, nvars = (project.numattrs + 26) * 2;
    char tmp[128], *str;
    uint32_t *pixels;
    ui_sprite_t *s;

    (void)data;
    if(!list) return;
    if(verbose) printf("choosers_preview: started\n");

    vars = (int*)main_alloc(nvars * sizeof(int));
    txts = (SDL_Texture**)main_alloc(choosers_opts.num * 3 * sizeof(SDL_Texture*));
    for(i = 0; i < choosers_opts.num; i++)
        if(list[i].pos < 13) {
            spr_texture(spr_getidx(choosers_imgn.cat, list[i].x1, 0, 0), &txts[i * 3 + 0]);
            spr_texture(spr_getidx(choosers_imgs.cat, list[i].y1, 0, 0), &txts[i * 3 + 1]);
            spr_texture(spr_getidx(choosers_imgp.cat, list[i].x2, 0, 0), &txts[i * 3 + 2]);
        }

    SDL_SetCursor(cursors[CURSOR_PTR]);
    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_GetWindowPosition(window, &i, &j);
    SDL_GetMouseState(&x, &y);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;
    while(SDL_PollEvent(&event));
    SDL_WarpMouseInWindow(window, x + i, y + j);
    ui_curx = x + i; ui_cury = y + j;

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
    if(texture) {
        SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
        /* checker */
        for(j = k = 0; j < dm.h; j++, k += p/4)
            for(i = 0; i < dm.w ; i++) {
                x = (i >> 5); y = (j >> 5);
                pixels[k + i] = theme[(y & 1) ^ (x & 1) ? THEME_LIGHT : THEME_DARK];
            }
        SDL_UnlockTexture(texture);
    }

again:
    memset(dst, 0, sizeof(dst));
    for(j = 0; j < 14; j++)
        for(i = 0; i < choosers_opts.num; i++) {
            if(!j) { list[i].d.x = list[i].d.y = 0; list[i].d.w = list[i].d.h = -1; }
            if(list[i].pos == j &&
              (list[i].rtype[0] == 2 ||
              (list[i].rtype[0] < 2 && vars[(list[i].rattr[0] + 26) * 2 + 1] == list[i].rval[0]) ||
              (list[i].rtype[0] > 2 && vars[(list[i].rtype[0] - 2) * 2 + 1] == list[i].rval[0])) &&
              (list[i].rtype[1] == 2 ||
              (list[i].rtype[1] < 2 && vars[(list[i].rattr[1] + 26) * 2 + 1] > list[i].rval[1]) ||
              (list[i].rtype[1] > 2 && vars[(list[i].rtype[1] - 2) * 2 + 1] > list[i].rval[1]))) {
                if(j < 13) {
                    k = (i != over ? list[i].x1 : (pressed && list[i].x2 >= 0 ? list[i].x2 : (list[i].y1 >= 0 ? list[i].y1 :
                        list[i].x1)));
                    s = spr_getidx(choosers_imgn.cat, k, 0, 0);
                    if(!s) continue;
                    list[i].d.w = s->w; list[i].d.h = s->h;
                    switch(j) {
                        case 0: case 1: case 2: case 3: case 7: case 8: case 9:
                            dst[j].w += (dst[j].w ? choosers_numH.val : 0) + s->w;
                            if(s->h > dst[j].h) dst[j].h = s->h;
                        break;
                        default:
                            dst[j].h += (dst[j].h ? choosers_numV.val : 0) + s->h;
                            if(s->w > dst[j].w) dst[j].w = s->w;
                        break;
                    }
                } else {
                    list[i].d.x = (dm.w * list[i].x1 + 50) / 100;
                    list[i].d.y = (dm.h * list[i].y1 + 50) / 100;
                    list[i].d.w = (dm.w * list[i].x2 + 50) / 100 - list[i].d.x;
                    list[i].d.h = (dm.h * list[i].y2 + 50) / 100 - list[i].d.y;
                }
            }
        }
    for(j = 0; j < 13; j++)
        for(i = 0; i < choosers_opts.num; i++)
            if(list[i].pos == j && list[i].d.w > 0 && list[i].d.h > 0) {
                switch(j) {
                    case 0:
                        list[i].d.x = (dm.w - dst[j].w) / 2 + dst[j].x;
                        list[i].d.y = (dm.h - list[i].d.h) / 2;
                        dst[j].x += choosers_numH.val + list[i].d.w;
                    break;
                    case 1:
                        list[i].d.x = dst[j].x + choosers_numl.val;
                        list[i].d.y = choosers_numt.val;
                        dst[j].x += choosers_numH.val + list[i].d.w;
                    break;
                    case 2:
                        list[i].d.x = (dm.w - dst[j].w) / 2 + dst[j].x;
                        list[i].d.y = choosers_numt.val;
                        dst[j].x += choosers_numH.val + list[i].d.w;
                    break;
                    case 3:
                        list[i].d.x = dm.w - dst[j].w + dst[j].x - choosers_numl.val;
                        list[i].d.y = choosers_numt.val;
                        dst[j].x += choosers_numH.val + list[i].d.w;
                    break;
                    case 4:
                        list[i].d.x = dm.w - list[i].d.w - choosers_numl.val;
                        list[i].d.y = dst[j].y + choosers_numt.val + dst[3].h;
                        dst[j].y += choosers_numV.val + list[i].d.h;
                    break;
                    case 5:
                        list[i].d.x = dm.w - list[i].d.w - choosers_numl.val;
                        list[i].d.y = (dm.h - dst[j].h) / 2 + dst[j].y;
                        dst[j].y += choosers_numV.val + list[i].d.h;
                    break;
                    case 6:
                        list[i].d.x = dm.w - list[i].d.w - choosers_numl.val;
                        list[i].d.y = dm.h - dst[j].h + dst[j].y - choosers_numt.val - dst[7].h;
                        dst[j].y += choosers_numV.val + list[i].d.h;
                    break;
                    case 7:
                        list[i].d.x = dm.w - dst[j].w + dst[j].x - choosers_numl.val;
                        list[i].d.y = dm.h - list[i].d.h - choosers_numt.val;
                        dst[j].x += choosers_numH.val + list[i].d.w;
                    break;
                    case 8:
                        list[i].d.x = (dm.w - dst[j].w) / 2 + dst[j].x;
                        list[i].d.y = dm.h - list[i].d.h - choosers_numt.val;
                        dst[j].x += choosers_numH.val + list[i].d.w;
                    break;
                    case 9:
                        list[i].d.x = dst[j].x + choosers_numl.val;
                        list[i].d.y = dm.h - list[i].d.h - choosers_numt.val;
                        dst[j].x += choosers_numH.val + list[i].d.w;
                    break;
                    case 10:
                        list[i].d.x = choosers_numl.val;
                        list[i].d.y = dm.h - dst[j].h + dst[j].y - choosers_numt.val - dst[9].h;
                        dst[j].y += choosers_numV.val + list[i].d.h;
                    break;
                    case 11:
                        list[i].d.x = choosers_numl.val;
                        list[i].d.y = (dm.h - dst[j].h) / 2 + dst[j].y;
                        dst[j].y += choosers_numV.val + list[i].d.h;
                    break;
                    case 12:
                        list[i].d.x = choosers_numl.val;
                        list[i].d.y = dst[j].y + choosers_numt.val + dst[1].h;
                        dst[j].y += choosers_numV.val + list[i].d.h;
                    break;
                }
            }

    while(1) {
        /* events */
        SDL_WaitEvent(&event);
        if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
            SDL_PushEvent(&event); goto cleanup;
        }
        switch(event.type) {
            case SDL_KEYUP: goto cleanup; break;
            case SDL_MOUSEBUTTONDOWN: pressed = 1; goto again; break;
            case SDL_MOUSEBUTTONUP:
                pressed = 0;
                if(results || over == -1) goto cleanup;
                if(!results) {
                    i = 0;
                    if(list[over].ptype < 2) i = list[over].pattr + 26;
                    if(list[over].ptype > 2) i = list[over].ptype - 2;
                    vars[i * 2] = 1; vars[i * 2 + 1] = list[over].pval;
                    vars[0] = 1; vars[1] = over + 1;
                    if(list[over].keep) goto again;
                }
                results = 1;
                if(texture) {
                    SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
                    for(i = 0; i < p / 4 * dm.h; i++) pixels[i] = theme[THEME_BG];
                    SDL_UnlockTexture(texture);
                    for(i = j = k = 0; i < nvars; i += 2)
                        if(vars[i]) {
                            p = i / 2 < 26 ? 8 : ui_textwidth(project.attrs[i / 2 - 26]);
                            if(p > k) k = p;
                            j++;
                        }
                    p = ui_textwidth(lang[CHOOSERS_RETURN]) / 2 + 1;
                    if(k < p) k = p;
                    k += 16;
                    y = (dm.h - j * 16) / 2;
                    if(y < 20) y = 20;
                    ssfn_dst.fg = theme[THEME_SELFG];
                    ui_fldgrp(texture, dm.w / 2 - k, y - 8, 2 * k, (j + 1) * 16, lang[CHOOSERS_RETURN]);
                    for(i = 0; i < nvars && y < dm.h; i += 2)
                        if(vars[i]) {
                            str = i / 2 < 26 ? ui_cmd_locals[i / 2 + 2] : project.attrs[i / 2 - 26];
                            ssfn_dst.fg = theme[THEME_FG];
                            ui_text(texture, dm.w / 2 - 8 - ui_textwidth(str), y, str);
                            ssfn_dst.fg = theme[THEME_LIGHT];
                            ui_text(texture, dm.w / 2 - 8, y, ":=");
                            sprintf(tmp, "%u", vars[i + 1]);
                            ssfn_dst.fg = theme[THEME_FG];
                            ui_text(texture, dm.w / 2 + 8, y, tmp);
                            y += 16;
                        }
                    ssfn_dst.fg = theme[THEME_FG];
                }
            break;
            case SDL_MOUSEMOTION:
                if(results) break;
                ui_curx = event.motion.x; ui_cury = event.motion.y; over = -1;
                for(i = 0; i < choosers_opts.num; i++)
                    if(ui_curx >= list[i].d.x && ui_curx < list[i].d.x + list[i].d.w &&
                      ui_cury >= list[i].d.y && ui_cury < list[i].d.y + list[i].d.h) { over = i; break; }
                if(last != over) { last = over; goto again; }
            break;
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        if(texture) SDL_RenderCopy(renderer, texture, NULL, NULL);
        if(!results) {
            for(i = 0; i < choosers_opts.num; i++)
                if(list[i].d.w && list[i].d.h) {
                    if(list[i].pos >= 12)
                        SDL_RenderCopy(renderer, spr_select, NULL, &list[i].d);
                    else {
                        k = (i != over ? list[i].x1 : (pressed && list[i].x2 >= 0 ? list[i].x2 : (list[i].y1 >= 0 ? list[i].y1 :
                            list[i].x1)));
                        s = spr_getidx(choosers_imgn.cat, k, 0, 0);
                        if(!s) continue;
                        k = (i != over ? 0 : (pressed && list[i].x2 >= 0 ? 2 : (list[i].y1 >= 0 ? 1 : 0)));
                        spr_render(s, -1, txts[i * 3 + k], &list[i].d);
                    }
                }
        }
        SDL_RenderPresent(renderer);
    }

cleanup:
    if(txts) {
        for(i = 0; i < choosers_opts.num; i++) {
            if(txts[i * 3 + 0]) SDL_DestroyTexture(txts[i * 3 + 0]);
            if(txts[i * 3 + 1]) SDL_DestroyTexture(txts[i * 3 + 1]);
            if(txts[i * 3 + 2]) SDL_DestroyTexture(txts[i * 3 + 2]);
        }
        free(txts);
    }
    if(texture) SDL_DestroyTexture(texture);
    free(vars);
    if(verbose) printf("choosers_preview: stopped\n");
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Get references
 */
void choosers_ref(tngctx_t *ctx, int idx)
{
    char *str, *s;
    int j;

    if(!tng_ref(ctx, TNG_IDX_CHR, idx)) return;
    str = project_loadfile(project_dirs[PROJDIRS_UI], project.choosers[idx], "chr", PROJMAGIC_CHOOSER, "choosers_ref");
    if(str) {
        s = project_skipnl(str);
        while(*s) {
            s = project_skipnl(s);
            if(!*s) break;
            s = project_skiparg(s, 6);
            while(*s == ' ') s++;
            if(*s != '-') {
                if(*s >= 'b' && *s <= 'z' && s[1] == '=')
                    s++;
                else {
                    s = project_getidx(s, &j, project.attrs, 0);
                    attrs_ref(ctx, j);
                }
                if(*s == '=') {
                    do{ s++; } while(*s && *s != '&' && *s != ' ' && *s != '\r' && *s != '\n');
                }
                if(*s == '&') {
                    s++;
                    if(*s >= 'b' && *s <= 'z' && s[1] == '>')
                        s++;
                    else {
                        s = project_getidx(s, &j, project.attrs, 0);
                        attrs_ref(ctx, j);
                    }
                    if(*s == '>') {
                        do{ s++; } while(*s && *s >= '0' && *s <= '9');
                    }
                }
            } else s++;
            while(*s == ' ') s++;
            if(*s != '-') {
                if(*s >= 'b' && *s <= 'z' && s[1] == '=')
                    s++;
                else {
                    s = project_getidx(s, &j, project.attrs, 0);
                    attrs_ref(ctx, j);
                }
            }
        }
        free(str);
    }
}

/**
 * Save tng
 */
int choosers_totng(tngctx_t *ctx)
{
    choosers_opt_t *list;
    uLongf cl;
    uint8_t *buf, *ptr, *comp;
    int i, j, l;

    if(!ctx || !project.choosers || !project.numchoosers) return 1;
    /* check if there's at least one chooser referenced */
    for(j = 0; j < project.numchoosers && !ctx->idx[TNG_IDX_CHR].ref[j]; j++);
    if(j >= project.numchoosers) return 1;
    /* add choosers to output */
    tng_section(ctx, TNG_SECTION_CHOOSERS);
    for(j = 0; j < project.numchoosers; j++) {
        if(!ctx->idx[TNG_IDX_CHR].ref[j]) continue;
        if(!choosers_loadchr(project.choosers[j])) {
            ui_switchtab(SUBMENU_CHOOSERS);
            choosers_tbl.val = j;
            ui_status(1, lang[ERR_LOADCHR]);
            return 0;
        }
        list = (choosers_opt_t*)choosers_opts.data;
        if(!choosers_opts.num || !list) continue;
        l = 10 + 31 * choosers_opts.num;
        buf = (uint8_t*)main_alloc(l);
        memcpy(buf + 0, &choosers_numl.val, 2);
        memcpy(buf + 2, &choosers_numt.val, 2);
        memcpy(buf + 4, &choosers_numH.val, 2);
        memcpy(buf + 6, &choosers_numV.val, 2);
        memcpy(buf + 8, &choosers_opts.num, 2);
        ptr = buf + 10;
        for(i = 0; i < choosers_opts.num; i++) {
            *ptr++ = list[i].pos | (list[i].keep ? 0x80 : 0);
            if(list[i].pos >= 13) {
                *ptr++ = list[i].x1;
                *ptr++ = list[i].y1;
                *ptr++ = list[i].x2;
                *ptr++ = list[i].y2;
                *ptr++ = 0;
                *ptr++ = 0;
            } else {
                /* do not use tng_asset_sprite(), because we don't store the category here */
                tng_sprref(ctx, choosers_imgn.cat, list[i].x1);
                memcpy(ptr, &project.sprites[choosers_imgn.cat][list[i].x1].idx, 2); ptr += 2;
                tng_sprref(ctx, choosers_imgn.cat, list[i].y1);
                memcpy(ptr, &project.sprites[choosers_imgn.cat][list[i].y1].idx, 2); ptr += 2;
                tng_sprref(ctx, choosers_imgn.cat, list[i].x2);
                memcpy(ptr, &project.sprites[choosers_imgn.cat][list[i].x2].idx, 2); ptr += 2;
            }
            *ptr++ = list[i].rtype[0];
            ptr = tng_asset_idx(ptr, ctx, project.attrs, TNG_IDX_ATR, list[i].rattr[0]);
            memcpy(ptr, &list[i].rval[0], 4); ptr += 4;
            *ptr++ = list[i].rtype[1];
            ptr = tng_asset_idx(ptr, ctx, project.attrs, TNG_IDX_ATR, list[i].rattr[1]);
            memcpy(ptr, &list[i].rval[1], 4); ptr += 4;
            *ptr++ = list[i].ptype;
            ptr = tng_asset_idx(ptr, ctx, project.attrs, TNG_IDX_ATR, list[i].pattr);
            memcpy(ptr, &list[i].pval, 4); ptr += 4;
        }
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        if(cl) {
            tng_desc(ctx, project.choosers[j], cl);
            tng_asset_int(ctx, comp, cl);
        } else
            free(comp);
        free(buf);
        /* free resources */
        choosers_new(NULL);
    }
    return 1;
}

/**
 * Read from tng
 */
int choosers_fromtng(tngctx_t *ctx)
{
    FILE *f;
    tng_asset_desc_t *asset;
    uint8_t *buf, *ptr, *end;
    int i, j, k, l, len;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_CHOOSERS; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;
    for(j = 0; j < len; j++, asset++) {
        ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
        buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &l);
        if(buf) {
            if(!asset->name || asset->name >= TNG_SECTION_SIZE(&ctx->tbl[0]) || l < 10) { free(buf); continue; }
            end = buf + l;
            f = project_savefile(0, project_dirs[PROJDIRS_UI], (char*)ctx->sts + asset->name, "chr", PROJMAGIC_CHOOSER,
                "choosers_fromtng");
            if(f) {
                fprintf(f, "%u %u %u %u\r\n", *((uint16_t*)ptr), *((uint16_t*)&ptr[2]), *((uint16_t*)&ptr[4]),
                    *((uint16_t*)&ptr[6])); ptr += 8;
                l = *((uint16_t*)ptr); ptr += 2;
                for(i = 0; i < l && ptr < end; i++) {
                    k = *ptr++;
                    fprintf(f, "%u %u ", k & 0x7f, k & 0x80 ? 1 : 0);
                    if((k & 0x7f) >= 13) {
                        fprintf(f, "%u %u %u %u ", ptr[0], ptr[1], ptr[2], ptr[3]);
                        ptr += 6;
                    } else {
                        ptr = tng_wrsprite2(ctx, ptr, f); fprintf(f, " ");
                        ptr = tng_wrsprite2(ctx, ptr, f); fprintf(f, " ");
                        ptr = tng_wrsprite2(ctx, ptr, f); fprintf(f, " ");
                        fprintf(f, "0 ");
                    }
                    if(*ptr == 2 || (k & 0x7f) >= 13) {
                        fprintf(f, "- ");
                        ptr += 16;
                    } else {
                        if(*ptr > 2) {
                            fprintf(f, "%s=%u", ui_cmd_locals[*ptr], *((uint32_t*)&ptr[4]));
                            ptr += 8;
                        } else {
                            ptr++;
                            ptr = tng_wridx(ctx, ptr, f);
                            fprintf(f, "=%u", *((uint32_t*)ptr));
                            ptr += 4;
                        }
                        if(*ptr == 2) {
                            fprintf(f, " ");
                            ptr += 8;
                        } else
                        if(*ptr > 2) {
                            fprintf(f, "&%s>%u ", ui_cmd_locals[*ptr], *((uint32_t*)&ptr[4]));
                            ptr += 8;
                        } else {
                            fprintf(f, "&");
                            ptr++;
                            ptr = tng_wridx(ctx, ptr, f);
                            fprintf(f, ">%u ", *((uint32_t*)ptr));
                            ptr += 4;
                        }
                    }
                    if(*ptr == 2) {
                        fprintf(f, "-\r\n");
                        ptr += 8;
                    } else
                    if(*ptr > 2) {
                        fprintf(f, "%s=%u\r\n", ui_cmd_locals[*ptr], *((uint32_t*)&ptr[4]));
                        ptr += 8;
                    } else {
                        ptr++;
                        ptr = tng_wridx(ctx, ptr, f);
                        fprintf(f, "=%u\r\n", *((uint32_t*)ptr));
                        ptr += 4;
                    }
                }
                fclose(f);
            }
            free(buf);
        }
    }
    return 1;
}
