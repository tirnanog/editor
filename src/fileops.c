/*
 * tnge/fileops.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief File and directory operations
 *
 */

#include "main.h"

#define HISTSIZE 64

extern ui_numptr_t newsprite_numf;
extern int newsprite_gridw, ui_oldtab, media_ffmpeg, newsprite_assimp;
extern char newsprite_dir, newsprite_gridA;
void newsprite_chdir(void *param);
#ifdef __WIN32__
#include <windows.h>
extern wchar_t szFull[PATH_MAX];
extern char wdname[FILENAME_MAX + 1];
#define dname wdname
#else
#define dname ent->d_name
#endif

typedef struct {
    char *name;
    char type;
    uint64_t size;
    time_t time;
} filelist_t;

int fileops_type = FILEOPS_TNG, fileops_all = 0, fileops_mc = 256;
char fileops_search[64], fileops_rs[16], fileops_re[16];
char *fileops_hist[HISTSIZE + 1] = { 0 };
char fileops_path[PATH_MAX + FILENAME_MAX];
void fileops_fromhist(void *data);
void fileops_reorder(void *data);
void fileops_getdir(void *data);
void fileops_load(void *data);
void fileops_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

time_t fileops_now;
ui_iconsel_t fileops_histsel = { ICON_HIST, NULL, { 0, 0, fileops_hist } };
ui_pathsel_t fileops_pathsel = { 0 };
ui_input_t fileops_searchinp = { INP_NAME, sizeof(fileops_search), fileops_search };
ui_input_t fileops_rsinp = { INP_NAME, sizeof(fileops_rs), fileops_rs };
ui_input_t fileops_reinp = { INP_NAME, sizeof(fileops_re), fileops_re };
ui_numptr_t fileops_mcnum = { &fileops_mc, 2, 256, 1 };
ui_numptr_t fileops_numw = { &newsprite_gridw, 16, 256, 1 };

ui_tablehdr_t fileops_table_hdr[] = {
    { -1, 20, 0, 0 },
    { FILEOP_NAME, 0, 1, 0 },
    { FILEOP_SIZE, 136, 1, 0 },
    { FILEOP_TIME, 160, 1, 0 },
    { 0 }
};

ui_table_t fileops_table = { fileops_table_hdr, 0, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0,
    sizeof(filelist_t), fileops_drawcell, NULL, fileops_reorder, NULL };

/**
 * The form
 */
ui_form_t fileops_form[] = {
    /* common */
    { FORM_ICONSEL, 10, 29, 20, 20, 0, FILEOP_HIST, &fileops_histsel, fileops_fromhist },
    { FORM_PATHSEL, 46, 30, 0, 18, 0, 0, &fileops_pathsel, fileops_getdir },
    { FORM_ICON, 0, 30, 16, 16, 0, 0, (void*)ICON_SRCH, NULL },
    { FORM_INPUT, 0, 29, 120, 18, 0, 0, &fileops_searchinp, fileops_getdir },
    { FORM_TABLE, 10, 54, 0, 0, 0, 0, &fileops_table, fileops_load },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, fileops_load },
    { FORM_BOOL, 0, 30, 16, 16, 0, FILEOP_ALL, &fileops_all, fileops_getdir },
    /* 7 fonts only */
    { FORM_TEXT, 20, 0, 0, 16, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 0, 80, 18, 0, 0, &fileops_rsinp, NULL },
    { FORM_INPUT, 0, 0, 80, 18, 0, 0, &fileops_reinp, NULL },
    /* 10 palettes only */
    { FORM_TEXT, 20, 0, 0, 16, 0, 0, NULL, NULL },
    { FORM_NUMPTR, 0, 0, 40, 18, 0, 0, &fileops_mcnum, NULL },
    /* 12 sprites only */
    { FORM_TEXT, 20, 0, 0, 16, 0, 0, NULL, NULL },
    { FORM_NUMPTR, 0, 0, 40, 18, 0, 0, &fileops_numw, NULL },
    { FORM_TEXT, 0, 0, 16, 16, 0, 0, NULL, NULL },
    { FORM_NUMPTR, 0, 0, 40, 18, 0, 0, &newsprite_numf, NULL },
    { FORM_TEXT, 0, 0, 16, 16, 0, 0, NULL, NULL },
    { FORM_CHRDBTN, 0, 0, 20, 20, 0, 0, &newsprite_dir, newsprite_chdir },
    { FORM_TEXT, 0, 0, 16, 16, 0, 0, NULL, NULL },
    { FORM_BOOL, 0, 0, 16, 16, 0, FILEOP_SELFRM, &newsprite_gridA, NULL },
    /* 20 */
    { FORM_LAST }
};

/**
 * Sort file names
 */
static int fncmp(const void *a, const void *b)
{
    filelist_t *A = (filelist_t*)a, *B = (filelist_t*)b;
    if(fileops_table.order < 3) {
        if(A->type != ICON_DIR && B->type == ICON_DIR) return 1;
        if(A->type == ICON_DIR && B->type != ICON_DIR) return -1;
    }
    switch(fileops_table.order) {
        case 1: return fileops_table.hdr[fileops_table.order].order == 1 ?
            strcasecmp(A->name, B->name) : strcasecmp(B->name, A->name);
        case 2: return fileops_table.hdr[fileops_table.order].order == 1 ?
            A->size > B->size : B->size > A->size;
        case 3: return fileops_table.hdr[fileops_table.order].order == 1 ?
            (int)(A->time - B->time) : (int)(B->time - A->time);
    }
    return 0;
}

/**
 * Reorder table entries
 */
void fileops_reorder(void *data)
{
    ui_table_t *table = (ui_table_t*)data;
    if(!table) return;
    qsort(table->data, table->num, table->entsize, fncmp);
    table->val = 0;
}

/**
 * Get path from history
 */
void fileops_fromhist(void *data)
{
    (void)data;
    ui_pathsel_set(&fileops_pathsel, fileops_histsel.sel.opts[fileops_histsel.sel.val]);
    fileops_getdir(&fileops_pathsel);
}

/**
 * Draw a file record
 */
void fileops_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    filelist_t *files = (filelist_t *)data;
    uint64_t size;
    uint32_t f = ssfn_dst.fg;
    struct tm *lt;
    time_t diff;
    char tmp[32];
    int i;

    (void)w; (void)h;

    if(sel != idx && files->type == ICON_DIR) ssfn_dst.fg = (theme[THEME_FG] - 0x202020);
    ui_icon(dst, x + 4, y, files->type == -1 ? ICON_FPIC : files->type);
    x += 8 + hdr[0].w;

    ui_text(dst, x, y, files->name);
    x += hdr[1].w;

    memset(tmp, ' ', sizeof(tmp));
    size = files->size; i = 15; tmp[i] = 0;
    do { tmp[--i] = '0' + (size % 10); size /= 10; } while(size != 0 && i > 0);
    ui_text(dst, x, y, tmp);
    x += hdr[2].w;

    diff = fileops_now - files->time;
    if(diff < 120) strcpy(tmp, lang[FILEOP_NOW]); else
    if(diff < 3600) sprintf(tmp, lang[FILEOP_MSAGO], (int)(diff/60)); else
    if(diff < 7200) sprintf(tmp, lang[FILEOP_HAGO], (int)(diff/60)); else
    if(diff < 24*3600) sprintf(tmp, lang[FILEOP_HSAGO], (int)(diff/3600)); else
    if(diff < 48*3600) strcpy(tmp, lang[FILEOP_YESTERDAY]); else {
        lt = localtime(&files->time);
        /* make the language checker script happy:
         * FILEOP_WDAY1, FILEOP_WDAY2, FILEOP_WDAY3, FILEOP_WDAY4, FILEOP_WDAY5, FILEOP_WDAY6 */
        if(files->time < 7*24*3600) strcpy(tmp, lang[FILEOP_WDAY0 + lt->tm_wday]); else
            sprintf(tmp, "%04d-%02d-%02d", lt->tm_year+1900, lt->tm_mon+1, lt->tm_mday);
    }
    ui_text(dst, x, y, tmp);
    ssfn_dst.fg = f;
}

/**
 * Get directory list
 */
void fileops_getdir(void *elem)
{
    filelist_t *files = (filelist_t *)fileops_table.data;
#ifdef __WIN32__
    _WDIR *dir;
    struct _wdirent *ent;
    struct _stat64 st;
#else
    DIR *dir;
    struct dirent *ent;
    struct stat st;
#endif
    int i, j, k, l, t;

    (void)elem;
    ui_pathsel_get(&fileops_pathsel, fileops_path);
    if(files) {
        for(i = 0; i < fileops_table.num; i++)
            if(files[i].name)
                free(files[i].name);
        free(files);
    }
    fileops_table.data = files = NULL;
    fileops_table.scr = fileops_table.num = 0;
    fileops_table.clk = -1;
    if(ui_input_type)
        strcpy(fileops_search, ui_input_buf);
    l = strlen(fileops_search);
    j = strlen(fileops_path);
#ifdef __WIN32__
    MultiByteToWideChar(CP_UTF8, 0, fileops_path, -1, szFull, PATH_MAX);
    if((dir = _wopendir(szFull)) != NULL) {
        while((ent = _wreaddir(dir))) {
            if(!fileops_all && ent->d_name[0] == L'.') continue;
            wdname[WideCharToMultiByte(CP_UTF8, 0, ent->d_name, wcslen(ent->d_name), wdname, FILENAME_MAX, NULL, NULL)] = 0;
            strcpy(fileops_path + j, dname);
            MultiByteToWideChar(CP_UTF8, 0, fileops_path, -1, szFull, PATH_MAX);
            if(_wstat64(szFull, &st)) continue;
#else
    if((dir = opendir(fileops_path)) != NULL) {
        while((ent = readdir(dir))) {
            if(!fileops_all && ent->d_name[0] == '.') continue;
            strcpy(fileops_path + j, dname);
            if(stat(fileops_path, &st)) continue;
#endif
            if(!strcmp(dname, ".") || !strcmp(dname, "..") || (!S_ISREG(st.st_mode) && !S_ISDIR(st.st_mode))) continue;
            k = strlen(dname);
            if(!S_ISDIR(st.st_mode)) {
                if(k < 3) continue;
                if(!memcmp(dname + k - 3, ".gz", 3)) k -= 3;
                if(k < 5) continue;
                t = ICON_FILE;
                if( !memcmp(dname + k - 4, ".sfn", 4) || !memcmp(dname + k - 4, ".pf2", 4) ||
                    !memcmp(dname + k - 4, ".pfa", 4) || !memcmp(dname + k - 4, ".pfb", 4) ||
                    !memcmp(dname + k - 5, ".woff",5) || !memcmp(dname + k - 5, "woff2",5) ||
                    !memcmp(dname + k - 4, ".ttf", 4) || !memcmp(dname + k - 4, ".ttc", 4) ||
                    !memcmp(dname + k - 4, ".otf", 4) || !memcmp(dname + k - 4, ".pcf", 4) ||
                    !memcmp(dname + k - 4, ".sfd", 4) || !memcmp(dname + k - 4, ".svg", 4) ||
                    !memcmp(dname + k - 4, ".fnt", 4) || !memcmp(dname + k - 4, ".fon", 4) ||
                    !memcmp(dname + k - 4, ".psf", 4) || !memcmp(dname + k - 5, ".psfu",5) ||
                    !memcmp(dname + k - 4, ".bdf", 4) || !memcmp(dname + k - 4, ".hex", 4)) t = ICON_FFONT; else
                if( !memcmp(dname + k - 4, ".jpg", 4) || !memcmp(dname + k - 5, ".jpeg",5) ||
                    !memcmp(dname + k - 4, ".tga", 4) || !memcmp(dname + k - 4, ".bmp", 4) ||
                    !memcmp(dname + k - 4, ".psd", 4) || !memcmp(dname + k - 4, ".xcf", 4) ||
                    !memcmp(dname + k - 4, ".pnm", 4) || !memcmp(dname + k - 4, ".gif", 4) ||
                    !memcmp(dname + k - 4, ".png", 4) || !memcmp(dname + k - 5, ".webp",5))  t = ICON_FPIC; else
                if( !memcmp(dname + k - 4, ".m3d", 4) || !memcmp(dname + k - 4, ".a3d", 4) ||
                    !memcmp(dname + k - 4, ".obj", 4) || (newsprite_assimp && (
                    /* TODO: list only extensions which are stable with assimp */
                    !memcmp(dname + k - 4, ".3ds", 4) || !memcmp(dname + k - 4, ".b3d", 4) ||
                    !memcmp(dname + k - 4, ".dae", 4) || !memcmp(dname + k - 8, ".collada",8) ||
                    !memcmp(dname + k - 4, ".dxf", 4) || !memcmp(dname + k - 6, ".blend",6) ||
                    !memcmp(dname + k - 4, ".fbx", 4) || !memcmp(dname + k - 4, ".glb", 4) ||
                    !memcmp(dname + k - 5, ".mesh",5) || !memcmp(dname + k - 5, ".ms3d",5) ||
                    !memcmp(dname + k - 4, ".ply", 4) || !memcmp(dname + k - 4, ".stl", 4) ||
                    !memcmp(dname + k - 4, ".x3d", 4) || !memcmp(dname + k - 4, ".xgl", 4)
                    )))  t = ICON_FOBJ; else
                if( !memcmp(dname + k - 4, ".ogg", 4) || !memcmp(dname + k - 4, ".mp3", 4) ||
                    !memcmp(dname + k - 4, ".wav", 4) || !memcmp(dname + k - 4, ".voc", 4) ||
                    !memcmp(dname + k - 5, ".aiff",5) || !memcmp(dname + k - 4, ".mod", 4) ||
                    !memcmp(dname + k - 3, ".xm",  3) || !memcmp(dname + k - 4, ".s3m", 4) ||
                    !memcmp(dname + k - 3, ".it",  3)) t = ICON_FSND; else
                if( !memcmp(dname + k - 4, ".ogv", 4) || (media_ffmpeg && (
                    !memcmp(dname + k - 4, ".avi", 4) || !memcmp(dname + k - 4, ".mpg", 4) ||
                    !memcmp(dname + k - 4, ".mkv", 4) || !memcmp(dname + k - 5, ".mpeg",5) ||
                    !memcmp(dname + k - 4, ".mp4", 4) || !memcmp(dname + k - 4, ".mov", 4) ||
                    !memcmp(dname + k - 4, ".asf", 4) || !memcmp(dname + k - 4, ".wmv", 4) ||
                    !memcmp(dname + k - 5, ".webm",5) || !memcmp(dname + k - 4, ".gif", 4) ||
                    !memcmp(dname + k - 5, ".mjpg",5) || !memcmp(dname + k - 6, ".mjpeg",6) ||
                    !memcmp(dname + k - 4, ".flv", 4) || !memcmp(dname + k - 4, ".flc", 4)
                    ))) t = ICON_FMOV; else
                if( !memcmp(dname + k - 4, ".gpl", 4) || !memcmp(dname + k - 4, ".pal", 4) ||
                    !memcmp(dname + k - 4, ".aco", 4) || !memcmp(dname + k - 4, ".ase", 4)) t = ICON_FPAL; else
                if( !memcmp(dname + k - 4, ".tng", 4)) t = ICON_FTNG;
                if( !memcmp(dname + k - 4, ".zip", 4)) t = ICON_FZIP;
                if(!fileops_all) {
                    if(fileops_type == FILEOPS_TNG &&  t != ICON_FTNG) continue;
                    if(fileops_type == FILEOPS_ZIP &&  t != ICON_FZIP) continue;
                    if(fileops_type == FILEOPS_SPRITE && t != ICON_FPIC && t != ICON_FOBJ) continue;
                    if((fileops_type == FILEOPS_AUDIO || fileops_type == FILEOPS_TRANSLATION) && t != ICON_FSND) continue;
                    if(fileops_type == FILEOPS_MOVIE &&  t != ICON_FMOV) continue;
                    if(fileops_type == FILEOPS_FONT) {
                        if(!memcmp(dname + k - 4, ".png", 4) || !memcmp(dname + k - 4, ".tga", 4)) t = ICON_FFONT;
                        if(t != ICON_FFONT) continue;
                    }
                    if(fileops_type == FILEOPS_PALETTE) { if(t == ICON_FPIC) { t = ICON_FPAL; } if(t != ICON_FPAL) continue; }
                    if(t == ICON_QMARK) continue;
                }
            } else t = ICON_DIR;
            if(l) {
                if(k < l) continue;
                k -= l;
                for(i = 0; i <= k && strncasecmp(dname + i, fileops_search, l); i++);
                if(i > k) continue;
            }
            i = fileops_table.num++;
            files = (filelist_t*)realloc(files, fileops_table.num * sizeof(filelist_t));
            if(!files) { fileops_table.num = 0; break; }
            files[i].name = (char*)malloc(strlen(dname)+1);
            if(!files[i].name) { fileops_table.num--; continue; }
            strcpy(files[i].name, dname);
            files[i].type = t;
            files[i].size = st.st_size;
            files[i].time = st.st_mtime;
        }
#ifdef __WIN32__
        _wclosedir(dir);
#else
        closedir(dir);
#endif
    }
    fileops_path[j] = 0;
    qsort(files, fileops_table.num, sizeof(filelist_t), fncmp);
    fileops_table.data = files;
    if(fileops_table.val >= fileops_table.num)
        fileops_table.val = fileops_table.num - 1;
    /* if stepping back, set the current selected record to the next dir in path */
    if(fileops_pathsel.fld == -2 && fileops_pathsel.path[fileops_pathsel.num].dir) {
        fileops_pathsel.fld = -1;
        l = strlen(fileops_pathsel.path[fileops_pathsel.num].dir) - 1;
        for(i = 0; i < fileops_table.num; i++)
            if(!memcmp(files[i].name, fileops_pathsel.path[fileops_pathsel.num].dir, l) && !files[i].name[l]) {
                fileops_table.val = i; break;
            }
    }
}

/**
 * Add to path history
 */
void fileops_histadd()
{
    ui_pathsel_get(&fileops_pathsel, fileops_path);
    if(fileops_hist[HISTSIZE - 1]) free(fileops_hist[HISTSIZE - 1]);
    memmove(&fileops_hist[1], &fileops_hist[0], (HISTSIZE - 1) * sizeof(char*));
    fileops_hist[0] = (char*)main_alloc(strlen(fileops_path) + 1);
    strcpy(fileops_hist[0], fileops_path);
}

/**
 * Exit fileops window
 */
void fileops_exit(int tab)
{
    filelist_t *files = (filelist_t *)fileops_table.data;
    int i;

    (void)tab;
    if(!files) return;
    for(i = 0; i < fileops_table.num; i++)
        if(files[i].name)
            free(files[i].name);
    free(files);
    fileops_table.data = NULL;
}

/**
 * Enter fileops window
 */
void fileops_init(int tab)
{
    int i;

    fileops_exit(tab);
    if(tab == SUBMENU_IMPZIP) fileops_type = FILEOPS_ZIP;
    if(tab == SUBMENU_IMPORT && fileops_type == FILEOPS_ZIP) fileops_type = FILEOPS_TNG;
    if(!fileops_pathsel.num && !fileops_pathsel.sel.opts) {
        ui_pathsel_getdrives(&fileops_pathsel);
        *projfn = 0;
        ui_pathsel_set(&fileops_pathsel, projdir);
        if(fileops_pathsel.num) fileops_pathsel.num--;
        fileops_pathsel.fld = -2;
    }
    memset(fileops_search, 0, sizeof(fileops_search));
    fileops_table.order = 1;
    fileops_getdir(&fileops_pathsel);
    fileops_form[5].param = lang[fileops_type == FILEOPS_SPRITE ? FILEOP_LOAD : FILEOP_IMPORT];
    fileops_form[5].w = ui_textwidth(fileops_form[5].param) + 40;
    if(fileops_form[5].w < 200) fileops_form[5].w = 200;
    fileops_form[7].param = lang[FILEOP_RANGE];
    fileops_form[7].w = ui_textwidth(fileops_form[7].param);
    fileops_form[8].x = fileops_form[7].x + fileops_form[7].w + 8;
    fileops_form[9].x = fileops_form[8].x + fileops_form[8].w + 8;
    strcpy(fileops_rs, "U+0"); strcpy(fileops_re, "U+10FFFF");
    fileops_form[10].param = lang[FILEOP_COLORS];
    fileops_form[10].w = ui_textwidth(fileops_form[10].param);
    fileops_form[11].x = fileops_form[10].x + fileops_form[10].w + 8;
    fileops_form[12].param = lang[FILEOP_RAST];
    fileops_form[12].w = ui_textwidth(fileops_form[12].param);
    fileops_form[13].x = fileops_form[12].x + fileops_form[12].w + 8;
    fileops_form[14].x = fileops_form[13].x + fileops_form[13].w + 12;
    fileops_form[14].param = lang[FILEOP_NUMFPS];
    fileops_form[14].w = ui_textwidth(fileops_form[14].param);
    fileops_form[15].x = fileops_form[14].x + fileops_form[14].w + 8;
    fileops_form[16].x = fileops_form[15].x + fileops_form[15].w + 12;
    fileops_form[16].param = lang[FILEOP_FACING];
    fileops_form[16].w = ui_textwidth(fileops_form[16].param);
    fileops_form[17].x = fileops_form[16].x + fileops_form[16].w + 8;
    fileops_form[18].x = fileops_form[17].x + fileops_form[17].w + 12;
    fileops_form[18].param = lang[FILEOP_MANUAL];
    fileops_form[18].w = ui_textwidth(fileops_form[18].param);
    fileops_form[19].x = fileops_form[18].x + fileops_form[18].w + 8;
    /* disable all optional form elements, only enable what's needed */
    for(i = 7; fileops_form[i].type; i++)
        fileops_form[i].inactive = 2;
    switch(fileops_type) {
        case FILEOPS_FONT:
            fileops_form[7].inactive = fileops_form[8].inactive = fileops_form[9].inactive = 0;
        break;
        case FILEOPS_PALETTE:
            fileops_form[10].inactive = fileops_form[11].inactive = 0;
        break;
    }
}

/**
 * Free current path and history (kept across tab switches, not freed by fileops_exit)
 */
void fileops_free()
{
    int i;

    project_freedir(&fileops_pathsel.sel.opts, NULL);
    for(i = 0; i < 256; i++)
        if(fileops_pathsel.path[i].dir) {
            free(fileops_pathsel.path[i].dir);
            fileops_pathsel.path[i].dir = NULL;
        }
    for(i = 0; i < HISTSIZE; i++)
        if(fileops_hist[i])
            free(fileops_hist[i]);
}

/**
 * Resize the view
 */
void fileops_resize(int tab, int w, int h)
{
    (void)tab;
    fileops_form[0].w = w - 40;
    fileops_form[1].w = w - 138 - 16 - 20 - 46;
    fileops_form[2].x = w - 134 - 16;
    fileops_form[3].x = w - 130;
    ui_table_resize(&fileops_form[4], w - 20, h - 114);
    fileops_form[5].y = fileops_form[7].y = fileops_form[8].y = fileops_form[9].y = fileops_form[10].y = fileops_form[11].y =
        fileops_form[12].y = fileops_form[13].y = fileops_form[14].y = fileops_form[15].y = fileops_form[16].y =
        fileops_form[17].y = fileops_form[18].y = fileops_form[19].y = h - 48;
    fileops_form[5].x = w - 20 - fileops_form[5].w;
    fileops_form[6].x = fileops_form[1].x + fileops_form[1].w;
}

/**
 * View layer
 */
void fileops_redraw(int tab)
{
    filelist_t *files = (filelist_t *)fileops_table.data;

    (void)tab;
    ui_form = fileops_form;
    fileops_form[0].inactive = (fileops_hist[0] == NULL);
    fileops_now = time(NULL);
    fileops_form[12].inactive = fileops_form[13].inactive = fileops_form[14].inactive = fileops_form[15].inactive =
      fileops_form[16].inactive = fileops_form[17].inactive = fileops_form[18].inactive = fileops_form[19].inactive =
      (fileops_type == FILEOPS_SPRITE && files && files[fileops_table.val].type == ICON_FOBJ ? 0 : 2);
}

/**
 * File selected
 */
void fileops_load(void *data)
{
    filelist_t *files = (filelist_t *)fileops_table.data;
    char *s;

    (void)data;
    memset(fileops_search, 0, sizeof(fileops_search));
    if(!files || fileops_table.val < 0 || fileops_table.val > fileops_table.num - 1) return;
    if(files[fileops_table.val].type == ICON_DIR) {
        ui_pathsel_add(&fileops_pathsel, files[fileops_table.val].name);
        fileops_getdir(&fileops_pathsel);
        fileops_table.val = (fileops_table.num > 0 ? 0 : -1);
    } else {
        fileops_form[5].inactive = 1;
        ui_redraw(5, 5);
        ui_render();
        SDL_RenderPresent(renderer);
        fileops_form[5].inactive = 0;
        fileops_histadd();
        strcat(fileops_path, files[fileops_table.val].name);
        switch(fileops_type) {
            case FILEOPS_TNG:
                if(!project_fromtng(fileops_path)) { ui_status(1, lang[ERR_LOADTNG]); return; }
                ui_switchtab(SUBMENU_MAPS);
            break;
            case FILEOPS_ZIP:
                if(!project_fromtemplate(project.id, fileops_path)) return;
                ui_switchtab(SUBMENU_ABOUT);
            break;
            case FILEOPS_FONT:
                if((fileops_rs[0] == 'U' || fileops_rs[0] == 'u') && fileops_rs[1] == '+') rs = (int)gethex(fileops_rs + 2, 6);
                else { s = fileops_rs; rs = (int)ssfn_utf8(&s); }
                if(rs < 0) rs = 0;
                if(rs > 0x10FFFF) rs = 0x10FFFF;
                if((fileops_re[0] == 'U' || fileops_re[0] == 'u') && fileops_re[1] == '+') re = (int)gethex(fileops_re + 2, 6);
                else { s = fileops_re; re = (int)ssfn_utf8(&s); }
                if(re < 0) re = 0;
                if(re > 0x10FFFF) re = 0x10FFFF;
                if(!fonts_import(fileops_path)) return;
                ui_switchtab(SUBMENU_FONTS);
            break;
            case FILEOPS_SPRITE:
                if(files[fileops_table.val].type == ICON_FOBJ) {
                    if(!newsprite_loadmodel(fileops_path)) return;
                } else {
                    if(!newsprite_loadsheet(fileops_path)) return;
                }
                ui_switchtab(SUBMENU_NEWSPRITE);
            break;
            case FILEOPS_PALETTE:
                if(!palette_save(fileops_path, fileops_mc)) { ui_status(1, lang[ERR_PALETTE]); return; }
                ui_switchtab(SUBMENU_NEWSPRITE);
                ui_popup_type = 0; ui_pressed = 10; ui_palette(ui_form[ui_pressed].x, ui_form[ui_pressed].y);
            break;
            case FILEOPS_TRANSLATION:
            case FILEOPS_AUDIO:
            case FILEOPS_MOVIE:
                if(!media_import(fileops_path)) return;
                ui_switchtab(ui_oldtab + SUBMENU_NEWSPRITE);
            break;
        }
        fileops_type = FILEOPS_TNG;
    }
}
