/*
 * tnge/project.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Project object header
 *
 */

#include <stdint.h>

#define COMMONDIR "common"
#define CREDITSFN "CREDITS.txt"

#define ISOP(x) (x == '-' || x == '+' || x == '*' || x == '/' || x == '%' || x == '=' || x == '<' || x == '>' || x == '!' || \
    x == '|' || x == '&')

#define PROJDIRS_MEDIA      0
#define PROJDIRS_SPRITES    4
#define PROJDIRS_UI         PROJDIRS_SPRITES
#define PROJDIRS_TILES      5
#define PROJDIRS_CHARS      6
#define PROJDIRS_OBJECTS    9
#define PROJDIRS_FONTS      10
#define PROJDIRS_GAME       11
#define PROJDIRS_NPCS       12
#define PROJDIRS_DIALOGS    13
#define PROJDIRS_MAPS       14
#define PROJDIRS_QUESTS     15
#define PROJDIRS_LANG       16
#define PROJDIRS_EXTS       17

#define PROJ_NUMSPRCATS     7

typedef struct {
    char id[17], name[PROJ_TITLEMAX], uuid[16], author[96], url[256];
    char **languages, **fonts, **music, **sounds, **speech, **movies, **choosers;
    char **cuts, **attrs, **chars, **dialogs, **crafts, **maps, **quests;
    int rev, hasGame, chk, langw, numlang, numfont, nummusic, numsounds, numspeech, nummovies, numchoosers;
    int numcuts, numattrs, numchars, numdialogs, numcrafts, nummaps, numquests, *mapidx;
    int type, tilew, tileh, mapsize;
    int spritenum[PROJ_NUMSPRCATS];
    ui_sprlist_t *sprites[PROJ_NUMSPRCATS];
} project_t;

#define PROJMAGIC_PALETTE   "GIMP Palette"
#define PROJMAGIC_CONFIG    "TirNanoG Project"
#define PROJMAGIC_EXTCFG    "TirNanoG Extension"
#define PROJMAGIC_LAYOUT    "TirNanoG Layout"
#define PROJMAGIC_ELEMENTS  "TirNanoG Elements"
#define PROJMAGIC_MAINMENU  "TirNanoG MainMenu"
#define PROJMAGIC_HUD       "TirNanoG HUD"
#define PROJMAGIC_ALERT     "TirNanoG Alerts"
#define PROJMAGIC_CREDITS   "TirNanoG Credits"
#define PROJMAGIC_CHOOSER   "TirNanoG Chooser"
#define PROJMAGIC_CUTSCENE  "TirNanoG CutScene"
#define PROJMAGIC_STARTUP   "TirNanoG StartUp"
#define PROJMAGIC_ACTION    "TirNanoG Action"
#define PROJMAGIC_ATTRCONF  "TirNanoG AttrConf"
#define PROJMAGIC_ATTR      "TirNanoG Attribute"
#define PROJMAGIC_CHARCONF  "TirNanoG CharBase"
#define PROJMAGIC_CHAROPT   "TirNanoG CharOptions"
#define PROJMAGIC_NPC       "TirNanoG NPC"
#define PROJMAGIC_SPAWNER   "TirNanoG Spawner"
#define PROJMAGIC_DIALOG    "TirNanoG Dialog"
#define PROJMAGIC_OBJECT    "TirNanoG Object"
#define PROJMAGIC_CRAFT     "TirNanoG CraftRules"
#define PROJMAGIC_TILE      "TirNanoG Tile"
#define PROJMAGIC_MAPTMX    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
#define PROJMAGIC_QUEST     "TirNanoG Quest"
#define PROJMAGIC_TRANSLATE "# TirNanoG Translation"
#define PROJMAGIC_ATLAS     "TirNanoG Atlas"

extern project_t project;
extern uint64_t project_curr, project_total, project_last;
extern char *projdir, *projfn, *project_rels[];
extern const char *project_dirs[], *project_directions[], *project_animtypes, *project_animframes, *project_eventcodes[];
extern char *project_behs[], *project_trns[], *project_clock[], *project_actions[], *project_eventtimers[];

void project_fseek(FILE *f, int64_t offs);
uint32_t project_filesize(char *path);
uint64_t project_getsizes(char *path, char *ext);
char **project_getdir(char *path, char *ext, int *num);
void project_freedir(char ***list, int *num);
void project_initdir(int menu);
void project_mkdir(char *path);
void project_rm(char *path);
FILE *project_fopen(const char *fn, const char *mode);
int  project_exists(char *id);
void project_loadlicense(char *id);
void project_load(char *id);
void project_loadlangs();
void project_loadactions();
char **project_loadobjs(int *num);
void project_save();
void project_free();
char *project_parsestr(char *str);
char *project_loadfile(const char *dir, char *fn, char *ext, const char *magic, const char *func);
FILE *project_savefile(int confirm, const char *dir, char *fn, char *ext, const char *magic, const char *func);
char *project_skipnl(char *str);
char *project_skiparg(char *str, int cnt);
char *project_skipscript(char *str);
char *project_getint(char *str, int *ret, int min, int max);
char *project_getidx(char *str, int *ret, char **opts, int def);
char *project_getobj(char *str, int *ret, int def);
char *project_getrel(char *str, int *ret);
char *project_getstr(char *str, char **ret, int type, int maxlen);
char *project_getstr2(char *str, char *ret, int type, int maxlen);
char *project_gettrstr(char *str, int type, int maxlen);
char *project_getfont(char *str, int *style, int *size, int *family);
char *project_getcolor(char *str, uint32_t *color);
char *project_getsprite(char *str, ui_sprsel_t *spr);
char *project_getscript(char *str, ui_cmd_t *cmd);
void project_escapestr(char *s, char *d);
void project_wridx(FILE *f, int idx, char **opts);
void project_wrrel(FILE *f, int idx);
void project_wrfont(FILE *f, int style, int size, int family);
void project_wrsprite(FILE *f, ui_sprsel_t *spr);
void project_wrsprite2(FILE *f, int val, int cat);
void project_wrscript(FILE *f, ui_cmd_t *cmd);
void project_wrmsg(FILE *f, char *id, char *str, int len);
void project_wrstr(FILE *f, char *str);
int  project_ffmpeg(char *from, char *to);
void project_help();
void project_play();
void project_delete(char *id);
void project_encbase(unsigned char *out);
char *project_encode(unsigned char *s);
void project_decode(char *s, unsigned char *o);
int  project_fromtemplate(char *id, char *zip);
void project_totemplate(int menu);
void project_togame(int menu);
int  project_fromtng(char *fn);
int  project_totng(char *name, int flags, char *opts, char *langs, char *maps);
