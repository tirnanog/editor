/*
 * tnge/image.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Image handling function
 *
 */

#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_LINEAR
#define STBI_NO_HDR
#define STBI_NO_FAILURE_STRINGS
#include "stb_image.h"
#include "main.h"
#include "libpng/png.h"
#include "libwebp/src/webp/decode.h"
#include "libwebp/src/webp/demux.h"

SDL_Surface *IMG_LoadXCF_RW(SDL_RWops *src);

/**
 * Save an image as png
 */
int image_save(FILE *f, int w, int h, int p, uint8_t *data)
{
    png_structp png_ptr;
    png_infop info_ptr;
    png_bytep *rows;
    int i;

    if(!f || !data || w < 1 || h < 1 || p < 4) return 0;
    rows = (png_bytep*)main_alloc(h * sizeof(png_bytep));
    for(i = 0; i < h; i++) rows[i] = data + i * p;
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png_ptr) { free(rows); return 0; }
    info_ptr = png_create_info_struct(png_ptr);
    if(!info_ptr) { png_destroy_write_struct(&png_ptr, NULL); free(rows); return 0; }
    if(setjmp(png_jmpbuf(png_ptr))) { png_destroy_write_struct(&png_ptr, &info_ptr); free(rows); return 0; }
    png_init_io(png_ptr, f);
    /* aim for fast encoding, even if it results in bigger files */
    png_set_compression_level(png_ptr, 2);
    png_set_compression_strategy(png_ptr, 2);
    png_set_filter(png_ptr, PNG_FILTER_TYPE_BASE, PNG_FILTER_VALUE_NONE);
    png_set_IHDR(png_ptr, info_ptr, w, h, 8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    png_write_info(png_ptr, info_ptr);
    png_write_image(png_ptr, rows);
    png_write_end(png_ptr, info_ptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    free(rows);
    return 1;
}

/**
 * Save an image as png into a memory buffer
 */
typedef struct {
    uint8_t *buf;
    int len;
} image_png_t;
static void image_write(png_structp png_ptr, png_bytep data, png_size_t len)
{
    image_png_t *pngbuf = (image_png_t*)png_get_io_ptr(png_ptr);
    pngbuf->buf = (uint8_t*)realloc(pngbuf->buf, pngbuf->len + len);
    if(!pngbuf->buf) { pngbuf->len = 0; return; }
    memcpy(pngbuf->buf + pngbuf->len, data, len);
    pngbuf->len += len;
}
static void image_flush(png_structp png_ptr)
{
    (void)png_ptr;
}
uint8_t *image_mem(int w, int h, uint8_t *data, int *len, char *com)
{
    image_png_t pngbuf = { 0 };
    uint32_t *ptr = (uint32_t*)data, pal[256];
    uint8_t *data2, *pal2 = (uint8_t*)&pal;
    png_color pngpal[256];
    png_byte pngtrn[256];
    png_structp png_ptr;
    png_infop info_ptr;
    png_bytep *rows;
    png_text texts[1] = { 0 };
    int i, j, nc = 0;

    if(len) *len = 0;
    if(!data || w < 1 || h < 1) return NULL;
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png_ptr) return NULL;
    info_ptr = png_create_info_struct(png_ptr);
    if(!info_ptr) { png_destroy_write_struct(&png_ptr, NULL); return NULL; }
    if(setjmp(png_jmpbuf(png_ptr))) { png_destroy_write_struct(&png_ptr, &info_ptr); return NULL; }
    png_set_write_fn(png_ptr, (void*)&pngbuf, image_write, image_flush);
    /* aim for small file size, even if it means slower encoding */
    png_set_compression_level(png_ptr, 9);
    png_set_compression_strategy(png_ptr, 0);
    png_set_filter(png_ptr, PNG_FILTER_TYPE_BASE, PNG_FILTER_VALUE_SUB);
    rows = (png_bytep*)main_alloc(h * sizeof(png_bytep));
    data2 = (uint8_t*)main_alloc(w * h);
    /* lets see if we can save this as an indexed image */
    for(i = 0; i < w * h; i++) {
        for(j = 0; j < nc && pal[j] != ptr[i]; j++);
        if(j >= nc) {
            if(nc == 256) { nc = -1; break; }
            pal[nc++] = ptr[i];
        }
        data2[i] = j;
    }
    if(nc != -1) {
        for(i = j = 0; i < nc; i++) {
            pngpal[i].red = pal2[i * 4 + 0];
            pngpal[i].green = pal2[i * 4 + 1];
            pngpal[i].blue = pal2[i * 4 + 2];
            pngtrn[i] = pal2[i * 4 + 3];
        }
        png_set_PLTE(png_ptr, info_ptr, pngpal, nc);
        png_set_tRNS(png_ptr, info_ptr, pngtrn, nc, NULL);
        for(i = 0; i < h; i++) rows[i] = data2 + i * w;
    } else
        for(i = 0; i < h; i++) rows[i] = data + i * w * 4;
    png_set_IHDR(png_ptr, info_ptr, w, h, 8, nc == -1 ? PNG_COLOR_TYPE_RGB_ALPHA : PNG_COLOR_TYPE_PALETTE, PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    if(com && *com) {
        texts[0].key = "Comment"; texts[0].text = com;
        png_set_text(png_ptr, info_ptr, texts, 1);
    }
    png_write_info(png_ptr, info_ptr);
    png_write_image(png_ptr, rows);
    png_write_end(png_ptr, info_ptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    free(rows);
    free(data2);
    if(len) *len = pngbuf.len;
    return pngbuf.buf;
}

/**
 * Load an image
 */
uint8_t *image_load(char *fn, int *w, int *h)
{
    FILE *f;
    stbi__context s;
    stbi__result_info ri;
    uint8_t *data = NULL, *buf;
    int i, c = 0, nf = 1;
    uint64_t size = 0;
    SDL_RWops *src;
    SDL_Surface *img;
    VP8StatusCode status;
    WebPBitstreamFeatures input;
    WebPIterator iter;
    struct WebPDemuxer *dm = NULL;
#ifndef __WIN32__
    struct stat st;
    if(!stat(fn, &st)) size = (uint64_t)st.st_size;
#endif

    *w = *h = 0;
    f = project_fopen(fn, "rb");
    if(!f) return NULL;
#ifdef __WIN32__
    size = (uint64_t)_filelengthi64(_fileno(f));
#endif
    project_curr += size;
    if(!size) { fclose(f); return NULL; }
    stbi__start_file(&s, f);
    if(stbi__gif_test(&s)) {
        data = stbi__load_gif_main(&s, NULL, w, h, &nf, &c, 4);
        if(data && *w > 0 && *h > 0 && nf > 1)
            *h *= nf;
    } else {
        data = stbi__load_main(&s, w, h, &c, 4, &ri, 8);
    }
    fclose(f);
    if(data && *w > 0 && *h > 0)
        return data;
    if(data) free(data);

    src = SDL_RWFromFile(fn, "rb");
    if(src) {
        img = IMG_LoadXCF_RW(src);
        if(img) {
            *w = img->w; *h = img->h;
            /*
            data = img->pixels;
            img->flags |= SDL_DONTFREE;
            */
            data = (uint8_t*)main_alloc(*w * *h * 4);
            memcpy(data, img->pixels, img->w * img->h * 4);
            SDL_FreeSurface(img);
            return data;
        }
        SDL_RWclose(src);
    }

    buf = (uint8_t*)main_alloc(size);
    f = project_fopen(fn, "rb");
    if(fread(buf, 1, size, f) == size) {
        fclose(f);
        memset(&input, 0, sizeof(input));
        status = WebPGetFeatures((uint8_t*)buf, (size_t)size, &input);
        if (status != VP8_STATUS_OK) goto Error;
        nf = WebPDemuxGetI(dm, WEBP_FF_FRAME_COUNT);
        if(nf < 1) nf = 1;
        data = (uint8_t*)malloc(nf * input.width * input.height * 4);
        if(!data) goto Error;
        if(nf == 1) {
            if(!WebPDecodeRGBAInto(buf, size, data, input.width * 4 * input.height, input.width * 4)) { free(data); goto Error; }
            *w = input.width; *h = input.height;
        } else {
            memset(data, 0, nf * input.width * input.height * 4);
            for(i = 0; i < nf; i++) {
                /* only report error if this is the first frame */
                if(WebPDemuxGetFrame(dm, i, &iter) == 0 && !i) { free(data); goto Error; }
                if(!WebPDecodeRGBAInto(iter.fragment.bytes, iter.fragment.size, data + i * input.width * input.height * 4,
                    input.width * 4 * input.height, input.width * 4) && !i) { free(data); goto Error; }
            }
            *w = input.width; *h = input.height * nf;
        }
        free(buf);
        return data;
    }
    fclose(f);
Error:
    free(buf);
    return NULL;
}

/*
 * the following is borrowed from IMG_xcf.c with very minor modifications
 * to work with TirNanoG Editor. Otherwise it is verbatim (I did not wanted
 * to introduce a new SDL_image dependency just because of one file format)
 *
 * UPDATE: I've added support for all the xcf files I could find that wasn't
 * working, mostly v0 and opacity stuff - bzt
 */

/*
  SDL_image:  An example image loading library for use with SDL
  Copyright (C) 2022 full xcf support by bzt <bzt@codeberg>
  Copyright (C) 1997-2019 Sam Lantinga <slouken@libsdl.org>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

/* This is a XCF image file loading framework */

#include "SDL_endian.h"
/* We'll use SDL for reporting errors */
#define IMG_SetError    SDL_SetError
#define IMG_GetError    SDL_GetError

#if DEBUG
static char prop_names [][30] = {
  "end",
  "colormap",
  "active_layer",
  "active_channel",
  "selection",
  "floating_selection",
  "opacity",
  "mode",
  "visible",
  "linked",
  "preserve_transparency",
  "apply_mask",
  "edit_mask",
  "show_mask",
  "show_masked",
  "offsets",
  "color",
  "compression",
  "guides",
  "resolution",
  "tattoo",
  "parasites",
  "unit",
  "paths",
  "user_unit",
  "vectors",
  "text_layer_flags",
  "sample_points",
  "lock_content",
  "group_item",
  "item_path",
  "group_item_flags",
  "lock_position",
  "float_opacity"
};
#endif


typedef enum
{
  PROP_END = 0,
  PROP_COLORMAP = 1,
  PROP_ACTIVE_LAYER = 2,
  PROP_ACTIVE_CHANNEL = 3,
  PROP_SELECTION = 4,
  PROP_FLOATING_SELECTION = 5,
  PROP_OPACITY = 6,
  PROP_MODE = 7,
  PROP_VISIBLE = 8,
  PROP_LINKED = 9,
  PROP_PRESERVE_TRANSPARENCY = 10,
  PROP_APPLY_MASK = 11,
  PROP_EDIT_MASK = 12,
  PROP_SHOW_MASK = 13,
  PROP_SHOW_MASKED = 14,
  PROP_OFFSETS = 15,
  PROP_COLOR = 16,
  PROP_COMPRESSION = 17,
  PROP_GUIDES = 18,
  PROP_RESOLUTION = 19,
  PROP_TATTOO = 20,
  PROP_PARASITES = 21,
  PROP_UNIT = 22,
  PROP_PATHS = 23,
  PROP_USER_UNIT = 24,
  PROP_VECTORS = 25,
  PROP_TEXT_LYRFLG = 26,
  PROP_SAMPLE_POINTS = 27,
  PROP_LOCK_CONTENT = 28,
  PROP_GROUP_ITEM = 29,
  PROP_ITEM_PATH = 30,
  PROP_GROUP_ITMFLG = 31,
  PROP_LOCK_POSITION = 32,
  PROP_FLOAT_OPACITY = 33
} xcf_prop_type;

typedef enum {
  COMPR_NONE    = 0,
  COMPR_RLE     = 1,
  COMPR_ZLIB    = 2,
  COMPR_FRACTAL = 3
} xcf_compr_type;

typedef enum {
  IMAGE_RGB       = 0,
  IMAGE_GREYSCALE = 1,
  IMAGE_INDEXED   = 2
} xcf_image_type;

typedef struct {
  Uint32 id;
  Uint32 length;
  union {
    struct {
      Uint32 num;
      char * cmap;
    } colormap;
    struct {
      Uint32 drawable_offset;
    } floating_selection;
    Sint32 opacity;
    Sint32 mode;
    int    visible;
    int    linked;
    int    preserve_transparency;
    int    apply_mask;
    int    show_mask;
    struct {
      Sint32 x;
      Sint32 y;
    } offset;
    unsigned char color [3];
    Uint8 compression;
    struct {
      Sint32 x;
      Sint32 y;
    } resolution;
    struct {
      char * name;
      Uint32 flags;
      Uint32 size;
      char * data;
    } parasite;
  } data;
} xcf_prop;

typedef struct {
  char   sign [14];
  Uint32 file_version;
  Uint32 width;
  Uint32 height;
  Sint32 image_type;
  Uint32 precision;
  xcf_prop * properties;

  Uint32 * layer_file_offsets;
  Uint32 * channel_file_offsets;

  xcf_compr_type compr;
  Uint32         cm_num;
  unsigned char * cm_map;
} xcf_header;

typedef struct {
  Uint32 width;
  Uint32 height;
  Sint32 layer_type;
  char * name;
  xcf_prop * properties;

  Uint32 hierarchy_file_offset;
  Uint32 layer_mask_offset;

  Uint32 offset_x;
  Uint32 offset_y;
  int visible;
  int opacity;
} xcf_layer;

typedef struct {
  Uint32 width;
  Uint32 height;
  char * name;
  xcf_prop * properties;

  Uint32 hierarchy_file_offset;

  Uint32 color;
  Uint32 opacity;
  int selection;
  int visible;
} xcf_channel;

typedef struct {
  Uint32 width;
  Uint32 height;
  Uint32 bpp;

  Uint32 * level_file_offsets;
} xcf_hierarchy;

typedef struct {
  Uint32 width;
  Uint32 height;

  Uint32 * tile_file_offsets;
} xcf_level;

typedef unsigned char * xcf_tile;

typedef unsigned char * (* load_tile_type) (SDL_RWops *, Uint32, int, int, int);


/* See if an image is contained in a data source */
int IMG_isXCF(SDL_RWops *src)
{
    Sint64 start;
    int is_XCF;
    char magic[14];

    if ( !src )
        return 0;
    start = SDL_RWtell(src);
    is_XCF = 0;
    if ( SDL_RWread(src, magic, sizeof(magic), 1) ) {
        if (SDL_strncmp(magic, "gimp xcf ", 9) == 0) {
            is_XCF = 1;
        }
    }
    SDL_RWseek(src, start, RW_SEEK_SET);
    return(is_XCF);
}

static char * read_string (SDL_RWops * src) {
  Sint64 remaining;
  Uint32 tmp;
  char * data;

  tmp = SDL_ReadBE32(src);
  remaining = SDL_RWsize(src) - SDL_RWtell(src);
  if (tmp > 0 && (Sint32)tmp <= remaining) {
    data = (char *) SDL_malloc (sizeof (char) * tmp);
    if (data) {
      SDL_RWread(src, data, tmp, 1);
      data[tmp - 1] = '\0';
    }
  }
  else {
    data = NULL;
  }
  return data;
}

static Uint64 read_offset (SDL_RWops * src, const xcf_header * h) {
  Uint64 offset;
  offset = (h->file_version >= 11) ? (Uint64)SDL_ReadBE32 (src) << 32 : 0;
  offset |= SDL_ReadBE32 (src);
  return offset;
}

static int xcf_read_property (SDL_RWops * src, const xcf_header * h, xcf_prop * prop) {
  Uint32 len, i;
  prop->id = SDL_ReadBE32 (src);
  prop->length = SDL_ReadBE32 (src);

#if DEBUG
  printf ("%08lX: %s(%u): %u\n", SDL_RWtell (src), prop->id < 34 ? prop_names [prop->id] : "unknown", prop->id, prop->length);
#endif

  switch (prop->id) {
  case PROP_COLORMAP:
    prop->data.colormap.num = SDL_ReadBE32 (src);
#if DEBUG
    printf("color num %u\n", prop->data.colormap.num);
#endif
    prop->data.colormap.cmap = (char *) SDL_malloc (sizeof (char) * prop->data.colormap.num * 3);
    if(h->file_version == 0) {
        if (SDL_RWseek (src, prop->data.colormap.num, RW_SEEK_CUR) < 0)
            return 0;
        for(i = 0; i < prop->data.colormap.num; i++) {
            prop->data.colormap.cmap[i * 3 + 0] = i;
            prop->data.colormap.cmap[i * 3 + 1] = i;
            prop->data.colormap.cmap[i * 3 + 2] = i;
        }
    } else
        SDL_RWread (src, prop->data.colormap.cmap, prop->data.colormap.num*3, 1);
#if DEBUG
    printf("after colormap %08lX\n", SDL_RWtell (src));
#endif
    break;

  case PROP_OFFSETS:
    prop->data.offset.x = SDL_ReadBE32 (src);
    prop->data.offset.y = SDL_ReadBE32 (src);
#if DEBUG
    printf("offsets x %u y %u\n", prop->data.offset.x, prop->data.offset.y);
#endif
    break;
  case PROP_OPACITY:
    prop->data.opacity = SDL_ReadBE32 (src);
#if DEBUG
    printf("opacity %u\n", prop->data.opacity);
#endif
    break;
  case PROP_COMPRESSION:
  case PROP_COLOR:
    if (prop->length > sizeof(prop->data)) {
        len = sizeof(prop->data);
    } else {
        len = prop->length;
    }
    SDL_RWread(src, &prop->data, len, 1);
    break;
  case PROP_VISIBLE:
    prop->data.visible = SDL_ReadBE32 (src);
#if DEBUG
    printf("visible %u\n", prop->data.visible);
#endif
    break;
  case PROP_PRESERVE_TRANSPARENCY:
    prop->data.preserve_transparency = SDL_ReadBE32 (src);
#if DEBUG
    printf("preserve transparency %u\n", prop->data.preserve_transparency);
#endif
    break;
  default:
    /*    SDL_RWread (src, &prop->data, prop->length, 1);*/
    if (SDL_RWseek (src, prop->length, RW_SEEK_CUR) < 0)
      return 0;
  }
  return 1;
}

static void free_xcf_header (xcf_header * h) {
  if (h->cm_num)
    SDL_free (h->cm_map);
  if (h->layer_file_offsets)
    SDL_free (h->layer_file_offsets);
  SDL_free (h);
}

static xcf_header * read_xcf_header (SDL_RWops * src) {
  xcf_header * h;
  xcf_prop prop;

  h = (xcf_header *) SDL_malloc (sizeof (xcf_header));
  if (!h) {
    return NULL;
  }
  SDL_RWread (src, h->sign, 14, 1);
  h->width       = SDL_ReadBE32 (src);
  h->height      = SDL_ReadBE32 (src);
  h->image_type  = SDL_ReadBE32 (src);
  if(h->sign[9] == 'f' && h->sign[10] == 'i' && h->sign[11] == 'l' && h->sign[12] == 'e')
    h->file_version = 0;
  else
    h->file_version = (h->sign[10] - '0') * 100 + (h->sign[11] - '0') * 10 + (h->sign[12] - '0');
#ifdef DEBUG
  printf ("XCF signature : %.14s (version %u)\n", h->sign, h->file_version);
  printf (" (%u,%u) type=%u\n", h->width, h->height, h->image_type);
#endif
  if (h->file_version >= 4)
    h->precision = SDL_ReadBE32 (src);
  else
    h->precision = 150;

  h->properties = NULL;
  h->layer_file_offsets = NULL;
  h->compr      = COMPR_NONE;
  h->cm_num = 0;
  h->cm_map = NULL;

  do {
    if (!xcf_read_property (src, h, &prop)) {
      free_xcf_header (h);
      return NULL;
    }
    if (prop.id == PROP_COMPRESSION)
      h->compr = (xcf_compr_type)prop.data.compression;
    else if (prop.id == PROP_COLORMAP) {
      Uint32 cm_num;
      unsigned char *cm_map;

      cm_num = prop.data.colormap.num;
      cm_map = (unsigned char *) SDL_realloc(h->cm_map, sizeof (unsigned char) * 3 * cm_num);
      if (cm_map) {
        h->cm_num = cm_num;
        h->cm_map = cm_map;
        SDL_memcpy (h->cm_map, prop.data.colormap.cmap, 3*sizeof (char)*h->cm_num);
      }
      SDL_free (prop.data.colormap.cmap);

      if (!cm_map) {
        free_xcf_header(h);
        return NULL;
      }
    }
  } while (prop.id != PROP_END);

  return h;
}

static void free_xcf_layer (xcf_layer * l) {
  SDL_free (l->name);
  SDL_free (l);
}

static xcf_layer * read_xcf_layer (SDL_RWops * src, const xcf_header * h) {
  xcf_layer * l;
  xcf_prop    prop;

  l = (xcf_layer *) SDL_malloc (sizeof (xcf_layer));
  l->width  = SDL_ReadBE32 (src);
  l->height = SDL_ReadBE32 (src);
  l->layer_type = SDL_ReadBE32 (src);
  l->opacity = 255;

  l->name = read_string (src);
#ifdef DEBUG
  printf ("layer (%d,%d) type=%d '%s'\n", l->width, l->height, l->layer_type, l->name);
#endif

  do {
    if (!xcf_read_property (src, h, &prop)) {
      free_xcf_layer (l);
      return NULL;
    }
    if (prop.id == PROP_OFFSETS) {
      l->offset_x = prop.data.offset.x;
      l->offset_y = prop.data.offset.y;
    } else if (prop.id == PROP_VISIBLE) {
      l->visible = prop.data.visible ? 1 : 0;
    } else if (prop.id == PROP_OPACITY) {
      l->opacity = prop.data.opacity;
    } else if (prop.id == PROP_COLORMAP) {
      SDL_free (prop.data.colormap.cmap);
    }
  } while (prop.id != PROP_END);

  l->hierarchy_file_offset = read_offset (src, h);
  l->layer_mask_offset     = read_offset (src, h);

  return l;
}

static void free_xcf_channel (xcf_channel * c) {
  SDL_free (c->name);
  SDL_free (c);
}

static xcf_channel * read_xcf_channel (SDL_RWops * src, const xcf_header * h) {
  xcf_channel * l;
  xcf_prop    prop;

  l = (xcf_channel *) SDL_malloc (sizeof (xcf_channel));
  l->width  = SDL_ReadBE32 (src);
  l->height = SDL_ReadBE32 (src);

  l->name = read_string (src);
#ifdef DEBUG
  printf ("channel (%u,%u) '%s'\n", l->width, l->height, l->name);
#endif

  l->selection = 0;
  do {
    if (!xcf_read_property (src, h, &prop)) {
      free_xcf_channel (l);
      return NULL;
    }
    switch (prop.id) {
    case PROP_OPACITY:
      l->opacity = prop.data.opacity << 24;
      break;
    case PROP_COLOR:
      l->color = ((Uint32) prop.data.color[0] << 16)
    | ((Uint32) prop.data.color[1] << 8)
    | ((Uint32) prop.data.color[2]);
      break;
    case PROP_SELECTION:
      l->selection = 1;
      break;
    case PROP_VISIBLE:
      l->visible = prop.data.visible ? 1 : 0;
      break;
    default:
        ;
    }
  } while (prop.id != PROP_END);

  l->hierarchy_file_offset = read_offset (src, h);

  return l;
}

static void free_xcf_hierarchy (xcf_hierarchy * h) {
  SDL_free (h->level_file_offsets);
  SDL_free (h);
}

static xcf_hierarchy * read_xcf_hierarchy (SDL_RWops * src, const xcf_header * head) {
  xcf_hierarchy * h;
  int i;

  h = (xcf_hierarchy *) SDL_malloc (sizeof (xcf_hierarchy));
  h->width  = SDL_ReadBE32 (src);
  h->height = SDL_ReadBE32 (src);
  h->bpp    = SDL_ReadBE32 (src);

  h->level_file_offsets = NULL;
  i = 0;
  do {
    h->level_file_offsets = (Uint32 *) SDL_realloc (h->level_file_offsets, sizeof (Uint32) * (i+1));
    h->level_file_offsets [i] = read_offset (src, head);
  } while (h->level_file_offsets [i++]);

  return h;
}

static void free_xcf_level (xcf_level * l) {
  SDL_free (l->tile_file_offsets);
  SDL_free (l);
}

static xcf_level * read_xcf_level (SDL_RWops * src, const xcf_header * h) {
  xcf_level * l;
  int i;

  l = (xcf_level *) SDL_malloc (sizeof (xcf_level));
  l->width  = SDL_ReadBE32 (src);
  l->height = SDL_ReadBE32 (src);

  l->tile_file_offsets = NULL;
  i = 0;
  do {
    l->tile_file_offsets = (Uint32 *) SDL_realloc (l->tile_file_offsets, sizeof (Uint32) * (i+1));
    l->tile_file_offsets [i] = read_offset (src, h);
  } while (l->tile_file_offsets [i++]);

  return l;
}

static void free_xcf_tile (unsigned char * t) {
  SDL_free (t);
}

static unsigned char * load_xcf_tile_none (SDL_RWops * src, Uint32 len, int bpp, int x, int y) {
  unsigned char * load;
  (void)bpp; (void)x; (void)y;
  load = (unsigned char *) SDL_malloc (len);
  if (load != NULL)
    SDL_RWread (src, load, len, 1);

  return load;
}

static unsigned char * load_xcf_tile_rle (SDL_RWops * src, Uint32 len, int bpp, int x, int y) {
  unsigned char * load, * t, * data, * d;
  int i, size, count, j, length;
  unsigned char val;

  if (len == 0) {  /* probably bogus data. */
    return NULL;
  }

  t = load = (unsigned char *) SDL_malloc (len);
  if (load == NULL)
    return NULL;
  SDL_RWread (src, t, 1, len);

  data = (unsigned char *) SDL_calloc (1, x*y*bpp);
  for (i = 0; i < bpp; i++) {
    d    = data + i;
    size = x*y;
    count = 0;

    while (size > 0) {
      val = *t++;

      length = val;
      if (length >= 128) {
        length = 255 - (length - 1);
        if (length == 128) {
          length = (*t << 8) + t[1];
          t += 2;
        }

        if (((size_t) (t - load) + length) >= len) {
          break;  /* bogus data */
        } else if (length > size) {
          break;  /* bogus data */
        }

        count += length;
        size -= length;

        while (length-- > 0) {
          *d = *t++;
          d += bpp;
        }
      } else {
        length += 1;
        if (length == 128) {
          length = (*t << 8) + t[1];
          t += 2;
        }

        if (((size_t) (t - load)) >= len) {
          break;  /* bogus data */
        } else if (length > size) {
          break;  /* bogus data */
        }

        count += length;
        size -= length;

        val = *t++;

        for (j = 0; j < length; j++) {
          *d = val;
          d += bpp;
        }
      }
    }

    if (size > 0) {
      break;  /* just drop out, untouched data initialized to zero. */
    }

  }

  SDL_free (load);
  return (data);
}

static unsigned char * load_xcf_tile_zlib (SDL_RWops * src, Uint32 len, int bpp, int x, int y) {
  unsigned char * load;
  char *data = NULL;
  int l, s = x*y*bpp;

  if(len < 2 || s < 1)
    return NULL;
  load = (unsigned char *) SDL_malloc (len);
  if (!load)
    return NULL;
  SDL_RWread (src, load, len, 1);
  data = stbi_zlib_decode_malloc_guesssize_headerflag((const char*)load, len, s, &l, 1);
  if(l != s) { if(data) free(data); data = NULL; }
  SDL_free (load);
  return (unsigned char*)data;
}

static Uint32 rgb2grey (Uint32 a) {
  Uint8 l;
  l = (Uint8)(0.2990 * ((a & 0x00FF0000) >> 16)
    + 0.5870 * ((a & 0x0000FF00) >>  8)
    + 0.1140 * ((a & 0x000000FF)));

  return (l << 16) | (l << 8) | l;
}

static void create_channel_surface (SDL_Surface * surf, xcf_image_type itype, Uint32 color, Uint32 opacity) {
  Uint32 c = 0;

  switch (itype) {
  case IMAGE_RGB:
  case IMAGE_INDEXED:
    c = opacity | color;
    break;
  case IMAGE_GREYSCALE:
    c = opacity | rgb2grey (color);
    break;
  }
  SDL_FillRect (surf, NULL, c);
}

static int
do_layer_surface(SDL_Surface * surface, SDL_RWops * src, xcf_header * head, xcf_layer * layer, load_tile_type load_tile)
{
    xcf_hierarchy  *hierarchy;
    xcf_level      *level;
    unsigned char  *tile;
    Uint8          *p8;
    int            i, j;
    Uint32         x, y, tx, ty, ox, oy;
    Uint32         *row;
    Uint32         length;

    SDL_RWseek(src, layer->hierarchy_file_offset, RW_SEEK_SET);
    hierarchy = read_xcf_hierarchy(src, head);

    if (hierarchy->bpp > 4) {  /* unsupported. */
        SDL_Log("Unknown Gimp image bpp (%u)\n", (unsigned int) hierarchy->bpp);
        free_xcf_hierarchy(hierarchy);
        return 1;
    }

    if ((hierarchy->width > 20000) || (hierarchy->height > 20000)) {  /* arbitrary limit to avoid integer overflow. */
        SDL_Log("Gimp image too large (%ux%u)\n", (unsigned int) hierarchy->width, (unsigned int) hierarchy->height);
        free_xcf_hierarchy(hierarchy);
        return 1;
    }
    memset(surface->pixels, 0, surface->h * surface->pitch);

    level = NULL;
    for (i = 0; hierarchy->level_file_offsets[i]; i++) {
        if (SDL_RWseek(src, hierarchy->level_file_offsets[i], RW_SEEK_SET) < 0)
            break;
        if (i > 0)
            continue;
        level = read_xcf_level(src, head);

        ty = tx = 0;
        for (j = 0; level->tile_file_offsets[j]; j++) {
            SDL_RWseek(src, level->tile_file_offsets[j], RW_SEEK_SET);
            ox = tx + 64 > level->width ? level->width % 64 : 64;
            oy = ty + 64 > level->height ? level->height % 64 : 64;
            length = ox*oy*6;

            if (level->tile_file_offsets[j + 1] > level->tile_file_offsets[j]) {
                length = level->tile_file_offsets[j + 1] - level->tile_file_offsets[j];
            }
            tile = load_tile(src, length, hierarchy->bpp, ox, oy);

            if (!tile) {
                if (hierarchy) {
                    free_xcf_hierarchy(hierarchy);
                }
                if (level) {
                    free_xcf_level(level);
                }
                return 1;
            }

            p8 = tile;
            for (y = ty; y < ty + oy; y++) {
                if ((y >= (Uint32)surface->h) || ((tx+ox) > (Uint32)surface->w)) {
                    break;
                }
                row = (Uint32 *) ((Uint8 *) surface->pixels + y * surface->pitch + tx * 4);
                switch (hierarchy->bpp) {
                case 4:
                    for (x = tx; x < tx + ox; x++) {
                        *row |= ((Uint32)*p8++ << 0);
                        *row |= ((Uint32)*p8++ << 8);
                        *row |= ((Uint32)*p8++ << 16);
                        *row |= ((Uint32)(*p8++ * layer->opacity / 255) << 24);
                        row++;
                    }
                    break;
                case 3:
                    for (x = tx; x < tx + ox; x++) {
                        *row = layer->opacity << 24;
                        *row |= ((Uint32)*p8++ << 0);
                        *row |= ((Uint32)*p8++ << 8);
                        *row |= ((Uint32)*p8++ << 16);
                        row++;
                    }
                    break;
                case 2:
                    /* Indexed / Greyscale + Alpha */
                    switch (head->image_type) {
                    case IMAGE_INDEXED:
                        for (x = tx; x < tx + ox; x++) {
                            *row = ((Uint32)(head->cm_map[*p8 * 3]) << 0);
                            *row |= ((Uint32)(head->cm_map[*p8 * 3 + 1]) << 8);
                            *row |= ((Uint32)(head->cm_map[*p8++ * 3 + 2]) << 16);
                            *row |= ((Uint32)(*p8++ * layer->opacity / 255) << 24);
                            row++;
                        }
                        break;
                    case IMAGE_GREYSCALE:
                        for (x = tx; x < tx + ox; x++) {
                            *row = ((Uint32)*p8 << 0);
                            *row |= ((Uint32)*p8 << 8);
                            *row |= ((Uint32)*p8++ << 16);
                            *row |= ((Uint32)(*p8++ * layer->opacity / 255) << 24);
                            row++;
                        }
                        break;
                    default:
                        SDL_Log("Unknown Gimp image type (%d)\n", head->image_type);
                        if (hierarchy) {
                            free_xcf_hierarchy(hierarchy);
                        }
                        if (level)
                            free_xcf_level(level);
                        return 1;
                    }
                    break;
                case 1:
                    /* Indexed / Greyscale */
                    switch (head->image_type) {
                    case IMAGE_INDEXED:
                        for (x = tx; x < tx + ox; x++) {
                            *row++ = (layer->opacity << 24)
                                | ((Uint32)(head->cm_map[*p8 * 3]) << 0)
                                | ((Uint32)(head->cm_map[*p8 * 3 + 1]) << 8)
                                | ((Uint32)(head->cm_map[*p8 * 3 + 2]) << 16);
                            p8++;
                        }
                        break;
                    case IMAGE_GREYSCALE:
                        for (x = tx; x < tx + ox; x++) {
                            *row++ = (layer->opacity << 24)
                                | (((Uint32)(*p8)) << 0)
                                | (((Uint32)(*p8)) << 8)
                                | (((Uint32)(*p8)) << 16);
                            ++p8;
                        }
                        break;
                    default:
                        SDL_Log("Unknown Gimp image type (%d)\n", head->image_type);
                        if (tile)
                            free_xcf_tile(tile);
                        if (level)
                            free_xcf_level(level);
                        if (hierarchy)
                            free_xcf_hierarchy(hierarchy);
                        return 1;
                    }
                    break;
                }
            }
            free_xcf_tile(tile);

            tx += 64;
            if (tx >= level->width) {
                tx = 0;
                ty += 64;
            }
            if (ty >= level->height) {
                break;
            }
        }
        free_xcf_level(level);
    }

    free_xcf_hierarchy(hierarchy);

    return 0;
}

SDL_Surface *IMG_LoadXCF_RW(SDL_RWops *src)
{
  Sint64 start;
  const char *error = NULL;
  SDL_Surface *surface, *lays;
  xcf_header * head;
  xcf_layer  * layer;
  xcf_channel ** channel;
  int chnls, i, offsets;
  Sint64 offset, fp;

  unsigned char * (* load_tile) (SDL_RWops *, Uint32, int, int, int);

  if (!src) {
    /* The error message has been set in SDL_RWFromFile */
    return NULL;
  }
  start = SDL_RWtell(src);

  /* Initialize the data we will clean up when we're done */
  surface = NULL;

  head = read_xcf_header(src);
  if (!head) {
    return NULL;
  }

  switch (head->compr) {
  case COMPR_NONE:
    load_tile = load_xcf_tile_none;
    break;
  case COMPR_RLE:
    load_tile = load_xcf_tile_rle;
    break;
  case COMPR_ZLIB:
    load_tile = load_xcf_tile_zlib;
    break;
  default:
    SDL_Log("Unsupported Compression %d.\n", head->compr);
    free_xcf_header (head);
    return NULL;
  }

  /* Create the surface of the appropriate type */
  surface = SDL_CreateRGBSurface(SDL_SWSURFACE, head->width, head->height, 32,
                 0x00FF0000,0x0000FF00,0x000000FF,0xFF000000);

  if ( surface == NULL ) {
    error = "Out of memory";
    goto done;
  }

  offsets = 0;

  while ((offset = read_offset (src, head))) {
    head->layer_file_offsets = (Uint32 *) SDL_realloc (head->layer_file_offsets, sizeof (Uint32) * (offsets+1));
    head->layer_file_offsets [offsets] = (Uint32)offset;
    offsets++;
  }
  fp = SDL_RWtell (src);
#if DEBUG
printf("num offsets %u\n",offsets);
#endif
  lays = SDL_CreateRGBSurface(SDL_SWSURFACE, head->width, head->height, 32,
              0x00FF0000,0x0000FF00,0x000000FF,0xFF000000);

  if ( lays == NULL ) {
    error = "Out of memory";
    goto done;
  }

  for (i = offsets; i > 0; i--) {
    SDL_Rect rs, rd;
    SDL_RWseek (src, head->layer_file_offsets [i-1], RW_SEEK_SET);

    layer = read_xcf_layer (src, head);
    if (layer != NULL) {
      do_layer_surface (lays, src, head, layer, load_tile);
      rs.x = 0;
      rs.y = 0;
      rs.w = layer->width;
      rs.h = layer->height;
      rd.x = layer->offset_x;
      rd.y = layer->offset_y;
      rd.w = layer->width;
      rd.h = layer->height;

      if (layer->visible && layer->opacity)
        SDL_BlitSurface (lays, &rs, surface, &rd);
      free_xcf_layer (layer);
    }
  }

  SDL_FreeSurface (lays);

  SDL_RWseek (src, fp, RW_SEEK_SET);

  channel = NULL;
  chnls   = 0;
  while ((offset = read_offset (src, head))) {
    channel = (xcf_channel **) SDL_realloc (channel, sizeof (xcf_channel *) * (chnls+1));
    fp = SDL_RWtell (src);
    SDL_RWseek (src, offset, RW_SEEK_SET);
    channel [chnls] = (read_xcf_channel (src, head));
    if (channel [chnls] != NULL)
      chnls++;
    SDL_RWseek (src, fp, RW_SEEK_SET);
  }

  if (chnls) {
    SDL_Surface * chs;

    chs = SDL_CreateRGBSurface(SDL_SWSURFACE, head->width, head->height, 32,
               0x00FF0000,0x0000FF00,0x000000FF,0xFF000000);

    if (chs == NULL) {
      error = "Out of memory";
      goto done;
    }
    for (i = 0; i < chnls; i++) {
      if (!channel [i]->selection && channel [i]->visible) {
    create_channel_surface (chs, (xcf_image_type)head->image_type, channel [i]->color, channel [i]->opacity);
    SDL_BlitSurface (chs, NULL, surface, NULL);
      }
      free_xcf_channel (channel [i]);
    }
    SDL_free(channel);

    SDL_FreeSurface (chs);
  }

done:
  free_xcf_header (head);
  if ( error ) {
    SDL_RWseek(src, start, RW_SEEK_SET);
    if ( surface ) {
      SDL_FreeSurface(surface);
      surface = NULL;
    }
    IMG_SetError("%s", error);
  }

  return(surface);
}
