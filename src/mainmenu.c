/*
 * tnge/mainmenu.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Main menu window
 *
 */

#include "main.h"
#include "theora.h"

void media_callback(int channel);
extern theora_t theora_ctx;
extern ui_sprsel_t elements_sprites[];

void mainmenu_load();
void mainmenu_save(void *data);
void mainmenu_delete(void *data);
void mainmenu_viewchrg(void *data);
void mainmenu_viewmenu(void *data);
void mainmenu_bgmov(void *data);
void mainmenu_bgimg(void *data);
void mainmenu_preview(int type);

int mainmenu_tab, mainmenu_enabled = 1, mainmenu_tittgl = 1, mainmenu_nocrop = 0;
int mainmenu_titstyle = 0, mainmenu_btnstyle = 0;
char mainmenu_url[256] = { 0 };
uint32_t mainmenu_titcolor = 0xFFFFFFFF, mainmenu_titshadow = 0x7F000000, mainmenu_grbcolor = 0, mainmenu_grccolor = 0;
uint32_t mainmenu_btncolor[5] = { 0 };
ui_select_t mainmenu_intro = { -1, LANG_NONE, NULL };
ui_select_t mainmenu_backmus = { -1, LANG_NONE, NULL };
ui_select_t mainmenu_backmov = { -1, LANG_NONE, NULL };
ui_sprsel_t mainmenu_backimg = { -1, 4, 25, 1 };
ui_tablehdr_t mainmenu_backhdr[] = {
    { MAINMENU_DX, 96, 0, 0 },
    { MAINMENU_DY, 96, 0, 0 },
    { MAINMENU_INTERVAL, 96, 0, 0 },
    { MAINMENU_PARALLAX, 0, 0, 0 },
    { 0 }
};
ui_table_t mainmenu_backprl = { mainmenu_backhdr, 0, 32, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL };
ui_num_t mainmenu_numval[] = {
    { 0, -100, 100, 1 },
    { 0, -100, 100, 1 },
    { 1, 1, 6000, 1 },
    { 0, -100, 100, 1 },
    { 0, -100, 100, 1 },
    { 1, 1, 6000, 1 },
    { 0, -100, 100, 1 },
    { 0, -100, 100, 1 },
    { 1, 1, 6000, 1 }
};
ui_sprsel_t mainmenu_prximg[] = {
    { -1, 4, 25, 1 },
    { -1, 4, 25, 1 },
    { -1, 4, 25, 1 }
};
ui_sprsel_t mainmenu_titimg = { -1, 0, 25, 1 };
ui_icontgl_t mainmenu_titbold = { ICON_BOLD, SSFN_STYLE_BOLD, &mainmenu_titstyle };
ui_icontgl_t mainmenu_tititalic = { ICON_ITALIC, SSFN_STYLE_ITALIC, &mainmenu_titstyle };
ui_num_t mainmenu_titsize = { 32, 16, 192, 1 };
ui_select_t mainmenu_titfont = { -1, LANG_DEF, NULL };
ui_input_t mainmenu_titinp = { INP_NAME, PROJ_TITLEMAX, project.name };
ui_input_t mainmenu_urlinp = { INP_NAME, PROJ_TITLEMAX, mainmenu_url };
ui_icontgl_t mainmenu_btnbold = { ICON_BOLD, SSFN_STYLE_BOLD, &mainmenu_btnstyle };
ui_icontgl_t mainmenu_btnitalic = { ICON_ITALIC, SSFN_STYLE_ITALIC, &mainmenu_btnstyle };
ui_num_t mainmenu_btnpad = { 0, -128, 128, 1 };
ui_num_t mainmenu_btnsize = { 16, 16, 192, 1 };
ui_select_t mainmenu_btnfont = { -1, LANG_DEF, NULL };
ui_sprsel_t mainmenu_btnspr[] = {
    { -1, 0, 146, 1 },
    { -1, 0, 138, 1 },
    { -1, 0, 147, 1 },
    { -1, 0, 146, 1 },
    { -1, 0, 138, 1 },
    { -1, 0, 147, 1 },
    { -1, 0, 146, 1 },
    { -1, 0, 138, 1 },
    { -1, 0, 147, 1 },
    { -1, 0, 146, 1 },
    { -1, 0, 138, 1 },
    { -1, 0, 147, 1 },
    { -1, 0, 146, 1 },
    { -1, 0, 138, 1 },
    { -1, 0, 147, 1 }
};
ui_font_t mainmenu_fnt;

/**
 * The form
 */
ui_form_t mainmenu_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, MAINMENU_DELETE, (void*)ICON_REMOVE, mainmenu_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, mainmenu_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, MAINMENU_VIEWCHRG, (void*)ICON_PVIEW, mainmenu_viewchrg },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, MAINMENU_VIEWMENU, (void*)ICON_PVIEW, mainmenu_viewmenu },
    /* 4 */
    { FORM_TEXT, 10, 29, 0, 18, 0, 0, NULL, NULL },
    { FORM_SELECT, 0, 29, 0, 20, 0, MAINMENU_SINTRO, &mainmenu_intro, NULL },
    { FORM_BOOL, 0, 29, 0, 20, 0, MAINMENU_ENABLED, &mainmenu_enabled, NULL },
    /* 7 */
    { FORM_TEXT, 10, 53, 0, 18, 0, 0, NULL, NULL },
    { FORM_MUSSEL, 0, 53, 0, 20, 0, MAINMENU_BACKMUS, &mainmenu_backmus, NULL },
    { FORM_MOVSEL, 0, 53, 0, 20, 0, MAINMENU_BACKMOV, &mainmenu_backmov, mainmenu_bgmov },
    { FORM_SPRITE, 0, 53, 0, 20, 0, MAINMENU_BACKIMG, &mainmenu_backimg, mainmenu_bgimg },
    { FORM_ICON, 0, 55, 16, 16, 0, 0, (void*)ICON_BACKGR, NULL },
    { FORM_DRAWTBL, 0, 76, 0, 64, 0, 0, &mainmenu_backprl, NULL },
    { FORM_NUM, 0, 102, 56, 18, 0, 0, &mainmenu_numval[0], NULL },
    { FORM_NUM, 0, 102, 56, 18, 0, 0, &mainmenu_numval[1], NULL },
    { FORM_TIME, 0, 102, 56, 18, 0, 0, &mainmenu_numval[2], NULL },
    { FORM_SPRITE, 0, 100, 37, 25, 0, 0, &mainmenu_prximg[0], NULL },
    { FORM_NUM, 0, 134, 56, 18, 0, 0, &mainmenu_numval[3], NULL },
    { FORM_NUM, 0, 134, 56, 18, 0, 0, &mainmenu_numval[4], NULL },
    { FORM_TIME, 0, 134, 56, 18, 0, 0, &mainmenu_numval[5], NULL },
    { FORM_SPRITE, 0, 132, 37, 25, 0, 0, &mainmenu_prximg[1], NULL },
    { FORM_NUM, 0, 166, 56, 18, 0, 0, &mainmenu_numval[6], NULL },
    { FORM_NUM, 0, 166, 56, 18, 0, 0, &mainmenu_numval[7], NULL },
    { FORM_TIME, 0, 166, 56, 18, 0, 0, &mainmenu_numval[8], NULL },
    { FORM_SPRITE, 0, 164, 37, 25, 0, 0, &mainmenu_prximg[2], NULL },
    /* 25 */
    { FORM_TEXT, 10, 200, 0, 18, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 200, 128, 18, 0, MAINMENU_HEADER, &mainmenu_titimg, NULL },
    { FORM_BOOL, 0, 200, 18, 18, 0, 0, &mainmenu_tittgl, NULL },
    { FORM_COLOR, 0, 200, 18, 18, 0, MAINMENU_TITCOLOR, &mainmenu_titcolor, NULL },
    { FORM_COLOR, 0, 200, 18, 18, 0, MAINMENU_TITSHADOW, &mainmenu_titshadow, NULL },
    { FORM_ICONTGL, 0, 200, 18, 18, 0, FONTS_BOLD, &mainmenu_titbold, NULL },
    { FORM_ICONTGL, 0, 200, 18, 18, 0, FONTS_ITALIC, &mainmenu_tititalic, NULL },
    { FORM_NUM, 0, 200, 42, 18, 0, FONTS_SIZE, &mainmenu_titsize, NULL },
    { FORM_SELECT, 0, 200, 0, 18, 0, FONTS_FAMILY, &mainmenu_titfont, NULL },
    /* 34 */
    { FORM_INPUT, 0, 224, 0, 18, 0, NEWPROJ_TITSTAT, &mainmenu_titinp, NULL },
    /* 35 */
    { FORM_TEXT, 10, 272, 0, 18, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 272, 18, 18, 0, 0, "\x1D", NULL },
    { FORM_NUM, 0, 272, 42, 18, 0, MAINMENU_BTNPAD, &mainmenu_btnpad, NULL },
    { FORM_ICONTGL, 0, 272, 18, 18, 0, FONTS_BOLD, &mainmenu_btnbold, NULL },
    { FORM_ICONTGL, 0, 272, 18, 18, 0, FONTS_ITALIC, &mainmenu_btnitalic, NULL },
    { FORM_NUM, 0, 272, 42, 18, 0, FONTS_SIZE, &mainmenu_btnsize, NULL },
    { FORM_SELECT, 0, 272, 0, 18, 0, FONTS_FAMILY, &mainmenu_btnfont, NULL },
    /* 42 */
    { FORM_TEXT, 10, 296, 0, 18, 0, 0, NULL, NULL },
    { FORM_COLOR, 0, 296, 18, 18, 0, 0, &mainmenu_btncolor[0], NULL },
    { FORM_SPRITE, 0, 296, 20, 20, 0, 0, &mainmenu_btnspr[0], NULL },
    { FORM_SPRITE, 0, 296, 40, 20, 0, 0, &mainmenu_btnspr[1], NULL },
    { FORM_SPRITE, 0, 296, 20, 20, 0, 0, &mainmenu_btnspr[2], NULL },
    /* 47 */
    { FORM_TEXT, 10, 320, 0, 18, 0, 0, NULL, NULL },
    { FORM_COLOR, 0, 320, 18, 18, 0, 0, &mainmenu_btncolor[1], NULL },
    { FORM_SPRITE, 0, 320, 20, 20, 0, 0, &mainmenu_btnspr[3], NULL },
    { FORM_SPRITE, 0, 320, 40, 20, 0, 0, &mainmenu_btnspr[4], NULL },
    { FORM_SPRITE, 0, 320, 20, 20, 0, 0, &mainmenu_btnspr[5], NULL },
    /* 52 */
    { FORM_TEXT, 10, 344, 0, 18, 0, 0, NULL, NULL },
    { FORM_COLOR, 0, 344, 18, 18, 0, 0, &mainmenu_btncolor[2], NULL },
    { FORM_SPRITE, 0, 344, 20, 20, 0, 0, &mainmenu_btnspr[6], NULL },
    { FORM_SPRITE, 0, 344, 40, 20, 0, 0, &mainmenu_btnspr[7], NULL },
    { FORM_SPRITE, 0, 344, 20, 20, 0, 0, &mainmenu_btnspr[8], NULL },
    /* 57 */
    { FORM_TEXT, 10, 368, 0, 18, 0, 0, NULL, NULL },
    { FORM_COLOR, 0, 368, 18, 18, 0, 0, &mainmenu_btncolor[3], NULL },
    { FORM_SPRITE, 0, 368, 20, 20, 0, 0, &mainmenu_btnspr[9], NULL },
    { FORM_SPRITE, 0, 368, 40, 20, 0, 0, &mainmenu_btnspr[10], NULL },
    { FORM_SPRITE, 0, 368, 20, 20, 0, 0, &mainmenu_btnspr[11], NULL },
    /* 62 */
    { FORM_TEXT, 10, 392, 0, 18, 0, 0, NULL, NULL },
    { FORM_COLOR, 0, 392, 18, 18, 0, 0, &mainmenu_btncolor[4], NULL },
    { FORM_SPRITE, 0, 392, 20, 20, 0, 0, &mainmenu_btnspr[12], NULL },
    { FORM_SPRITE, 0, 392, 40, 20, 0, 0, &mainmenu_btnspr[13], NULL },
    { FORM_SPRITE, 0, 392, 20, 20, 0, 0, &mainmenu_btnspr[14], NULL },
    /* 67 */
    { FORM_TEXT, 10, 416, 0, 18, 0, 0, NULL, NULL },
    { FORM_COLOR, 0, 416, 18, 18, 0, MAINMENU_GRAPHC, &mainmenu_grccolor, NULL },
    { FORM_COLOR, 0, 416, 18, 18, 0, MAINMENU_GRAPHB, &mainmenu_grbcolor, NULL },
    /* 70 */
    { FORM_BOOL, 0, 53, 12, 20, 0, MAINMENU_NOCROP, &mainmenu_nocrop, NULL },
    /* 71 */
    { FORM_TEXT, 10, 248, 0, 18, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 248, 0, 18, 0, MAINMENU_URLSTAT, &mainmenu_urlinp, NULL },
    { FORM_LAST }
};

/**
 * Change background handlers
 */
void mainmenu_bgmov(void *data) { (void)data; if(mainmenu_backmov.val != -1) mainmenu_backimg.val = -1; }
void mainmenu_bgimg(void *data) { (void)data; if(mainmenu_backimg.val != -1) mainmenu_backmov.val = -1; }

/**
 * Exit main menu window
 */
void mainmenu_exit(int tab)
{
    int i;

    (void)tab;
    mainmenu_intro.val = mainmenu_backmus.val = mainmenu_backmov.val = mainmenu_backimg.val =
        mainmenu_prximg[0].val = mainmenu_prximg[1].val = mainmenu_prximg[2].val =
        mainmenu_titimg.val = mainmenu_titfont.val = mainmenu_btnfont.val = -1;
    mainmenu_enabled = mainmenu_tittgl = 1;
    mainmenu_titstyle = mainmenu_btnstyle = mainmenu_numval[0].val = mainmenu_numval[1].val = mainmenu_numval[3].val =
        mainmenu_numval[4].val = mainmenu_numval[6].val = mainmenu_numval[7].val = 0;
    mainmenu_numval[2].val = mainmenu_numval[5].val = mainmenu_numval[8].val = 1;
    mainmenu_titsize.val = 32; mainmenu_btnsize.val = 16;
    for(i = 0; i < 15; i++)
        mainmenu_btnspr[i].val = -1;
    mainmenu_titcolor = 0xFFFFFFFF; mainmenu_titshadow = 0x7F000000; mainmenu_grbcolor = mainmenu_grccolor = 0;
    memset(mainmenu_btncolor, 0, sizeof(mainmenu_btncolor));
    memset(mainmenu_url, 0, sizeof(mainmenu_url));
}

/**
 * Enter main menu window
 */
void mainmenu_init(int tab)
{
    int i;

    mainmenu_exit(tab);
    mainmenu_form[1].param = lang[LANG_SAVE];
    mainmenu_form[1].w = ui_textwidth(mainmenu_form[1].param) + 40;
    if(mainmenu_form[1].w < 200) mainmenu_form[1].w = 200;
    mainmenu_form[4].param = lang[MAINMENU_INTRO];
    mainmenu_tab = ui_textwidth(mainmenu_form[4].param);
    mainmenu_intro.opts = project.cuts;
    mainmenu_form[7].param = lang[MAINMENU_BACK];
    i = ui_textwidth(mainmenu_form[7].param); if(i > mainmenu_tab) mainmenu_tab = i;
    mainmenu_form[25].param = lang[MAINMENU_TITLE];
    i = ui_textwidth(mainmenu_form[25].param); if(i > mainmenu_tab) mainmenu_tab = i;
    mainmenu_form[35].param = lang[MAINMENU_BUTTONS];
    i = ui_textwidth(mainmenu_form[35].param); if(i > mainmenu_tab) mainmenu_tab = i;
    mainmenu_form[67].param = lang[MAINMENU_GRAPH];
    i = ui_textwidth(mainmenu_form[67].param); if(i > mainmenu_tab) mainmenu_tab = i;
    mainmenu_form[71].param = lang[MAINMENU_WEBSITE];
    i = ui_textwidth(mainmenu_form[71].param); if(i > mainmenu_tab) mainmenu_tab = i;
    mainmenu_tab += 20;
    mainmenu_form[6].x = mainmenu_form[8].x = mainmenu_tab + 20;
    mainmenu_form[5].x = mainmenu_form[12].x = mainmenu_form[26].x = mainmenu_form[34].x = mainmenu_form[36].x =
        mainmenu_form[42].x = mainmenu_form[47].x = mainmenu_form[52].x = mainmenu_form[57].x = mainmenu_form[62].x =
        mainmenu_form[68].x = mainmenu_form[72].x = mainmenu_tab;
    mainmenu_form[69].x = mainmenu_form[68].x + mainmenu_form[68].w + 10;
    mainmenu_form[13].x = mainmenu_form[17].x = mainmenu_form[21].x = mainmenu_tab + 16;
    mainmenu_form[14].x = mainmenu_form[18].x = mainmenu_form[22].x =
        mainmenu_tab + 16 + mainmenu_backhdr[0].width;
    mainmenu_form[15].x = mainmenu_form[19].x = mainmenu_form[23].x =
        mainmenu_tab + 16 + mainmenu_backhdr[0].width + mainmenu_backhdr[1].width;
    mainmenu_form[16].x = mainmenu_form[20].x = mainmenu_form[24].x =
        mainmenu_tab + 16 + mainmenu_backhdr[0].width + mainmenu_backhdr[1].width + mainmenu_backhdr[2].width;
    mainmenu_form[27].x = mainmenu_tab + 128 + 40;
    mainmenu_form[28].x = mainmenu_tab + 128 + 40 + 24;
    mainmenu_form[29].x = mainmenu_tab + 128 + 40 + 24 + 24;
    mainmenu_form[30].x = mainmenu_tab + 128 + 40 + 24 + 24 + 24;
    mainmenu_form[31].x = mainmenu_tab + 128 + 40 + 24 + 24 + 24 + 24;
    mainmenu_form[32].x = mainmenu_tab + 128 + 40 + 24 + 24 + 24 + 24 + 24;
    mainmenu_form[33].x = mainmenu_tab + 128 + 40 + 24 + 24 + 24 + 24 + 24 + 46;
    mainmenu_form[36].x = mainmenu_form[70].x = mainmenu_tab;
    mainmenu_form[37].x = mainmenu_form[36].x + mainmenu_form[36].w;
    mainmenu_form[38].x = mainmenu_form[37].x + mainmenu_form[37].w + 10;
    mainmenu_form[39].x = mainmenu_form[37].x + mainmenu_form[37].w + 10 + 24;
    mainmenu_form[40].x = mainmenu_form[37].x + mainmenu_form[37].w + 10 + 24 + 24;
    mainmenu_form[41].x = mainmenu_form[37].x + mainmenu_form[37].w + 10 + 24 + 24 + 46;
    mainmenu_titfont.opts = mainmenu_btnfont.opts = project.fonts;
    mainmenu_form[42].param = lang[ELEMENTS_NORMAL];
    mainmenu_form[43].x = mainmenu_form[48].x = mainmenu_form[53].x = mainmenu_form[58].x = mainmenu_form[63].x =
        mainmenu_tab + 128 + 40;
    mainmenu_form[44].x = mainmenu_form[49].x = mainmenu_form[54].x = mainmenu_form[59].x = mainmenu_form[64].x =
        mainmenu_tab + 128 + 40 + 24;
    mainmenu_form[45].x = mainmenu_form[50].x = mainmenu_form[55].x = mainmenu_form[60].x = mainmenu_form[65].x =
        mainmenu_tab + 128 + 40 + 24 + 24;
    mainmenu_form[46].x = mainmenu_form[51].x = mainmenu_form[56].x = mainmenu_form[61].x = mainmenu_form[66].x =
        mainmenu_tab + 128 + 40 + 24 + 24 + 44;
    mainmenu_form[47].param = lang[ELEMENTS_SELECTED];
    mainmenu_form[52].param = lang[ELEMENTS_PRESSED];
    mainmenu_form[57].param = lang[ELEMENTS_INACTIVE];
    mainmenu_form[62].param = lang[MAINMENU_CHARNAME];
    mainmenu_load();
}

/**
 * Resize the view
 */
void mainmenu_resize(int tab, int w, int h)
{
    (void)tab;
    mainmenu_form[0].y = mainmenu_form[1].y = mainmenu_form[2].y = mainmenu_form[3].y = h - 48;
    mainmenu_form[1].x = w - 20 - mainmenu_form[1].w;
    mainmenu_form[2].x = mainmenu_form[1].x - 52;
    mainmenu_form[3].x = mainmenu_form[2].x - 52;
    mainmenu_form[5].w = mainmenu_form[6].w = (w - 10 - mainmenu_form[5].x) / 2 - 5;
    mainmenu_form[6].x = w - 10 - mainmenu_form[6].w;
    mainmenu_form[8].w = mainmenu_form[9].w = mainmenu_form[10].w = (scr_w - 90 - 20 - mainmenu_tab) / 3;
    mainmenu_form[9].x = mainmenu_form[8].x + mainmenu_form[8].w + 30;
    mainmenu_form[10].x = mainmenu_form[9].x + mainmenu_form[9].w + 30;
    mainmenu_form[11].x = mainmenu_form[10].x - 18;
    ui_table_resize(&mainmenu_form[12], scr_w - 10 - mainmenu_tab, 3 * mainmenu_backprl.row + 22);
    mainmenu_form[33].w = scr_w - 10 - mainmenu_form[33].x;
    mainmenu_form[34].w = mainmenu_form[72].w = scr_w - 10 - mainmenu_tab;
    mainmenu_form[41].w = scr_w - 10 - mainmenu_form[41].x;
    mainmenu_form[70].x = w - 10 - mainmenu_form[70].w;
}

/**
 * View layer
 */
void mainmenu_redraw(int tab)
{
    int i;

    (void)tab;
    ui_form = mainmenu_form;
    ui_form[2].inactive = ui_form[3].inactive = (!mainmenu_enabled);
    if(!mainmenu_enabled) {
        for(i = 7; ui_form[i].type; i++)
            ui_form[i].inactive = 1;
    } else {
        for(i = 7; ui_form[i].type; i++)
            ui_form[i].inactive = 0;
        ui_form[12].inactive = ui_form[13].inactive = ui_form[14].inactive = ui_form[15].inactive =
            ui_form[16].inactive = ui_form[17].inactive = ui_form[18].inactive = ui_form[19].inactive =
            ui_form[20].inactive = ui_form[21].inactive = ui_form[22].inactive = ui_form[23].inactive =
            ui_form[24].inactive = mainmenu_backimg.val == -1;
        ui_form[28].inactive = ui_form[29].inactive = ui_form[30].inactive = ui_form[31].inactive =
            ui_form[32].inactive = ui_form[33].inactive = !mainmenu_tittgl;
    }
}

/**
 * Delete main menu
 */
void mainmenu_delete(void *data)
{
    (void)data;
    sprintf(projfn, "%s" SEP "%s" SEP "mainmenu.cfg", project.id, project_dirs[PROJDIRS_UI]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("mainmenu_delete: removing %s\n", projdir);
        remove(projdir);
        mainmenu_exit(SUBMENU_MAINMENU);
    }
}

/**
 * Save main menu
 */
void mainmenu_save(void *data)
{
    FILE *f;
    int i;

    (void)data;
    project_save();
    f = project_savefile(0, project_dirs[PROJDIRS_UI], "mainmenu", "cfg", PROJMAGIC_MAINMENU, "mainmenu_save");
    if(f) {
        fprintf(f, "%u ", mainmenu_enabled);
        project_wridx(f, mainmenu_intro.val, project.cuts); fprintf(f, "\r\n");
        project_wridx(f, mainmenu_backmus.val, project.music); fprintf(f, " ");
        project_wridx(f, mainmenu_backmov.val, project.movies); fprintf(f, " ");
        project_wrsprite(f, &mainmenu_backimg); fprintf(f, "\r\n");
        for(i = 0; i < 3; i++) {
            fprintf(f, "%d %d %d ", mainmenu_numval[i * 3].val, mainmenu_numval[i * 3 + 1].val, mainmenu_numval[i * 3 + 2].val);
            project_wrsprite(f, &mainmenu_prximg[i]);
            fprintf(f, "\r\n");
        }
        project_wrsprite(f, &mainmenu_titimg); fprintf(f, "\r\n");
        fprintf(f, "%u %u %08X %08X ", mainmenu_nocrop, mainmenu_tittgl, mainmenu_titcolor, mainmenu_titshadow);
        project_wrfont(f, mainmenu_titstyle, mainmenu_titsize.val, mainmenu_titfont.val); fprintf(f, "\r\n");
        fprintf(f, "%s\r\n", mainmenu_url[0] ? mainmenu_url : "-");
        fprintf(f, "%d ", mainmenu_btnpad.val);
        project_wrfont(f, mainmenu_btnstyle, mainmenu_btnsize.val, mainmenu_btnfont.val); fprintf(f, "\r\n");
        for(i = 0; i < 5; i++) {
            fprintf(f, "%08X ", mainmenu_btncolor[i]);
            project_wrsprite(f, &mainmenu_btnspr[i * 3 + 0]); fprintf(f, " ");
            project_wrsprite(f, &mainmenu_btnspr[i * 3 + 1]); fprintf(f, " ");
            project_wrsprite(f, &mainmenu_btnspr[i * 3 + 2]); fprintf(f, "\r\n");
        }
        fprintf(f, "%08X %08X\r\n", mainmenu_grccolor, mainmenu_grbcolor);
        fclose(f);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVEUI]);
}

/**
 * Load main menu
 */
void mainmenu_load()
{
    char *str, *s;
    int i;

    str = project_loadfile(project_dirs[PROJDIRS_UI], "mainmenu", "cfg", PROJMAGIC_MAINMENU, "mainmenu_load");
    if(str) {
        s = project_skipnl(str);
        s = project_getint(s, &mainmenu_enabled, 0, 1);
        s = project_getidx(s, &mainmenu_intro.val, project.cuts, -1);
        s = project_skipnl(s);
        s = project_getidx(s, &mainmenu_backmus.val, project.music, -1);
        s = project_getidx(s, &mainmenu_backmov.val, project.movies, -1);
        s = project_getsprite(s, &mainmenu_backimg);
        s = project_skipnl(s);
        for(i = 0; i < 3; i++) {
            s = project_getint(s, &mainmenu_numval[i * 3 + 0].val, mainmenu_numval[i * 3 + 0].min, mainmenu_numval[i * 3 + 0].max);
            s = project_getint(s, &mainmenu_numval[i * 3 + 1].val, mainmenu_numval[i * 3 + 1].min, mainmenu_numval[i * 3 + 1].max);
            s = project_getint(s, &mainmenu_numval[i * 3 + 2].val, mainmenu_numval[i * 3 + 2].min, mainmenu_numval[i * 3 + 2].max);
            s = project_getsprite(s, &mainmenu_prximg[i]);
            s = project_skipnl(s);
        }
        s = project_getsprite(s, &mainmenu_titimg);
        s = project_skipnl(s);
        s = project_getint(s, &mainmenu_nocrop, 0, 1);
        s = project_getint(s, &mainmenu_tittgl, 0, 1);
        s = project_getcolor(s, &mainmenu_titcolor);
        s = project_getcolor(s, &mainmenu_titshadow);
        s = project_getfont(s, &mainmenu_titstyle, &mainmenu_titsize.val, &mainmenu_titfont.val);
        s = project_skipnl(s);
        s = project_getstr2(s, mainmenu_url, 0, sizeof(mainmenu_url));
        s = project_skipnl(s);
        s = project_getint(s, &mainmenu_btnpad.val, mainmenu_btnpad.min, mainmenu_btnpad.max);
        s = project_getfont(s, &mainmenu_btnstyle, &mainmenu_btnsize.val, &mainmenu_btnfont.val);
        s = project_skipnl(s);
        for(i = 0; i < 5; i++) {
            s = project_getcolor(s, &mainmenu_btncolor[i]);
            s = project_getsprite(s, &mainmenu_btnspr[i * 3 + 0]);
            s = project_getsprite(s, &mainmenu_btnspr[i * 3 + 1]);
            s = project_getsprite(s, &mainmenu_btnspr[i * 3 + 2]);
            s = project_skipnl(s);
        }
        s = project_getcolor(s, &mainmenu_grccolor);
        s = project_getcolor(s, &mainmenu_grbcolor);
        free(str);
    } else
        ui_status(1, lang[ERR_LOADUI]);
}

/**
 * Preview character generation
 */
void mainmenu_viewchrg(void *data)
{
    (void)data;
    mainmenu_preview(1);
}

/**
 * Preview main menu
 */
void mainmenu_viewmenu(void *data)
{
    (void)data;
    mainmenu_preview(0);
}

/**
 * Display a main menu button with ui elements
 */
int mainmenu_button(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, char *str, int type, int idx)
{
    ssfn_buf_t buf;
    ui_sprite_t *s;
    int ih, k, l, p, hb, icns[4] = { ICON_INLOAD, ICON_START, ICON_SETUP, ICON_QUIT };

    s = spr_getsel(&mainmenu_btnspr[type * 3 + 2], 0); w -= s->w; ih = s->h;
    s = spr_getsel(&mainmenu_btnspr[type * 3 + 1], 0); if(ih < s->h) ih = s->h;
    s = spr_getsel(&mainmenu_btnspr[type * 3 + 0], 0); w -= s->w; if(ih < s->h) ih = s->h;
    if(!dst) return ih;
    ui_blitbuf(dst, x, y - s->h / 2, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    x += s->w;

    s = spr_getsel(&mainmenu_btnspr[type * 3 + 1], 0);
    ui_blitbuf(dst, x, y - s->h / 2, w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    if(idx >= 0)
        ui_blit2(dst, x, y - 8 + (type == 2 ? 1 : 0), 16, 16, icons,
            (icns[idx] & 7) << 4, (icns[idx] & ~7) << 1, 16, 16);

    if(str) {
        ssfn_bbox(&mainmenu_fnt.ctx, str, &l, &p, &k, &hb);
        SDL_LockTexture(dst, NULL, (void**)&buf.ptr, &p);
        buf.p = p;
        buf.w = dstw;
        buf.h = dsth;
        buf.x = x + (idx != -2 ? (w - l) / 2 : 0);
        buf.y = y - ih / 2 + (ih - mainmenu_btnsize.val) / 2 + hb + mainmenu_btnpad.val + (type == 2 ? 1 : 0);
        buf.fg = type == 4 ? elements_colors[3] : mainmenu_btncolor[type];
        buf.bg = 0;
        while(*str && ((l = ssfn_render(&mainmenu_fnt.ctx, &buf, str)) > 0 || l == SSFN_ERR_NOGLYPH)) str += (l < 1 ? 1 : l);
        SDL_UnlockTexture(dst);
    }

    s = spr_getsel(&mainmenu_btnspr[type * 3 + 2], 0);
    ui_blitbuf(dst, x + w, y - s->h / 2, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    return ih;
}

/**
 * Draw a graph
 */
void mainmenu_graph(SDL_Texture *dst, int x, int y, int s)
{
    int p[] = { 127, 0, 255, 96, 215, 230, 42, 230, 0, 96, 127, 50, 214, 107, 172, 180, 42, 230, 60, 112 };
    int i, cx = 127 * s / 255, cy = 127 * s / 255;
    uint32_t color = (mainmenu_grccolor & 0xffffff) | ((mainmenu_grccolor >> 3) & 0xff000000);

    for(i = 0; i < 20; i++) p[i] = p[i] * s / 255;
    for(i = 0; i < 5; i++)
        ui_line(dst, x + cx, y + cy, x + p[i * 2], y + p[i * 2 + 1], mainmenu_grbcolor);
    for(i = 0; i < 4; i++) {
        ui_triangle(dst, x + cx, y + cy, x + p[10 + i * 2], y + p[11 + i * 2], x + p[12 + i * 2], y + p[13 + i * 2], color);
        ui_line(dst, x + p[10 + i * 2], y + p[11 + i * 2], x + p[12 + i * 2], y + p[13 + i * 2], mainmenu_grccolor);
    }
    ui_triangle(dst, x + cx, y + cy, x + p[18], y + p[19], x + p[10], y + p[11], color);
    ui_line(dst, x + p[18], y + p[19], x + p[10], y + p[11], mainmenu_grccolor);
}

/**
 * Preview
 */
void mainmenu_preview(int type)
{
    FILE *f = NULL;
    Mix_Music *music = NULL;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *texture = NULL, *texture2 = NULL, *bg = NULL, *hdrtxt = NULL, *prx[3] = { 0 };
    SDL_Rect bgsrc = { 0 }, bgdst = { 0 }, hdrrect, hdrsrc, chrsrc, chrrect, cprrect, src, dst;
    ui_sprite_t *bgspr = NULL, *hdrspr = NULL, *s;
    ui_font_t hdr;
    int wf = SDL_GetWindowFlags(window), i, k, l, y, x, w, h, p, b, noaudio = 0, prxx[3], prxy[3], bh, gy = 0;
    ssfn_buf_t buf = { 0 };
    char *str, *menu[5], *opt[16], tmp[256];
    ui_tablehdr_t tab[] = { { GAME_CHARGEN, 120, 0, 0 }, { 0 } };
    uint32_t prxtime[3] = { 0 }, now, colors[3] = { 0xFF000080, 0xFF008000, 0xFF800000 };
    uint8_t *ptr;

    if(verbose) printf("mainmenu_preview: displaying %s\n", type ? "character generation" : "main menu");

    ui_font_load(&hdr, mainmenu_titstyle, mainmenu_titsize.val, mainmenu_titfont.val);
    ui_font_load(&mainmenu_fnt, mainmenu_btnstyle, mainmenu_btnsize.val, mainmenu_btnfont.val);

    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

    if(Mix_OpenAudio(FREQ, AUDIO_S16SYS, 2, 4096) < 0) { ui_status(1, lang[ERR_AUDIO]); noaudio = 1; }

    if(mainmenu_backmus.val >= 0) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA],
            project.music[mainmenu_backmus.val]);
        music = Mix_LoadMUS(projdir);
    }
    if(mainmenu_titimg.val >= 0) {
        hdrspr = spr_getsel(&mainmenu_titimg, 0);
        hdrsrc.x = hdrsrc.y = hdrrect.y = 0; hdrsrc.w = hdrrect.w = hdrspr->w; hdrsrc.h = hdrrect.h = hdrspr->h;
        hdrrect.x = (dm.w - hdrrect.w) / 2;
        spr_texture(hdrspr, &hdrtxt);
    }
    bgdst.w = dm.w; bgdst.h = dm.h;
    if(mainmenu_backimg.val >= 0) {
        bgspr = spr_getsel(&mainmenu_backimg, 0);
        spr_texture(bgspr, &bg);
        if(!mainmenu_nocrop) {
            ui_fit(bgspr->w, bgspr->h, dm.w, dm.h, &bgsrc.w, &bgsrc.h);
            bgsrc.x = (bgspr->w - bgsrc.w) / 2; bgsrc.y = (bgspr->h - bgsrc.h) / 2;
        } else {
            ui_fit(dm.w, dm.h, bgspr->w, bgspr->h, &bgdst.w, &bgdst.h);
            bgsrc.w = bgspr->w; bgsrc.h = bgspr->h;
        }
        for(i = 0; i < 3; i++)
            if(mainmenu_prximg[i].val >= 0) {
                s = spr_getsel(&mainmenu_prximg[i], 0);
                prxx[i] = mainmenu_numval[i * 3 + 0].val < 0 ? dm.w : (mainmenu_numval[i * 3 + 0].val > 0 ? -s->w :
                    (dm.w - s->w) / 2);
                prxy[i] = mainmenu_numval[i * 3 + 1].val < 0 ? dm.h : (mainmenu_numval[i * 3 + 1].val > 0 ? -s->h :
                    (dm.h - s->h) / 2);
                spr_texture(s, &prx[i]);
            }
    } else
    if(mainmenu_backmov.val >= 0) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s.ogv", project.id, project_dirs[PROJDIRS_MEDIA + 3],
            project.movies[mainmenu_backmov.val]);
        f = project_fopen(projdir, "rb");
        theora_start(&theora_ctx, NULL, f);
        if(theora_ctx.hasVideo) {
            bg = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, theora_ctx.w, theora_ctx.h);
            if(!bg) goto cleanup;
            /* planar YUV is green by default, let's make it black */
            SDL_LockTexture(bg, NULL, (void**)&ptr, &p);
            memset(ptr + (theora_ctx.w * theora_ctx.h), 0x80, (theora_ctx.w / 2) * theora_ctx.h);
            SDL_UnlockTexture(bg);
            if(!mainmenu_nocrop) {
                ui_fit(theora_ctx.w, theora_ctx.h, dm.w, dm.h, &bgsrc.w, &bgsrc.h);
                bgsrc.x = (theora_ctx.w - bgsrc.w) / 2; bgsrc.y = (theora_ctx.h - bgsrc.h) / 2;
            } else {
                ui_fit(dm.w, dm.h, theora_ctx.w, theora_ctx.h, &bgdst.w, &bgdst.h);
                bgsrc.w = theora_ctx.w; bgsrc.h = theora_ctx.h;
            }
        }
    }
    bgdst.x = (dm.w - bgdst.w) / 2; bgdst.y = (dm.h - bgdst.h) / 2;
    cprrect.x = bgdst.w ? bgdst.x : 0; cprrect.y = (bgdst.h ? bgdst.y + bgdst.h : dm.h) - ccbyncsa_icon.h;
    cprrect.w = ccbyncsa_icon.w; cprrect.h = ccbyncsa_icon.h;

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
    if(texture) {
        SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(texture, NULL, (void**)&buf.ptr, &p);
        memset(buf.ptr, 0, p * dm.h);
        y = (mainmenu_titimg.val >= 0 ? project.sprites[mainmenu_titimg.cat][mainmenu_titimg.val].dir[0].h : 32);
        if(mainmenu_tittgl) {
            buf.p = p;
            str = project.name;
            ssfn_bbox(&hdr.ctx, str, &w, &h, &p, &b);
            buf.w = dm.w;
            buf.h = dm.h;
            buf.bg = 0;
            buf.x = (dm.w - w) / 2 + 4;
            buf.y = y + 4 + b;
            buf.fg = mainmenu_titshadow;
            hdr.ctx.style &= ~SSFN_STYLE_A;
            while(*str && ((i = ssfn_render(&hdr.ctx, &buf, str)) > 0 || i == SSFN_ERR_NOGLYPH)) str += (i < 1 ? 1 : i);
            str = project.name;
            buf.x = (dm.w - w) / 2;
            buf.y -= 4;
            buf.fg = mainmenu_titcolor;
            while(*str && ((i = ssfn_render(&hdr.ctx, &buf, str)) > 0 || i == SSFN_ERR_NOGLYPH)) str += (i < 1 ? 1 : i);
            hdr.ctx.style |= SSFN_STYLE_A;
            y += hdr.ctx.line;
        }
        SDL_UnlockTexture(texture);
        if(type) {
            opt[0] = lang[MAINMENU_OPT1];
            opt[1] = lang[MAINMENU_OPT2];
            opt[2] = lang[MAINMENU_OPT3];
            opt[3] = "6";
            opt[4] = "7";
            opt[5] = "5";
            opt[6] = "10";
            opt[7] = "5";
            opt[8] = NULL;
            elements_load();
            elements_loadfonts();
            elements_winsizes(NULL, 0, NULL, &k, NULL, NULL, NULL);
            i = k + mainmenu_button(NULL, 0, 0, 0, 0, 0, NULL, 0, -1) + 3 * elements_pad[4].val +
                10 * (elements_option(NULL, 0, 0, 0, 0, 0, NULL, 0, 0, 3) + elements_pad[4].val);
            if(y + i > dm.h - 10) y = dm.h - 10 - i;
            y += k;
            elements_window(texture, dm.w, dm.h, dm.w / 4 - elements_pad[4].val, y, dm.w / 2 + 2 * elements_pad[4].val,
                dm.h - 20 - y, tab, 0);
            texture2 = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
            if(texture2) {
                SDL_SetTextureBlendMode(texture2, SDL_BLENDMODE_BLEND);
                SDL_LockTexture(texture2, NULL, (void**)&buf.ptr, &p);
                memset(buf.ptr, 0, p * dm.h);
                SDL_UnlockTexture(texture2);
                y += 2 * elements_pad[3].val + mainmenu_btnsize.val / 2;
                l = elements_text(texture2, dm.w, dm.h, dm.w / 4 + elements_pad[4].val, y - k / 2,
                    dm.w / 4 + dm.w / 8 + dm.w / 16, k, lang[GAME_USERNAME], 3);
                bh = mainmenu_button(texture2, dm.w, dm.h, dm.w / 4 + 2 * elements_pad[4].val + l, y, dm.w / 2 - l -
                    2 * elements_pad[4].val, lang[MAINMENU_MERLIN], 4, -2);
                y += bh + elements_pad[3].val + mainmenu_btnsize.val; chrrect.y = y;
                k = elements_option(texture2, dm.w, dm.h, dm.w / 4 + elements_pad[4].val, y, dm.w / 4, opt, 0, 0, 3);
                y += k;
                s = spr_getsel(&elements_sprites[10/*E_OPTNEXT*/], 0);
                for(x = 0; x < 3; x++)
                    ui_box(texture2, dm.w / 4 + elements_pad[4].val + (s ? s->w : 0) + x * k, y + k / 4, k / 2, k / 2,
                        elements_colors[!x ? 4 : 3], colors[x], elements_colors[!x ? 4 : 3]);
                y += k + elements_pad[3].val;
                y += 2 * elements_option(texture2, dm.w, dm.h, dm.w / 4 + elements_pad[4].val, y, dm.w / 4, opt, 0, 1, 3);
                y += elements_pad[3].val;
                k = elements_option(texture2, dm.w, dm.h, dm.w / 4 + elements_pad[4].val, y, dm.w / 4, opt, 0, 2, 3);
                y += 2 * k; chrrect.w = chrrect.h = 4 * (k + elements_pad[3].val);
                gy = y + elements_pad[3].val;
                for(i = 0; i < 5; i++, y += bh) {
                    y += elements_pad[3].val;
                    /* make the language checker happy: MAINMENU_ATTR2, MAINMENU_ATTR3, MAINMENU_ATTR4, MAINMENU_ATTR5 */
                    elements_text(texture2, dm.w, dm.h, dm.w / 4 + elements_pad[4].val, y, dm.w / 4 + dm.w / 8 + dm.w / 16,
                        k, lang[MAINMENU_ATTR1 + i], 3);
                    elements_option(texture2, dm.w, dm.h, dm.w / 4 + dm.w / 8 + dm.w / 16 + elements_pad[4].val, y,
                        -dm.w / 16, opt, 0, 3 + i, 3);
                }
                sprintf(tmp, "%s: 0", lang[GAME_REMAIN]);
                elements_text(texture2, dm.w, dm.h, dm.w / 4, y, dm.w / 4, k, tmp, -3);
                elements_button(texture2, dm.w, dm.h, dm.w / 2 - dm.w / 8 - dm.w / 16, dm.h - 2*k, dm.w / 8, lang[GAME_BACK], 0);
                elements_button(texture2, dm.w, dm.h, dm.w / 2 + dm.w / 8 - dm.w / 16, dm.h - 2*k, dm.w / 8, lang[GAME_CREATE],1);
                if(chrrect.w > dm.w / 8) chrrect.w = chrrect.h = dm.w / 8;
                chrrect.x = dm.w / 2 + dm.w / 8 - chrrect.w / 2; chrsrc.w = chrsrc.h = 64;
                chrsrc.y = charwalk_icon.y;
                mainmenu_graph(texture2, chrrect.x, gy, chrrect.w);
            }
            elements_freefonts();
        } else {
            menu[0] = lang[GAME_LOADGAME];
            menu[1] = lang[GAME_NEWGAME];
            menu[2] = lang[GAME_CONFIG];
            menu[3] = lang[GAME_EXIT];
            menu[4] = NULL;

            for(i = k = l = 0; menu[i]; i++, l++) {
                ssfn_bbox(&mainmenu_fnt.ctx, menu[i], &w, &h, &p, &b);
                if(w > k) k = w;
            }
            k += 48;
            for(i = w = h = 0; i < 15; i++) {
                s = spr_getsel(&mainmenu_btnspr[i], 0);
                if(s->w > w) w = s->w;
                if(s->h > h) h = s->h;
            }
            l = y + (dm.h - y - (l * (h + 10) - 10)) / 2;
            for(i = 0; menu[i]; i++, l += h + 10)
                mainmenu_button(texture, dm.w, dm.h, (dm.w - k - 2 * w) / 2, l, k + 2 * w, menu[i], i > 3 ? 0 : 3 - i, i);
        }
    }

    if(f && theora_ctx.hasAudio) {
        Mix_ChannelFinished(media_callback);
        media_callback(1);
    }
    if(music) {
        Mix_VolumeMusic(MIX_MAX_VOLUME);
        Mix_FadeInMusic(music, -1, 250);
    }

    while(1) {
        /* update screen */
        if(mainmenu_backimg.val >= 0 && (mainmenu_prximg[0].val >= 0 || mainmenu_prximg[1].val >= 0 ||
          mainmenu_prximg[2].val >= 0)) {
            now = SDL_GetTicks();
            for(i = 0; i < 3; i++)
                if(mainmenu_prximg[i].val >= 0 && prxtime[i] + mainmenu_numval[i * 3 + 2].val * 10 < now) {
                    prxtime[i] = now;
                    prxx[i] += mainmenu_numval[i * 3 + 0].val;
                    prxy[i] += mainmenu_numval[i * 3 + 1].val;
                    s = spr_getsel(&mainmenu_prximg[i], 0);
                    if(prxx[i] + s->w < 0 && mainmenu_numval[i * 3 + 0].val < 0) prxx[i] += 2 * s->w;
                    if(prxx[i] > dm.w && mainmenu_numval[i * 3 + 0].val > 0) prxx[i] -= 2 * s->w;
                    if(prxy[i] + s->h < 0 && mainmenu_numval[i * 3 + 1].val < 0) prxy[i] += 2 * s->h;
                    if(prxy[i] > dm.h && mainmenu_numval[i * 3 + 1].val > 0) prxy[i] -= 2 * s->h;
                }
        } else
            theora_video(&theora_ctx, bg);

        /* events */
        if(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
                SDL_PushEvent(&event); goto cleanup;
            }
            if(event.type == SDL_KEYUP || event.type == SDL_MOUSEBUTTONUP) goto cleanup;
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        if(bg) SDL_RenderCopy(renderer, bg, &bgsrc, &bgdst);
        if(bgspr) {
            SDL_RenderSetClipRect(renderer, &bgdst);
            for(i = 0; i < 3; i++)
                if(prx[i] && mainmenu_prximg[i].val >= 0) {
                    s = spr_getsel(&mainmenu_prximg[i], 0);
                    src.w = dst.w = s->w; src.x = 0;
                    src.h = dst.h = s->h; src.y = 0;
                    for(dst.y = -(dst.h - prxy[i] % dst.h); dst.y < dm.h; dst.y += dst.h)
                        for(dst.x = -(dst.w - prxx[i] % dst.w); dst.x < dm.w; dst.x += dst.w)
                            SDL_RenderCopy(renderer, prx[i], &src, &dst);
                }
            SDL_RenderSetClipRect(renderer, NULL);
        }
        if(hdrspr && hdrtxt) {
            hdrsrc.x = ui_anim(hdrspr->type, hdrspr->nframe, -1) * hdrspr->w;
            SDL_RenderCopy(renderer, hdrtxt, &hdrsrc, &hdrrect);
        }
        if(texture) SDL_RenderCopy(renderer, texture, NULL, NULL);
        if(texture2) {
            SDL_RenderCopy(renderer, texture2, NULL, NULL);
            chrsrc.x = charwalk_icon.x + ui_anim(2, 8, -1) * 64;
            SDL_RenderCopy(renderer, icons, &chrsrc, &chrrect);
        }
        if(!project.author[0])
            SDL_RenderCopy(renderer, icons, &ccbyncsa_icon, &cprrect);
        SDL_RenderPresent(renderer);
        SDL_Delay(10);
    }

cleanup:
    if(!noaudio) {
        Mix_ChannelFinished(NULL);
        if(theora_ctx.hasAudio) Mix_HaltChannel(-1);
        if(music) { Mix_HaltMusic(); Mix_FreeMusic(music); }
        Mix_CloseAudio();
    }
    ui_font_free(&hdr);
    ui_font_free(&mainmenu_fnt);
    theora_stop(&theora_ctx);
    for(i = 0; i < 3; i++)
        if(prx[i]) SDL_DestroyTexture(prx[i]);
    if(hdrtxt) SDL_DestroyTexture(hdrtxt);
    if(texture2) SDL_DestroyTexture(texture2);
    if(texture) SDL_DestroyTexture(texture);
    if(bg) SDL_DestroyTexture(bg);
    if(f) fclose(f);
    if(verbose) printf("mainmenu_preview: stopped\n");
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Save tng
 */
int mainmenu_totng(tngctx_t *ctx)
{
    int i, url;

    if(!ctx) return 1;
    mainmenu_load();
    tng_section(ctx, TNG_SECTION_MAINMENU);
    tng_ref(ctx, TNG_IDX_CUT, mainmenu_intro.val);
    tng_idx(ctx, project.cuts, mainmenu_intro.val);
    if(mainmenu_enabled) {
        tng_ref(ctx, TNG_IDX_MUS, mainmenu_backmus.val);
        tng_idx(ctx, project.music, mainmenu_backmus.val);
        tng_ref(ctx, TNG_IDX_MOV, mainmenu_backmov.val);
        tng_idx(ctx, project.movies, mainmenu_backmov.val);
        tng_sprite(ctx, mainmenu_backimg.cat, mainmenu_backimg.val);
        i = 3; tng_data(ctx, &i, 1);
        for(i = 0; i < 3; i++) {
            tng_data(ctx, &mainmenu_numval[i * 3 + 0].val, 2);
            tng_data(ctx, &mainmenu_numval[i * 3 + 1].val, 2);
            tng_data(ctx, &mainmenu_numval[i * 3 + 2].val, 2);
            tng_sprite(ctx, mainmenu_prximg[i].cat, mainmenu_prximg[i].val);
        }
        tng_sprite(ctx, mainmenu_titimg.cat, mainmenu_titimg.val);
        i = mainmenu_tittgl | (mainmenu_nocrop << 1);
        tng_data(ctx, &i, 1);
        tng_data(ctx, &mainmenu_titcolor, 4);
        tng_data(ctx, &mainmenu_titshadow, 4);
        tng_font(ctx, mainmenu_titstyle, mainmenu_titsize.val, mainmenu_titfont.val);
        url = tng_str(ctx, mainmenu_url);
        tng_data(ctx, &url, 3);
        tng_data(ctx, &mainmenu_btnpad.val, 2);
        tng_font(ctx, mainmenu_btnstyle, mainmenu_btnsize.val, mainmenu_btnfont.val);
        i = 5; tng_data(ctx, &i, 1);
        for(i = 0; i < 5; i++) {
            tng_data(ctx, &mainmenu_btncolor[i], 4);
            tng_sprite(ctx, mainmenu_btnspr[i * 3 + 0].cat, mainmenu_btnspr[i * 3 + 0].val);
            tng_sprite(ctx, mainmenu_btnspr[i * 3 + 1].cat, mainmenu_btnspr[i * 3 + 1].val);
            tng_sprite(ctx, mainmenu_btnspr[i * 3 + 2].cat, mainmenu_btnspr[i * 3 + 2].val);
        }
        tng_data(ctx, &mainmenu_grccolor, 4);
        tng_data(ctx, &mainmenu_grbcolor, 4);
    }
    return 1;
}

/**
 * Read from tng
 */
int mainmenu_fromtng(tngctx_t *ctx)
{
    FILE *f;
    uint8_t *buf;
    int i, l, len;

    if(!ctx || !ctx->tbl || !ctx->numtbl) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_MAINMENU; i++);
    if(i >= ctx->numtbl) return 1;
    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
    buf = (uint8_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]);
    if(len < 4) return 0;
    f = project_savefile(0, project_dirs[PROJDIRS_UI], "mainmenu", "cfg", PROJMAGIC_MAINMENU, "mainmenu_fromtng");
    if(f) {
        fprintf(f, "%u ", len > 3);
        buf = tng_wridx(ctx, buf, f);
        fprintf(f, "\r\n");
        if(len > 3) {
            buf = tng_wridx(ctx, buf, f); fprintf(f, " ");
            buf = tng_wridx(ctx, buf, f); fprintf(f, " ");
            buf = tng_wrsprite(ctx, buf, f); fprintf(f, "\r\n");
            len = *buf++; l = len < 3 ? len : 3;
            for(i = 0; i < l; i++) {
                if(i < 3 && i < len) {
                    fprintf(f, "%d %d %d ", *((int16_t*)buf), *((int16_t*)&buf[2]), *((int16_t*)&buf[4])); buf += 6;
                    buf = tng_wrsprite(ctx, buf, f);
                } else {
                    if(i < 3) fprintf(f, "0 0 0 -");
                    if(i < len) buf += 6;
                }
                fprintf(f, "\r\n");
            }
            buf = tng_wrsprite(ctx, buf, f); fprintf(f, "\r\n");
            fprintf(f, "%u %u %08X %08X ", (buf[0] & 2) >> 1, buf[0] & 1, *((uint32_t*)&buf[1]), *((uint32_t*)&buf[5])); buf += 9;
            buf = tng_wrfont(ctx, buf, f); fprintf(f, "\r\n");
            tng_wridx(ctx, buf, f); fprintf(f, "\r\n");
            fprintf(f, "%d ", *((int16_t*)buf)); buf += 2;
            buf = tng_wrfont(ctx, buf, f); fprintf(f, "\r\n");
            len = *buf++; l = len < 5 ? len : 5;
            for(i = 0; i < l; i++) {
                if(i < 5 && i < len) {
                    fprintf(f, "%08X ", *((uint32_t*)buf)); buf += 4;
                    buf = tng_wrsprite(ctx, buf, f); fprintf(f, " ");
                    buf = tng_wrsprite(ctx, buf, f); fprintf(f, " ");
                    buf = tng_wrsprite(ctx, buf, f);
                } else {
                    if(i < 3) fprintf(f, "00000000 - - -");
                    if(i < len) buf += 13;
                }
                fprintf(f, "\r\n");
            }
            fprintf(f, "%08X ", *((uint32_t*)buf)); buf += 4;
            fprintf(f, "%08X\r\n", *((uint32_t*)buf)); buf += 4;
        }
        fclose(f);
        return 1;
    }
    return 0;
}
