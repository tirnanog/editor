/*
 * tnge/loadproj.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Load project window
 *
 */

#include "main.h"

void loadproj_load(void *data);
void loadproj_delete(void *data);
void loadproj_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void loadproj_new(void *data) { (void)data; ui_switchtab(SUBMENU_NEWPROJ); }

ui_tablehdr_t loadproj_table_hdr[] = {
    { NEWPROJ_GAMEID, 160, 0, 0 },
    { NEWPROJ_TITLE, 0, 0, 0 },
    { 0 }
};

ui_table_t loadproj_table = { loadproj_table_hdr, LOADPROJ_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0,
    sizeof(char*), loadproj_drawcell, NULL, NULL, NULL };

/**
 * The form
 */
ui_form_t loadproj_form[] = {
    { FORM_TNGLOGO, 0, 29, 0, 0, 0, 0, site_url, NULL },
    { FORM_TABLE, 10, 99, 0, 0, 0, 0, &loadproj_table, loadproj_load },
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, LOADPROJ_DELETE, (void*)ICON_REMOVE, loadproj_delete },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LOADPROJ_ADD, (void*)ICON_ADD, loadproj_new },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, loadproj_load },
    { FORM_LAST }
};

/**
 * Draw table cell
 */
void loadproj_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    char *str = *((char**)data), tmp[16];

    (void)idx; (void)sel; (void)w; (void)h;
    ui_text(dst, x + 4, y, str + 128);
    ui_text(dst, x + 4 + hdr[0].w, y, str);
    if(str[154] < PROJ_FORMATREV) {
        sprintf(tmp, "(rev %u)", (unsigned char)str[154]);
        ui_text(dst, x + w - 80, y, tmp);
    }
}

/**
 * Exit load project window
 */
void loadproj_exit(int tab)
{
    (void)tab;
    project_freedir((char***)&loadproj_table.data, &loadproj_table.num);
    loadproj_table.val = loadproj_table.scr = 0;
    loadproj_table.clk = -1;
}

/**
 * Enter load project window
 */
void loadproj_init(int tab)
{
    char **projlist = NULL;

    loadproj_exit(tab);
    *projfn = 0;
    projlist = project_getdir(projdir, "@", &loadproj_table.num);
    if(projlist && projlist[0] && projlist[0][0])
        loadproj_table.data = projlist;
    else
        ui_switchtab(SUBMENU_ABOUT);
    loadproj_form[4].param = lang[LOADPROJ_LOAD];
    loadproj_form[4].w = ui_textwidth(lang[LOADPROJ_LOAD]) + 40;
}

/**
 * Resize the view
 */
void loadproj_resize(int tab, int w, int h)
{
    (void)tab;
    loadproj_form[0].x = (w - tirnanog_icon.w) / 2;
    loadproj_form[0].w = tirnanog_icon.w;
    loadproj_form[0].h = tirnanog_icon.h;

    ui_table_resize(&loadproj_form[1], w - 20, h - 159);

    loadproj_form[2].y = loadproj_form[3].y = loadproj_form[4].y = h - 48;
    loadproj_form[3].x = w - 44;
    loadproj_form[4].x = w - 64 - loadproj_form[4].w;
}

/**
 * View layer
 */
void loadproj_redraw(int tab)
{
    (void)tab;
    ui_form = loadproj_form;
    ui_form[2].inactive = ui_form[4].inactive = loadproj_table.val < 0;
}

/**
 * Delete button handler
 */
void loadproj_delete(void *data)
{
    char **projlist = (char**)loadproj_table.data;

    (void)data;
    if(loadproj_table.val < 0) return;
    if(ui_modal(ICON_WARN, LANG_SURE, projlist[loadproj_table.val])) {
        project_delete(projlist[loadproj_table.val] + 128);
        loadproj_init(0);
    }
}

/**
 * Load button handler
 */
void loadproj_load(void *data)
{
    char **projlist = (char**)loadproj_table.data;

    (void)data;
    if(loadproj_table.val < 0) return;
    project_free();
    project_load(projlist[loadproj_table.val] + 128);
    ui_switchtab(SUBMENU_MAPS);
}
