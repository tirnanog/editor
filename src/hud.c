/*
 * tnge/hud.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief HUD configuration window
 *
 */

#include "main.h"

typedef struct {
    int type;                       /* progress bar type */
    int left;                       /* left margin */
    int top;                        /* top margin */
    int val;                        /* value attribute index */
    int max;                        /* maximum attribute index */
    int img;                        /* gauge sprite index */
    int back;                       /* background sprite index */
} hud_pbar_t;

void hud_save(void *data);
void hud_delete(void *data);
void hud_preview(void *data);
void hud_pbarerase(void *data);
void hud_pbaradd(void *data);
void hud_pbarclk(void *data);
void hud_equiperase(void *data);
void hud_equipadd(void *data);
void hud_equipclk(void *data);
void hud_drawpbar(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void hud_drawequip(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

char hud_equipname[PROJ_NAMEMAX + 1];
int hud_tab, hud_combined = 0, hud_invfstyle = 0;
ui_sprsel_t hud_nav = { -1, 0, 155, 1 };
ui_sprsel_t hud_btns = { -1, 0, 156, 1 };
ui_sprsel_t hud_itemb = { -1, 0, 25, 1 };
ui_sprsel_t hud_itemf = { -1, 0, 25, 1 };
ui_sprsel_t hud_statb = { -1, 0, 25, 1 };
ui_sprsel_t hud_statf = { -1, 0, 25, 1 };
ui_num_t hud_itemn = { 4, 0, 9, 1 };
ui_num_t hud_itemw = { 16, 1, 256, 1 };
ui_num_t hud_itemh = { 16, 1, 256, 1 };
ui_num_t hud_iteml = { 0, 0, 512, 1 };
ui_num_t hud_itemt = { 0, 0, 512, 1 };
ui_num_t hud_itemH = { 0, 0, 512, 1 };
ui_num_t hud_itema = { 0, 0, 512, 1 };
char *hud_pbardirs[] = { "\x17", "\x15", "\x10", " @", NULL };
ui_select_t hud_pbardir = { 0, 0, hud_pbardirs };
ui_num_t hud_pbarl = { 0, 0, 512, 1 };
ui_num_t hud_pbart = { 0, 0, 512, 1 };
ui_coord_t hud_pbarcrd = { NULL, NULL, NULL, &hud_pbarl.val, &hud_pbart.val };
ui_select_t hud_pbarval = { 0, 0, NULL };
ui_select_t hud_pbarmax = { 0, 0, NULL };
ui_sprsel_t hud_pbarimg = { -1, 0, 25, 1 };
ui_sprsel_t hud_pbarback = { -1, 0, 25, 1 };
ui_sprsel_t hud_inv = { -1, 0, 25, 1 };
ui_sprsel_t hud_invp = { -1, 0, 25, 1 };
ui_num_t hud_invw = { 16, 1, 256, 1 };
ui_num_t hud_invh = { 16, 1, 256, 1 };
ui_num_t hud_invl = { 0, 0, 512, 1 };
ui_num_t hud_invt = { 0, 0, 512, 1 };
ui_icontgl_t hud_invfbold = { ICON_BOLD, SSFN_STYLE_BOLD, &hud_invfstyle };
ui_icontgl_t hud_invfitalic = { ICON_ITALIC, SSFN_STYLE_ITALIC, &hud_invfstyle };
ui_num_t hud_invfsize = { 8, 8, 192, 1 };
ui_select_t hud_invfnt = { -1, LANG_DEF, NULL };
ui_sprsel_t hud_invimg = { -1, 0, 25, 1 };
ui_sprsel_t hud_invsel = { -1, 0, 25, 1 };
ui_sprsel_t hud_invequip = { -1, 0, 25, 1 };
ui_num_t hud_equipl = { 0, 0, 1024, 1 };
ui_num_t hud_equipt = { 0, 0, 1024, 1 };
ui_input_t hud_equipinp = { INP_ID, sizeof(hud_equipname), hud_equipname };
ui_coord_t hud_equipcrd = { &hud_invequip, &hud_invw.val, &hud_invh.val, &hud_equipl.val, &hud_equipt.val };

ui_tablehdr_t hud_pbartbl_hdr[] = {
    { HUD_DIRECTION, 80, 0, 0 },
    { -0x1C, 60, 0, 0 },
    { -0x1D, 60, 0, 0 },
    { HUD_VALUE, 0, 0, 0 },
    { HUD_MAXIMUM, 0, 0, 0 },
    { HUD_IMAGE, 0, 0, 0 },
    { 0 }
};
ui_table_t hud_pbartbl = { hud_pbartbl_hdr, HUD_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(hud_pbar_t), hud_drawpbar,
    NULL, NULL, NULL };

ui_tablehdr_t hud_equiptbl_hdr[] = {
    { -'#', 42, 0, 0 },
    { -0x1C, 60, 0, 0 },
    { -0x1D, 60, 0, 0 },
    { HUD_INVSLOT, 0, 0, 0 },
    { 0 }
};
ui_table_t hud_equiptbl = { hud_equiptbl_hdr, HUD_INVNONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(hud_equip_t), hud_drawequip,
    NULL, NULL, NULL };

/**
 * The form
 */
ui_form_t hud_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, HUD_DELETE, (void*)ICON_REMOVE, hud_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, hud_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, HUD_PREVIEW, (void*)ICON_PVIEW, hud_preview },
    /* 3 */
    { FORM_TEXT, 10, 29, 0, 18, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 29, 0, 20, 0, HUD_NAV, &hud_nav, NULL },
    { FORM_SPRITE, 0, 29, 0, 20, 0, HUD_BTNS, &hud_btns, NULL },
    /* 6 */
    { FORM_TEXT, 10, 53, 0, 18, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 53, 0, 20, 0, HUD_BACK, &hud_itemb, NULL },
    { FORM_SPRITE, 0, 53, 0, 20, 0, HUD_FRONTOPT, &hud_itemf, NULL },
    { FORM_TEXT, 0, 54, 14, 18, 0, 0, "#", NULL },
    { FORM_NUM, 0, 53, 42, 18, 0, HUD_NUM, &hud_itemn, NULL },
    { FORM_TEXT, 0, 54, 22, 18, 0, 0, "\x1A", NULL },
    { FORM_NUM, 0, 53, 42, 18, 0, HUD_WIDTH, &hud_itemw, NULL },
    { FORM_TEXT, 0, 54, 22, 18, 0, 0, "\x1B", NULL },
    { FORM_NUM, 0, 53, 42, 18, 0, HUD_HEIGHT, &hud_itemh, NULL },
    { FORM_TEXT, 0, 54, 22, 18, 0, 0, "\x1C", NULL },
    { FORM_NUM, 0, 53, 42, 18, 0, HUD_POS, &hud_iteml, NULL },
    { FORM_TEXT, 0, 54, 22, 18, 0, 0, "\x1D", NULL },
    { FORM_NUM, 0, 53, 42, 18, 0, HUD_POS, &hud_itemt, NULL },
    { FORM_TEXT, 0, 54, 20, 18, 0, 0, "\x1E", NULL },
    { FORM_NUM, 0, 53, 42, 18, 0, HUD_VGAP, &hud_itemH, NULL },
    { FORM_TEXT, 0, 54, 42, 18, 0, 0, "\xC2\x83\x1E", NULL },
    { FORM_NUM, 0, 53, 42, 18, 0, HUD_HAND, &hud_itema, NULL },
    /* 23 */
    { FORM_TEXT, 10, 77, 0, 18, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 77, 0, 20, 0, HUD_BACK, &hud_statb, NULL },
    { FORM_SPRITE, 0, 77, 0, 20, 0, HUD_FRONTOPT, &hud_statf, NULL },
    { FORM_BOOL, 0, 77, 0, 18, 0, HUD_COMBINED, &hud_combined, NULL },
    /* 27 */
    { FORM_TEXT, 10, 101, 0, 18, 0, 0, NULL, NULL },
    { FORM_TABLE, 0, 101, 0, 0, 0, 0, &hud_pbartbl, hud_pbarclk },
    /* 29 */
    { FORM_ICONBTN, 0, 0, 18, 18, 0, HUD_ERASE, (void*)ICON_REMOVE, hud_pbarerase },
    /* make the language checker happy: HUD_DIR_1, HUD_DIR_2, HUD_DIR_3 */
    { FORM_SELECT, 0, 0, 0, 18, 0, -HUD_DIR_0, &hud_pbardir, NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, HUD_PPOS, &hud_pbarl, NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, HUD_EFFBAR, &hud_pbart, NULL },
    { FORM_COORD, 0, 0, 18, 18, 0, HUD_PPOSHINT, &hud_pbarcrd, NULL },
    { FORM_SELECT, 0, 0, 0, 18, 0, HUD_VALUE, &hud_pbarval, NULL },
    { FORM_SELECT, 0, 0, 0, 18, 0, HUD_MAXIMUM, &hud_pbarmax, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, HUD_FRONT, &hud_pbarimg, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, HUD_BACKOPT, &hud_pbarback, NULL },
    { FORM_ICONBTN, 0, 0, 18, 18, 0, HUD_ADD, (void*)ICON_ADD, hud_pbaradd },
    /* 39 */
    { FORM_TEXT, 10, 0, 0, 18, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 40, 20, 0, HUD_INVICON, &hud_inv, NULL },
    { FORM_SPRITE, 0, 0, 40, 20, 0, HUD_INVPRESSED, &hud_invp, NULL },
    { FORM_TEXT, 0, 0, 22, 18, 0, 0, "\x1A", NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, HUD_INVWIDTH, &hud_invw, NULL },
    { FORM_TEXT, 0, 0, 22, 18, 0, 0, "\x1B", NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, HUD_INVHEIGHT, &hud_invh, NULL },
    { FORM_TEXT, 0, 0, 22, 18, 0, 0, "\x1C", NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, HUD_INVPOS, &hud_invl, NULL },
    { FORM_TEXT, 0, 0, 22, 18, 0, 0, "\x1D", NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, HUD_INVPOS, &hud_invt, NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, HUD_INVNUM, &hud_invfbold, NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, HUD_INVNUM, &hud_invfitalic, NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, HUD_INVNUM, &hud_invfsize, NULL },
    { FORM_SELECT, 0, 0, 0, 18, 0, HUD_INVNUM, &hud_invfnt, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, HUD_INVITEM, &hud_invimg, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, HUD_INVSEL, &hud_invsel, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, HUD_INVEQUIP, &hud_invequip, NULL },
    /* 57 */
    { FORM_TABLE, 0, 0, 0, 100, 0, 0, &hud_equiptbl, hud_equipclk },
    { FORM_ICONBTN, 0, 0, 18, 18, 0, HUD_INVERASE, (void*)ICON_REMOVE, hud_equiperase },
    { FORM_NUM, 0, 0, 42, 18, 0, HUD_INVEPOS, &hud_equipl, NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, HUD_INVEPOS, &hud_equipt, NULL },
    { FORM_COORD, 0, 0, 18, 18, 0, HUD_INVEPOS, &hud_equipcrd, NULL },
    { FORM_INPUT, 0, 0, 0, 18, 0, HUD_INVNAME, &hud_equipinp, NULL },
    { FORM_ICONBTN, 0, 0, 18, 18, 0, HUD_INVADD, (void*)ICON_ADD, hud_equipadd },

    { FORM_LAST }
};

/**
 * Draw table cell
 */
void hud_drawpbar(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    hud_pbar_t *pbar = (hud_pbar_t*)data;
    char tmp[8];
    int t = ui_clip.w;
    SDL_Rect rect;
    ui_sprite_t *s;

    if(!data) return;
    ui_text(dst, x + 20, y, hud_pbardirs[pbar->type]);
    sprintf(tmp, "%3d", pbar->left);
    ui_text(dst, x + hdr[0].w + hdr[1].w - 32, y, tmp);
    sprintf(tmp, "%3d", pbar->top);
    ui_text(dst, x + hdr[0].w + hdr[1].w + hdr[2].w - 32, y, tmp);
    ui_clip.w = hdr[0].w + hdr[1].w + hdr[3].w;
    ui_text(dst, x + hdr[0].w + hdr[1].w + hdr[2].w + 4, y, project.attrs[pbar->val]);
    ui_clip.w = hdr[0].w + hdr[1].w + hdr[3].w + hdr[4].w;
    ui_text(dst, x + hdr[0].w + hdr[1].w + hdr[2].w + hdr[3].w + 4, y, project.attrs[pbar->max]);
    ui_clip.w = t;
    w = (pbar->back >= 0 ? (hdr[5].w - 16) / 2 : hdr[5].w - 16);
    x += hdr[0].w + hdr[1].w + hdr[2].w + hdr[3].w + hdr[4].w + 4;
    s = spr_getidx(hud_pbarimg.cat, pbar->img, -1, 0);
    if(s) {
        ui_fit(w, h - 2, s->w, s->h, &rect.w, &rect.h);
        rect.x = x + (w - rect.w) / 2;
        rect.y = y + h - 1 - rect.h;
        spr_blit(s, idx == sel ? 255 : 160, dst, &rect);
        x += w;
    }
    s = spr_getidx(hud_pbarimg.cat, pbar->back, -1, 0);
    if(s) {
        ui_fit(w, h - 2, s->w, s->h, &rect.w, &rect.h);
        rect.x = x + (w - rect.w) / 2;
        rect.y = y + h - 1 - rect.h;
        spr_blit(s, idx == sel ? 255 : 160, dst, &rect);
    }
}

/**
 * Draw table cell
 */
void hud_drawequip(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    hud_equip_t *slot = (hud_equip_t*)data;
    char tmp[32] = " \xC2\x83";

    (void)dst; (void)sel; (void)w; (void)h;
    if(!data) return;
    if(idx) sprintf(tmp, "%3d.", idx + 1);
    ui_text(dst, x + hdr[0].w - 36, y, tmp);
    sprintf(tmp, "%3d", slot->left);
    ui_text(dst, x + hdr[0].w + hdr[1].w - 32, y, tmp);
    sprintf(tmp, "%3d", slot->top);
    ui_text(dst, x + hdr[0].w + hdr[1].w + hdr[2].w - 32, y, tmp);
    ui_text(dst, x + hdr[0].w + hdr[1].w + hdr[2].w + 4, y, slot->name);
}

/**
 * Exit hud window
 */
void hud_exit(int tab)
{
    (void)tab;

    hud_combined = hud_invfstyle = 0; hud_equipname[0] = 0;
    hud_nav.val = hud_btns.val = hud_itemb.val = hud_itemf.val = hud_statb.val = hud_statf.val = -1;
    hud_itemn.val = 4; hud_itemw.val = hud_itemh.val = hud_invw.val = hud_invh.val = 16;
    hud_iteml.val = hud_itemt.val = hud_itemH.val = hud_itema.val = hud_pbardir.val = hud_pbarl.val = hud_pbart.val =
        hud_pbarval.val = hud_pbarmax.val = hud_invl.val = hud_invt.val = hud_equipl.val = hud_equipt.val = 0;
    hud_pbarimg.val = hud_pbarback.val = hud_inv.val = hud_invp.val = hud_invfnt.val = hud_invimg.val = hud_invsel.val =
        hud_invequip.val = -1;
    hud_invfsize.val = 8; hud_pbartbl.num = hud_equiptbl.num = 0; hud_pbartbl.val = hud_equiptbl.val = -1;
    if(hud_pbartbl.data) { free(hud_pbartbl.data); hud_pbartbl.data = NULL; }
    if(hud_equiptbl.data) { free(hud_equiptbl.data); hud_equiptbl.data = NULL; }
}

/**
 * Enter hud window
 */
void hud_init(int tab)
{
    int i;

    hud_exit(tab);
    hud_form[1].param = lang[LANG_SAVE];
    hud_form[1].w = ui_textwidth(hud_form[1].param) + 40;
    if(hud_form[1].w < 200) hud_form[1].w = 200;
    hud_form[3].param = lang[HUD_CONTROLS];
    hud_tab = ui_textwidth(hud_form[3].param);
    hud_form[6].param = lang[HUD_ITEMBAR];
    i = ui_textwidth(hud_form[6].param); if(i > hud_tab) hud_tab = i;
    hud_form[23].param = lang[HUD_STATUSBAR];
    i = ui_textwidth(hud_form[23].param); if(i > hud_tab) hud_tab = i;
    hud_form[27].param = lang[HUD_PROGBARS];
    i = ui_textwidth(hud_form[27].param); if(i > hud_tab) hud_tab = i;
    hud_form[39].param = lang[HUD_INVENTORY];
    i = ui_textwidth(hud_form[39].param); if(i > hud_tab) hud_tab = i;
    hud_tab += 20;
    hud_pbarval.opts = hud_pbarmax.opts = project.attrs;
    hud_invfnt.opts = project.fonts;
    hud_load();
}

/**
 * Resize the view
 */
void hud_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    hud_form[0].y = hud_form[1].y = hud_form[2].y = h - 48;
    hud_form[1].x = w - 20 - hud_form[1].w;
    hud_form[2].x = hud_form[1].x - 52;
    hud_form[4].x = hud_form[7].x = hud_form[24].x = hud_form[28].x = hud_form[29].x = hud_form[40].x = hud_form[57].x =
        hud_form[58].x = hud_tab;
    hud_form[4].w = hud_form[5].w = (scr_w - 20 - hud_tab) / 2;
    hud_form[5].x = hud_form[4].x + hud_form[4].w + 10;

    hud_form[22].x = scr_w - 10 - hud_form[22].w;
    for(i = 21; i > 8; i--) {
        hud_form[i].x = hud_form[i + 1].x - hud_form[i].w;
        if(!(i & 1)) hud_form[i + 1].x += 4;
    }
    hud_form[9].x += 4; hud_form[26].x = hud_form[9].x;
    hud_form[7].w = hud_form[8].w = hud_form[24].w = hud_form[25].w = (hud_form[9].x - 10 - hud_form[7].x) / 2 - 10;
    hud_form[8].x = hud_form[25].x = hud_form[7].x + hud_form[7].w + 10;
    hud_form[26].w = scr_w - 10 - hud_form[26].x;
    ui_table_resize(&hud_form[28], scr_w - 10 - hud_tab, scr_h / 2 - 28 - 8 - hud_form[28].y);
    hud_form[29].y = hud_form[30].y = hud_form[31].y = hud_form[32].y = hud_form[33].y = hud_form[34].y =
        hud_form[35].y = hud_form[36].y = hud_form[37].y = hud_form[38].y = scr_h / 2 - 32;
    hud_form[30].x = hud_form[29].x + hud_form[29].w + 20;
    hud_form[30].w = hud_form[28].x + hud_pbartbl_hdr[0].w - hud_form[30].x - 2;
    hud_form[31].x = hud_form[30].x + hud_form[30].w + 8;
    hud_form[32].x = hud_form[31].x + hud_form[31].w + 2;
    hud_form[33].x = hud_form[32].x + hud_form[32].w + 2;
    hud_form[34].x = hud_form[28].x + hud_pbartbl_hdr[0].w + hud_pbartbl_hdr[1].w + hud_pbartbl_hdr[2].w + 2;
    hud_form[34].w = hud_pbartbl_hdr[3].w - 4;
    hud_form[35].x = hud_form[34].x + hud_form[34].w + 4;
    hud_form[35].w = hud_pbartbl_hdr[4].w - 4;
    hud_form[36].x = hud_form[35].x + hud_form[35].w + 4;
    hud_form[38].x = scr_w - 12 - hud_form[38].w;
    hud_form[36].w = hud_form[37].w = (hud_form[38].x - 20 - hud_form[36].x) / 2;
    hud_form[37].x = hud_form[36].x + hud_form[36].w + 4;
    hud_form[39].y = hud_form[40].y = hud_form[41].y = hud_form[43].y = hud_form[45].y = hud_form[47].y =
        hud_form[49].y = hud_form[50].y = hud_form[51].y = hud_form[52].y = hud_form[53].y = hud_form[54].y = hud_form[55].y =
        hud_form[56].y = scr_h / 2;
    hud_form[42].y = hud_form[44].y = hud_form[46].y = hud_form[48].y = scr_h / 2 + 1;
    hud_form[41].x = hud_form[40].x + hud_form[40].w + 4;
    hud_form[42].x = hud_form[41].x + hud_form[41].w + 20;
    for(i = 43; i < 50; i++) {
        hud_form[i].x = hud_form[i - 1].x + hud_form[i - 1].w - ((i & 1) ? 4 : 0);
        if(!(i & 1)) hud_form[i].x += 4;
    }
    hud_form[50].x = hud_form[49].x + hud_form[49].w + 20;
    for(i = 51; i < 54; i++)
        hud_form[i].x = hud_form[i - 1].x + hud_form[i - 1].w + 4;
    hud_form[53].w = hud_form[54].w = hud_form[55].w = hud_form[56].w = (scr_w - 20 - hud_form[53].x) / 4 - 4;
    hud_form[54].x = hud_form[53].x + hud_form[53].w + 20;
    hud_form[55].x = hud_form[54].x + hud_form[54].w + 4;
    hud_form[56].x = hud_form[55].x + hud_form[55].w + 4;
    hud_form[57].y = hud_form[40].y + 24;
    ui_table_resize(&hud_form[57], scr_w - 10 - hud_tab, scr_h - 88 - hud_form[57].y);
    hud_form[58].y = hud_form[59].y = hud_form[60].y = hud_form[61].y = hud_form[62].y = hud_form[63].y = scr_h - 84;
    hud_form[59].x = hud_form[58].x + hud_form[58].w + 20;
    hud_form[60].x = hud_form[59].x + hud_form[59].w + 2;
    hud_form[61].x = hud_form[60].x + hud_form[60].w + 2;
    hud_form[62].x = hud_form[57].x + hud_equiptbl_hdr[0].w + hud_equiptbl_hdr[1].w + hud_equiptbl_hdr[2].w + 2;
    hud_form[63].x = scr_w - 12 - hud_form[63].w;
    hud_form[62].w = hud_form[63].x - 10 - hud_form[62].x;
}

/**
 * View layer
 */
void hud_redraw(int tab)
{
    (void)tab;
    ui_form = hud_form;
    hud_form[24].inactive = (hud_form[25].inactive = hud_combined);
    hud_form[29].inactive = (hud_pbartbl.val < 0);
    hud_form[38].inactive = (hud_pbarval.val == hud_pbarmax.val || hud_pbarimg.val == -1);
    hud_pbarcrd.spr = hud_combined ? &hud_itemb : &hud_statb;
    hud_form[33].inactive = (hud_pbarcrd.spr->val < 0);
    if(hud_pbarimg.val >= 0) {
        hud_pbarcrd.w = &project.sprites[hud_pbarimg.cat][hud_pbarimg.val].dir[0].w;
        hud_pbarcrd.h = &project.sprites[hud_pbarimg.cat][hud_pbarimg.val].dir[0].h;
    } else
        hud_pbarcrd.w = hud_pbarcrd.h = NULL;
    hud_form[58].inactive = (hud_equiptbl.val < 0);
    hud_form[61].inactive = (hud_invequip.val < 0);
    hud_form[63].inactive = (!hud_equipname[0] || hud_equiptbl.num >= 32);
}

/**
 * Erase progressbar
 */
void hud_pbarerase(void *data)
{
    hud_pbar_t *list = (hud_pbar_t*)hud_pbartbl.data;

    (void)data;
    if(!list || hud_pbartbl.val < 0 || hud_pbartbl.val >= hud_pbartbl.num) return;
    memcpy(&list[hud_pbartbl.val], &list[hud_pbartbl.val + 1], (hud_pbartbl.num - hud_pbartbl.val) * sizeof(hud_pbar_t));
    hud_pbartbl.num--;
    if(hud_pbartbl.val >= hud_pbartbl.num)
        hud_pbartbl.val = hud_pbartbl.num - 1;
    if(!hud_pbartbl.num) { free(list); hud_pbartbl.data = NULL; }
}

/**
 * Add a progressbar
 */
void hud_pbaradd(void *data)
{
    hud_pbar_t *list = (hud_pbar_t*)hud_pbartbl.data;
    int i;

    (void)data;
    if(hud_pbardir.val < 0 || hud_pbardir.val > 3 || hud_pbarl.val < 0 || hud_pbart.val < 0 || hud_pbarval.val < 0 ||
      hud_pbarmax.val < 0 || hud_pbarval.val == hud_pbarmax.val || hud_pbarimg.val < 0 ||
      hud_pbarimg.val >= project.spritenum[hud_pbarimg.cat]) return;
    if(project.sprites[hud_pbarimg.cat][hud_pbarimg.val].dir[0].w > 500) {
        ui_status(1, lang[HUD_TOOWIDE]);
        return;
    }
    list = (hud_pbar_t*)realloc(list, (hud_pbartbl.num + 1) * sizeof(hud_pbar_t));
    hud_pbartbl.data = list;
    if(!list) main_error(ERR_MEM);
    if(hud_pbardir.val == 3) {
        for(i = 0; i < hud_pbartbl.num && list[i].type != 3; i++);
    } else
        i = hud_pbartbl.num;
    hud_pbartbl.val = i;
    list[i].type = hud_pbardir.val;
    list[i].left = hud_pbarl.val;
    list[i].top = hud_pbart.val;
    list[i].val = hud_pbarval.val;
    list[i].max = hud_pbarmax.val;
    list[i].img = hud_pbarimg.val;
    list[i].back = (hud_pbarback.val < 0 || hud_pbarback.val >= project.spritenum[hud_pbarback.cat] ||
        project.sprites[hud_pbarback.cat][hud_pbarback.val].dir[0].w > 500 ? -1 : hud_pbarback.val);
    if(i == hud_pbartbl.num) hud_pbartbl.num++;
    hud_pbardir.val = 0;
    hud_pbarimg.val = hud_pbarback.val = -1;
}

/**
 * Click on progressbar table
 */
void hud_pbarclk(void *data)
{
    hud_pbar_t *list = (hud_pbar_t*)hud_pbartbl.data;

    (void)data;
    if(!list || hud_pbartbl.val < 0 || hud_pbartbl.val >= hud_pbartbl.num) return;
    if(ui_curx > ui_form[28].x && ui_curx < ui_form[28].x + hud_pbartbl.hdr[0].w) {
        if(ui_curbtn == 1 && list[hud_pbartbl.val].type < 2) list[hud_pbartbl.val].type++;
        if(ui_curbtn != 1 && list[hud_pbartbl.val].type > 0) list[hud_pbartbl.val].type--;
    }
    if(ui_curx > ui_form[28].x + hud_pbartbl.hdr[0].w && ui_curx < ui_form[28].x + hud_pbartbl.hdr[0].w + hud_pbartbl.hdr[1].w) {
        if(ui_curbtn == 1 && list[hud_pbartbl.val].left < hud_pbarl.max) list[hud_pbartbl.val].left++;
        if(ui_curbtn != 1 && list[hud_pbartbl.val].left > 0) list[hud_pbartbl.val].left--;
    }
    if(ui_curx > ui_form[28].x + hud_pbartbl.hdr[0].w + hud_pbartbl.hdr[1].w && ui_curx < ui_form[28].x + hud_pbartbl.hdr[0].w +
      hud_pbartbl.hdr[1].w + hud_pbartbl.hdr[2].w) {
        if(ui_curbtn == 1 && list[hud_pbartbl.val].top < hud_pbart.max) list[hud_pbartbl.val].top++;
        if(ui_curbtn != 1 && list[hud_pbartbl.val].top > 0) list[hud_pbartbl.val].top--;
    }
}

/**
 * Erase equipment slot
 */
void hud_equiperase(void *data)
{
    hud_equip_t *list = (hud_equip_t*)hud_equiptbl.data;

    (void)data;
    if(!list || hud_equiptbl.val < 0 || hud_equiptbl.val >= hud_equiptbl.num) return;
    memcpy(&list[hud_equiptbl.val], &list[hud_equiptbl.val + 1], (hud_equiptbl.num - hud_equiptbl.val) * sizeof(hud_equip_t));
    hud_equiptbl.num--;
    if(hud_equiptbl.val >= hud_equiptbl.num)
        hud_equiptbl.val = hud_equiptbl.num - 1;
    if(!hud_equiptbl.num) { free(list); hud_equiptbl.data = NULL; }
}

/**
 * Add an equipment slot
 */
void hud_equipadd(void *data)
{
    hud_equip_t *list = (hud_equip_t*)hud_equiptbl.data;
    int i;

    (void)data;
    if(hud_equipl.val < 0 || hud_equipt.val < 0 || !hud_equipname[0] || hud_equiptbl.num >= 32) return;
    for(i = 0; i < hud_equiptbl.num; i++)
        if(!strcmp(list[i].name, hud_equipname)) {
            ui_status(1, lang[HUD_INVNOTUNIQUE]);
            return;
        }
    list = (hud_equip_t*)realloc(list, (hud_equiptbl.num + 1) * sizeof(hud_equip_t));
    hud_equiptbl.data = list;
    if(!list) main_error(ERR_MEM);
    hud_equiptbl.val = hud_equiptbl.num;
    list[hud_equiptbl.num].left = hud_equipl.val;
    list[hud_equiptbl.num].top = hud_equipt.val;
    strcpy(list[hud_equiptbl.num].name, hud_equipname);
    hud_equiptbl.num++;
    hud_equipname[0] = 0;
}

/**
 * Click on equipment slots table
 */
void hud_equipclk(void *data)
{
    hud_equip_t *list = (hud_equip_t*)hud_equiptbl.data;

    (void)data;
    if(!list || hud_equiptbl.val < 0 || hud_equiptbl.val >= hud_equiptbl.num) return;
    if(ui_curx > ui_form[57].x + hud_equiptbl.hdr[0].w && ui_curx < ui_form[57].x + hud_equiptbl.hdr[0].w + hud_equiptbl.hdr[1].w) {
        if(ui_curbtn == 1 && list[hud_equiptbl.val].left < hud_equipl.max) list[hud_equiptbl.val].left++;
        if(ui_curbtn != 1 && list[hud_equiptbl.val].left > 0) list[hud_equiptbl.val].left--;
    }
    if(ui_curx > ui_form[57].x + hud_equiptbl.hdr[0].w + hud_equiptbl.hdr[1].w && ui_curx < ui_form[57].x + hud_equiptbl.hdr[0].w +
      hud_equiptbl.hdr[1].w + hud_equiptbl.hdr[2].w) {
        if(ui_curbtn == 1 && list[hud_equiptbl.val].top < hud_equipt.max) list[hud_equiptbl.val].top++;
        if(ui_curbtn != 1 && list[hud_equiptbl.val].top > 0) list[hud_equiptbl.val].top--;
    }
}

/**
 * Delete hud
 */
void hud_delete(void *data)
{
    (void)data;
    sprintf(projfn, "%s" SEP "%s" SEP "hud.cfg", project.id, project_dirs[PROJDIRS_UI]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("hud_delete: removing %s\n", projdir);
        remove(projdir);
        hud_exit(SUBMENU_HUD);
    }
}

/**
 * Save hud configuration
 */
void hud_save(void *data)
{
    hud_pbar_t *pbar = (hud_pbar_t*)hud_pbartbl.data;
    hud_equip_t *equip = (hud_equip_t*)hud_equiptbl.data;
    FILE *f;
    int i;

    (void)data;
    if(hud_pbarimg.val >= 0 && hud_pbarimg.val < project.spritenum[hud_pbarimg.cat] &&
      project.sprites[hud_pbarimg.cat][hud_pbarimg.val].dir[0].w > 500) {
        ui_status(1, lang[HUD_TOOWIDE]);
        return;
    }
    f = project_savefile(0, project_dirs[PROJDIRS_UI], "hud", "cfg", PROJMAGIC_HUD, "hud_save");
    if(f) {
        project_wrsprite(f, &hud_nav); fprintf(f, " ");
        project_wrsprite(f, &hud_btns); fprintf(f, "\r\n");
        project_wrsprite(f, &hud_itemb); fprintf(f, " ");
        project_wrsprite(f, &hud_itemf); fprintf(f, "\r\n");
        fprintf(f, "%u %u %u %u %u %u %u\r\n", hud_itemn.val, hud_itemw.val, hud_itemh.val, hud_iteml.val, hud_itemt.val,
            hud_itemH.val, hud_itema.val);
        project_wrsprite(f, &hud_statb); fprintf(f, " ");
        project_wrsprite(f, &hud_statf); fprintf(f, " ");
        fprintf(f, "%u\r\n", hud_combined);
        project_wrsprite(f, &hud_inv); fprintf(f, " ");
        project_wrsprite(f, &hud_invp); fprintf(f, " ");
        fprintf(f, "%u %u %u %u\r\n", hud_invw.val, hud_invh.val, hud_invl.val, hud_invt.val);
        project_wrfont(f, hud_invfstyle, hud_invfsize.val, hud_invfnt.val); fprintf(f, "\r\n");
        project_wrsprite(f, &hud_invimg); fprintf(f, " ");
        project_wrsprite(f, &hud_invsel); fprintf(f, " ");
        project_wrsprite(f, &hud_invequip); fprintf(f, "\r\n");
        if(pbar)
            for(i = 0; i < hud_pbartbl.num; i++) {
                fprintf(f, "p %u %u %u ", pbar[i].type, pbar[i].left, pbar[i].top);
                project_wridx(f, pbar[i].val, project.attrs); fprintf(f, " ");
                project_wridx(f, pbar[i].max, project.attrs); fprintf(f, " ");
                project_wrsprite2(f, pbar[i].img, hud_pbarimg.cat); fprintf(f, " ");
                project_wrsprite2(f, pbar[i].back, hud_pbarback.cat); fprintf(f, "\r\n");
            }
        if(equip)
            for(i = 0; i < hud_equiptbl.num; i++) {
                fprintf(f, "s %u %u %s\r\n", equip[i].left, equip[i].top, equip[i].name);
            }
        fclose(f);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVEUI]);
}

/**
 * Load hud configuration
 */
void hud_load()
{
    char *str, *s, t;

    str = project_loadfile(project_dirs[PROJDIRS_UI], "hud", "cfg", PROJMAGIC_HUD, "hud_load");
    if(str) {
        hud_pbartbl.num = hud_equiptbl.num = 0;
        if(hud_pbartbl.data) { free(hud_pbartbl.data); hud_pbartbl.data = NULL; }
        if(hud_equiptbl.data) { free(hud_equiptbl.data); hud_equiptbl.data = NULL; }
        s = project_skipnl(str);
        s = project_getsprite(s, &hud_nav);
        s = project_getsprite(s, &hud_btns);
        s = project_skipnl(s);
        s = project_getsprite(s, &hud_itemb);
        s = project_getsprite(s, &hud_itemf);
        s = project_skipnl(s);
        s = project_getint(s, &hud_itemn.val, hud_itemn.min, hud_itemn.max);
        s = project_getint(s, &hud_itemw.val, hud_itemw.min, hud_itemw.max);
        s = project_getint(s, &hud_itemh.val, hud_itemh.min, hud_itemh.max);
        s = project_getint(s, &hud_iteml.val, hud_iteml.min, hud_iteml.max);
        s = project_getint(s, &hud_itemt.val, hud_itemt.min, hud_itemt.max);
        s = project_getint(s, &hud_itemH.val, hud_itemH.min, hud_itemH.max);
        s = project_getint(s, &hud_itema.val, hud_itema.min, hud_itema.max);
        s = project_skipnl(s);
        s = project_getsprite(s, &hud_statb);
        s = project_getsprite(s, &hud_statf);
        s = project_getint(s, &hud_combined, 0, 1);
        s = project_skipnl(s);
        s = project_getsprite(s, &hud_inv);
        s = project_getsprite(s, &hud_invp);
        s = project_getint(s, &hud_invw.val, hud_invw.min, hud_invw.max);
        s = project_getint(s, &hud_invh.val, hud_invh.min, hud_invh.max);
        s = project_getint(s, &hud_invl.val, hud_invl.min, hud_invl.max);
        s = project_getint(s, &hud_invt.val, hud_invt.min, hud_invt.max);
        s = project_skipnl(s);
        s = project_getfont(s, &hud_invfstyle, &hud_invfsize.val, &hud_invfnt.val);
        s = project_skipnl(s);
        s = project_getsprite(s, &hud_invimg);
        s = project_getsprite(s, &hud_invsel);
        s = project_getsprite(s, &hud_invequip);
        while(*s) {
            s = project_skipnl(s);
            if(!*s) break;
            t = *s++;
            switch(t) {
                case 'p':
                    s = project_getint(s, &hud_pbardir.val, 0, 3);
                    s = project_getint(s, &hud_pbarl.val, hud_pbarl.min, hud_pbarl.max);
                    s = project_getint(s, &hud_pbart.val, hud_pbart.min, hud_pbart.max);
                    s = project_getidx(s, &hud_pbarval.val, project.attrs, 0);
                    s = project_getidx(s, &hud_pbarmax.val, project.attrs, 0);
                    s = project_getsprite(s, &hud_pbarimg);
                    s = project_getsprite(s, &hud_pbarback);
                    hud_pbaradd(NULL);
                break;
                case 's':
                    s = project_getint(s, &hud_equipl.val, hud_equipl.min, hud_equipl.max);
                    s = project_getint(s, &hud_equipt.val, hud_equipt.min, hud_equipt.max);
                    s = project_getstr2(s, hud_equipname, 0, sizeof(hud_equipname));
                    hud_equipadd(NULL);
                break;
            }
        }
        hud_pbardir.val = hud_pbarl.val = hud_pbart.val = hud_pbarval.val = hud_pbarmax.val = hud_equipl.val = hud_equipt.val = 0;
        hud_pbarimg.val = hud_pbarback.val = -1; hud_equipname[0] = 0;
        free(str);
    } else
        ui_status(1, lang[ERR_LOADUI]);
}

/**
 * Preview hud
 */
void hud_preview(void *data)
{
    int sintbl[] = { 0,0,0,0,2,6,11,18,27,36,46,50,60,69,78,86,92,97,100,100,100,100,95,90,84,76,67,57,47,37,28,19,12,6,2 };
    hud_pbar_t *pbar = (hud_pbar_t*)hud_pbartbl.data;
    hud_equip_t *equi = (hud_equip_t*)hud_equiptbl.data;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *texture = NULL, **txts = NULL;
    SDL_Rect src, rect, tmp = { 0 }, item = { 0 }, stat = { 0 }, equip = { 0 }, slot = { 0 };
    int wf = SDL_GetWindowFlags(window), i, j, k, x, y, p, top, right, bottom, w, h, btn = 0, nt, scw, sch, slw, slh;
    char *str = "99";
    uint32_t *pixels;
    ssfn_buf_t buf;
    ui_font_t fnt;
    ui_sprite_t *s;
    ui_tablehdr_t hdr[] = {
        { GAME_INV, 0, 0, 0 },
        { GAME_SKILLS, 0, 0, 0 },
        { GAME_QUESTS, 0, 0, 0 },
        { 0 }
    };

    (void)data;
    if(verbose) printf("hud_preview: started\n");

    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
    if(texture) {
        SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
        /* checker */
        for(j = k = 0; j < dm.h; j++, k += p/4)
            for(i = 0; i < dm.w ; i++) {
                x = (i >> 5); y = (j >> 5);
                pixels[k + i] = theme[(y & 1) ^ (x & 1) ? THEME_LIGHT : THEME_DARK];
            }
        SDL_UnlockTexture(texture);
        s = spr_getsel(&hud_itemb, 0);
        stat.x = item.x = (dm.w - s->w) / 2; stat.y = item.y = dm.h - s->h; stat.h = 0;
        ui_blitbuf(texture, item.x, item.y, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
        y = 32;
        if(!hud_combined && hud_statb.val >= 0) {
            s = spr_getsel(&hud_statb, 0); if(y < s->h) y = s->h;
            stat.x = (dm.w - s->w) / 2; stat.y = 0; stat.h = s->h;
            ui_blitbuf(texture, stat.x, stat.y, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
        }
        elements_load();
        elements_loadfonts();
        elements_winsizes(hdr, 0, &w, &top, &right, &bottom, NULL);
        right += 10; scw = elements_vscrsize();
        s = spr_getsel(&hud_invimg, 0); slw = s->w; slh = s->h;
        s = spr_getsel(&hud_invsel, 0); if(slw < s->w) { slw = s->w; } if(slh < s->h) slh = s->h;
        slw += 4; slh += 4;
        i = ((dm.w / 3) - 2 * elements_pad[4].val - scw) / slw;
        i = i * slw + 2 * elements_pad[4].val + scw;
        if(i > w) w = i;
        s = spr_getsel(&hud_invequip, 0);
        if(s->w > w) w = s->w;
        y += top;
        sch = (dm.h * 3 / 4 - y - bottom - s->h - 3 * elements_pad[3].val) / slh;
        h = sch * slh + s->h + 3 * elements_pad[3].val;
        x = dm.w - w - right;
        elements_window(texture, dm.w, dm.h, x, y, w, h, hdr, 0);
        elements_freefonts();
        y += elements_pad[3].val;
        equip.x = dm.w - right - w / 2 - s->w / 2; equip.y = y;
        ui_blitbuf(texture, equip.x, equip.y, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
        y += s->h + elements_pad[3].val;
        elements_vscr(texture, dm.w - right - elements_pad[4].val, y, sch * slh, 0);
        x += elements_pad[4].val;
        slot.x = x + hud_invl.val + 2; slot.w = hud_invw.val;
        slot.y = y + hud_invt.val + 2; slot.h = hud_invh.val;
        w = (w - 2 * elements_pad[4].val) / slw;
        for(k = 0; k < sch; y += slh, k++)
            for(p = 0; p < w; p++) {
                s = spr_getsel(!k && !p ? &hud_invsel : &hud_invimg, 0);
                ui_blitbuf(texture, x + p * slw + (slw - s->w) / 2, y + (slh - s->h) / 2, s->w, s->h, s->data, 0, 0, s->w, s->h,
                    s->w * s->nframe * 4);
            }
        ui_font_load(&fnt, hud_invfstyle, hud_invfsize.val, hud_invfnt.val);
        ssfn_bbox(&fnt.ctx, str, &w, &h, &k, &j);
        SDL_LockTexture(texture, NULL, (void**)&buf.ptr, &p);
        buf.p = p;
        buf.w = slot.x + hud_invw.val;
        buf.h = slot.y + hud_invh.val;
        buf.x = buf.w - w;
        buf.y = buf.h - hud_invfsize.val + j;
        buf.fg = elements_colors[3];
        buf.bg = 0;
        while(*str && ((i = ssfn_render(&fnt.ctx, &buf, str)) > 0 || i == SSFN_ERR_NOGLYPH)) str += (i < 1 ? 1 : i);
        SDL_UnlockTexture(texture);
        ui_font_free(&fnt);
    }
    nt = 5 + (pbar ? hud_pbartbl.num * 2 : 0);
    txts = (SDL_Texture**)main_alloc(nt * sizeof(SDL_Texture*));
    if(hud_nav.val >= 0)
        spr_texture(spr_getidx(hud_nav.cat, hud_nav.val, 0, 0), &txts[0]);
    if(hud_btns.val >= 0)
        spr_texture(spr_getidx(hud_btns.cat, hud_btns.val, 0, 0), &txts[1]);
    spr_texture(spr_getsel(btn ? &hud_invp : &hud_inv, 0), &txts[2]);
    if(hud_itemf.val >= 0)
        spr_texture(spr_getidx(hud_itemf.cat, hud_itemf.val, 0, 0), &txts[3]);
    if(!hud_combined && hud_statf.val >= 0)
        spr_texture(spr_getidx(hud_statf.cat, hud_statf.val, 0, 0), &txts[4]);
    if(pbar)
        for(i = 0; i < hud_pbartbl.num; i++) {
            if(pbar[i].back >= 0)
                spr_texture(spr_getidx(hud_pbarback.cat, pbar[i].back, 0, 1), &txts[5 + 2 * i]);
            if(pbar[i].img >= 0)
                spr_texture(spr_getidx(hud_pbarimg.cat, pbar[i].img, 0, 1), &txts[5 + 2 * i + 1]);
        }

    while(1) {
        /* events */
        SDL_WaitEvent(&event);
        if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
            SDL_PushEvent(&event); goto cleanup;
        }
        switch(event.type) {
            case SDL_KEYUP: goto cleanup; break;
            case SDL_MOUSEBUTTONDOWN:
                if(event.button.button == 1) goto cleanup;
                btn = 1;
            break;
            case SDL_MOUSEBUTTONUP: btn = 0; break;
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        if(texture) SDL_RenderCopy(renderer, texture, NULL, NULL);
        /* navigation */
        rect.x = 0; rect.y = dm.h * 3 / 4; rect.w = rect.h = dm.h / 4;
        if(!txts[0]) {
            src.x = control1_icon.x; src.y = control1_icon.y;
            src.w = control1_icon.w; src.h = control1_icon.h;
            SDL_RenderCopy(renderer, icons, &src, &rect);
        } else {
            spr_render(spr_getsel(&hud_nav, 0), -1, txts[0], &rect);
        }
        if(!btn) {
            src.x = src.w = src.h = rect.w / 3; src.y = rect.y + src.h;
            SDL_RenderCopy(renderer, spr_select, NULL, &src);
        }
        /* buttons */
        rect.x = dm.w - rect.w;
        if(!txts[1]) {
            src.x = control2_icon.x; src.y = control2_icon.y;
            src.w = control2_icon.w; src.h = control2_icon.h;
            SDL_RenderCopy(renderer, icons, &src, &rect);
        } else {
            spr_render(spr_getsel(&hud_btns, 0), -1, txts[1], &rect);
        }
        if(!btn) {
            src.w = src.h = rect.w / 3; src.x = rect.x + src.w; src.y = rect.y;
            SDL_RenderCopy(renderer, spr_select, NULL, &src);
            src.x = rect.x; src.y += src.h;
            SDL_RenderCopy(renderer, spr_select, NULL, &src);
            src.x = rect.x + src.w + src.w;
            SDL_RenderCopy(renderer, spr_select, NULL, &src);
            src.x = rect.x + src.w; src.y += src.h;
            SDL_RenderCopy(renderer, spr_select, NULL, &src);
        }
        /* iventory icon */
        if(txts[2]) {
            s = spr_getsel(btn ? &hud_invp : &hud_inv, 0);
            rect.x = dm.w - 32; rect.y = 0; rect.w = rect.h = 32;
            spr_render(spr_getsel(btn ? &hud_invp : &hud_inv, 0), -1, txts[2], &rect);
        }
        if(!btn) {
            /* inventory slot */
            SDL_RenderCopy(renderer, spr_select, NULL, &slot);
            /* equipment slots */
            if(equi)
                for(i = 0; i < hud_equiptbl.num; i++) {
                    rect.x = equip.x + equi[i].left; rect.y = equip.y + equi[i].top;
                    rect.w = hud_invw.val; rect.h = hud_invh.val;
                    SDL_RenderCopy(renderer, spr_select, NULL, &rect);
                }
        }
        /* itembar */
        if(!btn) {
            rect.x = item.x + hud_iteml.val; rect.y = item.y + hud_itemt.val;
            rect.w = hud_itemw.val; rect.h = hud_itemh.val;
            for(i = 0; i < hud_itemn.val; i++, rect.x += hud_itemw.val + hud_itemH.val) {
                SDL_RenderCopy(renderer, spr_select, NULL, &rect);
            }
            rect.x += hud_itema.val;
            SDL_RenderCopy(renderer, spr_select, NULL, &rect);
        }
        /* progressbars */
        if(pbar) {
            for(i = w = 0; i < hud_pbartbl.num; i++)
                if(pbar[i].img >= 0 && pbar[i].type != 3 && !pbar[i].top && !pbar[i].left) {
                    s = spr_getidx(hud_pbarimg.cat, pbar[i].img, 0, 1);
                    w += s->w;
                }
            w = (dm.w - w) / 2;
            for(i = 0; i < hud_pbartbl.num; i++) {
                rect.x = stat.x + pbar[i].left; rect.y = stat.y + pbar[i].top;
                if(pbar[i].img >= 0 && pbar[i].type != 3 && !pbar[i].top && !pbar[i].left) {
                    s = spr_getidx(hud_pbarimg.cat, pbar[i].img, 0, 1);
                    rect.y = stat.h; rect.x = w; w += s->w;
                }
                if(pbar[i].back >= 0 && txts[5 + i * 2]) {
                    s = spr_getidx(hud_pbarback.cat, pbar[i].back, 0, 1);
                    src.x = src.y = 0; src.w = rect.w = s->w; src.h = rect.h = s->h;
                    if(pbar[i].type == 3) { rect.x = dm.w / 3; rect.y = dm.h / 2 - 8; }
                    SDL_RenderCopy(renderer, txts[5 + i * 2], &src, &rect);
                }
                if(pbar[i].img >= 0 && txts[5 + i * 2 + 1]) {
                    s = spr_getidx(hud_pbarimg.cat, pbar[i].img, 0, 1);
                    src.x = src.y = 0; src.w = s->w; src.h = s->h;
                    k = sintbl[ui_anim_cnt % (int)(sizeof(sintbl)/sizeof(sintbl[0]))];
                    switch(pbar[i].type) {
                        case 0: src.w = rect.w = src.w * k / 100; break;
                        case 1: src.y = src.h - (src.h * k / 100); src.h -= src.y; rect.y += src.y; break;
                        case 2: SDL_SetTextureAlphaMod(txts[5 + i * 2 + 1], 255 * k / 100); break;
                        case 3:
                            tmp.x = charwalk_icon.x + ui_anim(2, 8, -1) * 64; tmp.y = charwalk_icon.y;
                            tmp.w = tmp.h = rect.w = rect.h = 64;
                            rect.x = dm.w / 3 + s->w / 2 - 32; rect.y = dm.h / 2 + s->h;
                            SDL_RenderCopy(renderer, icons, &tmp, &rect);
                            rect.y -= s->h + 8; rect.x = dm.w / 3; src.w = rect.w = src.w * k / 100;
                        break;
                    }
                    rect.w = src.w; rect.h = src.h;
                    SDL_RenderCopy(renderer, txts[5 + i * 2 + 1], &src, &rect);
                    if(pbar[i].type == 2) SDL_SetTextureAlphaMod(txts[5 + i * 2 + 1], 255);
                }
            }
        }
        /* optional front covers */
        if(!btn) {
            if(txts[3]) {
                s = spr_getsel(&hud_itemf, 0);
                rect.w = s->w; rect.h = s->h;
                rect.x = item.x; rect.y = item.y;
                spr_render(s, -1, txts[3], &rect);
            }
            if(txts[4]) {
                s = spr_getsel(&hud_statf, 0);
                rect.w = s->w; rect.h = s->h;
                rect.x = stat.x; rect.y = stat.y;
                spr_render(s, -1, txts[4], &rect);
            }
        }
        SDL_RenderPresent(renderer);
    }

cleanup:
    if(txts) {
        for(i = 0; i < nt; i++)
            if(txts[i]) SDL_DestroyTexture(txts[i]);
        free(txts);
    }
    if(texture) SDL_DestroyTexture(texture);
    if(verbose) printf("hud_preview: stopped\n");
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Get references
 */
void hud_ref(tngctx_t *ctx)
{
    hud_pbar_t *pbar;
    int i;

    if(!ctx) return;
    hud_load();
    pbar = (hud_pbar_t*)hud_pbartbl.data;
    if(pbar)
        for(i = 0; i < hud_pbartbl.num; i++) {
            attrs_ref(ctx, pbar[i].val);
            attrs_ref(ctx, pbar[i].max);
        }
}

/**
 * Save tng
 */
int hud_totng(tngctx_t *ctx)
{
    hud_pbar_t *pbar;
    hud_equip_t *equip;
    int i;

    if(!ctx) return 1;
    hud_load();
    pbar = (hud_pbar_t*)hud_pbartbl.data;
    equip = (hud_equip_t*)hud_equiptbl.data;
    tng_section(ctx, TNG_SECTION_HUD);
    tng_sprite(ctx, hud_nav.cat, hud_nav.val);
    tng_sprite(ctx, hud_btns.cat, hud_btns.val);
    tng_sprite(ctx, hud_itemb.cat, hud_itemb.val);
    tng_sprite(ctx, hud_itemf.cat, hud_itemf.val);
    tng_data(ctx, &hud_itemn.val, 1);
    tng_data(ctx, &hud_itemw.val, 1);
    tng_data(ctx, &hud_itemh.val, 1);
    tng_data(ctx, &hud_iteml.val, 1);
    tng_data(ctx, &hud_itemt.val, 1);
    tng_data(ctx, &hud_itemH.val, 1);
    tng_data(ctx, &hud_itema.val, 1);
    tng_data(ctx, &hud_combined, 1);
    tng_sprite(ctx, hud_statb.cat, hud_combined ? -1 : hud_statb.val);
    tng_sprite(ctx, hud_statf.cat, hud_combined ? -1 : hud_statf.val);
    tng_sprite(ctx, hud_inv.cat, hud_inv.val);
    tng_sprite(ctx, hud_invp.cat, hud_invp.val);
    tng_data(ctx, &hud_invw.val, 1);
    tng_data(ctx, &hud_invh.val, 1);
    tng_data(ctx, &hud_invl.val, 1);
    tng_data(ctx, &hud_invt.val, 1);
    tng_font(ctx, hud_invfstyle, hud_invfsize.val, hud_invfnt.val);
    tng_sprite(ctx, hud_invimg.cat, hud_invimg.val);
    tng_sprite(ctx, hud_invsel.cat, hud_invsel.val);
    tng_sprite(ctx, hud_invequip.cat, hud_invequip.val);
    tng_data(ctx, &hud_pbartbl.num, 1);
    if(pbar)
        for(i = 0; i < hud_pbartbl.num; i++) {
            tng_data(ctx, &pbar[i].type, 1);
            tng_data(ctx, &pbar[i].left, 2);
            tng_data(ctx, &pbar[i].top, 2);
            tng_idx(ctx, project.attrs, pbar[i].val);
            tng_idx(ctx, project.attrs, pbar[i].max);
            tng_sprite(ctx, hud_pbarimg.cat, pbar[i].img);
            tng_sprite(ctx, hud_pbarback.cat, pbar[i].back);
        }
    if(equip) {
        tng_section(ctx, TNG_SECTION_EQUIPSLOTS);
        for(i = 0; i < hud_equiptbl.num; i++) {
            tng_data(ctx, &equip[i].left, 2);
            tng_data(ctx, &equip[i].top, 2);
        }
    }
    return 1;
}

/**
 * Read from tng
 */
int hud_fromtng(tngctx_t *ctx)
{
    FILE *f;
    uint8_t *buf, *end;
    int i, l, len, e = -1;

    if(!ctx || !ctx->tbl || !ctx->numtbl) return 0;
    for(e = 0; e < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[e]) != TNG_SECTION_EQUIPSLOTS; e++);
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_HUD; i++);
    if(i >= ctx->numtbl) return 1;
    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
    buf = (uint8_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]);
    end = buf + len;
    if(len < 51) return 0;
    f = project_savefile(0, project_dirs[PROJDIRS_UI], "hud", "cfg", PROJMAGIC_HUD, "hud_fromtng");
    if(f) {
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, " ");
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, "\r\n");
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, " ");
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, "\r\n");
        fprintf(f, "%u %u %u %u %u %u %u\r\n", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6]); buf += 7;
        l = *buf++;
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, " ");
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, " ");
        fprintf(f, "%u\r\n", l);
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, " ");
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, " ");
        fprintf(f, "%u %u %u %u\r\n", buf[0], buf[1], buf[2], buf[3]); buf += 4;
        buf = tng_wrfont(ctx, buf, f); fprintf(f, "\r\n");
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, " ");
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, " ");
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, "\r\n");
        l = *buf++;
        for(i = 0; i < l && buf < end; i++) {
            fprintf(f, "p %u %u %u ", buf[0], *((uint16_t*)&buf[1]), *((uint16_t*)&buf[3])); buf += 5;
            buf = tng_wridx(ctx, buf, f); fprintf(f, " ");
            buf = tng_wridx(ctx, buf, f); fprintf(f, " ");
            buf = tng_wrsprite(ctx, buf, f); fprintf(f, " ");
            buf = tng_wrsprite(ctx, buf, f); fprintf(f, "\r\n");
        }
        if(e != -1) {
            buf = (uint8_t*)(ctx->sec + ctx->tbl[e].offs);
            len = TNG_SECTION_SIZE(&ctx->tbl[e]);
            end = buf + len;
            for(i = 0; buf < end; i++, buf += 4)
                if(!i)
                    fprintf(f, "s %u %u primary\r\n", *((uint16_t*)buf), *((uint16_t*)&buf[2]));
                else
                    fprintf(f, "s %u %u equipslot%02u\r\n", *((uint16_t*)buf), *((uint16_t*)&buf[2]), i);
        }
        fclose(f);
        return 1;
    }
    return 0;
}
