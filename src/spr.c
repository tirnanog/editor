/*
 * tnge/spr.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Sprite manager
 *
 */

#define STBI_NO_LINEAR
#define STBI_NO_HDR
#define STBI_NO_FAILURE_STRINGS
#include "stb_image.h"
#include "main.h"

extern int stbi__need_comment, chars_err;
extern char *stbi__comment;

typedef struct {
    int x, y, w, h;
    char *name;
    uint8_t *data;
} spr_pack_t;

typedef struct {
    int a, x, y, w, h;
} spr_atlas_slot_t;

typedef struct {
    int cat, idx, orig, dir, frm;
    int a, x, y, w, h, l, t, p, o;
    uint32_t *data;
} spr_atlas_spr_t;

typedef struct {
    uint64_t sp;            /* square product */
    int mw, mh, numimg;
    uint32_t **imgs;
    int numslot;
    spr_atlas_slot_t *slots;
    int numspr;
    spr_atlas_spr_t *spr;
    int len, nd;
    uint8_t *asset;
} spr_atlas_t;

/**
 * Sort sprites
 */
int strnatcmp(const void *a, const void *b);
int spr_strcmp(const void *a, const void *b) {
    return strcmp(*((char**)a), *((char**)b));
}
int spr_cmp(const void *a, const void *b)
{
    const ui_sprlist_t *A = (const ui_sprlist_t*)a;
    const ui_sprlist_t *B = (const ui_sprlist_t*)b;
    return strnatcmp(&A->name, &B->name);
}
void spr_sort(int cat)
{
    if(!project.sprites[cat] || project.spritenum[cat] < 2) return;
    qsort(project.sprites[cat], project.spritenum[cat], sizeof(ui_sprlist_t), spr_cmp);
}

/**
 * Load localized names
 */
void spr_loadlocnames(char *fn, char *ext, char ***locnames, int *numlocnames)
{
    FILE *f;
    long int len;
    int i, l;
    char *str, *s, *e;

    if(!fn || !*fn || !ext || !locnames || !numlocnames) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s%s", project.id, project_dirs[PROJDIRS_LANG], fn, ext);
    f = project_fopen(projdir, "rb");
    if(!f) return;
    fseek(f, 0, SEEK_END);
    len = (long int)ftell(f);
    fseek(f, 0, SEEK_SET);
    str = s = (char*)main_alloc(len + 1);
    len = fread(str, len, 1, f);
    fclose(f);
    while(*s) {
        while(*s == '\r' || *s == '\n') s++;
        for(e = s, l = 0; *e && *e != '\r' && *e != '\n'; e++)
            if(*e == '=') l = e - s;
        if(l && s + l + 1 < e) {
            for(i = 0; i < *numlocnames && (memcmp((*locnames)[i], s, l) || (*locnames)[i][l]); i++);
            if(i >= *numlocnames) {
                i = (*numlocnames)++;
                *locnames = (char**)realloc(*locnames, *numlocnames * sizeof(char*));
                if(!*locnames) main_error(ERR_MEM);
                (*locnames)[i] = (char*)main_alloc(e - s + 1);
                memcpy((*locnames)[i], s, e - s);
                (*locnames)[i][l] = 0;
            } else
            if(verbose > 1)
                printf("spr_loadlocnames: duplicate translation '%s'\n", (*locnames)[i]);
        }
        s = e;
    }
    free(str);
}

/**
 * Find a localized version of a name
 */
char *spr_findlocname(char *name, char **locnames, int numlocnames, int *jmptbl)
{
    char *ret = NULL, *s, *n;
    int i, j = numlocnames-1, k, l, m = 0, c = -1;

    if(!name || !*name || !locnames || numlocnames < 1 || !jmptbl) return NULL;
    k = jmptbl[(int)name[0]]; j = jmptbl[(int)name[0] + 1];
    if(k >= numlocnames) return NULL;
    while(k < j) {
        i = k + ((j - k) >> 1);
        c = strcmp(locnames[i], name);
        if(!c || k >= j) break;
        if(c < 0) k = i + 1; else j = i;
    }
    if(!c && i < numlocnames) {
        /* exact match */
        j = strlen(locnames[i]) + 1; l = strlen(locnames[i] + j);
        ret = (char*)main_alloc(l + 1);
        strcpy(ret, locnames[i] + j);
    } else {
        /* do series of partial matches */
        for(s = name; *s;) {
            /* can't do binary search here, we need the longest match */
            for(n = NULL, i = jmptbl[(int)*s], k = 0; i < jmptbl[(int)*s + 1] && *s == locnames[i][0]; i++) {
                j = strlen(locnames[i]);
                if(!memcmp(s, locnames[i], j) && (s[j] == '_' || !s[j]) && k < j) {
                    l = strlen(locnames[i] + j + 1);
                    n = locnames[i] + j + 1; k = j;
                }
            }
            if(!n) {
                for(n = s; *s && *s != '_'; s++);
                if(n == s) break;
                l = s - n;
            } else
                s += k;
            ret = (char*)realloc(ret, m + l + 2);
            if(!ret) main_error(ERR_MEM);
            memcpy(ret + m, n, l);
            ret[m + l] = *s; m += l + 1;
            if(*s == '_') s++;
        }
        if(ret && !strcmp(ret, name)) { free(ret); ret = NULL; }
    }
    return ret;
}

/**
 * Load all sprites
 */
void spr_init()
{
    ui_sprlist_t *spr;
    char **list, type, dir, nframe, *str, *name, **locnames = NULL, tmp[16];
    int i, j, k, l, x, y, W, H, w, h, num, len, cat, ns, tot = 0, numlocnames = 0, jmptbl[257];
    uint8_t *img;
    uint32_t *src, *dst;
    uint64_t curr, last;

    for(project_curr = project_total = 0, i = 0; i < PROJ_NUMSPRCATS - 2; i++) {
        sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_SPRITES + i]);
        project_total += project_getsizes(projdir, ".png");
    }
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_NPCS]);
    project_total += project_getsizes(projdir, "_npc.png");
    project_total += project_getsizes(projdir, "_swr.png");

    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_LANG]);
    sprintf(tmp, "_%c%c.txt", lang[-1][0], lang[-1][1]);
    list = project_getdir(projdir, tmp, NULL);
    for(i = 0; list && list[i]; i++)
        spr_loadlocnames(list[i], tmp, &locnames, &numlocnames);
    project_freedir(&list, NULL);
    if(locnames) {
        qsort(locnames, numlocnames, sizeof(char*), spr_strcmp);
        for(i = 0; i < 257; i++) jmptbl[i] = numlocnames;
        for(i = 0; i < numlocnames; i++)
            if(jmptbl[(int)locnames[i][0]] == numlocnames) jmptbl[(int)locnames[i][0]] = i;
    }

    spr_free();
    for(cat = 0; cat < PROJ_NUMSPRCATS; cat++) {
        ns = 0;
        if(cat < PROJ_NUMSPRCATS - 2) {
            sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_SPRITES + cat]);
            list = project_getdir(projdir, ".png", &num);
        } else
        if(cat == PROJ_NUMSPRCATS - 2) {
            sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_NPCS]);
            list = project_getdir(projdir, "_npc.png", &num);
        } else {
            sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_NPCS]);
            list = project_getdir(projdir, "_swr.png", &num);
        }
        if(list && num > 0) {
            len = num; spr = main_alloc(len * sizeof(ui_sprlist_t));
            for(i = j = 0; i < num; i++) {
                l = strlen(list[i]) - (cat < PROJ_NUMSPRCATS - 2 ? 5 : 0);
                if(l < 1) continue;
                img = NULL;
                sprintf(projfn, "%s" SEP "%s" SEP "%s%s.png", project.id,
                    project_dirs[cat < PROJ_NUMSPRCATS - 2 ? PROJDIRS_SPRITES + cat : PROJDIRS_NPCS], list[i],
                    cat < PROJ_NUMSPRCATS - 2 ? "" : (cat == PROJ_NUMSPRCATS - 2 ? "_npc" : "_swr"));
                if(cat < PROJ_NUMSPRCATS - 2) {
                    spr_getname(list[i], &dir, &type, &nframe);
                } else {
                    dir = type = 0; nframe = 1;
                }
                stbi__need_comment = 1;
                if(nframe && (img = image_load(projdir, &w, &h)) && w > 0 && h > 0) {
                    if(verbose > 1) printf("spr_init: loading %s\r\n", projdir);
                    if(nframe == -1) {
                        if(!stbi__comment || memcmp(stbi__comment, PROJMAGIC_ATLAS, 14)) {
                            if(verbose)
                                printf("spr_init: missing atlas meta info %s\n", projdir);
                        } else {
                            for(str = stbi__comment, k = 0; *str; str++)
                                if(*str == '\n') k++;
                            if(k > 1) {
                                spr = (ui_sprlist_t*)realloc(spr, (len + k) * sizeof(ui_sprlist_t));
                                if(!spr) main_error(ERR_MEM);
                                memset(&spr[len], 0, k * sizeof(ui_sprlist_t));
                                len += k;
                                list[i][l] = 0;
                                str = project_skipnl(stbi__comment);
                                curr = project_curr;
                                project_curr -= project_last;
                                last = project_last / (k - 1);
                                while(*str) {
                                    str = spr_getmeta(list[i], str, &x, &y, &W, &H, &dir, &type, &nframe, &name);
                                    if(!name || nframe < 1) continue;
                                    if(j && !strcmp(name, spr[j - 1].name)) { j--; free(name); }
                                    else {
                                        spr[j].name = name;
                                        if(!spr[j].loc) spr[j].loc = spr_findlocname(name, locnames, numlocnames, jmptbl);
                                    }
                                    spr[j].x = x; spr[j].y = y & 0xFFFF;
                                    spr[j].dir[(int)dir].type = type;
                                    spr[j].dir[(int)dir].nframe = nframe;
                                    spr[j].dir[(int)dir].w = W / (int)nframe;
                                    spr[j].dir[(int)dir].h = H;
                                    spr[j].dir[(int)dir].data = dst = (uint32_t*)main_alloc(W * H * 4);
                                    src = (uint32_t*)(img + (w * y + x) * 4);
                                    for(k = 0; k < H; k++, src += w, dst += W)
                                        memcpy(dst, src, W * 4);
                                    j++; tot++;
                                    project_curr += last;
                                    if(project_total) ui_progressbar(0, 0, project_curr, project_total, LANG_LOADING);
                                }
                                list[i][l] = '_';
                                ns++;
                                project_curr = curr;
                            }
                        }
                        free(img);
                    } else {
                        if(j && !memcmp(list[i], spr[j - 1].name, l) && !spr[j - 1].name[l]) {
                            j--;
                            if(spr[j].dir[(int)dir].data) {
                                if(verbose > 1)
                                    printf("spr_init: sprite name clash '%s' dir %d loading %s\n", spr[j].name, dir, projfn);
                                free(spr[j].dir[(int)dir].data);
                            }
                        } else {
                            spr[j].name = (char*)main_alloc(l + 1);
                            memcpy(spr[j].name, list[i], l);
                            spr[j].name[l] = 0;
                            if(!spr[j].loc) spr[j].loc = spr_findlocname(spr[j].name, locnames, numlocnames, jmptbl);
                        }
                        spr[j].x = -1;
                        spr[j].dir[(int)dir].type = type;
                        spr[j].dir[(int)dir].nframe = nframe;
                        spr[j].dir[(int)dir].w = w / (int)nframe;
                        spr[j].dir[(int)dir].h = h;
                        spr[j].dir[(int)dir].data = (uint32_t*)img;
                        j++; tot++;
                    }
                    if(project_total) ui_progressbar(0, 0, project_curr, project_total, LANG_LOADING);
                } else {
                    if(img) free(img);
                    else if(verbose)
                        printf("spr_init: load error %s\n", projdir);
                }
                if(stbi__comment) { free(stbi__comment); stbi__comment = NULL; }
                stbi__need_comment = 0;
            }
            project.spritenum[cat] = j;
            if(j)
                project.sprites[cat] = (ui_sprlist_t*)realloc(spr, j * sizeof(ui_sprlist_t));
            else
                free(spr);
        }
        project_freedir(&list, NULL);
        if(ns) spr_sort(cat);
    }
    if(verbose) {
        printf("spr_init:");
        for(i = l = 0, curr = 0; i < PROJ_NUMSPRCATS - 2; i++) {
            printf("%s %s %u", i ? "," : "", project_dirs[PROJDIRS_SPRITES + i], project.spritenum[i]);
            curr += project.spritenum[i] * sizeof(ui_sprlist_t);
            for(j = 0; j < project.spritenum[i]; j++) {
                if(project.sprites[i][j].name) curr += strlen(project.sprites[i][j].name);
                if(project.sprites[i][j].loc) curr += strlen(project.sprites[i][j].loc);
                for(k = 0; k < 8; k++)
                    if(project.sprites[i][j].dir[k].data) {
                        l += project.sprites[i][j].dir[k].nframe;
                        curr += project.sprites[i][j].dir[k].nframe * project.sprites[i][j].dir[k].w *
                            project.sprites[i][j].dir[k].h * 4;
                    }
            }
        }
        tmp[0] = tmp[1] = 0;
        if(curr > 1024 * 1024) { tmp[0] = 'M'; curr = (curr + 1024 * 1024 - 1) / 1024 / 1024; } else
        if(curr > 1024) { tmp[0] = 'K'; curr = (curr + 1023) / 1024; }
        printf(", %s %u + %u\nspr_init: total %u sprites, %u frames, mem %lu %sbytes\n", project_dirs[PROJDIRS_NPCS],
            project.spritenum[i], project.spritenum[i + 1], tot, l, (unsigned long int)curr, tmp);
    }
    project_freedir(&locnames, &numlocnames);
    project_curr = project_total = 0;
    ui_progressbar(0, 0, 0, 0, LANG_LOADING);
}

/**
 * Free all sprites
 */
void spr_free()
{
    int i, j, cat;

    for(cat = 0; cat < PROJ_NUMSPRCATS; cat++) {
        if(project.sprites[cat]) {
            for(i = 0; i < project.spritenum[cat]; i++) {
                if(project.sprites[cat][i].name) free(project.sprites[cat][i].name);
                if(project.sprites[cat][i].loc && project.sprites[cat][i].loc != project.sprites[cat][i].name)
                    free(project.sprites[cat][i].loc);
                if(project.sprites[cat][i].txt) SDL_DestroyTexture(project.sprites[cat][i].txt);
                for(j = 0; j < 8; j++)
                    if(project.sprites[cat][i].dir[j].data)
                        free(project.sprites[cat][i].dir[j].data);
            }
            free(project.sprites[cat]);
        }
        project.spritenum[cat] = 0;
        project.sprites[cat] = NULL;
    }
}

/**
 * Free all temporary sprites
 */
void spr_cleanup()
{
    int i, j, cat, freed;

    for(cat = 0; cat < PROJ_NUMSPRCATS; cat++) {
        if(project.sprites[cat]) {
            for(i = freed = 0; i < project.spritenum[cat]; i++)
                if(!project.sprites[cat][i].name) {
                    freed++;
                    if(project.sprites[cat][i].txt) SDL_DestroyTexture(project.sprites[cat][i].txt);
                    for(j = 0; j < 8; j++)
                        if(project.sprites[cat][i].dir[j].data)
                            free(project.sprites[cat][i].dir[j].data);
                }
            project.spritenum[cat] -= freed;
        }
    }
}

/**
 * Free sprite cache
 */
void spr_freecache(int cat, int idx)
{
    if(idx < 0 || idx >= project.spritenum[cat] || !project.sprites[cat]) return;
    if(project.sprites[cat][idx].txt) {
        SDL_DestroyTexture(project.sprites[cat][idx].txt);
        project.sprites[cat][idx].txt = NULL;
    }
}

/**
 * Flush sprite cache
 */
void spr_flush()
{
    int i, j;

    for(j = 0; j < PROJ_NUMSPRCATS; j++)
        for(i = 0; i < project.spritenum[j]; i++)
            spr_freecache(j, i);
}

/**
 * Reset all sprite index
 */
void spr_reset(int idx)
{
    int i, j;

    for(j = 0; j < PROJ_NUMSPRCATS; j++) {
        for(i = 0; project.sprites[j] && i < project.spritenum[j]; i++)
            project.sprites[j][i].idx = idx;
    }
}

/**
 * Move sprite from one category to another
 */
void spr_move(int idx, int oldcat, int newcat)
{
    ui_sprlist_t tmp;

    if(idx < 0 || !project.sprites[oldcat]) return;
    memcpy(&tmp, &project.sprites[oldcat][idx], sizeof(ui_sprlist_t));
    if(project.spritenum[oldcat] == 1) {
        free(project.sprites[oldcat]);
        project.sprites[oldcat] = NULL;
        project.spritenum[oldcat] = 0;
    } else {
        memcpy(&project.sprites[oldcat][idx], &project.sprites[oldcat][idx + 1], (project.spritenum[oldcat] - idx - 1) *
            sizeof(ui_sprlist_t));
        project.spritenum[oldcat]--;
    }
    idx = project.spritenum[newcat]++;
    project.sprites[newcat] = (ui_sprlist_t*)realloc(project.sprites[newcat], project.spritenum[newcat] * sizeof(ui_sprlist_t));
    if(!project.sprites[newcat]) main_error(ERR_MEM);
    memcpy(&project.sprites[newcat][idx], &tmp, sizeof(ui_sprlist_t));
    spr_sort(newcat);
}

/**
 * Copy sprite
 */
void spr_copy(int cat, int idx, char *newname)
{
    ui_sprite_t *s, *d;
    int i, j, l;

    if(idx < 0 || !project.sprites[cat] || !newname || !*newname) return;
    i = project.spritenum[cat]++;
    project.sprites[cat] = (ui_sprlist_t*)realloc(project.sprites[cat], project.spritenum[cat] * sizeof(ui_sprlist_t));
    if(!project.sprites[cat]) main_error(ERR_MEM);
    memcpy(&project.sprites[cat][i], &project.sprites[cat][idx], sizeof(ui_sprlist_t));
    project.sprites[cat][i].name = (char*)main_alloc(strlen(newname) + 1);
    strcpy(project.sprites[cat][i].name, newname);
    project.sprites[cat][i].loc = NULL;
    project.sprites[cat][i].x = -1;
    for(j = 0; j < 8; j++) {
        s = &project.sprites[cat][idx].dir[j];
        d = &project.sprites[cat][i].dir[j];
        if(s->data) {
            l = s->w * s->nframe * s->h * sizeof(uint32_t);
            d->data = (uint32_t*)malloc(l);
            if(d->data)
                memcpy(d->data, s->data, l);
            else
                d->w = d->h = 0;
        }
    }
    spr_sort(cat);
}

/**
 * Add a sprite to list
 */
void spr_add(int cat, char *name, int dir, int type, int nframe, int w, int h, int p, void *pixels)
{
    int i = project.spritenum[cat], j, p2, p3;

    if(!name || !pixels || w < 1 || h < 1 || p < 4 || nframe * w * 4 > p) return;
    dir &= 7;
    type &= 0x7F;
    if(nframe < 1) nframe = 1;
    if(nframe > 32) nframe = 32;
    if(project.sprites[cat])
        for(i = 0; i < project.spritenum[cat] && strcmp(project.sprites[cat][i].name, name); i++);
    if(i >= project.spritenum[cat]) {
        i = project.spritenum[cat]++;
        project.sprites[cat] = (ui_sprlist_t*)realloc(project.sprites[cat], project.spritenum[cat] * sizeof(ui_sprlist_t));
        if(!project.sprites[cat]) main_error(ERR_MEM);
        memset(&project.sprites[cat][i], 0, sizeof(ui_sprlist_t));
        project.sprites[cat][i].name = (char*)main_alloc(strlen(name) + 1);
        strcpy(project.sprites[cat][i].name, name);
    } else {
        if(project.sprites[cat][i].dir[dir].data)
            free(project.sprites[cat][i].dir[dir].data);
    }
    project.sprites[cat][i].x = -1;
    project.sprites[cat][i].dir[dir].type = type;
    project.sprites[cat][i].dir[dir].nframe = nframe;
    project.sprites[cat][i].dir[dir].data = (uint32_t*)malloc(w * nframe * h * 4);
    if(project.sprites[cat][i].dir[dir].data) {
        for(j = p2 = p3 = 0; j < h; j++, p2 += nframe * w * 4, p3 += p)
            memcpy((uint8_t*)project.sprites[cat][i].dir[dir].data + p2, (uint8_t*)pixels + p3, nframe * w * 4);
    } else
        w = h = 0;
    project.sprites[cat][i].dir[dir].w = w;
    project.sprites[cat][i].dir[dir].h = h;
    spr_sort(cat);
}

/**
 * Remove from sprite list
 */
void spr_del(int cat, int idx)
{
    int i;

    if(idx < 0 || !project.sprites[cat]) return;
    if(project.sprites[cat][idx].name) free(project.sprites[cat][idx].name);
    for(i = 0; i < 8; i++)
        if(project.sprites[cat][idx].dir[i].data)
            free(project.sprites[cat][idx].dir[i].data);
    if(project.spritenum[cat] == 1) {
        free(project.sprites[cat]);
        project.sprites[cat] = NULL;
        project.spritenum[cat] = 0;
    } else {
        memcpy(&project.sprites[cat][idx], &project.sprites[cat][idx + 1], (project.spritenum[cat] - idx - 1) *
            sizeof(ui_sprlist_t));
        project.spritenum[cat]--;
    }
}

/**
 * Parse atlas meta info for sprite attributes
 */
char *spr_getmeta(char *prefix, char *line, int *x, int *y, int *w, int *h, char *dir, char *type, char *nframe, char **name)
{
    char *e, *fn, *n;
    int i;

    if(!line || !*line || !x || !y || !w || !h || !dir || !type || !nframe || !name) return line;
    while(*line == ' ') line++;
    for(e = line; *e && *e != '\r' && *e != '\n'; e++);
    fn = e - 4; *x = *y = *w = *h = 0; *dir = *type = *nframe = 0; *name = NULL;
    while(*e && (*e == '\r' || *e == '\n')) e++;
    if(!memcmp(fn, "atls", 4) || !memcmp(fn, "full", 4)) return e;
    for(i = 0; project_directions[i]; i++)
        if(fn[0] == project_directions[i][0] && fn[1] == project_directions[i][1]) { *dir = i; break; }
    for(i = 0; project_animtypes[i]; i++)
        if(fn[2] == project_animtypes[i]) { *type = i; break; }
    for(i = 0; project_animframes[i]; i++)
        if(fn[3] == project_animframes[i]) { *nframe = i + 1; break; }
    *x = atoi(line); while(line < e && *line != ' ') { line++; } while(*line == ' ') line++;
    *y = atoi(line); while(line < e && *line != ' ') { line++; } while(*line == ' ') line++;
    *w = atoi(line); while(line < e && *line != ' ') { line++; } while(*line == ' ') line++;
    *h = atoi(line); while(line < e && *line != ' ') { line++; } while(*line == ' ') line++;
    i = (prefix && *prefix ? strlen(prefix) : 0) + 1;
    *name = n = (char*)main_alloc(i + (fn - line));
    if(i) { n += sprintf(n, "%s", prefix); if(fn > line) *n++ = '_'; }
    if(fn > line) memcpy(n, line, fn - line - 1);
    return e;
}

/**
 * Parse filename for sprite attributes
 */
void spr_getname(char *fn, char *dir, char *type, char *nframe)
{
    int i;

    if(!fn || !*fn || !dir || !type || !nframe) return;
    fn += strlen(fn) - 4; *dir = *type = *nframe = 0;
    if(!strcmp(fn, "atls")) { *nframe = -1; return; }
    if(!strcmp(fn, "full")) { *type = 0x7F; *nframe = 1; return; }
    for(i = 0; project_directions[i]; i++)
        if(fn[0] == project_directions[i][0] && fn[1] == project_directions[i][1]) { *dir = i; break; }
    for(i = 0; project_animtypes[i]; i++)
        if(fn[2] == project_animtypes[i]) { *type = i; break; }
    for(i = 0; project_animframes[i]; i++)
        if(fn[3] == project_animframes[i]) { *nframe = i + 1; break; }
}

/**
 * Write sprite attributes into a filename string
 */
void spr_wrname(int cat, int val, int dir, char *d)
{
    if(!d || !project.sprites[cat]) return;
    if(cat == PROJ_NUMSPRCATS - 2) strcpy(d, "npc"); else
    if(cat == PROJ_NUMSPRCATS - 1) strcpy(d, "swr"); else
    if(project.sprites[cat][val].dir[dir].type == 0x7F) strcpy(d, "full"); else {
        strcpy(d, project_directions[dir]);
        d[2] = project_animtypes[(int)project.sprites[cat][val].dir[dir].type];
        d[3] = project.sprites[cat][val].dir[dir].nframe > 31 ? 'V' :
            project_animframes[project.sprites[cat][val].dir[dir].nframe - 1];
    }
}

/**
 * Return a sprite for selector
 */
ui_sprite_t *spr_getsel(ui_sprsel_t *sel, int dir)
{
    if(!sel || sel->val < 0) return &ui_spr_none;
    return spr_getidx(sel->cat, sel->val, dir, 1);
}

/**
 * Return a sprite for index
 */
ui_sprite_t *spr_getidx(int cat, int idx, int dir, int none)
{
    if(idx < 0 || idx >= project.spritenum[cat] || !project.sprites[cat]) return (none ? &ui_spr_none : NULL);
    if(dir >= 0) return project.sprites[cat][idx].dir[dir].data ? &project.sprites[cat][idx].dir[dir] : NULL;
    for(dir = 0; dir < 8; dir++)
        if(project.sprites[cat][idx].dir[dir].data) return &project.sprites[cat][idx].dir[dir];
    return NULL;
}

/**
 * Blit a sprsel's first frame in the given direction to a texture
 */
void spr_blitsel(ui_sprsel_t *sel, int dir, int a, SDL_Texture *dst, SDL_Rect *rect)
{
    SDL_Rect rect2;
    ui_sprite_t *s;

    if(!sel || !dst || !rect) return;
    s = spr_getidx(sel->cat, sel->val, dir, 0);
    if(s) {
        ui_fit(rect->w, rect->h, s->w, s->h, &rect2.w, &rect2.h);
        rect2.x = rect->x + (rect->w - rect2.w) / 2; rect2.y = rect->y + rect->h - rect2.h;
        spr_blit(s, a, dst, &rect2);
    }
}

/**
 * Blit the first frame of a sprite to a texture
 */
void spr_blit(ui_sprite_t *s, int a, SDL_Texture *dst, SDL_Rect *rect)
{
    int p;
    uint32_t *data;

    if(dst == screen) { data = scr_data; p = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&data, &p);
    ui_resample(data, rect->x, rect->y, rect->w, rect->h, p, a, s->data, s->w, s->h, s->w * s->nframe * 4);
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Generate a texture from a sprite with all frames
 */
void spr_texture(ui_sprite_t *s, SDL_Texture **t)
{
    void *pixels;
    int p;

    if(!t) return;
    if(*t) { SDL_DestroyTexture(*t); *t = NULL; }
    if(!s || !s->data) return;
    *t = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, s->w * s->nframe, s->h);
    if(*t) {
        SDL_SetTextureBlendMode(*t, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(*t, NULL, (void**)&pixels, &p);
        memcpy(pixels, s->data, p * s->h);
        SDL_UnlockTexture(*t);
    }
}

/**
 * Render a sprite
 */
void spr_render(ui_sprite_t *s, int frame, SDL_Texture *t, SDL_Rect *rect)
{
    SDL_Rect src, dst;
    int needfree = 0;

    if(!s || !s->data || !s->w || !s->h) return;
    if(!t) {
        spr_texture(s, &t);
        needfree = 1;
    }
    if(t) {
        src.y = 0; src.w = s->w; src.h = s->h; src.x = ui_anim(s->type & 0x7F, s->nframe, frame) * s->w;
        ui_fit(rect->w, rect->h, src.w, src.h, &dst.w, &dst.h);
        dst.x = rect->x + (rect->w - dst.w) / 2; dst.y = rect->y + rect->h - dst.h;
        SDL_RenderCopy(renderer, t, &src, &dst);
        if(needfree)
            SDL_DestroyTexture(t);
    }
}

/**
 * Add merged sprites temporarily to list (to be freed with spr_cleanup)
 */
void spr_temp(int *sprout, int *sprin, int num, int cat, int stripe)
{
    int i, j, sel, mw, mh, mf, ca;
    ui_sprite_t sprites[8];

    if(!sprout) return;
    memset(sprout, 0xff, SPRPERLYR * sizeof(int));
    if(!sprin || num < 1 || stripe < 1) return;
    for(sel = 0; sel < stripe; sel++) {
        ca = (sel % SPRPERLYR) == SPRPERLYR - 1 ? 3 : cat;
        memset(sprites, 0, sizeof(sprites));
        spr_merge(sprites, &mw, &mh, &mf, sprin, 8, 0, num, ca, sel, stripe, -1);
        for(j = 0; j < 8 && (!sprites[j].data || !sprites[j].nframe); j++);
        if(j == 8) continue;
        i = sprout[sel] = project.spritenum[ca]++;
        project.sprites[ca] = (ui_sprlist_t*)realloc(project.sprites[ca], project.spritenum[ca] * sizeof(ui_sprlist_t));
        if(!project.sprites[ca]) main_error(ERR_MEM);
        memset(&project.sprites[ca][i], 0, sizeof(ui_sprlist_t));
        project.sprites[ca][i].idx = -1;
        for(j = 0; j < 8; j++) {
            if(!sprites[j].data || !sprites[j].nframe) continue;
            project.sprites[ca][i].dir[j].type = sprites[j].type;
            project.sprites[ca][i].dir[j].nframe = sprites[j].nframe;
            project.sprites[ca][i].dir[j].w = sprites[j].w;
            project.sprites[ca][i].dir[j].h = sprites[j].h;
            project.sprites[ca][i].dir[j].data = sprites[j].data;
        }
    }
}

/**
 * Merge sprites
 * - sprites: pointer to an array of 8 elements (or 1 if `one` is true)
 * - w,h,f: returned width, height, number of frames
 * - sprs: array of `num` sprite indeces
 * - type: 0 default, merge all frames, 1 merge one frame, 2 merge all and add checker bg
 * - sel, stripe: how to select sprite indeces from `sprs`
 * - dy: -1 or where to draw the base line
 */
void spr_merge(ui_sprite_t *sprites, int *w, int *h, int *f, int *sprs, int dir, int type, int num, int cat, int sel, int stripe,
    int dy)
{
    int i, j, k, x, y, mw, mh, mf, mt;
    ui_sprite_t *s;

    chars_err = 0;
    if(!sprites || !sprs) return;
    mw = mh = mf = mt = 0;
    for(i = 0; i < dir; i++) {
        if(sprites[i].data) free(sprites[i].data);
        sprites[i].data = NULL;
        for(j = 0; j < num; j += stripe) {
            s = spr_getidx(cat, sprs[j + sel], i, 0);
            if(s) {
                if(s->w > mw) mw = s->w;
                if(s->h > mh) mh = s->h;
                if(s->nframe > mf) { mf = s->nframe; mt = s->type; }
            }
        }
    }
    if(type == 1) mf = 1;
    if(mw > 0 && mh > 0 && project.sprites[cat]) {
        for(i = 0; i < dir; i++) {
            /* quickly check if there are any sprites at all */
            for(j = 0; j < num; j += stripe)
                if(sprs[j + sel] >= 0 && project.sprites[cat][sprs[j + sel]].dir[i].data) break;
            if(j >= num) continue;
            sprites[i].w = mw;
            sprites[i].h = mh;
            sprites[i].nframe = mf;
            sprites[i].type = mt;
            sprites[i].data = (uint32_t*)main_alloc(mw * mf * mh * 4);
            if(sprites[i].data) {
                ui_clip.x = ui_clip.h = 0; ui_clip.w = mw * mf; ui_clip.h = mh;
                if(type == 2) {
                    for(y = 0; y < mh; y++)
                        for(x = 0; x < mw * mf; x++)
                            sprites[i].data[y * mw * mf + x] = theme[((y >> 3) & 1) ^ (((x % mw) >> 3) & 1) ?
                                THEME_LIGHT : THEME_DARK];
                }
                y = (mh/2 + dy);
                if(dy != -1 && sel != SPRPERLYR - 1 && y >= 0 && y < mh)
                    for(x = 0; x < mw * mf; x++)
                        sprites[i].data[y * mw * mf + x] = theme[THEME_SELBX];
                for(j = 0; j < num; j += stripe) {
                    s = spr_getidx(cat, sprs[j + sel], i, 0);
                    if(s) {
                        for(k = 0; k < mf; k++)
                            ui_combine(sprites[i].data, k * mw + (mw - s->w) / 2, (mh - s->h) / 2, s->w, s->h, mw * mf * 4,
                                s->data, (k % s->nframe) * s->w, 0, s->w, s->h, s->w * s->nframe * 4);
                        if(type == 2 && (s->nframe != mf || s->type != mt)) {
                            sprites[i].type |= 0x80;
                            if(s->nframe != mf) chars_err |= 1;
                            if(s->type != mt) chars_err |= 2;
                        }
                    }
                }
            }
        }
    }
    ui_clip.x = ui_clip.h = 0; ui_clip.w = scr_w; ui_clip.h = scr_h;
    if(w) *w = mw;
    if(h) *h = mh;
    if(f) *f = mf;
}

/**
 * Helpers to sort atlas sprites
 */
int spr_atlas_sizecmp(const void *a, const void *b)
{
    const spr_atlas_spr_t *A = (const spr_atlas_spr_t*)a;
    const spr_atlas_spr_t *B = (const spr_atlas_spr_t*)b;
    if(A->h != B->h) return B->h - A->h;
    return B->w - A->w;
}
int spr_atlas_idxcmp(const void *a, const void *b)
{
    const spr_atlas_spr_t *A = (const spr_atlas_spr_t*)a;
    const spr_atlas_spr_t *B = (const spr_atlas_spr_t*)b;
    if(A->cat != B->cat) return A->cat - B->cat;
    if(A->idx != B->idx) return A->idx - B->idx;
    if(A->dir != B->dir) return A->dir - B->dir;
    return A->frm - B->frm;
}

/**
 * Add sprite to atlas
 */
void spr_atlas_add(spr_atlas_t *atlas, int cat, int idx, int orig, int dir, ui_sprite_t *s)
{
    int f, x, y;
    spr_atlas_spr_t spr;

    for(f = 0; f < s->nframe; f++) {
        ui_progressbar(1, 2, project_curr++, project_total, LANG_COLLECT);
        memset(&spr, 0, sizeof(spr));
        spr.orig = orig;
        spr.cat = cat;
        spr.idx = idx;
        spr.dir = dir;
        spr.frm = f;
        spr.w = s->w;
        spr.h = s->h;
        spr.p = s->nframe * s->w;
        if(!project.sprites[cat][orig].name || project.sprites[cat][orig].name[0] != '~') {
            /* crop sprite to content */
            for(y = 0; y < s->h && spr.h > 0; y++) {
                for(x = 0; x < s->w && !(s->data[y * spr.p + f * s->w + x] & 0xfc000000); x++);
                if(x < s->w) break;
                spr.t++; spr.h--;
            }
            for(y = s->h - 1; y >= spr.t && spr.h > 0; y--) {
                for(x = 0; x < s->w && !(s->data[y * spr.p + f * s->w + x] & 0xfc000000); x++);
                if(x < s->w) break;
                spr.h--;
            }
            for(x = 0; x < s->w && spr.h > 0 && spr.w > 0; x++) {
                for(y = 0; y < spr.h && !(s->data[(y + spr.t) * spr.p + f * s->w + x] & 0xfc000000); y++);
                if(y < spr.h) break;
                spr.l++; spr.w--;
            }
            for(x = s->w - 1; x >= spr.l && spr.h > 0 && spr.w > 0; x--) {
                for(y = 0; y < spr.h && !(s->data[(y + spr.t) * spr.p + f * s->w + x] & 0xfc000000); y++);
                if(y < spr.h) break;
                spr.w--;
            }
            if(spr.w < 1 || spr.h < 1) spr.w = spr.h = spr.l = spr.t = 0;
        } else
            spr.w = spr.h = spr.l = spr.t = 0;
        spr.data = s->data + spr.p * spr.t + f * s->w + spr.l;
        if(spr.w > atlas->mw) atlas->mw = spr.w;
        if(spr.h > atlas->mh) atlas->mw = spr.h;
        atlas->sp += spr.w * spr.h;
        atlas->spr = (spr_atlas_spr_t*)realloc(atlas->spr, (atlas->numspr + 1) * sizeof(spr_atlas_spr_t));
        if(!atlas->spr) main_error(ERR_MEM);
        memcpy(atlas->spr + atlas->numspr, &spr, sizeof(spr_atlas_spr_t));
        atlas->numspr++;
    }
}

/**
 * Build atlas and mappings
 */
void spr_atlas_build(spr_atlas_t *atlas, int size)
{
    uint32_t *dst, *src;
    int i, j, a, b, c, m;

    for(i = 0; i < atlas->numspr; i++)
        if(atlas->spr[i].h > 0) {
            /* deduplicate */
            for(m = 0; m < i; m++) {
                if(atlas->spr[m].o != TNG_ATLAS_COPY) continue;
                /* if the sprites match, simply use the previous sprite's atlas mapping */
                if(atlas->spr[m].w >= atlas->spr[i].w && atlas->spr[m].h >= atlas->spr[i].h) {
                    b = atlas->spr[m].w - atlas->spr[i].w;
                    c = atlas->spr[m].h - atlas->spr[i].h;
                    /* match on top left (or entire sprite match) */
                    for(a = 0; a < atlas->spr[i].h && !memcmp(atlas->spr[m].data + a * atlas->spr[m].p,
                        atlas->spr[i].data + a * atlas->spr[i].p, atlas->spr[i].w * sizeof(uint32_t)); a++);
                    if(a == atlas->spr[i].h) {
                        atlas->spr[i].o = TNG_ATLAS_COPY;
                        atlas->spr[i].a = atlas->spr[m].a;
                        atlas->spr[i].x = atlas->spr[m].x;
                        atlas->spr[i].y = atlas->spr[m].y;
                        break;
                    }
                    /* match on top right */
                    if(b > 0) {
                        for(a = 0; a < atlas->spr[i].h && !memcmp(atlas->spr[m].data + b + a * atlas->spr[m].p,
                            atlas->spr[i].data + a * atlas->spr[i].p, atlas->spr[i].w * sizeof(uint32_t)); a++);
                        if(a == atlas->spr[i].h) {
                            atlas->spr[i].o = TNG_ATLAS_COPY;
                            atlas->spr[i].a = atlas->spr[m].a;
                            atlas->spr[i].x = atlas->spr[m].x + b;
                            atlas->spr[i].y = atlas->spr[m].y;
                            break;
                        }
                    }
                    /* match on bottom left */
                    if(c > 0) {
                        for(a = 0; a < atlas->spr[i].h && !memcmp(atlas->spr[m].data + (a + c) * atlas->spr[m].p,
                            atlas->spr[i].data + a * atlas->spr[i].p, atlas->spr[i].w * sizeof(uint32_t)); a++);
                        if(a == atlas->spr[i].h) {
                            atlas->spr[i].o = TNG_ATLAS_COPY;
                            atlas->spr[i].a = atlas->spr[m].a;
                            atlas->spr[i].x = atlas->spr[m].x;
                            atlas->spr[i].y = atlas->spr[m].y + c;
                            break;
                        }
                    }
                    /* match on bottom right */
                    if(b > 0 && c > 0) {
                        for(a = 0; a < atlas->spr[i].h && !memcmp(atlas->spr[m].data + b + (a + c) * atlas->spr[m].p,
                            atlas->spr[i].data + a * atlas->spr[i].p, atlas->spr[i].w * sizeof(uint32_t)); a++);
                        if(a == atlas->spr[i].h) {
                            atlas->spr[i].o = TNG_ATLAS_COPY;
                            atlas->spr[i].a = atlas->spr[m].a;
                            atlas->spr[i].x = atlas->spr[m].x + b;
                            atlas->spr[i].y = atlas->spr[m].y + c;
                            break;
                        }
                    }
                }
                if(atlas->spr[m].w == atlas->spr[i].w && atlas->spr[m].h == atlas->spr[i].h) {
                    /* check if it's vertically flipped version of another sprite */
                    if(atlas->spr[i].h > 1) {
                        for(a = 0; a < atlas->spr[i].w; a++) {
                            for(b = 0; b < atlas->spr[i].h && atlas->spr[m].data[b * atlas->spr[m].p + a] ==
                              atlas->spr[i].data[(atlas->spr[i].h - 1 - b) * atlas->spr[i].p + a]; b++);
                            if(b < atlas->spr[i].h) break;
                        }
                        if(a == atlas->spr[i].w) {
                            atlas->spr[i].o = TNG_ATLAS_VFLIP;
                            atlas->spr[i].a = atlas->spr[m].a;
                            atlas->spr[i].x = atlas->spr[m].x;
                            atlas->spr[i].y = atlas->spr[m].y;
                            break;
                        }
                    }
                    /* check if it's horizontally flipped version of another sprite */
                    if(atlas->spr[i].w > 1) {
                        for(a = 0; a < atlas->spr[i].h; a++) {
                            for(b = 0; b < atlas->spr[i].w && atlas->spr[m].data[a * atlas->spr[m].p + b] ==
                              atlas->spr[i].data[a * atlas->spr[i].p + atlas->spr[i].w - 1 - b]; b++);
                            if(b < atlas->spr[i].w) break;
                        }
                        if(a == atlas->spr[i].h) {
                            atlas->spr[i].o = TNG_ATLAS_HFLIP;
                            atlas->spr[i].a = atlas->spr[m].a;
                            atlas->spr[i].x = atlas->spr[m].x;
                            atlas->spr[i].y = atlas->spr[m].y;
                            break;
                        }
                    }
                    /* check if it's a 180 degrees rotated version of another sprite */
                    for(b = 0; b < atlas->spr[i].h; b++) {
                        for(a = 0; a < atlas->spr[i].w &&
                          atlas->spr[m].data[(atlas->spr[m].h - 1 - b) * atlas->spr[m].p + atlas->spr[m].w - 1 - a] ==
                          atlas->spr[i].data[b * atlas->spr[i].p + a]; a++);
                        if(a < atlas->spr[i].w) break;
                    }
                    if(b == atlas->spr[i].h) {
                        atlas->spr[i].o = TNG_ATLAS_ROT;
                        atlas->spr[i].a = atlas->spr[m].a;
                        atlas->spr[i].x = atlas->spr[m].x;
                        atlas->spr[i].y = atlas->spr[m].y;
                        break;
                    }
                }
                if(atlas->spr[m].h == atlas->spr[i].w && atlas->spr[m].w == atlas->spr[i].h) {
                    /* check if it's clockwise rotated version of another sprite */
                    for(a = 0; a < atlas->spr[i].h; a++) {
                        for(b = 0; b < atlas->spr[i].w &&
                          atlas->spr[m].data[(atlas->spr[m].h - 1 - b) * atlas->spr[m].p + a] ==
                          atlas->spr[i].data[a * atlas->spr[i].p + b]; b++);
                        if(b < atlas->spr[i].w) break;
                    }
                    if(a == atlas->spr[i].h) {
                        atlas->spr[i].o = TNG_ATLAS_ROTCW;
                        atlas->spr[i].a = atlas->spr[m].a;
                        atlas->spr[i].x = atlas->spr[m].x;
                        atlas->spr[i].y = atlas->spr[m].y;
                        atlas->spr[i].w = atlas->spr[m].w;
                        atlas->spr[i].h = atlas->spr[m].h;
                        break;
                    }
                    /* check if it's counter clockwise rotated version of another sprite */
                    for(a = 0; a < atlas->spr[i].h; a++) {
                        for(b = 0; b < atlas->spr[i].w &&
                          atlas->spr[m].data[b * atlas->spr[m].p + atlas->spr[m].w - 1 - a] ==
                          atlas->spr[i].data[a * atlas->spr[i].p + b]; b++);
                        if(b < atlas->spr[i].w) break;
                    }
                    if(a == atlas->spr[i].h) {
                        atlas->spr[i].o = TNG_ATLAS_ROTCCW;
                        atlas->spr[i].a = atlas->spr[m].a;
                        atlas->spr[i].x = atlas->spr[m].x;
                        atlas->spr[i].y = atlas->spr[m].y;
                        atlas->spr[i].w = atlas->spr[m].w;
                        atlas->spr[i].h = atlas->spr[m].h;
                        break;
                    }
                }
            }
            if(m >= i) {
                /* find best fit */
                for(m = -1, a = size * size + 1, j = 0; atlas->slots && j < atlas->numslot; j++)
                    if(atlas->slots[j].w >= atlas->spr[i].w && atlas->slots[j].h >= atlas->spr[i].h) {
                        b = (atlas->slots[j].w - atlas->spr[i].w) * atlas->spr[i].h +
                            (atlas->slots[j].h - atlas->spr[i].h) * atlas->slots[j].w;
                        if(b < a) { a = b; m = j; }
                    }
                if(m == -1) {
                    /* if there was no slot to fit the sprite, add a new atlas image */
                    atlas->imgs = (uint32_t**)realloc(atlas->imgs, (atlas->numimg + 1) * sizeof(uint32_t*));
                    if(!atlas->imgs) main_error(ERR_MEM);
                    atlas->imgs[atlas->numimg] = (uint32_t*)main_alloc(size * size * sizeof(uint32_t));
                    atlas->slots = (spr_atlas_slot_t*)realloc(atlas->slots, (atlas->numslot + 2) * sizeof(spr_atlas_slot_t));
                    if(!atlas->slots) main_error(ERR_MEM);
                    atlas->slots[atlas->numslot].a = atlas->slots[atlas->numslot + 1].a = atlas->spr[i].a = atlas->numimg;
                    atlas->slots[atlas->numslot].x = atlas->spr[i].w;
                    atlas->slots[atlas->numslot].y = atlas->slots[atlas->numslot + 1].x = 0;
                    atlas->slots[atlas->numslot].w = size - atlas->spr[i].w;
                    atlas->slots[atlas->numslot].h = atlas->slots[atlas->numslot + 1].y = atlas->spr[i].h;
                    atlas->slots[atlas->numslot + 1].w = size;
                    atlas->slots[atlas->numslot + 1].h = size - atlas->spr[i].h;
                    atlas->numslot += 2;
                    atlas->numimg++;
                    if(atlas->numimg > 0xfff) main_error(ERR_MEM);
                } else {
                    /* we have found a slot, split it
                     *
                     * mmmmm    ssmmm     (m = slot found)
                     * mmmmm -> ssmmm     (s = sprite to insert)
                     * mmmmm    nnnnn     (n = new slot)
                     */
                    atlas->slots = (spr_atlas_slot_t*)realloc(atlas->slots, (atlas->numslot + 1) * sizeof(spr_atlas_slot_t));
                    if(!atlas->slots) main_error(ERR_MEM);
                    atlas->spr[i].a = atlas->slots[atlas->numslot].a = atlas->slots[m].a;
                    atlas->spr[i].x = atlas->slots[atlas->numslot].x = atlas->slots[m].x;
                    atlas->spr[i].y = atlas->slots[atlas->numslot].y = atlas->slots[m].y;
                    atlas->slots[atlas->numslot].w = atlas->slots[m].w;
                    atlas->slots[atlas->numslot].y += atlas->spr[i].h;
                    atlas->slots[atlas->numslot].h = atlas->slots[m].h - atlas->spr[i].h;
                    atlas->slots[m].x += atlas->spr[i].w;
                    atlas->slots[m].w -= atlas->spr[i].w;
                    atlas->slots[m].h = atlas->spr[i].h;
                    atlas->numslot++;
                }
                /* copy the sprite to the image */
                src = atlas->spr[i].data;
                dst = atlas->imgs[atlas->spr[i].a] + size * atlas->spr[i].y + atlas->spr[i].x;
                for(j = 0; j < atlas->spr[i].h; j++, dst += size, src += atlas->spr[i].p)
                    memcpy(dst, src, atlas->spr[i].w * sizeof(uint32_t));
            }
            ui_progressbar(1, 2, project_curr++, project_total, LANG_COLLECT);
        }
}

/**
 * Add data to atlas asset
 */
void spr_atlas_data(spr_atlas_t *atlas, void *data, int len)
{
    atlas->asset = (uint8_t*)realloc(atlas->asset, atlas->len + len);
    if(!atlas->asset) { atlas->len = 0; return; }
    memcpy(atlas->asset + atlas->len, data, len);
    atlas->len += len;
}

/**
 * Flush atlas asset
 */
void spr_atlas_flush(tngctx_t *ctx, spr_atlas_t *atlas)
{
    uLongf cl;
    uint8_t *comp;

    if(!ctx || !atlas->asset || atlas->len < 1) return;
    cl = compressBound(atlas->len);
    comp = (uint8_t*)main_alloc(cl);
    compress2(comp, &cl, atlas->asset, atlas->len, 9);
    if(cl) {
        tng_desc_idx(ctx, atlas->nd << 24, cl);
        tng_asset_int(ctx, comp, cl);
    } else
        free(comp);
    free(atlas->asset); atlas->asset = NULL;
    atlas->len = atlas->nd = 0;
}

/**
 * Free internal atlas resources
 */
void spr_atlas_free(spr_atlas_t *atlas)
{
    int i;
    if(atlas->imgs) {
        for(i = 0; i < atlas->numimg; i++)
            if(atlas->imgs[i]) free(atlas->imgs[i]);
        free(atlas->imgs);
    }
    if(atlas->slots) free(atlas->slots);
    if(atlas->spr) free(atlas->spr);
    if(atlas->asset) free(atlas->asset);
}

/**
 * Save tng
 */
int spr_totng(tngctx_t *ctx)
{
    spr_atlas_t atlas;
    int i, j, x, y, a, cat, len;
    uint32_t *img;
    uint8_t *png, *buf;

    if(!ctx) { spr_cleanup(); return 1; }

    memset(&atlas, 0, sizeof(atlas));
    for(cat = 0; cat < 4; cat++) {
        for(i = 0; project.sprites[cat] && i < project.spritenum[cat]; i++)
            if(project.sprites[cat][i].idx != -1)
                for(j = 0; j < 8; j++)
                    if(project.sprites[cat][i].dir[j].data)
                        spr_atlas_add(&atlas, cat, project.sprites[cat][i].idx, i, j, &project.sprites[cat][i].dir[j]);
    }
    if(atlas.spr) {
        qsort(atlas.spr, atlas.numspr, sizeof(spr_atlas_spr_t), spr_atlas_sizecmp);
        /* determine the optimal atlas size */
        for(j = 256, ctx->hdr.atlaswh = 8; j < atlas.mw || j < atlas.mh || j * j < atlas.sp; j <<= 1, ctx->hdr.atlaswh++);
        if(j > 2048) {
            j = 2048;
            if(j < atlas.mw) j = atlas.mw;
            if(j < atlas.mh) j = atlas.mh;
            for(ctx->hdr.atlaswh = 8; j > (1 << ctx->hdr.atlaswh); ctx->hdr.atlaswh++);
        }
        spr_atlas_build(&atlas, (1 << ctx->hdr.atlaswh));
        if(atlas.numimg > 0) {
            tng_section(ctx, TNG_SECTION_SPRITEATLAS);
            ui_progressbar(1, 2, project_curr, project_total + 1, LANG_COMPRESS);
            for(i = 0; i < atlas.numimg; i++) {
                /* crop bottom */
                img = atlas.imgs[i];
                for(y = j - 1; y > 0; y--) {
                    for(x = 0; x < j && !img[y * j + x]; x++);
                    if(x < j) break;
                }
                png = image_mem(j, y + 1, (uint8_t*)atlas.imgs[i], &len, NULL);
                if(png && len) {
                    tng_desc_idx(ctx, 0, len);
                    tng_asset_int(ctx, png, len);
                }
            }
            ui_progressbar(1, 2, project_curr, project_total + 1, LANG_COLLECT);
            qsort(atlas.spr, atlas.numspr, sizeof(spr_atlas_spr_t), spr_atlas_idxcmp);
            for(cat = x = y = -1, i = 0; i < atlas.numspr; i++) {
                if(atlas.spr[i].cat != cat) {
                    spr_atlas_flush(ctx, &atlas);
                    cat = atlas.spr[i].cat;
                    tng_section(ctx, TNG_SECTION_SPRITE_UI + cat);
                    y = -1;
                }
                if(atlas.spr[i].idx != y) {
                    spr_atlas_flush(ctx, &atlas);
                    y = atlas.spr[i].idx;
                    j = -1;
                }
                if(atlas.spr[i].dir != j) {
                    atlas.nd++;
                    j = atlas.spr[i].dir;
                    spr_atlas_data(&atlas, &project.sprites[cat][atlas.spr[i].orig].dir[j].w, 2);
                    spr_atlas_data(&atlas, &project.sprites[cat][atlas.spr[i].orig].dir[j].h, 2);
                    x = (project.sprites[cat][atlas.spr[i].orig].dir[j].type << 6) | j;
                    spr_atlas_data(&atlas, &x, 1);
                    spr_atlas_data(&atlas, &project.sprites[cat][atlas.spr[i].orig].dir[j].nframe, 1);
                }
                a = atlas.spr[i].a | (atlas.spr[i].o << 12);
                spr_atlas_data(&atlas, &a, 2);
                spr_atlas_data(&atlas, &atlas.spr[i].x, 2);
                spr_atlas_data(&atlas, &atlas.spr[i].y, 2);
                spr_atlas_data(&atlas, &atlas.spr[i].w, 2);
                spr_atlas_data(&atlas, &atlas.spr[i].h, 2);
                spr_atlas_data(&atlas, &atlas.spr[i].l, 2);
                spr_atlas_data(&atlas, &atlas.spr[i].t, 2);
            }
            spr_atlas_flush(ctx, &atlas);
        }
    }
    spr_atlas_free(&atlas);
    /* no sprite atlas for background images */
    if(project.sprites[4] && project.spritenum[4]) {
        for(j = 0; j < project.spritenum[4] && project.sprites[4][j].idx == -1; j++);
        if(j < project.spritenum[4]) {
            tng_section(ctx, TNG_SECTION_SPRITE_BACKGROUND);
            for(i = 0; i < project.spritenum[4]; i++) {
                for(j = 0; j < project.spritenum[4] && project.sprites[4][j].idx != i; j++);
                if(j >= project.spritenum[4]) break;
                sprintf(projfn, "%s" SEP "%s" SEP "%s_full.png", project.id, project_dirs[PROJDIRS_SPRITES + 4],
                    project.sprites[4][j].name);
                /* re-encode backgrounds with a different strategy (slower, but results in smaller file size) */
                if((buf = image_load(projdir, &x, &y))) {
                    png = image_mem(x, y, buf, &len, NULL);
                    if(png && len) {
                        tng_desc_idx(ctx, 0, len);
                        tng_asset_int(ctx, png, len);
                    }
                    free(buf);
                }
                ui_progressbar(1, 2, project_curr, project_total + 1, LANG_COLLECT);
                project_curr += 2;
            }
        }
    }
    spr_cleanup();
    return 1;
}

/**
 * Read from tng
 */
int spr_fromtng(tngctx_t *ctx)
{
    FILE *f;
    spr_atlas_t atlas;
    tng_asset_desc_t *asset;
    tng_sprmap_desc_t *sprmap;
    stbi__context s;
    stbi__result_info ri;
    uint32_t *data, *src, *dst;
    uint8_t *buf, *ptr, *end;
    int i, j, k, l, w, h, x, y, a, aw, len, cat, dir, type, nf;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->f) return 0;
    memset(&atlas, 0, sizeof(atlas));
    aw = 1 << ctx->hdr.atlaswh;
    for(j = 0; j < ctx->numtbl; j++)
        switch(TNG_SECTION_TYPE(&ctx->tbl[j])) {
            case TNG_SECTION_SPRITE_BACKGROUND:
                asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[j].offs);
                len = TNG_SECTION_SIZE(&ctx->tbl[j]) / sizeof(tng_asset_desc_t);
                sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_SPRITES + 5]);
                project_mkdir(projdir);
                for(i = 0; i < len; i++, asset++) {
                    sprintf(projfn, "%s" SEP "%s" SEP "%04X_full.png", project.id, project_dirs[PROJDIRS_SPRITES + 5], i);
                    if(verbose) printf("spr_fromtng: saving %s\n", projdir);
                    tng_save_asset(ctx, asset->offs, asset->size, projdir);
                    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
                }
            break;
            case TNG_SECTION_SPRITEATLAS:
                asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[j].offs);
                atlas.numimg = TNG_SECTION_SIZE(&ctx->tbl[j]) / sizeof(tng_asset_desc_t);
                atlas.imgs = (uint32_t**)main_alloc(atlas.numimg * sizeof(uint32_t*));
                for(i = 0; i < atlas.numimg; i++, asset++) {
                    project_fseek(ctx->f, ctx->offs + asset->offs);
                    buf = (uint8_t*)main_alloc(asset->size);
                    len = (int)fread(buf, 1, asset->size, ctx->f);
                    if(len == (int)asset->size) {
                        memset(&s, 0, sizeof(s));
                        memset(&ri, 0, sizeof(ri));
                        s.img_buffer = s.img_buffer_original = buf;
                        s.img_buffer_end = s.img_buffer_original_end = buf + len;
                        ri.bits_per_channel = 8;
                        atlas.imgs[i] = (uint32_t*)stbi__png_load(&s, &w, &h, &l, 4, &ri);
                        if(w != aw || h < 1 || l != 4) {
                            if(atlas.imgs[i]) free(atlas.imgs[i]);
                            atlas.imgs[i] = NULL;
                        }
                    }
                    free(buf);
                    if(verbose && !atlas.imgs[i]) printf("spr_fromtng: failed to load atlas #%d\n", i);
                }
            break;
        }
    if(atlas.numimg > 0)
        for(j = 0; j < ctx->numtbl; j++) {
            l = TNG_SECTION_TYPE(&ctx->tbl[j]);
            if(l >= TNG_SECTION_SPRITE_UI && l < TNG_SECTION_SPRITE_BACKGROUND) {
                cat = l - TNG_SECTION_SPRITE_UI;
                asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[j].offs);
                project.spritenum[cat] = len = TNG_SECTION_SIZE(&ctx->tbl[j]) / sizeof(tng_asset_desc_t);
                sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_SPRITES + cat]);
                project_mkdir(projdir);
                project.sprites[cat] = (ui_sprlist_t*)main_alloc(len * sizeof(ui_sprlist_t));
                for(i = 0; i < len; i++, asset++) {
                    project.sprites[cat][i].name = (char*)main_alloc(8);
                    sprintf(project.sprites[cat][i].name, "%04X", i);
                    buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &l);
                    if(buf && l > 0) {
                        end = buf + l;
                        for(l = 0; l < (int)(asset->name >> 24) && ptr < end; l++) {
                            w = (ptr[1] << 8) | ptr[0]; ptr += 2;
                            h = (ptr[1] << 8) | ptr[0]; ptr += 2;
                            type = ptr[0] >> 6; dir = ptr[0] & 7; ptr++;
                            nf = ptr[0]; ptr++;
                            project.sprites[cat][i].dir[dir].type = type == 3 ? 0x7F : type;
                            project.sprites[cat][i].dir[dir].nframe = nf;
                            project.sprites[cat][i].dir[dir].w = w;
                            project.sprites[cat][i].dir[dir].h = h;
                            project.sprites[cat][i].dir[dir].data = data = (uint32_t*)malloc(w * nf * h * 4);
                            if(data) {
                                memset(data, 0, w * nf * h * 4);
                                for(k = 0; k < nf && ptr < end; k++, ptr += sizeof(tng_sprmap_desc_t)) {
                                    sprmap = (tng_sprmap_desc_t*)ptr;
                                    a = sprmap->atlasid & 0xfff;
                                    if(a >= atlas.numimg || !atlas.imgs[a]) continue;
                                    src = atlas.imgs[a] + sprmap->y * aw + sprmap->x;
                                    dst = data + sprmap->t * w * nf + sprmap->l + k * w;
                                    switch(sprmap->atlasid >> 12) {
                                        case TNG_ATLAS_ROTCCW:
                                            for(y = 0; y < sprmap->h; y++, dst += w * nf)
                                                for(x = 0; x < sprmap->w; x++)
                                                    dst[x] = src[(sprmap->h - 1 - x) * aw + y];
                                        break;
                                        case TNG_ATLAS_ROTCW:
                                            for(y = 0; y < sprmap->h; y++, dst += w * nf)
                                                for(x = 0; x < sprmap->w; x++)
                                                    dst[x] = src[x * aw + sprmap->w - 1 - y];
                                        break;
                                        case TNG_ATLAS_HFLIP:
                                            src += (sprmap->h - 1) * aw;
                                            for(y = 0; y < sprmap->h; y++, src -= aw, dst += w * nf)
                                                memcpy(dst, src, sprmap->w * 4);
                                        break;
                                        case TNG_ATLAS_VFLIP:
                                            for(y = 0; y < sprmap->h; y++, src += aw, dst += w * nf)
                                                for(x = 0; x < sprmap->w; x++)
                                                    dst[x] = src[sprmap->w - 1 - x];
                                        break;
                                        default:
                                            for(y = 0; y < sprmap->h; y++, src += aw, dst += w * nf)
                                                memcpy(dst, src, sprmap->w * 4);
                                        break;
                                    }
                                }
                                if(type == 3)
                                    sprintf(projfn, "%s" SEP "%s" SEP "%04X_full.png", project.id,
                                        project_dirs[PROJDIRS_SPRITES + cat], i);
                                else
                                    sprintf(projfn, "%s" SEP "%s" SEP "%04X_%s%c%c.png", project.id,
                                        project_dirs[PROJDIRS_SPRITES + cat], i, project_directions[dir],
                                        project_animtypes[type], project_animframes[nf - 1]);
                                if(verbose) printf("spr_fromtng: saving %s\n", projdir);
                                f = project_fopen(projdir, "wb+");
                                if(f) {
                                    image_save(f, w * nf, h, w * nf * 4, (uint8_t*)data);
                                    fclose(f);
                                } else
                                    if(verbose) printf("spr_fromtng: unable to write %s\n", projdir);
                            } else
                                ptr += nf * sizeof(tng_sprmap_desc_t);
                            ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
                        }
                        free(buf); buf = NULL;
                    } else {
                        if(verbose) printf("spr_fromtng: failed to uncompress sprite definiton cat %d idx %d\n", cat, i);
                        ui_progressbar(0, 0, ctx->curr, ctx->total, LANG_EXTRACTING);
                        ctx->curr += asset->name >> 24;
                    }
                }
            }
        }
    spr_atlas_free(&atlas);
    return 1;
}

/**
 * Helpers to sort packed sprites
 */
int spr_pack_sizecmp(const void *a, const void *b)
{
    const spr_pack_t *A = (const spr_pack_t*)a;
    const spr_pack_t *B = (const spr_pack_t*)b;
    if(A->h != B->h) return B->h - A->h;
    return B->w - A->w;
}

/**
 * Pack multiple sprite images into a single atlas
 */
void spr_pack(char *name)
{
    int i, j, l, num = 0, w = 0, h = 0, nw, nh, a, b, m;
    char **list, fn[FILENAME_MAX], *c, *s;
    int64_t p = 0;
    uint32_t *out, *src, *dst;
    uint8_t *png;
    spr_pack_t *meta;
    spr_atlas_t atlas;
    FILE *f;

    if(!name || !*name) return;
    list = project_getdir(".", ".png", NULL);
    l = strlen(name);
    for(i = 0; list[i]; i++) {
        j = strlen(list[i]);
        if(j > 6 && !memcmp(list[i], name, l) && list[i][l] == '_' && memcmp(list[i] + j - 4, "atls", 4) &&
          memcmp(list[i] + j - 4, "full", 4))
            num++;
        else
            list[i][0] = 0;
    }
    if(num > 0) {
        meta = (spr_pack_t*)main_alloc(num * sizeof(spr_pack_t));
        for(i = num = w = h = 0, s = 0; list[i]; i++) {
            if(!list[i][0]) continue;
            strncpy(fn, list[i], sizeof(fn) - 1);
            strncat(fn, ".png", sizeof(fn) - 1);
            meta[num].data = image_load(fn, &meta[num].w, &meta[num].h);
            if(meta[num].data) {
                if(meta[num].w > w) w = meta[num].w;
                if(meta[num].h > h) h = meta[num].h;
                p += (int64_t)meta[num].w * (int64_t)meta[num].h;
                meta[num++].name = list[i] + l + 1;
            } else {
                meta[num].w = meta[num].h = 0;
                printf("tnge: %s %s\r\n", lang[ERR_IMAGE], fn);
            }
        }
        m = w < h ? w : h;
        for(j = 0; j < w || j < h || j * j < p; j += m);
        w = h = j;
        qsort(meta, num, sizeof(spr_pack_t), spr_pack_sizecmp);
again:  memset(&atlas, 0, sizeof(atlas));
        atlas.slots = (spr_atlas_slot_t*)main_alloc(sizeof(spr_atlas_slot_t));
        atlas.slots[0].w = w; atlas.slots[0].h = h; atlas.numslot = 1; nw = nh = 0;
        for(i = 0; i < num && meta[i].w; i++) {
            for(m = -1, a = w * h + 1, j = 0; j < atlas.numslot; j++)
                if(atlas.slots[j].w >= meta[i].w && atlas.slots[j].h >= meta[i].h) {
                    b = (atlas.slots[j].w - meta[i].w) * meta[i].h +
                        (atlas.slots[j].h - meta[i].h) * atlas.slots[j].w;
                    if(b < a) { a = b; m = j; }
                }
            if(m >= 0) {
                atlas.slots = (spr_atlas_slot_t*)realloc(atlas.slots, (atlas.numslot + 1) * sizeof(spr_atlas_slot_t));
                if(!atlas.slots) main_error(ERR_MEM);
                meta[i].x = atlas.slots[atlas.numslot].x = atlas.slots[m].x;
                meta[i].y = atlas.slots[atlas.numslot].y = atlas.slots[m].y;
                atlas.slots[atlas.numslot].w = atlas.slots[m].w;
                atlas.slots[atlas.numslot].y += meta[i].h;
                atlas.slots[atlas.numslot].h = atlas.slots[m].h - meta[i].h;
                atlas.slots[m].x += meta[i].w;
                atlas.slots[m].w -= meta[i].w;
                atlas.slots[m].h = meta[i].h;
                atlas.numslot++;
                if(meta[i].x + meta[i].w > nw) nw = meta[i].x + meta[i].w;
                if(meta[i].y + meta[i].h > nh) nh = meta[i].y + meta[i].h;
            } else {
                free(atlas.slots);
                h <<= 1;
                if(h > 0xFFFF) { printf("tnge: should never happen, no free slots?\r\n"); exit(1); }
                goto again;
            }
        }
        spr_atlas_free(&atlas);
        c = s = (char*)main_alloc(num * (FILENAME_MAX + 32) + 32);
        c += sprintf(c, PROJMAGIC_ATLAS "\n");
        out = (uint32_t*)main_alloc(nw * nh * 4);
        for(i = 0; i < num && meta[i].w; i++) {
            src = (uint32_t*)meta[i].data;
            dst = out + nw * meta[i].y + meta[i].x;
            for(j = 0; j < meta[i].h; j++, dst += nw, src += meta[i].w)
                memcpy(dst, src, meta[i].w * sizeof(uint32_t));
            c += sprintf(c, "%u %u %u %u %s\n", meta[i].x, meta[i].y, meta[i].w, meta[i].h, meta[i].name);
            free(meta[i].data);
        }
        png = image_mem(nw, nh, (uint8_t*)out, &l, s);
        if(png) {
            strncpy(fn, name, sizeof(fn) - 1);
            strncat(fn, "_atls.png", sizeof(fn) - 1);
            f = project_fopen(fn, "wb+");
            if(f) {
                if(fwrite(png, 1, l, f) == (size_t)l) {
                    printf("tnge: %s %s\r\n", fn, lang[LANG_SAVED]);
                    for(i = 0; i < num && meta[i].w; i++) {
                        strncpy(fn, name, sizeof(fn) - 1);
                        strncat(fn, "_", sizeof(fn) -1);
                        strncat(fn, meta[i].name, sizeof(fn) - 1);
                        strncat(fn, ".png", sizeof(fn) - 1);
                        project_rm(fn);
                    }
                }
                fclose(f);
            }
            free(png);
        }
        free(meta);
        free(out);
        free(s);
    } else
        printf("tnge: %s\r\n", lang[SPRITES_NONE]);
    project_freedir(&list, NULL);
}

/**
 * Unpack atlas image into multiple sprite images
 */
void spr_unpack(char *name)
{
    int i, x, y, W, H, w, h, err = 0;
    char fn[FILENAME_MAX], *str, dir, type, nframe, *full;
    uint8_t *img, *out;
    uint32_t *src, *dst;
    FILE *f;

    if(!name || !*name) return;
    strncpy(fn, name, sizeof(fn) - 1);
    strncat(fn, "_atls.png", sizeof(fn) - 1);
    stbi__need_comment = 1;
    if((img = image_load(fn, &w, &h)) && img && w > 0 && h > 0 && stbi__comment && !memcmp(stbi__comment, PROJMAGIC_ATLAS, 14)) {
        str = project_skipnl(stbi__comment);
        while(*str) {
            str = spr_getmeta(name, str, &x, &y, &W, &H, &dir, &type, &nframe, &full);
            if(!full) continue;
            sprintf(fn, "%s_%s%c%c.png", full, project_directions[(int)dir], project_animtypes[(int)type],
                nframe > 31 ? 'V' : project_animframes[(int)nframe - 1]);
            out = (uint8_t*)main_alloc(W * H * 4);
            src = (uint32_t*)(img + (w * y + x) * 4); dst = (uint32_t*)out;
            for(i = 0; i < H; i++, src += w, dst += W)
                memcpy(dst, src, W * 4);
            f = project_fopen(fn, "wb+");
            if(f) {
                if(!image_save(f, W, H, W * 4, out)) err++;
                else printf("tnge: %s %s\r\n", fn, lang[LANG_SAVED]);
                fclose(f);
            } else err++;
            free(out);
            free(full);
        }
        if(!err) {
            strncpy(fn, name, sizeof(fn) - 1);
            strncat(fn, "_atls.png", sizeof(fn) - 1);
            project_rm(fn);
        }
    } else
        printf("tnge: %s %s\r\n", lang[ERR_IMAGE], fn);
    stbi__need_comment = 0;
    if(stbi__comment) free(stbi__comment);
    if(img) free(img);
}

/**
 * List sprites in an atlas
 */
void spr_listatls(char *name)
{
    int w, h;
    char fn[FILENAME_MAX];
    uint8_t *img;

    if(!name || !*name) return;
    strncpy(fn, name, sizeof(fn) - 1);
    strncat(fn, "_atls.png", sizeof(fn) - 1);
    stbi__need_comment = 1;
    if((img = image_load(fn, &w, &h)) && img && w > 0 && h > 0 && stbi__comment && !memcmp(stbi__comment, PROJMAGIC_ATLAS, 14)) {
        printf("%s\r\n",stbi__comment);
    } else
        printf("tnge: %s %s\r\n", lang[ERR_IMAGE], fn);
    stbi__need_comment = 0;
    if(stbi__comment) free(stbi__comment);
    if(img) free(img);
}
