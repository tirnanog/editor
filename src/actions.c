/*
 * tnge/actions.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Startup commands window
 *
 */

#include "main.h"

void actions_save(void *data);
void actions_delete(void *data);

char actions_title[256];
char actions_name[9][PROJ_NAMEMAX + 1] = { 0 };
ui_input_t actions_nameinp = { INP_ID, sizeof(actions_name[0]), NULL };
ui_cmdpal_t actions_pal = { CMDPAL_ACTION, 0, 0 };
ui_cmd_t actions_cmd[9] = { 0 };
ui_select_t actions_range[9] = { 0 };
int actions_action = 0;

ui_icongrpelem_t actions_selelems[] = {
    { ICON_NUMACT + 1, 0 },
    { ICON_NUMACT + 2, 0 },
    { ICON_NUMACT + 3, 0 },
    { ICON_NUMACT + 4, 0 },
    { ICON_NUMACT + 5, 0 },
    { ICON_NUMACT + 6, 0 },
    { ICON_NUMACT + 7, 0 },
    { ICON_NUMACT + 8, 0 },
    { ICON_NUMACT + 9, 0 }
};
ui_icongrp_t actions_sel = { &actions_action, 0, 9, actions_selelems };

/**
 * The form
 */
ui_form_t actions_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_REMOVE, actions_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, actions_save },
    { FORM_ICONGRP, 96, 0, 9*24-4, 20, 0, 0, &actions_sel, NULL },
    { FORM_CMDPAL, 0, 54, 40, 0, 0, 0, &actions_pal, NULL },
    /* 4 */
    { FORM_COMMANDS, 10, 54, 0, 0, 0, 0, &actions_cmd[0], NULL },
    { FORM_COMMANDS, 10, 54, 0, 0, 0, 0, &actions_cmd[1], NULL },
    { FORM_COMMANDS, 10, 54, 0, 0, 0, 0, &actions_cmd[2], NULL },
    { FORM_COMMANDS, 10, 54, 0, 0, 0, 0, &actions_cmd[3], NULL },
    { FORM_COMMANDS, 10, 54, 0, 0, 0, 0, &actions_cmd[4], NULL },
    { FORM_COMMANDS, 10, 54, 0, 0, 0, 0, &actions_cmd[5], NULL },
    { FORM_COMMANDS, 10, 54, 0, 0, 0, 0, &actions_cmd[6], NULL },
    { FORM_COMMANDS, 10, 54, 0, 0, 0, 0, &actions_cmd[7], NULL },
    { FORM_COMMANDS, 10, 54, 0, 0, 0, 0, &actions_cmd[8], NULL },
    /* 13 */
    { FORM_INPUT, 0, 30, 200, 20, 0, LANG_NAME, &actions_nameinp, NULL },
    { FORM_COMMENT, 14, 56, 0, 0, 0, 0, &actions_title, NULL },
    /* 15 */
    { FORM_SELECT, 10, 30, 200, 20, 0, ACTIONS_RANGED, &actions_range[0], NULL },
    { FORM_SELECT, 10, 30, 200, 20, 0, ACTIONS_RANGED, &actions_range[1], NULL },
    { FORM_SELECT, 10, 30, 200, 20, 0, ACTIONS_RANGED, &actions_range[2], NULL },
    { FORM_SELECT, 10, 30, 200, 20, 0, ACTIONS_RANGED, &actions_range[3], NULL },
    { FORM_SELECT, 10, 30, 200, 20, 0, ACTIONS_RANGED, &actions_range[4], NULL },
    { FORM_SELECT, 10, 30, 200, 20, 0, ACTIONS_RANGED, &actions_range[5], NULL },
    { FORM_SELECT, 10, 30, 200, 20, 0, ACTIONS_RANGED, &actions_range[6], NULL },
    { FORM_SELECT, 10, 30, 200, 20, 0, ACTIONS_RANGED, &actions_range[7], NULL },
    { FORM_SELECT, 10, 30, 200, 20, 0, ACTIONS_RANGED, &actions_range[8], NULL },

    { FORM_LAST }
};

/**
 * Exit actions window
 */
void actions_exit(int tab)
{
    int i;

    (void)tab;
    for(i = 0; i < 9; i++)
        ui_cmd_free(&actions_cmd[i]);
    actions_action = 0;
}

/**
 * Enter actions window
 */
void actions_init(int tab)
{
    int i, j, k;

    actions_exit(tab);
    actions_form[1].param = lang[LANG_SAVE];
    actions_form[1].w = ui_textwidth(actions_form[1].param) + 40;
    if(actions_form[1].w < 200) actions_form[1].w = 200;
    for(i = j = 0; i < project.numattrs; i++) {
        k = ui_textwidth(project.attrs[i]);
        if(k > j) j = k;
    }
    for(i = 0; i < 9; i++) {
        actions_range[i].val = -1;
        actions_range[i].first = LANG_NONE;
        actions_range[i].opts = project.attrs;
        actions_form[15 + i].w = j + 24;
    }
    actions_load(0);
    project_loadactions();
    if(strlen(lang[ACTIONS_USERDEF]) > sizeof(actions_title) - 4)
        lang[ACTIONS_USERDEF][sizeof(actions_title) - 4] = 0;
}

/**
 * Resize the view
 */
void actions_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    actions_form[0].y = actions_form[1].y = actions_form[2].y = h - 48;
    actions_form[1].x = w - 20 - actions_form[1].w;
    actions_form[3].h = h - 89 - 24;
    actions_form[3].x = w - 48;
    for(i = 0; i < 9; i++) {
        actions_form[4 + i].h = h - 89 - 24;
        actions_form[4 + i].w = w - 62;
    }
    actions_form[13].x = w - 10 - actions_form[13].w;
}

/**
 * View layer
 */
void actions_redraw(int tab)
{
    int i;

    (void)tab;
    ui_form = actions_form;
    sprintf(actions_title, "%s #%d", lang[ACTIONS_USERDEF], actions_action + 1);
    for(i = 0; i < 9; i++)
        ui_form[4 + i].inactive = ui_form[15 + i].inactive = (i == actions_action ? 0 : 2);
    actions_nameinp.str = actions_name[actions_action];
}

/**
 * Delete actions commands hook
 */
void actions_delete(void *data)
{
    (void)data;
    sprintf(projfn, "%s" SEP "%s" SEP "actions.cfg", project.id, project_dirs[PROJDIRS_GAME]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("actions_delete: removing %s\n", projdir);
        remove(projdir);
        actions_exit(SUBMENU_ACTIONS);
    }
}

/**
 * Save actions commands hook
 */
void actions_save(void *data)
{
    int i;
    FILE *f;

    (void)data;
    f = project_savefile(0, project_dirs[PROJDIRS_GAME], "actions", "cfg", PROJMAGIC_ACTION, "actions_save");
    if(f) {
        for(i = 0; i < 9; i++) {
            fprintf(f, "e action%u %s ", i + 1, actions_name[i][0] ? actions_name[i] : "-");
            project_wridx(f, actions_range[i].val, project.attrs);
            fprintf(f, " ");
            project_wrscript(f, &actions_cmd[i]);
        }
        fclose(f);
        project_loadactions();
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVEACT]);
}

/**
 * Load actions commands
 */
void actions_load(int namesonly)
{
    int i;
    char *str, *s;

    str = s = project_loadfile(project_dirs[PROJDIRS_GAME], "actions", "cfg", PROJMAGIC_ACTION, "actions_load");
    if(str) {
        for(i = 0; i < 9; i++) {
            actions_range[i].val = -1;
            actions_name[i][0] = 0;
            ui_cmd_free(&actions_cmd[i]);
        }
        while(*s) {
            s = project_skipnl(s);
            if(!memcmp(s, "e action", 8) && s[8] >= '1' && s[8] <= '9') {
                i = s[8] - '1';
                s = project_getstr2(s + 9, actions_name[i], 0, sizeof(actions_name[0]));
                if(namesonly)
                    s = project_skipscript(s);
                else {
                    s = project_getidx(s, &actions_range[i].val, project.attrs, -1);
                    s = project_getscript(s, &actions_cmd[i]);
                }
            }
        }
        free(str);
    } else
        ui_status(1, lang[ERR_LOADACT]);
}

/**
 * Get references
 */
void actions_ref(tngctx_t *ctx)
{
    int i;

    if(!ctx) return;
    actions_load(0);
    for(i = 0; i < 9; i++)
        ui_cmd_ref(ctx, &actions_cmd[i].root, 0);
    actions_exit(SUBMENU_ACTIONS);
}

/**
 * Save tng
 */
int actions_totng(tngctx_t *ctx)
{
    ui_bc_t bc;
    uLongf cl;
    uint8_t *comp;
    char *name;
    int i, l = 9, err;

    if(!ctx) return 1;
    actions_load(0);
    while(l > 0 && !actions_cmd[l - 1].root.len) l--;
    ctx->hdr.numact = l;
    if(l) {
        tng_section(ctx, TNG_SECTION_ACTIONS);
        for(i = 0; i < l; i++) {
            memset(&bc, 0, sizeof(bc)); err = 0;
            ui_cmd_tobc(ctx, &bc, &actions_cmd[i].root, 0, &err);
            if(err) {
                if(bc.data) free(bc.data);
                ui_switchtab(SUBMENU_ACTIONS);
                actions_action = i;
                ui_status(1, lang[ERR_SCRIPT]);
                return 0;
            }
            name = actions_range[i].val >= 0 && project.attrs ? project.attrs[actions_range[i].val] : NULL;
            if(!bc.len) {
                tng_desc(ctx, name, 0);
            } else {
                cl = compressBound(bc.len);
                comp = (uint8_t*)main_alloc(cl);
                compress2(comp, &cl, bc.data, bc.len, 9);
                if(cl) {
                    tng_desc(ctx, name, cl);
                    tng_asset_int(ctx, comp, cl);
                } else
                    free(comp);
                free(bc.data);
            }
        }
    }
    actions_exit(SUBMENU_ACTIONS);
    return 1;
}

/**
 * Read from tng
 */
int actions_fromtng(tngctx_t *ctx)
{
    FILE *f;
    tng_asset_desc_t *asset;
    uint8_t *buf;
    int i, l, len;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_ACTIONS; i++);
    if(i >= ctx->numtbl) return 1;
    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;
    if(len > 9) len = 9;
    f = project_savefile(0, project_dirs[PROJDIRS_GAME], "actions", "cfg", PROJMAGIC_ACTION, "actions_fromtng");
    if(f) {
        for(i = 0; i < len; i++, asset++) {
            fprintf(f, "e action%u - %s ", i + 1, asset->name && asset->name < TNG_SECTION_SIZE(&ctx->tbl[0]) ?
                (char*)ctx->sts + asset->name : "-");
            buf = tng_get_asset(ctx, asset->offs, asset->size, &l);
            if(buf) {
                tng_wrscript(ctx, buf, l, f);
                free(buf);
            } else
                fprintf(f, "{}\r\n");
        }
        fclose(f);
        return 1;
    }
    return 0;
}
