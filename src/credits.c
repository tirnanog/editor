/*
 * tnge/credits.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Credits and attributions window
 *
 */

#include "main.h"

#define NUMCATS (NUMTEXTS - GAME_CRD_GAME)

void credits_load(void *data);
void credits_save(void *data);
void credits_delete(void *data);
void credits_preview(void *data);
void credits_erase(void *data);
void credits_add(void *data);

const char *credits_code[] = { "g", "c", "d", "a", "f", "m", "s", "v", "t", "p", NULL };
uint32_t credits_hdrcolor = 0xFFFFFFFF, credits_fntcolor = 0xFFF0F0F0;
int credits_tab, credits_hdrstyle = 0, credits_fntstyle = 0;
char *credits_catopts[NUMCATS + 1], credits_name[PROJ_TITLEMAX];
ui_select_t credits_backmus = { -1, LANG_NONE, NULL };
ui_sprsel_t credits_backimg = { -1, 4, 25, 1 };
ui_icontgl_t credits_hdrbold = { ICON_BOLD, SSFN_STYLE_BOLD, &credits_hdrstyle };
ui_icontgl_t credits_hdritalic = { ICON_ITALIC, SSFN_STYLE_ITALIC, &credits_hdrstyle };
ui_num_t credits_hdrsize = { 32, 8, 192, 1 };
ui_select_t credits_hdrfont = { -1, LANG_DEF, NULL };
ui_icontgl_t credits_fntbold = { ICON_BOLD, SSFN_STYLE_BOLD, &credits_fntstyle };
ui_icontgl_t credits_fntitalic = { ICON_ITALIC, SSFN_STYLE_ITALIC, &credits_fntstyle };
ui_num_t credits_fntsize = { 16, 8, 192, 1 };
ui_select_t credits_fntfont = { -1, LANG_DEF, NULL };
ui_select_t credits_catsel = { 0, 0, credits_catopts };
ui_input_t credits_nameinp = { INP_NAME, sizeof(credits_name), credits_name };
ui_table_t credits_tables[NUMCATS] = { 0 };

/**
 * The form
 */
ui_form_t credits_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, CREDITS_DELETE, (void*)ICON_REMOVE, credits_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, credits_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, CREDITS_PREVIEW, (void*)ICON_PVIEW, credits_preview },
    /* 3 */
    { FORM_TEXT, 10, 29, 0, 18, 0, 0, NULL, NULL },
    { FORM_MUSSEL, 0, 29, 0, 20, 0, CREDITS_BACKMUS, &credits_backmus, NULL },
    { FORM_SPRITE, 0, 29, 0, 20, 0, CREDITS_BACKIMG, &credits_backimg, NULL },
    { FORM_ICON, 0, 31, 16, 16, 0, 0, (void*)ICON_BACKGR, NULL },
    /* 7 */
    { FORM_TEXT, 10, 53, 0, 18, 0, 0, NULL, NULL },
    { FORM_COLOR, 0, 53, 18, 18, 0, FONTS_COLOR, &credits_hdrcolor, NULL },
    { FORM_ICONTGL, 0, 53, 18, 18, 0, FONTS_BOLD, &credits_hdrbold, NULL },
    { FORM_ICONTGL, 0, 53, 18, 18, 0, FONTS_ITALIC, &credits_hdritalic, NULL },
    { FORM_NUM, 0, 53, 42, 18, 0, FONTS_SIZE, &credits_hdrsize, NULL },
    { FORM_SELECT, 0, 53, 0, 18, 0, FONTS_FAMILY, &credits_hdrfont, NULL },
    /* 13 */
    { FORM_TEXT, 10, 77, 0, 18, 0, 0, NULL, NULL },
    { FORM_COLOR, 0, 77, 18, 18, 0, FONTS_COLOR, &credits_fntcolor, NULL },
    { FORM_ICONTGL, 0, 77, 18, 18, 0, FONTS_BOLD, &credits_fntbold, NULL },
    { FORM_ICONTGL, 0, 77, 18, 18, 0, FONTS_ITALIC, &credits_fntitalic, NULL },
    { FORM_NUM, 0, 77, 42, 18, 0, FONTS_SIZE, &credits_fntsize, NULL },
    { FORM_SELECT, 0, 77, 0, 18, 0, FONTS_FAMILY, &credits_fntfont, NULL },
    /* 19 */
    { FORM_TEXT, 10, 101, 0, 18, 0, 0, NULL, NULL },
    { FORM_SELECT, 0, 101, 0, 18, 0, 0, &credits_catsel, NULL },
    /* 21 */
    { FORM_TEXT, 10, 125, 0, 18, 0, 0, NULL, NULL },
    /* 22 */
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CREDITS_ERASE, (void*)ICON_REMOVE, credits_erase },
    { FORM_INPUT, 0, 0, 0, 20, 0, CREDITS_ANAME, &credits_nameinp, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CREDITS_ADD, (void*)ICON_ADD, credits_add },
    /* 25 */
    { FORM_TABLE, 0, 125, 0, 0, 0, 0, &credits_tables[0], NULL },
    { FORM_TABLE, 0, 125, 0, 0, 0, 0, &credits_tables[1], NULL },
    { FORM_TABLE, 0, 125, 0, 0, 0, 0, &credits_tables[2], NULL },
    { FORM_TABLE, 0, 125, 0, 0, 0, 0, &credits_tables[3], NULL },
    { FORM_TABLE, 0, 125, 0, 0, 0, 0, &credits_tables[4], NULL },
    { FORM_TABLE, 0, 125, 0, 0, 0, 0, &credits_tables[5], NULL },
    { FORM_TABLE, 0, 125, 0, 0, 0, 0, &credits_tables[6], NULL },
    { FORM_TABLE, 0, 125, 0, 0, 0, 0, &credits_tables[7], NULL },
    { FORM_TABLE, 0, 125, 0, 0, 0, 0, &credits_tables[8], NULL },
    { FORM_TABLE, 0, 125, 0, 0, 0, 0, &credits_tables[9], NULL },
    { FORM_LAST }
};

/**
 * Exit credits window
 */
void credits_exit(int tab)
{
    int i;

    (void)tab;
    memset(&credits_name, 0, sizeof(credits_name));
    credits_hdrcolor = 0xFFFFFFFF; credits_fntcolor = 0xFFF0F0F0;
    credits_hdrstyle = 0; credits_fntstyle = 0;
    credits_backmus.val = -1;
    credits_backimg.val = -1;
    credits_hdrsize.val = 32;
    credits_hdrfont.val = -1;
    credits_fntsize.val = 16;
    credits_fntfont.val = -1;
    credits_catsel.val = 0;
    for(i = 0; i < NUMCATS; i++)
        project_freedir((char***)&credits_tables[i].data, &credits_tables[i].num);
}

/**
 * Enter credits window
 */
void credits_init(int tab)
{
    int i;

    credits_exit(tab);
    credits_form[1].param = lang[LANG_SAVE];
    credits_form[1].w = ui_textwidth(credits_form[1].param) + 40;
    if(credits_form[1].w < 200) credits_form[1].w = 200;
    credits_form[3].param = lang[CREDITS_BACK];
    credits_tab = ui_textwidth(credits_form[3].param);
    credits_form[7].param = lang[CREDITS_HEADER];
    i = ui_textwidth(credits_form[7].param); if(i > credits_tab) credits_tab = i;
    credits_form[13].param = lang[CREDITS_FONT];
    i = ui_textwidth(credits_form[13].param); if(i > credits_tab) credits_tab = i;
    credits_form[19].param = lang[CREDITS_CATEGORY];
    i = ui_textwidth(credits_form[19].param); if(i > credits_tab) credits_tab = i;
    credits_form[21].param = lang[CREDITS_AUTHORS];
    i = ui_textwidth(credits_form[21].param); if(i > credits_tab) credits_tab = i;
    credits_tab += 20;
    credits_form[4].x = credits_tab + 20;
    credits_form[8].x = credits_form[14].x = credits_form[20].x = credits_form[22].x = credits_tab;
    credits_form[9].x = credits_form[15].x = credits_tab + 24;
    credits_form[10].x = credits_form[16].x = credits_tab + 24 + 24;
    credits_form[11].x = credits_form[17].x = credits_tab + 24 + 24 + 24;
    credits_form[12].x = credits_form[18].x = credits_tab + 24 + 24 + 24 + 44;
    credits_form[23].x = credits_tab + 32;
    credits_hdrfont.opts = credits_fntfont.opts = project.fonts;
    for(i = 0; i < NUMCATS; i++) {
        /* make the language checker happy:
         * GAME_CRD_ART, GAME_CRD_DESIGN, GAME_CRD_ANIMS, GAME_CRD_FONTS, GAME_CRD_MUSIC, GAME_CRD_SOUND,
         * GAME_CRD_VOICE, GAME_CRD_TR, GAME_CRD_PROG */
        credits_catopts[i] = lang[i + GAME_CRD_GAME];
        credits_tables[i].row = 18;
        credits_tables[i].nodata = CREDITS_NONE;
        credits_tables[i].entsize = sizeof(char*);
        credits_form[25 + i].x = credits_tab;
    }
    credits_catopts[i] = NULL;
    credits_load(NULL);
}

/**
 * Resize the view
 */
void credits_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    credits_form[0].y = credits_form[1].y = credits_form[2].y = h - 48;
    credits_form[1].x = w - 20 - credits_form[1].w;
    credits_form[2].x = credits_form[1].x - 52;
    credits_form[4].w = credits_form[5].w = (scr_w - 60 - credits_tab) / 2;
    credits_form[5].x = credits_form[4].x + credits_form[4].w + 30;
    credits_form[6].x = credits_form[5].x - 18;
    credits_form[12].w = credits_form[18].w = scr_w - 10 - credits_form[12].x;
    credits_form[20].w = scr_w - 10 - credits_form[20].x;
    credits_form[23].w = scr_w - 40 - credits_form[23].x;
    credits_form[24].x = scr_w - 30;
    credits_form[22].y = credits_form[23].y = credits_form[24].y = h - 84;
    for(i = 0; i < NUMCATS; i++)
        ui_table_resize(&credits_form[25 + i], scr_w - 10 - credits_tab, h - 214);
}

/**
 * View layer
 */
void credits_redraw(int tab)
{
    int i;

    (void)tab;
    ui_form = credits_form;
    for(i = 0; i < NUMCATS; i++)
        credits_form[25 + i].inactive = 2;
    credits_form[25 + credits_catsel.val].inactive = 0;
    credits_form[22].inactive = credits_tables[credits_catsel.val].val < 0;
    credits_form[24].inactive = !credits_name[0];
}

/**
 * Delete credits
 */
void credits_delete(void *data)
{
    (void)data;
    sprintf(projfn, "%s" SEP "%s" SEP "credits.cfg", project.id, project_dirs[PROJDIRS_UI]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("credits_delete: removing %s\n", projdir);
        remove(projdir);
        credits_exit(SUBMENU_ELEMENTS);
    }
}

/**
 * Remove an author from current category
 */
void credits_erase(void *data)
{
    char **list = (char**)credits_tables[credits_catsel.val].data;

    (void)data;
    if(credits_catsel.val < 0 || credits_tables[credits_catsel.val].val < 0) return;
    if(!list || credits_tables[credits_catsel.val].val < 0 || credits_tables[credits_catsel.val].val >=
        credits_tables[credits_catsel.val].num) return;
    if(list[credits_tables[credits_catsel.val].val]) free(list[credits_tables[credits_catsel.val].val]);
    memcpy(&list[credits_tables[credits_catsel.val].val], &list[credits_tables[credits_catsel.val].val + 1],
        (credits_tables[credits_catsel.val].num - credits_tables[credits_catsel.val].val) * sizeof(char*));
    credits_tables[credits_catsel.val].num--;
    if(credits_tables[credits_catsel.val].val >= credits_tables[credits_catsel.val].num)
        credits_tables[credits_catsel.val].val = credits_tables[credits_catsel.val].num - 1;
    if(!credits_tables[credits_catsel.val].num) { free(list); credits_tables[credits_catsel.val].data = NULL; }
}

/**
 * Helper to sort authors
 */
int credits_cmp(const void *a, const void *b)
{
    return strcasecmp(*((const char**)a), *((const char**)b));
}

/**
 * Add an author to current category
 */
void credits_add(void *data)
{
    char **list;
    int i;

    (void)data;
    if(credits_catsel.val < 0) return;
    list = (char**)credits_tables[credits_catsel.val].data;
    for(i = 0; list && list[i] && i < credits_tables[credits_catsel.val].num; i++)
        if(!strcmp(list[i], credits_name)) return;
    list = (char**)realloc(list, (i + 2) * sizeof(char*));
    credits_tables[credits_catsel.val].data = list;
    if(!list) main_error(ERR_MEM);
    list[i + 1] = NULL;
    list[i] = (char*)main_alloc(strlen(credits_name) + 1);
    strcpy(list[i], credits_name);
    credits_tables[credits_catsel.val].num++;
    qsort(list, credits_tables[credits_catsel.val].num, sizeof(char*), credits_cmp);
    for(i = 0; list && list[i] && i < credits_tables[credits_catsel.val].num && strcmp(list[i], credits_name); i++);
    credits_tables[credits_catsel.val].val = i;
    memset(&credits_name, 0, sizeof(credits_name));
}

/**
 * Load credits
 */
void credits_load(void *data)
{
    char *str, *s;
    int i;

    (void)data;
    str = project_loadfile(project_dirs[PROJDIRS_UI], "credits", "cfg", PROJMAGIC_CREDITS, "credits_load");
    if(str) {
        s = project_skipnl(str);
        s = project_getidx(s, &credits_backmus.val, project.music, -1);
        s = project_getsprite(s, &credits_backimg);
        s = project_skipnl(s);
        s = project_getcolor(s, &credits_hdrcolor);
        s = project_getfont(s, &credits_hdrstyle, &credits_hdrsize.val, &credits_hdrfont.val);
        s = project_skipnl(s);
        s = project_getcolor(s, &credits_fntcolor);
        s = project_getfont(s, &credits_fntstyle, &credits_fntsize.val, &credits_fntfont.val);
        s = project_skipnl(s);
        while(*s) {
            s = project_getidx(s, &credits_catsel.val, (char**)credits_code, -1);
            s = project_getstr2(s, credits_name, sizeof(credits_name), 2);
            s = project_skipnl(s);
            credits_add(NULL);
        }
        free(str);
    } else
        ui_status(1, lang[ERR_LOADUI]);
    credits_catsel.val = 0;
    for(i = 0; i < NUMCATS; i++)
        credits_tables[i].val = -1;
}

/**
 * Save credits
 */
void credits_save(void *data)
{
    FILE *f;
    char **list;
    int i, j;

    (void)data;
    f = project_savefile(0, project_dirs[PROJDIRS_UI], "credits", "cfg", PROJMAGIC_CREDITS, "credits_save");
    if(f) {
        project_wridx(f, credits_backmus.val, project.music); fprintf(f, " ");
        project_wrsprite(f, &credits_backimg); fprintf(f, "\r\n");
        fprintf(f, "%08X ", credits_hdrcolor);
        project_wrfont(f, credits_hdrstyle, credits_hdrsize.val, credits_hdrfont.val);
        fprintf(f, "\r\n");
        fprintf(f, "%08X ", credits_fntcolor);
        project_wrfont(f, credits_fntstyle, credits_fntsize.val, credits_fntfont.val);
        fprintf(f, "\r\n");
        for(i = 0; i < NUMCATS; i++) {
            list = (char**)credits_tables[i].data;
            for(j = 0; list && j < credits_tables[i].num; j++)
                fprintf(f, "%s %s\r\n", credits_code[i], list[j]);
        }
        fclose(f);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVEUI]);
}


/**
 * Preview credits
 */
void credits_preview(void *data)
{
    Mix_Music *music = NULL;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *texture = NULL, *bg = NULL, *bgm = NULL;
    SDL_Rect src, dst, bgsrc, bgdst;
    ui_font_t hdr, fnt;
    ssfn_buf_t buf = { 0 };
    int wf = SDL_GetWindowFlags(window), i, j, k, th, w, h, p, b, x, y, noaudio = 0;
    char *str, *S, **list;
    uint8_t *pixels1, *pixels2;
    ui_sprite_t *s;
    int nocrop = 0;

    (void)data;
    if(verbose) printf("credits_preview: started\n");

    str = project_loadfile(project_dirs[PROJDIRS_UI], "mainmenu", "cfg", PROJMAGIC_MAINMENU, "credits_preview");
    if(str) {
        S = project_skipnl(str);
        S = project_skipnl(S);
        S = project_skipnl(S);
        S = project_skipnl(S);
        S = project_skipnl(S);
        S = project_skipnl(S);
        S = project_skipnl(S);
        S = project_getint(S, &nocrop, 0, 1);
        free(str);
    }

    ui_font_load(&hdr, credits_hdrstyle | SSFN_STYLE_UNDERLINE, credits_hdrsize.val, credits_hdrfont.val);
    ui_font_load(&fnt, credits_fntstyle, credits_fntsize.val, credits_fntfont.val);
    for(j = th = 0; j < NUMCATS; j++)
        if(credits_tables[j].num)
            th += 2 * credits_hdrsize.val + (credits_tables[j].num + 1) * credits_fntsize.val + 4;
    if(th > 4096) th = 4096;

    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, th);
    if(texture) {
        SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(texture, NULL, (void**)&buf.ptr, &p);
        memset(buf.ptr, 0, p * th);
        buf.p = p;
        buf.w = dm.w;
        buf.h = th;
        for(j = y = 0; j < NUMCATS && y < th; j++) {
            if(!credits_tables[j].num) continue;
            str = credits_catopts[j];
            ssfn_bbox(&hdr.ctx, str, &w, &h, &p, &b);
            buf.fg = 0x7F000000;
            buf.x = (dm.w - w) / 2 + 4; buf.y = y + b + 4;
            hdr.ctx.style &= ~SSFN_STYLE_A;
            while(*str && ((i = ssfn_render(&hdr.ctx, &buf, str)) > 0 || i == SSFN_ERR_NOGLYPH)) str += (i < 1 ? 1 : i);
            str = credits_catopts[j];
            buf.fg = credits_hdrcolor;
            buf.x = (dm.w - w) / 2; buf.y = y + b;
            while(*str && ((i = ssfn_render(&hdr.ctx, &buf, str)) > 0 || i == SSFN_ERR_NOGLYPH)) str += (i < 1 ? 1 : i);
            hdr.ctx.style |= SSFN_STYLE_A;
            y += credits_hdrsize.val + credits_fntsize.val;
            list = credits_tables[j].data;
            for(k = 0; k < credits_tables[j].num && y + credits_fntsize.val < th; k++, y += credits_fntsize.val) {
                str = list[k];
                ssfn_bbox(&fnt.ctx, str, &w, &h, &p, &b);
                buf.fg = 0x7F000000;
                buf.x = (dm.w - w) / 2; buf.y = y + b;
                fnt.ctx.style &= ~SSFN_STYLE_A;
                while(*str && ((i = ssfn_render(&fnt.ctx, &buf, str)) > 0 || i == SSFN_ERR_NOGLYPH)) str += (i < 1 ? 1 : i);
                str = list[k];
                buf.fg = credits_fntcolor;
                buf.x = (dm.w - w) / 2; buf.y = y + b;
                while(*str && ((i = ssfn_render(&fnt.ctx, &buf, str)) > 0 || i == SSFN_ERR_NOGLYPH)) str += (i < 1 ? 1 : i);
                fnt.ctx.style |= SSFN_STYLE_A;
            }
            y += credits_hdrsize.val;
        }
        SDL_UnlockTexture(texture);
    }
    dst.x = bgdst.y = bgdst.x = bgsrc.x = bgsrc.y = 0; dst.y = bgdst.h = dm.h; dst.w = bgdst.w = dm.w; dst.h = 1;
    src.x = 0; src.y = 0; src.w = dm.w; src.h = 1;

    if(credits_backimg.val >= 0) {
        s = spr_getsel(&credits_backimg, 0);
        spr_texture(s, &bg);
        if(!nocrop) {
            ui_fit(s->w, s->h, dm.w, dm.h, &bgsrc.w, &bgsrc.h);
            bgsrc.x = (s->w - bgsrc.w) / 2; bgsrc.y = (s->h - bgsrc.h) / 2;
        } else {
            ui_fit(dm.w, dm.h, s->w, s->h, &bgdst.w, &bgdst.h);
            bgsrc.w = s->w; bgsrc.h = s->h;
        }
        bgdst.x = (dm.w - bgdst.w) / 2;
        bgdst.y = (dm.h - bgdst.h) / 2;
        dst.y = bgdst.y + bgdst.h;
        x = s->w; y = s->h;
    } else {
        x = bgdst.w; y = bgdst.h;
    }
    bgm = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, x, y);
    if(bgm) {
        SDL_SetTextureBlendMode(bgm, SDL_BLENDMODE_BLEND);
        if(bg) SDL_LockTexture(bg, NULL, (void**)&pixels1, &b);
        SDL_LockTexture(bgm, NULL, (void**)&pixels2, &p);
        memset(pixels2, 0, p * y);
        if(bg) {
            memcpy(pixels2, pixels1, p * 32);
            memcpy(pixels2 + p * (y - 32), pixels1 + b * (y - 32), p * 32);
        }
        for(j = 0; j < 32; j++)
            for(i = 0; i < p; i += 4) {
                pixels2[j * p + i + 3] = (32 - j) * 255 / 32;
                pixels2[(y - 32 + j) * p + i + 3] = j * 255 / 32;
            }
        SDL_UnlockTexture(bgm);
        if(bg) SDL_UnlockTexture(bg);
    }

    if(Mix_OpenAudio(FREQ, AUDIO_S16SYS, 2, 4096) < 0) { ui_status(1, lang[ERR_AUDIO]); noaudio = 1; }

    if(credits_backmus.val >= 0 && !noaudio) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA],
            project.music[credits_backmus.val]);
        if(verbose) printf("credits_preview: loading music %s\n", projdir);
        music = Mix_LoadMUS(projdir);
        if(music) {
            Mix_VolumeMusic(MIX_MAX_VOLUME);
            Mix_PlayMusic(music, -1);
        } else
            ui_status(1, lang[ERR_LOADSND]);
    }

    while(src.h > 0) {
        /* adjust scroll */
        if(dst.y > bgdst.y) {
            dst.y--; src.h = dst.h = (bgdst.y + bgdst.h - dst.y > th ? th : bgdst.y + bgdst.h - dst.y);
        } else {
            src.y++; src.h = dst.h = (src.y + bgdst.h > th ? th - src.y : bgdst.h);
        }
        /* events */
        if(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
                SDL_PushEvent(&event); goto cleanup;
            }
            if(event.type == SDL_KEYUP || event.type == SDL_MOUSEBUTTONUP) goto cleanup;
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        if(bg) SDL_RenderCopy(renderer, bg, &bgsrc, &bgdst);
        if(texture) SDL_RenderCopy(renderer, texture, &src, &dst);
        if(bgm) SDL_RenderCopy(renderer, bgm, &bgsrc, &bgdst);
        SDL_RenderPresent(renderer);
        SDL_Delay(33);
    }

cleanup:
    if(!noaudio) {
        if(music) { Mix_HaltMusic(); Mix_FreeMusic(music); }
        Mix_CloseAudio();
    }
    ui_font_free(&hdr);
    ui_font_free(&fnt);
    if(texture) SDL_DestroyTexture(texture);
    if(bgm) SDL_DestroyTexture(bgm);
    if(bg) SDL_DestroyTexture(bg);
    if(verbose) printf("credits_preview: stopped\n");
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Save tng
 */
int credits_totng(tngctx_t *ctx)
{
    char **list;
    int i, j, k;

    if(!ctx) return 1;
    credits_load(NULL);
    tng_section(ctx, TNG_SECTION_CREDITS);
    tng_ref(ctx, TNG_IDX_MUS, credits_backmus.val);
    tng_idx(ctx, project.music, credits_backmus.val);
    tng_sprite(ctx, credits_backimg.cat, credits_backimg.val);
    tng_data(ctx, &credits_hdrcolor, 4);
    tng_font(ctx, credits_hdrstyle, credits_hdrsize.val, credits_hdrfont.val);
    tng_data(ctx, &credits_fntcolor, 4);
    tng_font(ctx, credits_fntstyle, credits_fntsize.val, credits_fntfont.val);
    for(i = 0; i < NUMCATS; i++) {
        list = (char**)credits_tables[i].data;
        if(!list || credits_tables[i].num < 1) continue;
        tng_data(ctx, (void*)credits_code[i], 1);
        tng_data(ctx, &credits_tables[i].num, 2);
        for(j = 0; list && j < credits_tables[i].num; j++) {
            k = tng_str(ctx, list[j]);
            tng_data(ctx, &k, 3);
        }
        project_freedir((char***)&credits_tables[i].data, &credits_tables[i].num);
    }
    return 1;
}

/**
 * Read from tng
 */
int credits_fromtng(tngctx_t *ctx)
{
    FILE *f;
    uint8_t *buf, *end;
    int i, l, c = 0, len;

    if(!ctx || !ctx->tbl || !ctx->numtbl) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_CREDITS; i++);
    if(i >= ctx->numtbl) return 1;
    buf = (uint8_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]);
    end = buf + len;
    if(len < 24) return 0;
    f = project_savefile(0, project_dirs[PROJDIRS_UI], "credits", "cfg", PROJMAGIC_CREDITS, "credits_fromtng");
    if(f) {
        buf = tng_wridx(ctx, buf, f); fprintf(f, " ");
        buf = tng_wrsprite(ctx, buf, f); fprintf(f, "\r\n");
        fprintf(f, "%08X ", *((uint32_t*)buf)); buf += 4;
        buf = tng_wrfont(ctx, buf, f); fprintf(f, "\r\n");
        fprintf(f, "%08X ", *((uint32_t*)buf)); buf += 4;
        buf = tng_wrfont(ctx, buf, f); fprintf(f, "\r\n");
        while(buf < end) {
            c = *buf++;
            l = (buf[1] << 8) | buf[0]; buf += 2;
            for(i = 0; i < l && buf < end; i++) {
                fprintf(f, "%c ", c);
                buf = tng_wridx(ctx, buf, f); fprintf(f, "\r\n");
            }
        }
        fclose(f);
        return 1;
    }
    return 0;
}
