/*
 * tnge/newsprite.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Import spritesheet window
 *
 */

#include "main.h"

#define IMPICONSIZE 64
#define GRIDMASKALPHA 0x3F000000

/* built-in layout */
#define BUILTINLYT "TNG-Base"
typedef struct {
    char *name;                     /* name of the animation */
    int type;                       /* 0 - play once, 1 - forth and back, 2 - loop */
    int ndir;                       /* number of directions */
    int nframe;                     /* number of frames */
    int x, y;                       /* start position in grid cells */
} builtinlyt_t;
builtinlyt_t tngbase[] = {
    { "idle",   2, 4, 2, 0, 0 },
    { "hurt",   2, 4, 2, 2, 0 },
    { "block",  2, 4, 2, 4, 0 },
    { "walk",   2, 4, 6, 0, 4 },
    { "run",    2, 4, 6, 0, 8 },
    { "jump",   0, 4, 6, 0,12 },
    { "slash",  0, 4, 6, 6, 0 },
    { "thrust", 0, 4, 6, 6, 4 },
    { "bowing", 0, 4, 6, 6, 8 },
    { "cast",   0, 4, 6, 6,12 },
    { "push",   2, 4, 6,12, 0 },
    { "carry",  2, 4, 6,12, 4 },
    { "climb",  2, 1, 6,12, 8 },
    { "spawn",  0, 1, 6,12, 9 },
    { "die",    0, 1, 6,12,10 },
    { 0 },
};

extern int sprites_cat;
extern ui_icongrp_t sprites_cats;
char newsprite_lyt[FILENAME_MAX] = { 0 }, newsprite_spr[FILENAME_MAX] = { 0 }, newsprite_name[PROJ_NAMEMAX + 1] = { 0 };
char newsprite_type = 0, newsprite_dir = 2;
int newsprite_gridtgl = 0, newsprite_gridw, newsprite_gridh, newsprite_gridl, newsprite_gridt, newsprite_gridH, newsprite_gridV;
int newsprite_selsx, newsprite_selex, newsprite_selsy, newsprite_seley, newsprite_selast;
int newsprite_cropsx, newsprite_cropex, newsprite_cropsy, newsprite_cropey;
void newsprite_back(void *data) { (void)data; ui_switchtab(SUBMENU_SPRITES); }
void newsprite_load(void *data) { (void)data; newsprite_dir = 0; fileops_type = FILEOPS_SPRITE; ui_switchtab(SUBMENU_IMPORT); }
void newsprite_half(void *param);
void newsprite_double(void *param);
void newsprite_vflip(void *param);
void newsprite_hflip(void *param);
void newsprite_getlyts();
void newsprite_loadlyt(void *param);
void newsprite_savelyt(void *param);
void newsprite_chtype(void *param);
void newsprite_chdir(void *param);
void newsprite_chsel(void *param);
void newsprite_recalcgrid(void *param);
void newsprite_eraseall(void *data);
void newsprite_erase(void *data);
void newsprite_savesheet(void *data);
void newsprite_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void newsprite_rendercell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void newsprite_addfromgrid(ui_implist_t *imp, int sx, int sy, int ex, int ey, int sp);
void newsprite_addfromsel(void *data);
SDL_Texture *newsprite_sheet = NULL, *newsprite_grid = NULL, *newsprite_modelpreview = NULL;
ui_frames_t newsprite_frames = { 0 };

ui_table_t newsprite_table = { NULL, 0, IMPICONSIZE+22, 0, 0, 1, 0, 0, 0, 0, 0, -1, 0, sizeof(ui_implist_t), newsprite_drawcell,
    newsprite_rendercell, NULL, NULL };
ui_scrarea_t newsprite_scr = { 0 };
ui_numptr_t newsprite_numw = { &newsprite_gridw, 1, 1024, 1 };
ui_numptr_t newsprite_numh = { &newsprite_gridh, 1, 1024, 1 };
ui_numptr_t newsprite_numl = { &newsprite_gridl, 0, 1024, 1 };
ui_numptr_t newsprite_numt = { &newsprite_gridt, 0, 1024, 1 };
ui_numptr_t newsprite_numH = { &newsprite_gridH, 0, 1024, 1 };
ui_numptr_t newsprite_numV = { &newsprite_gridV, 0, 1024, 1 };
ui_combo_t newsprite_lytcombo = { { INP_ID, sizeof(newsprite_lyt), newsprite_lyt }, { 0, 0, NULL } };
ui_input_t newsprite_sprinp = { INP_ID, sizeof(newsprite_spr), newsprite_spr };
ui_input_t newsprite_nameinp = { INP_ID, sizeof(newsprite_name), newsprite_name };
ui_numptr_t newsprite_selstr = { &newsprite_frames.str, 1, 512, 1 };
ui_numptr_t newsprite_selcnt = { &newsprite_frames.cnt, 1, 31, 1 };

/**
 * The form
 */
ui_form_t newsprite_form[] = {
    { FORM_BUTTONICNP, 0, 0, 20, 20, 0, SPRITES_LIST, (void*)ICON_ADD, newsprite_back },
    { FORM_ICONGRP, 20, 0, 6*24-4, 20, 0, 0, &sprites_cats, NULL },
    { FORM_ZOOM, 0, 0, 68, 20, 0, 0, &newsprite_scr, NULL },
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, SPRITES_CLEAR, (void*)ICON_REMOVE, newsprite_free },
    { FORM_BUTTONICN, 56, 0, 20, 20, 1, SPRITES_MOVE, (void*)ICON_MOVE, NULL },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, newsprite_savesheet },
    /* 6 */
    { FORM_TABLE, 0, 59, IMPICONSIZE+46, 0, 0, 0, &newsprite_table, NULL },
    { FORM_ICON, 0, 59, 196, 0, 0, 0, (void*)ICON_INIMG, NULL },
    { FORM_SCRAREA, 10, 59, 0, 0, 0, SCRAREA_BOTH, &newsprite_scr, NULL },
    /* 9 */
    { FORM_ICONBTN, 10, 29, 20, 20, 0, SPRITES_LOAD, (void*)ICON_LOAD, newsprite_load },
    { FORM_PALETTE, 34, 29, 20, 20, 0, 0, (void*)ICON_PAL, NULL },
    { FORM_CHARBTN, 58, 29, 20, 20, 0, SPRITES_HALF, (void*)14, newsprite_half },
    { FORM_CHARBTN, 82, 29, 20, 20, 0, SPRITES_DOUBLE, (void*)15, newsprite_double },
    { FORM_CHARBTN, 106, 29, 20, 20, 0, SPRITES_VFLIP, (void*)11, newsprite_vflip },
    { FORM_CHARBTN, 130, 29, 20, 20, 0, SPRITES_HFLIP, (void*)12, newsprite_hflip },
    { FORM_BOOL, 158, 29, 16, 16, 0, SPRITES_GRID, &newsprite_gridtgl, newsprite_recalcgrid },
    { FORM_TEXT, 174, 31, 16, 16, 0, 0, "\x1A", NULL },
    { FORM_NUMPTR, 192, 29, 40, 18, 0, SPRITES_WIDTH, &newsprite_numw, newsprite_recalcgrid },
    { FORM_TEXT, 192+42, 31, 16, 16, 0, 0, "\x1B", NULL },
    { FORM_NUMPTR, 192+42+16, 29, 40, 18, 0, SPRITES_HEIGHT, &newsprite_numh, newsprite_recalcgrid },
    { FORM_TEXT, 192+42+16+42, 31, 16, 16, 0, 0, "\x1C", NULL },
    { FORM_NUMPTR, 192+42+16+42+16, 29, 40, 18, 0, SPRITES_LEFT, &newsprite_numl, newsprite_recalcgrid },
    { FORM_TEXT, 192+42+16+42+16+42, 31, 16, 16, 0, 0, "\x1D", NULL },
    { FORM_NUMPTR, 192+42+16+42+16+42+16, 29, 40, 18, 0, SPRITES_TOP, &newsprite_numt, newsprite_recalcgrid },
    { FORM_TEXT, 192+42+16+42+16+42+16+42, 31, 16, 16, 0, 0, "\x1E", NULL },
    { FORM_NUMPTR, 192+42+16+42+16+42+16+42+16, 29, 40, 18, 0, SPRITES_HORIZ, &newsprite_numH, newsprite_recalcgrid },
    { FORM_TEXT, 192+42+16+42+16+42+16+42+16+42, 31, 16, 16, 0, 0, "\x1F", NULL },
    { FORM_NUMPTR, 192+42+16+42+16+42+16+42+16+42+16, 29, 40, 18, 0, SPRITES_VERT, &newsprite_numV, newsprite_recalcgrid },
    { FORM_COMBO, 192+42+16+42+16+42+16+42+16+42+16+46, 29, 0, 18, 0, SPRITES_LAYOUT, &newsprite_lytcombo, newsprite_loadlyt },
    { FORM_ICONBTN, 0, 29, 20, 20, 0, SPRITES_SAVELYT, (void*)ICON_SAVE, newsprite_savelyt },
    { FORM_INPUT, 0, 29, 0, 20, 0, SPRITES_GRPNAME, &newsprite_sprinp, NULL },
    { FORM_ICONBTN, 0, 29, 20, 20, 0, SPRITES_ERASE, (void*)ICON_ERASE, newsprite_eraseall },
    /* 32 */
    { FORM_INPUTSB, 0, 0, IMPICONSIZE+30, 18, 0, SPRITES_NAME, &newsprite_nameinp, NULL },
    { FORM_CHRTBTN, 0, 0, 20, 20, 0, 0, &newsprite_type, newsprite_chtype },
    { FORM_CHRDBTN, 0, 0, 20, 20, 0, 0, &newsprite_dir, newsprite_chdir },
    { FORM_NUMPTRSB, 0, 60 + 21 + IMPICONSIZE/2 - 24, IMPICONSIZE - 4, 20, 0, SPRITES_START, &newsprite_selstr, newsprite_chsel },
    { FORM_NUMPTRSB, 0, 60 + 21 + IMPICONSIZE/2, IMPICONSIZE - 28, 20, 0, SPRITES_COUNT, &newsprite_selcnt, newsprite_chsel },
    { FORM_ICONBTN, 0, 60 + 21 + IMPICONSIZE/2, 20, 20, 0, SPRITES_ADDFRAMES, (void*)ICON_ADD, newsprite_addfromsel },
    { FORM_FRAMES, 10, 0, 0, 80, 0, 0, &newsprite_frames, NULL },
    /* 39 */
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, SPRITES_LOAD, (void*)ICON_LOAD, newsprite_load },
    { FORM_LAST }
};

/**
 * Draw table cell
 */
void newsprite_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    int i = 0;
    char tmp[8] = { 0 };
    ui_implist_t *imp = (ui_implist_t*)data;

    (void)hdr; (void)w; (void)h;
    if(sel == idx) {
        if(sel < 0) {
            newsprite_nameinp.str = newsprite_name;
            ui_form[33].param = &newsprite_type;
            ui_form[34].param = &newsprite_dir;
        } else {
            newsprite_nameinp.str = imp->name;
            ui_form[33].param = &(imp->type);
            ui_form[34].param = &(imp->dir);
            if(imp->type < 0) i = 2;
        }
        ui_form[32].x = x + 2; ui_form[32].y = y + 1;
        ui_form[33].x = ui_form[34].x = x + 8 + IMPICONSIZE;
        ui_form[33].y = y + 21 + IMPICONSIZE/2 - 24;
        ui_form[34].y = y + 21 + IMPICONSIZE/2;
        ui_form[32].inactive = ui_form[33].inactive = ui_form[34].inactive = i;
    }
    if(idx < 0) {
        if(sel != idx) {
            ui_text(dst, x + 4, y + 2, newsprite_name);
            tmp[0] = newsprite_type + 6; ui_text(dst, x + 10 + IMPICONSIZE, y + 23 + IMPICONSIZE/2 - 24, tmp);
            tmp[0] = newsprite_dir + 17; ui_text(dst, x + 10 + IMPICONSIZE, y + 23 + IMPICONSIZE/2, tmp);
        }
        if(ui_form[35].inactive)
            ui_icon(dst, x + 4 + (IMPICONSIZE - 16) / 2, y + 21 + (IMPICONSIZE - 16) / 2, ICON_ADD);
    } else {
        if(sel != idx && imp->type >= 0) {
            ui_text(dst, x + 4, y + 2, imp->name);
            tmp[0] = imp->type + 6; ui_text(dst, x + 10 + IMPICONSIZE, y + 23 + IMPICONSIZE/2 - 24, tmp);
            tmp[0] = imp->dir + 17; ui_text(dst, x + 10 + IMPICONSIZE, y + 23 + IMPICONSIZE/2, tmp);
        }
        if(imp->type < 0) {
            ui_text(dst, x + 4, y + 2, imp->name);
            ui_text(dst, x + 10 + IMPICONSIZE, y + 23 + IMPICONSIZE/2 - 24, "΁");
        }
        sprintf(tmp, "%u", imp->nframe);
        ui_number(dst, x + 24 + IMPICONSIZE - (imp->nframe > 9 ? 4 : 0), y + h - 9, tmp,
            theme[sel == idx ? THEME_SELFG : THEME_FG]);
    }
}

/**
 * Additional rendering of table cell
 */
void newsprite_rendercell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    ui_implist_t *imp = (ui_implist_t*)data;
    SDL_Rect src, rect;

    (void)dst; (void)sel; (void)hdr; (void)w; (void)h;
    if(idx >= 0 && imp && imp->data) {
        src.x = ui_anim(imp->type, imp->nframe, -1) * imp->ow; src.y = 0; src.w = imp->ow; src.h = imp->h;
        ui_fit(IMPICONSIZE, IMPICONSIZE, imp->ow, imp->h, &rect.w, &rect.h);
        rect.x = x + 4 + (IMPICONSIZE - rect.w) / 2; rect.y = y + 21 + IMPICONSIZE - rect.h;
        SDL_RenderCopy(renderer, imp->data, &src, &rect);
    }
}

/**
 * Remove all frames and the current record from the import list
 */
void newsprite_eraseall(void *data)
{
    ui_implist_t *imp = (ui_implist_t*)newsprite_table.data;

    (void)data;
    if(!imp || !newsprite_table.num || newsprite_table.val < 0) return;
    if(imp[newsprite_table.val].grid) free(imp[newsprite_table.val].grid);
    if(imp[newsprite_table.val].data) SDL_DestroyTexture(imp[newsprite_table.val].data);
    memcpy(&imp[newsprite_table.val], &imp[newsprite_table.val + 1], (newsprite_table.num - newsprite_table.val - 1) *
        sizeof(ui_implist_t));
    newsprite_table.num--;
    if(newsprite_table.val > newsprite_table.num - 1) newsprite_table.val--;
    /* if this was the last record, free the entire list */
    if(!newsprite_table.num) { free(imp); newsprite_table.data = NULL; }
}

/**
 * Remove the last frame or the current record from the import list
 */
void newsprite_erase(void *data)
{
    int w, p, p2, i;
    uint8_t *src, *dst;
    SDL_Texture *texture;
    ui_implist_t *imp = (ui_implist_t*)newsprite_table.data;

    (void)data;
    if(!imp || !newsprite_table.num || newsprite_table.val < 0) return;
    ui_anim_cnt = 0;
    w = imp[newsprite_table.val].ow * (imp[newsprite_table.val].nframe - 1);
    if(w > 0) {
        /* remove last frame from animation */
        texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, w,
            imp[newsprite_table.val].h);
        if(texture) {
            SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
            SDL_LockTexture(texture, NULL, (void**)&dst, &p2);
            SDL_LockTexture(imp[newsprite_table.val].data, NULL, (void**)&src, &p);
            if(dst && src)
                for(i = 0; i < imp[newsprite_table.val].h; i++, src += p, dst += p2)
                    memcpy(dst, src, p2);
            SDL_UnlockTexture(texture);
            SDL_UnlockTexture(imp[newsprite_table.val].data);
            SDL_DestroyTexture(imp[newsprite_table.val].data);
            imp[newsprite_table.val].data = texture;
            imp[newsprite_table.val].nframe--;
            imp[newsprite_table.val].w = w;
        }
    } else {
        /* only one frame, remove the record */
        if(imp[newsprite_table.val].grid) free(imp[newsprite_table.val].grid);
        if(imp[newsprite_table.val].data) SDL_DestroyTexture(imp[newsprite_table.val].data);
        memcpy(&imp[newsprite_table.val], &imp[newsprite_table.val + 1], (newsprite_table.num - newsprite_table.val - 1) *
            sizeof(ui_implist_t));
        newsprite_table.num--;
        if(newsprite_table.val > newsprite_table.num - 1) newsprite_table.val--;
        /* if this was the last record, free the entire list */
        if(!newsprite_table.num) { free(imp); newsprite_table.data = NULL; }
    }
}

/**
 * Empty the import list
 */
void newsprite_empty()
{
    ui_implist_t *imp = (ui_implist_t*)newsprite_table.data;
    int i;

    if(imp && newsprite_table.num) {
        for(i = 0; i < newsprite_table.num; i++) {
            if(imp[i].grid) free(imp[i].grid);
            if(imp[i].data) SDL_DestroyTexture(imp[i].data);
        }
        free(imp); newsprite_table.data = NULL; newsprite_table.num = 0;
    }
    newsprite_table.val = -1;
}

/**
 * Insert a new record at the top. Returns the new record
 */
ui_implist_t *newsprite_insert(char *name, int type, int dir)
{
    ui_implist_t *imp;

    imp = newsprite_table.data = realloc(newsprite_table.data, (newsprite_table.num + 1) * sizeof(ui_implist_t));
    if(!imp) { newsprite_table.num = 0; return NULL; }
    if(newsprite_table.num)
        memmove(&imp[1], &imp[0], newsprite_table.num * sizeof(ui_implist_t));
    memset(&imp[0], 0, sizeof(ui_implist_t));
    if(name) strncpy(imp[0].name, name, PROJ_NAMEMAX);
    imp[0].type = type;
    imp[0].dir = dir;
    newsprite_table.num++;
    return &imp[0];
}

/**
 * Insert a new record at the end. Returns the new record
 */
ui_implist_t *newsprite_new(char *name, int type, int dir)
{
    ui_implist_t *imp;

    imp = newsprite_table.data = realloc(newsprite_table.data, (newsprite_table.num + 1) * sizeof(ui_implist_t));
    if(!imp) { newsprite_table.num = 0; return NULL; }
    memset(&imp[newsprite_table.num], 0, sizeof(ui_implist_t));
    if(name) strncpy(imp[newsprite_table.num].name, name, PROJ_NAMEMAX);
    imp[newsprite_table.num].type = type;
    imp[newsprite_table.num].dir = dir;
    newsprite_table.num++;
    return &imp[newsprite_table.num - 1];
}

/**
 * Finds a record
 */
ui_implist_t *newsprite_find(char *name, int type, int dir)
{
    ui_implist_t *imp = (ui_implist_t *)newsprite_table.data;
    int i;

    if(!imp || !newsprite_table.num) return NULL;
    for(i = 0; i < newsprite_table.num; i++)
        if(imp[i].type == type && imp[i].dir == dir && ((!name && !imp[i].name[0]) || !strcmp(imp[i].name, name)))
            return &imp[i];
    return NULL;
}

/**
 * Add an empty frame to a record. Returns 1 on success
 */
int newsprite_add(ui_implist_t *imp, int w, int h)
{
    uint8_t *src, *dst;
    int p, p2, i;
    SDL_Texture *texture;

    if(!imp || w < 1 || h < 1) return 0;
    if(imp->nframe < 1 || !imp->data) {
        /* first frame */
        imp->nframe = 1;
        imp->ow = imp->w = w;
        imp->h = h;
        imp->data = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, w, h);
        if(imp->data) {
            SDL_SetTextureBlendMode(imp->data, SDL_BLENDMODE_BLEND);
            SDL_LockTexture(imp->data, NULL, (void**)&dst, &p);
            memset(dst, 0, p * h);
            SDL_UnlockTexture(imp->data);
        }
    } else {
        /* add a new frame */
        if(imp->nframe >= 31) { ui_status(1, lang[ERR_SPRLEN]); return 0; }
        if(w != imp->ow || h != imp->h) { ui_status(1, lang[ERR_SPRSIZE]); return 0; }
        texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, imp->w + w, h);
        if(texture) {
            SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
            SDL_LockTexture(texture, NULL, (void**)&dst, &p2);
            SDL_LockTexture(imp->data, NULL, (void**)&src, &p);
            if(dst && src)
                for(i = 0; i < h; i++, src += p, dst += p2) {
                    memcpy(dst, src, p);
                    memset(dst + p, 0, 4 * w);
                }
            SDL_UnlockTexture(texture);
            SDL_UnlockTexture(imp->data);
            SDL_DestroyTexture(imp->data);
            imp->data = texture;
            imp->nframe++;
            imp->w += w;
        }
    }
    return 1;
}

/**
 * Exit new sprites window
 */
void newsprite_exit(int tab)
{
    (void)tab;
    project_freedir(&newsprite_lytcombo.sel.opts, NULL);
}

/**
 * Enter new sprites window
 */
void newsprite_init(int tab)
{
    newsprite_exit(tab);
    newsprite_getlyts();
    newsprite_form[5].param = lang[FILEOP_IMPORT];
    newsprite_form[5].w = ui_textwidth(newsprite_form[5].param) + 40;
    if(newsprite_form[5].w < 200) newsprite_form[5].w = 200;
    newsprite_selsx = newsprite_selex = newsprite_selsy = newsprite_seley =
    newsprite_cropsx = newsprite_cropex = newsprite_cropsy = newsprite_cropey = -1;
    newsprite_selast = 0;
    if(!newsprite_gridw) {
        newsprite_gridw = project.tilew;
        newsprite_gridh = project.tileh;
        newsprite_gridtgl = 1;
    }
    ui_anim_cnt = 0;
}

/**
 * Free resources
 */
void newsprite_free(void *data)
{
    (void)data;
    if(newsprite_sheet) {
        SDL_DestroyTexture(newsprite_sheet);
        newsprite_sheet = NULL;
    }
    if(newsprite_grid) {
        SDL_DestroyTexture(newsprite_grid);
        newsprite_grid = NULL;
    }
    ui_scrarea_reset(&newsprite_scr, 0, 0);
    if(newsprite_frames.data) {
        free(newsprite_frames.data);
        newsprite_frames.data = NULL;
    }
    newsprite_frames.size = newsprite_frames.num = 0;
    if(newsprite_modelpreview) { SDL_DestroyTexture(newsprite_modelpreview); newsprite_modelpreview = NULL; }
}

/**
 * Resize the view
 */
void newsprite_resize(int tab, int w, int h)
{
    (void)tab;
    newsprite_form[0].y = newsprite_form[1].y = newsprite_form[2].y = newsprite_form[3].y = newsprite_form[4].y =
        newsprite_form[5].y = newsprite_form[39].y = h - 48;
    newsprite_form[0].x = w - 40;
    newsprite_form[1].x = w / 4 - newsprite_form[1].w / 2;
    newsprite_form[2].x = w / 2 - newsprite_form[2].w - 10;
    newsprite_form[5].x = w - 64 - newsprite_form[5].w;
    newsprite_form[6].x = w - 10 - newsprite_form[6].w;
    newsprite_form[6].h = newsprite_form[7].h = h - 119;
    newsprite_form[7].x = w - 18 - newsprite_form[6].w / 2;
    newsprite_form[8].h = h - 131;
    newsprite_form[8].w = w - 42 - newsprite_form[6].w;
    newsprite_form[28].w = newsprite_form[30].w = (w - newsprite_form[28].x - 68) / 2 - 4;
    newsprite_form[29].x = newsprite_form[28].x + newsprite_form[28].w + 4;
    newsprite_form[30].x = w - 34 - newsprite_form[30].w;
    newsprite_form[31].x = w - 30;
    newsprite_form[39].x = newsprite_form[5].x - newsprite_form[39].w - 20;
    newsprite_form[35].x = newsprite_form[37].x = newsprite_form[6].x + 7;
    newsprite_form[36].x = newsprite_form[37].x + 24;
    newsprite_form[38].y = h - 60 - newsprite_form[38].h;
    newsprite_form[38].w = newsprite_form[8].w + 12;
    ui_scrarea_set(&newsprite_scr, newsprite_form[8].x, newsprite_form[8].y, newsprite_form[8].w, newsprite_form[8].h);
}

/**
 * View layer
 */
void newsprite_redraw(int tab)
{
    (void)tab;
    ui_form = newsprite_form;
    newsprite_form[3].inactive = newsprite_form[11].inactive = newsprite_form[12].inactive =
        newsprite_form[13].inactive = newsprite_form[14].inactive = !newsprite_sheet;
    newsprite_form[5].inactive = !(newsprite_table.num || (newsprite_sheet && (!newsprite_gridtgl || sprites_cat == 4)));
    newsprite_form[6].inactive = (!newsprite_gridtgl || sprites_cat == 4 ? 2 : 0);
    newsprite_form[7].inactive = ((!newsprite_gridtgl || sprites_cat == 4) && !newsprite_sheet ? 0 : 2);
    newsprite_form[7].param = (void*)((uintptr_t)sprites_cats.elems[sprites_cat].icon);
    newsprite_form[15].inactive = (!newsprite_sheet || sprites_cat == 4);
    newsprite_form[16].inactive = newsprite_form[17].inactive =
    newsprite_form[18].inactive = newsprite_form[19].inactive =
    newsprite_form[20].inactive = newsprite_form[21].inactive =
    newsprite_form[22].inactive = newsprite_form[23].inactive =
    newsprite_form[24].inactive = newsprite_form[25].inactive =
    newsprite_form[26].inactive = newsprite_form[27].inactive =
        newsprite_form[28].inactive = (!newsprite_sheet || !newsprite_gridtgl || sprites_cat == 4);
    newsprite_form[29].inactive = (!newsprite_gridtgl || sprites_cat == 4 || !newsprite_lyt[0] ||
        !strcmp(newsprite_lyt, BUILTINLYT) || !newsprite_table.num);
    newsprite_form[31].inactive = (!newsprite_gridtgl || sprites_cat == 4 || !newsprite_table.num || newsprite_table.val < 0);
    ui_form[32].inactive = ui_form[33].inactive = ui_form[34].inactive = 2;
    newsprite_form[30].status = (!newsprite_gridtgl || sprites_cat == 4) ? SPRITES_NAME : SPRITES_GRPNAME;
    ui_form[35].inactive = ui_form[36].inactive = ui_form[37].inactive =
        newsprite_frames.data && newsprite_table.val == -1 ? 0 : 2;
    if(newsprite_frames.data && newsprite_frames.num) {
        newsprite_selstr.max = newsprite_frames.num;
        newsprite_chsel((void*)-1);
        newsprite_form[8].inactive = 2;
        newsprite_form[38].inactive = 0;
    } else {
        newsprite_form[8].inactive = 0;
        newsprite_form[38].inactive = 2;
    }
}

/**
 * Render additional view
 */
void newsprite_render(int tab)
{
    SDL_Rect rect, src;
    int l;

    (void)tab;
    if(newsprite_sheet) {
        ui_scrarea_win(&newsprite_scr, &src, &rect);
        SDL_RenderCopy(renderer, newsprite_sheet, &src, &rect);
        if((!newsprite_gridtgl || sprites_cat == 4)) {
            rect.x = newsprite_form[6].x; rect.y = newsprite_form[6].y;
            ui_fit(newsprite_form[6].w, newsprite_form[6].h, newsprite_scr.ow, newsprite_scr.oh, &rect.w, &rect.h);
            SDL_RenderCopy(renderer, newsprite_sheet, NULL, &rect);
        } else
        if(newsprite_grid)
            SDL_RenderCopy(renderer, newsprite_grid, &src, &rect);
    }
    if(newsprite_modelpreview && newsprite_frames.size) {
        l = newsprite_frames.cnt;
        if(l * newsprite_frames.size > 4096) l = 4096 / newsprite_frames.size;
        src.x = ui_anim(2, l, -1) * newsprite_frames.size; src.y = 0; src.w = src.h = newsprite_frames.size;
        ui_fit(newsprite_form[38].w - 42, newsprite_form[38].y - newsprite_form[8].y - 10, src.w, src.h, &rect.w, &rect.h);
        rect.x = newsprite_form[8].x + (newsprite_form[38].w - rect.w) / 2;
        rect.y = newsprite_form[8].y + (newsprite_form[38].y - newsprite_form[8].y - 10 - rect.h) / 2;
        SDL_RenderCopy(renderer, newsprite_modelpreview, &src, &rect);
    }
    if(!newsprite_form[6].inactive)
        ui_table_render(newsprite_form[6].x, newsprite_form[6].y, newsprite_form[6].w, newsprite_form[6].h, &newsprite_table);
}

/**
 * Controller
 */
int newsprite_ctrl(int tab)
{
    int ret = 0, step;
    ui_implist_t *imp;

    (void)tab;

    switch(event.type) {
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym) {
                case SDLK_UP: if(newsprite_table.val > -1) { newsprite_table.val--; ret = 1; } break;
                case SDLK_DOWN: if(newsprite_table.val < newsprite_table.num - 1) { newsprite_table.val++; ret = 1; } break;
                case SDLK_BACKSPACE: if(newsprite_table.val > -1) { newsprite_erase(NULL); ret = 1; } break;
                case SDLK_LEFT:
                    if(newsprite_frames.data && newsprite_frames.str > 1) {
                        newsprite_frames.str--; newsprite_chsel(NULL); ret = 1;
                    }
                break;
                case SDLK_RIGHT:
                    if(newsprite_frames.data && newsprite_frames.str < newsprite_selstr.max) {
                        newsprite_frames.str++; newsprite_chsel(NULL); ret = 1;
                    }
                break;
                case SDLK_MINUS: ui_scrarea_zoom(&newsprite_scr, -1); ret = 1; break;
                case SDLK_PLUS: case SDLK_EQUALS: ui_scrarea_zoom(&newsprite_scr, 1); ret = 1; break;
            }
        break;
        case SDL_MOUSEBUTTONDOWN:
            if(newsprite_sheet && newsprite_gridtgl &&
              event.button.x >= newsprite_form[8].x && event.button.x < newsprite_form[8].x + newsprite_form[8].w &&
              event.button.y >= newsprite_form[8].y && event.button.y < newsprite_form[8].y + newsprite_form[8].h) {
                ui_scrarea_btn(&newsprite_scr, event.button.x - newsprite_form[8].x,
                    event.button.y - newsprite_form[8].y, &newsprite_selsx, &newsprite_selsy);
                newsprite_selsx = newsprite_selex = (newsprite_selsx - newsprite_gridl) / (newsprite_gridw + newsprite_gridH);
                newsprite_selsy = newsprite_seley = (newsprite_selsy - newsprite_gridt) / (newsprite_gridh + newsprite_gridV);
                newsprite_selast = (newsprite_seley << 16) | newsprite_selex;
                newsprite_recalcgrid(NULL);
            }
        break;
        case SDL_MOUSEMOTION:
            if(newsprite_sheet && newsprite_gridtgl && newsprite_selsx != -1 && newsprite_selsy != -1 &&
              event.motion.x >= newsprite_form[8].x && event.motion.x < newsprite_form[8].x + newsprite_form[8].w &&
              event.motion.y >= newsprite_form[8].y && event.motion.y < newsprite_form[8].y + newsprite_form[8].h) {
                ui_scrarea_btn(&newsprite_scr, event.motion.x - newsprite_form[8].x,
                    event.motion.y - newsprite_form[8].y, &newsprite_selex, &newsprite_seley);
                newsprite_selex = (newsprite_selex - newsprite_gridl) / (newsprite_gridw + newsprite_gridH);
                newsprite_seley = (newsprite_seley - newsprite_gridt) / (newsprite_gridh + newsprite_gridV);
                if(newsprite_selast != ((newsprite_seley << 16) | newsprite_selex)) {
                    newsprite_selast = (newsprite_seley << 16) | newsprite_selex;
                    newsprite_recalcgrid(NULL);
                }
            }
        break;
        case SDL_MOUSEBUTTONUP:
            if(newsprite_sheet && newsprite_gridtgl && newsprite_selsx != -1 && newsprite_selsy != -1 &&
              event.button.x >= newsprite_form[8].x && event.button.x < newsprite_form[8].x + newsprite_form[8].w &&
              event.button.y >= newsprite_form[8].y && event.button.y < newsprite_form[8].y + newsprite_form[8].h) {
                if(ui_isshift) {
                    newsprite_cropsx = newsprite_selsx; newsprite_cropsy = newsprite_selsy;
                    newsprite_cropex = newsprite_selex; newsprite_cropey = newsprite_seley;
                    if(newsprite_cropsx > newsprite_cropex) {
                        ret = newsprite_cropsx; newsprite_cropsx = newsprite_cropex; newsprite_cropex = ret;
                    }
                    if(newsprite_cropsy > newsprite_cropey) {
                        ret = newsprite_cropsy; newsprite_cropsy = newsprite_cropey; newsprite_cropey = ret;
                    }
                } else {
                    if(newsprite_cropsx != -1) {
                        newsprite_cropsx = newsprite_cropex = newsprite_cropsy = newsprite_cropey = -1;
                    } else {
                        if(newsprite_table.val < 0)
                            imp = newsprite_insert(newsprite_name, newsprite_type, newsprite_dir);
                        else
                            imp = &((ui_implist_t*)newsprite_table.data)[newsprite_table.val];
                        newsprite_addfromgrid(imp, newsprite_selsx, newsprite_selex, newsprite_selsy, newsprite_seley, -1);
                    }
                }
                ret = 1;
            }
            newsprite_selsx = newsprite_selex = newsprite_selsy = newsprite_seley = -1;
            newsprite_recalcgrid(NULL);
        break;
        case SDL_MOUSEWHEEL:
            if(newsprite_sheet &&
              ui_curx >= newsprite_form[8].x && ui_curx < newsprite_form[8].x + newsprite_form[8].w &&
              ui_cury >= newsprite_form[8].y && ui_cury < newsprite_form[8].y + newsprite_form[8].h) {
                if(ui_isctrl) {
                    if(event.wheel.y < 0) ui_scrarea_zoom(&newsprite_scr, -1);
                    if(event.wheel.y > 0) ui_scrarea_zoom(&newsprite_scr, 1);
                } else {
                    step = newsprite_scr.w / 100;
                    if(event.wheel.y > 0) newsprite_scr.y -= step;
                    if(event.wheel.y < 0) newsprite_scr.y += step;
                    if(event.wheel.x > 0) newsprite_scr.x -= step;
                    if(event.wheel.x < 0) newsprite_scr.x += step;
                    if(newsprite_scr.y > newsprite_scr.h - newsprite_form[8].h)
                        newsprite_scr.y = newsprite_scr.h - newsprite_form[8].h;
                    if(newsprite_scr.y < 0) newsprite_scr.y = 0;
                    if(newsprite_scr.x > newsprite_scr.w - newsprite_form[8].w)
                        newsprite_scr.x = newsprite_scr.w - newsprite_form[8].w;
                    if(newsprite_scr.x < 0) newsprite_scr.x = 0;
                }
                ret = 1;
            }
        break;
    }
    return ret;
}

/**
 * Change animation type
 */
void newsprite_chtype(void *data)
{
    char *i = (char*)data;

    if(event.type == SDL_MOUSEBUTTONUP && event.button.button != 1) {
        if(*i > 0) (*i) -= 1; else *i = 2;
    } else {
        if(*i < 2) (*i) += 1; else *i = 0;
    }
    /* make the language checker happy: SPRITES_ANIMFB, SPRITES_ANIMLOOP */
    ui_status(0, lang[*i + SPRITES_ANIMONCE]);
}

/**
 * Change direction
 */
void newsprite_chdir(void *data)
{
    char *i = (char*)data;

    if(event.type == SDL_MOUSEBUTTONUP && event.button.button != 1) {
        if(*i > 0) (*i) -= 1; else *i = 7;
    } else {
        if(*i < 7) (*i) += 1; else *i = 0;
    }
    /* make the language checker happy:
     * SPRITES_TYPESW, SPRITES_TYPEWE, SPRITES_TYPENW, SPRITES_TYPENO, SPRITES_TYPENE, SPRITES_TYPEEA, SPRITES_TYPESE */
    ui_status(0, lang[*i + SPRITES_TYPESO]);
    if(newsprite_frames.data && newsprite_frames.num)
        newsprite_chsel(NULL);
}

/**
 * Change frame selection
 */
void newsprite_chsel(void *data)
{
    int i, l, y, p;
    uint8_t *pixels;

    if(newsprite_modelpreview) { SDL_DestroyTexture(newsprite_modelpreview); newsprite_modelpreview = NULL; }
    if(newsprite_frames.data && newsprite_frames.num) {
        if(newsprite_frames.cnt < 1) newsprite_frames.cnt = 1;
        if(newsprite_frames.cnt > 31) newsprite_frames.cnt = 31;
        if(newsprite_frames.str < 1) newsprite_frames.str = 1;
        if(newsprite_frames.str > newsprite_frames.num) newsprite_frames.str = newsprite_frames.num;
        if(newsprite_frames.str - 1 + newsprite_frames.cnt > newsprite_frames.num)
            newsprite_frames.cnt = newsprite_frames.num - newsprite_frames.str + 1;
        if(newsprite_frames.cnt > 31) newsprite_frames.cnt = 31;
        if(newsprite_frames.cnt < 1) {
            newsprite_frames.cnt = 1;
            newsprite_frames.str = newsprite_frames.num;
        }
        if(data != (void*)-1) {
            if((newsprite_frames.str - 1) * 68 > newsprite_frames.scr + newsprite_form[38].w)
                newsprite_frames.scr = (newsprite_frames.str - 1) * 68;
            if((newsprite_frames.str - 1 + newsprite_frames.cnt) * 68 < newsprite_frames.scr)
                newsprite_frames.scr = (newsprite_frames.str - 1 + newsprite_frames.cnt) * 68 - newsprite_form[38].w;
        }
        l = newsprite_frames.cnt;
        if(l * newsprite_frames.size > 4096) l = 4096 / newsprite_frames.size;
        newsprite_modelpreview = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING,
            l * newsprite_frames.size, newsprite_frames.size);
        if(!newsprite_modelpreview) return;
        SDL_SetTextureBlendMode(newsprite_modelpreview, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(newsprite_modelpreview, NULL, (void**)&pixels, &p);
        for(i = 0; i < l; i++) {
            for(y = 0; y < newsprite_frames.size; y++)
                memcpy(pixels + i * newsprite_frames.size * 4 + y * p,
                    newsprite_frames.data + ((((newsprite_dir + newsprite_frames.dir) & 7) * newsprite_frames.num +
                        newsprite_frames.str - 1 + i) * newsprite_frames.size + y) * newsprite_frames.size,
                    newsprite_frames.size * 4);
        }
        SDL_UnlockTexture(newsprite_modelpreview);
    }
}

/**
 * Add to import list from frames selection
 */
void newsprite_addfromsel(void *data)
{
    ui_implist_t *imp;
    int i, j;

    (void)data;
    if(newsprite_frames.data && newsprite_frames.num) {
        if(newsprite_frames.cnt < 1) newsprite_frames.cnt = 1;
        if(newsprite_frames.cnt > 31) newsprite_frames.cnt = 31;
        if(newsprite_frames.str < 1) newsprite_frames.str = 1;
        if(newsprite_frames.str > newsprite_frames.num) newsprite_frames.str = newsprite_frames.num;
        if(newsprite_frames.str - 1 + newsprite_frames.cnt > newsprite_frames.num)
            newsprite_frames.cnt = newsprite_frames.num - newsprite_frames.str + 1;
        if(newsprite_frames.cnt > 31) newsprite_frames.cnt = 31;
        if(newsprite_frames.cnt < 1) {
            newsprite_frames.cnt = 1;
            newsprite_frames.str = newsprite_frames.num;
        }
        /* for every direction */
        for(j = 0; j < 8; j++) {
            if(!(imp = newsprite_new(newsprite_name, newsprite_type, j))) break;
            /* for every frame */
            for(i = 0; i < newsprite_frames.cnt; i++)
                if(newsprite_add(imp, newsprite_frames.size, newsprite_frames.size) && imp->data) {
                    ui_blitbuf(imp->data, (imp->nframe - 1) * newsprite_frames.size, 0, newsprite_frames.size,
                        newsprite_frames.size, newsprite_frames.data, 0,
                        (((j + newsprite_frames.dir) & 7) * newsprite_frames.num + newsprite_frames.str - 1 + i) *
                            newsprite_frames.size,
                        newsprite_frames.size, newsprite_frames.size, newsprite_frames.size * 4);
                }
        }
        newsprite_frames.str += newsprite_frames.cnt;
        newsprite_frames.cnt = 1;
        newsprite_chsel(NULL);
    }
}

/**
 * Add palette from sheet
 */
void newsprite_addpal()
{
    ui_implist_t *imp;
    int i, p, p2;
    uint8_t *src, *dst;

    if(!newsprite_sheet) return;
    imp = newsprite_new("pal", -1, 0);
    if(!imp) return;
    if(newsprite_add(imp, 8, 16) && imp->data) {
        SDL_LockTexture(newsprite_sheet, NULL, (void**)&src, &p);
        SDL_LockTexture(imp->data, NULL, (void**)&dst, &p2);
        src += (newsprite_scr.oh - 16) * p + (newsprite_scr.ow - 8) * 4;
        for(i = 0; i < 16; i++, src += p, dst += p2)
            memcpy(dst, src, 4 * 8);
        SDL_UnlockTexture(newsprite_sheet);
        SDL_UnlockTexture(imp->data);
    }
}

/**
 * Add to import list from grid
 */
void newsprite_addfromgrid(ui_implist_t *imp, int sx, int ex, int sy, int ey, int sp)
{
    int i, j, k, w, p, p2;
    uint8_t *pal, *src, *dst, *s, *d;

    if(!imp || !newsprite_sheet || !newsprite_gridtgl || sx < 0 || ex < 0 || sy < 0 || ey < 0) return;
    if(ex < sx) { i = ex; ex = sx; sx = i; }
    if(ey < sy) { i = ey; ey = sy; sy = i; }
    if(newsprite_gridl + ex * (newsprite_gridw + newsprite_gridH) + newsprite_gridw > newsprite_scr.ow ||
       newsprite_gridt + ey * (newsprite_gridh + newsprite_gridV) + newsprite_gridh > newsprite_scr.oh) return;
    if(newsprite_add(imp, (ex - sx + 1) * newsprite_gridw, (ey - sy + 1) * newsprite_gridh)) {
        /* save grid coordinates too */
        imp->grid = (int*)realloc(imp->grid, 4 * imp->nframe * sizeof(int));
        if(imp->grid) {
            i = (imp->nframe - 1) * 4;
            imp->grid[i + 0] = sx;
            imp->grid[i + 1] = sy;
            imp->grid[i + 2] = ex;
            imp->grid[i + 3] = ey;
        }
        /* copy sprite data from sheet */
        if(imp->data) {
            w = imp->ow * (imp->nframe - 1);
            SDL_LockTexture(newsprite_sheet, NULL, (void**)&src, &p);
            SDL_LockTexture(imp->data, NULL, (void**)&dst, &p2);
            pal = src + (newsprite_scr.oh - 16 + sp) * p + (newsprite_scr.ow - 8) * 4;
            for(j = 0; j <= ey - sy; j++)
                for(i = 0; i <= ex - sx; i++) {
                    s = src + 4 * (newsprite_gridl + (sx + i) * (newsprite_gridw + newsprite_gridH)) +
                              p * (newsprite_gridt + (sy + j) * (newsprite_gridh + newsprite_gridV));
                    d = dst + 4 * (w + i * newsprite_gridw) + p2 * (j * newsprite_gridh);
                    if(sp >= 0 && sp < 16 && pal[3] == 255 && (pal[0] || pal[1] || pal[2])) {
                        ui_combinewp((uint32_t*)d, 0, 0, newsprite_gridw, newsprite_gridh, p2, (uint32_t*)s, 0, 0,
                            newsprite_gridw, newsprite_gridh, p, pal);
                    } else {
                        for(k = 0; k < newsprite_gridh; k++, s += p, d += p2)
                            memcpy(d, s, newsprite_gridw * 4);
                    }
                }
            SDL_UnlockTexture(newsprite_sheet);
            SDL_UnlockTexture(imp->data);
        }
        ui_anim_cnt = 0;
    }
}

/**
 * Add to import list with data
 */
void newsprite_addwithdata(char *name, int type, int dir, int nframe, SDL_Texture *txt, int sx, int sy, int sw, int sh, int sp)
{
    ui_implist_t *imp;
    uint8_t *dst, *src, *pal;
    int i, p, p2;

    if(!name || !*name || !txt) return;
    if(!(imp = newsprite_new(name, type, dir))) return;
    imp->nframe = nframe;
    imp->ow = newsprite_gridw;
    imp->w = newsprite_gridw * nframe;
    imp->h = newsprite_gridh;
    imp->data = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, imp->w, imp->h);
    if(imp->data) {
        SDL_SetTextureBlendMode(imp->data, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(imp->data, NULL, (void**)&dst, &p);
        SDL_LockTexture(txt, NULL, (void**)&src, &p2);
        pal = src + (sh - 16 + sp) * p2 + (sw - 8) * 4;
        if(sp >= 0 && sp < 16 && pal[3] == 255 && (pal[0] || pal[1] || pal[2])) {
            memset(dst, 0, p * newsprite_gridh);
            ui_combinewp((uint32_t*)dst, 0, 0, imp->w, imp->h, p, (uint32_t*)src, sx * newsprite_gridw, sy * newsprite_gridh,
                sw, sh, p2, pal);
        } else {
            src += sy * newsprite_gridh * p2 + sx * newsprite_gridw * 4;
            for(i = 0; i < newsprite_gridh; i++, src += p2, dst += p)
                memcpy(dst, src, imp->w * 4);
        }
        SDL_UnlockTexture(txt);
        SDL_UnlockTexture(imp->data);
    }
}

/**
 * Recalculate the grid mask
 */
void newsprite_recalcgrid(void *data)
{
    uint32_t *pixels, c = theme[THEME_SELBX] & 0xFFFFFF;
    int p, i, j, w, h, o, x, y, sx, ex, sy, ey;

    (void)data;
    if(newsprite_selsx < newsprite_selex) {
        sx = newsprite_selsx; ex = newsprite_selex;
    } else {
        sx = newsprite_selex; ex = newsprite_selsx;
    }
    if(newsprite_selsy < newsprite_seley) {
        sy = newsprite_selsy; ey = newsprite_seley;
    } else {
        sy = newsprite_seley; ey = newsprite_selsy;
    }
    if(newsprite_grid) {
        SDL_DestroyTexture(newsprite_grid);
        newsprite_grid = NULL;
    }
    if(newsprite_gridtgl) {
        newsprite_grid = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING,
            newsprite_scr.ow, newsprite_scr.oh);
        if(!newsprite_grid) return;
        SDL_LockTexture(newsprite_grid, NULL, (void**)&pixels, &p);
        SDL_SetTextureBlendMode(newsprite_grid, SDL_BLENDMODE_BLEND);
        for(i = 0; i < newsprite_scr.ow * newsprite_scr.oh; i++) pixels[i] = 0xCC000000;
        p /= 4;
        for(j = newsprite_gridt, y = 0; j < newsprite_scr.oh; y++) {
            for(h = 0; h < newsprite_gridh && j < newsprite_scr.oh; h++, j++) {
                for(i = newsprite_gridl, o = j * p + newsprite_gridl, x = 0; i < newsprite_scr.ow; x++) {
                    for(w = 0; w < newsprite_gridw && i < newsprite_scr.ow; w++, i++, o++)
                        pixels[o] = GRIDMASKALPHA | (x >= sx && x <= ex && y >= sy && y <= ey ? c :
                            (newsprite_cropex != -1 && newsprite_cropey != -1 &&
                            (x < newsprite_cropsx || x > newsprite_cropex || y < newsprite_cropsy || y > newsprite_cropey) ?
                            ((x ^ y) & 1 ? 0x7F000080 : 0x7F0000A0) :
                            ((x ^ y) & 1 ? 0x202020 : 0xE0E0E0)));
                    i += newsprite_gridH; o += newsprite_gridH;
                }
            }
            j += newsprite_gridV;
        }
        SDL_UnlockTexture(newsprite_grid);
    }
}

/**
 * Resize a sprite sheet with resampling, original code from SDL_gfx, but heavily modified
 */
void newsprite_resample(int w, int h)
{
    int p;
    uint32_t *data, *ret;
    SDL_Texture *newsheet;

    if(!newsprite_sheet || w < 2 || h < 2 || w > 4096 || h > 4096) return;
    if(verbose) printf("newsprite_resample: from %d x %d to %d x %d\n", newsprite_scr.ow, newsprite_scr.oh, w, h);
    newsheet = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, w, h);
    if(!newsheet) return;
    SDL_LockTexture(newsheet, NULL, (void**)&ret, &p);
    SDL_LockTexture(newsprite_sheet, NULL, (void**)&data, &p);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = w; ui_clip.h = h;
    ui_resample(ret, 0, 0, w, h, 0, 255, data, newsprite_scr.ow, newsprite_scr.oh, p);
    ui_clip.w = scr_w; ui_clip.h = scr_h;
    SDL_UnlockTexture(newsheet);
    SDL_UnlockTexture(newsprite_sheet);
    SDL_DestroyTexture(newsprite_sheet);
    newsprite_sheet = newsheet;
    ui_scrarea_reset(&newsprite_scr, w, h);
    if(newsprite_grid)
        newsprite_recalcgrid(NULL);
}

/**
 * Cut size in half
 */
void newsprite_half(void *param)
{
    (void)param;
    newsprite_resample(newsprite_scr.ow / 2, newsprite_scr.oh / 2);
}

/**
 * Enlarge to double
 */
void newsprite_double(void *param)
{
    (void)param;
    newsprite_resample(newsprite_scr.ow * 2, newsprite_scr.oh * 2);
}

/**
 * Flip vertically
 */
void newsprite_vflip(void *param)
{
    int x, y, p, pitch;
    uint32_t *data, tmp;

    (void)param;
    if(!newsprite_sheet) return;
    if(verbose) printf("newsprite_vflip: flipping vertically\n");
    SDL_LockTexture(newsprite_sheet, NULL, (void**)&data, &pitch);
    for(y = 0, p = 0; y < newsprite_scr.oh / 2; y++, p += pitch/4)
        for(x = 0; x < newsprite_scr.ow; x++) {
            tmp = data[p + x];
            data[p + x] = data[(newsprite_scr.oh - 1 - y) * pitch/4 + x];
            data[(newsprite_scr.oh - 1 - y) * pitch/4 + x] = tmp;
        }
    SDL_UnlockTexture(newsprite_sheet);
}

/**
 * Flip horizontally
 */
void newsprite_hflip(void *param)
{
    int x, y, p, pitch;
    uint32_t *data, tmp;

    (void)param;
    if(!newsprite_sheet) return;
    if(verbose) printf("newsprite_hflip: flipping horizontally\n");
    SDL_LockTexture(newsprite_sheet, NULL, (void**)&data, &pitch);
    for(y = 0, p = 0; y < newsprite_scr.oh; y++, p += pitch/4)
        for(x = 0; x < newsprite_scr.ow / 2; x++) {
            tmp = data[p + x];
            data[p + x] = data[p + newsprite_scr.ow - x - 1];
            data[p + newsprite_scr.ow - x - 1] = tmp;
        }
    SDL_UnlockTexture(newsprite_sheet);
}


/**
 * Swap red and blue channels
 */
void newsprite_swaprb()
{
    ui_implist_t *imp = (ui_implist_t *)newsprite_table.data;
    uint8_t *data, c;
    int i, j, p;

    if(verbose) printf("newsprite_swaprb: swapping red and blue channels\n");
    if(newsprite_sheet) {
        SDL_LockTexture(newsprite_sheet, NULL, (void**)&data, &p);
        for(i = 0; i < newsprite_scr.ow * newsprite_scr.oh; i++) {
            c = data[i * 4]; data[i * 4] = data[i * 4 + 2]; data[i * 4 + 2] = c;
        }
        SDL_UnlockTexture(newsprite_sheet);
    }
    if(newsprite_frames.data) {
        data = (uint8_t*)newsprite_frames.data;
        for(i = 0; i < newsprite_frames.size * newsprite_frames.size * newsprite_frames.num * 8; i++) {
            c = data[i * 4]; data[i * 4] = data[i * 4 + 2]; data[i * 4 + 2] = c;
        }
    }
    if(imp)
        for(j = 0; j < newsprite_table.num; j++) {
            SDL_LockTexture(imp[j].data, NULL, (void**)&data, &p);
            for(i = 0; i < imp[j].w * imp[j].h; i++) {
                c = data[i * 4]; data[i * 4] = data[i * 4 + 2]; data[i * 4 + 2] = c;
            }
            SDL_UnlockTexture(imp[j].data);
        }
}

/**
 * Convert to given palette
 */
void _newsprite_convpal(uint8_t *data, uint8_t *P, float *labpal, int num, int w, int h, long int cur, long int tot)
{
    float l, a, b, d, dm;
    int i, dr, dg, db, m, x, y;
    int64_t di, dmi;

    for(y = 0; y < h; y++) {
        ui_progressbar(0, 0, cur + y, tot, SPRITES_CONV);
        for(x = 0; x < w; x++, data += 4) {
            if(palette_cie)
                for(i = m = 0, dm = 1.8e+19; i < num && dm > 0.0; i++) {
                    palette_rgb2lab(data, &l, &a, &b);
                    l -= labpal[i * 3 + 0]; a -= labpal[i * 3 + 1]; b -= labpal[i * 3 + 2];
                    d = l*l + a*a + b*b;
                    if(d < dm) { dm = d; m = i; }
                }
            else
                for(i = m = 0, dmi = 3*65536+1; i < num && dmi; i++) {
                    dr = data[0] - P[i*4+0]; dg = data[1] - P[i*4+1]; db = data[2] - P[i*4+2];
                    di = dr*dr + dg*dg + db*db;
                    if(di < dmi) { dmi = di; m = i; }
                }
            data[0] = P[m*4+0];
            data[1] = P[m*4+1];
            data[2] = P[m*4+2];
        }
    }
}
void newsprite_convpal()
{
    float labpal[256 * 3];
    ui_implist_t *imp = (ui_implist_t *)newsprite_table.data;
    int i, p, num;
    long int cur = 0, tot;
    uint8_t *data, *P;

    if(palette_sel < 0 || !palette_list) return;
    num = palette_cpals[palette_sel * 257];
    P = (uint8_t*)&palette_cpals[palette_sel * 257 + 1];
    if(palette_cie)
        for(i = 0; i < num; i++)
            palette_rgb2lab(&P[(i + 1) * 4], &labpal[i * 3], &labpal[i * 3 + 1], &labpal[i * 3 + 2]);
    if(verbose) printf("newsprite_convpal: converting to palette %s with %d colors using %s distance\n",
        palette_list[palette_sel], num, palette_cie ? "CIE76" : "sRGB");
    tot = (newsprite_sheet ? newsprite_scr.oh : 0) + (newsprite_frames.data ? newsprite_frames.size * newsprite_frames.num * 8: 0);
    if(imp)
        for(i = 0; i < newsprite_table.num; i++)
            tot += imp[i].h;
    if(newsprite_sheet) {
        SDL_LockTexture(newsprite_sheet, NULL, (void**)&data, &p);
        _newsprite_convpal(data, P, labpal, num, newsprite_scr.ow, newsprite_scr.oh, cur, tot);
        SDL_UnlockTexture(newsprite_sheet);
        cur += newsprite_scr.oh;
    }
    if(newsprite_frames.data) {
        _newsprite_convpal((uint8_t*)newsprite_frames.data, P, labpal, num, newsprite_frames.size,
            newsprite_frames.size * newsprite_frames.num * 8, cur, tot);
        cur += newsprite_frames.size * newsprite_frames.num;
    }
    if(imp)
        for(i = 0; i < newsprite_table.num; i++) {
            SDL_LockTexture(imp[i].data, NULL, (void**)&data, &p);
            _newsprite_convpal(data, P, labpal, num, imp[i].w, imp[i].h, cur, tot);
            SDL_UnlockTexture(imp[i].data);
            cur += imp[i].h;
        }
    ui_progressbar(0, 0, 0, 0, SPRITES_CONV);
}

/**
 * Clean up import list
 */
void newsprite_cleanupimp()
{
    ui_implist_t *imp;
    uint8_t *dst;
    int i, sx, ex, p;

    /* clean up, remove empty sprites */
    imp = (ui_implist_t*)newsprite_table.data;
    for(i = 0; i < newsprite_table.num; i++) {
        sx = 0;
        if(imp[i].data && imp[i].h > 0 && imp[i].w > 0 && imp[i].nframe > 0) {
            SDL_LockTexture(imp[i].data, NULL, (void**)&dst, &p);
            for(ex = 0; ex < p * imp[i].h; ex += 4)
                if(dst[ex + 3] >= 3) { sx++; break; }
            SDL_UnlockTexture(imp[i].data);
        }
        if(!sx) {
            if(imp[i].grid) free(imp[i].grid);
            if(imp[i].data) SDL_DestroyTexture(imp[i].data);
            memcpy(&imp[i], &imp[i + 1], (newsprite_table.num - i - 1) * sizeof(ui_implist_t));
            newsprite_table.num--; i--;
        }
    }
}

/**
 * Apply the built-in TirNanoG Base layout
 */
void newsprite_tngbase(SDL_Texture *src, int w, int h, int sp)
{
    int i, j;

    if(w / 18 < 8 || h / 16 < 8) return;
    newsprite_empty();
    sprites_cat = 2;                /* character sprites */
    newsprite_gridtgl = 1;          /* use grid */
    newsprite_form[6].inactive = 0; /* not fullscreen */
    newsprite_gridw = w / 18;
    newsprite_gridh = h / 16;
    newsprite_gridl = newsprite_gridt = newsprite_gridV = newsprite_gridH = 0;
    newsprite_recalcgrid(NULL);
    if(verbose) printf("newsprite_tngbase: w %d h %d grid %d x %d\n", w, h, newsprite_gridw, newsprite_gridh);
    for(i = 0; tngbase[i].name; i++)
        for(j = 0; j < tngbase[i].ndir; j++)
            newsprite_addwithdata(tngbase[i].name, tngbase[i].type, j * 2, tngbase[i].nframe, src,tngbase[i].x, tngbase[i].y + j,
                w, h, sp);
    newsprite_cleanupimp();
}

/**
 * Prompt for color variation
 */
int newsprite_variantmodal(uint8_t *pal, int sp)
{
    int ret = -1, i, p, l = ui_textwidth(lang[SPRITES_CHOOSEVAR]), w;
    uint32_t *data = NULL;
    SDL_Texture *fade, *modal;
    SDL_Rect rect;
    SDL_Event event;

    ui_clip.x = ui_clip.y = 0; ui_clip.w = scr_w; ui_clip.h = scr_h;
    ssfn_dst.fg = theme[THEME_FG]; ssfn_dst.bg = 0;
    w = (l + 40 < 300 ? 300 : l + 40);
    modal = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, w, 64);
    if(!modal) main_error(ERR_DISPLAY);
    ui_box(modal, 0, 0, w, 64, theme[THEME_LIGHT], theme[THEME_BG], theme[THEME_DARK]);
    ui_text(modal, (w - l) / 2, 8, lang[SPRITES_CHOOSEVAR]);
    rect.x = (scr_w - w) / 2; rect.y = (scr_h - 64) / 2; rect.w = w; rect.h = 64;
    fade = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, scr_w, scr_h);
    if(fade) {
        SDL_SetTextureBlendMode(fade, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(fade, NULL, (void**)&data, &p);
        for(i = 0; i < p/4 * scr_h; i++) data[i] = 0x7F000000;
        SDL_UnlockTexture(fade);
    }
    SDL_SetCursor(cursors[CURSOR_PTR]);
    while(1) {
        ui_clrver(modal, (w - 206) / 2, 40, ret, (uint32_t*)pal, sp, 0, 1);
        ui_render();
        if(fade)
            SDL_RenderCopy(renderer, fade, NULL, NULL);
        SDL_RenderCopy(renderer, modal, NULL, &rect);
        SDL_RenderPresent(renderer);
        SDL_WaitEvent(&event);
        if(event.type==SDL_QUIT || (event.type==SDL_WINDOWEVENT && event.window.event==SDL_WINDOWEVENT_CLOSE)) break;
        if(event.type==SDL_WINDOWEVENT && (event.window.event==SDL_WINDOWEVENT_RESIZED ||
            event.window.event==SDL_WINDOWEVENT_SIZE_CHANGED)) {
            if(event.window.data1 < SCR_MINW || event.window.data2 < SCR_MINH) {
                if(event.window.data1 < SCR_MINW) event.window.data1 = SCR_MINW;
                if(event.window.data2 < SCR_MINH) event.window.data2 = SCR_MINH;
                SDL_SetWindowSize(window, event.window.data1, event.window.data2);
            }
            ui_resize(event.window.data1, event.window.data2);
            ui_redraw(0, 9999999);
            rect.x = (scr_w - w) / 2; rect.y = (scr_h - 64) / 2;
            continue;
        }
        if(event.type==SDL_KEYDOWN) {
            if(event.key.keysym.sym == SDLK_ESCAPE) ret = -1;
            if(event.key.keysym.sym == SDLK_RETURN) ret = 0;
        } else
        if(event.type==SDL_KEYUP) break;
        else
        if(event.type==SDL_MOUSEBUTTONDOWN) {
            ret = -1;
            if(event.button.y >= rect.y + 40 && event.button.y < rect.y + 50 &&
              event.button.x >= scr_w / 2 - 103 && event.button.x <= scr_w / 2 + 103) {
                i = (event.button.x - (scr_w / 2 - 103)) / 12;
                if(i == 16) ret = i; else
                if(i < 16 && pal[i * sp + 3] && (pal[i * sp + 0] || pal[i * sp + 1] || pal[i * sp + 2])) ret = i;
            }
        }
        if(event.type==SDL_MOUSEBUTTONUP) {
            ret = -1;
            if(event.button.y < rect.y || event.button.y > rect.y + rect.h ||
              event.button.x < rect.x || event.button.x > rect.x + rect.w) break;
            if(event.button.y >= rect.y + 40 && event.button.y < rect.y + 50 &&
              event.button.x >= scr_w / 2 - 103 && event.button.x <= scr_w / 2 + 103) {
                i = (event.button.x - (scr_w / 2 - 103)) / 12;
                if(i == 16) { ret = i; break; }
                if(i < 16 && pal[i * sp + 3] && (pal[i * sp + 0] || pal[i * sp + 1] || pal[i * sp + 2])) { ret = i; break; }
            }
        }
    }
    if(fade) SDL_DestroyTexture(fade);
    SDL_DestroyTexture(modal);
    ui_render();
    SDL_RenderPresent(renderer);
    return ret;
}

/**
 * Get layout list
 */
void newsprite_getlyts()
{
    int num;

    project_freedir(&newsprite_lytcombo.sel.opts, NULL);
    strcpy(projfn, "." COMMONDIR);
    newsprite_lytcombo.sel.opts = project_getdir(projdir, ".lyt", &num);
    newsprite_lytcombo.sel.opts = (char**)realloc(newsprite_lytcombo.sel.opts, (num + 2) * sizeof(char*));
    if(!newsprite_lytcombo.sel.opts) main_error(ERR_MEM);
    memmove(&newsprite_lytcombo.sel.opts[1], &newsprite_lytcombo.sel.opts[0], (num + 1) * sizeof(char*));
    newsprite_lytcombo.sel.opts[0] = (char*)main_alloc(16);
    strcpy(newsprite_lytcombo.sel.opts[0], BUILTINLYT);
    newsprite_lyt[0] = 0;
}

/**
 * Load a layout and apply it to get the import list
 */
void newsprite_loadlyt(void *data)
{
    ui_implist_t *imp;
    uint8_t *pal = NULL;
    char *str, *s, *n = NULL;
    int i, type, dir, sx, ex, sy, ey, p, sp = -1;

    (void)data;
    if(!newsprite_lyt[0] || !newsprite_sheet) return;
    /* check if dynamic recoloring needed:
     * 1. entire image is parsed by the layout
     * 2. has a 8x16 palette at the bottom right corner, that is: we have alpha 0 at (w-9, h-17), (w-8, h-17) and
     *    (w-9, h-16); pixel at (w-8, h-16) has alpha of 255 and a non-black color)
     * if so, then ask the user to choose a color variant */
    if(newsprite_cropex == -1 && newsprite_cropey == -1 && newsprite_scr.ow > 8 && newsprite_scr.oh > 16) {
        SDL_LockTexture(newsprite_sheet, NULL, (void**)&pal, &p);
        pal += (newsprite_scr.oh - 16) * p + (newsprite_scr.ow - 8) * 4;
        sp = (!pal[-1] && !pal[3 - p] && !pal[-(p + 1)] && pal[3] == 255 && (pal[0] || pal[1] || pal[2])) ?
            newsprite_variantmodal(pal, p) : -2;
        SDL_UnlockTexture(newsprite_sheet);
        if(sp == -1) { newsprite_lyt[0] = 0; return; }
        if(sp == -2) sp = -1;
    }
    if(!strcmp(newsprite_lyt, BUILTINLYT)) {
        newsprite_tngbase(newsprite_sheet, newsprite_scr.ow, newsprite_scr.oh, sp);
        if(sp == 16) newsprite_addpal();
        return;
    }
    str = project_loadfile("." COMMONDIR, newsprite_lyt, "lyt", PROJMAGIC_LAYOUT, "newsprite_loadlyt");
    if(!str) { ui_status(1, lang[ERR_LOADLYT]); return; }
    newsprite_empty();
    s = project_skipnl(str);
    s = project_getint(s, &newsprite_gridw, 0, 1024); s = project_getint(s, &newsprite_gridh, 0, 1024);
    s = project_getint(s, &newsprite_gridl, 0, 1024); s = project_getint(s, &newsprite_gridt, 0, 1024);
    s = project_getint(s, &newsprite_gridV, 0, 1024); s = project_getint(s, &newsprite_gridH, 0, 1024);
    for(i = 0; i < 5; i++)
        if(!memcmp(s, project_dirs[PROJDIRS_SPRITES + i], 2)) { sprites_cat = i; break; }
    while(*s) {
        s = project_skipnl(s);
        if(!*s) break;
        s = project_getidx(s, &dir, (char**)project_directions, 0);
        while(*s && *s == ' ') s++;
        for(type = 0; project_animtypes[type] && *s != project_animtypes[type]; type++);
        if(*s != project_animtypes[type]) type = 0;
        s = project_getstr(s + 1, &n, 1, PROJ_NAMEMAX);
        while(*s && *s == ' ') s++;
        imp = newsprite_new(n, type, dir);
        if(n) { free(n); n = NULL; }
        while(*s == '(') {
            s = project_getint(s + 1, &sx, 0, 1024); s = project_getint(s + 1, &sy, 0, 1024);
            if(*s == ',') { s = project_getint(s + 1, &ex, 0, 1024); s = project_getint(s + 1, &ey, 0, 1024); }
            else { ex = sx; ey = sy; }
            while(*s == ')' || *s == ' ') s++;
            if(newsprite_cropex != -1 && newsprite_cropey != -1) {
                sx += newsprite_cropsx; sy += newsprite_cropsy; ex += newsprite_cropsx; ey += newsprite_cropsy;
                if(ex > newsprite_cropex) ex = newsprite_cropex;
                if(ey > newsprite_cropey) ey = newsprite_cropey;
            }
            if(ex - sx >= 0 && ey - sy >= 0)
                newsprite_addfromgrid(imp, sx, ex, sy, ey, sp);
        }
    }
    free(str);
    if(sp == 16) newsprite_addpal();
    newsprite_recalcgrid(NULL);
    newsprite_cleanupimp();
}

/**
 * Save a layout
 */
void newsprite_savelyt(void *data)
{
    ui_implist_t *imp = (ui_implist_t *)newsprite_table.data;
    FILE *f;
    int i = 0, j, sx = 0, sy = 0;

    (void)data;
    if(!newsprite_lyt[0] || !strcmp(newsprite_lyt, BUILTINLYT) || !newsprite_table.num || !newsprite_sheet || !imp) return;
    f = project_savefile(1, "." COMMONDIR, newsprite_lyt, "lyt", PROJMAGIC_LAYOUT, "newsprite_savelyt");
    if(!f) { ui_status(1, lang[ERR_SAVELYT]); return; }
    fprintf(f, "%d %d %d %d %d %d %s\r\n", newsprite_gridw, newsprite_gridh, newsprite_gridl,
        newsprite_gridt, newsprite_gridV, newsprite_gridH, project_dirs[PROJDIRS_SPRITES + sprites_cat]);
    if(newsprite_cropex != -1 && newsprite_cropey != -1) { sx = newsprite_cropsx; sy = newsprite_cropsy; }
    for(i = 0; i < newsprite_table.num; i++) {
        fprintf(f, "%s %c %s", project_directions[(int)imp[i].dir], project_animtypes[(int)imp[i].type],
            imp[i].name[0] ? imp[i].name : "-");
        if(imp[i].grid)
            for(j = 0; j < imp[i].nframe; j++)
                if(imp[i].grid[j * 4] >= sx && imp[i].grid[j * 4 + 1] >= sy) {
                    if(imp[i].grid[j * 4] == imp[i].grid[j * 4 + 2] && imp[i].grid[j * 4 + 1] == imp[i].grid[j * 4 + 3])
                        fprintf(f, " (%u,%u)", imp[i].grid[j * 4] - sx, imp[i].grid[j * 4 + 1] - sy);
                    else
                        fprintf(f, " (%u,%u,%u,%u)", imp[i].grid[j * 4] - sx, imp[i].grid[j * 4 + 1] - sy,
                            imp[i].grid[j * 4 + 2] - sx, imp[i].grid[j * 4 + 3] - sy);
                }
        fprintf(f, "\r\n");
    }
    fclose(f);
    ui_status(0, lang[LANG_SAVED]);
    newsprite_getlyts();
}

/**
 * Load a sprite sheet
 */
int newsprite_loadsheet(char *fn)
{
    uint8_t *data = NULL, *img = NULL;
    char *s, *d;
    int p, w, h;

    if(!fn) return 1;
    newsprite_free(NULL);
    newsprite_lyt[0] = newsprite_spr[0] = 0;
    if((img = image_load(fn, &w, &h)) && w > 0 && h > 0) {
        if(verbose) printf("newsprite_loadsheet: loaded %s %d x %d\n", fn, w, h);
        ui_scrarea_reset(&newsprite_scr, w, h);
        newsprite_sheet = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, w, h);
        if(!newsprite_sheet) { free(img); ui_status(1, lang[ERR_MEM]); return 0; }
        SDL_SetTextureBlendMode(newsprite_sheet, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(newsprite_sheet, NULL, (void**)&data, &p);
        memcpy(data, img, w * h * 4);
        SDL_UnlockTexture(newsprite_sheet);
        free(img);
        s = strrchr(fn, '.'); if(s) *s = 0; else s = fn + strlen(fn);
        if(s - fn > 5 && s[-5] == '_' && (!strcmp(s - 4, "atls") || !strcmp(s - 4, "full") ||
          (strchr("swne", s[-4]) && strchr(project_animtypes, s[-2]) && strchr(project_animframes, s[-1])))) s[-5] = 0;
        s = strrchr(fn, SEP[0]); if(s) s++; else s = fn;
        for(d = newsprite_spr; *s && d < &newsprite_spr[sizeof(newsprite_spr)-1]; s++)
            if((*s >= '0' && *s <= '9') || (*s >= 'a' && *s <= 'z') || (*s >= 'A' && *s <= 'Z') || *s == '_' || *s == '.')
                *d++ = *s;
        *d = 0;
        newsprite_recalcgrid(NULL);
        return 1;
    }
    ui_status(1, lang[ERR_IMAGE]);
    return 0;
}

/**
 * Actually save import list into separate png files
 */
void newsprite_savesheet(void *data)
{
    FILE *f;
    char fn[FILENAME_MAX + PROJ_NAMEMAX + 12], *n = fn, *e = NULL;
    ui_implist_t *imp = (ui_implist_t *)newsprite_table.data;
    int i, p, err = 0, id = 1;
    uint8_t *ptr;

    (void)data;
    if(newsprite_spr[0]) {
        strcpy(fn, newsprite_spr);
        fn[FILENAME_MAX - PROJ_NAMEMAX - 11] = 0;
        n = e = fn + strlen(fn);
        *n++ = '_';
    }
    if(newsprite_form[6].inactive) {
        if(!newsprite_sheet) return;
        if(!newsprite_spr[0]) { ui_status(1, lang[ERR_SPRNAME]); return; }
        strcpy(n, "full");
        f = project_savefile(0, project_dirs[PROJDIRS_SPRITES + sprites_cat], fn, "png", NULL, "newsprite_savesheet");
        if(f) {
            SDL_LockTexture(newsprite_sheet, NULL, (void**)&ptr, &p);
            if(!image_save(f, newsprite_scr.ow, newsprite_scr.oh, p, ptr)) err++;
            else {
                *e = 0;
                spr_add(sprites_cat, fn, 0x7F, 0, 1, newsprite_scr.ow, newsprite_scr.oh, p, ptr);
            }
            SDL_UnlockTexture(newsprite_sheet);
            fclose(f);
        } else err++;
    } else {
        if(!imp || !newsprite_table.num) return;
        for(i = 0; i < newsprite_table.num; i++)
            if(!imp[i].name[0]) {
                if(!ui_modal(ICON_WARN, ERR_SPRNAMENUM, NULL)) {
                    newsprite_table.val = i;
                    ui_status(1, lang[ERR_SPRNAME]);
                    return;
                } else
                    break;
            } else
                for(p = 0; p < i; p++)
                    if(imp[p].name[0] && !strcmp(imp[p].name, imp[i].name) && imp[p].dir == imp[i].dir && imp[p].type == imp[i].type) {
                        newsprite_table.val = i;
                        ui_status(1, lang[ERR_SPRNAMEUNIQ]);
                        return;
                    }

        for(i = 0; i < newsprite_table.num; i++) {
            if(!imp[i].data || imp[i].nframe < 1 || imp[i].dir < 0 || imp[i].dir > 7 || imp[i].type < -1 || imp[i].type > 2)
                continue;
            if(!imp[i].name[0])
                sprintf(n, "%03d", id++);
            else
                strncpy(n, imp[i].name, PROJ_NAMEMAX);
            if(imp[i].dir < 0 || imp[i].dir > 7) imp[i].dir = 0;
            if(imp[i].type < -1 || imp[i].type > 2) imp[i].type = 0;
            if(imp[i].nframe > 32) imp[i].nframe = 32;
            if(imp[i].nframe < 1) imp[i].nframe = 1;
            if(imp[i].nframe > 32) imp[i].nframe = 32;
            e = n + strlen(n); *e++ = '_';
            strcpy(e, project_directions[(int)imp[i].dir]);
            e[2] = imp[i].type < 0 ? project_animtypes[0] : project_animtypes[(int)imp[i].type];
            e[3] = project_animframes[imp[i].nframe - 1];
            e[4] = 0;
            f = project_savefile(0, project_dirs[PROJDIRS_SPRITES + (imp[i].type < 0 ? 0 : sprites_cat)], fn, "png", NULL,
                "newsprite_savesheet");
            if(f) {
                SDL_LockTexture(imp[i].data, NULL, (void**)&ptr, &p);
                if(!image_save(f, imp[i].w, imp[i].h, p, ptr)) err++;
                else {
                    e[-1] = 0;
                    spr_add(imp[i].type < 0 ? 0 : sprites_cat, fn, imp[i].dir, imp[i].type < 0 ? 0 : imp[i].type, imp[i].nframe,
                        imp[i].ow, imp[i].h, p, ptr);
                    e[-1] = '_';
                }
                SDL_UnlockTexture(imp[i].data);
                fclose(f);
            } else err++;
            ui_progressbar(0, 0, i, newsprite_table.num, SPRITES_SAVING);
        }
        ui_progressbar(0, 0, 0, 0, SPRITES_SAVING);
    }
    if(err) ui_status(1, lang[ERR_SAVESPR]);
    else newsprite_empty();
}
