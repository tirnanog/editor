/*
 * tnge/fonts.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief New project window
 *
 */

#include "main.h"

ssfn_t *fonts = NULL;
ssfn_font_t **fontdata = NULL;

uint32_t fonts_color;
char fonts_text[64];
int fonts_style = 0, fonts_size = 16;
void fonts_delete(void *data);
void fonts_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void fonts_new(void *data) { (void)data; fileops_type = FILEOPS_FONT; ui_switchtab(SUBMENU_IMPORT); }
void fonts_textchange(void *data) { (void)data; strncpy(fonts_text, ui_input_buf, sizeof(fonts_text) - 1); }

ui_icontgl_t fonts_bold = { ICON_BOLD, SSFN_STYLE_BOLD, &fonts_style };
ui_icontgl_t fonts_italic = { ICON_ITALIC, SSFN_STYLE_ITALIC, &fonts_style };
ui_numptr_t fonts_num = { &fonts_size, 8, 128, 1 };
ui_input_t fonts_textinp = { INP_NAME, sizeof(fonts_text), fonts_text };

ui_tablehdr_t fonts_table_hdr[] = {
    { FONTS_NAME, 160, 0, 0 },
    { FONTS_PREVIEW, 0, 0, 0 },
    { 0 }
};

ui_table_t fonts_table = { fonts_table_hdr, FONTS_NONE, 34, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0,
    sizeof(char*), fonts_drawcell, NULL, NULL, NULL };

/**
 * The form
 */
ui_form_t fonts_form[] = {
    { FORM_TABLE, 10, 29, 0, 0, 0, 0, &fonts_table, NULL },
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, FONTS_DELETE, (void*)ICON_REMOVE, fonts_delete },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, FONTS_ADD, (void*)ICON_ADD, fonts_new },
    { FORM_COLOR, 0, 0, 18, 18, 0, 0, &fonts_color, NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, FONTS_BOLD, &fonts_bold, NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, FONTS_ITALIC, &fonts_italic, NULL },
    { FORM_NUMPTR, 0, 0, 40, 18, 0, FONTS_SIZE, &fonts_num, NULL },
    { FORM_INPUT, 0, 0, 200, 18, 0, 0, &fonts_textinp, fonts_textchange },
    { FORM_LAST }
};

/**
 * Draw table cell
 */
void fonts_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    ssfn_font_t *font;
    ssfn_buf_t buf = { 0 };
    int dy = h / 2, ow = ui_clip.w, ret;
    char tmp[32], *str;
    uint8_t *c = (uint8_t*)&fonts_color;

    (void)dst; (void)sel;
    ui_clip.w = hdr[0].w - 8;
    ui_text(dst, x + 4, y + dy - 16, *((char**)data));
    font = fonts[idx].numbuf ? (ssfn_font_t*)fonts[idx].bufs[0] : fontdata[idx];
    if(font->width == 255 || font->height == 255)
        sprintf(tmp, "(%s)", lang[FONTS_VECTOR]);
    else
        sprintf(tmp,"%3d x%3d", font->width, font->height);
    ui_text(dst, x + 4, y + dy, tmp);
    ui_clip.w = ow;
    ssfn_select(&fonts[idx], SSFN_FAMILY_ANY, NULL, fonts_style | SSFN_STYLE_NOCACHE, fonts_size);
    buf.ptr = (uint8_t*)scr_data + y * scr_p + (x + hdr[0].w) * 4; buf.y = h - fonts_size / 2;
    buf.fg = 0xFF000000 | (c[0] << 16) | (c[1] << 8) | c[2]; buf.w = -1 * (w - hdr[0].w); buf.h = h; buf.p = scr_p;
    if(y + buf.h > ui_clip.y + ui_clip.h) buf.h = ui_clip.y + ui_clip.h - y;
    str = fonts_text[0] ? fonts_text : "0123456789 aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ f i fi ffi";
    for(; (ret = ssfn_render(&fonts[idx], &buf, str)) > 0 || ret == SSFN_ERR_NOGLYPH; str += (ret < 1 ? 1 : ret));
}

/**
 * Exit fonts window
 */
void fonts_exit(int tab)
{
    int i;

    (void)tab;
    if(fontdata) {
        for(i = 0; i < fonts_table.num; i++)
            if(fontdata[i]) free(fontdata[i]);
        free(fontdata);
        fontdata = NULL;
    }
    if(fonts) {
        for(i = 0; i < fonts_table.num; i++) ssfn_free(&fonts[i]);
        free(fonts);
        fonts = NULL;
    }
    fonts_table.val = fonts_table.scr = 0;
    fonts_table.clk = -1;
}

/**
 * Enter fonts window
 */
void fonts_init(int tab)
{
    char *fn;
    unsigned long int size;
    int i;
    FILE *f;

    fonts_exit(tab);
    fonts_color = theme[THEME_FG];
    fonts_size = 16;
    fonts_style = 0;
    fonts_text[0] = 0;
    project_freedir(&project.fonts, &project.numfont);
    strcpy(projfn, project.id); strcat(projfn, SEP); strcat(projfn, project_dirs[PROJDIRS_FONTS]);
    project.fonts = project_getdir(projdir, ".sfn", &project.numfont);
    if(project.fonts && project.fonts[0] && project.fonts[0][0]) {
        fonts_table.data = project.fonts;
        fonts_table.num = project.numfont;
        fonts = (ssfn_t*)main_alloc(fonts_table.num * sizeof(ssfn_t));
        fontdata = (ssfn_font_t**)main_alloc(fonts_table.num * sizeof(ssfn_font_t*));
        fn = projfn + strlen(projfn); *fn++ = SEP[0];
        for(i = 0; i < fonts_table.num; i++) {
            strcpy(fn, project.fonts[i]);
            strcat(fn, ".sfn");
            f = project_fopen(projdir, "rb");
            if(f) {
                fseek(f, 0, SEEK_END);
                size = (unsigned long int)ftell(f);
                fseek(f, 0, SEEK_SET);
                fontdata[i] = (ssfn_font_t*)malloc(size + 1);
                if(fontdata[i]) {
                    memset(fontdata[i], 0, size + 1);
                    if(fread(fontdata[i], 1, size, f) == size)
                        ssfn_load(&fonts[i], fontdata[i]);
                }
                fclose(f);
            }
        }
    }
}

/**
 * Resize the view
 */
void fonts_resize(int tab, int w, int h)
{
    int d = (w/2 - 80 - fonts_form[6].w) / 2;

    (void)tab;
    ui_table_resize(&fonts_form[0], w - 20, h - 89);
    fonts_form[1].y = fonts_form[2].y = fonts_form[3].y = fonts_form[4].y = fonts_form[5].y = fonts_form[6].y =
        fonts_form[7].y = h - 48;
    fonts_form[2].x = w - 40;
    fonts_form[3].x = d; d += 24;
    fonts_form[4].x = d; d += 24;
    fonts_form[5].x = d; d += 24;
    fonts_form[6].x = d;
    fonts_form[7].x = d + 4 + fonts_form[6].w;
    fonts_form[7].w = w/2;
}

/**
 * View layer
 */
void fonts_redraw(int tab)
{
    (void)tab;
    ui_form = fonts_form;
    fonts_table.row = fonts_size * 2;
    if(fonts_table.row < 34) fonts_table.row = 34;
    fonts_form[1].inactive = fonts_table.val < 0;
}

/**
 * Delete button handler
 */
void fonts_delete(void *data)
{
    char **fontlist = (char**)fonts_table.data;

    (void)data;
    if(fonts_table.val < 0) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.sfn", project.id, project_dirs[PROJDIRS_FONTS], fontlist[fonts_table.val]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("fonts_delete: removing %s\n", projdir);
        remove(projdir);
        fonts_init(SUBMENU_FONTS);
    }
    *projfn = 0;
}

/**
 * Import font
 * returns true on success
 */
int fonts_import(char *path)
{
    FILE *f;
    int i;
    char *s = strrchr(path, SEP[0]), *d, *e;

    /* generate sfn filename with path */
    if(!s) s = path; else s++;
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_FONTS]);
    project_mkdir(projfn);
    strcat(projfn, SEP);
    for(d = e = projfn + strlen(projfn); *s; s++)
        if((*s >= '0' && *s <= '9') || (*s >= 'a' && *s <= 'z') || (*s >= 'A' && *s <= 'Z') ||
          *s == '_' || *s == '-' || *s == '.')
            *d++ = *s;
    s = strrchr(projfn, '.');
    if(s < e) s = e;
    if(s && !strcmp(s, ".gz")) { *s = 0; s = strrchr(projfn, '.'); }
    if(!s) s = projfn + strlen(projfn);
    strcpy(s, ".sfn");
    if(verbose) printf("fonts_import: importing %s (range %x - %x) to %s\n", path, rs, re, projdir);
    /* convert font */
    i = 1;
    f = project_fopen(projdir, "rb");
    if(f) {
        fclose(f);
        i = ui_modal(ICON_WARN, ERR_EXISTS, projfn);
    }
    if(i) {
        /* FONTS_IMPORT is used in libsfn.h */
        sfn_init(ui_progressbar);
        sfn_load(path);
        sfn_sanitize(-1);
        sfn_save(projdir);
        sfn_free();
        *projfn = 0;
        return 1;
    }
    return 0;
}

/**
 * Save tng
 */
int fonts_totng(tngctx_t *ctx)
{
    uint32_t cl;
    int i;

    if(!ctx || !project.fonts || !project.numfont) return 1;
    /* check if there's at least one font referenced */
    for(i = 0; i < project.numfont && !ctx->idx[TNG_IDX_FNT].ref[i]; i++);
    if(i >= project.numfont) return 1;
    tng_section(ctx, TNG_SECTION_FONTS);
    for(i = 0; i < project.numfont; i++) {
        if(!ctx->idx[TNG_IDX_FNT].ref[i]) continue;
        sprintf(projfn, "%s" SEP "%s" SEP "%s.sfn", project.id, project_dirs[PROJDIRS_FONTS], project.fonts[i]);
        cl = project_filesize(projdir);
        if(!cl) {
            ui_switchtab(SUBMENU_FONTS);
            fonts_table.val = i;
            ui_status(1, lang[ERR_LOADUI]);
            return 0;
        }
        tng_desc(ctx, project.fonts[i], cl);
        tng_asset_ext(ctx, projdir, cl);
    }
    return 1;
}

/**
 * Read from tng
 */
int fonts_fromtng(tngctx_t *ctx)
{
    tng_asset_desc_t *asset;
    int i, len;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->f) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_FONTS; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_FONTS]);
    project_mkdir(projdir);
    for(i = 0; i < len; i++, asset++) {
        if(asset->size < 1) continue;
        sprintf(projfn, "%s" SEP "%s" SEP "%s.sfn", project.id, project_dirs[PROJDIRS_FONTS], ctx->sts + asset->name);
        if(verbose) printf("fonts_fromtng: saving %s\n", projdir);
        tng_save_asset(ctx, asset->offs, asset->size, projdir);
        ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
    }
    return 1;
}
