/*
 * tnge/cutscn.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Cutscene editor window
 *
 */

#include "main.h"
#include "theora.h"

#define CUTSCN_MAXLEN 30*60*100

extern int maps_deltay;

void cutscn_loadsamples(void *data);
void cutscn_freesamples();
void cutscn_delslide(void *data);
void cutscn_setslide(void *data);
void cutscn_setcolor(void *data);
void cutscn_delspeech(void *data);
void cutscn_setspeech(void *data);
void cutscn_delsub(void *data);
void cutscn_setsub(void *data);
void cutscn_erasesub(void *data);
void cutscn_addsub(void *data);
void cutscn_crdhelper(void *data);
void cutscn_play(void *data);
void cutscn_load(void *data);
void cutscn_new(void *data);
void cutscn_save(void *data);
void cutscn_delete(void *data);
void cutscn_preview(void *data);
void cutscn_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

Mix_Chunk *cutscn_samples = NULL, cutscn_oncesample = { 0 }, cutscn_loopsample = { 0 };

char *cutscn_aligncodes[4] = { "l", "c", "r", NULL };
char cutscn_name[PROJ_NAMEMAX], cutscn_lens[4][16] = { 0 }, cutscn_crdtxt[16];
int cutscn_lasti = -1;
uint32_t cutscn_imgcolor = 0;
ui_input_t cutscn_nameinp = { INP_ID, sizeof(cutscn_name), cutscn_name };

ui_tablehdr_t cutscn_hdr[] = {
    { SUBMENU_CUTSCENES, 0, 0, 0 },
    { 0 }
};
ui_table_t cutscn_tbl = { cutscn_hdr, CUTSCN_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), NULL, NULL,
    NULL, NULL };

ui_cutscn_t cutscn_edit = { 0 };
ui_select_t cutscn_bg[4] = {
    { -1, LANG_NONE, NULL },
    { -1, LANG_NONE, NULL },
    { -1, LANG_NONE, NULL },
    { -1, LANG_NONE, NULL }
};

ui_tablehdr_t cutscn_bghdr[] = {
    { -1, 24, 0, 0 },
    { CUTSCN_BGMOV, 0, 0, 0 },
    { CUTSCN_BGMUS, 0, 0, 0 },
    { 0 }
};
ui_table_t cutscn_bgtbl = { cutscn_bghdr, 0, 24, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), NULL, NULL, NULL, NULL };

ui_table_t cutscn_subtbl = { NULL, CUTSCN_NOSUB, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(ui_sub_t), cutscn_drawcell, NULL,
    NULL, NULL };

ui_numptr_t cutscn_sels = { &cutscn_edit.sels, 0, 0, 1 };
ui_numptr_t cutscn_sele = { &cutscn_edit.sele, 0, 0, 1 };
ui_fade_t cutscn_imgf = { NULL, NULL, &cutscn_edit.sels, &cutscn_edit.sele, 0, 0 };
ui_sprsel_t cutscn_imgspr = { -1, 4, 25, 1 };
ui_select_t cutscn_spc = { -1, LANG_NONE, NULL };
ui_fade_t cutscn_subf = { NULL, NULL, &cutscn_edit.sels, &cutscn_edit.sele, 0, 0 };
ui_icongrpelem_t cutscn_aligns[3] = { { 177, CUTSCN_ALN_LEFT }, { 178, CUTSCN_ALN_CENTRE }, { 179, CUTSCN_ALN_RIGHT } };
ui_icongrp_t cutscn_align = { NULL, 0, 3, cutscn_aligns };
ui_icontgl_t cutscn_bold = { ICON_BOLD, SSFN_STYLE_BOLD, NULL };
ui_icontgl_t cutscn_italic = { ICON_ITALIC, SSFN_STYLE_ITALIC, NULL };
ui_numptr_t cutscn_size = { NULL, 16, 192, 1 };
ui_selptr_t cutscn_font = { NULL, { -1, LANG_NONE, NULL } };
ui_input_t cutscn_subinp = { INP_NAME, PROJ_TITLEMAX, NULL };
ui_cmdpal_t cutscn_pal = { CMDPAL_CUTSCENE, 0, 0 };

/**
 * The form
 */
ui_form_t cutscn_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, CUTSCN_DELETE, (void*)ICON_REMOVE, cutscn_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, cutscn_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, CUTSCN_PREVIEW, (void*)ICON_PVIEW, cutscn_preview },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, cutscn_new },
    { FORM_INPUT, 0, 30, 200, 20, 0, CUTSCN_NAME, &cutscn_nameinp, NULL },
    { FORM_TABLE, 10, 30, 200, 0, 0, 0, &cutscn_tbl, cutscn_load },
    /* 6 */
    { FORM_CUTSCNSEL, 0, 30, 152, 20, 0, 0, &cutscn_edit, NULL },
    { FORM_ICONBTN, 0, 30, 20, 20, 0, CUTSCN_PLAY, (void*)ICON_PLAY, cutscn_play },
    { FORM_CUTSCN, 0, 54, 0, 192, 0, 0, &cutscn_edit, NULL },
    /* 9 */
    { FORM_COMMENT, 0, 256+32, 0, 18, 0, 0, NULL, NULL },
    { FORM_COMMENT, 0, 256+32, 0, 18, 0, 0, NULL, NULL },
    { FORM_TIMEPTR, 0, 256, 80, 20, 0, 0, &cutscn_sels, NULL },
    { FORM_TIMEPTR, 0, 256, 80, 20, 0, 0, &cutscn_sele, NULL },
    /* 13 */
    { FORM_DRAWTBL, 0, 256, 0, 71, 0, 0, &cutscn_bgtbl, NULL },
    { FORM_TEXT, 0, 256+25, 16, 18, 0, CUTSCN_ONCE, "\x6", NULL },
    { FORM_MOVSEL, 0, 256+23, 0, 20, 0, 0, &cutscn_bg[0], cutscn_loadsamples },
    { FORM_TEXT, 0, 256+23, 80, 18, 0, 0, &cutscn_lens[0], NULL },
    { FORM_MUSSEL, 0, 256+23, 0, 20, 0, 0, &cutscn_bg[1], cutscn_loadsamples },
    { FORM_TEXT, 0, 256+23, 80, 18, 0, 0, &cutscn_lens[1], NULL },
    { FORM_TEXT, 0, 256+49, 16, 18, 0, CUTSCN_LOOP, "\x8", NULL },
    { FORM_MOVSEL, 0, 256+47, 0, 20, 0, 0, &cutscn_bg[2], cutscn_loadsamples },
    { FORM_TEXT, 0, 256+47, 80, 18, 0, 0, &cutscn_lens[2], NULL },
    { FORM_MUSSEL, 0, 256+47, 0, 20, 0, 0, &cutscn_bg[3], cutscn_loadsamples },
    { FORM_TEXT, 0, 256+47, 80, 18, 0, 0, &cutscn_lens[3], NULL },
    /* 24 */
    { FORM_FADE, 0, 256, 0, 20, 0, CUTSCN_FADE, &cutscn_imgf, NULL },
    { FORM_ICONBTN, 0, 256, 20, 20, 0, CUTSCN_DELSLD, (void*)ICON_ERASE, cutscn_delslide },
    { FORM_SPRITE, 0, 256 + 32, 0, 0, 0, CUTSCN_SLIDE, &cutscn_imgspr, cutscn_setslide },
    { FORM_COLOR, 0, 256 + 32, 20, 20, 0, CUTSCN_COLOR, &cutscn_imgcolor, cutscn_setcolor },
    /* 28 */
    { FORM_SPCSEL, 0, 256, 0, 20, 0, CUTSCN_FADE, &cutscn_spc, cutscn_setspeech },
    { FORM_ICONBTN, 0, 256, 20, 20, 0, CUTSCN_DELSPC, (void*)ICON_ERASE, cutscn_delspeech },
    /* 30 */
    { FORM_FADE, 0, 256, 0, 20, 0, CUTSCN_FADE, &cutscn_subf, NULL },
    { FORM_ICONBTN, 0, 256, 20, 20, 0, CUTSCN_DELSUB, (void*)ICON_ERASE, cutscn_delsub },
    { FORM_TEXT, 0, 256+32, 112, 18, 0, CUTSCN_POS, &cutscn_crdtxt, NULL },
    { FORM_CHARBTN, 0, 256+32, 20, 20, 0, CUTSCN_POS, (void*)162, cutscn_crdhelper },
    { FORM_CHARGRP, 0, 256+32, 68, 20, 0, 0, &cutscn_align, NULL },
    { FORM_COLOR, 0, 256+32, 20, 20, 0, FONTS_COLOR, NULL, NULL },
    { FORM_ICONTGL, 0, 256+32, 20, 20, 0, FONTS_BOLD, &cutscn_bold, NULL },
    { FORM_ICONTGL, 0, 256+32, 20, 20, 0, FONTS_ITALIC, &cutscn_italic, NULL },
    { FORM_NUMPTR, 0, 256+32, 42, 20, 0, FONTS_SIZE, &cutscn_size, NULL },
    { FORM_SELPTR, 0, 256+32, 0, 20, 0, FONTS_FAMILY, &cutscn_font, NULL },
    { FORM_INPUT, 0, 256+32+24, 0, 20, 0, LANG_ATTRSTR, &cutscn_subinp, NULL },
    { FORM_TABLE, 0, 256+64+24, 0, 0, 0, 0, &cutscn_subtbl, cutscn_setsub },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CUTSCN_REMSUB, (void*)ICON_REMOVE, cutscn_erasesub },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, CUTSCN_ADDSUB, (void*)ICON_COPY, cutscn_addsub },
    /* 44 */
    { FORM_COMMANDS, 0, 256, 0, 0, 0, CUTSCN_SCRIPT, &cutscn_edit.cmd, NULL },
    { FORM_CMDPAL, 0, 256, 40, 0, 0, 0, &cutscn_pal, NULL },
    /* 46 */
    { FORM_LAST }
};

/**
 * Draw table cell
 */
void cutscn_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    ui_sub_t *sub = (ui_sub_t*)data;
    char tmp[16];

    (void)idx; (void)sel; (void)hdr; (void)w; (void)h;
    if(!data) return;
    media_dur(sub->s, tmp);
    ui_text(dst, x + 4, y + 1, tmp);
    media_dur(sub->e, tmp);
    ui_text(dst, x + 76, y + 1, tmp);
    ui_text(dst, x + 148, y + 1, sub->txt);
}

/**
 * Exit cutscenes window
 */
void cutscn_exit(int tab)
{
    (void)tab;
    cutscn_tbl.clk = -1;
}

/**
 * Enter cutscenes window
 */
void cutscn_init(int tab)
{
    cutscn_exit(tab);
    cutscn_form[1].param = lang[LANG_SAVE];
    cutscn_form[1].w = ui_textwidth(cutscn_form[1].param) + 40;
    if(cutscn_form[1].w < 200) cutscn_form[1].w = 200;
    cutscn_form[9].param = lang[CUTSCN_NOBG];
    cutscn_form[9].w = ui_textwidth(cutscn_form[9].param);
    cutscn_form[10].param = lang[CUTSCN_NOSEL];
    cutscn_form[10].w = ui_textwidth(cutscn_form[10].param);
    project_freedir(&project.cuts, &project.numcuts);
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_UI]);
    project.cuts = project_getdir(projdir, ".cut", &project.numcuts);
    if(!cutscn_name[0]) cutscn_tbl.val = -1;
    cutscn_tbl.data = project.cuts;
    cutscn_tbl.num = project.numcuts;
    if(cutscn_tbl.val >= cutscn_tbl.num) cutscn_tbl.val = cutscn_tbl.num - 1;
    if(cutscn_tbl.val < 0)
        cutscn_freesamples();
    cutscn_edit.seli = -1;
    cutscn_font.sel.opts = project.fonts;
}

/**
 * Resize the view
 */
void cutscn_resize(int tab, int w, int h)
{
    (void)tab;
    cutscn_form[0].y = cutscn_form[1].y = cutscn_form[2].y = cutscn_form[3].y = h - 48;
    cutscn_form[1].x = w - 20 - cutscn_form[1].w;
    cutscn_form[2].x = cutscn_form[1].x - 52;
    ui_table_resize(&cutscn_form[5], cutscn_form[5].w, h - 89);
    cutscn_form[3].x = cutscn_form[5].x + cutscn_form[5].w + 20;
    cutscn_form[4].x = w - 10 - cutscn_form[4].w;
    cutscn_form[6].x = cutscn_form[8].x = cutscn_form[11].x = cutscn_form[13].x = cutscn_form[26].x = cutscn_form[32].x =
        cutscn_form[40].x = cutscn_form[41].x = cutscn_form[42].x = cutscn_form[44].x = cutscn_form[5].x + cutscn_form[5].w + 10;
    cutscn_form[7].x = cutscn_form[6].x + cutscn_form[6].w + 10;
    cutscn_form[8].w = w - 10 - cutscn_form[8].x;
    cutscn_form[9].x = cutscn_form[6].x + (w - 10 - cutscn_form[6].x - cutscn_form[9].w) / 2;
    cutscn_form[10].x = cutscn_form[6].x + (w - 10 - cutscn_form[6].x - cutscn_form[10].w) / 2;
    cutscn_form[12].x = cutscn_form[11].x + cutscn_form[11].w + 4;
    /* background inputs */
    ui_table_resize(&cutscn_form[13], w - 10 - cutscn_form[13].x, cutscn_form[13].h);
    cutscn_form[14].x = cutscn_form[19].x = cutscn_form[13].x + 5;
    cutscn_form[15].x = cutscn_form[20].x = cutscn_form[13].x + cutscn_bghdr[0].w + 20;
    cutscn_form[15].w = cutscn_form[20].w = cutscn_bghdr[1].w - 20 - cutscn_form[16].w;
    cutscn_form[16].x = cutscn_form[21].x = cutscn_form[15].x + cutscn_form[15].w + 4;
    cutscn_form[17].x = cutscn_form[22].x = cutscn_form[13].x + cutscn_bghdr[0].w + cutscn_bghdr[1].w + 20;
    cutscn_form[17].w = cutscn_form[22].w = cutscn_bghdr[2].w - 20 - cutscn_form[18].w;
    cutscn_form[18].x = cutscn_form[23].x = cutscn_form[17].x + cutscn_form[17].w + 4;
    /* slideshow images */
    cutscn_form[24].x = cutscn_form[12].x + cutscn_form[12].w + 4;
    cutscn_form[25].x = w - 10 - cutscn_form[25].w;
    cutscn_form[24].w = cutscn_form[25].x - cutscn_form[24].x - 4;
    cutscn_form[26].w = cutscn_form[26].h = h - 59 - cutscn_form[26].y;
    cutscn_form[27].x = cutscn_form[26].x + cutscn_form[26].w + 10;
    if(cutscn_edit.channel == 1 && cutscn_edit.img)
        ui_fade_set(cutscn_form[24].w, &cutscn_imgf);
    /* speech controls */
    cutscn_form[28].x = cutscn_form[12].x + cutscn_form[12].w + 24;
    cutscn_form[29].x = w - 10 - cutscn_form[29].w;
    cutscn_form[28].w = cutscn_form[29].x - cutscn_form[28].x - 4;
    /* subtitles */
    cutscn_form[30].x = cutscn_form[12].x + cutscn_form[12].w + 4;
    cutscn_form[31].x = w - 10 - cutscn_form[31].w;
    cutscn_form[30].w = cutscn_form[31].x - cutscn_form[30].x - 4;
    cutscn_form[33].x = cutscn_form[32].x + cutscn_form[32].w + 4;
    cutscn_form[34].x = cutscn_form[33].x + cutscn_form[33].w + 10;
    cutscn_form[35].x = cutscn_form[34].x + cutscn_form[34].w + 10;
    cutscn_form[36].x = cutscn_form[35].x + cutscn_form[35].w + 4;
    cutscn_form[37].x = cutscn_form[36].x + cutscn_form[36].w + 4;
    cutscn_form[38].x = cutscn_form[37].x + cutscn_form[37].w + 4;
    cutscn_form[39].x = cutscn_form[38].x + cutscn_form[38].w + 4;
    cutscn_form[39].w = w - 10 - cutscn_form[39].x;
    cutscn_form[40].w = w - 10 - cutscn_form[40].x;
    cutscn_form[43].x = w - 10 - cutscn_form[43].w;
    cutscn_form[42].y = cutscn_form[43].y = h - 59 - 24;
    ui_table_resize(&cutscn_form[41], w - 10 - cutscn_form[41].x, cutscn_form[42].y - cutscn_form[41].y - 4);
    if(cutscn_edit.channel == 3 && cutscn_edit.sub)
        ui_fade_set(cutscn_form[30].w, &cutscn_subf);
    /* scripts */
    cutscn_form[45].x = w - 10 - cutscn_form[45].w;
    cutscn_form[44].w = cutscn_form[45].x - cutscn_form[44].x - 4;
    cutscn_form[44].h = cutscn_form[45].h = h - 59 - cutscn_form[44].y;
}

/**
 * View layer
 */
void cutscn_redraw(int tab)
{
    int i;

    (void)tab;
    ui_form = cutscn_form;
    ui_form[0].inactive = (cutscn_tbl.val < 0);
    ui_form[1].inactive = (!cutscn_name[0]);
    ui_form[7].inactive = !(cutscn_samples && cutscn_samples->abuf && cutscn_edit.sele);
    for(i = 9; ui_form[i].type; i++)
        ui_form[i].inactive = 2;
    switch(cutscn_edit.len ? cutscn_edit.channel : (cutscn_edit.channel ? -1 : 0)) {
        case -1: ui_form[9].inactive = 0; break;
        case 0:
            for(i = 13; i < 24; i++) ui_form[i].inactive = 0;
        break;
        case 1:
            if(cutscn_edit.seli < 0 || cutscn_edit.numimg < 1)
                ui_form[10].inactive = 0;
            else {
                ui_form[11].inactive = ui_form[12].inactive = 0;
                for(i = 24; i < 28; i++) ui_form[i].inactive = 0;
                cutscn_edit.sels = cutscn_edit.img[cutscn_edit.seli].s;
                cutscn_edit.sele = cutscn_edit.img[cutscn_edit.seli].e;
                cutscn_sels.val = &cutscn_edit.img[cutscn_edit.seli].s;
                cutscn_sele.val = &cutscn_edit.img[cutscn_edit.seli].e;
                cutscn_imgf.min = &cutscn_edit.img[cutscn_edit.seli].s;
                cutscn_imgf.max = &cutscn_edit.img[cutscn_edit.seli].e;
                cutscn_imgf.in = &cutscn_edit.img[cutscn_edit.seli].fi;
                cutscn_imgf.out = &cutscn_edit.img[cutscn_edit.seli].fo;
                cutscn_imgspr.val = cutscn_edit.img[cutscn_edit.seli].img;
                cutscn_imgcolor = cutscn_edit.img[cutscn_edit.seli].color;
                if(cutscn_lasti != cutscn_edit.seli) {
                    cutscn_lasti = cutscn_edit.seli;
                    ui_fade_set(cutscn_form[24].w, &cutscn_imgf);
                }
            }
        break;
        case 2:
            if(cutscn_edit.seli < 0 || cutscn_edit.numspc < 1)
                ui_form[10].inactive = 0;
            else {
                ui_form[11].inactive = 0; ui_form[12].inactive = 1;
                for(i = 28; i < 30; i++) ui_form[i].inactive = 0;
                cutscn_edit.sels = cutscn_edit.spc[cutscn_edit.seli].s;
                cutscn_edit.sele = cutscn_edit.spc[cutscn_edit.seli].s + cutscn_edit.spc[cutscn_edit.seli].e;
                cutscn_sels.val = &cutscn_edit.spc[cutscn_edit.seli].s;
                cutscn_sele.val = &cutscn_edit.sele;
                cutscn_spc.val = cutscn_edit.spc[cutscn_edit.seli].spc;
            }
        break;
        case 3:
            if(cutscn_edit.seli < 0 || cutscn_edit.numsub < 1)
                ui_form[10].inactive = 0;
            else {
                ui_form[11].inactive = ui_form[12].inactive = 0;
                for(i = 30; i < 41; i++) ui_form[i].inactive = 0;
                cutscn_edit.sels = cutscn_edit.sub[cutscn_edit.seli].s;
                cutscn_edit.sele = cutscn_edit.sub[cutscn_edit.seli].e;
                cutscn_sels.val = &cutscn_edit.sub[cutscn_edit.seli].s;
                cutscn_sele.val = &cutscn_edit.sub[cutscn_edit.seli].e;
                cutscn_subf.min = &cutscn_edit.sub[cutscn_edit.seli].s;
                cutscn_subf.max = &cutscn_edit.sub[cutscn_edit.seli].e;
                cutscn_subf.in = &cutscn_edit.sub[cutscn_edit.seli].fi;
                cutscn_subf.out = &cutscn_edit.sub[cutscn_edit.seli].fo;
                sprintf(cutscn_crdtxt, "\x1c%3u%%, \x1d%3u%%", cutscn_edit.sub[cutscn_edit.seli].x,
                    cutscn_edit.sub[cutscn_edit.seli].y);
                cutscn_align.val = &cutscn_edit.sub[cutscn_edit.seli].a;
                cutscn_bold.val = &cutscn_edit.sub[cutscn_edit.seli].style;
                cutscn_italic.val = &cutscn_edit.sub[cutscn_edit.seli].style;
                cutscn_size.val = &cutscn_edit.sub[cutscn_edit.seli].size;
                cutscn_font.val = &cutscn_edit.sub[cutscn_edit.seli].font;
                cutscn_subinp.str = cutscn_edit.sub[cutscn_edit.seli].txt;
                cutscn_form[35].param = &cutscn_edit.sub[cutscn_edit.seli].color;
                if(cutscn_lasti != cutscn_edit.seli) {
                    cutscn_lasti = cutscn_edit.seli;
                    cutscn_subtbl.val = cutscn_edit.seli;
                    ui_fade_set(cutscn_form[30].w, &cutscn_subf);
                }
            }
            cutscn_subtbl.data = cutscn_edit.sub;
            cutscn_subtbl.num = cutscn_edit.numsub;
            ui_form[41].inactive = 0;
            ui_form[42].inactive = ui_form[43].inactive = !(cutscn_subtbl.val >= 0 && cutscn_subtbl.val < cutscn_edit.numsub);
        break;
        case 4:
            ui_form[44].inactive = ui_form[45].inactive = 0;
        break;
    }
}

/**
 * Delete slideshows
 */
void cutscn_delslide(void *data)
{
    int i;

    (void)data;
    if(cutscn_edit.img)
        for(i = 0; i < cutscn_edit.numimg; i++)
            if(cutscn_edit.img[i].s >= cutscn_edit.sels && cutscn_edit.img[i].e <= cutscn_edit.sele) {
                if(cutscn_edit.img[i].data) free(cutscn_edit.img[i].data);
                memmove(&cutscn_edit.img[i], &cutscn_edit.img[i + 1], (cutscn_edit.numimg - i - 1) * sizeof(ui_slide_t));
                cutscn_edit.numimg--;
            }
    if(!cutscn_edit.numimg && cutscn_edit.img) { free(cutscn_edit.img); cutscn_edit.img = NULL; }
    cutscn_imgspr.val = -1;
    cutscn_imgcolor = 0;
    cutscn_edit.seli = -1;
}

/**
 * Set the slideshow image
 */
void cutscn_setslide(void *data)
{
    (void)data;
    if(cutscn_edit.img && cutscn_edit.seli >= 0 && cutscn_edit.seli < cutscn_edit.numimg) {
        if(cutscn_edit.img[cutscn_edit.seli].data) free(cutscn_edit.img[cutscn_edit.seli].data);
        cutscn_edit.img[cutscn_edit.seli].data = NULL;
        cutscn_edit.img[cutscn_edit.seli].img = cutscn_imgspr.val;
        cutscn_edit.img[cutscn_edit.seli].color = 0;
    }
}

/**
 * Set the slideshow color
 */
void cutscn_setcolor(void *data)
{
    (void)data;
    if(cutscn_edit.img && cutscn_edit.seli >= 0 && cutscn_edit.seli < cutscn_edit.numimg && (cutscn_imgcolor & 0xff000000)) {
        if(cutscn_edit.img[cutscn_edit.seli].data) free(cutscn_edit.img[cutscn_edit.seli].data);
        cutscn_edit.img[cutscn_edit.seli].data = NULL;
        cutscn_edit.img[cutscn_edit.seli].img = -1;
        cutscn_edit.img[cutscn_edit.seli].color = cutscn_imgcolor;
    }
}

/**
 * Delete speech recordings
 */
void cutscn_delspeech(void *data)
{
    int i;

    (void)data;
    if(cutscn_edit.spc)
        for(i = 0; i < cutscn_edit.numspc; i++)
            if(cutscn_edit.spc[i].s >= cutscn_edit.sels && cutscn_edit.spc[i].s + cutscn_edit.spc[i].e <= cutscn_edit.sele) {
                memmove(&cutscn_edit.spc[i], &cutscn_edit.spc[i + 1], (cutscn_edit.numspc - i - 1) * sizeof(ui_speech_t));
                cutscn_edit.numspc--;
            }
    if(!cutscn_edit.numspc && cutscn_edit.spc) { free(cutscn_edit.spc); cutscn_edit.spc = NULL; }
    cutscn_spc.val = -1;
    cutscn_edit.seli = -1;
}

/**
 * Set the speech recording
 */
void cutscn_setspeech(void *data)
{
    (void)data;
    if(cutscn_edit.spc && cutscn_edit.seli >= 0 && cutscn_edit.seli < cutscn_edit.numspc) {
        cutscn_edit.spc[cutscn_edit.seli].spc = cutscn_spc.val;
        sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
            project.speech[cutscn_spc.val], lang[-1][0], lang[-1][1]);
        cutscn_edit.spc[cutscn_edit.seli].e = theora_getduration(projdir);
        cutscn_edit.sele = cutscn_edit.spc[cutscn_edit.seli].s + cutscn_edit.spc[cutscn_edit.seli].e;
        if(cutscn_edit.sele > cutscn_edit.len)
            cutscn_edit.sele = cutscn_edit.len;
    }
}

/**
 * Delete subtitles
 */
void cutscn_delsub(void *data)
{
    int i;

    (void)data;
    if(cutscn_edit.sub)
        for(i = 0; i < cutscn_edit.numsub; i++)
            if(cutscn_edit.sub[i].s >= cutscn_edit.sels && cutscn_edit.sub[i].e <= cutscn_edit.sele) {
                memmove(&cutscn_edit.sub[i], &cutscn_edit.sub[i + 1], (cutscn_edit.numsub - i - 1) * sizeof(ui_sub_t));
                cutscn_edit.numsub--;
            }
    if(!cutscn_edit.numsub && cutscn_edit.sub) { free(cutscn_edit.sub); cutscn_edit.sub = NULL; }
    cutscn_edit.seli = cutscn_subtbl.val = -1;
}

/**
 * Set to a specific subtitle
 */
void cutscn_setsub(void *data)
{

    (void)data;
    if(cutscn_subtbl.data && cutscn_subtbl.val >= 0 && cutscn_subtbl.val < cutscn_edit.numsub) {
        cutscn_edit.sels = cutscn_edit.sub[cutscn_subtbl.val].s;
        cutscn_edit.sele = cutscn_edit.sub[cutscn_subtbl.val].e;
        cutscn_edit.seli = cutscn_subtbl.val;
    }
}

/**
 * Delete a specific subtitle
 */
void cutscn_erasesub(void *data)
{
    (void)data;
    if(cutscn_edit.sub && cutscn_subtbl.val >= 0 && cutscn_subtbl.val < cutscn_edit.numsub) {
        memmove(&cutscn_edit.sub[cutscn_subtbl.val], &cutscn_edit.sub[cutscn_subtbl.val + 1],
            (cutscn_edit.numsub - cutscn_subtbl.val - 1) * sizeof(ui_sub_t));
        cutscn_edit.numsub--;
        if(cutscn_subtbl.val >= cutscn_edit.numsub)
            cutscn_subtbl.val = cutscn_edit.numsub - 1;
        if(!cutscn_edit.numsub && cutscn_edit.sub) { free(cutscn_edit.sub); cutscn_edit.sub = NULL; }
        cutscn_edit.seli = cutscn_subtbl.val;
    }
}

/**
 * Duplicate a subtitle
 */
void cutscn_addsub(void *data)
{

    (void)data;
    if(cutscn_edit.sub && cutscn_subtbl.val >= 0 && cutscn_subtbl.val < cutscn_edit.numsub) {
        cutscn_edit.numsub++;
        cutscn_edit.sub = (ui_sub_t*)realloc(cutscn_edit.sub, cutscn_edit.numsub * sizeof(ui_sub_t));
        if(!cutscn_edit.sub) { cutscn_edit.numsub = cutscn_subtbl.val = 0; return; }
        memmove(&cutscn_edit.sub[cutscn_subtbl.val + 1], &cutscn_edit.sub[cutscn_subtbl.val],
            (cutscn_edit.numsub - cutscn_subtbl.val - 1) * sizeof(ui_sub_t));
        cutscn_subtbl.val++;
        cutscn_edit.sub[cutscn_subtbl.val].txt[0] = 0;
        cutscn_edit.seli = cutscn_subtbl.val;
    }
}

/**
 * Coordinate picker helper
 */
void cutscn_crdhelper(void *data)
{
    (void)data;
    if(cutscn_edit.sub && cutscn_edit.seli >= 0 && cutscn_edit.seli < cutscn_edit.numsub) {
        ui_coord_helper(NULL, cutscn_edit.sub[cutscn_edit.seli].a == 1 ? NULL : &cutscn_edit.sub[cutscn_edit.seli].x,
            &cutscn_edit.sub[cutscn_edit.seli].y, NULL, NULL);
        if(cutscn_edit.sub[cutscn_edit.seli].a == 1) cutscn_edit.sub[cutscn_edit.seli].x = 50;
    }
}

/**
 * Free resources
 */
void cutscn_free()
{
    int i;

    if(cutscn_edit.img) {
        for(i = 0; i < cutscn_edit.numimg; i++)
            if(cutscn_edit.img[i].data) free(cutscn_edit.img[i].data);
        free(cutscn_edit.img); cutscn_edit.img = NULL;
    }
    if(cutscn_edit.spc) { free(cutscn_edit.spc); cutscn_edit.spc = NULL; }
    if(cutscn_edit.sub) { free(cutscn_edit.sub); cutscn_edit.sub = NULL; }
    cutscn_edit.numimg = cutscn_edit.numspc = cutscn_edit.numsub = 0;
    cutscn_freesamples();
    ui_cmd_free(&cutscn_edit.cmd);
}

/**
 * Play the selected interval of the samples
 */
void cutscn_play(void *data)
{
    SDL_Rect rect;
    uint32_t *pixels, base, now = 0;
    int i = 0, j, k, l, p, m;
    Mix_Chunk tmp = { 0 };

    (void)data;
    if(!cutscn_edit.sele) return;
    if(cutscn_samples && cutscn_samples->abuf) {
        /* play */
        if(Mix_OpenAudio(FREQ, AUDIO_S16LSB, 2, 4096) < 0) {
            ui_status(1, lang[ERR_AUDIO]);
            return;
        } else {
            tmp.abuf = cutscn_samples->abuf + (cutscn_edit.sels * FREQ / 100) * 2 * sizeof(int16_t);
            tmp.alen = ((cutscn_edit.sele - cutscn_edit.sels) * FREQ / 100) * 2 * sizeof(int16_t);
            tmp.volume = MIX_MAX_VOLUME;
            /* failsafe due to integer rounding errors */
            if(tmp.abuf + tmp.alen > cutscn_samples->abuf + cutscn_samples->alen)
                tmp.alen = cutscn_samples->abuf + cutscn_samples->alen - tmp.abuf;
            Mix_PlayChannelTimed(0, &tmp, 0, -1);
        }
    } else
        return;
    rect.x = cutscn_form[8].x + 32 + cutscn_edit.sels - cutscn_edit.scr;
    rect.y = cutscn_form[8].y + 11;
    rect.w = cutscn_edit.sele - cutscn_edit.sels;
    rect.h = 48;
    base = SDL_GetTicks();
    while(now < base + rect.w * 10) {
        if(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
                SDL_PushEvent(&event); break;
            }
            if(event.type == SDL_KEYUP || event.type == SDL_MOUSEBUTTONUP) break;
        }
        now = SDL_GetTicks();
        m = (now - base) / 10;
        if(i != m) {
            SDL_LockTexture(screen, NULL, (void**)&pixels, &p); p /= 4;
            for(k = (cutscn_edit.sels + i) * 2; i < m; i++, k += 2)
                if(cutscn_edit.smp) {
                    j = (rect.y + 24 + cutscn_edit.smp[k + 0]) * p + rect.x + i;
                    for(l = cutscn_edit.smp[k + 0]; l <= cutscn_edit.smp[k + 1]; l++, j += p)
                        pixels[j] = theme[THEME_FG];
                } else
                    pixels[(rect.y + 24) * p + rect.x + i] = theme[THEME_FG];
            SDL_UnlockTexture(screen);
            SDL_RenderCopy(renderer, screen, &rect, &rect);
            SDL_RenderPresent(renderer);
        }
        SDL_Delay(10);
    }
    Mix_HaltChannel(0);
    Mix_CloseAudio();
}

/**
 * Load the background samples
 */
void cutscn_loadsamples(void *data)
{
    Mix_Chunk *tmp;
    int i, j, k, l, m = 0, n, o, noaudio = 0;
    int16_t *pcm;

    (void)data;
    cutscn_freesamples();
    SDL_SetCursor(cursors[CURSOR_LOADING]);
    if(Mix_OpenAudio(FREQ, AUDIO_S16LSB, 2, 4096) < 0) {
        if(verbose) printf("cutscn_loadsamples: Mix_OpenAudio failed\n");
        ui_status(1, lang[ERR_AUDIO]);
        noaudio = 1;
    }
    if(cutscn_bg[1].val >= 0) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA], project.music[cutscn_bg[1].val]);
        if(!noaudio) {
            cutscn_samples = Mix_LoadWAV_RW(SDL_RWFromFile(projdir, "rb"), 1);
            /* if we have pcm, then use the sample's size to calculate the duration */
            if(cutscn_samples) {
                cutscn_edit.bglen[1] = cutscn_samples->alen / 2 / sizeof(int16_t) * 100 / FREQ;
                cutscn_oncesample.alen = cutscn_samples->alen;
                cutscn_oncesample.abuf = cutscn_samples->abuf;
            }
            m = 1;
        }
        if(!cutscn_edit.bglen[1])
            cutscn_edit.bglen[1] = theora_getduration(projdir);
        if(cutscn_edit.bglen[1] > 0) {
            media_dur(cutscn_edit.bglen[1], cutscn_lens[1]);
        }
    }
    if(cutscn_bg[3].val >= 0) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA], project.music[cutscn_bg[3].val]);
        if(!noaudio) {
            if(cutscn_samples) {
                tmp = Mix_LoadWAV_RW(SDL_RWFromFile(projdir, "rb"), 1);
                if(tmp)
                    cutscn_edit.bglen[3] = tmp->alen / 2 / sizeof(int16_t) * 100 / FREQ;
                cutscn_samples->abuf = (uint8_t*)realloc(cutscn_samples->abuf, cutscn_samples->alen + tmp->alen);
                if(cutscn_samples->abuf) {
                    if(cutscn_oncesample.abuf && cutscn_oncesample.alen) {
                        cutscn_oncesample.abuf = cutscn_samples->abuf;
                        cutscn_loopsample.abuf = cutscn_samples->abuf + cutscn_samples->alen;
                        cutscn_loopsample.alen = tmp->alen;
                    }
                    memcpy(cutscn_samples->abuf + cutscn_samples->alen, tmp->abuf, tmp->alen);
                    cutscn_samples->alen += tmp->alen;
                    Mix_FreeChunk(tmp);
                } else {
                    memcpy(cutscn_samples, tmp, sizeof(Mix_Chunk));
                    cutscn_oncesample.abuf = NULL;
                    cutscn_oncesample.alen = 0;
                    cutscn_loopsample.abuf = tmp->abuf;
                    cutscn_loopsample.alen = tmp->alen;
                }
            } else
                cutscn_samples = Mix_LoadWAV_RW(SDL_RWFromFile(projdir, "rb"), 1);
            m = 1;
        }
        if(!cutscn_edit.bglen[3])
            cutscn_edit.bglen[3] = theora_getduration(projdir);
        if(cutscn_edit.bglen[3] > 0)
            media_dur(cutscn_edit.bglen[3], cutscn_lens[3]);
    }
    if(cutscn_bg[0].val >= 0) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s.ogv", project.id, project_dirs[PROJDIRS_MEDIA + 3],
            project.movies[cutscn_bg[0].val]);
        if(!noaudio && !m && !cutscn_samples) {
            cutscn_samples = Mix_LoadWAV_RW(SDL_RWFromFile(projdir, "rb"), 1);
            if(cutscn_samples)
                cutscn_edit.bglen[0] = cutscn_samples->alen / 2 / sizeof(int16_t) * 100 / FREQ;
        }
        if(!cutscn_edit.bglen[0])
            cutscn_edit.bglen[0] = theora_getduration(projdir);
        if(cutscn_edit.bglen[0] > 0)
            media_dur(cutscn_edit.bglen[0], cutscn_lens[0]);
    }
    if(cutscn_bg[2].val >= 0) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s.ogv", project.id, project_dirs[PROJDIRS_MEDIA + 3],
            project.movies[cutscn_bg[2].val]);
        if(!noaudio && !m) {
            if(cutscn_samples) {
                tmp = Mix_LoadWAV_RW(SDL_RWFromFile(projdir, "rb"), 1);
                if(tmp)
                    cutscn_edit.bglen[2] = tmp->alen / 2 / sizeof(int16_t) * 100 / FREQ;
                cutscn_samples->abuf = (uint8_t*)realloc(cutscn_samples->abuf, cutscn_samples->alen + tmp->alen);
                if(cutscn_samples->abuf) {
                    memcpy(cutscn_samples->abuf + cutscn_samples->alen, tmp->abuf, tmp->alen);
                    cutscn_samples->alen += tmp->alen;
                    Mix_FreeChunk(tmp);
                } else
                    memcpy(cutscn_samples, tmp, sizeof(Mix_Chunk));
            } else
                cutscn_samples = Mix_LoadWAV_RW(SDL_RWFromFile(projdir, "rb"), 1);
        }
        if(!cutscn_edit.bglen[2])
            cutscn_edit.bglen[2] = theora_getduration(projdir);
        if(cutscn_edit.bglen[2] > 0)
            media_dur(cutscn_edit.bglen[2], cutscn_lens[2]);
    }
    Mix_VolumeChunk(&cutscn_oncesample, MIX_MAX_VOLUME);
    Mix_VolumeChunk(&cutscn_loopsample, MIX_MAX_VOLUME);
    if(cutscn_samples)
        Mix_VolumeChunk(cutscn_samples, MIX_MAX_VOLUME);
    if(!noaudio) Mix_CloseAudio();
    cutscn_edit.len = cutscn_edit.bglen[1] + cutscn_edit.bglen[3];
    l = cutscn_edit.bglen[0] + cutscn_edit.bglen[2];
    if(l > cutscn_edit.len) cutscn_edit.len = l;
    if(cutscn_edit.len > CUTSCN_MAXLEN) cutscn_edit.len = CUTSCN_MAXLEN;
    /* create wave pattern from samples */
    if(cutscn_samples && cutscn_samples->abuf && cutscn_edit.len) {
        /* there's no direct relation between cutscn_edit.len and sample length */
        cutscn_edit.smp = (char*)main_alloc((cutscn_edit.len + 1) * 2);
        l = cutscn_samples->alen / 2 / sizeof(int16_t);
        /* get global minimum / maximum values */
        pcm = (int16_t*)cutscn_samples->abuf;
        for(i = m = n = 0; i < l; i++) {
            k = *pcm++; k += *pcm++; k /= 2;
            if(k < n) n = k;
            if(k > m) m = k;
        }
        n = (n < 0 ? -n : n);
        m = (m < 0 ? -m : m);
        o = (n > m ? n : m);
        /* get local minimum / maximum values over every l samples */
        pcm = (int16_t*)cutscn_samples->abuf;
        for(i = 0; i < cutscn_edit.len && l; i++) {
            for(j = m = n = 0; j < FREQ / 100 && l; j++, l--) {
                k = *pcm++; k += *pcm++; k /= 2;
                if(k < n) n = k;
                if(k > m) m = k;
            }
            cutscn_edit.smp[i * 2 + 0] = n * 23 / o;
            cutscn_edit.smp[i * 2 + 1] = m * 23 / o;
        }
    }
    cutscn_sels.max = cutscn_sele.max = cutscn_edit.len;
    SDL_SetCursor(cursors[CURSOR_PTR]);
}

/**
 * Free the background samples
 */
void cutscn_freesamples()
{
    int i;

    for(i = 0; i < 4; i++) {
        cutscn_edit.bglen[i] = 0;
        strcpy(cutscn_lens[i], "--:--.--");
    }
    cutscn_edit.len = cutscn_edit.sels = cutscn_edit.sele = cutscn_edit.scr = 0;
    if(cutscn_edit.smp) { free(cutscn_edit.smp); cutscn_edit.smp = NULL; }
    if(cutscn_samples) { Mix_FreeChunk(cutscn_samples); cutscn_samples = NULL; }
    cutscn_oncesample.alen = cutscn_loopsample.alen = 0;
    cutscn_oncesample.abuf = cutscn_loopsample.abuf = NULL;
}

/**
 * Tidy up cutscene
 */
int cutscn_tidyimg(const void *a, const void *b) { return ((ui_slide_t*)a)->s - ((ui_slide_t*)b)->s; }
int cutscn_tidyspc(const void *a, const void *b) { return ((ui_speech_t*)a)->s - ((ui_speech_t*)b)->s; }
int cutscn_tidysub(const void *a, const void *b) { return ((ui_sub_t*)a)->s - ((ui_sub_t*)b)->s; }
void cutscn_tidy()
{
    int i;

    cutscn_edit.sels = cutscn_edit.sele = cutscn_edit.scr = 0;
    cutscn_edit.seli = -1;

    if(cutscn_edit.img)
        for(i = 0; i < cutscn_edit.numimg; i++)
            if(cutscn_edit.img[i].img < 0 && !cutscn_edit.img[i].color) {
                memmove(&cutscn_edit.img[i], &cutscn_edit.img[i + 1], (cutscn_edit.numimg - i - 1) * sizeof(ui_slide_t));
                cutscn_edit.numimg--;
            }
    if(!cutscn_edit.numimg && cutscn_edit.img) { free(cutscn_edit.img); cutscn_edit.img = NULL; }
    else qsort(cutscn_edit.img, cutscn_edit.numimg, sizeof(ui_slide_t), cutscn_tidyimg);

    if(cutscn_edit.spc)
        for(i = 0; i < cutscn_edit.numspc; i++)
            if(cutscn_edit.spc[i].s >= cutscn_edit.sels && cutscn_edit.spc[i].s + cutscn_edit.spc[i].e <= cutscn_edit.sele) {
                memmove(&cutscn_edit.spc[i], &cutscn_edit.spc[i + 1], (cutscn_edit.numspc - i - 1) * sizeof(ui_speech_t));
                cutscn_edit.numspc--;
            }
    if(!cutscn_edit.numspc && cutscn_edit.spc) { free(cutscn_edit.spc); cutscn_edit.spc = NULL; }
    else qsort(cutscn_edit.spc, cutscn_edit.numspc, sizeof(ui_speech_t), cutscn_tidyspc);

    if(cutscn_edit.sub)
        for(i = 0; i < cutscn_edit.numsub; i++)
            if(!cutscn_edit.sub[i].txt[0]) {
                memmove(&cutscn_edit.sub[i], &cutscn_edit.sub[i + 1], (cutscn_edit.numsub - i - 1) * sizeof(ui_sub_t));
                cutscn_edit.numsub--;
            }
    if(!cutscn_edit.numsub && cutscn_edit.sub) { free(cutscn_edit.sub); cutscn_edit.sub = NULL; }
    else qsort(cutscn_edit.sub, cutscn_edit.numsub, sizeof(ui_sub_t), cutscn_tidysub);
}

/**
 * Clear form, new cutscene
 */
void cutscn_new(void *data)
{
    (void)data;
    cutscn_free();
    cutscn_bg[0].val = cutscn_bg[1].val = cutscn_bg[2].val = cutscn_bg[3].val = -1;
    cutscn_name[0] = 0;
}

/**
 * Delete cutscene
 */
void cutscn_delete(void *data)
{
    char **list = (char**)cutscn_tbl.data;

    (void)data;
    if(cutscn_tbl.val < 0 || !list) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.cut", project.id, project_dirs[PROJDIRS_UI], list[cutscn_tbl.val]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("cutscn_delete: removing %s\n", projdir);
        remove(projdir);
        cutscn_new(NULL);
        cutscn_init(SUBMENU_CUTSCENES);
    }
}

/**
 * Save cutscene
 */
void cutscn_save(void *data)
{
    char **list = (char**)cutscn_tbl.data;
    FILE *f;
    int i;

    (void)data;
    if(!cutscn_name[0]) return;
    cutscn_tidy();
    f = project_savefile(!list || cutscn_tbl.val < 0 || strcmp(list[cutscn_tbl.val], cutscn_name),
        project_dirs[PROJDIRS_UI], cutscn_name, "cut", PROJMAGIC_CUTSCENE, "cutscn_save");
    if(f) {
        project_wridx(f, cutscn_bg[0].val, project.movies); fprintf(f, " ");
        project_wridx(f, cutscn_bg[1].val, project.music); fprintf(f, " ");
        project_wridx(f, cutscn_bg[2].val, project.movies); fprintf(f, " ");
        project_wridx(f, cutscn_bg[3].val, project.music); fprintf(f, "\r\n");
        if(cutscn_edit.img)
            for(i = 0; i < cutscn_edit.numimg; i++) {
                fprintf(f, "i %u %u %u %u %08X ", cutscn_edit.img[i].s, cutscn_edit.img[i].fi, cutscn_edit.img[i].fo,
                    cutscn_edit.img[i].e, cutscn_edit.img[i].color);
                project_wrsprite2(f, cutscn_edit.img[i].img, cutscn_imgspr.cat);
                fprintf(f, "\r\n");
            }
        if(cutscn_edit.spc)
            for(i = 0; i < cutscn_edit.numspc; i++) {
                fprintf(f, "s %u ", cutscn_edit.spc[i].s);
                project_wridx(f, cutscn_edit.spc[i].spc, project.speech);
                fprintf(f, "\r\n");
            }
        if(cutscn_edit.sub)
            for(i = 0; i < cutscn_edit.numsub; i++) {
                fprintf(f, "t %u %u %u %u %u %u %s %08X ", cutscn_edit.sub[i].s, cutscn_edit.sub[i].fi, cutscn_edit.sub[i].fo,
                    cutscn_edit.sub[i].e, cutscn_edit.sub[i].x, cutscn_edit.sub[i].y, cutscn_aligncodes[cutscn_edit.sub[i].a],
                    cutscn_edit.sub[i].color);
                project_wrfont(f, cutscn_edit.sub[i].style, cutscn_edit.sub[i].size, cutscn_edit.sub[i].font);
                fprintf(f, " %s\r\n", cutscn_edit.sub[i].txt);
            }
        if(cutscn_edit.cmd.root.len) {
            fprintf(f, "e ");
            project_wrscript(f, &cutscn_edit.cmd);
            fprintf(f, "\r\n");
        }
        fclose(f);
        project_freedir(&project.cuts, &project.numcuts);
        sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_UI]);
        project.cuts = project_getdir(projdir, ".cut", &project.numcuts);
        cutscn_tbl.data = project.cuts;
        cutscn_tbl.num = project.numcuts;
        cutscn_edit.seli = -1;
        for(cutscn_tbl.val = 0; cutscn_tbl.val < cutscn_tbl.num &&
            strcmp(cutscn_name, project.cuts[cutscn_tbl.val]); cutscn_tbl.val++);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.cut", project.id, project_dirs[PROJDIRS_UI], cutscn_name);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVECUT]);
}

/**
 * Load cutscene
 */
int cutscn_loadcut(char *fn)
{
    char *str, *s, t;
    int i;

    if(!fn || !*fn) return 0;
    str = project_loadfile(project_dirs[PROJDIRS_UI], fn, "cut", PROJMAGIC_CUTSCENE, "cutscn_loadcut");
    if(str) {
        cutscn_new(NULL);
        s = project_skipnl(str);
        s = project_getidx(s, &cutscn_bg[0].val, project.movies, -1);
        s = project_getidx(s, &cutscn_bg[1].val, project.music, -1);
        s = project_getidx(s, &cutscn_bg[2].val, project.movies, -1);
        s = project_getidx(s, &cutscn_bg[3].val, project.music, -1);
        while(*s) {
            s = project_skipnl(s);
            if(!*s) break;
            t = *s; do { s++; } while(*s == ' ');
            switch(t) {
                case 'i':
                    i = cutscn_edit.numimg++;
                    cutscn_edit.img = (ui_slide_t*)realloc(cutscn_edit.img, cutscn_edit.numimg * sizeof(ui_slide_t));
                    if(!cutscn_edit.img) { cutscn_edit.numimg = 0; break; }
                    memset(&cutscn_edit.img[i], 0, sizeof(ui_slide_t));
                    s = project_getint(s, &cutscn_edit.img[i].s, 0, CUTSCN_MAXLEN);
                    s = project_getint(s, &cutscn_edit.img[i].fi, 0, CUTSCN_MAXLEN);
                    s = project_getint(s, &cutscn_edit.img[i].fo, 0, CUTSCN_MAXLEN);
                    s = project_getint(s, &cutscn_edit.img[i].e, 0, CUTSCN_MAXLEN);
                    s = project_getcolor(s, &cutscn_edit.img[i].color);
                    s = project_getsprite(s, &cutscn_imgspr);
                    cutscn_edit.img[i].img = cutscn_imgspr.val;
                break;
                case 's':
                    i = cutscn_edit.numspc++;
                    cutscn_edit.spc = (ui_speech_t*)realloc(cutscn_edit.spc, cutscn_edit.numspc * sizeof(ui_speech_t));
                    if(!cutscn_edit.spc) { cutscn_edit.numspc = 0; break; }
                    s = project_getint(s, &cutscn_edit.spc[i].s, 0, CUTSCN_MAXLEN);
                    s = project_getidx(s, &cutscn_edit.spc[i].spc, project.speech, -1);
                    if(cutscn_edit.spc[i].spc >= 0) {
                        sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                            project.speech[cutscn_edit.spc[i].spc], lang[-1][0], lang[-1][1]);
                        cutscn_edit.spc[i].e = theora_getduration(projdir);
                    } else
                        cutscn_edit.spc[i].e = 0;
                break;
                case 't':
                    i = cutscn_edit.numsub++;
                    cutscn_edit.sub = (ui_sub_t*)realloc(cutscn_edit.sub, cutscn_edit.numsub * sizeof(ui_sub_t));
                    if(!cutscn_edit.sub) { cutscn_edit.numsub = 0; break; }
                    memset(&cutscn_edit.sub[i], 0, sizeof(ui_sub_t));
                    s = project_getint(s, &cutscn_edit.sub[i].s, 0, CUTSCN_MAXLEN);
                    s = project_getint(s, &cutscn_edit.sub[i].fi, 0, CUTSCN_MAXLEN);
                    s = project_getint(s, &cutscn_edit.sub[i].fo, 0, CUTSCN_MAXLEN);
                    s = project_getint(s, &cutscn_edit.sub[i].e, 0, CUTSCN_MAXLEN);
                    s = project_getint(s, &cutscn_edit.sub[i].x, 0, 99);
                    s = project_getint(s, &cutscn_edit.sub[i].y, 0, 99);
                    s = project_getidx(s, &cutscn_edit.sub[i].a, cutscn_aligncodes, 1);
                    if(cutscn_edit.sub[i].a == 1)
                        cutscn_edit.sub[i].x = 50;
                    s = project_getcolor(s, &cutscn_edit.sub[i].color);
                    s = project_getfont(s, &cutscn_edit.sub[i].style, &cutscn_edit.sub[i].size, &cutscn_edit.sub[i].font);
                    s = project_getstr2(s, cutscn_edit.sub[i].txt, 2, PROJ_TITLEMAX);
                break;
                case 'e':
                    s = project_getscript(s, &cutscn_edit.cmd);
                break;
            }
        }
        free(str);
        cutscn_tidy();
        strncpy(cutscn_name, fn, sizeof(cutscn_name) - 1);
        cutscn_imgspr.val = cutscn_spc.val = cutscn_subtbl.val = -1;
        return 1;
    } else
        ui_status(1, lang[ERR_LOADCUT]);
    return 0;
}

/**
 * Load cutscene
 */
void cutscn_load(void *data)
{
    char **list = (char**)cutscn_tbl.data;

    (void)data;
    if(cutscn_tbl.val < 0 || !list) return;
    if(cutscn_loadcut(list[cutscn_tbl.val])) {
        cutscn_loadsamples(NULL);
    } else
        ui_status(1, lang[ERR_LOADCUT]);
}

/**
 * Load strings for translation
 */
void cutscn_loadstr()
{
    char *str, *s;
    int i;

    for(i = 0; i < project.numcuts; i++) {
        str = project_loadfile(project_dirs[PROJDIRS_UI], project.cuts[i], "cut", PROJMAGIC_CUTSCENE, "cutscn_loadstr");
        if(str) {
            s = project_skipnl(str);
            while(*s) {
                s = project_skipnl(s);
                if(!*s) break;
                if(*s == 't' && s[1] == ' ') {
                    s = project_skiparg(s, 12);
                    s = project_gettrstr(s, 2, PROJ_TITLEMAX);
                }
            }
            free(str);
        }
    }
}

/**
 * Callback for SDL_mixer
 */
void cutscn_callback(int channel)
{
    Mix_Chunk *audio;
    switch(channel) {
        case 0:
            audio = theora_audio(&theora_ctx);
            if(audio)
                Mix_PlayChannelTimed(channel, audio, 0, -1);
        break;
        case 1:
            if(cutscn_loopsample.abuf)
                Mix_PlayChannelTimed(channel, &cutscn_loopsample, 0, -1);
        break;
    }
}

/**
 * Preview cutscene
 */
void cutscn_preview(void *data)
{
    char tmp[16], *str;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *texture = NULL, *tm = NULL, **subtxt = NULL, *dialog = NULL, *portrait = NULL, **slides = NULL;
    int wf = SDL_GetWindowFlags(window), w, h, x, b, i, pitch, si = 0, sl = -1, ci = 0, cl = -1, cw = 0, sel = -1, noaudio = 0;
    int map_cx = -1, map_cy = -1;
    uint32_t *pixels, *p, now, end = cutscn_edit.len * 10, ct = 0;
    uint16_t *map = NULL;
    uint8_t *ptr;
    FILE *fonce = NULL, *floop = NULL;
    Mix_Chunk **chunks = NULL, **sounds = NULL;
    SDL_Rect src, dst, dstv, dstp, tmdst, *subsrc = NULL, scr = { 0 };
    ui_sprite_t *s;
    ui_font_t fnt;
    ssfn_buf_t buf;

    (void)data;
    if(verbose) printf("cutscn_preview: started\n");

    cutscn_tidy();

    if(cutscn_edit.sub) {
        subtxt = (SDL_Texture**)main_alloc(cutscn_edit.numsub * sizeof(SDL_Texture*));
        subsrc = (SDL_Rect*)main_alloc(cutscn_edit.numsub * sizeof(SDL_Rect));
        for(i = 0; i < cutscn_edit.numsub; i++) {
            str = cutscn_edit.sub[i].txt;
            ui_font_load(&fnt, cutscn_edit.sub[i].style, cutscn_edit.sub[i].size, cutscn_edit.sub[i].font);
            ssfn_bbox(&fnt.ctx, str, &w, &h, &x, &b);
            subtxt[i] = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, w + x, h);
            if(subtxt[i]) {
                SDL_SetTextureBlendMode(subtxt[i], SDL_BLENDMODE_BLEND);
                SDL_LockTexture(subtxt[i], NULL, (void**)&buf.ptr, &pitch);
                memset(buf.ptr, 0, pitch * h);
                buf.p = pitch;
                buf.w = subsrc[i].w = w + x;
                buf.h = subsrc[i].h = h;
                buf.bg = 0;
                buf.x = x;
                buf.y = subsrc[i].y = b;
                buf.fg = cutscn_edit.sub[i].color;
                fnt.ctx.style &= ~SSFN_STYLE_A;
                while(*str && ((x = ssfn_render(&fnt.ctx, &buf, str)) > 0 || x == SSFN_ERR_NOGLYPH)) str += (x < 1 ? 1 : x);
                fnt.ctx.style |= SSFN_STYLE_A;
                SDL_UnlockTexture(subtxt[i]);
            }
            ui_font_free(&fnt);
        }
    }

    if(Mix_OpenAudio(FREQ, AUDIO_S16SYS, 2, 4096) < 0) { ui_status(1, lang[ERR_AUDIO]); noaudio = 1; goto cleanup; }
    Mix_ChannelFinished(cutscn_callback);

    if(cutscn_edit.spc) {
        chunks = (Mix_Chunk**)main_alloc(cutscn_edit.numspc * sizeof(Mix_Chunk*));
        for(i = 0; i < cutscn_edit.numspc; i++)
            if(cutscn_edit.spc[i].spc >= 0 && cutscn_edit.spc[i].spc < project.numspeech) {
                sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
                    project.speech[cutscn_edit.spc[i].spc], lang[-1][0], lang[-1][1]);
                chunks[i] = Mix_LoadWAV_RW(SDL_RWFromFile(projdir, "rb"), 1);
                if(chunks[i]) Mix_VolumeChunk(chunks[i], MIX_MAX_VOLUME);
            }
    }

    maps_deltay = 0; chars_loadcfg((uint8_t*)&maps_deltay);
    maps_loadlights();
    if(cutscn_edit.cmd.root.len > 0) {
        sounds = (Mix_Chunk**)main_alloc(cutscn_edit.cmd.root.len * sizeof(Mix_Chunk*));
        for(i = 0; i < cutscn_edit.cmd.root.len; i++)
            if(cutscn_edit.cmd.root.children[i].type == CMD_SND - CMD_NOP) {
                if(cutscn_edit.cmd.root.children[i].arg2 >= 0 && cutscn_edit.cmd.root.children[i].arg2 < project.numsounds) {
                    sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 1],
                        project.sounds[cutscn_edit.cmd.root.children[i].arg2]);
                    sounds[i] = Mix_LoadWAV_RW(SDL_RWFromFile(projdir, "rb"), 1);
                    if(sounds[i]) Mix_VolumeChunk(sounds[i], MIX_MAX_VOLUME);
                }
            } else
            if(cutscn_edit.cmd.root.children[i].type == CMD_NPCADD - CMD_NOP)
                maps_loadnpc(cutscn_edit.cmd.root.children[i].arg2);
    }

    if(cutscn_bg[0].val >= 0 && cutscn_bg[0].val < project.nummovies) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s.ogv", project.id, project_dirs[PROJDIRS_MEDIA + 3],
            project.movies[cutscn_bg[0].val]);
        fonce = project_fopen(projdir, "rb");
    }
    if(cutscn_bg[2].val >= 0 && cutscn_bg[2].val < project.nummovies) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s.ogv", project.id, project_dirs[PROJDIRS_MEDIA + 3],
            project.movies[cutscn_bg[2].val]);
        floop = project_fopen(projdir, "rb");
    }

    elements_load();
    elements_loadfonts();

    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

    tm = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, 282, 20);
    if(!tm) goto cleanup;
    tmdst.x = dm.w - 282; tmdst.y = dm.h - 20; tmdst.w = 282, tmdst.h = 20;

    if(cutscn_edit.img && cutscn_edit.numimg > 0) {
        slides = (SDL_Texture**)main_alloc(cutscn_edit.numimg * sizeof(SDL_Texture*));
        for(i = 0; i < cutscn_edit.numimg; i++)
            if(cutscn_edit.img[i].color) {
                slides[i] = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, 1, 1);
                if(slides[i]) {
                    SDL_SetTextureBlendMode(slides[i], SDL_BLENDMODE_BLEND);
                    SDL_LockTexture(slides[i], NULL, (void**)&pixels, &x);
                    memcpy(pixels, &cutscn_edit.img[i].color, 4);
                    SDL_UnlockTexture(slides[i]);
                }
            } else {
                spr_texture(spr_getidx(cutscn_imgspr.cat, cutscn_edit.img[i].img, 0, 1), &slides[i]);
                s = spr_getidx(cutscn_imgspr.cat, cutscn_edit.img[i].img, 0, 1);
                ui_fit(dm.w, dm.h, s->w, s->h, &w, &h);
                if(w > scr.w) scr.w = w;
                if(h > scr.h) scr.w = h;
            }
    }

    theora_start(&theora_ctx, fonce, floop);
    if(theora_ctx.hasVideo) {
        texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, theora_ctx.w, theora_ctx.h);
        if(texture) {
            /* planar YUV is green by default, let's make it black */
            SDL_LockTexture(texture, NULL, (void**)&ptr, &i);
            memset(ptr + (theora_ctx.w * theora_ctx.h), 0x80, (theora_ctx.w / 2) * theora_ctx.h);
            SDL_UnlockTexture(texture);
            ui_fit(dm.w, dm.h, theora_ctx.w, theora_ctx.h, &dstv.w, &dstv.h);
            dstv.x = (dm.w - dstv.w) / 2; dstv.y = (dm.h - dstv.h) / 2;
            if(dstv.w > scr.w) scr.w = dstv.w;
            if(dstv.h > scr.h) scr.w = dstv.h;
        }
    }
    if(!scr.w || !scr.h) { scr.w = dm.w; scr.h = dm.h; } else { scr.x = (dm.w - scr.w) / 2; scr.y = (dm.h - scr.h) / 2; }
    if(theora_ctx.hasAudio)
        cutscn_callback(0);
    if(cutscn_oncesample.abuf)
        Mix_PlayChannelTimed(1, &cutscn_oncesample, 0, -1);
    else
    if(cutscn_loopsample.abuf)
        Mix_PlayChannelTimed(1, &cutscn_loopsample, 0, -1);

    theora_ctx.baseticks = SDL_GetTicks();
    while(1) {
        now = SDL_GetTicks() - theora_ctx.baseticks;
        if(!floop && !cutscn_loopsample.abuf && now > (uint32_t)cutscn_edit.len * 10) break;

        /* events */
        if(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
                SDL_PushEvent(&event); goto cleanup;
            }
            switch(event.type) {
                case SDL_KEYUP: goto cleanup; break;
                case SDL_MOUSEBUTTONUP: goto cleanup; break;
            }
        }

        /* speech */
        if(chunks && si < cutscn_edit.numspc && now >= (Uint32)cutscn_edit.spc[si].s * 10 && si != sl) {
            Mix_PlayChannelTimed(2, chunks[si], 0, -1);
            sl = si++;
        }

        /* script commands */
        if(now >= ct) {
            if(cl >= 0 && cutscn_edit.cmd.root.children[cl].type == CMD_DLG - CMD_NOP && dialog) {
                SDL_DestroyTexture(dialog); dialog = portrait = NULL;
                if(sounds[cl]) Mix_FadeOutChannel(2, 100);
            }
            if(ci < cutscn_edit.cmd.root.len && ci != cl && (!cw || maps_npcidle(sel))) {
                cw = 0;
                switch(cutscn_edit.cmd.root.children[ci].type + CMD_NOP) {
                    case CMD_DELAY:
                        ct = now + cutscn_edit.cmd.root.children[ci].arg3 * 10;
                    break;
                    case CMD_SND:
                        Mix_PlayChannelTimed(3, sounds[ci], 0, -1);
                    break;
                    case CMD_DLG:
                        dialog = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
                        dialogs_display(dialog, dm.w, dm.h, cutscn_edit.cmd.root.children[ci].arg2, &sounds[ci], &portrait, &dstp);
                        if(sounds[ci]) Mix_PlayChannelTimed(2, sounds[ci], 0, -1);
                        ct = now + cutscn_edit.cmd.root.children[ci].arg3 * 10;
                    break;
                    case CMD_SCENE:
                        if(map) { free(map); map = NULL; maps_free(); }
                        map = maps_loadscene(cutscn_edit.cmd.root.children[ci].arg4, cutscn_edit.cmd.root.children[ci].arg2);
                        map_cx = cutscn_edit.cmd.root.children[ci].arg5 & 0x7fff;
                        map_cy = (cutscn_edit.cmd.root.children[ci].arg5 >> 16) & 0x7fff;
                    break;
                    case CMD_NPCADD:
                        maps_npc2scr(cutscn_edit.cmd.root.children[ci].arg2, cutscn_edit.cmd.root.children[ci].arg5 & 0x7fff,
                            (cutscn_edit.cmd.root.children[ci].arg5 >> 16) & 0x7fff, map_cx, map_cy, dm.w, dm.h);
                        sel = cutscn_edit.cmd.root.children[ci].arg2;
                    break;
                    case CMD_NPCSEL: sel = cutscn_edit.cmd.root.children[ci].arg2; break;
                    case CMD_NPCS:
                    case CMD_NPCSW:
                    case CMD_NPCW:
                    case CMD_NPCNW:
                    case CMD_NPCN:
                    case CMD_NPCNE:
                    case CMD_NPCE:
                    case CMD_NPCSE:
                        maps_npccmd(sel, cutscn_edit.cmd.root.children[ci].type, cutscn_edit.cmd.root.children[ci].arg3, 0);
                    break;
                    case CMD_CANIM:
                        maps_npccmd(sel, CMD_CANIM - CMD_NOP,
                            cutscn_edit.cmd.root.children[ci].arg2, cutscn_edit.cmd.root.children[ci].arg3);
                    break;
                    case CMD_REMOVE:
                    case CMD_REPLACE:
                        maps_objplace(map, cutscn_edit.cmd.root.children[ci].type + CMD_NOP == CMD_REMOVE ? -1 :
                            cutscn_edit.cmd.root.children[ci].arg2,
                            cutscn_edit.cmd.root.children[ci].arg5 & 0x7fff,
                            (cutscn_edit.cmd.root.children[ci].arg5 >> 16) & 0x7fff);
                    break;
                    case CMD_OANIM:
                        maps_objanim(cutscn_edit.cmd.root.children[ci].arg2);
                    break;
                    case CMD_WAITNPC:
                        cw = 1;
                    break;
                }
                cl = ci++;
            }
        }

        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        /* video */
        if(texture) {
            theora_video(&theora_ctx, texture);
            SDL_RenderCopy(renderer, texture, NULL, &dstv);
        }
        /* maps */
        if(map) {
            maps_rendermap(map, MAP_MASK_ROOF1|MAP_MASK_ROOF2|MAP_MASK_ROOF3|MAP_MASK_ROOF4|MAP_MASK_NPC|MAP_MASK_OBJ|
                MAP_MASK_GRD1|MAP_MASK_GRD2|MAP_MASK_GRD3|MAP_MASK_GRD4, 0, 0, dm.w, dm.h, map_cx, map_cy);
        }
        /* dialogs */
        if(dialog) {
            dst.x = dst.y = 0; dst.w = dm.w; dst.h = dm.h;
            SDL_RenderCopy(renderer, dialog, NULL, &dst);
            if(portrait) {
                src.x = src.y = 0; src.w = dst.w = dstp.w < 0 ? -dstp.w : dstp.w; src.h = dst.h = dstp.h;
                dst.x = dstp.x; dst.y = dstp.y;
                SDL_RenderCopy(renderer, portrait, &src, &dst);
                SDL_RenderCopyEx(renderer, portrait, &src, &dst, 0, NULL, dstp.w < 0 ? SDL_FLIP_HORIZONTAL : 0);
            }
        }
        /* slideshow */
        if(cutscn_edit.img && slides)
            for(i = 0; i < cutscn_edit.numimg; i++)
                if(slides[i] && (uint32_t)cutscn_edit.img[i].s * 10 <= now && (uint32_t)cutscn_edit.img[i].e * 10 > now) {
                    w = (now < (uint32_t)cutscn_edit.img[i].fi * 10 ? ((now / 10 - (uint32_t)cutscn_edit.img[i].s) * 255 /
                            (cutscn_edit.img[i].fi - cutscn_edit.img[i].s + 1)) :
                        (now > (uint32_t)cutscn_edit.img[i].fo * 10 ? (255 - (now / 10 - (uint32_t)cutscn_edit.img[i].fo) * 255 /
                            (cutscn_edit.img[i].e - cutscn_edit.img[i].fo + 1)) : 255));
                    SDL_SetTextureAlphaMod(slides[i], w);
                    if(cutscn_edit.img[i].color || cutscn_edit.img[i].img < 0) {
                        SDL_RenderCopy(renderer, slides[i], NULL, NULL);
                    } else {
                        s = spr_getidx(cutscn_imgspr.cat, cutscn_edit.img[i].img, 0, 1);
                        src.x = src.y = 0; src.w = s->w; src.h = s->h;
                        ui_fit(dm.w, dm.h, src.w, src.h, &dst.w, &dst.h);
                        dst.x = (dm.w - dst.w) / 2; dst.y = (dm.h - dst.h) / 2;
                        SDL_RenderCopy(renderer, slides[i], &src, &dst);
                    }
                }
        /* subtitles */
        if(subtxt && subsrc)
            for(i = 0; i < cutscn_edit.numsub; i++)
                if(subtxt[i] && subsrc[i].w && subsrc[i].h &&
                  (uint32_t)cutscn_edit.sub[i].s * 10 <= now && (uint32_t)cutscn_edit.sub[i].e * 10 > now) {
                    src.x = src.y = 0;
                    dst.w = src.w = subsrc[i].w; dst.h = src.h = subsrc[i].h;
                    dst.x = scr.x + scr.w * cutscn_edit.sub[i].x / 100;
                    dst.y = scr.y + scr.h * cutscn_edit.sub[i].y / 100 - subsrc[i].y;
                    switch(cutscn_edit.sub[i].a) {
                        case 1: dst.x -= subsrc[i].w / 2; break;
                        case 2: dst.x -= subsrc[i].w; break;
                    }
                    w = (now < (uint32_t)cutscn_edit.sub[i].fi * 10 ? ((now / 10 - (uint32_t)cutscn_edit.sub[i].s) * 255 /
                            (cutscn_edit.sub[i].fi - cutscn_edit.sub[i].s + 1)) :
                        (now > (uint32_t)cutscn_edit.sub[i].fo * 10 ? (255 - (now / 10 - (uint32_t)cutscn_edit.sub[i].fo) * 255 /
                            (cutscn_edit.sub[i].e - cutscn_edit.sub[i].fo + 1)) : 255));
                    SDL_SetTextureAlphaMod(subtxt[i], w);
                    SDL_RenderCopy(renderer, subtxt[i], &src, &dst);
                }

        /* progressbar */
        if(tm) {
            SDL_LockTexture(tm, NULL, (void**)&pixels, &pitch);
            for(i = 0, p = pixels; i < tmdst.w * tmdst.h; i++) p[i] = theme[THEME_BG];
            w = pitch/4 * 4 + 76;
            for(h = 0; h < 12; h++)
                for(i = 0; i < 202; i++)
                    p[w + i + pitch / 4 * h] = theme[h == 0 || i == 0 ? THEME_DARKER : (h == 11 || i == 201 ? THEME_LIGHTER :
                        (!end || i <= (int)(202 * now / end) ? THEME_FG : THEME_INPBG))];
            SDL_UnlockTexture(tm);
            media_dur(now / 10, tmp);
            ui_text(tm, 4, 1, tmp);
            SDL_RenderCopy(renderer, tm, NULL, &tmdst);
        }
        SDL_RenderPresent(renderer);
    }

cleanup:
    theora_ctx.stop = 1; /* do not allow any theora_audio calls after this point */
    if(!noaudio) {
        Mix_ChannelFinished(NULL);
        Mix_HaltChannel(-1);
        SDL_Delay(10);
        Mix_CloseAudio();
    }
    theora_stop(&theora_ctx);
    if(fonce) fclose(fonce);
    if(floop) fclose(floop);
    if(slides) {
        for(i = 0; i < cutscn_edit.numimg; i++)
            if(slides[i]) SDL_DestroyTexture(slides[i]);
        free(slides);
    }
    /* do not free cutscn_oncesample and cutscn_loopsample, they are just pointers into cutscn_samples */
    if(chunks) {
        for(i = 0; i < cutscn_edit.numspc; i++)
            if(chunks[i]) Mix_FreeChunk(chunks[i]);
        free(chunks);
    }
    if(sounds) {
        for(i = 0; i < cutscn_edit.cmd.root.len; i++)
            if(sounds[i]) Mix_FreeChunk(sounds[i]);
        free(sounds);
    }
    if(subtxt) {
        for(i = 0; i < cutscn_edit.numsub; i++)
            if(subtxt[i]) SDL_DestroyTexture(subtxt[i]);
        free(subtxt);
    }
    if(subsrc) free(subsrc);
    if(tm) SDL_DestroyTexture(tm);
    if(texture) SDL_DestroyTexture(texture);
    if(dialog) SDL_DestroyTexture(dialog);
    if(map) {
        free(map);
        maps_free();
    }
    elements_freefonts();
    if(verbose) printf("cutscn_preview: stopped\n");
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Get references
 */
void cutscn_ref(tngctx_t *ctx, int idx)
{
    ui_cmd_t cmd = { 0 };
    char *str, *s, t;

    if(!tng_ref(ctx, TNG_IDX_CUT, idx)) return;
    str = project_loadfile(project_dirs[PROJDIRS_UI], project.cuts[idx], "cut", PROJMAGIC_CUTSCENE, "cutscn_ref");
    if(str) {
        s = project_skipnl(str);
        while(*s) {
            s = project_skipnl(s);
            if(!*s) break;
            t = *s; do { s++; } while(*s == ' ');
            switch(t) {
                case 'e':
                    s = project_getscript(s, &cmd);
                    ui_cmd_ref(ctx, &cmd.root, 0);
                    ui_cmd_free(&cmd);
                break;
            }
        }
        free(str);
    }
}

/**
 * Save tng
 */
int cutscn_totng(tngctx_t *ctx)
{
    ui_bc_t bc;
    uLongf cl;
    uint8_t *buf, *ptr, *comp;
    int i, j, l, err;

    if(!ctx || !project.cuts || !project.numcuts) return 1;
    /* check if there's at least one cutscene referenced */
    for(j = 0; j < project.numcuts && !ctx->idx[TNG_IDX_CUT].ref[j]; j++);
    if(j >= project.numcuts) return 1;
    /* add cutscenes to output */
    tng_section(ctx, TNG_SECTION_CUTSCENES);
    for(j = 0; j < project.numcuts; j++) {
        if(!ctx->idx[TNG_IDX_CUT].ref[j]) continue;
        if(!cutscn_loadcut(project.cuts[j])) {
            ui_switchtab(SUBMENU_CUTSCENES);
            cutscn_tbl.val = j;
            ui_status(1, lang[ERR_LOADCUT]);
            return 0;
        }
        memset(&bc, 0, sizeof(bc)); err = 0;
        if(cutscn_edit.cmd.root.len) {
            ui_cmd_tobc(ctx, &bc, &cutscn_edit.cmd.root, 0, &err);
            if(err || bc.len > 0xffffff) {
                if(bc.data) free(bc.data);
                ui_switchtab(SUBMENU_CUTSCENES);
                cutscn_loadsamples(NULL);
                cutscn_edit.channel = 4;
                cutscn_tbl.val = j;
                ui_status(1, lang[ERR_SCRIPT]);
                return 0;
            }
        }
        l = 21 + cutscn_edit.numimg * 19 + cutscn_edit.numspc * 6 + cutscn_edit.numsub * 27 + bc.len;
        buf = ptr = (uint8_t*)main_alloc(l);
        ptr = tng_asset_idx(ptr, ctx, project.movies, TNG_IDX_MOV, cutscn_bg[0].val);
        ptr = tng_asset_idx(ptr, ctx, project.music, TNG_IDX_MUS, cutscn_bg[1].val);
        ptr = tng_asset_idx(ptr, ctx, project.movies, TNG_IDX_MOV, cutscn_bg[2].val);
        ptr = tng_asset_idx(ptr, ctx, project.music, TNG_IDX_MUS, cutscn_bg[3].val);
        memcpy(ptr, &cutscn_edit.numimg, 2); ptr += 2;
        memcpy(ptr, &cutscn_edit.numspc, 2); ptr += 2;
        memcpy(ptr, &cutscn_edit.numsub, 2); ptr += 2;
        memcpy(ptr, &bc.len, 3); ptr += 3;
        if(cutscn_edit.img)
            for(i = 0; i < cutscn_edit.numimg; i++) {
                memcpy(ptr, &cutscn_edit.img[i].s, 3); ptr += 3;
                memcpy(ptr, &cutscn_edit.img[i].fi, 3); ptr += 3;
                memcpy(ptr, &cutscn_edit.img[i].fo, 3); ptr += 3;
                memcpy(ptr, &cutscn_edit.img[i].e, 3); ptr += 3;
                memcpy(ptr, &cutscn_edit.img[i].color, 4); ptr += 4;
                ptr = tng_asset_sprite(ptr, ctx, cutscn_imgspr.cat, cutscn_edit.img[i].img);
            }
        if(cutscn_edit.spc)
            for(i = 0; i < cutscn_edit.numspc; i++) {
                memcpy(ptr, &cutscn_edit.img[i].s, 3); ptr += 3;
                ptr = tng_asset_idx(ptr, ctx, project.speech, TNG_IDX_SPC, cutscn_edit.spc[i].spc);
            }
        if(cutscn_edit.sub)
            for(i = 0; i < cutscn_edit.numsub; i++) {
                memcpy(ptr, &cutscn_edit.sub[i].s, 3); ptr += 3;
                memcpy(ptr, &cutscn_edit.sub[i].fi, 3); ptr += 3;
                memcpy(ptr, &cutscn_edit.sub[i].fo, 3); ptr += 3;
                memcpy(ptr, &cutscn_edit.sub[i].e, 3); ptr += 3;
                memcpy(ptr, &cutscn_edit.sub[i].color, 4); ptr += 4;
                *ptr++ = cutscn_edit.sub[i].x;
                *ptr++ = cutscn_edit.sub[i].y;
                *ptr++ = cutscn_edit.sub[i].a;
                ptr = tng_asset_font(ptr, ctx, cutscn_edit.sub[i].style, cutscn_edit.sub[i].size, cutscn_edit.sub[i].font);
                ptr = tng_asset_text(ptr, ctx, cutscn_edit.sub[i].txt);
            }
        if(bc.data && bc.len) {
            memcpy(ptr, bc.data, bc.len); ptr += bc.len;
            free(bc.data);
        }
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        if(cl) {
            tng_desc(ctx, project.cuts[j], cl);
            tng_asset_int(ctx, comp, cl);
        } else
            free(comp);
        free(buf);
        /* free resources */
        cutscn_new(NULL);
    }
    return 1;
}

/**
 * Read from tng
 */
int cutscn_fromtng(tngctx_t *ctx)
{
    FILE *f;
    tng_asset_desc_t *asset;
    uint8_t *buf, *ptr, *end;
    int i, j, l, len, ni, nv, ns, ne;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_CUTSCENES; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;
    for(j = 0; j < len; j++, asset++) {
        ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
        buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &l);
        if(buf) {
            if(!asset->name || asset->name >= TNG_SECTION_SIZE(&ctx->tbl[0]) || l < 21) { free(buf); continue; }
            end = buf + l;
            f = project_savefile(0, project_dirs[PROJDIRS_UI], (char*)ctx->sts + asset->name, "cut", PROJMAGIC_CUTSCENE,
                "cutscn_fromtng");
            if(f) {
                ptr = tng_wridx(ctx, ptr, f); fprintf(f, " ");
                ptr = tng_wridx(ctx, ptr, f); fprintf(f, " ");
                ptr = tng_wridx(ctx, ptr, f); fprintf(f, " ");
                ptr = tng_wridx(ctx, ptr, f); fprintf(f, "\r\n");
                ni = *((uint16_t*)ptr); ptr += 2;
                nv = *((uint16_t*)ptr); ptr += 2;
                ns = *((uint16_t*)ptr); ptr += 2;
                ne = 0; memcpy(&ne, ptr, 3); ptr += 3;
                for(i = 0; i < ni && ptr < end; i++) {
                    fprintf(f, "i ");
                    ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " ");
                    ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " ");
                    ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " ");
                    ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " ");
                    fprintf(f, "%08X ", *((uint32_t*)ptr)); ptr += 4;
                    ptr = tng_wrsprite(ctx, ptr, f); fprintf(f, "\r\n");
                }
                for(i = 0; i < nv && ptr < end; i++) {
                    fprintf(f, "s ");
                    ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " ");
                    ptr = tng_wridx(ctx, ptr, f); fprintf(f, "\r\n");
                }
                for(i = 0; i < ns && ptr < end; i++) {
                    fprintf(f, "t ");
                    ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " ");
                    ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " ");
                    ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " ");
                    ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " ");
                    fprintf(f, "%u %u %s %08X ", ptr[4], ptr[5], cutscn_aligncodes[(int)ptr[6]], *((uint32_t*)ptr)); ptr += 7;
                    ptr = tng_wrfont(ctx, ptr, f); fprintf(f, " ");
                    ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                }
                if(ne > 0) {
                    fprintf(f, "e ");
                    ptr = tng_wrscript(ctx, ptr, ne, f);
                    fprintf(f, "\r\n");
                }
                fclose(f);
            }
            free(buf);
        }
    }
    return 1;
}
