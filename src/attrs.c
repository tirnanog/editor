/*
 * tnge/attrs.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Attributes window
 *
 */

#include "main.h"

void attrs_tglsetup(void *data);
void attrs_load(void *data);
void attrs_loadcfg();
void attrs_new(void *data);
void attrs_save(void *data);
void attrs_delete(void *data);

char attrs_name[PROJ_NAMEMAX], attrs_title[PROJ_TITLEMAX], attrs_expr[PROJ_EXPRMAX];
int attrs_tab, attrs_tab2;
char *attrs_types[6], *attrs_typecodes[] = { "p", "v", "c", "g", "G", NULL };
ui_input_t attrs_nameinp = { INP_ID, sizeof(attrs_name), attrs_name };
ui_input_t attrs_titleinp = { INP_NAME, sizeof(attrs_title), attrs_title };
ui_select_t attrs_typesel = { 0, 0, attrs_types };
ui_num_t attrs_val = { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 };
ui_input_t attrs_exprinp = { INP_EXPR, sizeof(attrs_expr), attrs_expr };
ui_cmdpal_t attrs_pal = { CMDPAL_ATTR, 0, 0 };
ui_cmd_t attrs_cmd = { 0 };
ui_num_t attrs_freetotal = { 0, 0, PROJ_ATTRMAX, 1 };
ui_num_t attrs_freeattr = { 0, 0, PROJ_ATTRMAX, 1 };
ui_select_t attrs_defattr[4] = { 0 };

ui_tablehdr_t attrs_hdr[] = {
    { SUBMENU_ATTRS, 0, 0, 0 },
    { 0 }
};
ui_table_t attrs_tbl = { attrs_hdr, ATTRS_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), NULL, NULL,
    NULL, NULL };

/**
 * The form
 */
ui_form_t attrs_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, ATTRS_DELETE, (void*)ICON_REMOVE, attrs_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, attrs_save },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, attrs_new },
    { FORM_INPUT, 0, 30, 200, 20, 0, ATTRS_NAME, &attrs_nameinp, NULL },
    { FORM_TABLE, 10, 30, 200, 0, 0, 0, &attrs_tbl, attrs_load },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, ATTRS_SETUP, (void*)ICON_SETUP, attrs_tglsetup },
    /* 6 */
    { FORM_SELECT, 0, 30, 0, 20, 0, ATTRS_TYPE, &attrs_typesel, NULL },
    /* 7 */
    { FORM_TEXT, 0, 55, 0, 18, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 54, 0, 20, 0, ATTRS_STITLE, &attrs_titleinp, NULL },
    /* 9 */
    { FORM_TEXT, 0, 79, 0, 18, 0, 0, ":=", NULL },
    { FORM_COMMENT, 0, 79, 0, 18, 0, 0, NULL, NULL },
    { FORM_NUM, 0, 78, 62, 20, 0, ATTRS_DEF, &attrs_val, NULL },
    { FORM_INPUT, 0, 78, 0, 20, 0, ATTRS_CALC, &attrs_exprinp, NULL },
    /* 13 */
    { FORM_COMMANDS, 0, 102, 0, 0, 0, CMD_EVT_ONCHANGE, &attrs_cmd, NULL },
    { FORM_CMDPAL, 0, 102, 40, 0, 0, 0, &attrs_pal, NULL },
    /* 15 */
    { FORM_TEXT, 0, 31, 0, 18, 0, 0, NULL, NULL },
    { FORM_NUM, 0, 30, 62, 20, 0, 0, &attrs_freetotal, NULL },
    /* 17 */
    { FORM_TEXT, 0, 55, 0, 18, 0, 0, NULL, NULL },
    { FORM_NUM, 0, 54, 62, 20, 0, 0, &attrs_freeattr, NULL },
    /* 19 */
    { FORM_TEXT, 0, 79, 0, 18, 0, 0, NULL, NULL },
    { FORM_SELECT, 0, 78, 62, 20, 0, 0, &attrs_defattr[0], NULL },
    { FORM_TEXT, 0, 103, 0, 18, 0, 0, NULL, NULL },
    { FORM_SELECT, 0, 102, 62, 20, 0, 0, &attrs_defattr[1], NULL },
    { FORM_TEXT, 0, 127, 0, 18, 0, 0, NULL, NULL },
    { FORM_SELECT, 0, 126, 62, 20, 0, 0, &attrs_defattr[2], NULL },
    { FORM_TEXT, 0, 151, 0, 18, 0, 0, NULL, NULL },
    { FORM_SELECT, 0, 150, 62, 20, 0, 0, &attrs_defattr[3], NULL },

    { FORM_LAST }
};

/**
 * Exit attrs window
 */
void attrs_exit(int tab)
{
    (void)tab;
    attrs_tbl.clk = -1;
}

/**
 * Enter attrs window
 */
void attrs_init(int tab)
{
    int i;

    attrs_exit(tab);
    attrs_form[1].param = lang[LANG_SAVE];
    attrs_form[1].w = ui_textwidth(attrs_form[1].param) + 40;
    if(attrs_form[1].w < 200) attrs_form[1].w = 200;
    attrs_form[7].param = lang[ATTRS_TITLE];
    attrs_tab = ui_textwidth(attrs_form[7].param); attrs_form[7].w = attrs_form[9].w = attrs_tab;
    attrs_form[6].x = attrs_form[7].x = attrs_form[9].x = attrs_form[13].x = attrs_form[15].x = attrs_form[17].x =
        attrs_form[4].x + attrs_form[4].w + 10;
    attrs_form[8].x = attrs_form[10].x = attrs_form[11].x = attrs_form[12].x = attrs_form[7].x + attrs_form[7].w + 4;
    attrs_form[10].param = lang[ATTRS_VALCHAR];
    attrs_form[15].param = lang[ATTRS_FREETOTAL];
    i = attrs_tab2 = ui_textwidth(attrs_form[15].param);
    attrs_form[17].param = lang[ATTRS_FREEATTR];
    i = ui_textwidth(attrs_form[17].param); if(i > attrs_tab2) attrs_tab2 = i;
    attrs_form[19].param = lang[ATTRS_LIGHT];
    i = ui_textwidth(attrs_form[19].param); if(i > attrs_tab2) attrs_tab2 = i;
    attrs_form[21].param = lang[ATTRS_WEIGHT];
    i = ui_textwidth(attrs_form[21].param); if(i > attrs_tab2) attrs_tab2 = i;
    attrs_form[23].param = lang[ATTRS_SPEED];
    i = ui_textwidth(attrs_form[23].param); if(i > attrs_tab2) attrs_tab2 = i;
    attrs_form[25].param = lang[ATTRS_LEVELUP];
    i = ui_textwidth(attrs_form[25].param); if(i > attrs_tab2) attrs_tab2 = i;
    attrs_form[15].w = attrs_form[17].w = attrs_tab2;
    attrs_form[16].x = attrs_form[18].x = attrs_form[15].x + attrs_form[15].w + 4;

    project_freedir(&project.attrs, &project.numattrs);
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_GAME]);
    project.attrs = project_getdir(projdir, ".atr", &project.numattrs);
    for(i = 0; i < (int)(sizeof(attrs_defattr)/sizeof(attrs_defattr[0])); i++) {
        attrs_defattr[i].first = LANG_NONE;
        attrs_defattr[i].opts = project.attrs;
        attrs_defattr[i].val = -1;
        attrs_form[19 + i * 2].x = attrs_form[15].x;
        attrs_form[19 + i * 2].w = attrs_tab2;
        attrs_form[20 + i * 2].x = attrs_form[16].x;
    }
    if(!attrs_name[0]) attrs_tbl.val = -1;
    attrs_tbl.data = project.attrs;
    attrs_tbl.num = project.numattrs;
    if(attrs_tbl.val >= attrs_tbl.num) attrs_tbl.val = attrs_tbl.num - 1;
    attrs_types[0] = lang[ATTRS_TYPE_PRI];
    attrs_types[1] = lang[ATTRS_TYPE_VAR];
    attrs_types[2] = lang[ATTRS_TYPE_CALC];
    attrs_types[3] = lang[ATTRS_TYPE_GLOBALVAR];
    attrs_types[4] = lang[ATTRS_TYPE_GLOBALCALC];
    attrs_types[5] = NULL;
    attrs_loadcfg();
}

/**
 * Resize the view
 */
void attrs_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    attrs_form[0].y = attrs_form[1].y = attrs_form[2].y = attrs_form[5].y = h - 48;
    attrs_form[1].x = w - 20 - attrs_form[1].w;
    ui_table_resize(&attrs_form[4], attrs_form[4].w, h - 89);
    attrs_form[2].x = attrs_form[4].x + attrs_form[4].w + 20;
    attrs_form[3].x = w - 10 - attrs_form[3].w;
    attrs_form[5].x = attrs_form[2].x + attrs_form[2].w + 20;
    attrs_form[6].w = attrs_form[3].x - attrs_form[6].x - 10;
    attrs_form[8].w = w - 10 - attrs_form[8].x;
    attrs_form[10].w = attrs_form[12].w = w - 10 - attrs_form[11].x;
    attrs_form[13].w = w - 52 - attrs_form[13].x;
    attrs_form[14].x = w - 48;
    attrs_form[13].h = attrs_form[14].h = h - 59 - attrs_form[13].y;
    for(i = 0; i < (int)(sizeof(attrs_defattr)/sizeof(attrs_defattr[0])); i++)
        attrs_form[20 + i * 2].w = w - 10 - attrs_form[20].x;
}

/**
 * View layer
 */
void attrs_redraw(int tab)
{
    int i;

    (void)tab;
    ui_form = attrs_form;
    ui_form[0].inactive = (attrs_tbl.val < 0);
    ui_form[1].inactive = (ui_form[5].type == FORM_BUTTONICN && (!attrs_name[0] || !attrs_name[1]));
    if(ui_form[5].type == FORM_BUTTONICN) {
        ui_form[3].inactive = 0;
        for(i = 6; i < 10; i++)
            ui_form[i].inactive = 0;
        ui_form[10].inactive = (attrs_typesel.val == 0 ? 0 : 2);
        ui_form[11].inactive = ui_form[13].inactive = ui_form[14].inactive =
            (attrs_typesel.val == 1 || attrs_typesel.val == 3 ? 0 : 2);
        ui_form[12].inactive = (attrs_typesel.val == 2 || attrs_typesel.val == 4 ? 0 : 2);
        for(i = 15; ui_form[i].type; i++)
            ui_form[i].inactive = 2;
    } else {
        ui_form[3].inactive = 2;
        for(i = 6; i < 15; i++)
            ui_form[i].inactive = 2;
        for(i = 15; ui_form[i].type; i++)
            ui_form[i].inactive = 0;
    }
}

/**
 * Toggle setup
 */
void attrs_tglsetup(void *data)
{
    (void)data;
    attrs_form[5].type = (attrs_form[5].type == FORM_BUTTONICN ? FORM_BUTTONICNP : FORM_BUTTONICN);
}

/**
 * Clear form, new attr
 */
void attrs_new(void *data)
{
    int i;

    if(!data || attrs_form[5].type == FORM_BUTTONICN) {
        attrs_name[0] = attrs_title[0] = attrs_expr[0] = 0;
        attrs_val.val = 0;
        ui_cmd_free(&attrs_cmd);
    } else {
        attrs_freetotal.val = attrs_freeattr.val = 0;
        for(i = 0; i < (int)(sizeof(attrs_defattr)/sizeof(attrs_defattr[0])); i++)
            attrs_defattr[i].val = -1;
    }
}

/**
 * Delete attribute
 */
void attrs_delete(void *data)
{
    char **list = (char**)attrs_tbl.data;

    (void)data;
    if(attrs_tbl.val < 0 || !list) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.atr", project.id, project_dirs[PROJDIRS_GAME], list[attrs_tbl.val]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("attrs_delete: removing %s\n", projdir);
        remove(projdir);
        attrs_new(NULL);
        attrs_init(SUBMENU_ATTRS);
    }
}

/**
 * Save attribute
 */
void attrs_save(void *data)
{
    char **list = (char**)attrs_tbl.data;
    int i;
    FILE *f;

    (void)data;
    if(ui_form[5].type == FORM_BUTTONICN) {
        if(!attrs_name[0] || !attrs_name[1]) return;
        if((attrs_typesel.val == 2 || attrs_typesel.val == 4) && (!attrs_expr[0] || !ui_cmd_validexpr(attrs_expr))) {
            ui_status(1, lang[ERR_EXPR]);
            return;
        }
        f = project_savefile(!list || attrs_tbl.val < 0 || strcmp(list[attrs_tbl.val], attrs_name),
            project_dirs[PROJDIRS_GAME], attrs_name, "atr", PROJMAGIC_ATTR, "attrs_save");
        if(f) {
            fprintf(f, "%s %s\r\n", attrs_typecodes[attrs_typesel.val], attrs_title[0] ? attrs_title : "-");
            if(attrs_typesel.val == 1 || attrs_typesel.val == 3) {
                fprintf(f, "%d\r\n", attrs_val.val);
                fprintf(f, "e onchange ");
                project_wrscript(f, &attrs_cmd);
            } else
            if(attrs_typesel.val != 0)
                fprintf(f, "%s\r\n", attrs_expr);
            fclose(f);
            attrs_init(SUBMENU_ATTRS);
            for(attrs_tbl.val = 0; attrs_tbl.val < attrs_tbl.num &&
                strcmp(attrs_name, project.attrs[attrs_tbl.val]); attrs_tbl.val++);
            sprintf(projfn, "%s" SEP "%s" SEP "%s.atr", project.id, project_dirs[PROJDIRS_GAME], attrs_name);
            ui_status(0, lang[LANG_SAVED]);
        } else
            ui_status(1, lang[ERR_SAVEATTR]);
    } else {
        f = project_savefile(0, project_dirs[PROJDIRS_GAME], "attributes", "cfg", PROJMAGIC_ATTRCONF, "attrs_save");
        if(f) {
            fprintf(f, "%u %u\r\n", attrs_freetotal.val, attrs_freeattr.val);
            for(i = 0; i < (int)(sizeof(attrs_defattr)/sizeof(attrs_defattr[0])); i++) {
                project_wridx(f, attrs_defattr[i].val, project.attrs); fprintf(f, "\r\n");
            }
            fclose(f);
            ui_form[5].type = FORM_BUTTONICN;
            ui_status(0, lang[LANG_SAVED]);
        } else
            ui_status(1, lang[ERR_SAVEATTR]);
    }
}

/**
 * Load attribute
 */
int attrs_loadatr(char *fn)
{
    char *str, *s;

    if(!fn || !*fn) return 0;
    str = project_loadfile(project_dirs[PROJDIRS_GAME], fn, "atr", PROJMAGIC_ATTR, "attrs_loadatr");
    if(str) {
        attrs_new(NULL);
        s = project_skipnl(str);
        s = project_getidx(s, &attrs_typesel.val, attrs_typecodes, 0);
        s = project_getstr2(s, attrs_title, 2, sizeof(attrs_title));
        s = project_skipnl(s);
        if(attrs_typesel.val == 1 || attrs_typesel.val == 3) {
            s = project_getint(s, &attrs_val.val, attrs_val.min, attrs_val.max);
            s = project_skipnl(s);
            s = project_getscript(s, &attrs_cmd);
        } else
        if(attrs_typesel.val != 0)
            s = project_getstr2(s, attrs_expr, 2, sizeof(attrs_expr));
        free(str);
        strncpy(attrs_name, fn, sizeof(attrs_name) - 1);
        return 1;
    }
    return 0;
}

/**
 * Load attribute
 */
void attrs_load(void *data)
{
    char **list = (char**)attrs_tbl.data;

    (void)data;
    if(attrs_tbl.val < 0 || !list) return;
    if(!attrs_loadatr(list[attrs_tbl.val]))
        ui_status(1, lang[ERR_LOADATTR]);
}

/**
 * Load strings for translation
 */
void attrs_loadstr()
{
    char *str, *s;
    int i;

    for(i = 0; i < project.numattrs; i++) {
        str = project_loadfile(project_dirs[PROJDIRS_GAME], project.attrs[i], "atr", PROJMAGIC_ATTR, "attrs_loadstr");
        if(str) {
            s = project_skipnl(str);
            s = project_skiparg(s, 1);
            s = project_gettrstr(s, 2, sizeof(attrs_title));
            free(str);
        }
    }
}

/**
 * Load attribute config
 */
void attrs_loadcfg()
{
    char *str, *s;
    int i;

    for(i = 0; i < (int)(sizeof(attrs_defattr)/sizeof(attrs_defattr[0])); i++)
        attrs_defattr[i].val = -1;
    attrs_freetotal.val = attrs_freeattr.val = 0;

    str = project_loadfile(project_dirs[PROJDIRS_GAME], "attributes", "cfg", PROJMAGIC_ATTRCONF, "attrs_loadcfg");
    if(str) {
        s = project_skipnl(str);
        s = project_getint(s, &attrs_freetotal.val, attrs_freetotal.min, attrs_freetotal.max);
        s = project_getint(s, &attrs_freeattr.val, attrs_freeattr.min, attrs_freeattr.max);
        for(i = 0; *s && i < (int)(sizeof(attrs_defattr)/sizeof(attrs_defattr[0])); i++) {
            s = project_skipnl(s);
            s = project_getidx(s, &attrs_defattr[i].val, project.attrs, -1);
        }
        free(str);
    } else
        ui_status(1, lang[ERR_LOADATTR]);
}

/**
 * Get references
 */
void attrs_ref(tngctx_t *ctx, int idx)
{
    ui_cmd_t cmd = { 0 };
    char *str, *s, *expr;
    int type;

    if(!tng_ref(ctx, TNG_IDX_ATR, idx)) return;
    str = project_loadfile(project_dirs[PROJDIRS_GAME], project.attrs[idx], "atr", PROJMAGIC_ATTR, "attrs_ref");
    if(str) {
        s = project_skipnl(str);
        s = project_getidx(s, &type, attrs_typecodes, 0);
        s = project_skipnl(s);
        if(type == 1 || type == 3) {
            s = project_skipnl(s);
            s = project_getscript(s, &cmd);
            ui_cmd_ref(ctx, &cmd.root, 0);
            ui_cmd_free(&cmd);
        } else
        if(type != 0) {
            s = project_getstr(s, &expr, 2, PROJ_EXPRMAX);
            if(expr) {
                ui_cmd_refexpr(ctx, expr);
                free(expr);
            }
        }
        free(str);
    }
}

/**
 * Save tng
 */
int attrs_totng(tngctx_t *ctx)
{
    ui_bc_t bc;
    int i, j, err;

    if(!ctx || !project.attrs || !project.numattrs) return 1;
    attrs_loadcfg();
    tng_section(ctx, TNG_SECTION_ATTRS);
    /* header */
    tng_data(ctx, &attrs_freetotal.val, 2);
    tng_data(ctx, &attrs_freeattr.val, 2);
    j = sizeof(attrs_defattr)/sizeof(attrs_defattr[0]);
    tng_data(ctx, &j, 1);
    for(i = 0; i < j; i++)
        tng_idx(ctx, project.attrs, attrs_defattr[i].val);
    for(j = i = 0; i < project.numattrs; i++)
        if(ctx->idx[TNG_IDX_ATR].ref[i]) j++;
    tng_data(ctx, &j, 2);
    /* attribute records */
    for(j = 0; j < project.numattrs; j++) {
        if(!ctx->idx[TNG_IDX_ATR].ref[j]) continue;
        if(!attrs_loadatr(project.attrs[j])) {
            ui_switchtab(SUBMENU_ATTRS);
            attrs_form[5].type = FORM_BUTTONICN;
            attrs_tbl.val = j;
            ui_status(1, lang[ERR_LOADATTR]);
            return 0;
        }
        tng_data(ctx, &attrs_typesel.val, 1);
        tng_idx(ctx, project.attrs, j);
        tng_text(ctx, attrs_title);
        memset(&bc, 0, sizeof(bc)); err = 0;
        if(attrs_typesel.val == 1 || attrs_typesel.val == 3) {
            ui_cmd_tobc(ctx, &bc, &attrs_cmd.root, 0, &err);
            if(err) {
                if(bc.data) free(bc.data);
                ui_switchtab(SUBMENU_ATTRS);
                attrs_form[5].type = FORM_BUTTONICN;
                attrs_tbl.val = j;
                ui_status(1, lang[ERR_SCRIPT]);
                return 0;
            }
            tng_data(ctx, &attrs_val.val, 4);
            tng_data(ctx, &bc.len, 3);
            tng_data(ctx, bc.data, bc.len);
        } else
        if(attrs_typesel.val != 0) {
            if(!ui_cmd_expr(ctx, &bc, attrs_expr)) {
                if(bc.data) free(bc.data);
                ui_switchtab(SUBMENU_ATTRS);
                attrs_form[5].type = FORM_BUTTONICN;
                attrs_tbl.val = j;
                ui_status(1, lang[ERR_EXPR]);
                return 0;
            }
            tng_data(ctx, &bc.len, 2);
            tng_data(ctx, bc.data, bc.len);
        }
        /* free resources */
        attrs_new(NULL);
    }
    return 1;
}

/**
 * Read from tng
 */
int attrs_fromtng(tngctx_t *ctx)
{
    FILE *f;
    ui_bc_t bc;
    uint8_t *buf, *end;
    int i, j, l, t, len;
    char tmp[PROJ_EXPRMAX];

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_ATTRS; i++);
    if(i >= ctx->numtbl) return 1;
    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
    buf = (uint8_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]);
    end = buf + len;
    if(len < 6) return 0;
    f = project_savefile(0, project_dirs[PROJDIRS_GAME], "attributes", "cfg", PROJMAGIC_ATTRCONF, "attrs_fromtng");
    if(f) {
        buf = tng_wrnum(ctx, buf, 2, f); fprintf(f, " ");
        buf = tng_wrnum(ctx, buf, 2, f); fprintf(f, "\r\n");
        j = (int)(sizeof(attrs_defattr)/sizeof(attrs_defattr[0]));
        len = *buf++; l = len > j ? len : j;
        for(i = 0; i < l; i++) {
            if(i < j && i < len) {
                buf = tng_wridx(ctx, buf, f);
            } else {
                if(i < j) fprintf(f, "-");
                if(i < len) buf += 3;
            }
            fprintf(f, "\r\n");
        }
        fclose(f);
        len = *((uint16_t*)buf); buf += 2;
        for(i = 0; i < len && buf < end; i++) {
            t = *buf++;
            j = 0; memcpy(&j, buf, 3); buf += 3;
            if(j < 1 || j >= TNG_SECTION_SIZE(&ctx->tbl[0])) break;
            f = project_savefile(0, project_dirs[PROJDIRS_GAME], (char*)ctx->sts + j, "atr", PROJMAGIC_ATTR, "attrs_fromtng");
            if(f) {
                fprintf(f, "%s ", attrs_typecodes[t]);
                buf = tng_wrtext(ctx, buf, f); fprintf(f, "\r\n");
                if(t == 1 || t == 3) {
                    buf = tng_wrnum(ctx, buf, 4, f); fprintf(f, "\r\n");
                    fprintf(f, "e onchange ");
                    j = 0; memcpy(&j, buf, 3); buf += 3;
                    buf = tng_wrscript(ctx, buf, j, f);
                } else
                if(t != 0) {
                    bc.len = 0; memcpy(&bc.len, buf, 2); buf += 2;
                    bc.data = buf; buf += bc.len;
                    ui_cmd_exprbc(ctx, &bc, tmp);
                    fprintf(f, "%s\r\n", tmp[0] ? tmp : "0");
                    /* TODO: remove this when ui_cmd_exprbc is working */
                    ui_cmd_frombc(ctx, &bc, 0, f);
                }
                fclose(f);
            }
        }
        return 1;
    }
    return 0;
}
