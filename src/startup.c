/*
 * tnge/startup.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Startup commands window
 *
 */

#include "main.h"

void startup_load();
void startup_save(void *data);
void startup_delete(void *data);
void startup_tgltimer(void *data);

ui_cmdpal_t startup_pals = { CMDPAL_START, 0, 0 };
ui_cmdpal_t startup_palt = { CMDPAL_TIMER, 0, 0 };
ui_cmd_t startup_cmd[4] = { 0 };

/**
 * The form
 */
ui_form_t startup_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, STARTUP_DELETE, (void*)ICON_REMOVE, startup_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, startup_save },
    { FORM_BUTTONICN, 96, 0, 40, 20, 0, STARTUP_TIMERS, (void*)ICON_DELAY, startup_tgltimer },
    /* 3 */
    { FORM_CMDPAL, 0, 29, 40, 0, 0, 0, &startup_pals, NULL },
    { FORM_COMMANDS, 10, 29, 0, 0, 0, CMD_EVT_ONSTART, &startup_cmd[0], NULL },
    /* 5 */
    { FORM_CMDPAL, 0, 29, 40, 0, 0, 0, &startup_palt, NULL },
    { FORM_COMMANDS, 10, 29, 0, 0, 0, CMD_EVT_ONTIMER110, &startup_cmd[1], NULL },
    { FORM_COMMANDS, 10, 29, 0, 0, 0, CMD_EVT_ONTIMER1, &startup_cmd[2], NULL },
    { FORM_COMMANDS, 10, 29, 0, 0, 0, CMD_EVT_ONTIMER10, &startup_cmd[3], NULL },
    { FORM_LAST }
};

/**
 * Toggle timer scripts
 */
void startup_tgltimer(void *data)
{
    (void)data;
    startup_form[2].type = (startup_form[2].type == FORM_BUTTONICN ? FORM_BUTTONICNP : FORM_BUTTONICN);
}

/**
 * Exit startup window
 */
void startup_exit(int tab)
{
    int i;

    (void)tab;
    for(i = 0; i < (int)(sizeof(startup_cmd)/sizeof(startup_cmd[0])); i++)
        ui_cmd_free(&startup_cmd[i]);
}

/**
 * Enter startup window
 */
void startup_init(int tab)
{
    startup_exit(tab);
    startup_form[1].param = lang[LANG_SAVE];
    startup_form[1].w = ui_textwidth(startup_form[1].param) + 40;
    if(startup_form[1].w < 200) startup_form[1].w = 200;
    startup_load();
}

/**
 * Resize the view
 */
void startup_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    startup_form[0].y = startup_form[1].y = startup_form[2].y = h - 48;
    startup_form[1].x = w - 20 - startup_form[1].w;
    for(i = 3; startup_form[i].type; i++)
        startup_form[i].h = h - 89;
    startup_form[3].x = startup_form[5].x = w - 48;
    startup_form[4].w = w - 62;
    i = (int)(sizeof(startup_cmd)/sizeof(startup_cmd[0])) - 1;
    startup_form[6].w = (w - 62 - (i - 1) * 4) / i;
    for(i = 7; startup_form[i].type; i++) {
        startup_form[i].w = startup_form[i - 1].w;
        startup_form[i].x = startup_form[i - 1].x + startup_form[i - 1].w + 4;
    }
}

/**
 * View layer
 */
void startup_redraw(int tab)
{
    int i;

    (void)tab;
    ui_form = startup_form;
    ui_form[3].inactive = ui_form[4].inactive = (ui_form[2].type == FORM_BUTTONICN ? 0 : 2);
    for(i = 5; ui_form[i].type; i++)
        ui_form[i].inactive = (ui_form[2].type == FORM_BUTTONICN ? 2 : 0);
}

/**
 * Delete startup commands hook
 */
void startup_delete(void *data)
{
    (void)data;
    sprintf(projfn, "%s" SEP "%s" SEP "startup.cfg", project.id, project_dirs[PROJDIRS_GAME]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("startup_delete: removing %s\n", projdir);
        remove(projdir);
        startup_exit(SUBMENU_START);
    }
}

/**
 * Save startup commands hook
 */
void startup_save(void *data)
{
    int i, err;
    FILE *f;

    (void)data;
    f = project_savefile(0, project_dirs[PROJDIRS_GAME], "startup", "cfg", PROJMAGIC_STARTUP, "startup_save");
    if(f) {
        fprintf(f, "e start ");
        project_wrscript(f, &startup_cmd[0]);
        err = startup_cmd[0].err;
        for(i = 1; i < (int)(sizeof(startup_cmd)/sizeof(startup_cmd[0])); i++) {
            fprintf(f, "e timer%u", i);
            project_wrscript(f, &startup_cmd[i]);
            err |= startup_cmd[i].err;
        }
        fclose(f);
        if(err)
            ui_status(1, lang[ERR_SCRIPT]);
        else
            ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVESTRT]);
}

/**
 * Load startup commands
 */
void startup_load()
{
    char *str, *s;
    int i, l = (int)(sizeof(startup_cmd)/sizeof(startup_cmd[0])), err = 0;

    str = s = project_loadfile(project_dirs[PROJDIRS_GAME], "startup", "cfg", PROJMAGIC_STARTUP, "startup_load");
    if(str) {
        for(i = 0; i < l; i++)
            ui_cmd_free(&startup_cmd[i]);
        while(*s) {
            s = project_skipnl(s);
            if(!memcmp(s, "e start", 7)) {
                s = project_getscript(s, &startup_cmd[0]);
                err |= startup_cmd[0].err;
            }
            if(!memcmp(s, "e timer", 7) && s[7] >= '1' && s[7] <= (l - 1 + '0')) {
                i = s[7] - '0';
                if(i >= l) i = l - 1;
                s = project_getscript(s + 8, &startup_cmd[i]);
                err |= startup_cmd[i].err;
            }
        }
        free(str);
        if(err)
            ui_status(1, lang[ERR_LOADSTRT]);
    }
}

/**
 * Get references
 */
void startup_ref(tngctx_t *ctx)
{
    int i, l = (int)(sizeof(startup_cmd)/sizeof(startup_cmd[0]));

    if(!ctx) return;
    startup_load();
    for(i = 0; i < l; i++)
        ui_cmd_ref(ctx, &startup_cmd[i].root, 0);
    startup_exit(SUBMENU_START);
}

/**
 * Save tng
 */
int startup_totng(tngctx_t *ctx)
{
    ui_bc_t bc;
    uLongf cl;
    uint8_t *comp;
    int i, l = (int)(sizeof(startup_cmd)/sizeof(startup_cmd[0])), t, err = 0;

    if(!ctx) return 1;
    startup_load();
    while(l > 1 && !startup_cmd[l - 1].root.len) l--;
    for(i = 0; i < l; i++)
        err |= startup_cmd[i].err;
    if(err || (!ctx->maps && !startup_cmd[0].root.len)) {
        ui_switchtab(SUBMENU_START);
        ui_status(1, lang[err ? ERR_SCRIPT : ERR_NOSTART]);
        startup_form[2].type = !startup_cmd[0].root.len || startup_cmd[0].err ? FORM_BUTTONICN : FORM_BUTTONICNP;
        return 0;
    }
    tng_section(ctx, TNG_SECTION_STARTUP);
    for(i = 0, t = 1; i < l; i++, t *= 10) {
        memset(&bc, 0, sizeof(bc)); err = 0;
        ui_cmd_tobc(ctx, &bc, &startup_cmd[i].root, 0, &err);
        if(err) {
            if(bc.data) free(bc.data);
            ui_switchtab(SUBMENU_START);
            ui_status(1, lang[ERR_SCRIPT]);
            startup_form[2].type = !i ? FORM_BUTTONICN : FORM_BUTTONICNP;
            return 0;
        }
        if(!bc.len) {
            tng_desc_idx(ctx, i ? t : 0, 0);
        } else {
            cl = compressBound(bc.len);
            comp = (uint8_t*)main_alloc(cl);
            compress2(comp, &cl, bc.data, bc.len, 9);
            if(cl) {
                tng_desc_idx(ctx, i ? t : 0, cl);
                tng_asset_int(ctx, comp, cl);
            } else
                free(comp);
            free(bc.data);
        }
    }
    startup_exit(SUBMENU_START);
    return 1;
}

/**
 * Read from tng
 */
int startup_fromtng(tngctx_t *ctx)
{
    FILE *f;
    tng_asset_desc_t *asset;
    uint8_t *buf;
    int i, j, l, t, len;

    if(!ctx || !ctx->tbl || !ctx->numtbl) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_STARTUP; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;
    f = project_savefile(0, project_dirs[PROJDIRS_GAME], "startup", "cfg", PROJMAGIC_STARTUP, "startup_fromtng");
    if(f) {
        for(i = 0; i < len; i++, asset++) {
            ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
            buf = tng_get_asset(ctx, asset->offs, asset->size, &l);
            if(buf) {
                for(j = asset->name & 0xffff, t = 0; j; t++, j /= 10);
                if(!t) fprintf(f, "e start ");
                else fprintf(f, "e timer%u", t);
                tng_wrscript(ctx, buf, l, f);
                free(buf);
            }
        }
        fclose(f);
        return 1;
    }
    return 0;
}
