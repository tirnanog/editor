/*
 * tnge/main.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Main function
 * https://tirnanog.codeberg.page
 *
 */

#include "main.h"
#include <signal.h>

/**
 * Translations
 */
char **lang = NULL;
char *dict[NUMLANGS][NUMTEXTS + 1] = {{
#include "lang_en.h"
},{
#include "lang_hu.h"
},{
#include "lang_de.h"
},{
#include "lang_fr.h"
},{
#include "lang_es.h"
},{
#include "lang_pt.h"
},{
#include "lang_ru.h"
},{
#include "lang_zh.h"
},{
#include "lang_ja.h"
}};

/* provide workarounds for dynamically linked shared libraries */
#ifdef _DYNSDLFIX_
char *strtok_r(char *s1, const char *s2, char **ptr);
char *SDL_strtokr(char *s1, const char *s2, char **ptr) { return strtok_r(s1, s2, ptr); }
#endif

void mbedtls_platform_zeroize(void *s, size_t n) { memset(s, 0, n); }
int verbose = 0;

char tngever[] = "1.0rc";
char site_url[] = "https://tirnanog.codeberg.page";

/**
 * Windows workaround
 */
#ifdef __WIN32__
#define CLIFLAG '/'
/* only include these when necessary, some of their defines conflict with SDL_mixer's... */
#include <windows.h>
#include <winnls.h>
#include <fileapi.h>
#include <shellapi.h>

/* these two functions were borrowed from sdl_windows_main.c */
static void UnEscapeQuotes(char *arg)
{
    char *last = NULL, *c_curr, *c_last;

    while (*arg) {
        if (*arg == '"' && (last != NULL && *last == '\\')) {
            c_curr = arg;
            c_last = last;

            while (*c_curr) {
                *c_last = *c_curr;
                c_last = c_curr;
                c_curr++;
            }
            *c_last = '\0';
        }
        last = arg;
        arg++;
    }
}

/* Parse a command line buffer into arguments */
static int ParseCommandLine(char *cmdline, char **argv)
{
    char *bufp;
    char *lastp = NULL;
    int argc, last_argc;

    argc = last_argc = 0;
    for (bufp = cmdline; *bufp;) {
        /* Skip leading whitespace */
        while (SDL_isspace(*bufp)) {
            ++bufp;
        }
        /* Skip over argument */
        if (*bufp == '"') {
            ++bufp;
            if (*bufp) {
                if (argv) {
                    argv[argc] = bufp;
                }
                ++argc;
            }
            /* Skip over word */
            lastp = bufp;
            while (*bufp && (*bufp != '"' || *lastp == '\\')) {
                lastp = bufp;
                ++bufp;
            }
        } else {
            if (*bufp) {
                if (argv) {
                    argv[argc] = bufp;
                }
                ++argc;
            }
            /* Skip over word */
            while (*bufp && !SDL_isspace(*bufp)) {
                ++bufp;
            }
        }
        if (*bufp) {
            if (argv) {
                *bufp = '\0';
            }
            ++bufp;
        }

        /* Strip out \ from \" sequences */
        if (argv && last_argc != argc) {
            UnEscapeQuotes(argv[last_argc]);
        }
        last_argc = argc;
    }
    if (argv) {
        argv[argc] = NULL;
    }
    return (argc);
}

/* Windows entry point */
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    char *cmdline = GetCommandLineA();
    int ret, argc = ParseCommandLine(cmdline, NULL);
    char **argv = SDL_stack_alloc(char*, argc+2);
    (void)hInstance;
    (void)hPrevInstance;
    (void)lpCmdLine;
    (void)nCmdShow;
    ParseCommandLine(cmdline, argv);
    SDL_SetMainReady();
    ret = main(argc, argv);
    SDL_stack_free(argv);
    exit(ret);
    return ret;
}
#else
#define CLIFLAG '-'
#ifdef __MACOSX__
#define OPEN "open"
#else
#define OPEN "xdg-open"
#endif
#endif

/**
 * Stop editor, sigint handler
 */
static void main_stop(int signal)
{
    (void)signal;
    fprintf(stdout, "\rtnge: Ctrl+C exit                \r\n");
    ui_quit();
    exit(1);
}

/**
 * Common error handler
 */
void main_error(int msg)
{
    char *mess = lang ? lang[msg] : dict[0][msg];
#ifdef __WIN32__
    wchar_t buf[256];
    int wlen;

    wlen = MultiByteToWideChar(CP_UTF8, 0, mess, strlen(mess), NULL, 0);
    if(wlen > 255) wlen = 255;
    if(!wlen) {
        /* we don't have stderr, only stdout */
        fprintf(stdout, "tnge: %s\r\n", mess);
        MessageBoxA(NULL, mess, "TirNanoG", MB_ICONEXCLAMATION | MB_OK);
    } else {
        MultiByteToWideChar(CP_UTF8, 0, mess, strlen(mess), buf, wlen);
        buf[wlen] = 0;
        fwprintf(stdout, L"tnge: %s\r\n", buf);
        MessageBoxW(NULL, buf, L"TirNanoG", MB_ICONEXCLAMATION | MB_OK);
    }
#else
    SDL_DisplayMode dm;
    SDL_Window *window = NULL;
    SDL_Surface *surface = NULL;
    char *s = mess;
    int w;

    fprintf(stderr, "tnge: %s\r\n", mess);
    /* create a dummy MessageBox on Linux */
    if(msg != ERR_DISPLAY && ssfn_src && SDL_WasInit(SDL_INIT_VIDEO)) {
        w = ui_textwidth(mess) + 48;
        SDL_GetDesktopDisplayMode(0, &dm);
        window = SDL_CreateWindow("TirNanoG", (dm.w - w) / 2, (dm.h - 64) / 2, w, 64, SDL_WINDOW_ALWAYS_ON_TOP |
            SDL_WINDOW_SKIP_TASKBAR);
        if(window) {
            surface = SDL_GetWindowSurface(window);
            if(surface) {
                memset(surface->pixels, 0x88, 64 * surface->pitch);
                ssfn_dst.ptr = (uint8_t*)surface->pixels + 24 * surface->pitch + 96;
                ssfn_dst.p = surface->pitch;
                ssfn_dst.x = ssfn_dst.y = ssfn_dst.w = ssfn_dst.h = 0; ssfn_dst.bg = 0; ssfn_dst.fg = 0xff000000;
                while(s && *s) ssfn_putc(ssfn_utf8(&s));
                while(1) {
                    SDL_UpdateWindowSurface(window);
                    SDL_RaiseWindow(window);
                    SDL_WaitEvent(&event);
                    if(event.type == SDL_QUIT || event.type == SDL_MOUSEBUTTONUP || event.type == SDL_KEYUP ||
                        (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) break;
                }
            }
            SDL_DestroyWindow(window);
        }
    }
#endif
    ui_quit();
    exit(1);
}

/**
 * Allocate memory with error handler
 */
void *main_alloc(size_t size)
{
    void *tmp = malloc(size);
    if(!tmp) main_error(ERR_MEM);
    memset(tmp, 0, size);
    return tmp;
}

/**
 * Open the repository in the default browser
 */
void main_open_url(char *url)
{
#ifdef __WIN32__
    ShellExecuteA(0, 0, url, 0, 0, SW_SHOW);
#else
    char cmd[512];
    if(!fork()) {
        fclose(stdin);
        fclose(stdout);
        fclose(stderr);
        sprintf(cmd, OPEN " %s", url);
        exit(system(cmd));
    }
#endif
    if(verbose) printf("main_open_url: opening %s\n", url);
}

/**
 * Print program version and copyright
 */
void main_hdr()
{
    printf("\r\nTirNanoG Editor v%s (build %u) by bzt Copyright (C) 2021 - 2022 GPLv3+\r\n\r\n", tngever, BUILD);
}

/**
 * The real main procedure
 */
int main(int argc, char** argv)
{
    int i, j, cmd = 0;
    char *lng = getenv("LANG"), *id = NULL;
#ifdef __WIN32__
    int lid;
    FILE *f;
    if(!lng) {
        lid = GetUserDefaultLangID(); /* GetUserDefaultUILanguage(); */
        /* see https://docs.microsoft.com/en-us/windows/win32/intl/language-identifier-constants-and-strings */
        switch(lid & 0xFF) {
            case 0x01: lng = "ar"; break;   case 0x02: lng = "bg"; break;   case 0x03: lng = "ca"; break;
            case 0x04: lng = "zh"; break;   case 0x05: lng = "cs"; break;   case 0x06: lng = "da"; break;
            case 0x07: lng = "de"; break;   case 0x08: lng = "el"; break;   case 0x0A: lng = "es"; break;
            case 0x0B: lng = "fi"; break;   case 0x0C: lng = "fr"; break;   case 0x0D: lng = "he"; break;
            case 0x0E: lng = "hu"; break;   case 0x0F: lng = "is"; break;   case 0x10: lng = "it"; break;
            case 0x11: lng = "jp"; break;   case 0x12: lng = "ko"; break;   case 0x13: lng = "nl"; break;
            case 0x14: lng = "no"; break;   case 0x15: lng = "pl"; break;   case 0x16: lng = "pt"; break;
            case 0x17: lng = "rm"; break;   case 0x18: lng = "ro"; break;   case 0x19: lng = "ru"; break;
            case 0x1A: lng = "hr"; break;   case 0x1B: lng = "sk"; break;   case 0x1C: lng = "sq"; break;
            case 0x1D: lng = "sv"; break;   case 0x1E: lng = "th"; break;   case 0x1F: lng = "tr"; break;
            case 0x20: lng = "ur"; break;   case 0x21: lng = "id"; break;   case 0x22: lng = "uk"; break;
            case 0x23: lng = "be"; break;   case 0x24: lng = "sl"; break;   case 0x25: lng = "et"; break;
            case 0x26: lng = "lv"; break;   case 0x27: lng = "lt"; break;   case 0x29: lng = "fa"; break;
            case 0x2A: lng = "vi"; break;   case 0x2B: lng = "hy"; break;   case 0x2D: lng = "bq"; break;
            case 0x2F: lng = "mk"; break;   case 0x36: lng = "af"; break;   case 0x37: lng = "ka"; break;
            case 0x38: lng = "fo"; break;   case 0x39: lng = "hi"; break;   case 0x3A: lng = "mt"; break;
            case 0x3C: lng = "gd"; break;   case 0x3E: lng = "ms"; break;   case 0x3F: lng = "kk"; break;
            case 0x40: lng = "ky"; break;   case 0x45: lng = "bn"; break;   case 0x47: lng = "gu"; break;
            case 0x4D: lng = "as"; break;   case 0x4E: lng = "mr"; break;   case 0x4F: lng = "sa"; break;
            case 0x53: lng = "kh"; break;   case 0x54: lng = "lo"; break;   case 0x56: lng = "gl"; break;
            case 0x5E: lng = "am"; break;   case 0x62: lng = "fy"; break;   case 0x68: lng = "ha"; break;
            case 0x6D: lng = "ba"; break;   case 0x6E: lng = "lb"; break;   case 0x6F: lng = "kl"; break;
            case 0x7E: lng = "br"; break;   case 0x92: lng = "ku"; break;   case 0x09: default: lng = "en"; break;
        }
    }
    /* restore stdout to console */
    AttachConsole(ATTACH_PARENT_PROCESS);
    f = _fdopen(_open_osfhandle((intptr_t)GetStdHandle(STD_OUTPUT_HANDLE), 0x4000/*_O_TEXT*/), "w");
    if(f) *stdout = *f;
#endif
    lang = NULL;
    /* parse command line */
    for(i = 1; i < argc; i++) {
        if(!memcmp(argv[i], "--help", 6) || !strcmp(argv[i], "-h") || !strcmp(argv[i], "/h") ||
          !strcmp(argv[i], "-?") || !strcmp(argv[i], "/?")) goto usage;
        if(argv[i][0] == CLIFLAG) {
            switch(argv[i][1]) {
                case 'L': lng = argv[++i]; break;
                case 't': ui_gettheme(argv[++i]); break;
                case 'v': for(j = 1; argv[i][j] == 'v'; j++) verbose++; break;
                case 'g': id = argv[++i]; cmd = 1; break;
                case 'c': id = argv[++i]; cmd = 2; break;
                case 'p': id = argv[++i]; cmd = 3; break;
                case 'u': id = argv[++i]; cmd = 4; break;
                case 'l': id = argv[++i]; cmd = 5; break;
                default:
usage:              main_hdr();
                    printf("  tnge [%cL <", CLIFLAG);
                    for(i = 0; i < NUMLANGS; i++)
                        printf("%s%s", i ? "|" : "", dict[i][0]);
                    printf(">] [%ct <theme.gpl>] [%cg <id>] [%cc <id>] [%cv|%cvv] [projdir]\r\n",
                        CLIFLAG, CLIFLAG, CLIFLAG, CLIFLAG, CLIFLAG);
                    printf("  tnge [%cp|%cu|%cl] <atlas>\r\n\r\n", CLIFLAG, CLIFLAG, CLIFLAG);
                    exit(0);
                break;
            }
        } else
        /* one letter path is probably a mistyped flag */
        if(strlen(argv[i]) < 2) goto usage;
        else {
            projdir = (char*)malloc(PATH_MAX + FILENAME_MAX);
            if(projdir)
                strcpy(projdir, argv[i]);
        }
    }
    /* if just a license generation requested */
    if(cmd == 1) {
        if(!id) goto usage;
        project_initdir(0);
        project_loadlicense(id);
        license_init(SUBMENU_LICENSES);
        license_generate(NULL);
        license_exit(SUBMENU_LICENSES);
        free(projdir);
        return 0;
    }

    /* get dictionary */
    if(!lng) lng = DEFLANG;
    for(i = 0; i < NUMLANGS; i++)
        if(!strncmp(lng, dict[i][0], strlen(dict[i][0]))) break;
    if(i >= NUMLANGS)
        for(i = 0; i < NUMLANGS; i++)
            if(!strncmp(lng, dict[i][0], 2)) break;
    if(i >= NUMLANGS) i = 0;
    lang = &dict[i][1];

    /* atlas packing / unpacking */
    if(cmd == 3) {
        if(!id) goto usage;
        spr_pack(id);
        if(projdir) free(projdir);
        return 0;
    }
    if(cmd == 4) {
        if(!id) goto usage;
        spr_unpack(id);
        if(projdir) free(projdir);
        return 0;
    }
    if(cmd == 5) {
        if(!id) goto usage;
        spr_listatls(id);
        if(projdir) free(projdir);
        return 0;
    }

    /* get projects directory */
    project_initdir(1);
    if(verbose) {
        main_hdr();
        printf("  You should have received a copy of the GNU General Public License\r\n"
        "  along with this program; if not, write to the Free Software\r\n"
        "  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.\r\n\r\n"
        "LANG = %s, PROJDIR = %s\r\n\r\n", lang[-1], projdir);
        fflush(stdout);
    }
    /* command line game file creation requested */
    if(cmd == 2) {
        if(!id) goto usage;
        project_load(id);
        project_togame(0);
        project_free();
        printf("\r\n");
        free(projdir);
        return 0;
    }
    signal(SIGINT, main_stop);
    signal(SIGTERM, main_stop);

    /* do ui stuff */
    ui_init();
    ui_events();
    ui_quit();

    free(projdir);
#ifdef __WIN32__
    fflush(stdout);
    FreeConsole();
#endif
    return 0;
}
