/*
 * tnge/objects.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Objects window
 *
 */

#include "main.h"

/* needed by object equipted on character preview */
extern ui_table_t chars_opts;
/* for the inventory preview */
extern ui_num_t hud_invw, hud_invh, hud_invl, hud_invt;
extern ui_sprsel_t hud_invimg, hud_invsel, hud_invequip;
extern ui_table_t hud_equiptbl;
extern ui_num_t elements_size[];

unsigned char *readfile(char *fn, unsigned int *size);
void objects_select(void *data);
void objects_load(void *data);
void objects_duplicate(void *data);
void objects_new(void *data);
void objects_save(void *data);
void objects_delete(void *data);
void objects_preview(void *data);
void objects_lyrtgl(void *data);
void objects_lyrclk(void *data);
void objects_drawspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void objects_renderspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void objects_attrdel(void *data);
void objects_attradd(void *data);
void objects_drawattr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void objects_moddel(void *data);
void objects_modadd(void *data);
void objects_drawmod(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

SDL_Texture *objects_anim = NULL;
char objects_name[PROJ_NAMEMAX], objects_title[PROJ_TITLEMAX], **objects = NULL;
char *objects_events[19] = { 0 }, objects_title[PROJ_TITLEMAX];
int objects_tab = 0, objects_fore = 1, *objects_spr = NULL, objects_modeq = 0, objects_modper = 0, objects_num = 0;
int objects_skill = 0, objects_eqslot[32] = { 0 };
ui_sprsel_t objects_bypic = { -1, 1, 158, 1 };
ui_input_t objects_nameinp = { INP_ID, sizeof(objects_name), objects_name };
ui_icongrpelem_t objects_selelems[] = {
    { ICON_OBJ, OBJECTS_ONMAP },
    { ICON_INVENT, OBJECTS_ININV }
};
ui_icongrp_t objects_sel = { &objects_tab, 0, 2, objects_selelems };
ui_select_t objects_actiongrp = { -1, LANG_NONE, &project_actions[10] };
ui_select_t objects_event = { 0, 0, objects_events };
ui_num_t objects_eventnum1 = { 1, 1, 15, 1 };
ui_num_t objects_eventnum2 = { 1, 1, 15, 1 };
ui_sprsel_t objects_eventobj[3] = { { -1, 1, 158, 4 }, { -1, 1, 158, 4 }, { -1, 1, 158, 4 } };
ui_select_t objects_eventtimers = { 0, 0, project_eventtimers };
ui_cmdpal_t objects_pal = { CMDPAL_OBJEVT, 0, 0 };
ui_cmd_t objects_cmd[17] = { 0 };
ui_coll_t objects_coll = { 0 };
ui_sprsel_t objects_spr_hidden = { -1, 2, 158, 0 };
ui_sprsel_t objects_spr_hidden2 = { -1, 3, 168, 1 };
ui_select_t objects_charopt = { -1, LANG_DEF, NULL };
ui_num_t objects_layer = { 1, 1, 99, 1 };
ui_num_t objects_cat = { 0, 0, 255, 1 };

ui_num_t objects_projtime = { 10, 10, 3000, 10 };
ui_sprsel_t objects_projunit = { -1, 1, 158, 3 };
ui_sprsel_t objects_projectile = { -1, 1, 158, 0 };
ui_input_t objects_titinp = { INP_NAME, sizeof(objects_title), objects_title };
ui_sprsel_t objects_invspr = { -1, 1, 158, 1 };
ui_select_t objects_invsnd = { -1, LANG_NONE, NULL };
ui_num_t objects_pricenum = { 0, -999999, 999999, 1 };
ui_sprsel_t objects_priceunit = { -1, 1, 158, 4 };
ui_select_t objects_attr = { 0, 0, NULL };
ui_select_t objects_rel = { 0, 0, project_rels };
ui_num_t objects_val = { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 };
ui_select_t objects_mattr = { -1, OBJECTS_TRANSPORT, NULL };
ui_num_t objects_mval = { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 };
ui_select_t objects_mtrn = { 0, 0, project_trns };
ui_sprsel_t objects_eqspr[32];

ui_tablehdr_t objects_hdr[] = {
    { OBJECTS_META, 0, 0, 0 },
    { 0 }
};
ui_table_t objects_tbl = { objects_hdr, OBJECTS_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), NULL, NULL,
    NULL, NULL };

ui_tablehdr_t objects_sprshdr[] = {
    { NUMTEXTS + ICON_IDLE, 20, CHARS_ANIM_IDLE, 0 },
    { NUMTEXTS + ICON_WALK, 20, CHARS_ANIM_WALK, 0 },
    { NUMTEXTS + ICON_CLIMB, 20, CHARS_ANIM_CLIMB, 0 },
    { NUMTEXTS + ICON_JUMP, 20, CHARS_ANIM_JUMP, 0 },
    { NUMTEXTS + ICON_RUN, 20, CHARS_ANIM_RUN, 0 },
    { NUMTEXTS + ICON_SWIM, 20, CHARS_ANIM_SWIM, 0 },
    { NUMTEXTS + ICON_FLY, 20, CHARS_ANIM_FLY, 0 },
    { NUMTEXTS + ICON_SHIELD, 20, CHARS_ANIM_BLOCK, 0 },
    { NUMTEXTS + ICON_HURT, 20, CHARS_ANIM_HURT, 0 },
    { NUMTEXTS + ICON_DEATH, 20, CHARS_ANIM_DIE, 0 },
    { NUMTEXTS + ICON_NUMACT + 1, 20, CHARS_ANIM_A1, 0 },
    { NUMTEXTS + ICON_NUMACT + 2, 20, CHARS_ANIM_A2, 0 },
    { NUMTEXTS + ICON_NUMACT + 3, 20, CHARS_ANIM_A3, 0 },
    { NUMTEXTS + ICON_NUMACT + 4, 20, CHARS_ANIM_A4, 0 },
    { NUMTEXTS + ICON_NUMACT + 5, 20, CHARS_ANIM_A5, 0 },
    { NUMTEXTS + ICON_NUMACT + 6, 20, CHARS_ANIM_A6, 0 },
    { NUMTEXTS + ICON_NUMACT + 7, 20, CHARS_ANIM_A7, 0 },
    { NUMTEXTS + ICON_NUMACT + 8, 20, CHARS_ANIM_A8, 0 },
    { NUMTEXTS + ICON_NUMACT + 9, 20, CHARS_ANIM_A9, 0 },
    { NUMTEXTS + ICON_PORT, 20, NPCS_PORTRAIT, 0 },
    { 0 }
};
ui_table_t objects_sprs = { objects_sprshdr, 0, 20, 20, 0, 0, 0, 0, 0, 0, 0, -1, SPRPERLYR, sizeof(int), objects_drawspr,
    objects_renderspr, NULL, NULL };

ui_tablehdr_t objects_eqhdr[] = {
    { OBJECTS_EQUIPSLOTS, 0, 0, 0 },
    { 0 }
};
ui_table_t objects_equip = { objects_eqhdr, 0, 22, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), NULL, NULL, NULL, NULL };

ui_tablehdr_t objects_attrshdr[] = {
    { -1, 20, 0, 0 },
    { OBJECTS_REQATTRS, 0, 0, 0 },
    { LANG_VALUE, 82, 0, 0 },
    { 0 }
};
ui_table_t objects_attrs = { objects_attrshdr, CHARS_NOATTR, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(chars_attrs_t),
    objects_drawattr, NULL, NULL, NULL };

ui_tablehdr_t objects_modshdr[] = {
    { -1, 20, 0, 0 },
    { OBJECTS_MODATTRS, 0, 0, 0 },
    { LANG_VALUE, 66, 0, 0 },
    { 0 }
};
ui_table_t objects_mods = { objects_modshdr, CHARS_NOATTR, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(chars_attrs_t),
    objects_drawmod, NULL, NULL, NULL };

/**
 * The form
 */
ui_form_t objects_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, OBJECTS_DELETE, (void*)ICON_REMOVE, objects_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, objects_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, OBJECTS_PVIEWEQP, (void*)ICON_PVIEW, objects_preview },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, objects_new },
    { FORM_SPRITE, 0, 30, 40, 18, 0, OBJECTS_BYPIC, &objects_bypic, objects_select },
    { FORM_TABLE, 10, 30, 200, 0, 0, 0, &objects_tbl, objects_load },
    { FORM_INPUT, 0, 0, 0, 20, 0, OBJECTS_DUPLICATE, &objects_nameinp, NULL },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, OBJECTS_DUPLICATE, (void*)ICON_COPY, objects_duplicate },
    /* 8 */
    { FORM_ICONGRP, 0, 30, 2*24-4, 20, 0, 0, &objects_sel, NULL },
    /* 9 */
    { FORM_SELECT, 0, 54, 210, 20, 0, OBJECTS_ACTIONGRP, &objects_actiongrp, NULL },
    /* 10 */
    { FORM_SELECT, 0, 84, 0, 20, 0, 0, &objects_event, NULL },
    { FORM_NUM, 0, 84, 38, 20, 0, NPCS_SDIST, &objects_eventnum1, NULL },
    { FORM_NUM, 0, 84, 38, 20, 0, NPCS_SDIST, &objects_eventnum2, NULL },
    { FORM_SPRITE, 0, 84, 0, 20, 0, 0, &objects_eventobj[0], NULL },
    { FORM_SPRITE, 0, 84, 0, 20, 0, 0, &objects_eventobj[1], NULL },
    { FORM_SPRITE, 0, 84, 0, 20, 0, 0, &objects_eventobj[2], NULL },
    { FORM_SELECT, 0, 84, 62, 20, 0, 0, &objects_eventtimers, NULL },
    /* 17 */
    { FORM_CMDPAL, 0, 108, 38, 0, 0, 0, &objects_pal, NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[0], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[1], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[2], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[3], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[4], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[5], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[6], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[7], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[8], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[9], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[10], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[11], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[12], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[13], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[14], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[15], NULL },
    { FORM_COMMANDS, 0, 108, 0, 0, 0, NPCS_ONEVENT, &objects_cmd[16], NULL },
    /* 35 */
    { FORM_COLL, 0, 54, 0, 0, 0, 0, &objects_coll, NULL },
    /* 36 */
    { FORM_TABLE, 0, 0, 415, 46, 0, 0, &objects_sprs, objects_lyrclk },
    { FORM_SELECT, 0, 0, 0, 20, 0, OBJECTS_EQUIPBY, &objects_charopt, NULL },
    { FORM_CHARBTN, 10, 10, 20, 20, 0, OBJECTS_FORE, (void*)11027, objects_lyrtgl },
    { FORM_NUM, 0, 0, 32, 20, 0, OBJECTS_LAYERORD, &objects_layer, NULL },
    /* 40 */
    { FORM_TIME, 0, 0, 56, 20, 0, OBJECTS_PROJTIME, &objects_projtime, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, OBJECTS_PROJSPR, &objects_projectile, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, OBJECTS_PROJUNIT, &objects_projunit, NULL },
    /* 43 */
    { FORM_TEXT, 0, 54, 0, 20, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 54, 0, 20, 0, OBJECTS_TITLE, &objects_titinp, NULL },
    { FORM_SPRITE, 0, 54, 20, 20, 0, OBJECTS_INVSPRITE, &objects_invspr, NULL },
    /* 46 */
    { FORM_DRAWTBL, 0, 78, 0, 0, 0, 0, &objects_equip, NULL },
    { FORM_SNDSEL, 0, 0, 0, 20, 0, OBJECTS_SOUND, &objects_invsnd, NULL },
    { FORM_ICON, 0, 0, 18, 20, 0, OBJECTS_PRICE, (void*)ICON_MARKET, NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, OBJECTS_PRICE, &objects_pricenum, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, OBJECTS_PRICE, &objects_priceunit, NULL },
    /* 51 */
    { FORM_TABLE, 0, 78, 0, 167, 0, 0, &objects_attrs, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, OBJECTS_DELRQ, (void*)ICON_REMOVE, objects_attrdel },
    { FORM_SELECT, 0, 0, 0, 20, 0, 0, &objects_attr, NULL },
    { FORM_SELECT, 0, 0, 40, 20, 0, 0, &objects_rel, NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, 0, &objects_val, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, OBJECTS_ADDRQ, (void*)ICON_ADD, objects_attradd },
    /* 57 */
    { FORM_TABLE, 0, 0, 0, 0, 0, 0, &objects_mods, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, OBJECTS_DELATR, (void*)ICON_REMOVE, objects_moddel },
    { FORM_BOOL, 0, 0, 16, 20, 0, OBJECTS_MODEQ, &objects_modeq, NULL },
    { FORM_SELECT, 0, 0, 0, 20, 0, 0, &objects_mattr, NULL },
    { FORM_BOOL, 0, 0, 16, 20, 0, OBJECTS_MODPER, &objects_modper, NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, 0, &objects_mval, NULL },
    { FORM_SELECT, 0, 0, 0, 20, 0, 0, &objects_mtrn, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, OBJECTS_ADDATR, (void*)ICON_ADD, objects_modadd },
    /* 65 */
    { FORM_ICON, 0, 0, 18, 20, 0, OBJECTS_PRICECAT, (void*)ICON_SHELF, NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, OBJECTS_PRICECAT, &objects_cat, NULL },
    /* 67 */
    { FORM_BOOL, 0, 0, 0, 20, 0, OBJECTS_SKILL, &objects_skill, NULL },
    { FORM_TEXT, 0, 0, 16, 20, 0, 0, "\xC2\x83", NULL },
    /* 69 */
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[0], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[0], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[1], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[1], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[2], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[2], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[3], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[3], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[4], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[4], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[5], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[5], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[6], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[6], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[7], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[7], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[8], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[8], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[9], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[9], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[10], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[10], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[11], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[11], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[12], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[12], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[13], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[13], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[14], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[14], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[15], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[15], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[16], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[16], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[17], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[17], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[18], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[18], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[19], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[19], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[20], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[20], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[21], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[21], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[22], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[22], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[23], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[23], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[24], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[24], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[25], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[25], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[26], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[26], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[27], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[27], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[28], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[28], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[29], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[29], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[30], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[30], NULL },
    { FORM_BOOL,   0, 0, 0, 20, 0, 0, &objects_eqslot[31], NULL }, { FORM_TEXT, 0, 0, 0, 20, 0, 0, NULL, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &objects_eqspr[31], NULL },

    { FORM_LAST }
};

/**
 * Draw table cell
 */
void objects_drawspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    ui_sprite_t *s;
    SDL_Rect rect;

    (void)hdr;
    if(!data || idx >= objects_sprs.num) return;
    s = spr_getidx((idx % SPRPERLYR) == SPRPERLYR - 1 ? objects_spr_hidden2.cat : objects_spr_hidden.cat, *((int*)data), -1, 0);
    if(!s)
        ui_icon(dst, x + 2, y, ICON_INADD);
    else
    if(idx != sel) {
        ui_fit(w, h, s->w, s->h, &rect.w, &rect.h);
        rect.x = x + (w - rect.w) / 2; rect.y = y + h - rect.h;
        spr_blit(s, 160, dst, &rect);
    } else
        spr_texture(s, &objects_anim);
}

/**
 * Draw table cell
 */
void objects_renderspr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    SDL_Rect rect;

    (void)dst; (void)hdr;
    if(!data || !objects_anim || *((int*)data) < 0 || idx != sel) return;
    rect.x = x; rect.y = y; rect.w = w; rect.h = h;
    spr_render(spr_getidx((idx % SPRPERLYR) == SPRPERLYR - 1 ? objects_spr_hidden2.cat : objects_spr_hidden.cat,
        *((int*)data), -1, 1), -1, objects_anim, &rect);
}

/**
 * Draw table cell
 */
void objects_drawattr(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    chars_attrs_t *attr = (chars_attrs_t*)data;
    char tmp[16];

    (void)idx; (void)sel; (void)w; (void)h;
    if(!data) return;
    ui_icon(dst, x + 4, y + 1, ICON_QMARK);
    ui_text(dst, x + 4 + hdr[0].w, y + 1, project.attrs[attr->attr]);
    if(attr->rel >= 0)
        ui_text(dst, x + 4 + hdr[0].w + hdr[1].w, y + 1, project_rels[attr->rel]);
    sprintf(tmp, "%6d", attr->val);
    ui_text(dst, x + 20 + hdr[0].w + hdr[1].w, y + 1, tmp);
}

/**
 * Draw table cell
 */
void objects_drawmod(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    chars_attrs_t *attr = (chars_attrs_t*)data;
    char tmp[16];

    (void)idx; (void)sel; (void)w; (void)h;
    if(!data) return;
    if(!(attr->rel & 2))
        ui_icon(dst, x + 2, y + 1, ICON_INVENT);
    if(attr->attr < 0) {
        ui_text(dst, x + 4 + hdr[0].w, y + 1, lang[OBJECTS_TRANSPORT]);
        ui_text(dst, x + 4 + hdr[0].w + hdr[1].w, y + 1, project_trns[attr->val]);
    } else {
        ui_text(dst, x + 4 + hdr[0].w, y + 1, project.attrs[attr->attr]);
        sprintf(tmp, "%s%d%s", attr->val >= 0 ? "+" : "", attr->val, attr->rel & 1 ? "%" : " ");
        ui_text(dst, x + 4 + hdr[0].w + hdr[1].w + 6*8 - ui_textwidth(tmp), y + 1, tmp);
    }
}

/**
 * Exit objects window
 */
void objects_exit(int tab)
{
    int i;

    (void)tab;
    project_freedir((char***)&objects_tbl.data, &objects_tbl.num);
    objects_tbl.val = objects_tbl.clk = -1;
    objects_tbl.scr = 0;
    for(i = 0; i < 16; i++)
        ui_cmd_free(&objects_cmd[i]);
    if(objects_charopt.opts) { free(objects_charopt.opts); objects_charopt.opts = NULL; }
    if(objects_attrs.data) { free(objects_attrs.data); objects_attrs.data = NULL; }
    objects_attrs.num = 0; objects_attrs.clk = objects_attrs.val = -1;
    if(objects_mods.data) { free(objects_mods.data); objects_mods.data = NULL; }
    objects_mods.num = 0; objects_mods.clk = objects_mods.val = -1;
    if(objects_coll.mask.data) { free(objects_coll.mask.data); objects_coll.mask.data = NULL; }
    chars_free();
    if(objects_spr) { free(objects_spr); objects_spr = NULL; }
    objects_sprs.data = NULL;
    hud_exit(SUBMENU_HUD);
    if(objects_anim) SDL_DestroyTexture(objects_anim);
    objects_anim = NULL;
    ui_sprite_free(1);
}

/**
 * Enter objects window
 */
void objects_init(int tab)
{
    hud_equip_t *equi;
    chars_opt_t *opts;
    int i;

    objects_exit(tab);
    objects_form[1].param = lang[LANG_SAVE];
    objects_form[1].w = ui_textwidth(objects_form[1].param) + 40;
    if(objects_form[1].w < 200) objects_form[1].w = 200;
    objects_form[43].param = lang[OBJECTS_TITLE];
    objects_form[43].w = ui_textwidth(objects_form[43].param);

    objects_tbl.data = project_loadobjs(&objects_tbl.num);
    if(objects_tbl.val >= objects_tbl.num) objects_tbl.val = objects_tbl.num - 1;
    objects_attr.opts = objects_mattr.opts = project.attrs;
    objects_coll.spr = &objects_bypic;

    objects_events[0] = lang[CMD_EVT_ONCLICK];
    objects_events[1] = lang[CMD_EVT_ONTOUCH];
    objects_events[2] = lang[CMD_EVT_ONLEAVE];
    objects_events[3] = lang[CMD_EVT_ONAPPROACH];
    objects_events[4] = actions_name[0][0] ? actions_name[0] : lang[CMD_EVT_ONACTION1];
    objects_events[5] = actions_name[1][0] ? actions_name[1] : lang[CMD_EVT_ONACTION2];
    objects_events[6] = actions_name[2][0] ? actions_name[2] : lang[CMD_EVT_ONACTION3];
    objects_events[7] = actions_name[3][0] ? actions_name[3] : lang[CMD_EVT_ONACTION4];
    objects_events[8] = actions_name[4][0] ? actions_name[4] : lang[CMD_EVT_ONACTION5];
    objects_events[9] = actions_name[5][0] ? actions_name[5] : lang[CMD_EVT_ONACTION6];
    objects_events[10] = actions_name[6][0] ? actions_name[6] : lang[CMD_EVT_ONACTION7];
    objects_events[11] = actions_name[7][0] ? actions_name[7] : lang[CMD_EVT_ONACTION8];
    objects_events[12] = actions_name[8][0] ? actions_name[8] : lang[CMD_EVT_ONACTION9];
    objects_events[13] = lang[CMD_EVT_ONOBJ1];
    objects_events[14] = lang[CMD_EVT_ONOBJ2];
    objects_events[15] = lang[CMD_EVT_ONOBJ3];
    objects_events[16] = lang[CMD_EVT_ONTIMER];

    hud_load();
    equi = (hud_equip_t*)hud_equiptbl.data;
    for(i = 0; equi && i < 32 && i < hud_equiptbl.num; i++)
        objects_form[70 + i * 3].param = equi[i].name;

    chars_loadopt(chars_loadcfg(NULL));
    opts = chars_opts.data;
    objects_charopt.opts = (char**)main_alloc((chars_opts.num + 1) * sizeof(char*));
    if(opts && chars_opts.num) {
        for(i = 0; i < chars_opts.num; i++)
            objects_charopt.opts[i] = opts[i].opt;
    }
    objects_spr = (int*)main_alloc((chars_opts.num + 1) * 2 * SPRPERLYR * sizeof(int));
    /* we must reload from disk because array indeces might have changed */
    if(objects_tbl.val < 0)
        objects_new(NULL);
    else
        objects_load(NULL);
}

/**
 * Resize the view
 */
void objects_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    objects_form[0].y = objects_form[1].y = objects_form[2].y = objects_form[3].y = objects_form[6].y = objects_form[7].y =
        h - 48;
    objects_form[1].x = w - 20 - objects_form[1].w;
    objects_form[2].x = objects_form[1].x - 52;
    ui_table_resize(&objects_form[5], objects_form[5].w, h - 89);
    objects_form[3].x = objects_form[5].x + objects_form[5].w + 20;
    objects_form[4].x = objects_form[9].x = objects_form[10].x = objects_form[5].x + objects_form[5].w + 10;
    objects_form[7].x = objects_form[5].x + objects_form[5].w - 10 - objects_form[7].w;
    objects_form[6].x = objects_form[0].x + objects_form[0].w + 20;
    objects_form[6].w = objects_form[7].x - objects_form[6].x - 4;
    objects_form[8].x = objects_form[4].x + objects_form[4].w + 20;
    objects_form[35].w = (w - 10 - objects_form[9].x) / 2 - 5;
    objects_form[35].x = w - 10 - objects_form[35].w;
    ui_coll_resize(&objects_coll, objects_form[35].w, objects_form[35].h);
    objects_form[9].w = objects_form[35].x - objects_form[9].x - 10;
    objects_form[11].x = objects_form[9].x + objects_form[9].w - objects_form[11].w;
    objects_form[12].x = objects_form[9].x + objects_form[9].w - objects_form[12].w;
    objects_form[13].x = objects_form[14].x = objects_form[15].x = objects_form[9].x + objects_form[9].w / 2 + 4;
    objects_form[13].w = objects_form[14].w = objects_form[15].w = objects_form[9].w / 2 - 2;
    objects_form[16].x = objects_form[9].x + objects_form[9].w - objects_form[16].w;
    objects_form[17].x = objects_form[9].x + objects_form[9].w - objects_form[17].w;
    objects_form[17].h = h - 59 - 56 - objects_form[17].y;
    for(i = 0; i < 17; i++) {
        objects_form[i + 18].x = objects_form[5].x + objects_form[5].w + 10;
        objects_form[i + 18].w = objects_form[17].x - objects_form[i + 18].x - 4;
        objects_form[i + 18].h = objects_form[17].h;
    }
    objects_form[35].h = h - 59 - objects_form[36].h - 10 - objects_form[35].y;
    objects_form[36].y = objects_form[37].y = h - 59 - objects_form[36].h;
    objects_form[36].x = w - 10 - objects_form[36].w;
    ui_table_resize(&objects_form[36], objects_form[36].w, objects_form[36].h);
    objects_form[37].w = objects_form[36].x - objects_form[3].x; if(objects_form[37].w > 200) objects_form[37].w = 200;
    objects_form[37].x = objects_form[36].x - objects_form[37].w - 4;
    objects_form[38].y = objects_form[39].y = objects_form[40].y = objects_form[41].y = objects_form[42].y =
        objects_form[37].y + objects_form[37].h + 4;
    objects_form[38].x = objects_form[36].x - 4 - objects_form[38].w;
    objects_form[39].x = objects_form[38].x - 4 - objects_form[39].w;

    objects_form[41].x = objects_form[43].x = objects_form[46].x = objects_form[48].x = objects_form[65].x =
        objects_form[5].x + objects_form[5].w + 10;
    objects_form[40].x = objects_form[41].x + 4 + objects_form[41].w;
    objects_form[45].x = w - 10 - objects_form[45].w;
    objects_form[44].x = objects_form[43].x + objects_form[43].w + 10;
    objects_form[44].w = objects_form[45].x - 4 - objects_form[44].x;
    ui_table_resize(&objects_form[46], (w - 10 - objects_form[46].x) / 2 - 5, h - 59 - 72 - objects_form[46].y);
    objects_form[66].x = objects_form[49].x = objects_form[46].x + 18;
    objects_form[47].y = objects_form[65].y = objects_form[66].y = objects_form[46].y + objects_form[46].h + 4;
    objects_form[48].y = objects_form[49].y = objects_form[50].y = objects_form[47].y + objects_form[47].h + 4;
    objects_form[50].x = objects_form[42].x = objects_form[49].x + objects_form[49].w + 4;
    objects_form[50].w = objects_form[42].w = objects_form[46].x + objects_form[46].w - objects_form[50].x;
    objects_form[47].x = objects_form[50].x + 18; objects_form[47].w = objects_form[50].w - 18;
    ui_table_resize(&objects_form[51], objects_form[46].w, objects_form[51].h);
    objects_form[51].x = objects_form[52].x = objects_form[57].x = objects_form[58].x =
        objects_form[46].x + objects_form[46].w + 10;
    objects_form[52].y = objects_form[53].y = objects_form[54].y = objects_form[55].y = objects_form[56].y =
        objects_form[51].y + objects_form[51].h + 4;
    objects_form[53].x = objects_form[52].x + objects_form[52].w + 20;
    objects_form[56].x = objects_form[64].x = w - 10 - objects_form[56].w;
    objects_form[55].x = objects_form[56].x - 4 - objects_form[55].w;
    objects_form[54].x = objects_form[55].x - 4 - objects_form[54].w;
    objects_form[53].w = objects_form[54].x - objects_form[53].x - 4;

    objects_form[57].y = objects_form[52].y + objects_form[52].h + 10;
    ui_table_resize(&objects_form[57], objects_form[51].w, h - 59 - 24 - objects_form[57].y);
    objects_form[58].y = objects_form[59].y = objects_form[60].y = objects_form[61].y = objects_form[62].y = objects_form[63].y =
        objects_form[64].y = objects_form[57].y + objects_form[57].h + 4;
    objects_form[62].x = objects_form[56].x - objects_form[55].w - 4;
    objects_form[61].x = objects_form[63].x = objects_form[62].x - objects_form[61].w;
    objects_form[59].x = objects_form[58].x + objects_form[58].w + 20;
    objects_form[60].x = objects_form[59].x + objects_form[59].w;
    objects_form[60].w = objects_form[61].x - objects_form[60].x - 10;
    objects_form[63].w = objects_form[64].x - objects_form[63].x - 4;

    objects_form[67].y = objects_form[46].y + 23;
    objects_form[67].x = objects_form[46].x + 8;
    objects_form[67].w = objects_form[46].w - 42;
    objects_form[68].y = objects_form[46].y + 23 + objects_equip.row;
    objects_form[68].x = objects_form[46].x + objects_form[46].w - 30 - objects_form[68].w;
    for(i = 0; i < 32; i++) {
        objects_form[69 + i * 3].y = objects_form[70 + i * 3].y = objects_form[71 + i * 3].y =
            objects_form[46].y + 23 + (i + 1) * objects_equip.row;
        objects_form[69 + i * 3].x = objects_form[46].x + 8;
        objects_form[69 + i * 3].w = objects_form[46].w - 28 - 8;
        objects_form[70 + i * 3].x = objects_form[46].x + 24;
        objects_form[70 + i * 3].w = objects_form[46].w - 28 - 24;
        objects_form[71 + i * 3].x = objects_form[46].x + objects_form[46].w - 28;
    }
}

/**
 * View layer
 */
void objects_redraw(int tab)
{
    int i, j;

    (void)tab;
    ui_form = objects_form;
    ui_form[0].inactive = (objects_tbl.val < 0);
    ui_form[1].inactive = ui_form[2].inactive = (objects_bypic.val < 0);
    ui_form[6].inactive = (objects_tbl.val < 0);
    ui_form[7].inactive = (objects_tbl.val < 0 || !objects_name[0]);
    for(i = 9; ui_form[i].type; i++)
        ui_form[i].inactive = (i < 40 ? (objects_tab ? 2 : 0) : (objects_tab ? 0 : 2));
    if(!objects_tab) {
        ui_form[2].status = OBJECTS_PVIEWEQP;
        ui_form[11].inactive = (objects_event.val == 2 ? 0 : 2);
        ui_form[12].inactive = (objects_event.val == 3 ? 0 : 2);
        ui_form[13].inactive = (objects_event.val == 13 ? 0 : 2);
        ui_form[14].inactive = (objects_event.val == 14 ? 0 : 2);
        ui_form[15].inactive = (objects_event.val == 15 ? 0 : 2);
        ui_form[16].inactive = (objects_event.val == 16 ? 0 : 2);
        ui_form[10].w = (objects_event.val == 2 || objects_event.val == 3 ? ui_form[9].w - ui_form[11].w - 4 :
            (objects_event.val == 16 ? ui_form[9].w - ui_form[16].w - 4 :
            (objects_event.val >= 13 ? ui_form[9].w / 2 - 2 : ui_form[9].w)));
        for(i = 0; i < 9; i++)
            objects_sprshdr[10 + i].label = (objects_actiongrp.val != i ? ICON_INACT : ICON_NUMACT + 1 + i) + NUMTEXTS;
        for(i = 0; i < 17; i++)
            ui_form[i + 18].inactive = (objects_event.val == i ? 0 : 2);
        ui_form[38].status = (objects_fore ? OBJECTS_FORE : OBJECTS_BACK);
        ui_form[38].param = (void*)((uintptr_t)(11026 + objects_fore));
        objects_sprs.data = objects_spr ? objects_spr + (objects_charopt.val + 1) * 2 * SPRPERLYR + objects_fore * SPRPERLYR : NULL;
    } else {
        ui_form[2].status = OBJECTS_PVIEWINV;
        ui_form[50].inactive = (!objects_pricenum.val);
        ui_form[52].inactive = (!objects_attrs.num);
        ui_form[58].inactive = (!objects_mods.num);
        ui_form[64].inactive = (objects_mattr.val >= 0 && !objects_mval.val);
        ui_form[61].inactive = ui_form[62].inactive = (objects_mattr.val == -1 ? 2 : 0);
        ui_form[63].inactive = (objects_mattr.val != -1 ? 2 : 0);
        if(objects_skill) {
            ui_form[68].inactive = 1;
            for(i = 0; i < 32 && i < hud_equiptbl.num; i++)
                ui_form[69 + i * 3].inactive = ui_form[70 + i * 3].inactive = ui_form[71 + i * 3].inactive = 1;
        }
        /* disable what doesn't fit on screen. Not the best, but even with the smallest window size there's place for 12 slots */
        j = (ui_form[46].h - 23) / objects_equip.row - 1;
        for(i = (j < hud_equiptbl.num ? j : hud_equiptbl.num); i < 32; i++)
            ui_form[69 + i * 3].inactive = ui_form[70 + i * 3].inactive = ui_form[71 + i * 3].inactive = 2;
        if(hud_equiptbl.num < 1) ui_form[68].inactive = 2;
    }
}

/**
 * Toggle foreground / background layer
 */
void objects_lyrtgl(void *data)
{
    (void)data;
    objects_fore ^= 1;
}

/**
 * Click on a sprite on a layer, pop up sprite picker
 */
void objects_lyrclk(void *data)
{
    int *lyrs = (int*)objects_sprs.data;

    (void)data;
    if(!lyrs || objects_sprs.val < 0 || objects_sprs.val >= objects_sprs.num) return;
    if(ui_curbtn == 1) {
        ui_pressed = -1;
        ui_sprite_ptr = &lyrs[objects_sprs.val];
        ui_sprite_picker(scr_w, scr_h, (objects_sprs.val % SPRPERLYR) == SPRPERLYR - 1 ? &objects_spr_hidden2 : &objects_spr_hidden);
    } else
        lyrs[objects_sprs.val] = -1;
}

/**
 * Erase a required attribute from object
 */
void objects_attrdel(void *data)
{
    chars_attrs_t *attrs = (chars_attrs_t*)objects_attrs.data;

    (void)data;
    if(!attrs || objects_attrs.val < 0 || objects_attrs.val >= objects_attrs.num) return;
    memcpy(&attrs[objects_attrs.val], &attrs[objects_attrs.val + 1],
        (objects_attrs.num - objects_attrs.val) * sizeof(chars_attrs_t));
    objects_attrs.num--;
    if(objects_attrs.val >= objects_attrs.num)
        objects_attrs.val = objects_attrs.num - 1;
    if(!objects_attrs.num) { free(attrs); objects_attrs.data = NULL; }
}

/**
 * Add a new required attribute to object
 */
void objects_attradd(void *data)
{
    chars_attrs_t *attrs = (chars_attrs_t*)objects_attrs.data;
    int i;

    (void)data;
    for(i = 0; attrs && i < objects_attrs.num; i++)
        if(attrs[i].attr == objects_attr.val) {
            objects_attrs.val = i;
            attrs[i].rel = objects_rel.val;
            attrs[i].val = objects_val.val;
            return;
        }
    attrs = (chars_attrs_t*)realloc(attrs, (objects_attrs.num + 1) * sizeof(chars_attrs_t));
    objects_attrs.data = attrs;
    if(!attrs) main_error(ERR_MEM);
    i = objects_attrs.val = objects_attrs.num++;
    attrs[i].rel = objects_rel.val;
    attrs[i].val = objects_val.val;
    attrs[i].attr = objects_attr.val;
}

/**
 * Erase an attribute modifier from object
 */
void objects_moddel(void *data)
{
    chars_attrs_t *attrs = (chars_attrs_t*)objects_mods.data;

    (void)data;
    if(!attrs || objects_mods.val < 0 || objects_mods.val >= objects_mods.num) return;
    memcpy(&attrs[objects_mods.val], &attrs[objects_mods.val + 1], (objects_mods.num - objects_mods.val) * sizeof(chars_attrs_t));
    objects_mods.num--;
    if(objects_mods.val >= objects_mods.num)
        objects_mods.val = objects_mods.num - 1;
    if(!objects_mods.num) { free(attrs); objects_mods.data = NULL; }
}

/**
 * Add a new attribute modifier to object
 */
void objects_modadd(void *data)
{
    chars_attrs_t *attrs = (chars_attrs_t*)objects_mods.data;
    int i;

    (void)data;
    for(i = 0; attrs && i < objects_mods.num; i++)
        if(attrs[i].attr == objects_mattr.val) {
            objects_mods.val = i;
            attrs[i].rel = objects_modper + (objects_modeq << 1);
            attrs[i].val = objects_mattr.val < 0 ? objects_mtrn.val : objects_mval.val;
            return;
        }
    attrs = (chars_attrs_t*)realloc(attrs, (objects_mods.num + 1) * sizeof(chars_attrs_t));
    objects_mods.data = attrs;
    if(!attrs) main_error(ERR_MEM);
    i = objects_mods.val = objects_mods.num++;
    attrs[i].rel = objects_modper + (objects_modeq << 1);
    attrs[i].val = objects_mattr.val < 0 ? objects_mtrn.val : objects_mval.val;
    attrs[i].attr = objects_mattr.val;
}

/**
 * Duplicate object
 */
void objects_duplicate(void *data)
{
    char **list = (char**)objects_tbl.data, *str, *s;
    unsigned char *buf;
    int i;
    unsigned int size;
    FILE *f;

    (void)data;
    if(!objects_name[0] || objects_tbl.val < 0 || objects_tbl.val >= objects_tbl.num || !project.sprites[objects_bypic.cat])
        return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.obj", project.id, project_dirs[PROJDIRS_OBJECTS], objects_name);
    for(i = 0; i < objects_tbl.num && strcmp(list[i], objects_name); i++);
    if(i < objects_tbl.num && !ui_modal(ICON_WARN, ERR_EXISTS, projfn)) return;
    /* failsafe */
    for(objects_bypic.val = 0; objects_bypic.val < project.spritenum[objects_bypic.cat] &&
        strcmp(project.sprites[objects_bypic.cat][objects_bypic.val].name, list[objects_tbl.val]); objects_bypic.val++);
    if(objects_bypic.val >= project.spritenum[objects_bypic.cat]) { objects_bypic.val = -1; return; }
    /* copy the configuration */
    str = project_loadfile(project_dirs[PROJDIRS_OBJECTS], list[objects_tbl.val], "obj", PROJMAGIC_OBJECT, "objects_duplicate");
    s = project_skipnl(str);
    f = project_savefile(0, project_dirs[PROJDIRS_OBJECTS], objects_name, "obj", PROJMAGIC_OBJECT, "objects_duplicate");
    if(f) {
        if(s) fprintf(f, "%s", s);
        fclose(f);
        for(i = 0; i < 8; i++)
            if(project.sprites[objects_bypic.cat][objects_bypic.val].dir[i].data) {
                s = projfn + sprintf(projfn, "%s" SEP "%s" SEP "%s_", project.id,
                    project_dirs[PROJDIRS_SPRITES + objects_bypic.cat],
                    project.sprites[objects_bypic.cat][objects_bypic.val].name);
                spr_wrname(objects_bypic.cat, objects_bypic.val, i, s);
                s += 4; strcpy(s, ".png");
                if(verbose) printf("objects_duplicate: reading sprite from %s\r\n", projdir);
                buf = readfile(projdir, &size);
                if(buf) {
                    s = projfn + sprintf(projfn, "%s" SEP "%s" SEP "%s_", project.id,
                        project_dirs[PROJDIRS_SPRITES + objects_bypic.cat], objects_name);
                    spr_wrname(objects_bypic.cat, objects_bypic.val, i, s);
                    strcpy(s + 4, ".png");
                    f = project_fopen(projdir, "wb+");
                    if(f) {
                        if(verbose) printf("objects_duplicate: writing sprite to %s\r\n", projdir);
                        fwrite(buf, size, 1, f);
                        fclose(f);
                    }
                    free(buf);
                }
            }
        /* copy sprites for every existing directions too */
        spr_copy(objects_bypic.cat, objects_bypic.val, objects_name);
        /* refresh central lists */
        project_freedir((char***)&objects_tbl.data, &objects_tbl.num);
        objects_tbl.data = project_loadobjs(&objects_tbl.num);
        if(objects_tbl.val >= objects_tbl.num) objects_tbl.val = objects_tbl.num - 1;
        list = (char**)objects_tbl.data;
        /* find the index of new object in the new lists */
        for(objects_tbl.val = 0; objects_tbl.val < objects_tbl.num && list &&
            strcmp(list[objects_tbl.val], objects_name); objects_tbl.val++);
        if(objects_tbl.val >= objects_tbl.num) objects_tbl.val = -1;
        for(objects_bypic.val = 0; objects_bypic.val < project.spritenum[objects_bypic.cat] &&
            strcmp(project.sprites[objects_bypic.cat][objects_bypic.val].name, objects_name); objects_bypic.val++);
        if(objects_bypic.val >= project.spritenum[objects_bypic.cat]) objects_bypic.val = -1;
        /* restore clobbered filename for status report */
        sprintf(projfn, "%s" SEP "%s" SEP "%s.obj", project.id, project_dirs[PROJDIRS_OBJECTS], objects_name);
        ui_status(0, lang[LANG_SAVED]);
        objects_name[0] = 0;
    } else
        ui_status(1, lang[ERR_SAVEOBJ]);
    if(str) free(str);
}

/**
 * Clear form, new object
 */
void objects_new(void *data)
{
    int i;

    (void)data;
    objects_name[0] = objects_title[0] = 0;
    objects_modeq = objects_modper = objects_skill = objects_event.val = objects_cat.val = 0;
    objects_eventobj[0].val = objects_eventobj[1].val = objects_eventobj[2].val = objects_eventtimers.val = objects_pricenum.val =
        objects_attr.val = objects_val.val = objects_mval.val = 0;
    objects_fore = objects_eventnum1.val = objects_eventnum2.val = 1;
    objects_bypic.val = objects_actiongrp.val = objects_charopt.val = objects_projectile.val = objects_invspr.val =
        objects_invsnd.val = objects_priceunit.val = objects_projunit.val = objects_mattr.val = -1;
    objects_projtime.val = 10;
    for(i = 0; objects_spr && i < (chars_opts.num + 1) * 2 * SPRPERLYR; i++)
        objects_spr[i] = -1;
    for(i = 0; i < 32; i++) {
        objects_eqslot[i] = 0;
        objects_eqspr[i].val = -1;
        objects_eqspr[i].cat = 1;
        objects_eqspr[i].icon = 158;
        objects_eqspr[i].unique = 1;
    }
    for(i = 0; i < 17; i++)
        ui_cmd_free(&objects_cmd[i]);
    if(objects_attrs.data) { free(objects_attrs.data); objects_attrs.data = NULL; }
    objects_attrs.num = 0; objects_attrs.clk = objects_attrs.val = -1;
    if(objects_mods.data) { free(objects_mods.data); objects_mods.data = NULL; }
    objects_mods.num = 0; objects_mods.clk = objects_mods.val = -1;
    if(objects_coll.mask.data) { free(objects_coll.mask.data); objects_coll.mask.data = NULL; }
    objects_coll.mask.w = objects_coll.mask.h = 0;
}

/**
 * Delete object
 */
void objects_delete(void *data)
{
    char **list = (char**)objects_tbl.data;

    (void)data;
    if(objects_tbl.val < 0 || !list) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.obj", project.id, project_dirs[PROJDIRS_OBJECTS], list[objects_tbl.val]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("objects_delete: removing %s\n", projdir);
        remove(projdir);
        objects_new(NULL);
        objects_init(SUBMENU_OBJECTS);
    }
}

/**
 * Select object by picture
 */
void objects_select(void *data)
{
    char **list = (char**)objects_tbl.data;

    (void)data;
    if(objects_bypic.val < 0) return;
    for(objects_tbl.val = 0; objects_tbl.val < objects_tbl.num && list &&
        strcmp(project.sprites[objects_bypic.cat][objects_bypic.val].name, list[objects_tbl.val]); objects_tbl.val++);
    if(objects_tbl.val >= objects_tbl.num) objects_tbl.val = -1;
    if(objects_coll.mask.data) { free(objects_coll.mask.data); objects_coll.mask.data = NULL; }
    objects_coll.mask.w = objects_coll.mask.h = 0;
    objects_load(NULL);
}

/**
 * Save object
 */
void objects_save(void *data)
{
    hud_equip_t *equi = (hud_equip_t*)hud_equiptbl.data;
    chars_attrs_t *attrs = (chars_attrs_t*)objects_attrs.data;
    chars_attrs_t *mods = (chars_attrs_t*)objects_mods.data;
    char **list;
    int i, x, y;
    FILE *f;

    (void)data;
    if(objects_bypic.val < 0) return;
    f = project_savefile(0, project_dirs[PROJDIRS_OBJECTS], project.sprites[objects_bypic.cat][objects_bypic.val].name, "obj",
        PROJMAGIC_OBJECT, "objects_save");
    if(f) {
        fprintf(f, "%s\r\n", objects_title[0] ? objects_title : "-");
        /* detect if collision mask is empty */
        if(!objects_coll.mask.data)
            objects_coll.mask.w = objects_coll.mask.h = 0;
        else {
            for(i = 0; i < objects_coll.mask.w * objects_coll.mask.h && !objects_coll.mask.data[i]; i++);
            if(i >= objects_coll.mask.w * objects_coll.mask.h)
                objects_coll.mask.w = objects_coll.mask.h = 0;
        }
        /* parameters */
        fprintf(f, "%u %u %u ", objects_coll.mask.w, objects_coll.mask.h, objects_actiongrp.val + 1);
        project_wrsprite(f, &objects_invspr); fprintf(f, " ");
        project_wridx(f, objects_invsnd.val, project.sounds); fprintf(f, " ");
        fprintf(f, "%d ", objects_pricenum.val);
        if(objects_pricenum.val)
            project_wrsprite(f, &objects_priceunit);
        else
            fprintf(f, "-");
        fprintf(f, " %u %u %u\r\n", objects_cat.val, objects_skill, objects_layer.val);
        /* collision mask */
        for(y = i = 0; y < objects_coll.mask.h; y++) {
            for(x = 0; x < objects_coll.mask.w; x++, i++)
                fprintf(f, "%s%u", x ? " " : "", objects_coll.mask.data[i]);
            fprintf(f, "\r\n");
        }
        /* equipment slots */
        for(i = 0; equi && i < 32 && i < hud_equiptbl.num; i++)
            if(objects_eqslot[i]) {
                fprintf(f, "s %s ", equi[i].name);
                project_wrsprite(f, &objects_eqspr[i]);
                fprintf(f, "\r\n");

            }
        /* required attributes */
        for(i = 0; attrs && i < objects_attrs.num; i++)
            fprintf(f, "r %s%s%d\r\n", project.attrs[attrs[i].attr], project_rels[attrs[i].rel], attrs[i].val);
        /* attribute modifiers */
        for(i = 0; mods && i < objects_mods.num; i++)
            fprintf(f, "m %d %d %s %d\r\n", (mods[i].rel >> 1) & 1, mods[i].rel & 1,
                mods[i].attr >= 0 ? project.attrs[mods[i].attr] : "-", mods[i].val);
        /* projectile */
        if(objects_projectile.val >= 0 || objects_projtime.val != objects_projtime.min || objects_projunit.val != -1) {
            fprintf(f, "p ");
            project_wrsprite(f, &objects_projectile);
            fprintf(f, " %u ", objects_projtime.val);
            project_wrsprite(f, &objects_projunit);
            fprintf(f, "\r\n");
        }
        if(objects_spr) {
            /* default sprites */
            for(i = 0; i < 2 * SPRPERLYR && objects_spr[i] == -1; i++);
            if(i < 24) {
                fprintf(f, "f");
                for(i = 0; i < SPRPERLYR; i++) {
                    fprintf(f, " ");
                    project_wrsprite2(f, objects_spr[SPRPERLYR + i], (i % SPRPERLYR) == SPRPERLYR - 1 ?
                        objects_spr_hidden2.cat : objects_spr_hidden.cat);
                }
                fprintf(f, "\r\nb");
                for(i = 0; i < SPRPERLYR; i++) {
                    fprintf(f, " ");
                    project_wrsprite2(f, objects_spr[i], (i % SPRPERLYR) == SPRPERLYR - 1 ?
                        objects_spr_hidden2.cat : objects_spr_hidden.cat);
                }
                fprintf(f, "\r\n");
            }
            /* character option specific sprites */
            for(y = 0; y < chars_opts.num; y++) {
                for(i = 0; i < 2 * SPRPERLYR && objects_spr[(y + 1) * 2 * SPRPERLYR + i] == -1; i++);
                if(i == 2 * SPRPERLYR) continue;
                fprintf(f, "o %s\r\n f", objects_charopt.opts[y]);
                for(i = 0; i < SPRPERLYR; i++) {
                    fprintf(f, " ");
                    project_wrsprite2(f, objects_spr[(y + 1) * 2 * SPRPERLYR + SPRPERLYR + i],
                        (i % SPRPERLYR) == SPRPERLYR - 1 ? objects_spr_hidden2.cat : objects_spr_hidden.cat);
                }
                fprintf(f, "\r\n b");
                for(i = 0; i < SPRPERLYR; i++) {
                    fprintf(f, " ");
                    project_wrsprite2(f, objects_spr[(y + 1) * 2 * SPRPERLYR + i],
                        (i % SPRPERLYR) == SPRPERLYR - 1 ? objects_spr_hidden2.cat : objects_spr_hidden.cat);
                }
                fprintf(f, "\r\n");
            }
        }
        /* event handlers */
        for(i = 0; i < 17; i++) {
            if(!objects_cmd[i].root.len) continue;
            fprintf(f, "e %s ", project_eventcodes[i]);
            switch(i) {
                case 2: fprintf(f, "%u ", objects_eventnum1.val); break;
                case 3: fprintf(f, "%u ", objects_eventnum2.val); break;
                case 13:
                case 14:
                case 15: project_wrsprite(f, &objects_eventobj[i - 13]); fprintf(f, " "); break;
                case 16: project_wridx(f, objects_eventtimers.val, project_eventtimers); break;
            }
            project_wrscript(f, &objects_cmd[i]);
        }
        fclose(f);
        project_freedir((char***)&objects_tbl.data, &objects_tbl.num);
        objects_tbl.data = project_loadobjs(&objects_tbl.num);
        if(objects_tbl.val >= objects_tbl.num) objects_tbl.val = objects_tbl.num - 1;
        list = (char**)objects_tbl.data;
        for(objects_tbl.val = 0; objects_tbl.val < objects_tbl.num && list &&
            strcmp(project.sprites[objects_bypic.cat][objects_bypic.val].name, list[objects_tbl.val]); objects_tbl.val++);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.obj", project.id, project_dirs[PROJDIRS_OBJECTS],
            project.sprites[objects_bypic.cat][objects_bypic.val].name);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVEOBJ]);
}

/**
 * Load object
 */
int objects_loadobj(char *fn)
{
    hud_equip_t *equi = (hud_equip_t*)hud_equiptbl.data;
    char *str, *s, t;
    int i, j, x, y, n, o = 0;

    if(!fn || !*fn) return 0;
    str = project_loadfile(project_dirs[PROJDIRS_OBJECTS], fn, "obj", PROJMAGIC_OBJECT, "objects_loadobj");
    if(str) {
        objects_new(NULL);
        s = project_skipnl(str);
        s = project_getstr2(s, objects_title, 2, sizeof(objects_title));
        s = project_skipnl(s);
        s = project_getint(s, &objects_coll.mask.w, 0, 512);
        s = project_getint(s, &objects_coll.mask.h, 0, 512);
        s = project_getint(s, &n, 0, 9); objects_actiongrp.val = n - 1;
        s = project_getsprite(s, &objects_invspr);
        s = project_getidx(s, &objects_invsnd.val, project.sounds, -1);
        s = project_getint(s, &objects_pricenum.val, objects_pricenum.min, objects_pricenum.max);
        s = project_getobj(s, &objects_priceunit.val, -1);
        if(objects_priceunit.val == -1) objects_pricenum.val = 0;
        s = project_getint(s, &objects_cat.val, objects_cat.min, objects_cat.max);
        s = project_getint(s, &objects_skill, 0, 1);
        s = project_getint(s, &objects_layer.val, objects_layer.min, objects_layer.max);
        if(objects_coll.mask.h > 0) {
            if(objects_coll.mask.w > 0)
                objects_coll.mask.data = (char*)main_alloc(objects_coll.mask.w * objects_coll.mask.h);
            objects_coll.scrx = -1;
            for(y = j = 0; *s && y < objects_coll.mask.h; y++) {
                s = project_skipnl(s);
                for(x = 0; *s && x < objects_coll.mask.w; x++, j++) {
                    s = project_getint(s, &n, 0, 15);
                    objects_coll.mask.data[j] = (char)n;
                }
            }
        }
        while(*s) {
            s = project_skipnl(s);
            while(*s == ' ') s++;
            if(!*s || s[1] != ' ') break;
            t = *s; do { s++; } while(*s == ' ');
            switch(t) {
                case 's':
                    for(i = 0, x = -1; equi && i < 32 && i < hud_equiptbl.num; i++) {
                        j = strlen(equi[i].name);
                        if(!memcmp(s, equi[i].name, j) && s[j] == ' ') { s += j + 1; x = i; break; }
                    }
                    if(x >= 0) {
                        objects_eqslot[x] = 1;
                        s = project_getsprite(s, &objects_eqspr[x]);
                    }
                break;
                case 'r':
                    s = project_getidx(s, &objects_attr.val, project.attrs, 0);
                    s = project_getrel(s, &objects_rel.val);
                    s = project_getint(s, &objects_val.val, objects_val.min, objects_val.max);
                    objects_attradd(NULL);
                break;
                case 'm':
                    s = project_getint(s, &objects_modeq, 0, 1);
                    s = project_getint(s, &objects_modper, 0, 1);
                    s = project_getidx(s, &objects_mattr.val, project.attrs, -1);
                    if(objects_mattr.val == -1)
                        s = project_getint(s, &objects_mtrn.val, 0, 2);
                    else
                        s = project_getint(s, &objects_mval.val, objects_mval.min, objects_mval.max);
                    objects_modadd(NULL);
                break;
                case 'p':
                    s = project_getsprite(s, &objects_projectile);
                    s = project_getint(s, &objects_projtime.val, objects_projtime.min, objects_projtime.max);
                    s = project_getobj(s, &objects_projunit.val, -1);
                break;
                case 'o':
                    s = project_getidx(s, &o, objects_charopt.opts, -1);
                    if(o >= 0) o++;
                break;
                case 'f':
                case 'b':
                    if(o >= 0) {
                        j = o * 2 * SPRPERLYR + (t == 'f' ? SPRPERLYR : 0);
                        for(i = 0; i < SPRPERLYR; i++) {
                            s = project_getsprite(s, (i % SPRPERLYR) == SPRPERLYR - 1 ? &objects_spr_hidden2 : &objects_spr_hidden);
                            if(objects_spr)
                                objects_spr[j++] = (i % SPRPERLYR) == SPRPERLYR - 1 ? objects_spr_hidden2.val : objects_spr_hidden.val;
                        }
                    }
                break;
                case 'e':
                    s = project_getidx(s, &i, (char**)project_eventcodes, -1);
                    if(i >= 0) {
                        switch(i) {
                            case 2:
                                s = project_getint(s, &objects_eventnum1.val, objects_eventnum1.min, objects_eventnum1.max);
                            break;
                            case 3:
                                s = project_getint(s, &objects_eventnum2.val, objects_eventnum2.min, objects_eventnum2.max);
                            break;
                            case 13:
                            case 14:
                            case 15: s = project_getobj(s, &objects_eventobj[i - 13].val, 0); break;
                            case 16: s = project_getidx(s, &objects_eventtimers.val, project_eventtimers, 0); break;
                        }
                        s = project_getscript(s, &objects_cmd[i]);
                    }
                break;
            }
        }
        free(str);
        objects_attr.val = objects_val.val = objects_mval.val = objects_modper = objects_modeq = 0; objects_mattr.val = -1;
        return 1;
    }
    return 0;
}

/**
 * Load object
 */
void objects_load(void *data)
{
    char **list = (char**)objects_tbl.data;

    (void)data;
    if(objects_tbl.val < 0 || !list) return;
    if(!objects_loadobj(list[objects_tbl.val]))
        ui_status(1, lang[ERR_LOADOBJ]);
    else {
        for(objects_bypic.val = 0; objects_bypic.val < project.spritenum[objects_bypic.cat] && list &&
            strcmp(project.sprites[objects_bypic.cat][objects_bypic.val].name, list[objects_tbl.val]); objects_bypic.val++);
        if(objects_bypic.val >= project.spritenum[objects_bypic.cat]) objects_bypic.val = -1;
    }
}

/**
 * Load strings for translation
 */
void objects_loadstr()
{
    char *str, *s;
    int i;

    for(i = 0; i < project.spritenum[1]; i++) {
        if(!(project.sprites[1][i].y & 0x10000)) continue;
        str = project_loadfile(project_dirs[PROJDIRS_OBJECTS], project.sprites[1][i].name, "obj", PROJMAGIC_OBJECT,
            "objects_loadstr");
        if(str) {
            s = project_skipnl(str);
            s = project_gettrstr(s, 2, sizeof(objects_title));
            free(str);
        }
    }
}

/**
 * Preview object
 */
void objects_preview(void *data)
{
    hud_equip_t *equi = (hud_equip_t*)hud_equiptbl.data;
    chars_opt_t *opts = (chars_opt_t*)chars_opts.data;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *texture = NULL, *txts[33] = { 0 };
    SDL_Rect rect, equip = { 0 }, slot = { 0 }, port;
    int wf = SDL_GetWindowFlags(window), i, j, k = 0, x, y, p, *lyrs, top, right, bottom, w, h, scw, sch, slw, slh;
    char *list[] = { " 6", " 7", " 5", "10", " 5" }, tmp[256];
    uint32_t *pixels;
    ui_sprite_t *s;
    ui_tablehdr_t hdr[] = {
        { GAME_INV, 0, 0, 0 },
        { GAME_SKILLS, 0, 0, 0 },
        { GAME_QUESTS, 0, 0, 0 },
        { 0 }
    };

    (void)data;
    if(verbose) printf("objects_preview: started\n");
    if(!objects_tab) {
        if(objects_spr) {
            j = objects_charopt.val < 0 ? 0 : objects_charopt.val;
            k = opts && opts[j].sprs ? opts[j].numsprs / SPRPERLYR : 0;
            lyrs = (int*)main_alloc(SPRPERLYR * (k + 2) * sizeof(int));
            /* first, the background layer */
            for(i = x = 0; i < SPRPERLYR; i++)
                lyrs[x++] = objects_spr[(objects_charopt.val + 1) * 2 * SPRPERLYR + i];
            /* then the base character option layers */
            if(opts && opts[j].sprs && k) {
                for(y = 0; y < k; y++) {
                    for(i = 0; i < SPRPERLYR; i++)
                        lyrs[x++] = opts[j].sprs[y * SPRPERLYR + i];
                }
            }
            /* finally the foreground layer for this character option */
            for(i = 0; i < SPRPERLYR; i++)
                lyrs[x++] = objects_spr[(objects_charopt.val + 1) * 2 * SPRPERLYR + SPRPERLYR + i];
            chars_sprites(lyrs, x, objects_spr_hidden.cat, objects_sprs.val % SPRPERLYR);
            free(lyrs);
        }
    } else {
        SDL_GetDesktopDisplayMode(0, &dm);
        SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
        SDL_ShowCursor(SDL_DISABLE);
        SDL_RenderClear(renderer);
        SDL_RenderPresent(renderer);
        ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

        texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
        if(texture) {
            SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
            /* checker */
            for(j = k = 0; j < dm.h; j++, k += p/4)
                for(i = 0; i < dm.w ; i++) {
                    x = (i >> 5); y = (j >> 5);
                    pixels[k + i] = theme[(y & 1) ^ (x & 1) ? THEME_LIGHT : THEME_DARK];
                }
            SDL_UnlockTexture(texture);
            elements_load();
            elements_loadfonts();
            elements_winsizes(hdr, objects_skill, &w, &top, &right, &bottom, NULL);
            y = 32; right += 10; scw = elements_vscrsize();
            s = spr_getsel(&hud_invimg, 0); slw = s->w; slh = s->h;
            s = spr_getsel(&hud_invsel, 0); if(slw < s->w) { slw = s->w; } if(slh < s->h) slh = s->h;
            slw += 4; slh += 4;
            i = ((dm.w / 3) - 2 * elements_pad[4].val - scw) / slw;
            i = i * slw + 2 * elements_pad[4].val + scw;
            if(i > w) w = i;
            s = spr_getsel(&hud_invequip, 0);
            if(s->w > w) w = s->w;
            y += top; i = s->h + elements_pad[3].val + (objects_skill ? 6 * (elements_size[3].val + elements_pad[3].val) : 0);
            sch = (dm.h * 3 / 4 - y - bottom - i - 2 * elements_pad[3].val) / slh;
            h = sch * slh + i + 2 * elements_pad[3].val;
            x = dm.w - w - right;
            elements_window(texture, dm.w, dm.h, x, y, w, h, hdr, objects_skill);
            y += elements_pad[3].val;
            equip.x = dm.w - right - w / 2 - s->w / 2; equip.y = y;
            if(!objects_skill) {
                ui_blitbuf(texture, equip.x, equip.y, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
                y += s->h + elements_pad[3].val;
            } else {
                ui_fit(w, s->h, portrait_icon.w, portrait_icon.h, &port.w, &port.h);
                port.y = y; port.x = x + (w - port.w) / 2;
                y += s->h + elements_pad[3].val;
                for(j = 0; j < 5; j++, y += elements_size[3].val + elements_pad[3].val) {
                    elements_text(texture, dm.w, dm.h, x + elements_pad[4].val, y, w - 2 * elements_pad[4].val,
                        elements_size[3].val, lang[MAINMENU_ATTR1 + j], 3);
                    elements_text(texture, dm.w, dm.h, x + w + right - elements_pad[4].val - 2 * elements_size[3].val, y,
                        2 * elements_size[3].val, elements_size[3].val, list[j], 3);
                }
                sprintf(tmp, "%s: 0", lang[GAME_REMAIN]);
                elements_text(texture, dm.w, dm.h, x, y, w, elements_size[3].val, tmp, -3);
                y += elements_size[3].val + elements_pad[3].val;
            }
            elements_freefonts();
            elements_vscr(texture, dm.w - right - elements_pad[4].val, y, sch * slh, 0);
            x += elements_pad[4].val;
            slot.x = x + hud_invl.val + 2; slot.w = hud_invw.val;
            slot.y = y + hud_invt.val + 2; slot.h = hud_invh.val;
            w = (w - 2 * elements_pad[4].val) / slw;
            for(k = 0; k < sch; y += slh, k++)
                for(p = 0; p < w; p++) {
                    s = spr_getsel(!k && !p ? &hud_invsel : &hud_invimg, 0);
                    ui_blitbuf(texture, x + p * slw + (slw - s->w) / 2, y + (slh - s->h) / 2, s->w, s->h, s->data, 0, 0, s->w, s->h,
                        s->w * s->nframe * 4);
                }
        }
        spr_texture(spr_getsel(objects_invspr.val >= 0 ? &objects_invspr : &objects_bypic, 0), &txts[32]);
        if(!objects_skill && equi)
            for(i = 0; i < 32 && i < hud_equiptbl.num; i++)
                if(objects_eqslot[i])
                    spr_texture(spr_getsel(objects_eqspr[i].val >= 0 ? &objects_eqspr[i] :
                        (objects_invspr.val >= 0 ? &objects_invspr : &objects_bypic), 0), &txts[i]);

        while(1) {
            /* events */
            SDL_WaitEvent(&event);
            if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
                SDL_PushEvent(&event); goto cleanup;
            }
            switch(event.type) {
                case SDL_KEYUP: goto cleanup; break;
                case SDL_MOUSEBUTTONUP: goto cleanup; break;
            }
            /* render */
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
            SDL_RenderClear(renderer);
            if(texture) SDL_RenderCopy(renderer, texture, NULL, NULL);
            spr_render(spr_getsel(objects_invspr.val >= 0 ? &objects_invspr : &objects_bypic, 0), -1, txts[32], &slot);
            rect.w = slot.w; rect.h = slot.h;
            if(!objects_skill) {
                if(equi)
                    for(i = 0; i < 32 && i < hud_equiptbl.num; i++)
                        if(objects_eqslot[i]) {
                            rect.x = equip.x + equi[i].left;
                            rect.y = equip.y + equi[i].top;
                            spr_render(spr_getsel(objects_eqspr[i].val >= 0 ? &objects_eqspr[i] :
                                (objects_invspr.val >= 0 ? &objects_invspr : &objects_bypic), 0), -1, txts[i], &rect);
                        }
            } else {
                SDL_RenderCopy(renderer, icons, &portrait_icon, &port);
            }
            SDL_RenderPresent(renderer);
        }

cleanup:
        for(i = 0; i < 33; i++)
            if(txts[i]) SDL_DestroyTexture(txts[i]);
        if(texture) SDL_DestroyTexture(texture);
        SDL_ShowCursor(SDL_ENABLE);
        SDL_SetWindowFullscreen(window, 0);
        if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
    }
    if(verbose) printf("objects_preview: stopped\n");
}

/**
 * Get references (with sprite index)
 */
void objects_ref(tngctx_t *ctx, int idx)
{
    ui_cmd_t cmd = { 0 };
    char *str, *s, t;
    int i, j, y;

    if(!tng_sprref(ctx, 1, idx)) return;
    str = project_loadfile(project_dirs[PROJDIRS_OBJECTS], project.sprites[1][idx].name, "obj", PROJMAGIC_OBJECT, "objects_ref");
    if(str) {
        s = project_skipnl(str);
        s = project_skipnl(s);
        s = project_getint(s, &y, 0, 512);
        s = project_getint(s, &y, 0, 512);
        s = project_skiparg(s, 4);
        s = project_getobj(s, &j, -1);
        if(j != -1 && j != idx) objects_ref(ctx, j);
        for(j = 0; *s && j < y; j++)
            s = project_skipnl(s);
        while(*s) {
            s = project_skipnl(s);
            while(*s == ' ') s++;
            if(!*s || s[1] != ' ') break;
            t = *s; do { s++; } while(*s == ' ');
            switch(t) {
                case 'p':
                    s = project_skiparg(s, 2);
                    s = project_getobj(s, &j, -1);
                    if(j != -1 && j != idx) objects_ref(ctx, j);
                break;
                case 'r':
                    s = project_getidx(s, &j, project.attrs, 0);
                    attrs_ref(ctx, j);
                break;
                case 'm':
                    s = project_skiparg(s, 2);
                    s = project_getidx(s, &j, project.attrs, -1);
                    attrs_ref(ctx, j);
                break;
                case 'e':
                    s = project_getidx(s, &i, (char**)project_eventcodes, -1);
                    if(i >= 0) {
                        switch(i) {
                            case 13:
                            case 14:
                            case 15:
                                s = project_getobj(s, &j, -1);
                                if(j != -1 && j != idx) objects_ref(ctx, j);
                            break;
                        }
                        s = project_getscript(s, &cmd);
                        ui_cmd_ref(ctx, &cmd.root, 0);
                        ui_cmd_free(&cmd);
                    }
                break;
            }
        }
        free(str);
    }
}

/**
 * Save tng
 */
int objects_totng(tngctx_t *ctx)
{
    chars_attrs_t *attrs;
    chars_attrs_t *mods;
    ui_bc_t bc[18];
    uLongf cl;
    uint8_t *buf, *ptr, *comp;
    int i, j, k, l, c, x, o, err;
    char **list;

    if(!ctx || !project.sprites[1]) return 1;
    /* check if there's at least one object referenced */
    for(j = 0; j < project.spritenum[1]; j++) {
        if((project.sprites[1][j].y & 0x10000) && project.sprites[1][j].idx != -1) break;
    }
    if(j >= project.spritenum[1]) return 1;
    chars_loadopt(chars_loadcfg(NULL));
    /* add objects to output */
    tng_section(ctx, TNG_SECTION_OBJECTS);
    for(j = 0; j < project.spritenum[1]; j++) {
        if(!(project.sprites[1][j].y & 0x10000) || project.sprites[1][j].idx == -1) continue;
        if(!objects_loadobj(project.sprites[1][j].name)) {
            list = project_loadobjs(NULL);
            for(o = 0; list[o] && strcmp(project.sprites[1][j].name, list[o]); o++);
            project_freedir(&list, NULL);
            objects_tbl.val = o;
            ui_switchtab(SUBMENU_OBJECTS);
            objects_tab = 0;
            ui_status(1, lang[ERR_LOADOBJ]);
            return 0;
        }
        attrs = (chars_attrs_t*)objects_attrs.data;
        mods = (chars_attrs_t*)objects_mods.data;
        /* detect if collision mask is empty */
        if(!objects_coll.mask.data)
            objects_coll.mask.w = objects_coll.mask.h = 0;
        else {
            for(i = 0; i < objects_coll.mask.w * objects_coll.mask.h && !objects_coll.mask.data[i]; i++);
            if(i >= objects_coll.mask.w * objects_coll.mask.h)
                objects_coll.mask.w = objects_coll.mask.h = 0;
        }
        memset(&bc, 0, sizeof(bc)); err = c = 0;
        l = 39 + objects_coll.mask.w * objects_coll.mask.h + 32 * 3 + (objects_attrs.num + objects_mods.num) * 8;
        if(objects_spr) {
            /* 0xffffff + all character background + foreground sprites */
            for(i = 0; i < 2 * SPRPERLYR && objects_spr[i] == -1; i++);
            if(i < 2 * SPRPERLYR) { c++; l += (2 * SPRPERLYR + 1) * 3; }
            /* character option string + background + foreground sprites */
            for(x = 0; x < chars_opts.num; x++) {
                for(i = 0; i < 2 * SPRPERLYR && objects_spr[(x + 1) * 2 * SPRPERLYR + i] == -1; i++);
                if(i < 2 * SPRPERLYR) { c++; l += (2 * SPRPERLYR + 1) * 3; }
            }
        }
        for(i = k = 0; i < 17; i++)
            if(objects_cmd[i].root.len) {
                ui_cmd_tobc(ctx, &bc[i], &objects_cmd[i].root, 0, &err);
                if(err) {
                    for(k = 0; k <= i; k++)
                        if(bc[k].data) free(bc[k].data);
                    objects_tbl.val = j;
                    ui_switchtab(SUBMENU_OBJECTS);
                    objects_event.val = i;
                    objects_tab = 0;
                    ui_status(1, lang[ERR_SCRIPT]);
                    return 0;
                }
                if(bc[i].data && bc[i].len) {
                    l += 7 + bc[i].len;
                    k++;
                }
            }
        buf = ptr = (uint8_t*)main_alloc(l);
        ptr = tng_asset_text(ptr, ctx, objects_title);
        memcpy(ptr, &objects_coll.mask.w, 2); ptr += 2;
        memcpy(ptr, &objects_coll.mask.h, 2); ptr += 2;
        *ptr++ = objects_actiongrp.val;
        ptr = tng_asset_sprite(ptr, ctx, objects_invspr.cat, objects_invspr.val);
        ptr = tng_asset_idx(ptr, ctx, project.sounds, TNG_IDX_SND, objects_invsnd.val);
        memcpy(ptr, &objects_pricenum.val, 3); ptr += 3;
        if(objects_pricenum.val && objects_priceunit.val != -1) {
            tng_sprref(ctx, objects_priceunit.cat, objects_priceunit.val);
            memcpy(ptr, &objects_priceunit.val, 2); ptr += 2;
        } else { *ptr++ = 0xff; *ptr++ = 0xff; }
        *ptr++ = objects_cat.val;
        ptr = tng_asset_sprite(ptr, ctx, objects_projectile.cat, objects_projectile.val);
        memcpy(ptr, &objects_projtime.val, 3); ptr += 3;
        ptr = tng_asset_sprite(ptr, ctx, objects_projunit.cat, objects_projunit.val);
        *ptr++ = objects_layer.val;
        if(objects_skill)
            x = 0xffffffff;
        else
            for(i = x = 0; i < 32; i++)
                if(objects_eqslot[i]) x |= (1 << i);
        memcpy(ptr, &x, 4); ptr += 4;
        *ptr++ = 0; /* sound effects, on walk, on climb etc. */
        *ptr++ = objects_attrs.num;
        *ptr++ = objects_mods.num;
        *ptr++ = c;
        *ptr++ = k;
        i = objects_coll.mask.w * objects_coll.mask.h;
        memcpy(ptr, objects_coll.mask.data, i); ptr += i;
        for(i = 0; i < 32; i++)
            ptr = tng_asset_sprite(ptr, ctx, objects_eqspr[i].cat, objects_eqslot[i] ? objects_eqspr[i].val : -1);
        for(i = 0; attrs && i < objects_attrs.num; i++) {
            *ptr++ = attrs[i].rel < 0 ? 0xff : attrs[i].rel;
            ptr = tng_asset_idx(ptr, ctx, project.attrs, TNG_IDX_ATR, attrs[i].attr);
            memcpy(ptr, &attrs[i].val, 4); ptr += 4;
        }
        for(i = 0; mods && i < objects_mods.num; i++) {
            *ptr++ = mods[i].rel;
            ptr = tng_asset_idx(ptr, ctx, project.attrs, TNG_IDX_ATR, mods[i].attr);
            memcpy(ptr, &mods[i].val, 4); ptr += 4;
        }
        if(objects_spr) {
            /* default sprites */
            for(i = 0; i < 2 * SPRPERLYR && objects_spr[i] == -1; i++);
            if(i < 2 * SPRPERLYR) {
                x = 0xffffff;
                memcpy(ptr, &x, 3); ptr += 3;
                for(i = 0; i < 2 * SPRPERLYR; i++)
                    ptr = tng_asset_sprite(ptr, ctx, (i % SPRPERLYR) == SPRPERLYR - 1 ? objects_spr_hidden2.cat :
                        objects_spr_hidden.cat, objects_spr[i]);
            }
            /* character option specific sprites */
            for(x = 0; x < chars_opts.num; x++) {
                for(i = 0; i < 2 * SPRPERLYR && objects_spr[(x + 1) * 2 * SPRPERLYR + i] == -1; i++);
                if(i == 2 * SPRPERLYR) continue;
                ptr = tng_asset_text(ptr, ctx, objects_charopt.opts[x]);
                for(i = 0; i < 2 * SPRPERLYR; i++)
                    ptr = tng_asset_sprite(ptr, ctx, (i % SPRPERLYR) == SPRPERLYR - 1 ? objects_spr_hidden2.cat :
                        objects_spr_hidden.cat, objects_spr[(x + 1) * 2 * SPRPERLYR + i]);
            }
        }
        for(i = 0; i < 17; i++) {
            if(!objects_cmd[i].root.len || !bc[i].data || !bc[i].len) continue;
            *ptr++ = i;
            switch(i) {
                case 2: memcpy(ptr, &objects_eventnum1.val, 3); ptr += 3; break;
                case 3: memcpy(ptr, &objects_eventnum2.val, 3); ptr += 3; break;
                case 13:
                case 14:
                case 15: ptr = tng_asset_sprite(ptr, ctx, 2, objects_eventobj[i - 13].val); break;
                case 16: memcpy(ptr, &objects_eventtimers.val, 3); ptr += 3; break;
                default: *ptr++ = 0; *ptr++ = 0; *ptr++ = 0; break;
            }
            memcpy(ptr, &bc[i].len, 3); ptr += 3;
            memcpy(ptr, bc[i].data, bc[i].len); ptr += bc[i].len;
            free(bc[i].data);
        }
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        if(cl) {
            tng_desc_idx(ctx, project.sprites[1][j].idx, cl);
            tng_asset_int(ctx, comp, cl);
        } else
            free(comp);
        free(buf);
        /* free resources */
        objects_new(NULL);
    }
    return 1;
}

/**
 * Read from tng
 */
int objects_fromtng(tngctx_t *ctx)
{
    FILE *f;
    tng_asset_desc_t *asset;
    uint32_t emsk;
    uint8_t *buf, *ptr, *end, *prj, c, cat;
    int i, j, l, len, na, nm, ns, ne, r, w, h;
    char name[8];

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_OBJECTS; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;
    for(j = 0; j < len; j++, asset++) {
        ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
        buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &l);
        if(buf) {
            if(l < 34) { free(buf); continue; }
            end = buf + l;
            memset(name, 0, sizeof(name));
            sprintf(name, "%04X", asset->name);
            f = project_savefile(0, project_dirs[PROJDIRS_OBJECTS], name, "obj", PROJMAGIC_OBJECT, "objects_fromtng");
            if(f) {
                ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                w = *((uint16_t*)ptr); ptr += 2;
                h = *((uint16_t*)ptr); ptr += 2;
                /* parameters */
                fprintf(f, "%u %u %u ", w, h, ptr[0] == 0xff ? 0 : ptr[0] + 1); ptr++;
                ptr = tng_wrsprite(ctx, ptr, f); fprintf(f, " ");
                ptr = tng_wridx(ctx, ptr, f); fprintf(f, " ");
                ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " ");
                if(ptr[0] == 0xff && ptr[1] == 0xff)
                    fprintf(f, "- ");
                else
                    fprintf(f, "%02X%02X ", ptr[1], ptr[0]);
                ptr += 2; cat = *ptr++;
                prj = ptr; ptr += 9;
                c = *ptr++;
                memcpy(&emsk, ptr, 4); ptr += 4;
                fprintf(f, "%u %u %u\r\n", emsk == 0xffffffff ? 1 : 0, c, cat);
                r = *ptr++;
                na = *ptr++;
                nm = *ptr++;
                ns = *ptr++;
                ne = *ptr++;
                /* collision mask */
                for(l = 0; l < h && ptr < end; l++) {
                    for(i = 0; i < w && ptr < end; i++)
                        fprintf(f, "%s%u", i ? " " : "", *ptr++);
                    fprintf(f, "\r\n");
                }
                /* equipment slots */
                if(emsk != 0xffffffff) {
                    for(i = 0, l = 1; i < 32 && ptr < end; i++, l <<= 1)
                        if(emsk & l) {
                            if(!i)
                                fprintf(f, "s primary ");
                            else
                                fprintf(f, "s equipslot%02u ", i);
                            ptr = tng_wrsprite(ctx, ptr, f);
                            fprintf(f, "\r\n");
                        } else
                            ptr += 3;
                } else ptr += 32 * 3;
                /* sound effects */
                ptr += r * 3;
                /* required attributes */
                for(i = 0; i < na && ptr < end; i++) {
                    r = *ptr++;
                    fprintf(f, "r ");
                    ptr = tng_wridx(ctx, ptr, f);
                    fprintf(f, "%s%d\r\n", r != 0xff ? project_rels[r] : "=", *((int32_t*)ptr));
                    ptr += 4;
                }
                /* attribute modifiers */
                for(i = 0; i < nm && ptr < end; i++) {
                    r = *ptr++;
                    fprintf(f, "m %d %d ", r & 2 ? 1 : 0, r & 1);
                    ptr = tng_wridx(ctx, ptr, f);
                    fprintf(f, " %d\r\n", *((int32_t*)ptr));
                    ptr += 4;
                }
                /* projectile */
                if(*((uint16_t*)&prj[1]) != 0xffff || *((uint16_t*)&prj[7]) != 0xffff) {
                    fprintf(f, "p ");
                    if(*((uint16_t*)&prj[1]) != 0xffff) {
                        prj = tng_wrsprite(ctx, prj, f);
                        fprintf(f, " %u ", *((uint32_t*)prj) & 0xffffff);
                    } else
                        fprintf(f, "- 0 ");
                    prj = tng_wrsprite(ctx, prj + 3, f);
                    fprintf(f, "\r\n");
                }
                /* default and character option specific sprites */
                for(i = 0; i < ns && ptr < end; i++) {
                    if(memcmp(ptr, "\xff\xff\xff", 3)) {
                        fprintf(f, "o ");
                        ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                        r = 1;
                    } else {
                        ptr += 3;
                        r = 0;
                    }
                    fprintf(f, "%sb", r ? " " : "");
                    for(l = 0; l < SPRPERLYR; l++) {
                        fprintf(f, " ");
                        ptr = tng_wrsprite(ctx, ptr, f);
                    }
                    fprintf(f, "\r\n%sf", r ? " " : "");
                    for(l = 0; l < SPRPERLYR; l++) {
                        fprintf(f, " ");
                        ptr = tng_wrsprite(ctx, ptr, f);
                    }
                    fprintf(f, "\r\n");
                }
                /* event handlers */
                for(i = 0; i < ne && ptr < end; i++) {
                    r = *ptr++;
                    fprintf(f, "e %s ", project_eventcodes[r]);
                    switch(r) {
                        case 2: ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " "); break;
                        case 3: ptr = tng_wrnum(ctx, ptr, 3, f); fprintf(f, " "); break;
                        case 13:
                        case 14:
                        case 15: ptr = tng_wrsprite(ctx, ptr, f); fprintf(f, " "); break;
                        case 16: r = 0; memcpy(&r, ptr, 3); ptr += 3; fprintf(f, "%s ", project_eventtimers[r]); break;
                        default: ptr += 3; break;
                    }
                    r = 0; memcpy(&r, ptr, 3); ptr += 3;
                    ptr = tng_wrscript(ctx, ptr, r, f);
                }
                fclose(f);
            }
            free(buf);
        }
    }
    return 1;
}
