/*
 * tnge/maps.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Maps window
 *
 */

#include "main.h"
#include "zstd/zstd.h"
#include <math.h>

#define PI 3.14159265358979323846
#define MAPS_NUMLYR (MAPS_LYR_PATHS - MAPS_LYR_GRD1)
#define MAPS_STRIPE (3 * project.mapsize)
#define MAPS_IDX(x, y) (y * MAPS_STRIPE * project.mapsize + x * project.mapsize)

char *stbi_zlib_decode_malloc_guesssize_headerflag(const char *buffer, int len, int initial_size, int *outlen, int parse_header);
extern ui_table_t translate_tbl;
extern SDL_Texture *popup;
extern ui_font_t elements_fonts[6];
extern ui_num_t elements_size[];

typedef struct {
    int bg;                 /* set if this parallax is behind layers */
    int sx, dx;             /* speed x (how much to move per time interval) and delta x (how much to move per player movement) */
    int sy, dy;             /* speed y and delta y */
    int tm;                 /* time interval */
    int spr;                /* sprite */
    int x, y;               /* not stored, only used by maps_rendermap */
    uint32_t last;          /* not stored, last update time */
} maps_par_t;

typedef struct {
    char type;                      /* command type, see CMD_* */
    int par1, par2;                 /* parameters */
} maps_npccmd_t;

typedef struct {
    int x, y;                       /* npc coordinate */
    int dx, dy;                     /* delta movement */
    int nx, ny;                     /* new coordinate to move to */
    int t, c;                       /* total time for movement, current movement */
    uint32_t nm, la;                /* next command timestamp, last animation timestamp */
    int d, anim, f;                 /* direction, animation to play, current frame */
    int numcmd, idxcmd;             /* number of commands and current command index */
    maps_npccmd_t *cmd;             /* commands to execute */
    int speed;                      /* speed */
    int light;                      /* light radious */
    ui_sprite_t *spr;               /* sprites for all actions in all directions */
} maps_npc_t;
extern ui_num_t npcs_speed;

typedef struct {
    int lyr;                        /* the layer on which the modification was made */
    int x, y, w, h;                 /* dimensions */
    void *old;                      /* the old buffer */
    void *buf;                      /* the new buffer */
} maps_hist_t;

typedef struct {
    int id, cat;                    /* index and category */
    char *name;                     /* name */
} maps_tile_t;

void maps_hist_add(int lyr, int x0, int y0, int x1, int y1, void *old, void *buf);
void maps_hist_free();
void maps_status();
void maps_chglyr(void *data);
void maps_chglink(void *data);
void maps_chgpaint(void *data);
void maps_erasepar(void *data);
void maps_addpar(void *data);
void maps_clkpar(void *data);
void maps_search(void *data);
void maps_erase(void *data);
void maps_load(void *data);
void maps_new(void *data);
void maps_clrform(void *data);
void maps_save(void *data);
void maps_delete(void *data);
void maps_preview(void *data);
void maps_copy(void *data);
void maps_cut(void *data);
void maps_paste(void *data);
void maps_hflip(void *data);
void maps_vflip(void *data);
void maps_flipdir(void *data);
void maps_moveleft(void *data);
void maps_moveup(void *data);
void maps_movedown(void *data);
void maps_moveright(void *data);
void maps_clrmask(void *data);
void maps_drawtbl(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void maps_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void maps_rendercell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void maps_select_check();

const char *maps_tmxorient[] = { "orthogonal", "isometric", "hexagonal", "hexagonal", NULL };
const char *maps_tmxorientextra[] = { "", "", "staggeraxis=\"y\" ", "staggeraxis=\"x\" ", NULL };
char *maps_tmxdirs[7];
char *maps_partypes[] = { "\xe2\xac\x93", "\xe2\xac\x92", NULL };

char maps_name[PROJ_NAMEMAX], maps_srch[PROJ_NAMEMAX] = { 0 }, maps_title[PROJ_TITLEMAX], *maps_lyrnames[MAPS_NUMLYR + 2] = { 0 };
char *maps_neightstrs[7];
int maps_meta = 0, maps_tab, maps_layers, maps_tool = 1, maps_pathlen[99] = { 0 }, maps_tw, maps_th, maps_sellen = 0;
int maps_selnum = 0, maps_cliplyr = -1, maps_neightbors[6], maps_daylast, maps_ox = -1, maps_oy = -1, maps_oanim = -1, maps_of;
int maps_hist_len = 0, maps_hist_idx = 0, maps_deltay = 0, maps_clk = 0, maps_lx, maps_ly, maps_clipmrk = 0;
int maps_numauto = 0, *maps_auto = NULL, **maps_autoidx = NULL;
SDL_Point *maps_paths[99] = { 0 }, maps_selline, maps_selbox, *maps_selply = NULL;
SDL_Rect maps_clipcrd;
SDL_Texture *maps_daytxt = NULL, *maps_empty = NULL;
uint32_t maps_base = 0, maps_ol;
uint16_t *maps_map = NULL, *maps_clipboard = NULL, *maps_objlights = NULL, *maps_histchk = NULL;
uint8_t *maps_selmask = NULL, *maps_coll = NULL;
maps_npc_t *maps_npcs = NULL;
maps_hist_t *maps_hist = NULL;
ui_sprsel_t maps_autospr = { -1, 1, 157, 1 };
ui_input_t maps_nameinp = { INP_ID, sizeof(maps_name), maps_name };
ui_input_t maps_srchinp = { INP_ID, sizeof(maps_srch), maps_srch };
ui_input_t maps_titinp = { INP_NAME, sizeof(maps_title), maps_title };
ui_select_t maps_backmus = { -1, LANG_NONE, NULL };
ui_mask_t *maps_masks = NULL;
ui_scrarea_t maps_scr = { 0 };
ui_icontgl_t maps_metatgl = { ICON_INFO, 1, &maps_meta };
ui_num_t maps_daylen = { 20, 0, 10080, 1 };
ui_sprsel_t maps_dayspr = { -1, 4, 25, 1 };
ui_select_t maps_partype = { 0, 0, maps_partypes };
ui_num_t maps_parsx = { 0, -100, 100, 1 };
ui_num_t maps_pardx = { 0, 0, 100, 1 };
ui_num_t maps_parsy = { 0, -100, 100, 1 };
ui_num_t maps_pardy = { 0, 0, 100, 1 };
ui_num_t maps_partm = { 1, 1, 6000, 1 };
ui_sprsel_t maps_parspr = { -1, 4, 25, 1 };
ui_iconsel_t maps_lyrsel = { ICON_LAYERS, &maps_layers, { 0, 0, maps_lyrnames } };
ui_sprsel_t maps_paint[4] = {
    { -1, 1, 157, 1 },
    { -1, 1, 158, 2 },
    { -1, PROJ_NUMSPRCATS - 2, 185, 1 },
    { -1, PROJ_NUMSPRCATS - 1, 185, 1 }
};
int maps_p[MAPS_NUMLYR - 1] = { 0, 0, 0, 0, 1, 2, 3, 0, 0, 0, 0 };
ui_num_t maps_path = { 1, 1, 99, 1 };
ui_icongrpelem_t maps_toolelems[] = {
    { ICON_PIPETTE, MAPS_PIPETTE },
    { ICON_PAINT, MAPS_PAINT },
    { ICON_FILL, MAPS_FILL },
    { ICON_SELBOX, MAPS_SELBOX },
    { ICON_SELPLY, MAPS_SELPLY },
    { ICON_SELFZY, MAPS_SELFZY }
};
ui_icongrp_t maps_grp1 = { &maps_tool, 0, 3, &maps_toolelems[0] };
ui_icongrp_t maps_grp2 = { &maps_tool, 3, 3, &maps_toolelems[3] };
ui_map_t maps_dir[6] = { 0 };

ui_tablehdr_t maps_hdr[] = {
    { SUBMENU_MAPS, 0, 0, 0 },
    { 0 }
};
ui_table_t maps_tbl = { maps_hdr, MAPS_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(int), maps_drawtbl, NULL,
    NULL, NULL };

ui_tablehdr_t maps_parhdr[] = {
    { -1, 24, 0, 0 },
    { MAINMENU_DX, 96, 0, 0 },
    { MAINMENU_DY, 96, 0, 0 },
    { MAINMENU_INTERVAL, 96, 0, 0 },
    { MAINMENU_PARALLAX, 0, 0, 0 },
    { 0 }
};
ui_table_t maps_partbl = { maps_parhdr, MAPS_NOPAR, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(maps_par_t), maps_drawcell,
    maps_rendercell, NULL, NULL };

/**
 * The form
 */
ui_form_t maps_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, MAPS_DELETE, (void*)ICON_REMOVE, maps_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, maps_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, MAPS_PREVIEW, (void*)ICON_PVIEW, maps_preview },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, maps_clrform },
    { FORM_INPUT, 0, 30, 200, 20, 0, MAPS_NAME, &maps_nameinp, NULL },
    { FORM_TABLE, 10, 54, 200, 0, 0, 0, &maps_tbl, maps_load },
    { FORM_ICON, 26, 32, 16, 16, 0, 0, (void*)ICON_SRCH, NULL },
    { FORM_INPUT, 46, 30, 164, 20, 0, MAPS_SEARCH, &maps_srchinp, maps_search },
    /* 8 */
    { FORM_ICONTGL, 0, 30, 20, 20, 0, MAPS_INFO, &maps_metatgl, NULL },
    /* 9 */
    { FORM_TEXT, 0, 55, 20, 18, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 54, 0, 20, 0, MAPS_STTITLE, &maps_titinp, NULL },
    { FORM_TEXT, 0, 79, 20, 18, 0, 0, NULL, NULL },
    { FORM_NUM, 0, 78, 80, 20, 0, MAPS_DAYLEN, &maps_daylen, NULL },
    { FORM_SPRITE, 0, 78, 0, 20, 0, MAPS_DAYSPR, &maps_dayspr, NULL },
    { FORM_MUSSEL, 0, 102, 20, 20, 0, MAINMENU_BACKMUS, &maps_backmus, NULL },
    { FORM_TABLE, 0, 128, 0, 0, 0, 0, &maps_partbl, maps_clkpar },
    /* 16 */
    { FORM_ICONBTN, 0, 0, 20, 20, 0, MAPS_PARERASE, (void*)ICON_REMOVE, maps_erasepar },
    { FORM_SELECT, 0, 0, 40, 20, 0, -MAPS_PARFG, &maps_partype, NULL }, /* also MAPS_PARBG */
    { FORM_NUM, 0, 0, 56, 20, 0, MAPS_PARSX, &maps_parsx, NULL },
    { FORM_NUM, 0, 0, 56, 20, 0, MAPS_PARDX, &maps_pardx, NULL },
    { FORM_NUM, 0, 0, 56, 20, 0, MAPS_PARSY, &maps_parsy, NULL },
    { FORM_NUM, 0, 0, 56, 20, 0, MAPS_PARDY, &maps_pardy, NULL },
    { FORM_TIME, 0, 0, 56, 20, 0, MAPS_PARTM, &maps_partm, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, MAPS_PARSPR, &maps_parspr, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, MAPS_PARADD, (void*)ICON_ADD, maps_addpar },
    /* 25 */
    { FORM_SCRAREA, 0, 54, 0, 0, 0, SCRAREA_BOTH, &maps_scr, NULL },
    { FORM_ZOOM, 0, 0, 68, 20, 0, 0, &maps_scr, maps_clrmask },
    { FORM_ICONSEL, 0, 30, 20, 20, 0, MAPS_STLAYERS, &maps_lyrsel, maps_chglyr },
    /* 28 */
    { FORM_SPRITE, 0, 30, 40, 20, 0, MAPS_ITEM, &maps_paint[0], maps_chgpaint },
    { FORM_SPRITE, 0, 30, 40, 20, 0, MAPS_ITEM, &maps_paint[1], maps_chgpaint },
    { FORM_SPRITE, 0, 30, 40, 20, 0, MAPS_ITEM, &maps_paint[2], maps_chgpaint },
    { FORM_SPRITE, 0, 30, 40, 20, 0, MAPS_ITEM, &maps_paint[3], maps_chgpaint },
    { FORM_SPRITE, 0, 30, 40, 20, 2, MAPS_ITEM, &maps_paint[0], NULL },
    { FORM_SPRITE, 0, 30, 40, 20, 2, MAPS_ITEM, &maps_paint[0], NULL },
    { FORM_SPRITE, 0, 30, 40, 20, 2, MAPS_ITEM, &maps_paint[0], NULL },
    { FORM_NUM, 0, 30, 40, 20, 0, MAPS_ITEM, &maps_path, NULL },
    { FORM_ICONBTN, 0, 30, 20, 20, 0, MAPS_ERASE, (void*)ICON_ERASE, maps_erase },
    /* 37 */
    { FORM_ICONGRP, 0, 30, 3*24-4, 20, 0, 0, &maps_grp1, NULL },
    { FORM_ICONGRP, 0, 30, 3*24-4, 20, 0, 0, &maps_grp2, NULL },
    /* 39 */
    { FORM_ICONBTN, 0, 30, 20, 20, 0, MAPS_COPY, (void*)ICON_COPY, maps_copy },
    { FORM_ICONBTN, 0, 30, 20, 20, 0, MAPS_CUT, (void*)ICON_CUT, maps_cut },
    { FORM_ICONBTN, 0, 30, 20, 20, 0, MAPS_PASTE, (void*)ICON_PASTE, maps_paste },
    /* 42 */
    { FORM_CHARBTN, 0, 30, 20, 20, 0, MAPS_HFLIP, (void*)11, maps_hflip },
    { FORM_CHARBTN, 0, 30, 20, 20, 0, MAPS_VFLIP, (void*)12, maps_vflip },
    { FORM_CHARBTN, 0, 30, 20, 20, 0, MAPS_FLIPDIR, (void*)12, maps_flipdir },
    /* 45 */
    { FORM_MAPOPT, 0, 0, 20, 20, 0, 0, &maps_dir[0], maps_chglink },
    { FORM_MAPOPT, 0, 0, 20, 20, 0, 0, &maps_dir[1], maps_chglink },
    { FORM_MAPOPT, 0, 0, 20, 20, 0, 0, &maps_dir[2], maps_chglink },
    { FORM_MAPOPT, 0, 0, 20, 20, 0, 0, &maps_dir[3], maps_chglink },
    { FORM_MAPOPT, 0, 0, 20, 20, 0, 0, &maps_dir[4], maps_chglink },
    { FORM_MAPOPT, 0, 0, 20, 20, 0, 0, &maps_dir[5], maps_chglink },
    /* 51 */
    { FORM_CHARBTN, 0, 30, 20, 20, 0, MAPS_MOVE, (void*)11013, maps_moveleft },
    { FORM_CHARBTN, 0, 30, 20, 20, 0, MAPS_MOVE, (void*)11014, maps_moveup },
    { FORM_CHARBTN, 0, 30, 20, 20, 0, MAPS_MOVE, (void*)11015, maps_movedown },
    { FORM_CHARBTN, 0, 30, 20, 20, 0, MAPS_MOVE, (void*)10145, maps_moveright },
    { FORM_LAST }
};

/**
 * Convert sprite size to tile size
 */
void maps_sprite2tilewh(int sw, int sh, int *w, int *h)
{
    switch(project.type) {
        case TNG_TYPE_ORTHO:
            *w = (sw + project.tilew - 1) / project.tilew;
            *h = (sh + project.tileh - 1) / project.tileh;
        break;
        case TNG_TYPE_ISO:
            *w = (sw + project.tilew - 1) / project.tilew;
            *h = (sh + project.tileh - 1) / project.tileh * 2 - 1;
        break;
        case TNG_TYPE_HEXV:
            /* TODO: hexagonal grid */
        break;
        case TNG_TYPE_HEXH:
        break;
    }
    if(*h < 1) *h = 1;
    if(!(*w & 1)) (*w) += 1;
}

/**
 * Map to screen coordinate
 */
void maps_map2scr(ui_scrarea_t *s, int sx, int sy, int *x, int *y)
{
    if(!s || !x || !y) return;
    switch(project.type) {
        case TNG_TYPE_ORTHO:
            *x = sx * maps_tw + maps_tw / 2 + 24;
            *y = sy * maps_th;
        break;
        case TNG_TYPE_ISO:
            *x = (sx - sy) * maps_tw / 2 + s->w / 2;
            *y = (sx + sy) * maps_th / 2;
        break;
        case TNG_TYPE_HEXV:
        break;
        case TNG_TYPE_HEXH:
        break;
    }
    *x = *x - s->x + s->sx;
    *y = *y - s->y + s->sy + 24 + 3 * maps_th;
}

/**
 * Screen to map coordinate
 */
void maps_scr2map(ui_scrarea_t *s, int sx, int sy, int *x, int *y)
{
    if(!s || !x || !y) return;
    if(maps_tw < 1) maps_tw = 1;
    if(maps_th < 1) maps_th = 1;
    if(sx < s->sx || sy < s->sy || sx >= s->sx + s->sw || sy >= s->sy + s->sh) {
        *x = *y = -1;
        return;
    }
    sx = sx + s->x - s->sx;
    sy = sy + s->y - s->sy - 24 - 2 * maps_th;
    if(sx < 0 || sy < 0) {
        *x = *y = -1;
        return;
    }
    switch(project.type) {
        case TNG_TYPE_ORTHO:
            *x = (sx - 24) / maps_tw;
            *y = sy / maps_th;
        break;
        case TNG_TYPE_ISO:
            sx -= s->w / 2;
            /* this is integer arithmetic madness: 31/32 = 0, -31/32 = 0 loses sign! */
            *x = (int)((float)sx / (float)maps_tw + (float)sy / (float)maps_th);
            *y = (int)((float)sy / (float)maps_th - (float)sx / (float)maps_tw);
        break;
        case TNG_TYPE_HEXV:
        break;
        case TNG_TYPE_HEXH:
        break;
    }
    if(*x < 0 || *x >= project.mapsize) *x = -1;
    if(*y < 0 || *y >= project.mapsize) *y = -1;
}

/**
 * Get map size
 */
void maps_size(int z, int *w, int *h)
{
    maps_tw = project.tilew; maps_th = project.tileh;
    if(z < 0) { maps_tw /= (1 << -z); maps_th /= (1 << -z); }
    if(z > 0) { maps_tw *= (1 << z); maps_th *= (1 << z); }
    if(maps_tw < 1) maps_tw = 1;
    if(maps_th < 1) maps_th = 1;
    switch(project.type) {
        case TNG_TYPE_ORTHO:
        case TNG_TYPE_ISO:
            *w = maps_tw * project.mapsize + 48;
            *h = maps_th * (project.mapsize + 2) + 48;
        break;
        case TNG_TYPE_HEXV:
        break;
        case TNG_TYPE_HEXH:
        break;
    }
}

/**
 * Place npcs on map with screen coordinates
 */
void maps_npc2scr(int idx, int i, int j, int cx, int cy, int w, int h)
{
    if(!maps_npcs || idx < 0 || idx >= project.spritenum[PROJ_NUMSPRCATS - 2]) return;
    switch(project.type) {
        case TNG_TYPE_ORTHO:
            maps_npcs[idx].x = maps_npcs[idx].nx = (i - cx) * project.tilew + w / 2;
            maps_npcs[idx].y = maps_npcs[idx].ny = (j - cy + 1) * project.tileh + h / 2 + maps_deltay;
        break;
        case TNG_TYPE_ISO:
            maps_npcs[idx].x = maps_npcs[idx].nx = ((i - cx) - (j - cy)) * (project.tilew / 2) + w / 2;
            maps_npcs[idx].y = maps_npcs[idx].ny = ((i - cx) + (j - cy)) * (project.tileh / 2) + project.tileh + h / 2 +
                maps_deltay;
        break;
        case TNG_TYPE_HEXV:
        break;
        case TNG_TYPE_HEXH:
        break;
    }
}

/**
 * Add a command to npc
 */
void maps_npccmd(int idx, int type, int par1, int par2)
{
    int i;

    if(!maps_npcs || idx < 0 || idx >= project.spritenum[PROJ_NUMSPRCATS - 2]) return;
    i = maps_npcs[idx].numcmd++;
    maps_npcs[idx].cmd = (maps_npccmd_t*)realloc(maps_npcs[idx].cmd, maps_npcs[idx].numcmd * sizeof(maps_npccmd_t));
    if(maps_npcs[idx].cmd) {
        maps_npcs[idx].cmd[i].type = type;
        maps_npcs[idx].cmd[i].par1 = par1;
        maps_npcs[idx].cmd[i].par2 = par2;
    } else maps_npcs[idx].numcmd = 0;
}

/**
 * Check it npc has commands
 */
int maps_npcidle(int idx)
{
    if(!maps_npcs || idx < 0 || idx >= project.spritenum[PROJ_NUMSPRCATS - 2]) return 1;
    return !(maps_npcs[idx].x > 0 && maps_npcs[idx].y > 0 &&
        ((maps_npcs[idx].nm > 0 && maps_npcs[idx].nm < SDL_GetTicks()) || maps_npcs[idx].idxcmd < maps_npcs[idx].numcmd));
}

/**
 * Place an object to map
 */
void maps_objplace(uint16_t *map, int idx, int x, int y)
{
    if(!map || x < 0 || x >= project.mapsize || y < 0 || y >= project.mapsize) return;
    map[MAPS_STRIPE * MAPS_STRIPE + (y + project.mapsize) * MAPS_STRIPE + project.mapsize + x] = (idx < 0 ? 0xffff : idx);
    maps_ox = x; maps_oy = y; maps_oanim = -1;
}

/**
 * Play object animation
 */
void maps_objanim(int idx)
{
    maps_oanim = idx;
    maps_of = 0;
    maps_ol = ui_anim_cnt;
}

/**
 * Set collision mask
 * This has to be done all at once, because objects may overlap, and when removed,
 * masks must be set from the other, non-changed objects
 */
void maps_setcoll()
{
    uint16_t *map = maps_map + (MAPS_LYR_OBJECTS - MAPS_LYR_GRD1) * project.mapsize * project.mapsize;
    int i, j, k, x, y, X, Y;

    if(maps_coll) free(maps_coll);
    maps_coll = (uint8_t*)main_alloc(project.mapsize * project.mapsize);
    switch(project.type) {
        case TNG_TYPE_ORTHO:
            for(y = 0; y < project.mapsize; y++)
                for(x = 0; x < project.mapsize; x++, map++)
                    if(*map != 0xffff) {
                        if(maps_masks[*map].data) {
                            for(j = k = 0, Y = y - maps_masks[*map].h + 1; j < maps_masks[*map].h; j++, Y++)
                                for(i = 0, X = x - maps_masks[*map].w / 2; i < maps_masks[*map].w; i++, X++, k++)
                                    if(X >= 0 && Y >= 0 && X < project.mapsize && Y < project.mapsize)
                                        maps_coll[Y * project.mapsize + X] = maps_masks[*map].data[k];
                        } else
                            maps_coll[y * project.mapsize + x] = 1;
                    }
        break;
        case TNG_TYPE_ISO:
            for(y = 0; y < project.mapsize; y++)
                for(x = 0; x < project.mapsize; x++, map++)
                    if(*map != 0xffff) {
                        if(maps_masks[*map].data) {
                            for(j = k = 0; j < maps_masks[*map].h; j++)
                                for(i = 0; i < maps_masks[*map].w; i++, k++) {
                                    Y = y - (maps_masks[*map].h) / 2 + (maps_masks[*map].w) / 2 - i + j / 2;
                                    X = x - (maps_masks[*map].h) / 2 - (maps_masks[*map].w) / 2 + i + (j & 1) + j / 2;
                                    if(X >= 0 && Y >= 0 && X < project.mapsize && Y < project.mapsize)
                                        maps_coll[Y * project.mapsize + X] = maps_masks[*map].data[k];
                                }
                        } else
                            maps_coll[y * project.mapsize + x] = 1;
                    }
        break;
        case TNG_TYPE_HEXV:
        break;
        case TNG_TYPE_HEXH:
        break;
    }
}

/**
 * Render a map
 */
void maps_rendertmx(ui_scrarea_t *scr, int mask, int cx, int cy)
{
    int i, j, k, l, m = 1, r, g, b, p, tw = maps_tw / 2, th = maps_th / 2, cat;
    void *data;
    double d;
    char tmp[8];
    SDL_Rect src, rect, rect2;
    SDL_Point src2 = { 4, 0 };
    SDL_Texture *texture;
    uint16_t *map = maps_map;
    ui_sprite_t *s;

    if(!scr || scr->w < 1 || scr->h < 1) return;
    if(scr->x >= scr->w - scr->sw) scr->x = scr->w - scr->sw - 1;
    if(scr->y >= scr->h - scr->sh) scr->y = scr->h - scr->sh - 1;
    if(scr->x < 0) scr->x = 0;
    if(scr->y < 0) scr->y = 0;
    if(scr->w < scr->sw) scr->x = (scr->w - scr->sw) / 2;
    if(scr->h < scr->sh) scr->y = (scr->h - scr->sh) / 2;
    if(maps_tool < MAPS_SELBOX - MAPS_PIPETTE || maps_tool > MAPS_SELFZY - MAPS_PIPETTE || (!ui_isshift && !ui_isctrl && !ui_ismeta)) {
        r = theme[THEME_SELBX] & 0xff;
        g = (theme[THEME_SELBX] >> 8) & 0xff;
        b = (theme[THEME_SELBX] >> 16) & 0xff;
    } else {
        k = (SDL_GetTicks() >> 1) & 0x1ff;
        if(k >= 256) k = 512 - k;
        r = ((theme[THEME_SELBX] & 0xff) * k + (theme[THEME_GRID1] & 0xff) * (256 - k)) >> 8;
        g = (((theme[THEME_SELBX] >> 8) & 0xff) * k + ((theme[THEME_GRID1] >> 8) & 0xff) * (256 - k)) >> 8;
        b = (((theme[THEME_SELBX] >> 16) & 0xff) * k + ((theme[THEME_GRID1] >> 16) & 0xff) * (256 - k)) >> 8;
    }
    if(map) {
        rect.x = scr->sx; rect.y = scr->sy; rect.w = scr->sw; rect.h = scr->sh;
        if(!scr->x) { rect.x += 24; rect.w -= 24; }
        if(!scr->y) { rect.y += 24; rect.h -= 24; }
        if(scr->x >= scr->w - scr->sw) rect.w -= 24;
        if(scr->y >= scr->h - scr->sh) rect.h -= 24;
        SDL_RenderSetClipRect(renderer, &rect);
        for(k = 0; k < MAPS_NUMLYR - 1; k++, m <<= 1)
            for(j = l = 0; j < project.mapsize; j++)
                for(i = 0; i < project.mapsize; i++, l++, map++) {
                    cat = maps_paint[maps_p[k]].cat;
                    if(!(mask & m)) continue;
                    maps_map2scr(scr, i, j, &rect.x, &rect.y);
                    if(*map == 0xffff && ((i == cx && j == cy) ||
                      (maps_clipmrk && k == maps_lyrsel.sel.val && maps_selmask && maps_selmask[l]))) {
                        if(maps_empty) {
                            rect.w = maps_tw; rect.h = maps_th;
                            rect.x -= rect.w / 2; rect.y -= rect.h;
                            SDL_RenderCopy(renderer, maps_empty, NULL, &rect);
                        }
                        continue;
                    }
                    if(*map < project.spritenum[cat]) {
                        s = &project.sprites[cat][(int)*map].dir[0];
                        if(s->data) {
                            src.x = ui_anim(s->type, s->nframe, -1) * s->w; src.y = 0; src.w = s->w; src.h = s->h;
                            rect.w = s->w * maps_tw / project.tilew; rect.h = s->h * rect.w / s->w;
                            rect.x -= rect.w / 2; rect.y -= rect.h;
                            if(!project.sprites[cat][(int)*map].txt)
                                spr_texture(s, &project.sprites[cat][(int)*map].txt);
                            if((texture = project.sprites[cat][(int)*map].txt)) {
                                if((i == cx && j == cy) || (k == maps_lyrsel.sel.val && maps_selmask && maps_selmask[l]))
                                    SDL_SetTextureColorMod(texture, r, g, b);
                                SDL_RenderCopy(renderer, texture, &src, &rect);
                                SDL_SetTextureColorMod(texture, 255, 255, 255);
                            }
                        }
                    }
                }
    }
    if(mask & MAP_MASK_GRID && ui_popup_type == -1 && ui_menu == -1) {
        if(maps_tool == MAPS_PASTE - MAPS_PIPETTE && maps_clipboard && maps_cliplyr >= 0 && maps_cliplyr < MAPS_NUMLYR - 1) {
            map = maps_clipboard;
            maps_scr2map(scr, ui_curx, ui_cury, &k, &l);
            if(k != -1 && l != -1) {
                k -= maps_clipcrd.x; l -= maps_clipcrd.y;
                for(j = l; j < project.mapsize + l; j++)
                    for(i = k; i < project.mapsize + k; i++, map++) {
                        cat = maps_paint[maps_p[maps_cliplyr]].cat;
                        if(i < 0 || i >= project.mapsize || j < 0 || j >= project.mapsize || *map == 0xffff ||
                            *map >= project.spritenum[cat]) continue;
                        s = &project.sprites[cat][(int)*map].dir[0];
                        if(s->data) {
                            maps_map2scr(scr, i, j, &rect.x, &rect.y);
                            src.x = ui_anim(s->type, s->nframe, -1) * s->w; src.y = 0; src.w = s->w; src.h = s->h;
                            rect.w = s->w * maps_tw / project.tilew; rect.h = s->h * rect.w / s->w;
                            rect.x -= rect.w / 2; rect.y -= rect.h;
                            if(!project.sprites[cat][(int)*map].txt)
                                spr_texture(s, &project.sprites[cat][(int)*map].txt);
                            if((texture = project.sprites[cat][(int)*map].txt)) {
                                SDL_SetTextureAlphaMod(texture, 128);
                                SDL_RenderCopy(renderer, texture, &src, &rect);
                                SDL_SetTextureAlphaMod(texture, 255);
                            }
                        }
                    }
            }
        } else
        if(maps_tool == MAPS_PAINT - MAPS_PIPETTE && ui_popup_type == -1 && ui_menu == -1 && maps_lyrsel.sel.val >= 0 &&
          maps_lyrsel.sel.val < MAPS_NUMLYR - 1 && maps_paint[maps_p[maps_lyrsel.sel.val]].val >= 0 &&
          maps_paint[maps_p[maps_lyrsel.sel.val]].val < project.spritenum[maps_paint[maps_p[maps_lyrsel.sel.val]].cat]) {
            maps_scr2map(scr, ui_curx, ui_cury, &i, &j);
            cat = maps_paint[maps_p[maps_lyrsel.sel.val]].cat;
            s = &project.sprites[cat][maps_paint[maps_p[maps_lyrsel.sel.val]].val].dir[0];
            if(s->data && i != -1 && j != -1) {
                maps_map2scr(scr, i, j, &rect.x, &rect.y);
                src.x = ui_anim(s->type, s->nframe, -1) * s->w; src.y = 0; src.w = s->w; src.h = s->h;
                rect.w = s->w * maps_tw / project.tilew; rect.h = s->h * rect.w / s->w;
                rect.x -= rect.w / 2; rect.y -= rect.h;
                if(!project.sprites[cat][maps_paint[maps_p[maps_lyrsel.sel.val]].val].txt)
                    spr_texture(s, &project.sprites[cat][maps_paint[maps_p[maps_lyrsel.sel.val]].val].txt);
                if((texture = project.sprites[cat][maps_paint[maps_p[maps_lyrsel.sel.val]].val].txt)) {
                    SDL_SetTextureAlphaMod(texture, 128);
                    SDL_RenderCopy(renderer, texture, &src, &rect);
                    SDL_SetTextureAlphaMod(texture, 255);
                }
            }
        }
    }
    rect.x = scr->sx; rect.y = scr->sy; rect.w = scr->sw; rect.h = scr->sh;
    SDL_RenderSetClipRect(renderer, &rect);
    if(mask & MAP_MASK_COLL) {
        if(!maps_coll) maps_setcoll();
        for(j = l = 0; j < project.mapsize; j++)
            for(i = 0; i < project.mapsize; i++, l++, map++) {
                maps_map2scr(scr, i, j, &rect.x, &rect.y);
                src.x = ((ICON_WALK + (maps_coll[l] & 7)) & 7) << 4;
                src.y = ((ICON_WALK + (maps_coll[l] & 7)) & ~7) << 1;
                src.w = src.h = rect.w = rect.h = 16;
                rect.x -= 8; rect.y -= th + 8;
                SDL_RenderCopy(renderer, icons, &src, &rect);
                if(maps_coll[l] & 8) {
                    src.x = (ICON_EVENT & 7) << 4;
                    src.y = (ICON_EVENT & ~7) << 1;
                    SDL_RenderCopy(renderer, icons, &src, &rect);
                }
            }
    }
    if(mask & MAP_MASK_PATH)
        for(k = 0; k < 99; k++)
            if(maps_paths[k]) {
                SDL_LockTexture(pos, NULL, (void**)&data, &p);
                memset(data, 0, p * 8);
                SDL_UnlockTexture(pos);
                sprintf(tmp, "%u", k + 1);
                if(maps_lyrsel.sel.val + MAPS_LYR_GRD1 == MAPS_LYR_PATHS && k + 1 == maps_path.val) {
                    SDL_SetRenderDrawColor(renderer, r, g, b, 0);
                    src.y = 4;
                    ui_number(pos, 0, 0, tmp, theme[THEME_SELBX]);
                } else {
                    SDL_SetRenderDrawColor(renderer, theme[THEME_PATH] & 0xff, (theme[THEME_PATH] >> 8) & 0xff,
                        (theme[THEME_PATH] >> 16) & 0xff, 0);
                    src.y = 0;
                    ui_number(pos, 0, 0, tmp, theme[THEME_PATH]);
                }
                maps_map2scr(scr, maps_paths[k][0].x, maps_paths[k][0].y, &rect.x, &rect.y);
                rect2.w = rect.w = 8; rect2.h = rect.h = 6;
                rect.x -= 4; rect.y -= th + 8; rect2.x = rect2.y = 0;
                SDL_RenderCopy(renderer, pos, &rect2, &rect);
                maps_map2scr(scr, maps_paths[k][maps_pathlen[k] - 1].x, maps_paths[k][maps_pathlen[k] - 1].y,
                    &rect.x, &rect.y);
                rect.y -= th;
                for(i = 0; i < maps_pathlen[k]; i++) {
                    rect.w = rect.x; rect.h = rect.y;
                    maps_map2scr(scr, maps_paths[k][i].x, maps_paths[k][i].y, &rect.x, &rect.y);
                    rect.y -= th;
                    SDL_RenderDrawLine(renderer, rect.w, rect.h, rect.x, rect.y);
                    src.x = 0; rect2.x = rect.x - 3; rect2.y = rect.y;
                    src.w = rect2.w = 7; src.h = rect2.h = 4;
                    d = (atan2(rect.y - rect.h, rect.x - rect.w) + PI) * 360 / (2 * PI) - 90;
                    if(d < 0) d += 360;
                    SDL_RenderCopyEx(renderer, arrows, &src, &rect2, d, &src2, SDL_FLIP_NONE);
                }
            }
    if(mask & MAP_MASK_GRID) {
        switch(project.type) {
            case TNG_TYPE_ORTHO:
                maps_map2scr(scr, 0, 0, &rect.x, &rect.y);
                maps_map2scr(scr, project.mapsize, project.mapsize, &rect.w, &rect.h);
                rect.x -= tw; rect.y -= maps_th;
                rect.w -= tw; rect.h -= maps_th;
                for(i = 0; i <= project.mapsize; i++) {
                    SDL_SetRenderDrawColor(renderer, theme[i % 10 ? THEME_GRID1 : THEME_GRID2] & 0xff,
                        (theme[i % 10 ? THEME_GRID1 : THEME_GRID2] >> 8) & 0xff,
                        (theme[i % 10 ? THEME_GRID1 : THEME_GRID2] >> 16) & 0xff, 0);
                    SDL_RenderDrawLine(renderer, rect.x, rect.y + i * maps_th, rect.w, rect.y + i * maps_th);
                    SDL_RenderDrawLine(renderer, rect.x + i * maps_tw, rect.y, rect.x + i * maps_tw, rect.h);
                }
                if(maps_selnum && maps_selmask) {
                    SDL_SetRenderDrawColor(renderer, r, g, b, 0);
                    for(j = 0; j <= project.mapsize; j++)
                        for(i = 0; i <= project.mapsize; i++) {
                            if(i < project.mapsize && ((!j && maps_selmask[i]) ||
                              (j == project.mapsize && maps_selmask[(j - 1) * project.mapsize + i]) ||
                              (j > 0 && j < project.mapsize &&
                              ((maps_selmask[j * project.mapsize + i] && !maps_selmask[(j - 1) * project.mapsize + i]) ||
                              (!maps_selmask[j * project.mapsize + i] && maps_selmask[(j - 1) * project.mapsize + i])))))
                                SDL_RenderDrawLine(renderer, rect.x + i * maps_tw, rect.y + j * maps_th,
                                    rect.x + (i + 1) * maps_tw, rect.y + j * maps_th);
                            if(j < project.mapsize && ((!i && maps_selmask[j * project.mapsize]) ||
                              (i == project.mapsize && maps_selmask[j * project.mapsize + i - 1]) ||
                              (i > 0 && i < project.mapsize &&
                              ((maps_selmask[j * project.mapsize + i] && !maps_selmask[j * project.mapsize + i - 1]) ||
                              (!maps_selmask[j * project.mapsize + i] && maps_selmask[j * project.mapsize + i - 1])))))
                                SDL_RenderDrawLine(renderer, rect.x + i * maps_tw, rect.y + j * maps_th,
                                    rect.x + i * maps_tw, rect.y + (j + 1) * maps_th);
                        }
                }
                if(cx != -1 && cy != -1) {
                    SDL_SetRenderDrawColor(renderer, r, g, b, 0);
                    SDL_RenderDrawLine(renderer, rect.x + cx * maps_tw, rect.y + cy * maps_th, rect.x + (cx + 1) * maps_tw,
                        rect.y + cy * maps_th);
                    SDL_RenderDrawLine(renderer, rect.x + cx * maps_tw, rect.y + cy * maps_th, rect.x + cx * maps_tw,
                        rect.y + (cy + 1) * maps_th);
                    SDL_RenderDrawLine(renderer, rect.x + (cx + 1) * maps_tw, rect.y + cy * maps_th, rect.x + (cx + 1) * maps_tw,
                        rect.y + (cy + 1) * maps_th);
                    SDL_RenderDrawLine(renderer, rect.x + cx * maps_tw, rect.y + (cy + 1) * maps_th, rect.x + (cx + 1) * maps_tw,
                        rect.y + (cy + 1) * maps_th);
                }
                maps_scr2map(scr, ui_curx, ui_cury, &i, &j);
                if(i != -1 && j != -1 && (scr != &maps_scr || ui_popup_type == -1) && ui_menu == -1) {
                    SDL_SetRenderDrawColor(renderer, theme[THEME_FG] & 0xff, (theme[THEME_FG] >> 8) & 0xff,
                        (theme[THEME_FG] >> 16) & 0xff, 0);
                    SDL_RenderDrawLine(renderer, rect.x + i * maps_tw, rect.y + j * maps_th, rect.x + (i + 1) * maps_tw,
                        rect.y + j * maps_th);
                    SDL_RenderDrawLine(renderer, rect.x + i * maps_tw, rect.y + j * maps_th, rect.x + i * maps_tw,
                        rect.y + (j + 1) * maps_th);
                    SDL_RenderDrawLine(renderer, rect.x + (i + 1) * maps_tw, rect.y + j * maps_th, rect.x + (i + 1) * maps_tw,
                        rect.y + (j + 1) * maps_th);
                    SDL_RenderDrawLine(renderer, rect.x + i * maps_tw, rect.y + (j + 1) * maps_th, rect.x + (i + 1) * maps_tw,
                        rect.y + (j + 1) * maps_th);
                    if(maps_tool == MAPS_SELBOX - MAPS_PIPETTE && maps_selbox.x != -1 && maps_selbox.y != -1) {
                        SDL_SetRenderDrawColor(renderer, r, g, b, 0);
                        maps_map2scr(scr, maps_selbox.x, maps_selbox.y, &rect.x, &rect.y);
                        maps_map2scr(scr, i, j, &k, &l);
                        rect.y -= th; l -= th;
                        SDL_RenderDrawLine(renderer, rect.x, rect.y, k, rect.y);
                        SDL_RenderDrawLine(renderer, rect.x, rect.y, rect.x, l);
                        SDL_RenderDrawLine(renderer, k, rect.y, k, l);
                        SDL_RenderDrawLine(renderer, rect.x, l, k, l);
                    }
                }
            break;
            case TNG_TYPE_ISO:
                maps_map2scr(scr, 0, 0, &rect.x, &rect.y);
                maps_map2scr(scr, 0, project.mapsize, &rect.w, &rect.h);
                rect.y -= maps_th; rect.h -= maps_th;
                for(i = 0; i <= project.mapsize; i++) {
                    SDL_SetRenderDrawColor(renderer, theme[i % 10 ? THEME_GRID1 : THEME_GRID2] & 0xff,
                        (theme[i % 10 ? THEME_GRID1 : THEME_GRID2] >> 8) & 0xff,
                        (theme[i % 10 ? THEME_GRID1 : THEME_GRID2] >> 16) & 0xff, 0);
                    SDL_RenderDrawLine(renderer, rect.x + i * tw, rect.y + i * th, rect.w + i * tw, rect.h + i * th);
                }
                maps_map2scr(scr, project.mapsize, 0, &rect.w, &rect.h);
                rect.h -= maps_th;
                for(i = 0; i <= project.mapsize; i++) {
                    SDL_SetRenderDrawColor(renderer, theme[i % 10 ? THEME_GRID1 : THEME_GRID2] & 0xff,
                        (theme[i % 10 ? THEME_GRID1 : THEME_GRID2] >> 8) & 0xff,
                        (theme[i % 10 ? THEME_GRID1 : THEME_GRID2] >> 16) & 0xff, 0);
                    SDL_RenderDrawLine(renderer, rect.x - i * tw, rect.y + i * th, rect.w - i * tw, rect.h + i * th);
                }
                if(maps_selnum && maps_selmask) {
                    SDL_SetRenderDrawColor(renderer, r, g, b, 0);
                    for(j = 0; j <= project.mapsize; j++)
                        for(i = 0; i <= project.mapsize; i++) {
                            maps_map2scr(scr, i, j, &rect.x, &rect.y);
                            rect.y -= maps_th;
                            if(i < project.mapsize && ((!j && maps_selmask[i]) ||
                              (j == project.mapsize && maps_selmask[(j - 1) * project.mapsize + i]) ||
                              (j > 0 && j < project.mapsize &&
                              ((maps_selmask[j * project.mapsize + i] && !maps_selmask[(j - 1) * project.mapsize + i]) ||
                              (!maps_selmask[j * project.mapsize + i] && maps_selmask[(j - 1) * project.mapsize + i])))))
                                SDL_RenderDrawLine(renderer, rect.x, rect.y, rect.x + tw, rect.y + th);
                            if(j < project.mapsize && ((!i && maps_selmask[j * project.mapsize]) ||
                              (i == project.mapsize && maps_selmask[j * project.mapsize + i - 1]) ||
                              (i > 0 && i < project.mapsize &&
                              ((maps_selmask[j * project.mapsize + i] && !maps_selmask[j * project.mapsize + i - 1]) ||
                              (!maps_selmask[j * project.mapsize + i] && maps_selmask[j * project.mapsize + i - 1])))))
                                SDL_RenderDrawLine(renderer, rect.x, rect.y, rect.x - tw, rect.y + th);
                        }
                }
                if(cx != -1 && cy != -1) {
                    SDL_SetRenderDrawColor(renderer, r, g, b, 0);
                    maps_map2scr(scr, cx, cy, &rect.x, &rect.y);
                    SDL_RenderDrawLine(renderer, rect.x, rect.y - maps_th, rect.x + tw, rect.y - th);
                    SDL_RenderDrawLine(renderer, rect.x, rect.y - maps_th, rect.x - tw, rect.y - th);
                    SDL_RenderDrawLine(renderer, rect.x - tw, rect.y - th, rect.x, rect.y);
                    SDL_RenderDrawLine(renderer, rect.x + tw, rect.y - th, rect.x, rect.y);
                }
                maps_scr2map(scr, ui_curx, ui_cury, &i, &j);
                if(i != -1 && j != -1 && (scr != &maps_scr || ui_popup_type == -1) && ui_menu == -1) {
                    SDL_SetRenderDrawColor(renderer, theme[THEME_FG] & 0xff, (theme[THEME_FG] >> 8) & 0xff,
                        (theme[THEME_FG] >> 16) & 0xff, 0);
                    maps_map2scr(scr, i, j, &rect.x, &rect.y);
                    SDL_RenderDrawLine(renderer, rect.x, rect.y - maps_th, rect.x + tw, rect.y - th);
                    SDL_RenderDrawLine(renderer, rect.x, rect.y - maps_th, rect.x - tw, rect.y - th);
                    SDL_RenderDrawLine(renderer, rect.x - tw, rect.y - th, rect.x, rect.y);
                    SDL_RenderDrawLine(renderer, rect.x + tw, rect.y - th, rect.x, rect.y);
                    if(maps_tool == MAPS_SELBOX - MAPS_PIPETTE && maps_selbox.x != -1 && maps_selbox.y != -1) {
                        SDL_SetRenderDrawColor(renderer, r, g, b, 0);
                        maps_map2scr(scr, maps_selbox.x, maps_selbox.y, &rect.w, &rect.h);
                        maps_map2scr(scr, i, maps_selbox.y, &k, &l);
                        rect.y -= th; rect.h -= th; l -= th;
                        SDL_RenderDrawLine(renderer, rect.x, rect.y, k, l);
                        SDL_RenderDrawLine(renderer, k, l, rect.w, rect.h);
                        maps_map2scr(scr, maps_selbox.x, j, &k, &l);
                        l -= th;
                        SDL_RenderDrawLine(renderer, rect.x, rect.y, k, l);
                        SDL_RenderDrawLine(renderer, k, l, rect.w, rect.h);
                    }
                }
            break;
            case TNG_TYPE_HEXV:
            break;
            case TNG_TYPE_HEXH:
            break;
        }
        SDL_SetRenderDrawColor(renderer, r, g, b, 0);
        if((maps_tool == MAPS_PAINT - MAPS_PIPETTE || maps_tool == MAPS_PASTE - MAPS_PIPETTE) && maps_selline.x != -1 &&
          ui_isshift && !popup && !ui_input_type) {
            maps_map2scr(scr, maps_selline.x, maps_selline.y, &rect.x, &rect.y);
            rect.y -= maps_th / 2;
            SDL_RenderDrawLine(renderer, rect.x, rect.y, ui_curx, ui_cury);
        }
        if(maps_selply) {
            maps_map2scr(scr, maps_selply[0].x, maps_selply[0].y, &rect.x, &rect.y);
            rect.y -= maps_th / 2;
            for(i = 1; i < maps_sellen; i++) {
                rect.w = rect.x; rect.h = rect.y;
                maps_map2scr(scr, maps_selply[i].x, maps_selply[i].y, &rect.x, &rect.y);
                rect.y -= maps_th / 2;
                SDL_RenderDrawLine(renderer, rect.w, rect.h, rect.x, rect.y);
            }
            SDL_RenderDrawLine(renderer, rect.x, rect.y, ui_curx, ui_cury);
        }
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
    }
    SDL_RenderSetClipRect(renderer, NULL);
    (void)mask;
}

/**
 * Draw table cell
 */
void maps_drawtbl(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    int *i = (int*)data;

    (void)idx; (void)sel; (void)w; (void)h; (void)hdr;
    if(!data || *i < 0) return;
    ui_text(dst, x + 4, y + 1, project.maps[*i]);
}

/**
 * Draw table cell
 */
void maps_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    char tmp[16] = { 0 };
    maps_par_t *par = (maps_par_t*)data;

    (void)idx; (void)sel; (void)w; (void)h;
    if(!data) return;
    ui_text(dst, x + 4, y + 1, maps_partypes[par->bg]);
    sprintf(tmp, "%3d  %3u", par->sx, par->dx);
    ui_text(dst, x + 24 + hdr[0].w, y + 1, tmp);
    sprintf(tmp, "%3d  %3u", par->sy, par->dy);
    ui_text(dst, x + 24 + hdr[0].w + hdr[1].w, y + 1, tmp);
    sprintf(tmp, "%02u.%02u", par->tm / 100, par->tm % 100);
    ui_text(dst, x + 52 + hdr[0].w + hdr[1].w + hdr[2].w, y + 1, tmp);
    if(par->spr >= 0)
        ui_text(dst, x + 24 + hdr[0].w + hdr[1].w + hdr[2].w + hdr[3].w, y + 1, project.sprites[maps_parspr.cat][par->spr].name);
}

/**
 * Draw table cell
 */
void maps_rendercell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    maps_par_t *par = (maps_par_t*)data;
    SDL_Rect src, rect;
    ui_sprite_t *s;

    (void)dst; (void)idx; (void)sel; (void)w; (void)h;
    if(!data || par->spr < 0) return;
    s = spr_getidx(maps_parspr.cat, par->spr, 0, 0);
    if(s) {
        src.x = ui_anim(s->type, s->nframe, -1) * s->w; src.y = 0; src.w = s->w; src.h = s->h;
        rect.x = x + 4 + hdr[0].w + hdr[1].w + hdr[2].w + hdr[3].w; rect.y = y + 1;
        ui_fit(16, 16, s->w, s->h, &rect.w, &rect.h);
        if(!project.sprites[maps_parspr.cat][par->spr].txt)
            spr_texture(s, &project.sprites[maps_parspr.cat][par->spr].txt);
        if(project.sprites[maps_parspr.cat][par->spr].txt)
            SDL_RenderCopy(renderer, project.sprites[maps_parspr.cat][par->spr].txt, &src, &rect);
    }
}

/**
 * Map search box callback
 */
void maps_search(void *data)
{
    int *maps_idx = (int*)maps_tbl.data;
    int i, j, l, k;

    (void)data;
    if(!ui_input_buf[0]) {
        maps_tbl.num = project.nummaps;
        for(i = 0; i < maps_tbl.num; i++)
            maps_idx[i] = i;
    } else {
        maps_tbl.val = maps_tbl.scr = maps_tbl.num = 0; l = strlen(ui_input_buf);
        for(j = 0; j < project.nummaps; j++) {
            k = strlen(project.maps[j]);
            if(k < l) continue;
            k -= l;
            for(i = 0; i <= k && strncasecmp(project.maps[j] + i, ui_input_buf, l); i++);
            if(i > k) continue;
            maps_idx[maps_tbl.num++] = j;
        }
    }
    if(maps_tbl.val >= maps_tbl.num) maps_tbl.val = maps_tbl.num - 1;
}

/**
 * Free resources
 */
void maps_freetmx()
{
    int i;

    spr_flush();
    maps_hist_free();
    if(maps_daytxt) { SDL_DestroyTexture(maps_daytxt); maps_daytxt = NULL; }
    if(maps_map) { free(maps_map); maps_map = NULL; }
    if(maps_coll) { free(maps_coll); maps_coll = NULL; }
    if(maps_selmask) { free(maps_selmask); maps_selmask = NULL; }
    if(maps_partbl.data) { free(maps_partbl.data); maps_partbl.data = NULL; }
    maps_partbl.num = 0; maps_partbl.val = maps_partbl.clk = maps_dayspr.val = maps_backmus.val = -1;
    for(i = 0; i < 6; i++)
        maps_neightbors[i] = -1;
    memset(maps_neightstrs, 0, sizeof(maps_neightstrs));
    for(i = 0; i < 99; i++)
        if(maps_paths[i]) free(maps_paths[i]);
    memset(maps_paths, 0, sizeof(maps_paths));
    memset(maps_pathlen, 0, sizeof(maps_pathlen));
    if(maps_selply) { free(maps_selply); maps_selply = NULL; }
    maps_sellen = maps_selnum = maps_daylast = maps_daylen.val = 0;
    maps_selbox.x = maps_selbox.y = maps_selline.x = maps_selline.y = maps_ox = maps_oy = maps_oanim = -1;
    maps_base = 0;
}

/**
 * Free all resources
 */
void maps_free()
{
    int i, j;

    maps_freetmx();
    maps_clrmask(NULL);
    if(maps_clipboard) { free(maps_clipboard); maps_clipboard = NULL; }
    if(maps_objlights) { free(maps_objlights); maps_objlights = NULL; }
    if(maps_npcs) {
        for(i = 0; i < project.spritenum[PROJ_NUMSPRCATS - 2]; i++) {
            if(maps_npcs[i].cmd) free(maps_npcs[i].cmd);
            if(maps_npcs[i].spr) {
                for(j = 0; j < 8 * SPRPERLYR; j++)
                    if(maps_npcs[i].spr[j].data) free(maps_npcs[i].spr[j].data);
                free(maps_npcs[i].spr);
            }
        }
        free(maps_npcs);
        maps_npcs = NULL;
    }
    for(j = 0; j < MAPS_NUMLYR - 1; j++)
        for(i = 0; i < project.spritenum[maps_paint[maps_p[j]].cat]; i++)
            if(project.sprites[maps_paint[maps_p[j]].cat][i].txt) {
                SDL_DestroyTexture(project.sprites[maps_paint[maps_p[j]].cat][i].txt);
                project.sprites[maps_paint[maps_p[j]].cat][i].txt = NULL;
            }
}

/**
 * Exit maps window
 */
void maps_exit(int tab)
{
    int i;

    (void)tab;
    maps_tbl.clk = -1;
    if(maps_tbl.data) {
        free(maps_tbl.data); maps_tbl.data = NULL;
    }
    if(maps_masks) {
        for(i = 0; i < project.spritenum[1]; i++)
            if(maps_masks[i].data) free(maps_masks[i].data);
        free(maps_masks); maps_masks = NULL;
    }
    if(maps_auto) { free(maps_auto); maps_auto = NULL; }
    maps_numauto = 0;
    if(maps_autoidx) {
        for(i = 0; i < project.spritenum[1]; i++)
            if(maps_autoidx[i]) free(maps_autoidx[i]);
        free(maps_autoidx); maps_autoidx = NULL;
    }
    maps_clrmask(NULL);
}

/**
 * Enter maps window
 */
void maps_init(int tab)
{
    char *str, *s, **list;
    int i, j, k, x, y, n, nl = 0, v = verbose;

    if(verbose > 0) verbose--;
    maps_exit(tab);
    maps_form[1].param = lang[LANG_SAVE];
    maps_form[1].w = ui_textwidth(maps_form[1].param) + 40;
    if(maps_form[1].w < 200) maps_form[1].w = 200;
    project_freedir(&project.maps, &project.nummaps);
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_MAPS]);
    project.maps = project_getdir(projdir, ".tmx", &project.nummaps);
    maps_tbl.data = main_alloc((project.nummaps + 1) * sizeof(int));
    strcpy(ui_input_buf, maps_srch);
    maps_search(NULL);
    if(!maps_name[0]) maps_tbl.val = -1;
    for(i = 0; i < 6; i++) {
        maps_dir[i].icon = ICON_LINK;
        maps_dir[i].crd = NULL;
        maps_dir[i].val = &maps_neightbors[i];
        maps_neightbors[i] = -1;
    }
    /* load automap rules */
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_TILES]);
    list = project_getdir(projdir, ".til", &nl);
    if(project.spritenum[1] > 0 && nl > 0 && list) {
        maps_autoidx = (int**)main_alloc(project.spritenum[1] * sizeof(int*));
        for(n = 0; n < nl; n++) {
            for(j = 0; j < project.spritenum[1] && strcmp(list[n], project.sprites[1][j].name); j++);
            if(j >= project.spritenum[1]) continue;
            str = project_loadfile(project_dirs[PROJDIRS_TILES], list[n], "til", PROJMAGIC_TILE, "maps_load");
            if(str) {
                s = project_skipnl(str);
                s = project_skipnl(s);
                s = project_skipnl(s);
                while(*s) {
                    s = project_skipnl(s);
                    if(!*s) break;
                    maps_auto = (int*)realloc(maps_auto, (maps_numauto + 10) * sizeof(int));
                    if(!maps_auto) main_error(ERR_MEM);
                    for(i = 0; *s && i < 9; i++) {
                        s = project_getsprite(s, &maps_autospr);
                        maps_auto[maps_numauto + i] = maps_autospr.val;
                        /* create automap index */
                        if(maps_autospr.val >= 0 && maps_autospr.val < project.spritenum[1]) {
                            /* a tile might appear multiple times in the same rule, only index once */
                            k = 0;
                            if(maps_autoidx[maps_autospr.val])
                                for(; maps_autoidx[maps_autospr.val][k] != -1; k++)
                                    if(maps_autoidx[maps_autospr.val][k] == maps_numauto) { k = -1; break; }
                            if(k != -1) {
                                maps_autoidx[maps_autospr.val] = (int*)realloc(maps_autoidx[maps_autospr.val],
                                    (k + 2) * sizeof(int));
                                if(maps_autoidx[maps_autospr.val]) {
                                    maps_autoidx[maps_autospr.val][k] = maps_numauto;
                                    maps_autoidx[maps_autospr.val][k + 1] = -1;
                                }
                            }
                        }
                    }
                    for(i = 0; i < 9 && maps_auto[maps_numauto + i] == -1; i++);
                    if(i < 9) {
                        maps_auto[maps_numauto + 9] = j;
                        maps_numauto += 10;
                    }
                }
                free(str);
            }
        }
    }
    project_freedir((char***)&list, &nl);
    /* load object collision masks */
    if(project.spritenum[1] > 0) {
        maps_masks = (ui_mask_t*)main_alloc(project.spritenum[1] * sizeof(ui_mask_t));
        for(i = 0; i < project.spritenum[1]; i++) {
            str = project_loadfile(project_dirs[PROJDIRS_OBJECTS], project.sprites[1][i].name, "obj", PROJMAGIC_OBJECT, "maps_load");
            if(str) {
                s = project_skipnl(str);
                s = project_skipnl(s);
                s = project_getint(s, &maps_masks[i].w, 0, 512);
                s = project_getint(s, &maps_masks[i].h, 0, 512);
                if(maps_masks[i].w > 0 && maps_masks[i].h > 0) {
                    maps_masks[i].data = (char*)main_alloc(maps_masks[i].w * maps_masks[i].h);
                    for(y = k = 0; *s && y < maps_masks[i].h; y++) {
                        s = project_skipnl(s);
                        for(x = 0; *s && x < maps_masks[i].w; x++, k++) {
                            s = project_getint(s, &n, 0, 15);
                            maps_masks[i].data[k] = (char)n;
                        }
                    }
                }
                free(str);
            }
        }
    }
    for(i = 0; i < MAPS_NUMLYR + 1; i++) {
        maps_lyrnames[i] = lang[MAPS_LYR_GRD1 + i];
    }
    maps_lyrnames[i] = NULL;
    maps_layers = MAP_MASK_GRD1;
    maps_form[9].param = lang[MAPS_TITLE];
    maps_tab = ui_textwidth(maps_form[9].param);
    maps_form[11].param = lang[MAPS_DAYLIGHT];
    i = ui_textwidth(maps_form[11].param); if(i > maps_tab) maps_tab = i;
    if(!maps_map) maps_new(NULL);
    maps_scr.w = 0;
    memset(maps_tmxdirs, 0, sizeof(maps_tmxdirs));
    switch(project.type) {
        case TNG_TYPE_ORTHO:
        case TNG_TYPE_ISO:
            maps_form[44].status = MAPS_LINKSO; maps_tmxdirs[0] = "south";
            maps_form[45].status = MAPS_LINKWE; maps_tmxdirs[1] = "west";
            maps_form[46].status = MAPS_LINKNO; maps_tmxdirs[2] = "north";
            maps_form[47].status = MAPS_LINKEA; maps_tmxdirs[3] = "east";
        break;
        case TNG_TYPE_HEXV:
            maps_form[44].status = MAPS_LINKSW; maps_tmxdirs[0] = "south-west";
            maps_form[45].status = MAPS_LINKWE; maps_tmxdirs[1] = "west";
            maps_form[46].status = MAPS_LINKNW; maps_tmxdirs[2] = "north-west";
            maps_form[47].status = MAPS_LINKNE; maps_tmxdirs[3] = "north-east";
            maps_form[48].status = MAPS_LINKEA; maps_tmxdirs[4] = "east";
            maps_form[49].status = MAPS_LINKSE; maps_tmxdirs[5] = "south-east";
        break;
        case TNG_TYPE_HEXH:
            maps_form[44].status = MAPS_LINKSO; maps_tmxdirs[0] = "south";
            maps_form[45].status = MAPS_LINKSW; maps_tmxdirs[1] = "south-west";
            maps_form[46].status = MAPS_LINKNW; maps_tmxdirs[2] = "north-west";
            maps_form[47].status = MAPS_LINKNO; maps_tmxdirs[3] = "north";
            maps_form[48].status = MAPS_LINKNE; maps_tmxdirs[4] = "north-east";
            maps_form[49].status = MAPS_LINKSE; maps_tmxdirs[5] = "south-east";
        break;
    }
    verbose = v;
}

/**
 * Resize the view
 */
void maps_resize(int tab, int w, int h)
{
    int i, j;

    (void)tab;
    maps_form[0].y = maps_form[1].y = maps_form[2].y = maps_form[3].y = maps_form[26].y = maps_form[51].y = maps_form[52].y =
        maps_form[53].y = maps_form[54].y = h - 48;
    maps_form[1].x = w - 20 - maps_form[1].w;
    maps_form[2].x = maps_form[1].x - 52;
    ui_table_resize(&maps_form[5], maps_form[5].w, h - 59 - maps_form[5].y);
    maps_form[3].x = maps_form[5].x + maps_form[5].w + 20;
    maps_form[8].x = maps_form[9].x = maps_form[11].x = maps_form[15].x = maps_form[16].x = maps_form[25].x =
        maps_form[5].x + maps_form[5].w + 10;
    /* meta info */
    maps_form[9].w = maps_form[11].w = maps_tab;
    maps_form[10].x = maps_form[12].x = maps_form[14].x = maps_form[9].x + maps_tab + 4;
    maps_form[10].w = w - 10 - maps_form[10].x;
    maps_form[13].x = maps_form[12].x + maps_form[12].w + 10;
    maps_form[13].w = w - 10 - maps_form[13].x;
    maps_form[14].w = w - 10 - maps_form[14].x;
    ui_table_resize(&maps_form[15], w - 10 - maps_form[15].x, h - 59 - 24 - maps_form[15].y);
    maps_form[16].y = maps_form[17].y = maps_form[18].y = maps_form[19].y = maps_form[20].y = maps_form[21].y =
        maps_form[22].y = maps_form[23].y = maps_form[24].y = maps_form[15].y + maps_form[15].h + 4;
    maps_form[17].x = maps_form[16].x + maps_form[16].w + 10;
    maps_form[18].x = maps_form[17].x + maps_form[17].w + 4;
    maps_form[19].x = maps_form[18].x + maps_form[18].w + 4;
    maps_form[20].x = maps_form[19].x + maps_form[19].w + 4;
    maps_form[21].x = maps_form[20].x + maps_form[20].w + 4;
    maps_form[22].x = maps_form[21].x + maps_form[21].w + 4;
    maps_form[23].x = maps_form[22].x + maps_form[22].w + 4;
    maps_form[24].x = w - 10 - maps_form[24].w;
    maps_form[23].w = maps_form[24].x - maps_form[23].x - 10;
    /* map editor */
    maps_form[25].w = w - 10 - 12 - maps_form[25].x;
    maps_form[25].h = h - 59 - 12 - maps_form[25].y;
    maps_form[26].x = w / 2 - maps_form[26].w - 10;
    maps_form[27].x = maps_form[8].x + maps_form[8].w + 4;
    maps_form[28].x = maps_form[29].x = maps_form[30].x = maps_form[31].x = maps_form[32].x = maps_form[33].x = maps_form[34].x =
        maps_form[35].x =maps_form[27].x + maps_form[27].w + 10;
    maps_form[36].x = maps_form[35].x + maps_form[35].w + 4;
    maps_form[37].x = maps_form[44].x = maps_form[36].x + maps_form[36].w + 8;
    maps_form[38].x = maps_form[37].x + maps_form[37].w + 8;
    maps_form[39].x = maps_form[38].x + maps_form[38].w + 8;
    maps_form[40].x = maps_form[39].x + maps_form[39].w + 4;
    maps_form[41].x = maps_form[40].x + maps_form[40].w + 4;
    maps_form[42].x = maps_form[41].x + maps_form[41].w + 8;
    maps_form[43].x = maps_form[42].x + maps_form[42].w + 4;
    maps_form[51].x = w / 2 + 10;
    maps_form[52].x = maps_form[51].x + maps_form[51].w + 4;
    maps_form[53].x = maps_form[52].x + maps_form[52].w + 4;
    maps_form[54].x = maps_form[53].x + maps_form[53].w + 4;
    i = maps_form[43].x + maps_form[43].w + 8;
    maps_form[4].w = 200; maps_form[4].x = w - 10 - maps_form[4].w;
    if(i > maps_form[4].x) { maps_form[4].x = i; maps_form[4].w = w - 10 - maps_form[4].x; }
    i = maps_scr.x + maps_scr.sw / 2;
    j = maps_scr.y + maps_scr.sh / 2;
    ui_scrarea_set(&maps_scr, maps_form[25].x, maps_form[25].y, maps_form[25].w, maps_form[25].h);
    if(!maps_scr.w) {
        maps_size(0, &maps_scr.w, &maps_scr.h);
        maps_scr.ow = maps_scr.oh = maps_scr.z = 0;
        maps_scr.x = (maps_scr.w - maps_scr.sw) / 2;
        maps_scr.y = (maps_scr.h - maps_scr.sh) / 2 + 2 * project.tileh;
    } else {
        maps_scr.x = i - maps_scr.sw / 2;
        maps_scr.y = j - maps_scr.sh / 2;
        if(maps_scr.x + maps_scr.sw > maps_scr.w) maps_scr.x = maps_scr.w - maps_scr.sw;
        if(maps_scr.x < 0) maps_scr.x = 0;
        if(maps_scr.y + maps_scr.sh > maps_scr.h) maps_scr.y = maps_scr.h - maps_scr.sh;
        if(maps_scr.y < 0) maps_scr.y = 0;
    }
    /* map links */
    switch(project.type) {
        case TNG_TYPE_ORTHO:
            maps_form[45].x = maps_scr.sx + (maps_scr.sw - 20) / 2; /* south */
            maps_form[45].y = maps_scr.sy + maps_scr.sh - 22;
            maps_form[46].x = maps_scr.sx + 2;                      /* west */
            maps_form[46].y = maps_scr.sy + (maps_scr.sh - 20) / 2;
            maps_form[47].x = maps_scr.sx + (maps_scr.sw - 20) / 2; /* north */
            maps_form[47].y = maps_scr.sy + 2;
            maps_form[48].x = maps_scr.sx + maps_scr.sw - 22;       /* east */
            maps_form[48].y = maps_scr.sy + (maps_scr.sh - 20) / 2;
        break;
        case TNG_TYPE_ISO:
            maps_form[45].x = maps_scr.sx + 2;                      /* south */
            maps_form[45].y = maps_scr.sy + maps_scr.sh - 22;
            maps_form[46].x = maps_scr.sx + 2;                      /* west */
            maps_form[46].y = maps_scr.sy + 2;
            maps_form[47].x = maps_scr.sx + maps_scr.sw - 22;       /* north */
            maps_form[47].y = maps_scr.sy + 2;
            maps_form[48].x = maps_scr.sx + maps_scr.sw - 22;       /* east */
            maps_form[48].y = maps_scr.sy + maps_scr.sh - 22;
        break;
        case TNG_TYPE_HEXV:
        break;
        case TNG_TYPE_HEXH:
        break;
    }
}

/**
 * Clear empty cell mask
 */
void maps_clrmask(void *data)
{
    (void)data;
    if(maps_empty) { SDL_DestroyTexture(maps_empty); maps_empty = NULL; }
}

/**
 * View layer
 */
void maps_redraw(int tab)
{
    int i, j, k;
    uint32_t *data, c;

    (void)tab;
    ui_form = maps_form;
    ui_form[0].inactive = (maps_tbl.val < 0);
    ui_form[1].inactive = (!maps_name[0]);
    for(i = 9; i < 25; i++)
        ui_form[i].inactive = (maps_meta ? 0 : 2);
    if(maps_meta) {
        for(; ui_form[i].type; i++)
            ui_form[i].inactive = 2;
        ui_form[16].inactive = (maps_partbl.val < 0);
        ui_form[24].inactive = (maps_parspr.val < 0);
    } else {
        ui_form[25].inactive = ui_form[26].inactive = ui_form[27].inactive = 0;
        for(i = 28; i < 51; i++)
            ui_form[i].inactive = 2;
        for(; ui_form[i].type; i++)
            ui_form[i].inactive = 0;
        maps_layers |= 1 << maps_lyrsel.sel.val;
        switch(maps_lyrsel.sel.val + MAPS_LYR_GRD1) {
            case MAPS_LYR_GRD1:
            case MAPS_LYR_GRD2:
            case MAPS_LYR_GRD3:
            case MAPS_LYR_GRD4:
                maps_grp1.num = 3;
                ui_form[28].inactive = ui_form[36].inactive = ui_form[37].inactive = ui_form[38].inactive =
                    ui_form[39].inactive = ui_form[40].inactive = ui_form[41].inactive = 0;
                    ui_form[42].inactive = ui_form[43].inactive = (maps_tool != MAPS_PASTE - MAPS_PIPETTE);
            break;
            case MAPS_LYR_OBJECTS:
                maps_grp1.num = 3;
                ui_form[29].inactive = ui_form[36].inactive = ui_form[37].inactive = ui_form[38].inactive =
                    ui_form[39].inactive = ui_form[40].inactive = ui_form[41].inactive = 0;
                    ui_form[42].inactive = ui_form[43].inactive = (maps_tool != MAPS_PASTE - MAPS_PIPETTE);
            break;
            case MAPS_LYR_NPCS:
                if(maps_tool > 1) maps_tool = 1;
                maps_grp1.num = 2;
                ui_form[30].inactive = ui_form[36].inactive = ui_form[37].inactive = 0;
            break;
            case MAPS_LYR_SPWN:
                if(maps_tool > 1) maps_tool = 1;
                maps_grp1.num = 2;
                ui_form[31].inactive = ui_form[36].inactive = ui_form[37].inactive = 0;
            break;
            case MAPS_LYR_ROOF1:
            case MAPS_LYR_ROOF2:
            case MAPS_LYR_ROOF3:
            case MAPS_LYR_ROOF4:
                maps_grp1.num = 3;
                ui_form[28].inactive = ui_form[36].inactive = ui_form[37].inactive = ui_form[38].inactive =
                    ui_form[39].inactive = ui_form[40].inactive = ui_form[41].inactive = 0;
                    ui_form[42].inactive = ui_form[43].inactive = (maps_tool != MAPS_PASTE - MAPS_PIPETTE);
            break;
            case MAPS_LYR_COLL:
                maps_tool = 1;
            break;
            case MAPS_LYR_PATHS:
                ui_form[35].inactive = ui_form[36].inactive = ui_form[44].inactive = 0;
            break;
        }
        ui_form[41].type = (maps_tool == MAPS_PASTE - MAPS_PIPETTE ? FORM_ICONBTNP : FORM_ICONBTN);
        /* map links */
        for(i = 0; i < 6; i++)
            maps_dir[i].icon = (maps_neightbors[i] >= 0 ? ICON_LINK : ICON_INLNK);
        if(!ui_scroll.scrx && !ui_scroll.scry)
            switch(project.type) {
                case TNG_TYPE_ORTHO:
                    if(maps_scr.y >= maps_scr.h - maps_scr.sh - 2) maps_form[45].inactive = 0;  /* south */
                    if(maps_scr.x < 2) maps_form[46].inactive = 0;                              /* west */
                    if(maps_scr.y < 2) maps_form[47].inactive = 0;                              /* north */
                    if(maps_scr.x >= maps_scr.w - maps_scr.sw - 2) maps_form[48].inactive = 0;  /* east */
                break;
                case TNG_TYPE_ISO:
                    /* these calculations could be and should be simplified... */
                    i = (((maps_scr.y - 24 - 2 * maps_th) * (maps_scr.w / 2)) / (project.mapsize * maps_th / 2) + maps_tw / 2);
                    j = (((maps_scr.h + maps_th) / 2) - ((maps_scr.x - maps_tw / 2) * (project.mapsize * maps_th / 2)) /
                        (maps_scr.w / 2) - maps_scr.y) / 2;
                    k = (((maps_scr.h + maps_th) / 2) - ((maps_scr.w - maps_scr.x - maps_scr.sw + maps_tw / 2) *
                        (project.mapsize * maps_th / 2)) / (maps_scr.w / 2) - maps_scr.y) / 2;
                    if(maps_scr.w / 2 - i - maps_scr.x > 26 && j > 26)
                        maps_form[46].inactive = 0;                                             /* west */
                    if(maps_scr.sw - (maps_scr.w / 2 + i - maps_scr.x) > 26 && k > 26)
                         maps_form[47].inactive = 0;                                            /* north */
                    i = (maps_scr.h - 24 - maps_scr.sh - maps_scr.y) * (maps_scr.w / 2) / (project.mapsize * maps_th / 2) +
                        maps_tw / 2;
                    if(i < maps_tw / 2) i = maps_tw / 2;
                    j = (maps_scr.x - maps_tw / 2) * (project.mapsize * maps_th / 2) / (project.mapsize * maps_tw / 2) + 24 +
                        3 * maps_th + project.mapsize * maps_th / 2 - maps_scr.y;
                    k = (maps_scr.w - maps_scr.sw - maps_scr.x - maps_tw / 2) * (project.mapsize * maps_th / 2) /
                        (project.mapsize * maps_tw / 2) + 24 + 3 * maps_th + project.mapsize * maps_th / 2 - maps_scr.y;
                    if(maps_scr.w / 2 - i - maps_scr.x > 26 && maps_scr.sh - j > 26)
                         maps_form[45].inactive = 0;                                            /* south */
                    if(maps_scr.sw - (maps_scr.w / 2 + i - maps_scr.x) > 26 && maps_scr.sh - k > 26)
                         maps_form[48].inactive = 0;                                            /* east */
                break;
                case TNG_TYPE_HEXV:
                break;
                case TNG_TYPE_HEXH:
                break;
            }
    }
    /* generate empty cell mask */
    if(!maps_empty) {
        maps_empty = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, maps_tw, maps_th);
        if(maps_empty) {
            SDL_SetTextureBlendMode(maps_empty, SDL_BLENDMODE_BLEND);
            SDL_LockTexture(maps_empty, NULL, (void**)&data, &k);
            c = 0x7F00000 | (theme[THEME_SELBX] & 0xFFFFFF);
            switch(project.type) {
                case TNG_TYPE_ORTHO:
                    for(i = 0; i < maps_tw * maps_th; i++)
                        data[i] = c;
                break;
                case TNG_TYPE_ISO:
                    for(j = 0; j < maps_th; j++)
                        for(i = 0; i < maps_tw; i++) {
                            k = maps_tw / 2 - 1 - (maps_tw / 2 * (j < maps_th / 2 ? j : maps_th - j -1) /
                                (maps_th / 2));
                            data[j * maps_tw + i] = (i < k || i >= maps_tw - k ? 0 : c);
                        }
                break;
                case TNG_TYPE_HEXV:
                break;
                case TNG_TYPE_HEXH:
                break;
            }
            SDL_UnlockTexture(maps_empty);
        }
    }
}

/**
 * View layer
 */
void maps_render(int tab)
{
    (void)tab;
    if(!maps_meta)
        maps_rendertmx(&maps_scr, MAP_MASK_GRID | maps_layers, -1, -1);
}

/**
 * Layer change handler
 */
void maps_chglyr(void *data)
{
    (void)data;
    maps_select_check();
    maps_status();
}

/**
 * Link change handler
 */
void maps_chglink(void *data)
{
    ui_icnsptr_t *icnp = (ui_icnsptr_t*)data;
    int *maps_idx = (int*)maps_tbl.data;

    /* do not allow selecting the current map as link */
    if(icnp && icnp->sptr.val && *(icnp->sptr.val) >= 0 && (
      (maps_idx && maps_tbl.val >= 0 && maps_idx[maps_tbl.val] == *(icnp->sptr.val)) ||
      !strcmp(project.maps[*(icnp->sptr.val)], maps_name)))
        *(icnp->sptr.val) = -1;
}

/**
 * Change paint
 */
void maps_chgpaint(void *data)
{
    int i;

    (void)data;
    for(i = 0; i < (int)(sizeof(maps_paint)/sizeof(maps_paint[0])); i++)
        if(i != maps_p[maps_lyrsel.sel.val] && maps_paint[i].cat == maps_paint[maps_p[maps_lyrsel.sel.val]].cat)
            maps_paint[i].val = maps_paint[maps_p[maps_lyrsel.sel.val]].val;
    maps_clk = 0;
    maps_tool = 1;
    maps_status();
}

/**
 * Add a history record
 */
void maps_hist_add(int lyr, int x0, int y0, int x1, int y1, void *old, void *buf)
{
    uint16_t *map;
    int i, w = 0, h = 0;

    if(!old && !buf) return;
    if(maps_hist && maps_hist_idx < maps_hist_len) {
        for(i = maps_hist_idx; i < maps_hist_len; i++) {
            if(maps_hist[i].old) free(maps_hist[i].old);
            if(maps_hist[i].buf) free(maps_hist[i].buf);
        }
        maps_hist_len = maps_hist_idx + 1;
    } else {
        maps_hist_idx = maps_hist_len++;
        maps_hist = (maps_hist_t*)realloc(maps_hist, maps_hist_len * sizeof(maps_hist_t));
    }
    if(!maps_hist) { maps_hist_len = maps_hist_idx = 0; return; }
    maps_hist[maps_hist_idx].old = maps_hist[maps_hist_idx].buf = NULL;
    if(lyr == MAPS_LYR_PATHS - MAPS_LYR_GRD1) {
        h = 0;
        if(x0 > 0 && old) {
            maps_hist[maps_hist_idx].old = malloc(x0 * sizeof(SDL_Point));
            if(maps_hist[maps_hist_idx].old)
                memcpy(maps_hist[maps_hist_idx].old, old, x0 * sizeof(SDL_Point));
            else
                x0 = 0;
        }
        if(y0 > 0 && buf) {
            maps_hist[maps_hist_idx].buf = malloc(y0 * sizeof(SDL_Point));
            if(maps_hist[maps_hist_idx].buf)
                memcpy(maps_hist[maps_hist_idx].buf, buf, y0 * sizeof(SDL_Point));
            else
                y0 = 0;
        }
    } else {
        w = x1 - x0 + 1;
        h = y1 - y0 + 1;
        if(old) {
            maps_hist[maps_hist_idx].old = map = malloc(w * h * sizeof(uint16_t));
            if(map)
                for(i = 0; i < h; i++)
                    memcpy(map + i * w, (uint16_t*)old + (i + y0) * project.mapsize + x0, w * sizeof(uint16_t));
            if(buf)
                memcpy(old, buf, project.mapsize * project.mapsize * sizeof(uint16_t));
            else
                memset(old, 0xff, project.mapsize * project.mapsize * sizeof(uint16_t));
        }
        if(buf) {
            maps_hist[maps_hist_idx].buf = map = malloc(w * h * sizeof(uint16_t));
            if(map)
                for(i = 0; i < h; i++)
                    memcpy(map + i * w, (uint16_t*)buf + (i + y0) * project.mapsize + x0, w * sizeof(uint16_t));
        }
    }
    maps_hist[maps_hist_idx].lyr = lyr;
    maps_hist[maps_hist_idx].x = x0;
    maps_hist[maps_hist_idx].y = y0;
    maps_hist[maps_hist_idx].w = w;
    maps_hist[maps_hist_idx].h = h;
    maps_hist_idx++;
}

/**
 * Prepare for history transaction (save current layer in maps_histchk)
 */
void maps_hist_prepare()
{
    if(!maps_map || maps_lyrsel.sel.val >= MAPS_NUMLYR - 1) return;
    if(!maps_histchk) {
        maps_histchk = (uint16_t*)malloc(project.mapsize * project.mapsize * sizeof(uint16_t));
        if(!maps_histchk) return;
    }
    memcpy(maps_histchk, maps_map + maps_lyrsel.sel.val * project.mapsize * project.mapsize,
        project.mapsize * project.mapsize * sizeof(uint16_t));
}

/**
 * Commit a history transaction (find bounding box of modified items and call history add)
 */
void maps_hist_commit()
{
    int i, x, y, mix = project.mapsize, miy = project.mapsize, max = 0, may = 0;
    uint16_t *map = maps_map + maps_lyrsel.sel.val * project.mapsize * project.mapsize;

    if(!maps_map || !maps_histchk || maps_lyrsel.sel.val >= MAPS_NUMLYR - 1) return;
    for(y = i = 0; y < project.mapsize; y++)
        for(x = 0; x < project.mapsize; x++, i++)
            if(maps_histchk[i] != map[i]) {
                if(x < mix) mix = x;
                if(x > max) max = x;
                if(y < miy) miy = y;
                if(y > may) may = y;
            }
    if(mix <= max && miy <= may)
        maps_hist_add(maps_lyrsel.sel.val, mix, miy, max, may, maps_histchk, map);
}

/**
 * Undo
 */
void maps_hist_undo()
{
    int i;
    uint16_t *map, *old;

    if(maps_hist_idx < 1) return;
    maps_hist_idx--;
    if(!(old = maps_hist[maps_hist_idx].old)) return;
    if(maps_hist[maps_hist_idx].lyr == MAPS_LYR_PATHS - MAPS_LYR_GRD1) {
        i = maps_hist[maps_hist_idx].w;
        if(maps_hist[maps_hist_idx].x > 0 && maps_hist[maps_hist_idx].old) {
            maps_paths[i] = (SDL_Point*)realloc(maps_paths[i], maps_hist[maps_hist_idx].x * sizeof(SDL_Point));
            if(maps_paths[i]) {
                memcpy(maps_paths[i], maps_hist[maps_hist_idx].old, maps_hist[maps_hist_idx].x * sizeof(SDL_Point));
                maps_pathlen[i] = maps_hist[maps_hist_idx].x;
            } else
                maps_pathlen[i] = 0;
        } else {
            if(maps_paths[i]) free(maps_paths[i]);
            maps_paths[i] = NULL;
            maps_pathlen[i] = 0;
        }
    } else {
        map = maps_map + maps_hist[maps_hist_idx].lyr * project.mapsize * project.mapsize;
        for(i = 0; i < maps_hist[maps_hist_idx].h; i++)
            memcpy(map + (i + maps_hist[maps_hist_idx].y) * project.mapsize + maps_hist[maps_hist_idx].x,
                old + i * maps_hist[maps_hist_idx].w, maps_hist[maps_hist_idx].w * sizeof(uint16_t));
    }
}

/**
 * Redo
 */
void maps_hist_redo()
{
    int i;
    uint16_t *map, *buf;

    if(maps_hist_idx >= maps_hist_len || !maps_hist[maps_hist_idx].buf) return;
    if(!(buf = maps_hist[maps_hist_idx].buf)) return;
    if(maps_hist[maps_hist_idx].lyr == MAPS_LYR_PATHS - MAPS_LYR_GRD1) {
        i = maps_hist[maps_hist_idx].w;
        if(maps_hist[maps_hist_idx].y > 0 && maps_hist[maps_hist_idx].buf) {
            maps_paths[i] = (SDL_Point*)realloc(maps_paths[i], maps_hist[maps_hist_idx].y * sizeof(SDL_Point));
            if(maps_paths[i]) {
                memcpy(maps_paths[i], maps_hist[maps_hist_idx].buf, maps_hist[maps_hist_idx].y * sizeof(SDL_Point));
                maps_pathlen[i] = maps_hist[maps_hist_idx].y;
            } else
                maps_pathlen[i] = 0;
        } else {
            if(maps_paths[i]) free(maps_paths[i]);
            maps_paths[i] = NULL;
            maps_pathlen[i] = 0;
        }
    } else {
        map = maps_map + maps_hist[maps_hist_idx].lyr * project.mapsize * project.mapsize;
        for(i = 0; i < maps_hist[maps_hist_idx].h; i++)
            memcpy(map + (i + maps_hist[maps_hist_idx].y) * project.mapsize + maps_hist[maps_hist_idx].x,
                buf + i * maps_hist[maps_hist_idx].w, maps_hist[maps_hist_idx].w * sizeof(uint16_t));
    }
    maps_hist_idx++;
}

/**
 * Drop history
 */
void maps_hist_free()
{
    int i;

    if(maps_histchk) { free(maps_histchk); maps_histchk = NULL; }
    if(maps_hist) {
        for(i = 0; i < maps_hist_len; i++) {
            if(maps_hist[i].old) free(maps_hist[i].old);
            if(maps_hist[i].buf) free(maps_hist[i].buf);
        }
        free(maps_hist); maps_hist = NULL;
    }
    maps_hist_len = maps_hist_idx = 0;
}

/**
 * Add a path
 */
void maps_addpath()
{
    maps_hist_add(MAPS_LYR_PATHS - MAPS_LYR_GRD1, maps_pathlen[maps_path.val - 1], maps_sellen, maps_path.val - 1, 0,
        maps_paths[maps_path.val - 1], maps_selply);
    if(maps_paths[maps_path.val - 1]) free(maps_paths[maps_path.val - 1]);
    maps_paths[maps_path.val - 1] = maps_selply;
    maps_pathlen[maps_path.val - 1] = maps_sellen;
}

/**
 * Flip path direction
 */
void maps_flipdir(void *data)
{
    SDL_Point tmp;
    int i;

    (void)data;
    if(maps_pathlen[maps_path.val - 1] < 2 || !maps_paths[maps_path.val - 1]) return;
    for(i = 0; i < maps_pathlen[maps_path.val - 1] / 2; i++) {
        memcpy(&tmp, &maps_paths[maps_path.val - 1][i], sizeof(SDL_Point));
        memcpy(&maps_paths[maps_path.val - 1][i], &maps_paths[maps_path.val - 1][maps_pathlen[maps_path.val - 1] - 1 - i],
            sizeof(SDL_Point));
        memcpy(&maps_paths[maps_path.val - 1][maps_pathlen[maps_path.val - 1] - 1 - i], &tmp, sizeof(SDL_Point));
    }
}

/**
 * Set to eraser mode
 */
void maps_erase(void *data)
{
    int i;

    (void)data;
    maps_tool = 1;
    for(i = 0; i < (int)(sizeof(maps_paint)/sizeof(maps_paint[0])); i++)
        maps_paint[i].val = -1;
    if(maps_lyrsel.sel.val + MAPS_LYR_GRD1 == MAPS_LYR_PATHS) {
        /* erase path */
        maps_hist_add(MAPS_LYR_PATHS - MAPS_LYR_GRD1, maps_pathlen[maps_path.val - 1], 0, maps_path.val - 1, 0,
            maps_paths[maps_path.val - 1], NULL);
        if(maps_paths[maps_path.val - 1]) free(maps_paths[maps_path.val - 1]);
        maps_paths[maps_path.val - 1] = NULL;
        maps_pathlen[maps_path.val - 1] = 0;
    }
}

/**
 * Dump automap rules (for debugging only)
 */
void maps_autodump()
{
    int i, j;

    if(maps_auto) {
        printf("Ruleset:\nRule  Match pattern                          Set\n");
        for(j = 0; j < maps_numauto; ) {
            printf("%4d:", j);
            for(i = 0; i < 9; i++, j++)
                printf(" %3d%s", maps_auto[j], i == 2 || i == 5 ? "," : "");
            printf(": %3d (%s)\n",maps_auto[j],maps_auto[j] >= 0 && maps_auto[j] < project.spritenum[1] ?
                project.sprites[1][maps_auto[j]].name : "???");
            j++;
        }
        printf("\n");
    }
    if(maps_autoidx) {
        printf("Index:\nTile  In rules\n");
        for(j = 0; j < project.spritenum[1]; j++)
            if(maps_autoidx[j]) {
                printf("%4d:", j);
                for(i = 0; maps_autoidx[j][i] != -1; i++)
                    printf(" %4d", maps_autoidx[j][i]);
                printf("\n");
            }
        printf("\n");
    }
}

/**
 * Set a map item if automap rule pattern matches
 */
void maps_autoset(uint16_t *map, int x, int y, int *rule)
{
    int i, j, k, l, X, Y, dx, dy, m;

    switch(project.type) {
        case TNG_TYPE_ORTHO:
        case TNG_TYPE_ISO:
            /* we have to match in 9 different positions, and we only know that X is on the map, but not sure
             * about the o's. What's more, if rule isn't set for that position, then off-map counts as a match.
             *
             * j = 0 .....   j = 1 .....   j = 2 .....
             *       .....         .....         .....
             *       ..Xoo         .oXo.         ooX..
             *       ..ooo         .ooo.         ooo..
             *       ..ooo         .ooo.         ooo..
             *
             * j = 3 .....   j = 4 .....   j = 5 .....
             *       ..ooo         .ooo.         ooo..
             *       ..Xoo         .oXo.         ooX..
             *       ..ooo         .ooo.         ooo..
             *       .....         .....         .....
             *
             * j = 6 ..ooo   j = 7 .ooo.   j = 8 ooo..
             *       ..ooo         .ooo.         ooo..
             *       ..Xoo         .oXo.         ooX..
             *       .....         .....         .....
             *       .....         .....         .....
             */
            for(j = 0; j < 9; j++) {
                m = 1; dy = j / 3; dx = j % 3;
                for(Y = l = 0; m && Y < 3; Y++) {
                    i = (y + Y - dy);
                    if(i >= 0 && i < project.mapsize) {
                        k = x - dx;
                        i = i * project.mapsize + k;
                        for(X = 0; X < 3; X++, k++, i++, l++)
                            if(rule[l] != -1 && (k < 0 || k >= project.mapsize || map[i] != rule[l])) {
                                m = 0; break;
                            }
                    } else {
                        if(rule[l] != -1 || rule[l + 1] != -1 || rule[l + 2] != -1) m = 0;
                        else l += 3;
                    }
                }
                /* if there was a match, change the tile in the middle of the match one layer up */
                if(m) {
                    i = (y + 1 - dy);
                    k = x + 1 - dx;
                    if(i >= 0 && i < project.mapsize && k >= 0 && k < project.mapsize)
                        map[i * project.mapsize + project.mapsize * project.mapsize + k] = rule[9];
                }
            }
        break;
        case TNG_TYPE_HEXV:
        break;
        case TNG_TYPE_HEXH:
        break;
    }
}

/**
 * Set a map item
 */
void maps_set(int x, int y, uint16_t t)
{
    int i = y * project.mapsize + x, j, tool = maps_tool + MAPS_PIPETTE, s = project.mapsize * project.mapsize, k;
    uint16_t *map = maps_map + maps_lyrsel.sel.val * s;

    if(x < 0 || y < 0 || x >= project.mapsize || y >= project.mapsize) return;
    if(tool == MAPS_PIPETTE) {
        if(maps_lyrsel.sel.val < MAPS_NUMLYR - 1) {
            maps_paint[maps_p[maps_lyrsel.sel.val]].val = (map[i] == 0xffff ? -1 : (int)map[i]);
            if(map[i] == 0xffff) {
                if(maps_lyrsel.sel.val < 5) {
                    for(k = 4; k >= 0; k--)
                        if(maps_map[i + k * s] != 0xffff) {
                            maps_paint[maps_p[maps_lyrsel.sel.val]].val = (int)maps_map[i + k * s];
                            break;
                        }
                }
                if(maps_lyrsel.sel.val > 6) {
                    for(k = MAPS_NUMLYR - 2; k > 6; k--)
                        if(maps_map[i + k * s] != 0xffff) {
                            maps_paint[maps_p[maps_lyrsel.sel.val]].val = (int)maps_map[i + k * s];
                            break;
                        }
                }
            }
            maps_chgpaint(NULL);
        }
        return;
    }
    if(tool != MAPS_PASTE && ((tool != MAPS_PAINT && tool != MAPS_FILL) || (maps_selnum && maps_selmask && !maps_selmask[i])))
        return;
    switch(maps_lyrsel.sel.val) {
        case MAPS_NUMLYR - 1: /* no direct collision mask setting */
        case MAPS_NUMLYR: /* no mask setting for paths */ break;
        default:
            if(maps_lyrsel.sel.val == 4 && maps_coll) { free(maps_coll); maps_coll = NULL; }
            if(maps_lyrsel.sel.val < 5)
                for(k = 0; k < 5; k++)
                    if(maps_map[i + k * s] == t) {
                        if(k == 4 && maps_coll) { free(maps_coll); maps_coll = NULL; }
                        maps_map[i + k * s] = 0xffff;
                    }
            if(maps_lyrsel.sel.val > 6)
                for(k = 7; k < MAPS_NUMLYR - 1; k++)
                    if(maps_map[i + k * s] == t) maps_map[i + k * s] = 0xffff;
            map[i] = t;
            if(!maps_lyrsel.sel.val) {
                if(t != 0xffff && maps_auto && maps_autoidx && maps_autoidx[t]) {
                    /* automap rules */
                    for(j = 0; maps_autoidx[t][j] != -1; j++)
                        maps_autoset(maps_map, x, y, &maps_auto[maps_autoidx[t][j]]);
                }
            }
        break;
    }
}

/**
 * Set a map item in a line
 */
void maps_line(int x0, int y0, int x1, int y1, uint16_t t)
{
    int e, e2, dx, dy, sx = x0 < x1 ? 1 : -1, sy = y0 < y1 ? 1 : -1;

    if(x0 < 0 || y0 < 0 || x1 < 0 || y1 < 0) return;
    dx = (x1 > x0 ? x1 - x0 : x0 - x1);
    dy = (y1 > y0 ? y0 - y1 : y1 - y0);
    e = dx + dy;
    while(1) {
        maps_set(x0, y0, t);
        e2 = 2 * e;
        if(e2 >= dy) { if(x0 == x1) break; e += dy; x0 += sx; }
        if(e2 <= dx) { if(y0 == y1) break; e += dx; y0 += sy; }
    }
}

/**
 * Flood fill map with item
 */
void maps_fill(int x, int y, uint16_t t)
{
    int i = y * project.mapsize + x, s = maps_lyrsel.sel.val * project.mapsize * project.mapsize;
    uint16_t o;

    if(x < 0 || y < 0 || x >= project.mapsize || y >= project.mapsize) return;
    if(maps_map[s + i] != t) {
        if(ui_ismeta) {
            o = maps_map[s + i];
            for(y = i = 0; y < project.mapsize; y++)
                for(x = 0; x < project.mapsize; x++, i++)
                    if(maps_map[s + i] == o && (!maps_selnum || !maps_selmask || maps_selmask[i]))
                        maps_set(x, y, t);
        } else
        if(ui_isshift) {
            if(maps_selnum && maps_selmask)
                for(y = i = 0; y < project.mapsize; y++)
                    for(x = 0; x < project.mapsize; x++, i++)
                        if(maps_selmask[i])
                            maps_set(x, y, t);
        } else {
            maps_set(x, y, t);
            if(y > 0 && (!maps_selnum || !maps_selmask || maps_selmask[i - project.mapsize]))
                maps_fill(x, y - 1, t);
            if(x > 0 && (!maps_selnum || !maps_selmask || maps_selmask[i - 1]))
                maps_fill(x - 1, y, t);
            if(x < project.mapsize - 1 && (!maps_selnum || !maps_selmask || maps_selmask[i + 1]))
                maps_fill(x + 1, y, t);
            if(y < project.mapsize - 1 && (!maps_selnum || !maps_selmask || maps_selmask[i + project.mapsize]))
                maps_fill(x, y + 1, t);
        }
    }
}


/**
 * Check if only empty tiles are selected
 */
void maps_select_check()
{
    int i, s = project.mapsize * project.mapsize;

    maps_clipmrk = 0;
    if(!maps_selmask || maps_lyrsel.sel.val >= MAPS_NUMLYR - 1) return;
    maps_clipmrk = 1;
    for(i = 0; i < s; i++)
        if(maps_map[maps_lyrsel.sel.val * s + i] != 0xffff) { maps_clipmrk = 0; break; }
}

/**
 * Make a box selection
 */
void maps_select_box(int x0, int y0, int x1, int y1, int clear)
{
    int j, x, y;

    if(!maps_selmask) return;
    if(!ui_isshift && !ui_isctrl) { memset(maps_selmask, 0, project.mapsize * project.mapsize); maps_selnum = 0; }
    if(x0 < 0 || y0 < 0 || x1 < 0 || y1 < 0 || (x0 == x1 && y0 == y1 && clear)) return;
    if(x0 > x1) { j = x0; x0 = x1; x1 = j; }
    if(y0 > y1) { j = y0; y0 = y1; y1 = j; }
    for(y = y0; y <= y1; y++) {
        j = y * project.mapsize;
        for(x = x0; x <= x1; x++)
            if(ui_isctrl) {
                if(maps_selmask[j + x]) { maps_selnum--; maps_selmask[j + x] = 0; }
            } else {
                if(!maps_selmask[j + x]) { maps_selnum++; maps_selmask[j + x] = 1; }
            }
    }
    maps_select_check();
}

/**
 * Make a polygon selection
 */
int maps_polycmp(const void *a, const void *b) { return (int)(*((const float*)a) - *((const float*)b)); }
void maps_select_poly()
{
    int e, e2, sx, sy, dx, dy, x0, x1, y0, y1, i, j, l, n;
    float x, y, mix, miy, max, may, *nodes, *line;

    if(!maps_selmask) return;
    if(!ui_isshift && !ui_isctrl) { memset(maps_selmask, 0, project.mapsize * project.mapsize); maps_selnum = 0; }
    if(!maps_selply || maps_sellen < 1) return;
    line = (float*)malloc(maps_sellen * 2 * sizeof(float));
    if(!line) return;
    mix = max = line[0] = (float)maps_selply[0].x; miy = may = line[1] = (float)maps_selply[0].y;
    for(l = 2, i = 1; i < maps_sellen; i++) {
        x = line[l++] = (float)maps_selply[i].x; y = line[l++] = (float)maps_selply[i].y;
        if(line[l - 2] != x || line[l - 1] != y) { line[l++] = x; line[l++] = y; }
        if(x < mix) mix = x;
        if(x > max) max = x;
        if(y < miy) miy = y;
        if(y > may) may = y;
    }
    if(l < 4) {
        j = maps_selply[0].y * project.mapsize + maps_selply[0].x;
        if(ui_isctrl) {
            if(maps_selmask[j]) { maps_selnum--; maps_selmask[j] = 0; }
        } else {
            if(!maps_selmask[j]) { maps_selnum++; maps_selmask[j] = 1; }
        }
        free(line);
        return;
    }
    nodes = (float*)malloc(l * sizeof(float));
    if(nodes) {
        for(y = miy; y < may; y++) {
            for(i = n = 0, j = l - 2; i < l; i += 2) {
                if((line[i + 1] < y && line[j + 1] >= y) ||
                   (line[j + 1] < y && line[i + 1] >= y)) {
                    x = line[i + 0] + 0.5 + ((y - line[i + 1]) * (line[j + 0] - line[i + 0]) / (line[j + 1] - line[i + 1]));
                    nodes[n++] = x;
                }
                j = i;
            }
            qsort(nodes, n, sizeof(float), maps_polycmp);
            j = (int)y * project.mapsize;
            for(i = 0; i < n && nodes[i] < max; i += 2) {
                if(nodes[i + 1] > mix) {
                    if(nodes[i] < mix) nodes[i] = mix;
                    if(nodes[i + 1] > max) nodes[i + 1] = max;
                    for(x = nodes[i]; x < nodes[i + 1]; x++)
                        if(ui_isctrl) {
                            if(maps_selmask[j + (int)x]) { maps_selnum--; maps_selmask[j + (int)x] = 0; }
                        } else {
                            if(!maps_selmask[j + (int)x]) { maps_selnum++; maps_selmask[j + (int)x] = 1; }
                        }
                }
            }
        }
        free(nodes);
    }
    for(i = 2; i < l + 2; i += 2) {
        x0 = (int)line[i - 2]; y0 = (int)line[i - 1]; x1 = (int)line[i % l]; y1 = (int)line[(i + 1) % l];
        sx = x0 < x1 ? 1 : -1; sy = y0 < y1 ? 1 : -1;
        dx = (x1 > x0 ? x1 - x0 : x0 - x1);
        dy = (y1 > y0 ? y0 - y1 : y1 - y0);
        e = dx + dy;
        while(1) {
            j = y0 * project.mapsize + x0;
            if(ui_isctrl) {
                if(maps_selmask[j] == 1) { maps_selnum--; maps_selmask[j] = 0; }
            } else {
                if(!maps_selmask[j]) { maps_selnum++; maps_selmask[j] = 1; }
            }
            e2 = 2 * e;
            if(e2 >= dy) { if(x0 == x1) break; e += dy; x0 += sx; }
            if(e2 <= dx) { if(y0 == y1) break; e += dx; y0 += sy; }
        }
    }
    free(line);
    maps_select_check();
}

/**
 * Make a fuzzy selection of the same type
 */
void _maps_select_fuzzy(int x, int y, uint16_t t)
{
    int i = y * project.mapsize + x;

    if(x < 0 || y < 0 || x >= project.mapsize || y >= project.mapsize) return;
    if(maps_map[maps_lyrsel.sel.val * project.mapsize * project.mapsize + i] == t) {
        if(ui_isctrl) {
            if(maps_selmask[i] == 1) { maps_selnum--; maps_selmask[i] = 2; }
        } else {
            if(!maps_selmask[i]) { maps_selnum++; maps_selmask[i] = 3; }
        }
        if(y > 0 && maps_selmask[i - project.mapsize] < 2) _maps_select_fuzzy(x, y - 1, t);
        if(x > 0 && maps_selmask[i - 1] < 2) _maps_select_fuzzy(x - 1, y, t);
        if(x < project.mapsize - 1 && maps_selmask[i + 1] < 2) _maps_select_fuzzy(x + 1, y, t);
        if(y < project.mapsize - 1 && maps_selmask[i + project.mapsize] < 2) _maps_select_fuzzy(x, y + 1, t);
    }
}
void maps_select_fuzzy(int x, int y)
{
    int i;
    uint16_t t;

    if(!maps_selmask || maps_lyrsel.sel.val >= MAPS_NUMLYR - 1) return;
    if(!ui_isshift && !ui_isctrl) { memset(maps_selmask, 0, project.mapsize * project.mapsize); maps_selnum = 0; }
    t = maps_map[maps_lyrsel.sel.val * project.mapsize * project.mapsize + y * project.mapsize + x];
    if(!ui_ismeta)
        _maps_select_fuzzy(x, y, t);
    else
        for(i = 0; i < project.mapsize * project.mapsize; i++) {
            if(maps_map[maps_lyrsel.sel.val * project.mapsize * project.mapsize + i] == t) {
                if(ui_isctrl) {
                    if(maps_selmask[i] == 1) { maps_selnum--; maps_selmask[i] = 2; }
                } else {
                    if(!maps_selmask[i]) { maps_selnum++; maps_selmask[i] = 3; }
                }
            }
        }
    for(i = 0; i < project.mapsize * project.mapsize; i++)
        maps_selmask[i] &= ~2;
    maps_select_check();
}

/**
 * Do the actual copy and cut
 */
void maps_docopycut(int cut)
{
    uint16_t *map = maps_map + maps_lyrsel.sel.val * project.mapsize * project.mapsize;
    int i, j, k;

    if(!maps_selnum || !maps_selmask || maps_lyrsel.sel.val < 0 || maps_lyrsel.sel.val >= MAPS_NUMLYR - 1)
        return;
    if(!maps_clipboard) {
        maps_clipboard = (uint16_t*)malloc(project.mapsize * project.mapsize * sizeof(uint16_t));
        if(!maps_clipboard) return;
    }
    memset(maps_clipboard, 0xff, project.mapsize * project.mapsize * sizeof(uint16_t));
    maps_clipcrd.x = maps_clipcrd.y = project.mapsize; maps_clipcrd.w = maps_clipcrd.h = 0;
    maps_cliplyr = maps_lyrsel.sel.val;
    for(j = k = 0; j < project.mapsize; j++)
        for(i = 0; i < project.mapsize; i++, k++)
            if(maps_selmask[k]) {
                if(i < maps_clipcrd.x) maps_clipcrd.x = i;
                if(i > maps_clipcrd.w) maps_clipcrd.w = i;
                if(j < maps_clipcrd.y) maps_clipcrd.y = j;
                if(j > maps_clipcrd.h) maps_clipcrd.h = j;
                maps_clipboard[k] = map[k];
                if(cut) map[k] = 0xffff;
            }
    maps_clipcrd.x += (maps_clipcrd.w - maps_clipcrd.x) / 2;
    maps_clipcrd.y += (maps_clipcrd.h - maps_clipcrd.y) / 2;
}

/**
 * Flip clipboard horizontally
 */
void maps_hflip(void *data)
{
    uint16_t tmp;
    int i, j;

    (void)data;
    if(!maps_clipboard) return;
    for(j = 0; j < project.mapsize / 2; j++)
        for(i = 0; i < project.mapsize; i++) {
            tmp = maps_clipboard[j * project.mapsize + i];
            maps_clipboard[j * project.mapsize + i] = maps_clipboard[(project.mapsize - 1 - j) * project.mapsize + i];
            maps_clipboard[(project.mapsize - 1 - j) * project.mapsize + i] = tmp;
        }
    maps_clipcrd.y -= (maps_clipcrd.h - maps_clipcrd.y);
    maps_clipcrd.y = project.mapsize - 1 - maps_clipcrd.y;
}

/**
 * Flip clipboard vertically
 */
void maps_vflip(void *data)
{
    uint16_t tmp;
    int i, j;

    (void)data;
    if(!maps_clipboard) return;
    for(j = 0; j < project.mapsize; j++)
        for(i = 0; i < project.mapsize / 2; i++) {
            tmp = maps_clipboard[j * project.mapsize + i];
            maps_clipboard[j * project.mapsize + i] = maps_clipboard[j * project.mapsize + (project.mapsize - 1 - i)];
            maps_clipboard[j * project.mapsize + (project.mapsize - 1 - i)] = tmp;
        }
    maps_clipcrd.x = project.mapsize - 1 - maps_clipcrd.x;
}

/**
 * Do the actual paste
 */
void maps_dopaste(int x0, int y0, int x1, int y1)
{
    uint16_t *map;
    int i, j, e, e2, dx, dy, sx = x0 < x1 ? 1 : -1, sy = y0 < y1 ? 1 : -1;

    maps_selnum = 0;
    if(maps_selmask)
        memset(maps_selmask, 0, project.mapsize * project.mapsize);
    if(x0 < 0 || y0 < 0 || x1 < 0 || y1 < 0 || !maps_clipboard || maps_cliplyr < 0 || maps_cliplyr >= MAPS_NUMLYR - 1 ||
      maps_paint[maps_p[maps_cliplyr]].cat != maps_paint[maps_p[maps_lyrsel.sel.val]].cat) return;
    if(maps_lyrsel.sel.val == 4 && maps_coll) { free(maps_coll); maps_coll = NULL; }
    dx = (x1 > x0 ? x1 - x0 : x0 - x1);
    dy = (y1 > y0 ? y0 - y1 : y1 - y0);
    e = dx + dy;
    while(1) {
        for(j = y0 - maps_clipcrd.y, map = maps_clipboard; j < project.mapsize + y0 - maps_clipcrd.y; j++)
            for(i = x0 - maps_clipcrd.x; i < project.mapsize + x0 - maps_clipcrd.x; i++, map++)
                if(i >= 0 && i < project.mapsize && j >= 0 && j < project.mapsize && *map != 0xffff)
                    maps_set(i, j, *map);
        e2 = 2 * e;
        if(e2 >= dy) { if(x0 == x1) break; e += dy; x0 += sx; }
        if(e2 <= dx) { if(y0 == y1) break; e += dx; y0 += sy; }
    }
}

/**
 * Copy selected items to clipboard
 */
void maps_copy(void *data)
{
    (void)data;
    maps_docopycut(0);
}

/**
 * Cut selected items to clipboard
 */
void maps_cut(void *data)
{
    (void)data;
    maps_hist_prepare();
    maps_docopycut(1);
    maps_hist_commit();
    if(maps_lyrsel.sel.val == 4 && maps_coll) { free(maps_coll); maps_coll = NULL; }
}

/**
 * Paste items from clipboard
 */
void maps_paste(void *data)
{
    (void)data;
    if(maps_clipboard && maps_paint[maps_p[maps_cliplyr]].cat == maps_paint[maps_p[maps_lyrsel.sel.val]].cat) {
        maps_tool = MAPS_PASTE - MAPS_PIPETTE;
    }
}

/**
 * Move map left
 */
void maps_moveleft(void *data)
{
    int i, k, s = 0, e = 99;
    uint16_t tmp, *map = maps_map;

    (void)data;
    if(!maps_map) return;
    if(maps_lyrsel.sel.val != MAPS_LYR_PATHS - MAPS_LYR_GRD1) {
        for(i = 0; i < project.mapsize * (MAPS_NUMLYR - 1); i++, map += project.mapsize) {
            tmp = map[0];
            memcpy(&map[0], &map[1], (project.mapsize - 1) * sizeof(uint16_t));
            map[project.mapsize - 1] = tmp;
        }
    } else
        s = e = maps_path.val;
    for(k = s; k < e; k++)
        if(maps_paths[k] && maps_pathlen[k])
            for(i = 0; i < maps_pathlen[k]; i++)
                maps_paths[k][i].x = (maps_paths[k][i].x > 0 ? maps_paths[k][i].x : project.mapsize) - 1;
}

/**
 * Move map up
 */
void maps_moveup(void *data)
{
    int i, k, s = 0, e = 99;
    uint16_t *tmp, *map = maps_map;

    (void)data;
    if(!maps_map) return;
    if(maps_lyrsel.sel.val != MAPS_LYR_PATHS - MAPS_LYR_GRD1) {
        tmp = (uint16_t*)malloc(project.mapsize * sizeof(uint16_t));
        if(!tmp) return;
        for(i = 0; i < (MAPS_NUMLYR - 1); i++, map += project.mapsize * project.mapsize) {
            memcpy(tmp, map, project.mapsize * sizeof(uint16_t));
            memcpy(&map[0], &map[project.mapsize], project.mapsize * (project.mapsize - 1) * sizeof(uint16_t));
            memcpy(&map[project.mapsize * (project.mapsize - 1)], tmp, project.mapsize * sizeof(uint16_t));
        }
        free(tmp);
    } else
        s = e = maps_path.val;
    for(k = s; k < e; k++)
        if(maps_paths[k] && maps_pathlen[k])
            for(i = 0; i < maps_pathlen[k]; i++)
                maps_paths[k][i].y = (maps_paths[k][i].y > 0 ? maps_paths[k][i].y : project.mapsize) - 1;
}

/**
 * Move map down
 */
void maps_movedown(void *data)
{
    int i, k, s = 0, e = 99;
    uint16_t *tmp, *map = maps_map;

    (void)data;
    if(!maps_map) return;
    if(maps_lyrsel.sel.val != MAPS_LYR_PATHS - MAPS_LYR_GRD1) {
        tmp = (uint16_t*)malloc(project.mapsize * sizeof(uint16_t));
        if(!tmp) return;
        for(i = 0; i < (MAPS_NUMLYR - 1); i++, map += project.mapsize * project.mapsize) {
            memcpy(tmp, &map[project.mapsize * (project.mapsize - 1)], project.mapsize * sizeof(uint16_t));
            memmove(&map[project.mapsize], &map[0], project.mapsize * (project.mapsize - 1) * sizeof(uint16_t));
            memcpy(map, tmp, project.mapsize * sizeof(uint16_t));
        }
        free(tmp);
    } else
        s = e = maps_path.val;
    for(k = s; k < e; k++)
        if(maps_paths[k] && maps_pathlen[k])
            for(i = 0; i < maps_pathlen[k]; i++)
                maps_paths[k][i].y = (maps_paths[k][i].y < project.mapsize - 1 ? maps_paths[k][i].y + 1 : 0);
}

/**
 * Move map right
 */
void maps_moveright(void *data)
{
    int i, k, s = 0, e = 99;
    uint16_t tmp, *map = maps_map;

    (void)data;
    if(!maps_map) return;
    if(maps_lyrsel.sel.val != MAPS_LYR_PATHS - MAPS_LYR_GRD1) {
        for(i = 0; i < project.mapsize * (MAPS_NUMLYR - 1); i++, map += project.mapsize) {
            tmp = map[project.mapsize - 1];
            memmove(&map[1], &map[0], (project.mapsize - 1) * sizeof(uint16_t));
            map[0] = tmp;
        }
    } else
        s = e = maps_path.val;
    for(k = s; k < e; k++)
        if(maps_paths[k] && maps_pathlen[k])
            for(i = 0; i < maps_pathlen[k]; i++)
                maps_paths[k][i].x = (maps_paths[k][i].x < project.mapsize - 1 ? maps_paths[k][i].x + 1 : 0);
}

/**
 * Set status string
 */
void maps_status()
{
    char tmp[256] = { 0 };
    int x, y, i, j;

    if(ui_over == -1 && !maps_meta) {
        if(ui_curx >= maps_scr.sx && ui_curx < maps_scr.sx + maps_scr.sw &&
          ui_cury >= maps_scr.sy && ui_cury < maps_scr.sy + maps_scr.sh) {
            maps_scr2map(&maps_scr, ui_curx, ui_cury, &x, &y);
            if(x != -1 && y != -1) {
                if(maps_tool == MAPS_SELBOX - MAPS_PIPETTE && maps_selbox.x != -1) {
                    i = (x > maps_selbox.x ? x - maps_selbox.x : maps_selbox.x - x);
                    j = (y > maps_selbox.y ? y - maps_selbox.y : maps_selbox.y - y);
                    sprintf(tmp,"%s, X:%4u, Y:%4u (%u x %u) ", maps_lyrnames[maps_lyrsel.sel.val],
                        maps_selbox.x, maps_selbox.y, i, j);
                } else
                    sprintf(tmp,"%s, X:%4u, Y:%4u ", maps_lyrnames[maps_lyrsel.sel.val], x, y);
                if(ui_isshift) strcat(tmp, "\xcd\xb8 ");
                if(ui_isctrl) strcat(tmp, "\xcd\xb9 ");
                if(maps_tool == MAPS_PAINT - MAPS_PIPETTE && ui_isshift && !ui_isctrl)
                    strncat(tmp, lang[MAPS_LINE], 196);
                if(maps_tool >= MAPS_SELBOX - MAPS_PIPETTE && maps_tool <= MAPS_SELFZY - MAPS_PIPETTE) {
                    if(ui_isshift && !ui_isctrl) strncat(tmp, lang[MAPS_SELADD], 196);
                    if(!ui_isshift && ui_isctrl) strncat(tmp, lang[MAPS_SELSUB], 196);
                }
            }
        }
        if(ui_cury > 24)
            ui_status(0, tmp);
    }
}

/**
 * Controller
 */
int maps_ctrl(int tab)
{
    int x, y, i, val = maps_lyrsel.sel.val >= 0 && maps_lyrsel.sel.val < MAPS_NUMLYR - 1 ?
        maps_paint[maps_p[maps_lyrsel.sel.val]].val : -1;

    (void)tab;
    if(popup) return 0;
    switch(event.type) {
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym) {
                case SDLK_c:
                    if(ui_isctrl) { ui_pressed = 39; return 1; }
                break;
                case SDLK_x:
                    if(ui_isctrl) { ui_pressed = 40; return 1; }
                break;
                case SDLK_PAGEUP:
                    if(maps_lyrsel.sel.val < MAPS_NUMLYR) maps_lyrsel.sel.val++; else maps_lyrsel.sel.val = 0;
                    maps_chglyr(NULL);
                    return 1;
                break;
                case SDLK_PAGEDOWN:
                    if(maps_lyrsel.sel.val > 0) maps_lyrsel.sel.val--; else maps_lyrsel.sel.val = MAPS_NUMLYR;
                    maps_chglyr(NULL);
                    return 1;
                break;
            }
            maps_status();
        break;
        case SDL_KEYUP:
            maps_status();
            switch(event.key.keysym.sym) {
                case SDLK_ESCAPE:
                    if(maps_selply) { free(maps_selply); maps_selply = NULL; }
                    maps_sellen = 0;
                break;
                /* history */
                case SDLK_z:
                    if(ui_isctrl) { maps_hist_undo(); ui_pressed = -1; }
                    return 1;
                break;
                case SDLK_y:
                    if(ui_isctrl) { maps_hist_redo(); ui_pressed = -1; }
                    return 1;
                break;
                /* selections, copy'n'paste */
                case SDLK_a:
                    if(ui_isctrl && maps_selmask) {
                        i = project.mapsize * project.mapsize;
                        if(maps_selnum != i) {
                            maps_selnum = i;
                            memset(maps_selmask, 1, i);
                        } else {
                            maps_selnum = 0;
                            memset(maps_selmask, 0, i);
                        }
                    }
                break;
                case SDLK_i:
                    if(ui_isctrl) {
                        if(!maps_selmask) {
                            maps_selmask = (uint8_t*)main_alloc(project.mapsize * project.mapsize);
                            maps_selnum = 0;
                        }
                        for(i = 0; i < project.mapsize * project.mapsize; i++) {
                            maps_selnum += maps_selmask[i] ? -1 : 1;
                            maps_selmask[i] ^= 1;
                        }
                    }
                break;
                case SDLK_c:
                    if(ui_isctrl) { maps_copy(NULL); ui_pressed = -1; }
                    return 1;
                break;
                case SDLK_x:
                    if(ui_isctrl) { maps_cut(NULL); ui_pressed = -1; }
                    return 1;
                break;
                /* flipping */
                case SDLK_v:
                    if(ui_isctrl) maps_paste(NULL);
                    else maps_vflip(NULL);
                    return 1;
                break;
                case SDLK_h:
                    maps_hflip(NULL);
                    return 1;
                break;
                /* select item */
                case SDLK_SPACE:
                    if(ui_curx >= maps_scr.sx && ui_curx < maps_scr.sx + maps_scr.sw &&
                      ui_cury >= maps_scr.sy && ui_cury < maps_scr.sy + maps_scr.sh && maps_lyrsel.sel.val < MAPS_NUMLYR - 1) {
                        ui_pressed = 28 + maps_lyrsel.sel.val;
                        ui_form = maps_form;
                        ui_sprite_picker(ui_form[ui_pressed].x, ui_form[ui_pressed].y, ui_form[ui_pressed].param);
                        return 1;
                    }
                break;
                /* GIMP shortcuts */
                case SDLK_e: maps_erase(NULL); return 1; break;
                case SDLK_o: maps_tool = 0; return 1; break;
                case SDLK_p: case SDLK_n: maps_tool = 1; return 1; break;
                case SDLK_b: maps_tool = 2; return 1; break;
                case SDLK_r: maps_tool = 3; return 1; break;
                case SDLK_f: maps_tool = 4; return 1; break;
                case SDLK_u: maps_tool = 5; return 1; break;
                /* zooming */
                case SDLK_MINUS:
                    ui_scrarea_zoom(&maps_scr, -1);
                    maps_clrmask(NULL);
                    return 1;
                break;
                case SDLK_PLUS:
                case SDLK_EQUALS:
                    ui_scrarea_zoom(&maps_scr, 1);
                    maps_clrmask(NULL);
                    return 1;
                break;
                /* move canvas */
                case SDLK_UP:
                    if(!ui_isshift && !ui_isalt) {
                        if(ui_isctrl) maps_moveup(NULL); else
                        if(ui_curx >= maps_scr.sx && ui_curx < maps_scr.sx + maps_scr.sw &&
                          ui_cury >= maps_scr.sy && ui_cury < maps_scr.sy + maps_scr.sh) {
                            maps_scr.y -= 32;
                            return 1;
                        }
                    }
                break;
                case SDLK_DOWN:
                    if(!ui_isshift && !ui_isalt) {
                        if(ui_isctrl) maps_movedown(NULL); else
                        if(ui_curx >= maps_scr.sx && ui_curx < maps_scr.sx + maps_scr.sw &&
                          ui_cury >= maps_scr.sy && ui_cury < maps_scr.sy + maps_scr.sh) {
                            maps_scr.y += 32;
                            return 1;
                        }
                    }
                break;
                case SDLK_LEFT:
                    if(!ui_isshift && !ui_isalt) {
                        if(ui_isctrl) maps_moveleft(NULL); else
                        if(ui_curx >= maps_scr.sx && ui_curx < maps_scr.sx + maps_scr.sw &&
                          ui_cury >= maps_scr.sy && ui_cury < maps_scr.sy + maps_scr.sh) {
                            maps_scr.x -= 32;
                            return 1;
                        }
                    }
                break;
                case SDLK_RIGHT:
                    if(!ui_isshift && !ui_isalt) {
                        if(ui_isctrl) maps_moveright(NULL); else
                        if(ui_curx >= maps_scr.sx && ui_curx < maps_scr.sx + maps_scr.sw &&
                          ui_cury >= maps_scr.sy && ui_cury < maps_scr.sy + maps_scr.sh) {
                            maps_scr.x += 32;
                            return 1;
                        }
                    }
                break;
                /* debug */
                case SDLK_d:
                    /* dump automap rules */
                    if(ui_isctrl)
                        maps_autodump();
                break;
            }
        break;
        case SDL_MOUSEBUTTONDOWN:
            if(event.button.button == 1 && event.button.x >= maps_scr.sx && event.button.x < maps_scr.sx + maps_scr.sw &&
              event.button.y >= maps_scr.sy && event.button.y < maps_scr.sy + maps_scr.sh) {
                if(maps_tool == MAPS_SELBOX - MAPS_PIPETTE)
                    maps_scr2map(&maps_scr, event.button.x, event.button.y, &maps_selbox.x, &maps_selbox.y);
                maps_scr2map(&maps_scr, event.button.x, event.button.y, &x, &y);
                if(x != -1 && y != -1 && maps_lyrsel.sel.val < MAPS_NUMLYR - 1) {
                    maps_hist_prepare(); maps_clk = 1;
                    switch(maps_tool + MAPS_PIPETTE) {
                        case MAPS_SELBOX: maps_lx = event.button.x; maps_ly = event.button.y; break;
                        case MAPS_PAINT: maps_set(x, y, val); break;
                        case MAPS_FILL: maps_fill(x, y, val); break;
                        case MAPS_PASTE: maps_dopaste(x, y, x, y); break;
                    }
                }
            }
        break;
        case SDL_MOUSEMOTION:
            if(ui_curbtn == 1) {
                if(event.motion.y > maps_form[25].y && event.motion.y < maps_form[25].y + 8 &&
                  event.motion.x > maps_form[25].x + 8 && event.motion.x < maps_form[25].x + maps_form[25].w - 8) {
                    maps_scr.y -= 32;
                }
                if(event.motion.y > maps_form[25].y + maps_form[25].h - 8 && event.motion.y < maps_form[25].y + maps_form[25].h &&
                  event.motion.x > maps_form[25].x + 8 && event.motion.x < maps_form[25].x + maps_form[25].w - 8) {
                    maps_scr.y += 32;
                }
                if(event.motion.y > maps_form[25].y + 8 && event.motion.y < maps_form[25].y + maps_form[25].h - 8&&
                  event.motion.x > maps_form[25].x && event.motion.x < maps_form[25].x + 8) {
                    maps_scr.x -= 32;
                }
                if(event.motion.y > maps_form[25].y + 8 && event.motion.y < maps_form[25].y + maps_form[25].h - 8&&
                  event.motion.x > maps_form[25].x + maps_form[25].w - 8 && event.motion.x < maps_form[25].x + maps_form[25].w) {
                    maps_scr.x += 32;
                }
                if(!ui_isshift && maps_clk && maps_lyrsel.sel.val < MAPS_NUMLYR - 1) {
                    maps_scr2map(&maps_scr, event.motion.x, event.motion.y, &x, &y);
                    if(x != -1 && y != -1)
                        switch(maps_tool + MAPS_PIPETTE) {
                            case MAPS_PAINT: maps_set(x, y, val); break;
                            case MAPS_PASTE: maps_dopaste(x, y, x, y); break;
                        }
                }
            }
            maps_status();
        break;
        case SDL_MOUSEBUTTONUP:
            if(maps_clk) {
                if(event.button.x >= maps_scr.sx && event.button.x < maps_scr.sx + maps_scr.sw &&
                  event.button.y >= maps_scr.sy && event.button.y < maps_scr.sy + maps_scr.sh) {
                    maps_scr2map(&maps_scr, event.button.x, event.button.y, &x, &y);
                    if(x != -1 && y != -1)
                        switch(maps_lyrsel.sel.val == MAPS_LYR_PATHS - MAPS_LYR_GRD1 ? MAPS_SELPLY : maps_tool + MAPS_PIPETTE) {
                            case MAPS_PIPETTE:
                                if(event.button.button == 1 && maps_lyrsel.sel.val < MAPS_NUMLYR - 1)
                                    maps_set(x, y, 0);
                                maps_tool = MAPS_PAINT - MAPS_PIPETTE;
                                return 1;
                            break;
                            case MAPS_PAINT:
                                if(event.button.button == 1 && maps_lyrsel.sel.val < MAPS_NUMLYR - 1) {
                                    if(ui_isshift && maps_selline.x != -1)
                                        maps_line(maps_selline.x, maps_selline.y, x, y, val);
                                    maps_selline.x = x; maps_selline.y = y;
                                }
                            break;
                            case MAPS_SELBOX:
                                if(event.button.button == 1) {
                                    maps_lx -= event.button.x; if(maps_lx < 0) maps_lx = -maps_lx;
                                    maps_ly -= event.button.y; if(maps_ly < 0) maps_ly = -maps_ly;
                                    maps_select_box(maps_selbox.x, maps_selbox.y, x, y, maps_lx < 4 && maps_ly < 4);
                                }
                            break;
                            case MAPS_SELFZY:
                                if(event.button.button == 1)
                                    maps_select_fuzzy(x, y);
                            break;
                            case MAPS_SELPLY:
                                if(event.button.button != 1) {
                                    if(maps_sellen) {
                                        maps_sellen--;
                                        if(!maps_sellen) { free(maps_selply); maps_selply = NULL; }
                                    }
                                } else {
                                    if(maps_selply && x == maps_selply[0].x && y == maps_selply[0].y) {
                                        if(maps_lyrsel.sel.val == MAPS_LYR_PATHS - MAPS_LYR_GRD1)
                                            maps_addpath();
                                        else {
                                            maps_select_poly();
                                            free(maps_selply);
                                        }
                                        maps_selply = NULL;
                                        maps_sellen = 0;
                                    } else
                                    if(x != -1 && y != -1) {
                                        i = maps_sellen++;
                                        maps_selply = (SDL_Point*)realloc(maps_selply, maps_sellen * sizeof(SDL_Point));
                                        if(maps_selply) { maps_selply[i].x = x; maps_selply[i].y = y; }
                                        else maps_sellen = 0;
                                    }
                                }
                            break;
                            case MAPS_PASTE:
                                if(event.button.button == 1 && maps_lyrsel.sel.val < MAPS_NUMLYR - 1) {
                                    if(ui_isshift && maps_selline.x != -1)
                                        maps_dopaste(maps_selline.x, maps_selline.y, x, y);
                                    maps_selline.x = x; maps_selline.y = y;
                                }
                            break;
                        }
                }
                maps_hist_commit();
                maps_selbox.x = maps_selbox.y = -1; maps_clk = 0;
            }
        break;
        case SDL_MOUSEWHEEL:
            if(ui_curx >= maps_scr.sx && ui_curx < maps_scr.sx + maps_scr.sw &&
              ui_cury >= maps_scr.sy && ui_cury < maps_scr.sy + maps_scr.sh) {
                if(ui_isctrl) {
                    if(event.wheel.y < 0) ui_scrarea_zoom(&maps_scr, -1);
                    if(event.wheel.y > 0) ui_scrarea_zoom(&maps_scr, 1);
                    maps_clrmask(NULL);
                } else {
                    if(event.wheel.y < 0) maps_scr.y += maps_th;
                    if(event.wheel.y > 0) maps_scr.y -= maps_th;
                    if(event.wheel.x < 0) maps_scr.x += maps_tw;
                    if(event.wheel.x > 0) maps_scr.x -= maps_tw;
                    if(maps_scr.x + maps_scr.sw > maps_scr.w) maps_scr.x = maps_scr.w - maps_scr.sw;
                    if(maps_scr.x < 0) maps_scr.x = 0;
                    if(maps_scr.y + maps_scr.sh > maps_scr.h) maps_scr.y = maps_scr.h - maps_scr.sh;
                    if(maps_scr.y < 0) maps_scr.y = 0;
                }
                return 1;
            }
        break;
    }
    return 0;
}

/**
 * Erase parallax from list
 */
void maps_erasepar(void *data)
{
    maps_par_t *list = (maps_par_t*)maps_partbl.data;

    (void)data;
    if(!list || maps_partbl.val < 0 || maps_partbl.val >= maps_partbl.num) return;
    memcpy(&list[maps_partbl.val], &list[maps_partbl.val + 1], (maps_partbl.num - maps_partbl.val) * sizeof(maps_par_t));
    maps_partbl.num--;
    if(maps_partbl.val >= maps_partbl.num)
        maps_partbl.val = maps_partbl.num - 1;
    if(!maps_partbl.num) { free(list); maps_partbl.data = NULL; }
}

/**
 * Add parallax to list
 */
void maps_addpar(void *data)
{
    maps_par_t *list = (maps_par_t*)maps_partbl.data;
    int i;

    (void)data;
    if(maps_parspr.val < 0) return;
    list = (maps_par_t*)realloc(list, (maps_partbl.num + 1) * sizeof(maps_par_t));
    maps_partbl.data = list;
    if(!list) main_error(ERR_MEM);
    i = maps_partbl.num++;
    list[i].bg = maps_partype.val;
    list[i].sx = maps_parsx.val;
    list[i].dx = maps_pardx.val;
    list[i].sy = maps_parsy.val;
    list[i].dy = maps_pardy.val;
    list[i].tm = maps_partm.val;
    list[i].spr = maps_parspr.val;
    list[i].x = list[i].y = list[i].last = 0;
    maps_parspr.val = -1;
}

/**
 * Click on a parallax record
 */
void maps_clkpar(void *data)
{
    maps_par_t *list = (maps_par_t*)maps_partbl.data;

    (void)data;
    if(!list || maps_partbl.val < 0 || maps_partbl.val >= maps_partbl.num) return;
    if(ui_curx > ui_form[15].x && ui_curx < ui_form[15].x + maps_partbl.hdr[0].w) {
        list[maps_partbl.val].bg ^= 1;
    }
    if(ui_curx > ui_form[15].x + maps_partbl.hdr[0].w && ui_curx < ui_form[15].x + maps_partbl.hdr[0].w +
      maps_partbl.hdr[1].w / 2) {
        if(ui_curbtn == 1 && list[maps_partbl.val].sx < maps_parsx.max) list[maps_partbl.val].sx++;
        if(ui_curbtn != 1 && list[maps_partbl.val].sx > maps_parsx.min) list[maps_partbl.val].sx--;
    }
    if(ui_curx > ui_form[15].x + maps_partbl.hdr[0].w + maps_partbl.hdr[1].w / 2 && ui_curx < ui_form[15].x +
      maps_partbl.hdr[0].w + maps_partbl.hdr[1].w) {
        if(ui_curbtn == 1 && list[maps_partbl.val].dx < maps_pardx.max) list[maps_partbl.val].dx++;
        if(ui_curbtn != 1 && list[maps_partbl.val].dx > maps_pardx.min) list[maps_partbl.val].dx--;
    }
    if(ui_curx > ui_form[15].x + maps_partbl.hdr[0].w + maps_partbl.hdr[1].w &&
      ui_curx < ui_form[15].x + maps_partbl.hdr[0].w + maps_partbl.hdr[1].w + maps_partbl.hdr[2].w / 2) {
        if(ui_curbtn == 1 && list[maps_partbl.val].sy < maps_parsy.max) list[maps_partbl.val].sy++;
        if(ui_curbtn != 1 && list[maps_partbl.val].sy > maps_parsy.min) list[maps_partbl.val].sy--;
    }
    if(ui_curx > ui_form[15].x + maps_partbl.hdr[0].w + maps_partbl.hdr[1].w + maps_partbl.hdr[2].w / 2 &&
      ui_curx < ui_form[15].x + maps_partbl.hdr[0].w + maps_partbl.hdr[1].w + maps_partbl.hdr[2].w) {
        if(ui_curbtn == 1 && list[maps_partbl.val].dy < maps_pardy.max) list[maps_partbl.val].dy++;
        if(ui_curbtn != 1 && list[maps_partbl.val].dy > maps_pardy.min) list[maps_partbl.val].dy--;
    }
    if(ui_curx > ui_form[15].x + maps_partbl.hdr[0].w + maps_partbl.hdr[1].w + maps_partbl.hdr[2].w &&
      ui_curx < ui_form[15].x + maps_partbl.hdr[0].w + maps_partbl.hdr[1].w + maps_partbl.hdr[2].w + maps_partbl.hdr[3].w) {
        if(ui_curbtn == 1 && list[maps_partbl.val].tm < maps_partm.max) list[maps_partbl.val].tm++;
        if(ui_curbtn != 1 && list[maps_partbl.val].tm > maps_partm.min) list[maps_partbl.val].tm--;
    }
}

/**
 * Clear form
 */
void maps_clrform(void *data)
{
    int i;

    (void)data;
    for(i = 0; i < 6; i++) maps_neightbors[i] = -1;
    for(i = 0; i < (int)(sizeof(maps_paint)/sizeof(maps_paint[0])); i++) maps_paint[i].val = -1;
    maps_new(NULL);
    maps_layers = MAP_MASK_GRD1;
    maps_lyrsel.sel.val = 0;
    maps_tool = 1;
    ui_scrarea_zoom(&maps_scr, 0);
    maps_clrmask(NULL);
}

/**
 * New map
 */
void maps_new(void *data)
{
    (void)data;
    maps_freetmx();
    maps_name[0] = maps_title[0] = 0;
    maps_map = (uint16_t*)main_alloc(project.mapsize * project.mapsize * (MAPS_NUMLYR - 1) * sizeof(uint16_t));
    memset(maps_map, 0xff, project.mapsize * project.mapsize * (MAPS_NUMLYR - 1) * sizeof(uint16_t));
    maps_selmask = (uint8_t*)main_alloc(project.mapsize * project.mapsize);
    memset(maps_neightstrs, 0, sizeof(maps_neightstrs));
}

/**
 * Delete map
 */
void maps_delete(void *data)
{
    int *list = (int*)maps_tbl.data;

    (void)data;
    if(maps_tbl.val < 0 || !list) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.tmx", project.id, project_dirs[PROJDIRS_MAPS], project.maps[list[maps_tbl.val]]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("maps_delete: removing %s\n", projdir);
        remove(projdir);
        maps_new(NULL);
        maps_init(SUBMENU_MAPS);
    }
}

/**
 * Save map
 */
void maps_save(void *data)
{
    uint16_t *map;
    ui_sprite_t *s;
    maps_par_t *par = (maps_par_t*)maps_partbl.data;
    int i, j, k, l, n, x, y, *list = (int*)maps_tbl.data, cat;
    char *quoted, tmp[16], *c;
    FILE *f;

    (void)data;
    if(!maps_name[0]) return;
    f = project_savefile(!list || maps_tbl.val < 0 || strcmp(project.maps[list[maps_tbl.val]], maps_name),
        project_dirs[PROJDIRS_MAPS], maps_name, "tmx", PROJMAGIC_MAPTMX, "maps_save");
    if(f) {
        quoted = (char*)main_alloc(2 * strlen(maps_title) + 1);
        project_escapestr(maps_title, quoted);
        for(i = n = 0; i < 99; i++)
            if(maps_paths[i] && maps_pathlen[i]) n = i + 1;
        fprintf(f, "<map version=\"1.5\" orientation=\"%s\" %srenderorder=\"left-down\" compressionlevel=\"0\" "
            "width=\"%u\" height=\"%u\" tilewidth=\"%u\" tileheight=\"%u\" infinite=\"0\" "
            "nextlayerid=\"%u\" nextobjectid=\"%u\">\r\n <properties>\r\n  <property name=\"title\" value=\"%s\"/>\r\n",
            maps_tmxorient[project.type], maps_tmxorientextra[project.type], project.mapsize, project.mapsize, project.tilew,
            project.tileh, MAPS_NUMLYR, n + 1, quoted);
        free(quoted);
        for(i = 0; i < 6; i++)
            if(maps_tmxdirs[i] && maps_neightbors[i] >= 0 && strcmp(project.maps[maps_neightbors[i]], maps_name)) {
                fprintf(f, "  <property name=\"%s\" value=\"", maps_tmxdirs[i]);
                project_wridx(f, maps_neightbors[i], project.maps);
                fprintf(f, "\"/>\r\n");
            }
        for(i = 0; i < 6; i++)
            if(maps_tmxdirs[i] && maps_neightstrs[i] && *maps_neightstrs[i] && strcmp(maps_neightstrs[i], maps_name)) {
                fprintf(f, "  <property name=\"%s\" value=\"%s\"/>\r\n", maps_tmxdirs[i], maps_neightstrs[i]);
            }
        if(maps_dayspr.val >= 0) {
            fprintf(f, "  <property name=\"daylight\" value=\"%u ", maps_daylen.val);
            project_wrsprite(f, &maps_dayspr);
            fprintf(f, "\"/>\r\n");
        }
        if(maps_backmus.val >= 0) {
            fprintf(f, "  <property name=\"music\" value=\"");
            project_wridx(f, maps_backmus.val, project.music);
            fprintf(f, "\"/>\r\n");
        }
        if(par)
            for(i = 0; i < maps_partbl.num; i++) {
                fprintf(f, "  <property name=\"parallax\" value=\"%u %d %d %d %d %u ", par[i].bg, par[i].sx, par[i].dx,
                    par[i].sy, par[i].dy, par[i].tm);
                project_wrsprite2(f, par[i].spr, maps_parspr.cat);
                fprintf(f, "\"/>\r\n");
            }
        fprintf(f, " </properties>\r\n");
        /* generate local indeces for tiles */
        for(k = 0; k < (int)(sizeof(maps_paint)/sizeof(maps_paint[0])); k++)
            for(i = 0; i < project.spritenum[maps_paint[k].cat]; i++)
                project.sprites[maps_paint[k].cat][i].idx = 0;
        /* clear ground 2 where it matches ground 1, or both ground 1 and 2 if it matches objects layer */
        for(map = maps_map, i = 0, k = project.mapsize * project.mapsize; i < k; i++) {
            if(map[i] == map[i + k + k]) map[i] = 0xffff;
            if(map[i + k] == map[i + k + k]) map[i + k] = 0xffff;
            if(map[i] == map[i + k]) map[i + k] = 0xffff;
        }

        for(map = maps_map, l = k = 0; k < MAPS_NUMLYR - 1; k++)
            for(j = 0; j < project.mapsize; j++)
                for(i = 0; i < project.mapsize; i++, map++) {
                    cat = maps_paint[maps_p[k]].cat;
                    if(*map == 0xffff || *map >= project.spritenum[cat]) continue;
                    if(!project.sprites[cat][(int)*map].idx &&
                      project.sprites[cat][(int)*map].dir[0].data) {
                        project.sprites[cat][(int)*map].idx = ++l;
                        s = &project.sprites[cat][(int)*map].dir[0];
                        /* we have animated sprites, but Tiled does not support those in any sane way, so we save each
                         * sprite in a separate "tileset" with only the first frame. Also needed for other workarounds. */
                        fprintf(f, " <tileset firstgid=\"%u\" name=\"%s\" tilewidth=\"%u\" tileheight=\"%u\" tilecount=\"1\" "
                            " columns=\"1\" objectalignment=\"bottomleft\">\r\n", l,
                            project.sprites[cat][(int)*map].name, s->w, s->h);
                        spr_wrname(cat, *map, 0, tmp); tmp[4] = 0;
                        /* workaround a Tiled bug: bottom alignment should be bottom center, but it is the same as bottomleft
                         * for some reason, so we explicitly set bottomleft and add a tileoffset manually */
                        if(s->w != project.tilew)
                            fprintf(f, "  <tileoffset x=\"%d\"/>\r\n", (project.tilew - s->w) / 2);
                        /* another Tiled bug: does not support tile atlases... no comment. For now we always use the sprite
                         * file, and just put the atlas reference in an XML comment to avoid Tiled misbehave. */
#if TILED_ATLASFIXED
                        if(project.sprites[cat][(int)*map].x == -1) {
#endif
                            fprintf(f, "  <image source=\"../%s/%s_%s.png\" width=\"%u\" height=\"%u\"/>\r\n",
                                cat < PROJ_NUMSPRCATS - 2 ? project_dirs[PROJDIRS_SPRITES + cat] : project_dirs[PROJDIRS_NPCS],
                                project.sprites[cat][(int)*map].name, tmp, s->nframe * s->w, s->h);
#if TILED_ATLASFIXED
                        } else {
#else
                        if(project.sprites[cat][(int)*map].x != -1) {
#endif
                            for(c = project.sprites[cat][(int)*map].name; *c && *c != '_'; c++);
                            if(*c == '_') *c = 0; else c = NULL;
                            fprintf(f,
#if TILED_ATLASFIXED
                                "  <image source=\"../%s/%s_atls.png\"/>"
                                "<tile id=\"%u\" x=\"%u\" y=\"%u\" width=\"%u\" height=\"%u\"/>\r\n",
#else
                                "  <!--image source=\"../%s/%s_atls.png\"/>"
                                "<tile id=\"%u\" x=\"%u\" y=\"%u\" width=\"%u\" height=\"%u\"/-->\r\n",
#endif
                                cat < PROJ_NUMSPRCATS - 2 ? project_dirs[PROJDIRS_SPRITES + cat] : project_dirs[PROJDIRS_NPCS],
                                project.sprites[cat][(int)*map].name, l,
                                project.sprites[cat][(int)*map].x, project.sprites[cat][(int)*map].y, s->w, s->h);
                            if(c) *c = '_';
                        }
                        fprintf(f, " </tileset>\r\n");
                    }
                }
        /* print out layers */
        for(map = maps_map, l = k = 0; k < MAPS_NUMLYR - 1; k++) {
            quoted = (char*)main_alloc(2 * strlen(lang[MAPS_LYR_GRD1 + k]) + 1);
            project_escapestr(lang[MAPS_LYR_GRD1 + k], quoted);
            fprintf(f, " <layer id=\"%u\" name=\"%s\" width=\"%u\" height=\"%u\">\r\n  <data encoding=\"csv\">\r\n", k + 1,
                quoted, project.mapsize, project.mapsize);
            free(quoted);
            cat = maps_paint[maps_p[k]].cat;
            for(j = 0; j < project.mapsize; j++) {
                for(i = 0; i < project.mapsize; i++, map++) {
                    if(*map == 0xffff || *map >= project.spritenum[cat]) l = 0;
                    else l = project.sprites[cat][(int)*map].idx;
                    fprintf(f, "%s%u", i ? "," : "", l);
                }
                fprintf(f, "%s\r\n", j < project.mapsize - 1 ? "," : "");
            }
            fprintf(f, "  </data>\r\n </layer>\r\n");
        }
        /* print out paths */
        if(n > 0) {
            quoted = (char*)main_alloc(2 * strlen(lang[MAPS_LYR_GRD1 + MAPS_NUMLYR]) + 1);
            project_escapestr(lang[MAPS_LYR_GRD1 + MAPS_NUMLYR], quoted);
            fprintf(f, " <objectgroup id=\"%u\" name=\"%s\">\r\n", MAPS_NUMLYR, quoted);
            free(quoted);
            for(k = n = l = j = 0; k < 99; k++)
                if(maps_paths[k] && maps_pathlen[k]) {
                    for(i = 0; i < maps_pathlen[k]; i++) {
                        /* do not use map2scr, we must save zoom independent values */
                        x = y = 0;
                        switch(project.type) {
                            case TNG_TYPE_ORTHO:
                                x = maps_paths[k][i].x * project.tilew + project.tilew / 2;
                                y = maps_paths[k][i].y * project.tileh + project.tileh / 2;
                            break;
                            case TNG_TYPE_ISO:
                                /* workaround a Tiled bug with coordinates... These aren't iso coordinates */
                                x = maps_paths[k][i].x * project.tilew / 2;
                                y = maps_paths[k][i].y * project.tileh + project.tileh / 2;
                            break;
                            case TNG_TYPE_HEXV:
                            break;
                            case TNG_TYPE_HEXH:
                            break;
                        }
                        if(!i) {
                            l = x; j = y;
                            fprintf(f, "  <object id=\"%u\" name=\"%u\" x=\"%d\" y=\"%d\">\r\n"
                                "   <polygon points=\"0,0", ++n, k + 1, x, y);
                        } else
                            fprintf(f, " %d,%d", x - l, y - j);
                    }
                    fprintf(f, "\"/>\r\n  </object>\r\n");
                }
            fprintf(f, " </objectgroup>\r\n");
        }
        fprintf(f, "</map>\r\n");
        fclose(f);
        project_freedir(&project.maps, &project.nummaps);
        sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_MAPS]);
        project.maps = project_getdir(projdir, ".tmx", &project.nummaps);
        maps_tbl.data = realloc(maps_tbl.data, (project.nummaps + 1) * sizeof(int));
        ui_input_buf[0] = maps_srch[0] = 0;
        maps_search(NULL);
        for(maps_tbl.val = 0; maps_tbl.val < maps_tbl.num &&
            strcmp(maps_name, project.maps[maps_tbl.val]); maps_tbl.val++);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.tmx", project.id, project_dirs[PROJDIRS_MAPS], maps_name);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVEMAP]);
}

/**
 * Load map
 */
void maps_load(void *data)
{
    int *list = (int*)maps_tbl.data;

    (void)data;
    if(maps_tbl.val < 0 || !list) return;
    maps_new(NULL);
    if(!maps_loadtmx(project.maps[list[maps_tbl.val]], maps_map, maps_neightbors, 0))
        ui_status(1, lang[ERR_LOADMAP]);
    else
        strncpy(maps_name, project.maps[list[maps_tbl.val]], sizeof(maps_name) - 1);
    maps_meta = 0;
}

/**
 * RFC4648 base64 decoder. Decode in place, return out length
 */
int base64_decode(char *s)
{
    char *out = s, *ret = s;
    int b = 0, c = 0, d;
    while(*s && *s != '=' && *s != '<') {
        while(*s && (*s == ' ' || *s == '\r' || *s == '\n')) s++;
        if(!*s) break;
        if(*s >= 'A' && *s <= 'Z') d = *s - 'A'; else
        if(*s >= 'a' && *s <= 'z') d = *s - 'a' + 26; else
        if(*s >= '0' && *s <= '9') d = *s - '0' + 52; else
        if(*s == '+' || *s == '-' || *s == '.') d = 62; else
        if(*s == '/' || *s == '_') d = 63; else
            continue;
        b += d; c++; s++;
        if(c == 4) {
            *out++ = (b >> 16);
            *out++ = ((b >> 8) & 0xff);
            *out++ = (b & 0xff);
            b = c = 0;
        } else {
            b <<= 6;
        }
    }
    switch(c) {
        case 1: return 0;
        case 2: *out++ = (b >> 10); break;
        case 3: *out++ = (b >> 16); *out++ = ((b >> 8) & 0xff); break;
    }
    *out = 0;
    return (int)(out - ret);
}

/**
 * Load one tmx into a specified area
 */
int maps_loadtmx(char *fn, uint16_t *maps, int *neightbors, int onlymap)
{
    ZSTD_DCtx* zcmp;
    ZSTD_inBuffer zi;
    ZSTD_outBuffer zo;
    int stripe, version = 0, type = 0, tw = project.tilew, th = project.tileh, w, h, m = 0, n, i, j, k, l, o;
    int numtiles = 0, cat;
    maps_tile_t *tiles = NULL;
    uint16_t *map = NULL;
    uint32_t *data, *buff;
    char *str, *s, *v, *e, *name;

    if(!maps || maps == maps_map) {
        if(!maps_map) maps_new(NULL);
        map = maps_map;
        stripe = project.mapsize;
    } else {
        map = maps;
        stripe = (onlymap == 2 ? 1 : 3) * project.mapsize;
    }
    if(onlymap == 2) onlymap = 0;
    if(!map || !fn || !*fn) return 0;
    str = project_loadfile(project_dirs[PROJDIRS_MAPS], fn, "tmx", PROJMAGIC_MAPTMX, "maps_loadtmx");
    if(str) {
        s = project_skipnl(str);
        if(memcmp(s, "<map", 4)) {
            free(str);
            return 0;
        }
        /* clear map */
        for(k = 0; k < MAPS_NUMLYR - 1; k++)
            for(j = 0; j < project.mapsize; j++)
                for(i = 0; i < project.mapsize; i++)
                    map[k * stripe * stripe + j * stripe + i] = 0xffff;
        if(!onlymap) { maps_daylen.val = 0; maps_dayspr.val = maps_backmus.val = -1; maps_title[0] = 0; }
        for(i = 0; i < 99; i++)
            if(maps_paths[i]) free(maps_paths[i]);
        memset(maps_paths, 0, sizeof(maps_paths));
        memset(maps_pathlen, 0, sizeof(maps_pathlen));
        if(!onlymap) maps_layers = 0;
        /* parse <map> tag for version, should they ever fix that polygon coordinate bug in tmx */
        for(; *s && *s != '>'; s++) {
            if(!memcmp(s, " version=", 9)) {
                s += 10;
                version = atoi(s) << 8;
                while(*s && s[-1] != '.') s++;
                version |= atoi(s) & 0xff;
            }
            if(!memcmp(s, " orientation=", 13)) s = project_getidx(s + 14, &type, (char**)maps_tmxorient, 0);
            if(!memcmp(s, " staggeraxis=", 13)) { s += 14; if(type == TNG_TYPE_HEXV && *s == 'x') type = TNG_TYPE_HEXH; }
            if(!memcmp(s, " tilewidth=", 11)) { s += 12; tw = atoi(s); }
            if(!memcmp(s, " tileheight=", 12)) { s += 13; th = atoi(s); }
        }
        if(tw < 1) tw = 1;
        if(th < 1) th = 1;
        if(verbose > 2) printf("  map version %d.%d type %d tilew %d tileh %d\n", version >> 8, version & 0xff, type, tw, th);
        /* parse properties */
        while(*s && memcmp(s, "</", 2)) {
            s = project_skipnl(s);
            while(*s == ' ' || *s == '\t') s++;
            if(!*s || *s != '<') break;
            if(!memcmp(s, "<property name=\"", 16)) {
                s += 16; for(v = s; *v && *v != '>' && memcmp(v, " value=\"", 8); v++);
                if(!*v) break;
                if(*v == '>') continue;
                v += 8;
                if(verbose > 2) {
                    for(e = s; *e && *e != '\"'; e++);
                    if(*e == '\"') {
                        *e = 0; printf("  property '%s' ", s); *e = '\"';
                        for(e = v; *e && *e != '\"'; e++);
                        if(*e == '\"') { *e = 0; printf(" value '%s'\n", v); *e = '\"'; }
                    }
                }
                if(!onlymap) {
                    if(!memcmp(s, "title\"", 6)) {
                        s = project_getstr2(v - 1, maps_title, 4, sizeof(maps_title));
                    } else
                    if(!memcmp(s, "music\"", 6)) {
                        s = project_getidx(v, &maps_backmus.val, project.music, -1);
                    } else
                    if(!memcmp(s, "daylight\"", 9)) {
                        s = project_getint(v, &maps_daylen.val, maps_daylen.min, maps_daylen.max);
                        s = project_getsprite(s, &maps_dayspr);
                    } else
                    if(!memcmp(s, "parallax\"", 9)) {
                        s = project_getint(v, &maps_partype.val, 0, 1);
                        s = project_getint(s, &maps_parsx.val, maps_parsx.min, maps_parsx.max);
                        s = project_getint(s, &maps_pardx.val, maps_pardx.min, maps_pardx.max);
                        s = project_getint(s, &maps_parsy.val, maps_parsy.min, maps_parsy.max);
                        s = project_getint(s, &maps_pardy.val, maps_pardy.min, maps_pardy.max);
                        s = project_getint(s, &maps_partm.val, maps_partm.min, maps_partm.max);
                        s = project_getsprite(s, &maps_parspr);
                        maps_addpar(NULL);
                        maps_partype.val = maps_parsx.val = maps_pardx.val = maps_parsy.val = maps_pardy.val = maps_partm.val = 0;
                        maps_parspr.val = -1;
                    }
                }
                if(neightbors) {
                    project_getidx(s, &n, maps_tmxdirs, -1);
                    if(n >= 0 && n < 6)
                        s = project_getidx(v, &neightbors[n], project.maps, -1);
                }
            }
        }
        /* parse tiles */
        while(*s && memcmp(s, "<layer", 6) && memcmp(s, "<object", 7) && memcmp(s, "</map", 5)) {
            s = project_skipnl(s);
            while(*s == ' ' || *s == '\t') s++;
            if(!*s || *s != '<') break;
            if(!memcmp(s, "<tileset", 8)) {
                n = -1; name = NULL;
                for(s += 8; *s && memcmp(s, "</tileset", 9); s++) {
                    if(!memcmp(s, " firstgid=", 10)) { s += 11; n = atoi(s); }
                    if(!memcmp(s, " name=", 6)) { s += 7; name = s; }
                    /* we can't and should not store category and sprite id directly, so parse only the name to figure out */
                    if(n > 0 && name) {
                        if(n >= numtiles) {
                            tiles = (maps_tile_t*)realloc(tiles, (n + 1) * sizeof(maps_tile_t));
                            if(!tiles) main_error(ERR_MEM);
                            for(; numtiles < n + 1; numtiles++) {
                                tiles[numtiles].id = tiles[numtiles].cat = 0xffff;
                                tiles[numtiles].name = NULL;
                            }
                        }
                        for(j = 0; name[j] && name[j] != '\r' && name[j] != '\n' && name[j] != '\"'; j++);
                        if(j > 0) {
                            if(tiles[n].name) free(tiles[n].name);
                            tiles[n].name = (char*)main_alloc(j + 1);
                            memcpy(tiles[n].name, name, j);
                        }
                        for(; *s && memcmp(s, "</tileset", 9); s++);
                        break;
                    }
                }
            }
        }
        /* compensate for the lack of closing tag */
        while(s > str && *s != '\n') s--;
        /* parse layers */
        n = -1; w = project.mapsize; h = project.mapsize;
        while(*s && memcmp(s, "<object", 7) && memcmp(s, "</map", 5)) {
            s = project_skipnl(s);
            while(*s == ' ' || *s == '\t') s++;
            if(!*s) break;
            if(!memcmp(s, "<layer", 6)) {
                n = -1; w = project.mapsize; h = project.mapsize;
                for(; *s && *s != '>'; s++) {
                    if(!memcmp(s, " id=", 4)) { s += 5; n = atoi(s) - 1; }
                    if(!memcmp(s, " width=", 7)) { s += 8; w = atoi(s); }
                    if(!memcmp(s, " height=", 8)) { s += 9; h = atoi(s); }
                }
                if(!*s) break;
            }
            if(n >= 0 && n < MAPS_NUMLYR - 1 && w > 0 && h > 0 && tiles && !memcmp(s, "<data", 5)) {
                for(i = j = o = 0; *s && *s != '>'; s++) {
                    if(!memcmp(s, " encoding=\"csv\"", 15)) o = 1; else
                    if(!memcmp(s, " encoding=\"base64\"", 18)) o = 2; else
                    if(!memcmp(s, " compression=\"gzip\"", 19)) o = 3; else
                    if(!memcmp(s, " compression=\"zlib\"", 19)) o = 4; else
                    if(!memcmp(s, " compression=\"zstd\"", 19)) o = 5;
                }
                if(!*s) break;
                s++; buff = NULL;
                if(verbose > 2) printf("  layer %d w %d h %d encoding %d (%s)\n", n + 1, w, h, o, lang[MAPS_LYR_GRD1 + n]);
                switch(o) {
                    case 0: /* XML */
                        while(*s && memcmp(s, "</data", 6) && j < h) {
                            s = project_skipnl(s);
                            while(*s == ' ' || *s == '\t') s++;
                            if(!*s || *s != '<') goto brk;
                            if(!memcmp(s, "<tile gid=\"", 11)) {
                                s += 11; k = atoi(s);
                                if(k > 0 && k < numtiles && n >= 0 && n < MAPS_NUMLYR - 1 &&
                                  i < project.mapsize && j < project.mapsize) {
                                    if(!onlymap) maps_layers |= (1 << n);
                                    cat = maps_paint[maps_p[n]].cat;
                                    if(tiles[k].cat != cat) {
                                        tiles[k].cat = cat; tiles[k].id = 0xffff;
                                    }
                                    if(tiles[k].id == 0xffff && tiles[k].name) {
                                        for(l = 0; l < project.spritenum[cat] &&
                                            strcmp(tiles[k].name, project.sprites[cat][l].name); l++);
                                        if(l < project.spritenum[cat]) {
                                            tiles[k].id = l;
                                            if(verbose > 2)
                                                printf("  tile %d cat %d sprite %d %s\n", k, cat, l, tiles[k].name);
                                        }
                                    }
                                    map[n * stripe * stripe + j * stripe + i] = tiles[k].id;
                                }
                            }
                            i++; if(i >= w) { i = 0; j++; }
                        }
                    break;
                    case 1: /* CSV */
                        while(*s && *s != '<' && j < h) {
                            while(*s && (*s == ' ' || *s == '\r' || *s == '\n' || *s == ',')) s++;
                            if(!*s) goto brk;
                            k = atoi(s); while(*s && *s >= '0' && *s <= '9') s++;
                            if(k > 0 && k < numtiles && n >= 0 && n < MAPS_NUMLYR - 1 &&
                              i < project.mapsize && j < project.mapsize) {
                                if(!onlymap) maps_layers |= (1 << n);
                                cat = maps_paint[maps_p[n]].cat;
                                if(tiles[k].cat != cat) {
                                    tiles[k].cat = cat; tiles[k].id = 0xffff;
                                }
                                if(tiles[k].id == 0xffff && tiles[k].name) {
                                    for(l = 0; l < project.spritenum[cat] &&
                                        strcmp(tiles[k].name, project.sprites[cat][l].name); l++);
                                    if(l < project.spritenum[cat]) {
                                        tiles[k].id = l;
                                        if(verbose > 2)
                                            printf("  tile %d cat %d sprite %d %s\n", k, cat, l, tiles[k].name);
                                    }
                                }
                                map[n * stripe * stripe + j * stripe + i] = tiles[k].id;
                            }
                            i++; if(i >= w) { i = 0; j++; }
                        }
                    break;
                    case 2: /* base64 */
                        while(*s && (*s == ' ' || *s == '\r' || *s == '\n')) s++;
                        if(!*s) goto brk;
                        for(v = s; *s && *s != '<'; s++);
                        base64_decode(v);
                        data = (uint32_t*)v;
readuints:              for(i = j = 0; j < h && data && (char*)data < s; data++) {
                            k = (int)(*data);
                            if(k > 0 && k < numtiles && n >= 0 && n < MAPS_NUMLYR - 1 &&
                              i < project.mapsize && j < project.mapsize) {
                                if(!onlymap) maps_layers |= (1 << n);
                                cat = maps_paint[maps_p[n]].cat;
                                if(tiles[k].cat != cat) {
                                    tiles[k].cat = cat; tiles[k].id = 0xffff;
                                }
                                if(tiles[k].id == 0xffff && tiles[k].name) {
                                    for(l = 0; l < project.spritenum[cat] &&
                                        strcmp(tiles[k].name, project.sprites[cat][l].name); l++);
                                    if(l < project.spritenum[cat]) {
                                        tiles[k].id = l;
                                        if(verbose > 2)
                                            printf("  tile %d cat %d sprite %d %s\n", k, cat, l, tiles[k].name);
                                    }
                                }
                                map[n * stripe * stripe + j * stripe + i] = tiles[k].id;
                            }
                            i++; if(i >= w) { i = 0; j++; }
                        }
                    break;
                    case 3: /* gzip? */
                    case 4: /* zlib */
                        while(*s && (*s == ' ' || *s == '\r' || *s == '\n')) s++;
                        if(!*s) goto brk;
                        for(v = s; *s && *s != '<'; s++);
                        i = base64_decode(v);
                        /* makes no sense... there's no gzip compression, uses zlib, just with a different header */
                        if(((uint8_t*)v)[0] == 0x78 && ((uint8_t*)v)[1] == 0x01) { v += 2; i -= 2; }
                        if(((uint8_t*)v)[0] == 0x1F && ((uint8_t*)v)[1] == 0x8B) { v += 10; i -= 10; }
                        data = buff = (uint32_t*)stbi_zlib_decode_malloc_guesssize_headerflag((const char*)v, i, 4096, &i, 0);
                        if(data) goto readuints;
                    break;
                    case 5: /* zstd */
                        while(*s && (*s == ' ' || *s == '\r' || *s == '\n')) s++;
                        if(!*s) goto brk;
                        for(v = s; *s && *s != '<'; s++);
                        zi.size = base64_decode(v);
                        zo.size = w * h * sizeof(uint32_t) + sizeof(uint32_t);
                        zcmp = ZSTD_createDCtx();
                        if(zcmp) {
                            data = buff = (uint32_t*)malloc(zo.size);
                            if(buff) {
                                zi.src = v; zi.pos = 0;
                                zo.dst = (uint8_t*)buff; zo.pos = 0;
                                ZSTD_decompressStream(zcmp, &zo, &zi);
                                ZSTD_freeDCtx(zcmp);
                                goto readuints;
                            }
                        }
                    break;
                }
                if(buff) free(buff);
            }
        }
brk:    if(tiles) {
            for(i = 0; i < numtiles; i++)
                if(tiles[i].name) free(tiles[i].name);
            free(tiles);
        }
        /* clear ground 2 where it matches ground 1, or both ground 1 and 2 if it matches objects layer */
        for(i = 0, k = stripe * stripe; i < k; i++) {
            if(map[i] == map[i + k + k]) map[i] = 0xffff;
            if(map[i + k] == map[i + k + k]) map[i + k] = 0xffff;
            if(map[i] == map[i + k]) map[i + k] = 0xffff;
        }
        /* parse paths */
        if(!onlymap) {
            n = -1;
            while(*s && memcmp(s, "</objectg", 9) && memcmp(s, "</map", 5)) {
                s = project_skipnl(s);
                while(*s == ' ' || *s == '\t') s++;
                if(!*s || *s != '<') break;
                k = l = 0;
                if(!memcmp(s, "<object ", 8)) {
                    n = -1;
                    for(; *s && *s != '>'; s++) {
                        if(!memcmp(s, " name=", 6)) { s += 7; n = atoi(s) - 1; }
                        if(!memcmp(s, " x=", 3)) { s += 4; k = atoi(s); }
                        if(!memcmp(s, " y=", 3)) { s += 4; l = atoi(s); }
                    }
                }
                if(n >= 0 && !memcmp(s, "<polygon ", 9)) {
                    maps_layers |= MAP_MASK_PATH;
                    while(*s && *s != '\"' && *s != '>') s++;
                    if(*s != '\"') break;
                    s++; m = maps_pathlen[n] = 0;
                    if(verbose > 2) printf("  path %d", n + 1);
                    while(*s && *s != '\"') {
                        while(*s && (*s == ' ' || *s == '\r' || *s == '\n' || *s == ',')) s++;
                        if(!*s || *s == '\"' || *s == '>') break;
                        s = project_getint(s, &i, -2147483647, 2147483647);
                        if(*s == ',') s++;
                        s = project_getint(s, &j, -2147483647, 2147483647);
                        if(maps_pathlen[n] >= m) {
                            m += 16;
                            maps_paths[n] = (SDL_Point*)realloc(maps_paths[n], m * sizeof(SDL_Point));
                            if(!maps_paths[n]) main_error(ERR_MEM);
                            memset(&maps_paths[n][m - 16], 0, 16 * sizeof(SDL_Point));
                        }
                        i += k; j += l;
                        switch(type) {
                            case TNG_TYPE_ORTHO: i /= tw; j /= th; break;
                            case TNG_TYPE_ISO: i /= (tw / 2); j /= th; break;
                            case TNG_TYPE_HEXV: break;
                            case TNG_TYPE_HEXH: break;
                        }
                        if(verbose > 2) printf(" (%u,%u)", i, j);
                        maps_paths[n][maps_pathlen[n]].x = i;
                        maps_paths[n][maps_pathlen[n]++].y = j;
                    }
                    if(verbose > 2) printf("\n");
                }
            }
        }
        free(str);
        return 1;
    }
    return 0;
}

/**
 * Load strings for translation
 */
void maps_loadstr()
{
    char *str, *s;
    int i;

    for(i = 0; i < project.nummaps; i++) {
        str = s = project_loadfile(project_dirs[PROJDIRS_MAPS], project.maps[i], "tmx", PROJMAGIC_MAPTMX, "maps_loadstr");
        if(str) {
            while(*s && memcmp(s, "</", 2)) {
                s = project_skipnl(s);
                while(*s == ' ' || *s == '\t') s++;
                if(!*s || *s != '<') break;
                if(!memcmp(s, "<property name=\"title\"", 22)) {
                    s += 22; while(*s && *s != '\"') s++;
                    s = project_gettrstr(s, 4, sizeof(maps_title));
                    break;
                }
            }
            free(str);
        }
    }
}

/**
 * Load npc animations
 */
void maps_loadnpc(int idx)
{
    if(idx >= 0 && idx < project.spritenum[PROJ_NUMSPRCATS - 2]) {
        if(!maps_npcs)
            maps_npcs = (maps_npc_t*)main_alloc(project.spritenum[PROJ_NUMSPRCATS - 2] * sizeof(maps_npc_t));
        npcs_loadnpc(idx, 1);
        maps_npcs[idx].light = npcs_attrget(attrs_defattr[0].val);
        maps_npcs[idx].speed = npcs_speed.val;
        maps_npcs[idx].spr = npcs_loadsprites();
        npcs_free();
    }
}

/**
 * Load object and npc light radiuses
 */
void maps_loadlights()
{
    int i, l;
    char *str, *s;

    if(maps_objlights) { free(maps_objlights); maps_objlights = NULL; }
    attrs_loadcfg();
    if(project.attrs && attrs_defattr[0].val >= 0 && project.spritenum[1] > 0) {
        l = strlen(project.attrs[attrs_defattr[0].val]);
        maps_objlights = (uint16_t*)main_alloc(project.spritenum[1] * sizeof(uint16_t));
        for(i = 0; i < project.spritenum[1]; i++) {
            str = project_loadfile(project_dirs[PROJDIRS_OBJECTS], project.sprites[1][i].name, "obj", PROJMAGIC_OBJECT,
                "maps_loadlights");
            if(str) {
                s = project_skipnl(str);
                s = project_skipnl(s);
                while(*s) {
                    s = project_skipnl(s);
                    if(!*s) break;
                    if(*s == 'm' && s[1] == ' ' && !memcmp(s + 6, project.attrs[attrs_defattr[0].val], l) && s[6 + l] == ' '){
                        maps_objlights[i] = (uint16_t)atoi(s + 7 + l);
                        break;
                    }
                }
                free(str);
            }
        }
    }
}

/**
 * Load neightboring maps into a big 3 x 3 map
 */
void maps_loadneightbors(uint16_t *maps, int *neightbors)
{
    int myneight[6] = { -1, -1, -1, -1, -1, -1 };

    if(!maps || !neightbors) return;
    switch(project.type) {
        case TNG_TYPE_ORTHO:
        case TNG_TYPE_ISO:
            /* south */
            if(neightbors[0] >= 0) {
                maps_loadtmx(project.maps[neightbors[0]], maps + MAPS_IDX(1, 2), myneight, 1);
                /* south-west */
                if(myneight[1] >= 0)
                    maps_loadtmx(project.maps[myneight[1]], maps + MAPS_IDX(0, 2), NULL, 1);
                /* south-east */
                if(myneight[3] >= 0)
                    maps_loadtmx(project.maps[myneight[3]], maps + MAPS_IDX(2, 2), NULL, 1);
            }
            /* west */
            if(neightbors[1] >= 0)
                maps_loadtmx(project.maps[neightbors[1]], maps + MAPS_IDX(0, 1), NULL, 1);
            /* north */
            if(neightbors[2] >= 0) {
                myneight[1] = myneight[3] = -1;
                maps_loadtmx(project.maps[neightbors[2]], maps + MAPS_IDX(1, 0), myneight, 1);
                /* north-west */
                if(myneight[1] >= 0)
                    maps_loadtmx(project.maps[myneight[1]], maps + MAPS_IDX(0, 0), NULL, 1);
                /* north-east */
                if(myneight[3] >= 0)
                    maps_loadtmx(project.maps[myneight[3]], maps + MAPS_IDX(2, 0), NULL, 1);
            }
            /* east */
            if(neightbors[3] >= 0)
                maps_loadtmx(project.maps[neightbors[3]], maps + MAPS_IDX(2, 1), NULL, 1);
        break;
        case TNG_TYPE_HEXV:
            /* south-west */
            if(neightbors[0] >= 0)
                maps_loadtmx(project.maps[neightbors[0]], maps + MAPS_IDX(0, 2) + project.mapsize / 2, NULL, 1);
            /* west */
            if(neightbors[1] >= 0)
                maps_loadtmx(project.maps[neightbors[1]], maps + MAPS_IDX(0, 1), NULL, 1);
            /* north-west */
            if(neightbors[2] >= 0)
                maps_loadtmx(project.maps[neightbors[2]], maps + MAPS_IDX(0, 0) + project.mapsize / 2, NULL, 1);
            /* north-east */
            if(neightbors[3] >= 0)
                maps_loadtmx(project.maps[neightbors[2]], maps + MAPS_IDX(1, 0) + project.mapsize / 2, NULL, 1);
            /* east */
            if(neightbors[4] >= 0)
                maps_loadtmx(project.maps[neightbors[1]], maps + MAPS_IDX(2, 1), NULL, 1);
            /* south-east */
            if(neightbors[5] >= 0)
                maps_loadtmx(project.maps[neightbors[5]], maps + MAPS_IDX(1, 2) + project.mapsize / 2, NULL, 1);
        break;
        case TNG_TYPE_HEXH:
            /* south */
            if(neightbors[0] >= 0)
                maps_loadtmx(project.maps[neightbors[0]], maps + MAPS_IDX(1, 2), NULL, 1);
            /* south-west */
            if(neightbors[1] >= 0)
                maps_loadtmx(project.maps[neightbors[1]], maps + MAPS_IDX(0, 1) + project.mapsize / 2 * MAPS_STRIPE, NULL, 1);
            /* north-west */
            if(neightbors[2] >= 0)
                maps_loadtmx(project.maps[neightbors[2]], maps + MAPS_IDX(0, 0) + project.mapsize / 2 * MAPS_STRIPE, NULL, 1);
            /* north */
            if(neightbors[3] >= 0)
                maps_loadtmx(project.maps[neightbors[3]], maps + MAPS_IDX(1, 0), NULL, 1);
            /* north-east */
            if(neightbors[4] >= 0)
                maps_loadtmx(project.maps[neightbors[4]], maps + MAPS_IDX(2, 0) + project.mapsize / 2 * MAPS_STRIPE, NULL, 1);
            /* south-east */
            if(neightbors[5] >= 0)
                maps_loadtmx(project.maps[neightbors[5]], maps + MAPS_IDX(2, 1) + project.mapsize / 2 * MAPS_STRIPE, NULL, 1);
        break;
    }
}

/**
 * Render a sprite on a big 3 x 3 map
 * k = layer
 * mx, my = map coordinates
 * x, y = screen coordinates
 * w, h = screen dimension
 * t = sprite index
 * l = lights texture or NULL if no update needed
 */
void maps_renderspr(int k, int mx, int my, int x, int y, int w, int h, int t, uint32_t *l, int a)
{
    int n = -1, cat = maps_paint[maps_p[k]].cat;
    SDL_Rect src, rect;
    ui_sprite_t *s = NULL;

    rect.x = x; rect.y = y; rect.w = project.tilew; rect.h = project.tileh;
    if(k == 4 && mx == maps_ox + project.mapsize && my == maps_oy + project.mapsize && maps_oanim >= 0) {
        s = spr_getidx(cat, maps_oanim, 0, 0);
        t = maps_oanim;
        n = maps_of;
        if(maps_ol != ui_anim_cnt) {
            maps_ol = ui_anim_cnt;
            maps_of++;
        }
        if(s && !s->type && maps_of > s->nframe)
            maps_oanim = -1;
    } else {
        s = spr_getidx(cat, t, 0, 0);
    }
    if(s) {
        if(!project.sprites[cat][t].txt)
            spr_texture(s, &project.sprites[cat][t].txt);
        if(project.sprites[cat][t].txt) {
            src.x = ui_anim(s->type, s->nframe, n) * s->w; src.y = 0;
            src.w = rect.w = s->w; src.h = rect.h = s->h;
            rect.x = x - rect.w / 2; rect.y = y - s->h;
            SDL_RenderCopy(renderer, project.sprites[cat][t].txt, &src, &rect);
        }
    }
    if(l && k == 4 && maps_objlights && maps_objlights[t] > 0)
        ui_circle(l, w, h, x, y - rect.h / 2, maps_objlights[t] * project.tilew + (rand() % 10), a);
}

/**
 * Render NPCs to big 3 x 3 map
 */
void maps_rendernpcs(int w, int h, uint32_t *l, int a)
{
    int i;
    SDL_Rect src, dst;
    ui_sprite_t *s;

    if(!maps_npcs || !project.sprites[PROJ_NUMSPRCATS - 2]) return;
    for(i = 0; i < project.spritenum[PROJ_NUMSPRCATS - 2]; i++)
        if(maps_npcs[i].x > 0 && maps_npcs[i].y > 0) {
            s = &maps_npcs[i].spr[maps_npcs[i].anim * 8 + maps_npcs[i].d];
            if(!s->data && maps_npcs[i].d && maps_npcs[i].spr[maps_npcs[i].anim * 8 + maps_npcs[i].d - 1].data)
                s = &maps_npcs[i].spr[maps_npcs[i].anim * 8 + maps_npcs[i].d - 1];
            if(!s->data && maps_npcs[i].spr[maps_npcs[i].anim * 8].data) s = &maps_npcs[i].spr[maps_npcs[i].anim * 8];
            if(s->data) {
                src.w = dst.w = s->w; src.h = dst.h = s->h;
                src.x = ui_anim(s->type, s->nframe, maps_npcs[i].f) * s->w;
                src.y = 0; dst.x = maps_npcs[i].x - (s->w / 2); dst.y = maps_npcs[i].y - s->h;
                if(!project.sprites[PROJ_NUMSPRCATS - 2][i].txt)
                    spr_texture(s, &project.sprites[PROJ_NUMSPRCATS - 2][i].txt);
                if(project.sprites[PROJ_NUMSPRCATS - 2][i].txt)
                    SDL_RenderCopy(renderer, project.sprites[PROJ_NUMSPRCATS - 2][i].txt, &src, &dst);
                if(maps_npcs[i].la != ui_anim_cnt) {
                    maps_npcs[i].la = ui_anim_cnt;
                    maps_npcs[i].f++;
                    /* play once animations should stop at the last frame until new command */
                    if(!s->type && maps_npcs[i].f > s->nframe)
                        maps_npcs[i].f = s->nframe;
                }
                if(l && maps_npcs[i].light > 0)
                    ui_circle(l, w, h, maps_npcs[i].x, maps_npcs[i].y - (s->h / 2), maps_npcs[i].light * project.tilew +
                        (rand() % 10), a);
            }
        }
}

/**
 * Render a big 3 x 3 map with neightbors and parallaxes and everything with center at x, y
 */
void maps_rendermap(uint16_t *maps, int mask, int x, int y, int w, int h, int cx, int cy)
{
    uint32_t now = SDL_GetTicks(), *pixels, c = 0;
    maps_par_t *par = (maps_par_t*)maps_partbl.data;
    maps_npccmd_t *cmd;
    int i, j, k, m = 1, t, p, tw = project.tilew / 2, th = project.tileh / 2, needlight = 0, cat;
    SDL_Texture *texture;
    SDL_Rect src, dst, rect;
    ui_sprite_t *s;

    if(!maps || x < 0 || y < 0 || w < 1 || h < 1 || cx < 0 || cy < 0 || cx >= project.mapsize || cy >= project.mapsize) return;

    rect.x = x; rect.y = x; rect.w = w; rect.h = h;
    SDL_RenderSetClipRect(renderer, &rect);
    if(maps_npcs) {
        for(i = 0; i < project.spritenum[PROJ_NUMSPRCATS - 2]; i++)
            if(maps_npcs[i].x > 0 && maps_npcs[i].y > 0 && maps_npcs[i].nm < now) {
                if(maps_npcs[i].numcmd > maps_npcs[i].idxcmd) {
                    cmd = &maps_npcs[i].cmd[maps_npcs[i].idxcmd];
                    if(maps_npcs[i].t) {
                        maps_npcs[i].nm = now + 10000 / (maps_npcs[i].speed > 0 ? maps_npcs[i].speed : 1);
                        maps_npcs[i].anim = 1;
                        maps_npcs[i].c++;
                        if(maps_npcs[i].c >= maps_npcs[i].t) {
                            maps_npcs[i].x = maps_npcs[i].dx = maps_npcs[i].nx;
                            maps_npcs[i].y = maps_npcs[i].dy = maps_npcs[i].ny;
                            maps_npcs[i].anim = maps_npcs[i].c = maps_npcs[i].t = 0;
                            maps_npcs[i].idxcmd++;
                        } else {
                            maps_npcs[i].x = maps_npcs[i].dx + (maps_npcs[i].nx - maps_npcs[i].dx) *
                                maps_npcs[i].c / maps_npcs[i].t;
                            maps_npcs[i].y = maps_npcs[i].dy + (maps_npcs[i].ny - maps_npcs[i].dy) *
                                maps_npcs[i].c / maps_npcs[i].t;
                        }
                        maps_daylast = 0;
                    } else
                    switch(cmd->type + CMD_NOP) {
                        case CMD_CANIM:
                            spr_freecache(PROJ_NUMSPRCATS - 2, i);
                            maps_npcs[i].anim = cmd->par1;
                            maps_npcs[i].f = maps_npcs[i].la = 0;
                            maps_npcs[i].nm = now + cmd->par2 * 10;
                            maps_npcs[i].idxcmd++;
                        break;
                        default:
                            spr_freecache(PROJ_NUMSPRCATS - 2, i);
                            maps_npcs[i].f = maps_npcs[i].la = maps_npcs[i].nm = maps_npcs[i].c = 0;
                            maps_npcs[i].d = cmd->type + CMD_NOP - CMD_NPCS;
                            maps_npcs[i].dx = maps_npcs[i].x;
                            maps_npcs[i].dy = maps_npcs[i].y;
                            switch(cmd->type + CMD_NOP) {
                                case CMD_NPCS:
                                    maps_npcs[i].ny += cmd->par1 * project.tileh;
                                break;
                                case CMD_NPCSW:
                                    maps_npcs[i].nx -= cmd->par1 * project.tilew;
                                    maps_npcs[i].ny += cmd->par1 * project.tileh;
                                break;
                                case CMD_NPCW:
                                    maps_npcs[i].nx -= cmd->par1 * project.tilew;
                                break;
                                case CMD_NPCNW:
                                    maps_npcs[i].nx -= cmd->par1 * project.tilew;
                                    maps_npcs[i].ny -= cmd->par1 * project.tileh;
                                break;
                                case CMD_NPCN:
                                    maps_npcs[i].ny -= cmd->par1 * project.tileh;
                                break;
                                case CMD_NPCNE:
                                    maps_npcs[i].nx += cmd->par1 * project.tilew;
                                    maps_npcs[i].ny -= cmd->par1 * project.tileh;
                                break;
                                case CMD_NPCE:
                                    maps_npcs[i].nx += cmd->par1 * project.tilew;
                                break;
                                case CMD_NPCSE:
                                    maps_npcs[i].nx += cmd->par1 * project.tilew;
                                    maps_npcs[i].ny += cmd->par1 * project.tileh;
                                break;
                            }
                            k = maps_npcs[i].nx - maps_npcs[i].x;
                            j = maps_npcs[i].ny - maps_npcs[i].y;
                            maps_npcs[i].t = (int)sqrt(k * k + j * j);
                            if(!cmd->par1) {
                                maps_npcs[i].idxcmd++;
                                maps_npcs[i].anim = 0;
                            } else
                                maps_npcs[i].anim = 1;
                        break;
                    }
                }
            }
    }
    if(maps_daylen.val > 0 && maps_dayspr.val >= 0 && project.sprites[maps_dayspr.cat] &&
      project.sprites[maps_dayspr.cat][maps_dayspr.val].dir[0].data) {
        if(!maps_daytxt) {
            maps_daytxt = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, w, h);
            j = 1;
        } else
            j = 0;
        if(maps_daytxt) {
            SDL_SetTextureBlendMode(maps_daytxt, SDL_BLENDMODE_BLEND);
            SDL_LockTexture(maps_daytxt, NULL, (void**)&pixels, &p);
            s = spr_getidx(maps_dayspr.cat, maps_dayspr.val, 0, 0);
            if(s) {
                i = maps_daylen.val * 60000;
                i = ((now - maps_base) % i) * s->w / i;
                c = s->data[i % s->w];
                if((c >> 24) > 0xF0) c &= 0xF0FFFFFF;
                if((c >> 24) > 16 && (j || i != maps_daylast || !((now - maps_base) % 50))) {
                    maps_daylast = i;
                    for(i = 0; i < p / 4 * h; i++) pixels[i] = c;
                    if(maps_objlights || maps_npcs) needlight = 1;
                }
            }
        }
    }
    if(maps) {
        cx += project.mapsize; cy += project.mapsize;
        if(maps[10 * MAPS_STRIPE * MAPS_STRIPE + cy * MAPS_STRIPE + cx] != 0xffff)
            mask &= ~MAP_MASK_ROOF4;
        /* we have to display parallaxes twice: once behind, once above, and layers in the middle */
        for(p = 0; p < 3; p++)
            switch(p) {
                case 1:
                    for(k = 0; k < MAPS_NUMLYR - 1; k++, m <<= 1)
                        if(mask & m) {
                            cat = maps_paint[maps_p[k]].cat;
                            if(k == 5)
                                maps_rendernpcs(w, h, needlight ? pixels : NULL, c >> 24);
                            else
                                switch(project.type) {
                                    case TNG_TYPE_ORTHO:
                                        for(j = 0; j < MAPS_STRIPE; j++)
                                            for(i = 0; i < MAPS_STRIPE; i++) {
                                                t = maps[k * MAPS_STRIPE * MAPS_STRIPE + j * MAPS_STRIPE + i];
                                                if(!(mask & m) || t == 0xffff || t >= project.spritenum[cat] ||
                                                  project.sprites[cat][t].name[0] == '~')
                                                    continue;
                                                maps_renderspr(k, i, j,
                                                    (i - cx) * project.tilew + w / 2 + rect.x,
                                                    (j - cy + 1) * project.tileh + h / 2 + rect.y,
                                                    w, h, t, needlight ? pixels : NULL, c >> 24);
                                            }
                                    break;
                                    case TNG_TYPE_ISO:
                                        for(j = 0; j < 2 * MAPS_STRIPE; j++)
                                            for(i = 0; i < MAPS_STRIPE; i++) {
                                                if((j - i) < 0 || (j - i) >= MAPS_STRIPE) continue;
                                                t = maps[k * MAPS_STRIPE * MAPS_STRIPE + (j - i) * MAPS_STRIPE + i];
                                                if(!(mask & m) || t == 0xffff || t >= project.spritenum[cat] ||
                                                  project.sprites[cat][t].name[0] == '~')
                                                    continue;
                                                maps_renderspr(k, i, j - i,
                                                    ((i - cx) - ((j - i) - cy)) * tw + w / 2 + rect.x,
                                                    ((i - cx) + ((j - i) - cy)) * th + project.tileh + h / 2 + rect.y,
                                                    w, h, t, needlight ? pixels : NULL, c >> 24);
                                            }
                                    break;
                                    case TNG_TYPE_HEXV: break;
                                    case TNG_TYPE_HEXH: break;
                                }
                        }
                break;
                default:
                    if(par && project.sprites[maps_parspr.cat])
                        for(i = 0; i < maps_partbl.num; i++)
                            if(par[i].spr >= 0 && par[i].bg == (!p ? 1 : 0)) {
                                s = spr_getidx(maps_parspr.cat, par[i].spr, 0, 0);
                                if(!s) continue;
                                if(!project.sprites[maps_parspr.cat][par[i].spr].txt)
                                    spr_texture(s, &project.sprites[maps_parspr.cat][par[i].spr].txt);
                                if(par[i].last + par[i].tm * 10 < now) {
                                    par[i].last = now;
                                    par[i].x += par[i].sx;
                                    par[i].y += par[i].sy;
                                    if(par[i].x + s->w < 0 && par[i].sx < 0) par[i].x += 2 * s->w;
                                    if(par[i].x > w && par[i].sx > 0) par[i].x -= 2 * s->w;
                                    if(par[i].y + s->h < 0 && par[i].sy < 0) par[i].y += 2 * s->h;
                                    if(par[i].y > h && par[i].sy > 0) par[i].y -= 2 * s->h;
                                }
                                if((texture = project.sprites[maps_parspr.cat][par[i].spr].txt)) {
                                    src.w = dst.w = s->w; src.x = 0;
                                    src.h = dst.h = s->h; src.y = 0;
                                    for(dst.y = -(dst.h - (par[i].y + cy * par[i].dy) % dst.h); dst.y < h; dst.y += dst.h)
                                        for(dst.x = -(dst.w - (par[i].x + cx * par[i].dx) % dst.w); dst.x < w; dst.x += dst.w)
                                            SDL_RenderCopy(renderer, texture, &src, &dst);
                                }
                            }
                break;
            }
    }
    if(maps_daytxt) {
        SDL_UnlockTexture(maps_daytxt);
        SDL_RenderCopy(renderer, maps_daytxt, NULL, &rect);
    }
    if(mask & MAP_MASK_GRID) {
        SDL_SetRenderDrawColor(renderer, theme[THEME_FG] & 0xff, (theme[THEME_FG] >> 8) & 0xff,
            (theme[THEME_FG] >> 16) & 0xff, 0);
        rect.x += w / 2; rect.y += h / 2;
        switch(project.type) {
            case TNG_TYPE_ORTHO:
                SDL_RenderDrawLine(renderer, rect.x - tw, rect.y, rect.x + tw, rect.y);
                SDL_RenderDrawLine(renderer, rect.x - tw, rect.y, rect.x - tw, rect.y + project.tileh);
                SDL_RenderDrawLine(renderer, rect.x + tw, rect.y, rect.x + tw, rect.y + project.tileh);
                SDL_RenderDrawLine(renderer, rect.x - tw, rect.y + project.tileh, rect.x + tw, rect.y + project.tileh);
            break;
            case TNG_TYPE_ISO:
                SDL_RenderDrawLine(renderer, rect.x, rect.y, rect.x + tw, rect.y + th);
                SDL_RenderDrawLine(renderer, rect.x, rect.y, rect.x - tw, rect.y + th);
                SDL_RenderDrawLine(renderer, rect.x - tw, rect.y + th, rect.x, rect.y + project.tileh);
                SDL_RenderDrawLine(renderer, rect.x + tw, rect.y + th, rect.x, rect.y + project.tileh);
            break;
            case TNG_TYPE_HEXV:
            break;
            case TNG_TYPE_HEXH:
            break;
        }
    }
    SDL_RenderSetClipRect(renderer, NULL);
}

/**
 * Load a map with neightbors
 */
uint16_t *maps_loadscene(int idx, int tm)
{
    uint16_t *map = NULL;

    if(!project.maps || idx < 0 || idx >= project.nummaps) return NULL;
    if(verbose) printf("maps_loadscene: loading map %s\n", project.maps[idx]);
    map = (uint16_t*)main_alloc(MAPS_STRIPE * MAPS_STRIPE * (MAPS_NUMLYR - 1) * sizeof(uint16_t));
    memset(map, 0xff, MAPS_STRIPE * MAPS_STRIPE * (MAPS_NUMLYR - 1) * sizeof(uint16_t));
    maps_freetmx();
    maps_loadtmx(project.maps[idx], map + project.mapsize * MAPS_STRIPE + project.mapsize, maps_neightbors, 0);
    maps_loadneightbors(map, maps_neightbors);
    maps_base = SDL_GetTicks() + maps_daylen.val * 60000 * tm / 24;
    maps_daylast = 0;
    return map;
}

/**
 * Preview map
 */
void maps_preview(void *data)
{
    maps_par_t *par = (maps_par_t*)maps_partbl.data;
    Mix_Music *music = NULL;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *texture = NULL, *title = NULL;
    SDL_Rect rect;
    ssfn_buf_t buf;
    char *str;
    int wf = SDL_GetWindowFlags(window), i, j, k, l, w, h, b, x, y, p, noaudio;
    int cx = project.mapsize / 2, cy = project.mapsize / 2;
    uint32_t *pixels;
    uint16_t *map = NULL;

    (void)data;
    if(!maps_map) return;
    if(verbose) printf("maps_preview: started\n");

    /* create a big 3 x 3 map */
    map = (uint16_t*)main_alloc(MAPS_STRIPE * MAPS_STRIPE * (MAPS_NUMLYR - 1) * sizeof(uint16_t));
    memset(map, 0xff, MAPS_STRIPE * MAPS_STRIPE * (MAPS_NUMLYR - 1) * sizeof(uint16_t));
    /* copy our current map in the middle */
    for(k = 0; k < MAPS_NUMLYR - 1; k++) {
        for(j = 0; j < project.mapsize; j++)
            memcpy(map + k * MAPS_STRIPE * MAPS_STRIPE + (j + project.mapsize) * MAPS_STRIPE + project.mapsize,
                maps_map + k * project.mapsize * project.mapsize + j * project.mapsize, project.mapsize * sizeof(uint16_t));
    }
    /* load surrounding maps and light sources */
    maps_loadneightbors(map, maps_neightbors);
    maps_loadlights();
    maps_deltay = 0; chars_loadcfg((uint8_t*)&maps_deltay);
    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
    if(texture) {
        SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
        /* checker */
        for(j = k = 0; j < dm.h; j++, k += p/4)
            for(i = 0; i < dm.w ; i++) {
                x = (i >> 5); y = (j >> 5);
                pixels[k + i] = theme[(y & 1) ^ (x & 1) ? THEME_LIGHT : THEME_DARK];
            }
        SDL_UnlockTexture(texture);
    }
    if(par)
        for(i = 0; i < maps_partbl.num; i++)
            par[i].x = par[i].y = par[i].last = 0;
    maps_daylast = 0;

    elements_load();
    elements_loadfonts();
    str = maps_title;
    /* always display the label, even if the map title is missing, it could be a good reminder to set it */
    if(*str) ssfn_bbox(&elements_fonts[0].ctx, str, &w, &h, &l, &b);
    else { h = elements_size[0].val; w = 8 * h; l = b = 0; }
    buf.w = rect.w = w + l + 2 * elements_size[0].val; rect.h = h + 4;
    title = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, rect.w, rect.h);
    if(title) {
        SDL_SetTextureBlendMode(title, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(title, NULL, (void**)&pixels, &p);
        for(i = 0; i <= rect.w / 2; i++) {
            k = (i * 3 < 0xcf ? i * 3 : 0xcf) << 24;
            for(j = 0; j < rect.h; j++)
                pixels[j * rect.w + i] = pixels[j * rect.w + rect.w - 1 - i] = k;
        }
        if(*str) {
            buf.ptr = (uint8_t*)pixels;
            buf.p = p;
            buf.x = l + elements_size[0].val;
            buf.y = b + 2;
            buf.bg = 0;
            buf.fg = 0xFFC0C0C0;
            elements_fonts[0].ctx.style &= ~SSFN_STYLE_A;
            while(*str && ((i = ssfn_render(&elements_fonts[0].ctx, &buf, str)) > 0 || i == SSFN_ERR_NOGLYPH)) str += (i < 1 ? 1 : i);
            elements_fonts[0].ctx.style |= SSFN_STYLE_A;
        }
        SDL_UnlockTexture(title);
        rect.x = (dm.w - rect.w) / 2;
        rect.y = dm.h - rect.h - 32;
    }
    elements_freefonts();

    if(Mix_OpenAudio(FREQ, AUDIO_S16SYS, 2, 4096) < 0) { ui_status(1, lang[ERR_AUDIO]); noaudio = 1; }

    if(maps_backmus.val >= 0 && !noaudio) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA],
            project.music[maps_backmus.val]);
        if(verbose) printf("maps_preview: loading music %s\n", projdir);
        music = Mix_LoadMUS(projdir);
        if(music) {
            Mix_VolumeMusic(MIX_MAX_VOLUME);
            Mix_PlayMusic(music, -1);
        } else
            ui_status(1, lang[ERR_LOADSND]);
    }

    maps_base = SDL_GetTicks();
    while(1) {
        /* events */
        if(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
                SDL_PushEvent(&event); goto cleanup;
            }
            switch(event.type) {
                case SDL_KEYUP:
                    maps_daylast = 0;
                    switch(event.key.keysym.sym) {
                        case SDLK_UP: if(cy > 0) cy--; break;
                        case SDLK_DOWN: if(cy < project.mapsize - 1) cy++; break;
                        case SDLK_LEFT: if(cx > 0) cx--; break;
                        case SDLK_RIGHT: if(cx < project.mapsize - 1) cx++; break;
                        default: goto cleanup;
                    }
                break;
                case SDL_MOUSEBUTTONUP: goto cleanup; break;
            }
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        if(texture) SDL_RenderCopy(renderer, texture, NULL, NULL);
        maps_rendermap(map, MAP_MASK_GRID|MAP_MASK_ROOF1|MAP_MASK_ROOF2|MAP_MASK_ROOF3|MAP_MASK_ROOF4|MAP_MASK_OBJ|
            MAP_MASK_GRD1|MAP_MASK_GRD2|MAP_MASK_GRD3|MAP_MASK_GRD4, 0, 0, dm.w, dm.h, cx, cy);
        if(title) SDL_RenderCopy(renderer, title, NULL, &rect);
        SDL_RenderPresent(renderer);
        SDL_Delay(10);
    }

cleanup:
    if(!noaudio) {
        if(music) { Mix_HaltMusic(); Mix_FreeMusic(music); }
        Mix_CloseAudio();
    }
    if(map) free(map);
    if(texture) SDL_DestroyTexture(texture);
    if(title) SDL_DestroyTexture(title);
    if(maps_daytxt) { SDL_DestroyTexture(maps_daytxt); maps_daytxt = NULL; }
    if(maps_objlights) { free(maps_objlights); maps_objlights = NULL; }
    if(verbose) printf("maps_preview: stopped\n");
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Get references
 */
void maps_ref(tngctx_t *ctx)
{
    uint16_t *map;
    int i, j, k;

    if(!ctx || !project.maps || !project.nummaps) return;
    for(j = 0; j < project.nummaps; j++) {
        if((ctx->maps && !ctx->maps[j])) continue;
        map = (uint16_t*)main_alloc(project.mapsize * project.mapsize * (MAPS_NUMLYR - 1) * sizeof(uint16_t));
        memset(map, 0xff, project.mapsize * project.mapsize * (MAPS_NUMLYR - 1) * sizeof(uint16_t));
        maps_loadtmx(project.maps[j], map, NULL, 2);
        for(i = 0; i < 4 * project.mapsize * project.mapsize; i++)
            if(map[i] != 0xffff) tng_sprref(ctx, 1, map[i]);
        k = 4 * project.mapsize * project.mapsize;
        for(i = 0; i < project.mapsize * project.mapsize; i++)
            if(map[k + i] != 0xffff) objects_ref(ctx, map[k + i]);
        k = 5 * project.mapsize * project.mapsize;
        for(i = 0; i < project.mapsize * project.mapsize; i++)
            if(map[k + i] != 0xffff) npcs_ref(ctx, map[k + i]);
        k = 6 * project.mapsize * project.mapsize;
        for(i = 0; i < project.mapsize * project.mapsize; i++)
            if(map[k + i] != 0xffff) spawners_ref(ctx, map[k + i]);
        k = 7 * project.mapsize * project.mapsize;
        for(i = 0; i < 4 * project.mapsize * project.mapsize; i++)
            if(map[k + i] != 0xffff) tng_sprref(ctx, 1, map[k + i]);
        free(map);
    }
}

/**
 * Save tng
 */
int maps_totng(tngctx_t *ctx)
{
    uint16_t *map;
    maps_par_t *par;
    uLongf cl;
    uint8_t *buf, *ptr, *comp;
    int i, j, k, l, m, n = project.nummaps, a = 0;

    if(!ctx || !project.maps || !project.nummaps) return 1;
    /* check if there's at least one map selected */
    if(ctx->maps) {
        for(j = n = 0; j < project.nummaps; j++) if(ctx->maps[j]) n++;
        if(n < 1) return 1;
    }
    ctx->nummapassets = n;
    ctx->mapassets = (tng_asset_t*)main_alloc(n * sizeof(tng_asset_t));
    for(j = 0; j < project.nummaps; j++) {
        ui_progressbar(1, 2, project_curr++, project_total, LANG_COLLECT);
        if(ctx->maps && !ctx->maps[j]) continue;
        maps_new(NULL);
        if(!maps_loadtmx(project.maps[j], maps_map, maps_neightbors, 0)) {
            ui_switchtab(SUBMENU_MAPS);
            maps_tbl.val = j;
            maps_meta = 0;
            ui_status(1, lang[ERR_LOADMAP]);
            return 0;
        }
        par = (maps_par_t*)maps_partbl.data;
        /* convert map to tng local indeces */
        for(map = maps_map, k = 0; k < MAPS_NUMLYR - 1; k++)
            for(n = 0; n < project.mapsize; n++)
                for(i = 0; i < project.mapsize; i++, map++) {
                    if(*map == 0xffff || *map >= project.spritenum[maps_paint[maps_p[k]].cat]) *map = 0xffff;
                    else *map = project.sprites[maps_paint[maps_p[k]].cat][(int)*map].idx;
                }
        k = (MAPS_NUMLYR -1) * project.mapsize * project.mapsize;
        comp = (uint8_t*)main_alloc(3 * k);
        tng_rle_enc16(maps_map, k, comp, &m);
        l = 34 + 10 * maps_partbl.num + m;
        for(i = n = 0; i < 99; i++)
            if(maps_paths[i] && maps_pathlen[i]) { n++; l += 4 * maps_pathlen[i] + 3; }
        buf = ptr = (uint8_t*)main_alloc(l);
        ptr = tng_asset_text(ptr, ctx, maps_title);
        for(i = 0; i < 6; i++)
            ptr = tng_asset_idx(ptr, ctx, project.maps, -1, maps_neightbors[i]);
        memcpy(ptr, &maps_daylen.val, 2); ptr += 2;
        ptr = tng_asset_sprite(ptr, ctx, maps_dayspr.cat, maps_dayspr.val);
        ptr = tng_asset_idx(ptr, ctx, project.music, TNG_IDX_MUS, maps_backmus.val);
        *ptr++ = maps_partbl.num;
        *ptr++ = n;
        memcpy(ptr, &m, 3); ptr += 3;
        if(par)
            for(i = 0; i < maps_partbl.num; i++) {
                *ptr++ = par[i].bg;
                *ptr++ = par[i].sx; *ptr++ = par[i].dx;
                *ptr++ = par[i].sy; *ptr++ = par[i].dy;
                memcpy(ptr, &par[i].tm, 2); ptr += 2;
                ptr = tng_asset_sprite(ptr, ctx, maps_parspr.cat, par[i].spr);
            }
        for(k = 0; k < 99; k++)
            if(maps_paths[k] && maps_pathlen[k]) {
                *ptr++ = k;
                memcpy(ptr, &maps_pathlen[k], 2); ptr += 2;
                for(i = 0; i < maps_pathlen[k]; i++) {
                    memcpy(ptr, &maps_paths[k][i].x, 2); ptr += 2;
                    memcpy(ptr, &maps_paths[k][i].y, 2); ptr += 2;
                }
            }
        memcpy(ptr, comp, m); ptr += m;
        free(comp);
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        if(cl) {
            ctx->mapassets[a].data = comp;
            ctx->mapassets[a].size = cl;
        } else
            free(comp);
        a++;
        free(buf);
        /* free resources */
        maps_freetmx();
    }
    return 1;
}

/**
 * Save tng assets - maps must be the last assets in the file
 */
void maps_toassets(tngctx_t *ctx)
{
    int i, j;

    if(!ctx || !ctx->mapassets) return;
    /* add maps to output */
    tng_section(ctx, TNG_SECTION_MAPS);
    for(i = j = 0; j < project.nummaps; j++) {
        if(ctx->maps && !ctx->maps[j]) continue;
        if(ctx->mapassets[i].data && ctx->mapassets[i].size) {
            tng_desc(ctx, project.maps[j], ctx->mapassets[i].size);
            tng_asset_int(ctx, ctx->mapassets[i].data, ctx->mapassets[i].size);
        }
        ctx->mapassets[i].data = NULL;
        i++;
    }
    free(ctx->mapassets); ctx->mapassets = NULL;
    ctx->nummapassets = 0;
}

/**
 * Read from tng
 */
int maps_fromtng(tngctx_t *ctx)
{
    trn_t *trn = (trn_t*)translate_tbl.data;
    tng_asset_desc_t *asset;
    uint8_t *buf, *ptr, *end;
    int i, j, k, l, np, nn, nm, len, p;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_MAPS; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;
    for(j = 0; j < len; j++, asset++) {
        ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
        buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &l);
        if(buf) {
            if(!asset->name || asset->name >= TNG_SECTION_SIZE(&ctx->tbl[0]) || l < 34) { free(buf); continue; }
            end = buf + l;
            maps_new(NULL);
            strncpy(maps_name, (char*)ctx->sts + asset->name, sizeof(maps_name) - 1);
            i = 0; memcpy(&i, ptr, 3); ptr += 3;
            if(i >= 0 && i < translate_tbl.num && trn)
                strncpy(maps_title, trn[i].id, sizeof(maps_title) - 1);
            for(i = 0; i < 6; i++) {
                l = 0; memcpy(&l, ptr, 3); ptr += 3;
                maps_neightstrs[i] = (char*)ctx->sts + l;
            }
            maps_daylen.val = *((uint16_t*)ptr); ptr += 2;
            maps_dayspr.val = *((int16_t*)&ptr[1]); ptr += 3;
            l = 0; memcpy(&l, ptr, 3); ptr += 3;
            maps_backmus.val = -1;
            if(l >= 0 && l < TNG_SECTION_SIZE(&ctx->tbl[0]))
                for(i = 0; i < project.nummusic; i++)
                    if(!strcmp(project.music[i], (char*)ctx->sts + l)) { maps_backmus.val = i; break; }
            np = *ptr++;
            nn = *ptr++;
            nm = 0; memcpy(&nm, ptr, 3); ptr += 3;
            for(i = 0; i < np && ptr < end; i++) {
                maps_partype.val = *ptr++;
                maps_parsx.val = *((int8_t*)ptr); ptr++;
                maps_pardx.val = *ptr++;
                maps_parsy.val = *((int8_t*)ptr); ptr++;
                maps_pardy.val = *ptr++;
                maps_partm.val = 0; memcpy(&maps_partm.val, ptr, 2); ptr += 2;
                maps_parspr.val = *((int16_t*)&ptr[1]); ptr += 3;
                maps_addpar(NULL);
            }
            for(i = 0; i < nn && ptr < end; i++) {
                p = *ptr++;
                l = 0; memcpy(&l, ptr, 2); ptr += 2;
                maps_pathlen[p] = l;
                maps_paths[p] = (SDL_Point*)main_alloc(l * sizeof(SDL_Point));
                for(k = 0; k < l; k++) {
                    maps_paths[p][k].x = *((int16_t*)ptr); ptr += 2;
                    maps_paths[p][k].y = *((int16_t*)ptr); ptr += 2;
                }
            }
            l = (ctx->hdr.numlyr > MAPS_NUMLYR - 1 ? MAPS_NUMLYR - 1 : ctx->hdr.numlyr) * project.mapsize * project.mapsize;
            tng_rle_dec16(ptr, nm, maps_map, &l);
            free(buf);
            maps_save(NULL);
            memset(maps_neightstrs, 0, sizeof(maps_neightstrs));
        }
    }
    maps_free();
    return 1;
}
