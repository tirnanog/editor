TirNanoG Editor
===============

Source of TirNanoG Editor, the `tnge` executable. Also contains stripped down version of libraries it depends on.

Compilation
-----------

You have multiple options on how to compile. You can compile everything statically and produce a dependency-free,
single portable executable.

Or you can choose to compile dynamically, in which case the final executable will be much smaller, however it will
require certain .so files and DLLs to be installed on the end user's computer in order to run.

Makefile rules: `clean` will delete all objects for tnge, but won't touch the libraries. `distclean` will purge all
binaries, even in dependencies. `all` will compile, and finally `package` will create a distributable archive file.

### Linux

For Linux, I'd recommend using the dynamic linking method, as Linux distros have a package manager which can install
the dependencies easily. Keep in mind, you might run into problems if your distro does not ship sufficiently up-to-date
versions of the libraries.

#### Statically

As simple as:

```sh
$ make all package
```

NOTE: I had to remove wayland support from the static SDL2, because it did not compile due to a bug in wayland scanner and
xml2c converter (wait, what?), it wasn't SDL's fault. If wayland f*cked up tool gets fixed, remove `--disable-video-wayland`
from the Makefile.

Luckily you can compile everything statically in from the source tree, except SDL. To do this, use

```sh
$ USE_DYNSDL=1 make all package
```

This linking option also has a rule to create a debian package instead of a tarball:

```sh
$ USE_DYNSDL=1 make all deb
```

Creating a deb file uses only the standard GNU toolchain, no third party tools required.

#### Dynamically

Using your repo's package management system, install development versions of the dependencies. They are probably called:
`libsdl2-dev`, `libsdl2-mixer-dev`, `libvorbis-dev`, `libtheora-dev`, `libpng-dev`, `libfreetype2-dev`, `libmpg123-dev`,
`libzstd-dev` (actual names vary on distros).

Then

```sh
$ USE_DYNLIBS=1 make all package
```

NOTE: your distro could ship outdated versions of these libraries, and (in lack of strict configuration for just the
necessary features only) they might have additional dependencies too! Most notably the default SDL installation has lots of
further dependencies, and the shared library version of SDL2_mixer does not support Mix_MusicDuration, so there'll be no
progressbar when you import audio files.

### Windows

Requires MSYS2 and MinGW. Under Windows, where there's no package managment at all, I'd recommend static linking to create
a portable executable. Note that dynamic linking is *NOT* recommended.

#### Statically

```sh
$ make all package
```

Just like under Linux, you can compile everything statically in, except SDL. To do that, use

```sh
$ USE_DYNSDL=1 make all package
```

NOTE: the linker will complain about resources, something like "duplicate leaf: type: 10 (VERSION)", this is okay, it
happens because all libraries try to set their own version info. Don't care, `tnge.exe` will be generated just fine.

#### Dynamically

Use MSYS2's pacman to install the dependencies (listed above in Linux's Dynamically section), then

```sh
$ USE_DYNLIBS=1 make all package
```

#### Dynamically with official SDL2

Just for completeness, this should not be needed, as MSYS2's pacman should provide the correct version of SDL2.

Download the mingw version of the SDL2 library from the official repo, [libsdl.org](http://libsdl.org/download-2.0.php).
After unpack, it should contain a `i686-w64-mingw32` subdirectory. Specify the directory where you have extracted:

```sh
$ USE_DYNLIBS=1 USE_MINGWSDL=../../SDL2-mingw make all package
```

### MacOS

Known to have issues with compiling the libraries, **officially not supported**, but dynamically linking with the SDL2 framework
from its official repo, [libsdl.org](http://libsdl.org/download-2.0.php) should work (download the dmg under section development
and extract it to `/Library/Frameworks`). Everything else must be statically compiled in, if you have issues with this, let me
know.

```sh
$ USE_DYNSDL=1 make all package
```

Feedback and patches are welcome.
