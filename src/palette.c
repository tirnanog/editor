/*
 * tnge/palette.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Palette functions
 *
 */

#include "main.h"

unsigned char *image_load(char *fn, int *w, int *h);

uint32_t *palette_cpals = NULL;
char **palette_list = NULL;
int palette_num = 0, palette_sel = -1, palette_scr, palette_cie = 0;

/**
 * Color conversion sRGB to HSV
 */
void palette_rgb2hsv(uint32_t c, int *h, int *s, int *v)
{
    int r = (int)(((uint8_t*)&c)[2]), g = (int)(((uint8_t*)&c)[1]), b = (int)(((uint8_t*)&c)[0]), m, d;

    m = r < g? r : g; if(b < m) m = b;
    *v = r > g? r : g; if(b > *v) *v = b;
    d = *v - m; *h = 0;
    if(!*v) { *s = 0; return; }
    *s = d * 255 / *v;
    if(!*s) return;

    if(r == *v) *h = 43*(g - b) / d;
    else if(g == *v) *h = 85 + 43*(b - r) / d;
    else *h = 171 + 43*(r - g) / d;
    if(*h < 0) *h += 256;
}

/**
 * Color conversion HSV to sRGB
 */
uint32_t palette_hsv2rgb(int a, int h, int s, int v)
{
    int i, f, p, q, t;
    uint32_t c = (a & 255) << 24;

    if(!s) { ((uint8_t*)&c)[2] = ((uint8_t*)&c)[1] = ((uint8_t*)&c)[0] = v; }
    else {
        if(h > 255) i = 0; else i = h / 43;
        f = (h - i * 43) * 6;
        p = (v * (255 - s) + 127) >> 8;
        q = (v * (255 - ((s * f + 127) >> 8)) + 127) >> 8;
        t = (v * (255 - ((s * (255 - f) + 127) >> 8)) + 127) >> 8;
        switch(i) {
            case 0:  ((uint8_t*)&c)[2] = v; ((uint8_t*)&c)[1] = t; ((uint8_t*)&c)[0] = p; break;
            case 1:  ((uint8_t*)&c)[2] = q; ((uint8_t*)&c)[1] = v; ((uint8_t*)&c)[0] = p; break;
            case 2:  ((uint8_t*)&c)[2] = p; ((uint8_t*)&c)[1] = v; ((uint8_t*)&c)[0] = t; break;
            case 3:  ((uint8_t*)&c)[2] = p; ((uint8_t*)&c)[1] = q; ((uint8_t*)&c)[0] = v; break;
            case 4:  ((uint8_t*)&c)[2] = t; ((uint8_t*)&c)[1] = p; ((uint8_t*)&c)[0] = v; break;
            default: ((uint8_t*)&c)[2] = v; ((uint8_t*)&c)[1] = p; ((uint8_t*)&c)[0] = q; break;
        }
    }
    return c;
}

/**
 * Convert color from sRGB to LAB (needed by CIE76 color matching)
 */
void palette_rgb2lab(unsigned char *rgb, float *L, float *A, float *B)
{
    float x, y, z, r, g, b;
    r = rgb[0] / 255.0; if(r > 0.04045) { r = __builtin_powf((r + 0.055) / 1.055, 2.4); } else { r /= 12.92; } r *= 100.0;
    g = rgb[1] / 255.0; if(g > 0.04045) { g = __builtin_powf((g + 0.055) / 1.055, 2.4); } else { g /= 12.92; } g *= 100.0;
    b = rgb[2] / 255.0; if(b > 0.04045) { b = __builtin_powf((b + 0.055) / 1.055, 2.4); } else { b /= 12.92; } b *= 100.0;
    x = (r*0.4124 + g*0.3576 + b*0.1805) /  95.047; if(x > 0.008856) x = __builtin_powf(x, 0.33333); else x = (7.787*x) + (16.0/116.0);
    y = (r*0.2126 + g*0.7152 + b*0.0722) / 100.000; if(y > 0.008856) y = __builtin_powf(y, 0.33333); else y = (7.787*y) + (16.0/116.0);
    z = (r*0.0193 + g*0.1192 + b*0.9505) / 108.883; if(z > 0.008856) z = __builtin_powf(z, 0.33333); else z = (7.787*z) + (16.0/116.0);
    *L = 116.0 * y - 16.0; *A = 500.0 * (x - y); *B = 200.0 * (y - z);
}

/**
 * Convert color from LAB to sRGB (needed by Adobe Palette readers)
 */
uint32_t palette_lab2rgb(float L, float A, float B)
{
    float y = (L + 16.0) / 116.0;
    float x = A / 500.0 + y;
    float z = y - B / 200.0;
    float r, g, b;
    x = 0.95047 * ((x * x * x > 0.008856) ? x * x * x : (x - 16/116) / 7.787);
    y = 1.00000 * ((y * y * y > 0.008856) ? y * y * y : (y - 16/116) / 7.787);
    z = 1.08883 * ((z * z * z > 0.008856) ? z * z * z : (z - 16/116) / 7.787);
    r = x *  3.2406 + y * -1.5372 + z * -0.4986; r = (r > 0.0031308) ? (1.055 * __builtin_powf(r, 1/2.4) - 0.055) : 12.92 * r;
    g = x * -0.9689 + y *  1.8758 + z *  0.0415; g = (g > 0.0031308) ? (1.055 * __builtin_powf(g, 1/2.4) - 0.055) : 12.92 * g;
    b = x *  0.0557 + y * -0.2040 + z *  1.0570; b = (b > 0.0031308) ? (1.055 * __builtin_powf(b, 1/2.4) - 0.055) : 12.92 * b;
    if(r > 1.0) { r = 1.0; } if(r < 0.0) r = 0.0;
    if(g > 1.0) { g = 1.0; } if(g < 0.0) g = 0.0;
    if(b > 1.0) { b = 1.0; } if(b < 0.0) b = 0.0;
    return 0xFF000000 | (((int)(r * 255.0) & 0xFF) << 0) | (((int)(g * 255.0) & 0xFF) << 8) | (((int)(b * 255.0) & 0xFF) << 16);
}

/**
 * Add a color to color map
 */
void palette_add(uint32_t *out, int r, int g, int b, int mc)
{
    int i, dr, dg, db, m, q;
    int64_t d, dm;
    unsigned char *pal = (unsigned char*)(&out[1]);

    for(q=1; q<=8; q++) {
        m=-1; dm=3*65536+1;
        for(i=0; (uint32_t)i<out[0]; i++) {
            if(r==pal[i*4+0] && g==pal[i*4+1] && b==pal[i*4+2]) return;
            if(r>>q==pal[i*4+0]>>q && g>>q==pal[i*4+1]>>q && b>>q==pal[i*4+2]>>q) {
                db = b - pal[i*4+2]; dg = g - pal[i*4+1]; dr = r - pal[i*4+0];
                d = dr*dr + dg*dg + db*db;
                if(d < dm) { dm = d; m = i; }
                if(!dm) break;
            }
        }
        if(dm>9+9+9 && i<mc) {
            pal[i*4+2] = b;
            pal[i*4+1] = g;
            pal[i*4+0] = r;
            out[0]++;
            return;
        }
        if(m>=0) {
            pal[m*4+2] = ((pal[m*4+2] + b) >> 1);
            pal[m*4+1] = ((pal[m*4+1] + g) >> 1);
            pal[m*4+0] = ((pal[m*4+0] + r) >> 1);
            return;
        }
    }
}

/**
 * Sort color map
 */
int palette_srt(const void *a, const void *b)
{
    uint8_t *A = (uint8_t*)a, *B = (uint8_t*)b;
    /* green is most significant, alpha is the least */
    return  ((((int)A[1]<<3) + ((int)A[0]<<1) + ((int)A[2]<<1) + 255)>>2) -
            ((((int)B[1]<<3) + ((int)B[0]<<1) + ((int)B[2]<<1) + 255)>>2);
}

/**
 * Get palette from file
 */
void palette_load(char *fn, uint32_t *out, int mc)
{
    FILE *f;
    char buf[16384], *s, *t;
    unsigned char *data = NULL, *d, *e;
    int i, w = 0, h = 0;
    float C,M,Y,K;
    uint32_t c;
    memset(out, 0, 257 * sizeof(uint32_t));
    if(mc > 256) mc = 256;
    f = project_fopen(fn, "rb");
    if(f) {
        memset(buf, 0, sizeof(buf));
        i = (int)fread(buf, sizeof(buf) - 1, 1, f);
        fclose(f);
        /* Adobe Photoshop Color File, no magic to identify... */
        s = strrchr(fn, '.');
        if(s && !strcmp(s, ".aco")) {
            if(verbose) printf("palette_load: ACO %s\n", fn);
            for(w = 0, data = (uint8_t*)buf + 4; w < (((uint8_t)buf[2] << 8) | (uint8_t)buf[3]) && w < 256 && out[0] < 256; w++) {
                c = 0;
                switch(data[1]) {
                    /* RGB color space */
                    case 0: c = 0xFF000000 | (data[2]) | (data[4] << 8) | (data[6] << 16); break;
                    /* HSV color space */
                    case 1: c = palette_hsv2rgb(0xFF, data[2], data[4], data[6]); break;
                    /* CMYK */
                    case 2:
                        C = (float)((data[2] << 8) | data[3]) / 655.35; M = (float)((data[4] << 8) | data[5]) / 655.35;
                        Y = (float)((data[6] << 8) | data[7]) / 655.35; K = (float)((data[8] << 8) | data[9]) / 655.35;
                        c = 0xFF000000 | (((int)(2.55 * C * K) & 0xFF) << 0) | (((int)(2.55 * M * K) & 0xFF) << 8) |
                            (((int)(2.55 * Y * K) & 0xFF) << 16);
                    break;
                    /* LAB */
                    case 7:
                        c = palette_lab2rgb((float)((data[2] << 8) | data[3]) / 10000.0,
                                    ((float)((data[4] << 8) | data[5]) + 12800.0) / 25600.0,
                                    ((float)((data[6] << 8) | data[7]) + 12800.0) / 25600.0);
                    break;
                    /* grayscale */
                    case 8:
                        h = ((int)(255.0 * (float)((data[2] << 8) | data[3]) / 10000.0)) & 0xFF;
                        c = 0xFF000000 | h | (h << 8) | (h << 16);
                    break;
                    /* wide CMYK */
                    case 9:
                        C = (float)((data[2] << 8) | data[3]) / 10000.0; M = (float)((data[4] << 8) | data[5]) / 10000.0;
                        Y = (float)((data[6] << 8) | data[7]) / 10000.0; K = (float)((data[8] << 8) | data[9]) / 10000.0;
                        c = 0xFF000000 | (((int)(255.0 * (1.0 - C) * (1.0 - K)) & 0xFF) << 0) |
                            (((int)(255.0 * (1.0 - M) * (1.0 - K)) & 0xFF) << 8) | (((int)(255.0 * (1.0 - Y) * (1.0 - K)) & 0xFF) << 16);
                    break;
                    default: if(verbose) printf("palette_load: unsupported color space %02x\n", data[1]); break;
                }
                data += 10;
                if(c) {
                    for(i = 0; (uint32_t)i < out[0] && out[i + 1] != c; i++);
                    if((uint32_t)i >= out[0]) out[++out[0]] = c;
                    if(out[0] == (uint32_t)mc) break;
                }
                /* skip over name */
                if(buf[1] == 2) data += 4 + data[3] * 2;
            }
        } else
        /* Adobe Swatch Exchange */
        if(!memcmp(buf, "ASEF", 4)) {
            if(verbose) printf("palette_load: ASEF %s\n", fn);
            data = (uint8_t*)buf;
            w = (data[10] << 16) | data[11]; /* number of blocks, we don't handle more than 65536 blocks */
            for(data += 12; w >= 0 && data < (uint8_t*)buf + sizeof(buf) && out[0] < 256; w--, data += h) {
                h = ((data[2] << 24) | (data[3] << 16) | (data[4] << 8) | data[5]) + 6;
                /* if this is a color block */
                if(data[0] == 0 && data[1] == 1) {
                    /* skip over name */
                    d = data + 12 + ((data[6] << 8) | data[7]) * 2;
                    c = 0;
                    switch(d[-4]) {
                        /* RGB color space */
                        case 'R':
                            e = (uint8_t*)&C; e[0] = d[3]; e[1] = d[2]; e[2] = d[1]; e[3] = d[0];
                            e = (uint8_t*)&M; e[0] = d[7]; e[1] = d[6]; e[2] = d[5]; e[3] = d[4];
                            e = (uint8_t*)&Y; e[0] = d[11]; e[1] = d[10]; e[2] = d[9]; e[3] = d[8];
                            c = 0xFF000000 | (((int)(255.0 * Y) & 0xFF) << 16) | (((int)(255.0 * M) & 0xFF) << 8) | ((int)(255.0 * C) & 0xFF);
                        break;
                        /* LAB */
                        case 'L':
                            e = (uint8_t*)&C; e[0] = d[3]; e[1] = d[2]; e[2] = d[1]; e[3] = d[0];
                            e = (uint8_t*)&M; e[0] = d[7]; e[1] = d[6]; e[2] = d[5]; e[3] = d[4];
                            e = (uint8_t*)&Y; e[0] = d[11]; e[1] = d[10]; e[2] = d[9]; e[3] = d[8];
                            c = palette_lab2rgb(C, M, Y);
                        break;
                        /* CMYK */
                        case 'C':
                            e = (uint8_t*)&C; e[0] = d[3]; e[1] = d[2]; e[2] = d[1]; e[3] = d[0];
                            e = (uint8_t*)&M; e[0] = d[7]; e[1] = d[6]; e[2] = d[5]; e[3] = d[4];
                            e = (uint8_t*)&Y; e[0] = d[11]; e[1] = d[10]; e[2] = d[9]; e[3] = d[8];
                            e = (uint8_t*)&K; e[0] = d[15]; e[1] = d[14]; e[2] = d[13]; e[3] = d[12];
                            c = 0xFF000000 | (((int)(255.0 * (1.0 - C) * (1.0 - K)) & 0xFF) << 0) |
                                (((int)(255.0 * (1.0 - M) * (1.0 - K)) & 0xFF) << 8) | (((int)(255.0 * (1.0 - Y) * (1.0 - K)) & 0xFF) << 16);
                        break;
                        /* grayscale */
                        case 'G':
                            e = (uint8_t*)&C; e[0] = d[3]; e[1] = d[2]; e[2] = d[1]; e[3] = d[0];
                            c = 0xFF000000 | (((int)(255.0 * C) & 0xFF) << 16) | (((int)(255.0 * C) & 0xFF) << 8) | ((int)(255.0 * C) & 0xFF);
                        break;
                        default:
                            if(verbose) printf("palette_load: unsupported color space '%c%c%c%c'\n", d[-4], d[-3], d[-2], d[-1]);
                        break;
                    }
                    if(c) {
                        for(i = 0; (uint32_t)i < out[0] && out[i + 1] != c; i++);
                        if((uint32_t)i >= out[0]) out[++out[0]] = c;
                        if(out[0] == (uint32_t)mc) break;
                    }
                }
            }
        } else
        /* JASC Microsoft Palette */
        if(!memcmp(buf, "JASC-PAL", 7)) {
            if(verbose) printf("palette_load: JASC-PAL %s\n", fn);
            s = buf;
            while(*s && *s != '\r' && *s != '\n') { s++; } while(*s == '\r' || *s == '\n') { s++; } /* skip JASC-PAL */
            while(*s && *s != '\r' && *s != '\n') { s++; } while(*s == '\r' || *s == '\n') { s++; } /* skip 0100 */
            while(*s && *s != '\r' && *s != '\n') { s++; } while(*s == '\r' || *s == '\n') { s++; } /* skip 256 */
            goto readtxt;
        } else
        /* GIMP Palette */
        if(!memcmp(buf, "GIMP", 4) || !memcmp(buf, PROJMAGIC_PALETTE, strlen(PROJMAGIC_PALETTE))) {
            if(verbose && fn != projdir) printf("palette_load: GIMP Palette %s\n", fn);
            s = buf;
readtxt:    for(; *s && s < buf + sizeof(buf) && out[0] < 256; s++) {
                if(*s == '\r' || *s == '\n') continue;
                if(*s == ' ' || (*s >= '0' && *s <= '9')) {
                    while(*s && *s == ' ') s++;
                    c = (atoi(s) & 0xFF) | 0xFF000000;
                    while(*s && *s >= '0' && *s <= '9') s++;
                    while(*s && *s == ' ') s++;
                    c |= (atoi(s) & 0xFF) << 8;
                    while(*s && *s >= '0' && *s <= '9') s++;
                    while(*s && *s == ' ') s++;
                    c |= (atoi(s) & 0xFF) << 16;
                    for(i = 0; (uint32_t)i < out[0] && out[i + 1] != c; i++);
                    if((uint32_t)i >= out[0]) out[++out[0]] = c;
                    if(out[0] == (uint32_t)mc) break;
                }
                while(*s && *s != '\r' && *s != '\n') s++;
            }
        } else
        /* GIF with global color table */
        if(!memcmp(buf, "GIF8", 4) && (buf[0xA] & 0x80)) {
            if(verbose) printf("palette_load: GIF %s\n", fn);
            out[0] = 1 << ((buf[0xA] & 3) + 1);
            if(out[0] > (uint32_t)mc) out[0] = mc;
            if(out[0] < (uint32_t)mc) mc = out[0];
            for(i = 0; i < mc; i++)
                out[i + 1] = buf[0xD + i * 3] | (buf[0xE + i * 3] << 8) | (buf[0xF + i * 3] << 16) | 0xFF000000;
        } else
        /* Paint.NET, no real magic either */
        if(buf[0] == ';' || (buf[0] >= '0' && buf[0] <= '9') || (buf[0] >= 'a' && buf[0] <= 'f') || (buf[0] >= 'A' && buf[0] <= 'F')) {
            for(; *s && s < buf + sizeof(buf) && out[0] < 256; s++) {
                if(*s == ';') { while(*s && *s != '\r' && *s != '\n') s++; }
                if(*s == ' ' || *s == '\r' || *s == '\n') continue;
                for(t = s; *t && *t != ' ' && *t != '\r' && *t != '\n'; t++);
                if(t - s == 8) { c = (gethex(s, 2) << 24); s += 2; } else c = 0xFF000000;
                c |= gethex(s, 2) | (gethex(s + 2, 2) << 8) | (gethex(s + 4, 2) << 16);
                while(*s && *s != '\r' && *s != '\n') s++;
                for(i = 0; (uint32_t)i < out[0] && out[i + 1] != c; i++);
                if((uint32_t)i >= out[0]) out[++out[0]] = c;
                if(out[0] == (uint32_t)mc) break;
            }
        } else
        /* we have to do it the hard way */
        if(buf[0] || buf[1] || buf[2] || buf[3]) {
            if((data = image_load(fn, &w, &h)) && w > 0 && h > 0) {
                if(verbose) printf("palette_load: quantizing image %s %d x %d\n", fn, w, h);
                for(i = 0; i < w * h * 4 && out[0] < 256; i += 4)
                    palette_add(out, data[i], data[i + 1], data[i + 2], mc);
                qsort(&out[1], out[0], sizeof(uint32_t), palette_srt);
            }
            if(data) free(data);
        }
        if(!out[0] && verbose) printf("palette_load: unable to parse %s\n", fn);
    }
}

/**
 * Read palette from a file and save as a GIMP palette
 */
int palette_save(char *fn, int mc)
{
    FILE *f;
    char *s = strrchr(fn, SEP[0]), *n;
    uint32_t pal[257];
    int i;
    palette_load(fn, pal, mc);
    if(!pal[0]) return 0;
    if(mc > 256) mc = 256;
    if(pal[0] < (uint32_t)mc) mc = pal[0];
    /* generate gpl filename with path */
    if(!s) s = fn; else s++;
    strcpy(projfn, "." COMMONDIR);
    project_mkdir(projdir);
    strcpy(projfn, "." COMMONDIR SEP);
    n = projfn + strlen(projfn);
    strcpy(n, s);
    s = strrchr(n, '.');
    if(!s) s = projfn + strlen(projfn);
    strcpy(s, ".gpl");
    if(verbose) printf("palette_save: saving %s with %d colors\n", projdir, mc);
    f = project_fopen(projdir, "wb+");
    if(f) {
        fn = malloc(strlen(n) + 1);
        if(fn) strcpy(fn, n);
        *s = 0;
        fprintf(f, "%s\r\nName: %s\r\n#\r\n", PROJMAGIC_PALETTE, projfn + 8);
        for(i = 0; i < mc; i++)
            fprintf(f, "%3d %3d %3d\r\n", pal[i + 1] & 0xFF, (pal[i + 1] >> 8) & 0xFF, (pal[i + 1] >> 16) & 0xFF);
        fclose(f);
        palette_init();
        if(fn) {
            for(palette_sel = 0; palette_sel < palette_num && palette_list && strcmp(fn, palette_list[palette_sel]); palette_sel++);
            free(fn);
        }
        *projfn = 0;
        return 1;
    }
    *projfn = 0;
    return 0;
}

/**
 * Delete a palette
 */
void palette_del(int idx)
{
    if(idx < 0 || idx >= palette_num || !palette_list || !palette_list[idx] || !palette_cpals) return;
    strcpy(projfn, "." COMMONDIR SEP);
    strcat(projfn, palette_list[idx]);
    strcat(projfn, ".gpl");
    if(verbose) printf("palette_del: removing %s\n", projdir);
    remove(projdir);
    free(palette_list[idx]);
    memcpy(&palette_list[idx], &palette_list[idx + 1], (palette_num - idx) * sizeof(char*));
    memcpy(&palette_cpals[idx * 257], &palette_cpals[(idx + 1) * 257], (palette_num - idx) * 257 * sizeof(uint32_t));
    if(palette_sel) palette_sel--;
    palette_num--;
    if(!palette_num) palette_free();
}

/**
 * Free palette list
 */
void palette_free()
{
    if(palette_cpals) { free(palette_cpals); palette_cpals = NULL; }
    project_freedir(&palette_list, &palette_num);
    palette_scr = 0; palette_sel = -1;
}

/**
 * Load palette list
 */
void palette_init()
{
    char *fn;
    int i;

    palette_free();
    strcpy(projfn, "." COMMONDIR);
    palette_list = project_getdir(projdir, ".gpl", &palette_num);
    if(palette_list && palette_list[0] && palette_list[0][0]) {
        palette_cpals = (uint32_t*)main_alloc(palette_num * 257 * sizeof(uint32_t));
        fn = projfn + strlen(projfn); *fn++ = SEP[0];
        for(i = 0; i < palette_num; i++) {
            strcpy(fn, palette_list[i]);
            strcat(fn, ".gpl");
            palette_load(projdir, &palette_cpals[i * 257], 256);
        }
    }
}

/**
 * Display palette list
 */
void palette_show(SDL_Texture *dst, int x, int y, int pressed)
{
    uint32_t B = 0xFF000000 | ((theme[THEME_BG] + 0x0A0A0A) & 0x7F7F7F);
    int i, j, w = ui_clip.w;

    ui_box2(dst, x, y + 18, 256+6+13, 200 + 2, theme[THEME_DARKER], theme[THEME_BG], theme[THEME_LIGHTER], 0, 0, 20, 0);
    if(palette_cpals && palette_num) {
        if(palette_sel > palette_num) palette_sel = -1;
        if(palette_sel < palette_scr) palette_scr = palette_sel;
        if(palette_sel > palette_scr + 10) palette_scr = palette_sel - 10;
        if(palette_scr < 0) palette_scr = 0;
        ui_clip.w = x + 256 - ui_clip.x;
        ui_text(dst, x + 3, y, palette_list && palette_sel >= 0 && palette_sel < palette_num ?
            palette_list[palette_sel] : NULL);
        ui_clip.w = w;
        for(j = y + 21, i = palette_scr; i < palette_num && j < y + 203; i++, j += 20) {
            if(i == palette_sel && !pressed)
                ui_box(dst, x + 1, j - 2, 256+4+8, 20, theme[THEME_SELBG], 0xFF000000, theme[THEME_SELBG]);
            ui_pal(dst, x + 7, j, palette_cpals[i * 257], &palette_cpals[i * 257 + 1], i == palette_sel && !pressed);
        }
        if(palette_num < 10)
            ui_box(dst, x + 256+6+8, y + 19, 4, 200, B, B, B);
        else {
            ui_box(dst, x + 256+6+8, y + 19, 4, 200, theme[THEME_INPBG], theme[THEME_INPBG], theme[THEME_INPBG]);
            i = palette_scr;
            if(i + 10 > palette_num) i = palette_num - 10;
            ui_box(dst, x + 256+6+8, y + 19 + 180 * i / palette_num, 4, 20 + 180 * 10 / palette_num, B, B, B);
        }
    }
}
