/*
 * tnge/spawners.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief NPC spawners window
 *
 */

#define STBI_NO_LINEAR
#define STBI_NO_HDR
#define STBI_NO_FAILURE_STRINGS
#include "stb_image.h"
#include "main.h"

typedef struct {
    int npc;
    int anim;
    int rattr;
    int rel;
    int rval;
    int pattr;
    int pval;
} spawners_npc_t;

void spawners_load(void *data);
void spawners_new(void *data);
void spawners_save(void *data);
void spawners_delete(void *data);
void spawners_drawlst(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void spawners_add(void *data);
void spawners_erase(void *data);
void spawners_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void spawners_rendercell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

SDL_Texture *spawners_anims = NULL;
char spawners_name[PROJ_NAMEMAX];
int spawners_attrlen;
ui_input_t spawners_nameinp = { INP_ID, sizeof(spawners_name), spawners_name };
ui_num_t spawners_num = { 1, 1, 16, 1 };
ui_num_t spawners_freq = { 6000, 100, 360000, 100 };
ui_select_t spawners_req = { -1, LANG_NONE, NULL };
ui_select_t spawners_rel = { 0, 0, project_rels };
ui_num_t spawners_rval = { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 };
ui_select_t spawners_prov = { -1, LANG_NONE, NULL };
ui_num_t spawners_pval = { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 };
ui_sprsel_t spawners_anim = { -1, 2, 185, 1 };
ui_sprsel_t spawners_npc = { 0, PROJ_NUMSPRCATS - 2, 185, 1 };

ui_tablehdr_t spawners_hdr[] = {
    { SUBMENU_SPAWNERS, 0, 0, 0 },
    { 0 }
};
ui_table_t spawners_tbl = { spawners_hdr, SPAWNERS_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(ui_sprlist_t),
    spawners_drawlst, NULL, NULL, NULL };

ui_tablehdr_t spawners_npcshdr[] = {
    { SPAWNERS_REQUIRE, 0, 0, 0 },
    { SPAWNERS_PROVIDE, 0, 0, 0 },
    { SPAWNERS_NPCKIND, 0, 0, 0 },
    { 0 }
};
ui_table_t spawners_npcs = { spawners_npcshdr, SPAWNERS_NONPC, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(spawners_npc_t),
    spawners_drawcell, spawners_rendercell, NULL, NULL };

/**
 * The form
 */
ui_form_t spawners_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, SPAWNERS_DELETE, (void*)ICON_REMOVE, spawners_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, spawners_save },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, spawners_new },
    { FORM_INPUT, 0, 30, 200, 18, 0, SPAWNERS_NAME, &spawners_nameinp, NULL },
    { FORM_TABLE, 10, 30, 200, 0, 0, 0, &spawners_tbl, spawners_load },
    /* 5 */
    { FORM_TEXT, 0, 55, 0, 18, 0, 0, NULL, NULL },
    { FORM_NUM, 0, 54, 42, 20, 0, SPAWNERS_NUMNPC, &spawners_num, NULL },
    { FORM_TEXT, 0, 55, 0, 18, 0, 0, NULL, NULL },
    { FORM_TIME, 0, 54, 80, 20, 0, SPAWNERS_FREQ, &spawners_freq, NULL },
    /* 9 */
    { FORM_TABLE, 0, 78, 0, 0, 0, 0, &spawners_npcs, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, SPAWNERS_ERASE, (void*)ICON_REMOVE, spawners_erase },
    /* 11 */
    { FORM_SELECT, 0, 0, 0, 20, 0, SPAWNERS_SREQUIRE, &spawners_req, NULL },
    { FORM_SELECT, 0, 0, 42, 20, 0, SPAWNERS_SREQUIRE, &spawners_rel, NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, SPAWNERS_SREQUIRE, &spawners_rval, NULL },
    { FORM_SELECT, 0, 0, 0, 20, 0, SPAWNERS_SPROVIDE, &spawners_prov, NULL },
    { FORM_TEXT, 0, 0, 16, 18, 0, SPAWNERS_SPROVIDE, ":=", NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, SPAWNERS_SPROVIDE, &spawners_pval, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, SPAWNERS_ANIM, &spawners_anim, NULL },
    { FORM_SPRITE, 0, 0, 0, 20, 0, SPAWNERS_NPCKIND, &spawners_npc, NULL },
    /* 19 */
    { FORM_ICONBTN, 0, 0, 20, 20, 0, SPAWNERS_ADD, (void*)ICON_ADD, spawners_add },

    { FORM_LAST }
};

/**
 * Draw table cell
 */
void spawners_drawlst(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    ui_sprlist_t *swr = (ui_sprlist_t*)data;

    (void)idx; (void)sel; (void)w; (void)h; (void)hdr;
    if(!data) return;
    ui_text(dst, x + 4, y + 1, swr->name);
}

/**
 * Draw table cell
 */
void spawners_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    spawners_npc_t *npc = (spawners_npc_t*)data;
    ui_sprite_t *s;
    SDL_Rect rect;
    char tmp[128];
    int t = ui_clip.w;

    (void)idx; (void)sel; (void)w; (void)h;
    if(!data) return;
    if(npc->rattr >= 0) {
        ui_clip.w = hdr[0].w;
        sprintf(tmp, "%s%s%d", project.attrs[npc->rattr], project_rels[npc->rel], npc->rval);
        ui_text(dst, x + 4, y + 1, tmp);
    }
    if(npc->pattr >= 0) {
        ui_clip.w = hdr[0].w + hdr[1].w;
        sprintf(tmp, "%s:=%d", project.attrs[npc->pattr], npc->pval);
        ui_text(dst, x + 4 + hdr[0].w, y + 1, tmp);
    }
    ui_clip.w = t;
    s = spr_getidx(spawners_anim.cat, npc->anim, -1, 0);
    if(!s) s = spr_getidx(spawners_npc.cat, npc->npc, -1, 0);
    if(s) {
        if(sel != idx) {
            rect.x = x + 4 + hdr[0].w + hdr[1].w; rect.y = y + 1;
            ui_fit(16, 16, s->w, s->h, &rect.w, &rect.h);
            spr_blit(s, 160, dst, &rect);
        } else
            spr_texture(s, &spawners_anims);
    }
    ui_text(dst, x + 24 + hdr[0].w + hdr[1].w, y + 1, project.sprites[PROJ_NUMSPRCATS - 2][npc->npc].name);
}

/**
 * Draw table cell
 */
void spawners_rendercell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    spawners_npc_t *npc = (spawners_npc_t*)data;
    ui_sprite_t *s;
    SDL_Rect rect;

    (void)dst; (void)idx; (void)sel; (void)w; (void)h;
    if(!data || !spawners_anims || sel != idx) return;
    rect.x = x + 4 + hdr[0].w + hdr[1].w; rect.y = y + 1; rect.w = rect.h = 16;
    s = spr_getidx(spawners_anim.cat, npc->anim, -1, 0);
    if(!s) s = spr_getidx(spawners_npc.cat, npc->npc, -1, 0);
    spr_render(s, -1, spawners_anims, &rect);
}

/**
 * Exit spawners window
 */
void spawners_exit(int tab)
{
    (void)tab;
    spawners_tbl.clk = -1;
    if(spawners_anims)
        SDL_DestroyTexture(spawners_anims);
    spawners_anims = NULL;
}

/**
 * Enter spawners window
 */
void spawners_init(int tab)
{
    int i, j;

    spawners_exit(tab);
    spawners_form[1].param = lang[LANG_SAVE];
    spawners_form[1].w = ui_textwidth(spawners_form[1].param) + 40;
    if(spawners_form[1].w < 200) spawners_form[1].w = 200;
    spawners_form[5].param = lang[SPAWNERS_NUMNPC];
    spawners_form[7].param = lang[SPAWNERS_FREQ];
    spawners_form[5].x = spawners_form[9].x = spawners_form[10].x = spawners_form[4].x + spawners_form[4].w + 10;
    if(!spawners_name[0]) spawners_tbl.val = -1;
    spawners_tbl.data = project.sprites[PROJ_NUMSPRCATS - 1];
    spawners_tbl.num = project.spritenum[PROJ_NUMSPRCATS - 1];
    if(spawners_tbl.val >= spawners_tbl.num) spawners_tbl.val = spawners_tbl.num - 1;
    spawners_req.opts = spawners_prov.opts = project.attrs;
    for(i = spawners_attrlen = 0; i < project.numattrs; i++) {
        j = ui_textwidth(project.attrs[i]);
        if(j > spawners_attrlen) spawners_attrlen = j;
    }
    spawners_attrlen += 24;
}

/**
 * Resize the view
 */
void spawners_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    spawners_form[0].y = spawners_form[1].y = spawners_form[2].y = h - 48;
    spawners_form[1].x = w - 20 - spawners_form[1].w;
    ui_table_resize(&spawners_form[4], spawners_form[4].w, h - 89);
    spawners_form[2].x = spawners_form[4].x + spawners_form[4].w + 20;
    spawners_form[3].x = w - 10 - spawners_form[3].w;
    i = (w - 10 - spawners_form[5].x) / 2;
    spawners_form[6].x = spawners_form[5].x + i - spawners_form[6].w - 10;
    spawners_form[5].w = spawners_form[6].x - spawners_form[5].x - 4;
    spawners_form[7].x = spawners_form[5].x + i;
    spawners_form[8].x = w - 10 - spawners_form[8].w;
    spawners_form[7].w = spawners_form[8].x - spawners_form[7].x - 4;
    ui_table_resize(&spawners_form[9], w - 10 - spawners_form[9].x, h - 82 - spawners_form[9].y);
    for(i = 10; spawners_form[i].type; i++)
        spawners_form[i].y = h - 48 - 30;
    spawners_form[19].x = w - 10 - spawners_form[19].w;
    spawners_form[11].x = spawners_form[10].x + spawners_form[10].w + 20;
    spawners_form[11].w = spawners_form[14].w = spawners_form[18].w = (spawners_form[19].x - spawners_form[11].x -
        spawners_form[12].w - spawners_form[13].w - spawners_form[15].w - spawners_form[16].w - spawners_form[18].w - 48) / 3;
    spawners_form[12].x = spawners_form[11].x + spawners_form[11].w + 2;
    spawners_form[13].x = spawners_form[12].x + spawners_form[12].w + 2;
    spawners_form[14].x = spawners_form[13].x + spawners_form[13].w + 10;
    spawners_form[15].x = spawners_form[14].x + spawners_form[14].w + 2;
    spawners_form[16].x = spawners_form[15].x + spawners_form[15].w + 2;
    spawners_form[17].x = spawners_form[16].x + spawners_form[16].w + 10;
    spawners_form[18].x = spawners_form[17].x + spawners_form[17].w + 4;
    spawners_form[18].w = spawners_form[19].x - spawners_form[18].x - 10;
}

/**
 * View layer
 */
void spawners_redraw(int tab)
{
    (void)tab;
    ui_form = spawners_form;
    ui_form[0].inactive = (spawners_tbl.val < 0);
    ui_form[1].inactive = (!spawners_name[0]);
    ui_form[12].inactive = ui_form[13].inactive = (spawners_req.val < 0);
    ui_form[15].inactive = ui_form[16].inactive = (spawners_prov.val < 0);
}

/**
 * Clear form, new spawner
 */
void spawners_new(void *data)
{
    (void)data;
    spawners_name[0] = 0;
    spawners_num.val = 1;
    spawners_freq.val = 6000;
    if(spawners_npcs.data) { free(spawners_npcs.data); spawners_npcs.data = NULL; }
    spawners_npcs.num = spawners_rval.val = spawners_pval.val = spawners_npc.val = 0;
    spawners_npcs.clk = spawners_npcs.val = spawners_req.val = spawners_prov.val = spawners_anim.val = -1;
}

/**
 * Erase NPC kind from list
 */
void spawners_erase(void *data)
{
    spawners_npc_t *list = (spawners_npc_t*)spawners_npcs.data;

    (void)data;
    if(!list || spawners_npcs.val < 0 || spawners_npcs.val >= spawners_npcs.num) return;
    memcpy(&list[spawners_npcs.val], &list[spawners_npcs.val + 1], (spawners_npcs.num - spawners_npcs.val) *
        sizeof(spawners_npc_t));
    spawners_npcs.num--;
    if(spawners_npcs.val >= spawners_npcs.num)
        spawners_npcs.val = spawners_npcs.num - 1;
    if(!spawners_npcs.num) { free(list); spawners_npcs.data = NULL; }
}

/**
 * Add NPC kind to list
 */
void spawners_add(void *data)
{
    spawners_npc_t *list = (spawners_npc_t*)spawners_npcs.data;
    int i;

    (void)data;
    if(spawners_npc.val < 0) return;
    list = (spawners_npc_t*)realloc(list, (spawners_npcs.num + 1) * sizeof(spawners_npc_t));
    spawners_npcs.data = list;
    if(!list) main_error(ERR_MEM);
    i = spawners_npcs.num++;
    list[i].npc = spawners_npc.val;
    list[i].anim = spawners_anim.val;
    list[i].rattr = spawners_req.val;
    list[i].rel = spawners_rel.val;
    list[i].rval = spawners_rval.val;
    list[i].pattr = spawners_prov.val;
    list[i].pval = spawners_pval.val;
}

/**
 * Delete spawner
 */
void spawners_delete(void *data)
{
    (void)data;
    if(spawners_tbl.val < 0 || spawners_tbl.val >= project.spritenum[PROJ_NUMSPRCATS - 1]) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.swr", project.id, project_dirs[PROJDIRS_NPCS],
        project.sprites[PROJ_NUMSPRCATS - 1][spawners_tbl.val].name);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("spawners_delete: removing %s\n", projdir);
        remove(projdir);
        sprintf(projfn, "%s" SEP "%s" SEP "%s_swr.png", project.id, project_dirs[PROJDIRS_NPCS],
            project.sprites[PROJ_NUMSPRCATS - 1][spawners_tbl.val].name);
        if(verbose) printf("spawners_delete: removing %s\n", projdir);
        remove(projdir);
        spawners_new(NULL);
        spawners_init(SUBMENU_SPAWNERS);
    }
}

/**
 * Save spawner
 */
void spawners_save(void *data)
{
    void *pixels;
    uint32_t *out, *spawner;
    spawners_npc_t *npcs = (spawners_npc_t*)spawners_npcs.data;
    ui_sprite_t *s;
    FILE *f;
    int i, j, k, mw = project.tilew, mh = project.tileh;

    (void)data;
    if(!spawners_name[0]) return;
    f = project_savefile(spawners_tbl.val < 0 || spawners_tbl.val >= project.spritenum[PROJ_NUMSPRCATS - 1] ||
        strcmp(project.sprites[PROJ_NUMSPRCATS - 1][spawners_tbl.val].name, spawners_name),
        project_dirs[PROJDIRS_NPCS], spawners_name, "swr", PROJMAGIC_SPAWNER, "spawners_save");
    if(f) {
        fprintf(f, "%u %u\r\n", spawners_num.val, spawners_freq.val);
        for(i = 0; i < project.spritenum[PROJ_NUMSPRCATS - 2]; i++)
            project.sprites[PROJ_NUMSPRCATS - 2][i].idx = 0;
        for(i = 0; i < spawners_npcs.num; i++) {
            if(npcs[i].rattr >= 0) {
                project_wridx(f, npcs[i].rattr, project.attrs);
                fprintf(f, "%s%d ", project_rels[npcs[i].rel], npcs[i].rval);
            } else
                fprintf(f, "- ");
            if(npcs[i].pattr >= 0) {
                project_wridx(f, npcs[i].pattr, project.attrs);
                fprintf(f, "=%d ", npcs[i].pval);
            } else
                fprintf(f, "- ");
            project_wrsprite2(f, npcs[i].anim, spawners_anim.cat); fprintf(f, " ");
            project_wrsprite2(f, npcs[i].npc, spawners_npc.cat);
            if(npcs[i].npc >= 0)
                project.sprites[PROJ_NUMSPRCATS - 2][npcs[i].npc].idx++;
            fprintf(f, "\r\n");
        }
        fclose(f);
        /* save spawner as separate image file too because tmx needs to reference it */
        for(i = j = k = 0; i < project.spritenum[PROJ_NUMSPRCATS - 2]; i++)
            if(project.sprites[PROJ_NUMSPRCATS - 2][i].idx > k) { j = i; k = project.sprites[PROJ_NUMSPRCATS - 2][i].idx; }
        s = &project.sprites[PROJ_NUMSPRCATS - 2][j].dir[0];
        /* rescale the "spawner egg" icon to the sprite's size */
        spawner = (uint32_t*)main_alloc(spawner_icon.w * spawner_icon.h * 4);
        SDL_LockTexture(icons, NULL, (void**)&pixels, &i);
        ui_combine(spawner, 0, 0, spawner_icon.w, spawner_icon.h, spawner_icon.w * 4, pixels, spawner_icon.x, spawner_icon.y,
            spawner_icon.w, spawner_icon.h, i);
        SDL_UnlockTexture(icons);
        ui_fit(s->w, s->h, spawner_icon.w / 3, spawner_icon.h, &mw, &mh);
        pixels = (uint32_t*)main_alloc(mw * 3 * mh * 4);
        ui_resample(pixels, 0, 0, mw * 3, mh, mw * 3 * 4, 255, spawner, spawner_icon.w, spawner_icon.h, spawner_icon.w * 4);
        /* merge egg layers with the sprite */
        out = (uint32_t*)main_alloc(s->w * s->h * 4);
        ui_combine(out, (s->w - mw) / 2, s->h - mh, mw, mh, s->w * 4, pixels, 0, 0, mw, mh, mw * 3 * 4);
        ui_combine(out, 0, 0, s->w, s->h, s->w * 4, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
        ui_combine(out, (s->w - mw) / 2, s->h - mh, mw, mh, s->w * 4, pixels, mw, 0, mw, mh, mw * 3 * 4);
        ui_combine(out, (s->w - mw) / 2, 0, mw, mh, s->w * 4, pixels, 2 * mw, 0, mw, mh, mw * 3 * 4);
        free(pixels);
        sprintf(projfn, "%s" SEP "%s" SEP "%s_swr.png", project.id, project_dirs[PROJDIRS_NPCS], spawners_name);
        f = project_fopen(projdir, "wb");
        if(f) {
            if(verbose) printf("spawners_save: saving %s\n", projdir);
            if(image_save(f, s->w, s->h, s->w * 4, (uint8_t*)out))
                spr_add(PROJ_NUMSPRCATS - 1, spawners_name, 0, 0, 1, s->w, s->h, s->w * 4, out);
            fclose(f);
        }
        free(out);
        spawners_init(SUBMENU_SPAWNERS);
        for(spawners_tbl.val = 0; spawners_tbl.val < project.spritenum[PROJ_NUMSPRCATS - 1] &&
            strcmp(spawners_name, project.sprites[PROJ_NUMSPRCATS - 1][spawners_tbl.val].name); spawners_tbl.val++);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.swr", project.id, project_dirs[PROJDIRS_NPCS], spawners_name);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVESWR]);
}

/**
 * Load spawner
 */
int spawners_loadswr(int idx)
{
    char *str, *s;

    str = project_loadfile(project_dirs[PROJDIRS_NPCS], project.sprites[PROJ_NUMSPRCATS - 1][idx].name, "swr",
        PROJMAGIC_SPAWNER, "spawners_loadswr");
    if(str) {
        spawners_new(NULL);
        s = project_skipnl(str);
        s = project_getint(s, &spawners_num.val, spawners_num.min, spawners_num.max);
        s = project_getint(s, &spawners_freq.val, spawners_freq.min, spawners_freq.max);
        while(*s) {
            s = project_skipnl(s);
            if(!*s) break;
            spawners_req.val = spawners_prov.val = spawners_npc.val = -1; spawners_rel.val = spawners_rval.val =
                spawners_pval.val = 0;
            if(*s != '-') {
                s = project_getidx(s, &spawners_req.val, project.attrs, -1);
                s = project_getrel(s, &spawners_rel.val);
                s = project_getint(s, &spawners_rval.val, spawners_rval.min, spawners_rval.max);
            } else s++;
            while(*s == ' ') s++;
            if(*s != '-') {
                s = project_getidx(s, &spawners_prov.val, project.attrs, -1);
                if(*s == '=')
                    s = project_getint(s + 1, &spawners_pval.val, spawners_pval.min, spawners_pval.max);
            } else s++;
            s = project_getsprite(s, &spawners_anim);
            s = project_getsprite(s, &spawners_npc);
            spawners_add(NULL);
        }
        spawners_req.val = spawners_prov.val = spawners_anim.val = -1;
        spawners_rel.val = spawners_rval.val = spawners_pval.val = spawners_npc.val = 0;
        free(str);
        strncpy(spawners_name, project.sprites[PROJ_NUMSPRCATS - 1][idx].name, sizeof(spawners_name) - 1);
        return 1;
    }
    return 0;
}

/**
 * Load spawner
 */
void spawners_load(void *data)
{
    (void)data;
    if(spawners_tbl.val < 0 || spawners_tbl.val >= project.spritenum[PROJ_NUMSPRCATS - 1]) return;
    if(!spawners_loadswr(spawners_tbl.val))
        ui_status(1, lang[ERR_LOADSWR]);
}

/**
 * Get references
 */
void spawners_ref(tngctx_t *ctx, int idx)
{
    ui_sprsel_t npc = { 0, PROJ_NUMSPRCATS - 2, 185, 1 };
    char *str, *s;
    int j;

    if(!tng_sprref(ctx, PROJ_NUMSPRCATS - 1, idx)) return;
    str = project_loadfile(project_dirs[PROJDIRS_NPCS], project.sprites[PROJ_NUMSPRCATS - 1][idx].name, "swr",
        PROJMAGIC_SPAWNER, "spawners_ref");
    if(str) {
        s = project_skipnl(str);
        while(*s) {
            s = project_skipnl(s);
            if(!*s) break;
            if(*s != '-') {
                s = project_getidx(s, &j, project.attrs, -1);
                attrs_ref(ctx, j);
                s = project_skiparg(s, 1);
            } else s++;
            while(*s == ' ') s++;
            if(*s != '-') {
                s = project_getidx(s, &j, project.attrs, -1);
                attrs_ref(ctx, j);
                s = project_skiparg(s, 1);
            }
            s = project_skiparg(s, 1);
            s = project_getsprite(s, &npc);
            npcs_ref(ctx, npc.val);
        }
        free(str);
    }
}

/**
 * Save tng
 */
int spawners_totng(tngctx_t *ctx)
{
    spawners_npc_t *npcs;
    uLongf cl;
    uint8_t *buf, *ptr, *comp;
    int i, j, l, m;

    if(!ctx || !project.sprites[PROJ_NUMSPRCATS - 1] || !project.spritenum[PROJ_NUMSPRCATS - 1]) return 1;
    /* check if there's at least one spawner referenced */
    for(j = 0; j < project.spritenum[PROJ_NUMSPRCATS - 1] && project.sprites[PROJ_NUMSPRCATS - 1][j].idx == -1; j++);
    if(j >= project.spritenum[PROJ_NUMSPRCATS - 1]) return 1;
    /* add spawners to output */
    tng_section(ctx, TNG_SECTION_SPAWNERS);
    for(m = 0; m < project.spritenum[PROJ_NUMSPRCATS - 1]; m++) {
        for(j = 0; j < project.spritenum[PROJ_NUMSPRCATS - 1] && project.sprites[PROJ_NUMSPRCATS - 1][j].idx != m; j++);
        if(j >= project.spritenum[PROJ_NUMSPRCATS - 1]) break;
        if(!spawners_loadswr(j)) {
            ui_switchtab(SUBMENU_SPAWNERS);
            spawners_tbl.val = j;
            ui_status(1, lang[ERR_LOADSWR]);
            return 0;
        }
        npcs = (spawners_npc_t*)spawners_npcs.data;
        l = 6 + spawners_npcs.num * 21;
        buf = ptr = (uint8_t*)main_alloc(l);
        *ptr++ = spawners_num.val;
        memcpy(ptr, &spawners_freq.val, 3); ptr += 3;
        memcpy(ptr, &spawners_npcs.num, 2); ptr += 2;
        for(i = 0; i < spawners_npcs.num; i++) {
            ptr = tng_asset_idx(ptr, ctx, project.attrs, TNG_IDX_ATR, npcs[i].rattr);
            *ptr++ = npcs[i].rel;
            memcpy(ptr, &npcs[i].rval, 4); ptr += 4;
            ptr = tng_asset_idx(ptr, ctx, project.attrs, TNG_IDX_ATR, npcs[i].pattr);
            memcpy(ptr, &npcs[i].pval, 4); ptr += 4;
            ptr = tng_asset_sprite(ptr, ctx, spawners_anim.cat, npcs[i].anim);
            ptr = tng_asset_sprite(ptr, ctx, spawners_npc.cat, npcs[i].npc);
        }
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        if(cl) {
            tng_desc(ctx, project.sprites[PROJ_NUMSPRCATS - 1][j].name, cl);
            tng_asset_int(ctx, comp, cl);
        } else
            free(comp);
        free(buf);
        /* free resources */
        spawners_new(NULL);
    }
    return 1;
}

/**
 * Read from tng
 */
int spawners_fromtng(tngctx_t *ctx)
{
    FILE *f;
    stbi__context s;
    stbi__result_info ri;
    ui_sprite_t *spr;
    tng_asset_desc_t *asset;
    uint8_t *buf, *ptr, *end;
    uint32_t *out, *pixels, *spawner;
    int i, j, k, l, len, n, r;
    int mw = project.tilew, mh = project.tileh, w, h;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_SPAWNERS; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;

    /* get icons, do not use SDL_LockTexture here */
    memset(&s, 0, sizeof(s));
    memset(&ri, 0, sizeof(ri));
    s.read_from_callbacks = 0;
    s.img_buffer = s.img_buffer_original = binary_icons_png;
    s.img_buffer_end = s.img_buffer_original_end = binary_icons_png + sizeof(binary_icons_png);
    ri.bits_per_channel = 8;
    pixels = (uint32_t*)stbi__png_load(&s, (int*)&w, (int*)&h, (int*)&r, 4, &ri);
    spawner = (uint32_t*)main_alloc(spawner_icon.w * spawner_icon.h * 4);
    ui_combine(spawner, 0, 0, spawner_icon.w, spawner_icon.h, spawner_icon.w * 4, pixels, spawner_icon.x, spawner_icon.y,
        spawner_icon.w, spawner_icon.h, w * 4);
    free(pixels);

    for(j = 0; j < len; j++, asset++) {
        ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
        buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &l);
        if(buf) {
            if(!asset->name || asset->name >= TNG_SECTION_SIZE(&ctx->tbl[0]) || l < 6) { free(buf); continue; }
            end = buf + l;
            f = project_savefile(0, project_dirs[PROJDIRS_NPCS], (char*)ctx->sts + asset->name, "swr", PROJMAGIC_SPAWNER,
                "spawners_fromtng");
            if(f) {
                fprintf(f, "%u ", *ptr++);
                ptr = tng_wrnum(ctx, ptr, 3, f);
                fprintf(f, "\r\n");
                n = *((uint16_t*)ptr); ptr += 2;
                for(i = 0; i < project.spritenum[PROJ_NUMSPRCATS - 2]; i++)
                    project.sprites[PROJ_NUMSPRCATS - 2][i].idx = 0;
                for(i = 0; i < n && ptr < end; i++) {
                    r = 0; memcpy(&r, ptr, 3);
                    if(!r) {
                        fprintf(f, "- ");
                        ptr += 8;
                    } else {
                        ptr = tng_wridx(ctx, ptr, f);
                        r = *ptr++;
                        fprintf(f, "%s%d ", r != 0xff ? project_rels[r] : "=", *((int32_t*)ptr));
                        ptr += 4;
                    }
                    r = 0; memcpy(&r, ptr, 3);
                    if(!r) {
                        fprintf(f, "- ");
                        ptr += 7;
                    } else {
                        ptr = tng_wridx(ctx, ptr, f);
                        fprintf(f, "=%d ", *((int32_t*)ptr));
                        ptr += 4;
                    }
                    ptr = tng_wrsprite(ctx, ptr, f); fprintf(f, " ");
                    ptr++; r = *((uint16_t*)ptr); ptr += 2;
                    project_wrsprite2(f, r, spawners_npc.cat);
                    if(r < project.spritenum[PROJ_NUMSPRCATS - 2])
                        project.sprites[PROJ_NUMSPRCATS - 2][r].idx++;
                    fprintf(f, "\r\n");
                }
                fclose(f);
                for(r = i = k = 0; r < project.spritenum[PROJ_NUMSPRCATS - 2]; r++)
                    if(project.sprites[PROJ_NUMSPRCATS - 2][r].idx > k) { i = r; k = project.sprites[PROJ_NUMSPRCATS - 2][r].idx; }
                spr = &project.sprites[PROJ_NUMSPRCATS - 2][i].dir[0];
                /* rescale the "spawner egg" icon to the sprite's size */
                ui_fit(spr->w, spr->h, spawner_icon.w / 3, spawner_icon.h, &mw, &mh);
                pixels = (uint32_t*)main_alloc(mw * 3 * mh * 4);
                ui_resample(pixels, 0, 0, mw * 3, mh, mw * 3 * 4, 255, spawner, spawner_icon.w, spawner_icon.h, spawner_icon.w * 4);
                /* merge egg layers with the sprite */
                out = (uint32_t*)main_alloc(spr->w * spr->h * 4);
                ui_combine(out, (spr->w - mw) / 2, spr->h - mh, mw, mh, spr->w * 4, pixels, 0, 0, mw, mh, mw * 3 * 4);
                ui_combine(out, 0, 0, spr->w, spr->h, spr->w * 4, spr->data, 0, 0, spr->w, spr->h, spr->w * spr->nframe * 4);
                ui_combine(out, (spr->w - mw) / 2, spr->h - mh, mw, mh, spr->w * 4, pixels, mw, 0, mw, mh, mw * 3 * 4);
                ui_combine(out, (spr->w - mw) / 2, 0, mw, mh, spr->w * 4, pixels, 2 * mw, 0, mw, mh, mw * 3 * 4);
                free(pixels);
                sprintf(projfn, "%s" SEP "%s" SEP "%s_swr.png", project.id, project_dirs[PROJDIRS_NPCS],
                    (char*)ctx->sts + asset->name);
                f = project_fopen(projdir, "wb");
                if(f) {
                    if(verbose) printf("spawners_fromtng: saving %s\n", projdir);
                    if(image_save(f, spr->w, spr->h, spr->w * 4, (uint8_t*)out))
                        spr_add(PROJ_NUMSPRCATS - 1, (char*)ctx->sts + asset->name, 0, 0, 1, spr->w, spr->h, spr->w * 4, out);
                    fclose(f);
                }
                free(out);
            }
            free(buf);
        }
    }
    free(spawner);
    return 1;
}
