/*
 * tnge/tiles.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Tiles window
 *
 */

#include "main.h"

void tiles_select(void *data);
void tiles_combine(void *data);
void tiles_load(void *data);
void tiles_new(void *data);
void tiles_save(void *data);
void tiles_delete(void *data);
void tiles_autodel(void *data);
void tiles_autoadd(void *data);
void tiles_drawauto(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);
void tiles_renderauto(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

SDL_Texture *tiles_anims[9] = { 0 };
ui_sprsel_t tiles_bypic = { -1, 1, 157, 1 };
ui_sprsel_t tiles_combspr = { -1, 1, 157, 1 };
ui_num_t tiles_speeds[5] = {
    { 100, 1, 1000, 1 },
    { 100, 1, 1000, 1 },
    { 100, 1, 1000, 1 },
    { 100, 1, 1000, 1 },
    { 100, 1, 1000, 1 }
};
ui_select_t tiles_snds[5] = {
    { -1, LANG_NONE, NULL },
    { -1, LANG_NONE, NULL },
    { -1, LANG_NONE, NULL },
    { -1, LANG_NONE, NULL },
    { -1, LANG_NONE, NULL }
};
ui_sprsel_t tiles_autosprs[9] = {
    { -1, 1, 157, 1 },
    { -1, 1, 157, 1 },
    { -1, 1, 157, 1 },
    { -1, 1, 157, 1 },
    { -1, 1, 157, 1 },
    { -1, 1, 157, 1 },
    { -1, 1, 157, 1 },
    { -1, 1, 157, 1 },
    { -1, 1, 157, 1 }
};

ui_tablehdr_t tiles_hdr[] = {
    { TILES_META, 0, 0, 0 },
    { 0 }
};
ui_table_t tiles_tbl = { tiles_hdr, TILES_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), NULL, NULL,
    NULL, NULL };

ui_tablehdr_t tiles_movhdr[] = {
    { -1, 32, 0, 0 },
    { TILES_SPEED, 80, 0, 0 },
    { TILES_SOUND, 0, 0, 0 },
    { 0 }
};
ui_table_t tiles_moves = { tiles_movhdr, 0, 24, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), NULL, NULL,
    NULL, NULL };

ui_tablehdr_t tiles_amhdr[] = {
    { TILES_AUTOMAP, 0, 0, 0 },
    { 0 }
};
ui_table_t tiles_automap = { tiles_amhdr, TILES_NOAUTO, 100, 100, 0, 0, 0, 0, 0, 0, 0, -1, 0, 9 * sizeof(int), tiles_drawauto,
    tiles_renderauto, NULL, NULL };

/**
 * The form
 */
ui_form_t tiles_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, TILES_DELETE, (void*)ICON_REMOVE, tiles_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, tiles_save },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, tiles_new },
    { FORM_SPRITE, 0, 30, 40, 18, 0, TILES_BYPIC, &tiles_bypic, tiles_select },
    { FORM_TABLE, 10, 30, 200, 0, 0, 0, &tiles_tbl, tiles_load },
    { FORM_SPRITE, 0, 30, 20, 20, 0, TILES_COMBINE, &tiles_combspr, NULL },
    { FORM_ICONBTN, 0, 30, 20, 20, 0, TILES_COMBINE, (void*)ICON_MERGE, tiles_combine },
    /* 7 */
    { FORM_DRAWTBL, 0, 54, 0, 23+3*24, 0, 0, &tiles_moves, NULL },
    /* 8 */
    { FORM_ICON, 0, 78, 20, 20, 0, TILES_ONWALK, (void*)ICON_WALK, NULL },
    { FORM_NUM, 0, 77, 52, 20, 0, 0, &tiles_speeds[0], NULL },
    { FORM_TEXT, 0, 78, 8, 18, 0, 0, "%", NULL },
    { FORM_SNDSEL, 0, 77, 0, 20, 0, 0, &tiles_snds[0], NULL },
    /* 12 */
    { FORM_ICON, 0, 100, 20, 20, 0, TILES_ONSWIM, (void*)ICON_SWIM, NULL },
    { FORM_NUM, 0, 99, 52, 20, 0, 0, &tiles_speeds[1], NULL },
    { FORM_TEXT, 0, 100, 8, 18, 0, 0, "%", NULL },
    { FORM_SNDSEL, 0, 99, 0, 20, 0, 0, &tiles_snds[1], NULL },
    /* 16 */
    { FORM_ICON, 0, 124, 20, 20, 0, TILES_ONFLY, (void*)ICON_FLY, NULL },
    { FORM_NUM, 0, 123, 52, 20, 0, 0, &tiles_speeds[2], NULL },
    { FORM_TEXT, 0, 124, 8, 18, 0, 0, "%", NULL },
    { FORM_SNDSEL, 0, 123, 0, 20, 0, 0, &tiles_snds[2], NULL },
    /* 20 */
    { FORM_TABLE, 0, 160, 0, 0, 0, 0, &tiles_automap, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, TILES_DELAUTO, (void*)ICON_REMOVE, tiles_autodel },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, TILES_ADDAUTO, (void*)ICON_ADD, tiles_autoadd },
    /* 23 */
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &tiles_autosprs[0], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &tiles_autosprs[1], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &tiles_autosprs[2], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &tiles_autosprs[3], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &tiles_autosprs[4], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &tiles_autosprs[5], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &tiles_autosprs[6], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &tiles_autosprs[7], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, 0, 0, &tiles_autosprs[8], NULL },
    /*32 */
    { FORM_COMMENT, 0, 0, 16, 20, 0, 0, "\xe2\x9e\xa1", NULL },
    /* 33 */
    { FORM_SPRITE, 0, 0, 20, 20, -1, 0, &tiles_autosprs[0], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, -1, 0, &tiles_autosprs[1], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, -1, 0, &tiles_autosprs[2], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, -1, 0, &tiles_autosprs[3], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, -1, 0, &tiles_bypic, NULL },
    { FORM_SPRITE, 0, 0, 20, 20, -1, 0, &tiles_autosprs[5], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, -1, 0, &tiles_autosprs[6], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, -1, 0, &tiles_autosprs[7], NULL },
    { FORM_SPRITE, 0, 0, 20, 20, -1, 0, &tiles_autosprs[8], NULL },

    { FORM_LAST }
};

/**
 * Get automap coordinates
 */
void tiles_getcoords(int x, int y, int w, int h, SDL_Rect *rect)
{
    int dh = 16/*project.tileh * 16 / project.tilew*/;

    switch(project.type) {
        case TNG_TYPE_ORTHO:
            rect[0].x = x + w / 2 - 32;     rect[0].y = y + h / 2 - dh;
            rect[1].x = x + w / 2;          rect[1].y = y + h / 2 - dh;
            rect[2].x = x + w / 2 + 32;     rect[2].y = y + h / 2 - dh;
            rect[3].x = x + w / 2 - 32;     rect[3].y = y + h / 2 + dh;
            rect[4].x = x + w / 2;          rect[4].y = y + h / 2 + dh;
            rect[5].x = x + w / 2 + 32;     rect[5].y = y + h / 2 + dh;
            rect[6].x = x + w / 2 - 32;     rect[6].y = y + h / 2 + 3*dh;
            rect[7].x = x + w / 2;          rect[7].y = y + h / 2 + 3*dh;
            rect[8].x = x + w / 2 + 32;     rect[8].y = y + h / 2 + 3*dh;
        break;
        case TNG_TYPE_ISO:
            rect[0].x = x + w / 2;          rect[0].y = y + h / 2 - dh;
            rect[1].x = x + w / 2 + 16;     rect[1].y = y + h / 2;
            rect[2].x = x + w / 2 + 32;     rect[2].y = y + h / 2 + dh;
            rect[3].x = x + w / 2 - 16;     rect[3].y = y + h / 2;
            rect[4].x = x + w / 2;          rect[4].y = y + h / 2 + dh;
            rect[5].x = x + w / 2 + 16;     rect[5].y = y + h / 2 + 2*dh;
            rect[6].x = x + w / 2 - 32;     rect[6].y = y + h / 2 + dh;
            rect[7].x = x + w / 2 - 16;     rect[7].y = y + h / 2 + 2*dh;
            rect[8].x = x + w / 2;          rect[8].y = y + h / 2 + 3*dh;
        break;
        case TNG_TYPE_HEXV:
            rect[1].x = x + w / 2;          rect[1].y = y + h / 2 - dh;
            rect[2].x = x + w / 2 + 32;     rect[2].y = y + h / 2;
            rect[3].x = x + w / 2 - 32;     rect[3].y = y + h / 2;
            rect[4].x = x + w / 2;          rect[4].y = y + h / 2 + dh;
            rect[5].x = x + w / 2 + 32;     rect[5].y = y + h / 2 + 2*dh;
            rect[6].x = x + w / 2 - 32;     rect[6].y = y + h / 2 + 2*dh;
            rect[7].x = x + w / 2;          rect[7].y = y + h / 2 + 3*dh;
        break;
        case TNG_TYPE_HEXH:
            rect[1].x = x + w / 2 - 16;     rect[1].y = y + h / 2 - dh;
            rect[2].x = x + w / 2 + 16;     rect[2].y = y + h / 2 - dh;
            rect[3].x = x + w / 2 - 32;     rect[3].y = y + h / 2 + dh;
            rect[4].x = x + w / 2;          rect[4].y = y + h / 2 + dh;
            rect[5].x = x + w / 2 + 32;     rect[5].y = y + h / 2 + dh;
            rect[6].x = x + w / 2 - 16;     rect[6].y = y + h / 2 + 3*dh;
            rect[7].x = x + w / 2 + 16;     rect[7].y = y + h / 2 + 3*dh;
        break;
    }
}

/**
 * Draw table cell
 */
void tiles_drawauto(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    ui_sprite_t *s;
    SDL_Rect rect[9] = { 0 };
    int i, *spr = (int*)data;

    (void)hdr;
    if(!data) return;
    tiles_getcoords(x, y, w, h, rect);
    for(i = 0; i < 9; i++)
        if((s = spr_getidx(tiles_autosprs[0].cat, spr[i], 0, 0))) {
            if(idx != sel) {
                ui_fit(32, 512, s->w, s->h, &rect[i].w, &rect[i].h);
                rect[i].x -= rect[i].w / 2; rect[i].y -= rect[i].h;
                spr_blit(s, 160, dst, &rect[i]);
            } else
                spr_texture(s, &tiles_anims[i]);
        }
}

/**
 * Draw table cell
 */
void tiles_renderauto(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    ui_sprite_t *s;
    SDL_Rect rect[9] = { 0 };
    int i, *spr = (int*)data;

    (void)dst; (void)hdr;
    if(!data || idx != sel) return;
    tiles_getcoords(x, y, w, h, rect);
    for(i = 0; i < 9; i++)
        if(tiles_anims[i] && (s = spr_getidx(tiles_autosprs[0].cat, spr[i], 0, 0))) {
            ui_fit(32, 512, s->w, s->h, &rect[i].w, &rect[i].h);
            rect[i].x -= rect[i].w / 2; rect[i].y -= rect[i].h;
            spr_render(s, -1, tiles_anims[i], &rect[i]);
        }
}

/**
 * Exit tiles window
 */
void tiles_exit(int tab)
{
    int i;

    (void)tab;
    project_freedir((char***)&tiles_tbl.data, &tiles_tbl.num);
    tiles_tbl.val = tiles_tbl.clk = -1;
    tiles_tbl.scr = 0;
    for(i = 0; i < 9; i++)
        if(tiles_anims[i]) {
            SDL_DestroyTexture(tiles_anims[i]);
            tiles_anims[i] = NULL;
        }
}

/**
 * Enter tiles window
 */
void tiles_init(int tab)
{
    tiles_exit(tab);
    tiles_form[1].param = lang[LANG_SAVE];
    tiles_form[1].w = ui_textwidth(tiles_form[1].param) + 40;
    if(tiles_form[1].w < 200) tiles_form[1].w = 200;
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_TILES]);
    tiles_tbl.data = project_getdir(projdir, ".til", &tiles_tbl.num);
    tiles_snds[0].opts = tiles_snds[1].opts = tiles_snds[2].opts = project.sounds;
    tiles_new(NULL);
}

/**
 * Resize the view
 */
void tiles_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    tiles_form[0].y = tiles_form[1].y = tiles_form[2].y = h - 48;
    tiles_form[1].x = w - 20 - tiles_form[1].w;
    ui_table_resize(&tiles_form[4], tiles_form[4].w, h - 89);
    tiles_form[2].x = tiles_form[4].x + tiles_form[4].w + 20;
    tiles_form[3].x = tiles_form[7].x = tiles_form[20].x = tiles_form[21].x = tiles_form[4].x + tiles_form[4].w + 10;
    tiles_form[5].x = tiles_form[3].x + tiles_form[3].w + 20;
    tiles_form[6].x = tiles_form[5].x + tiles_form[5].w + 4;

    ui_table_resize(&tiles_form[7], w - 10 - tiles_form[7].x, tiles_form[7].h);
    tiles_form[8].x = tiles_form[12].x = tiles_form[16].x = tiles_form[7].x + (tiles_movhdr[0].w - 16) / 2;
    tiles_form[9].x = tiles_form[7].x + tiles_movhdr[0].w + 10;
    tiles_form[10].x = tiles_form[9].x + tiles_form[9].w + 2;
    tiles_form[11].x = tiles_form[7].x + tiles_movhdr[0].w + tiles_movhdr[1].w + 24;
    tiles_form[11].w = tiles_movhdr[2].w - 28;
    tiles_form[13].x = tiles_form[7].x + tiles_movhdr[0].w + 10;
    tiles_form[14].x = tiles_form[13].x + tiles_form[13].w + 2;
    tiles_form[15].x = tiles_form[7].x + tiles_movhdr[0].w + tiles_movhdr[1].w + 24;
    tiles_form[15].w = tiles_movhdr[2].w - 28;
    tiles_form[17].x = tiles_form[7].x + tiles_movhdr[0].w + 10;
    tiles_form[18].x = tiles_form[17].x + tiles_form[17].w + 2;
    tiles_form[19].x = tiles_form[7].x + tiles_movhdr[0].w + tiles_movhdr[1].w + 24;
    tiles_form[19].w = tiles_movhdr[2].w - 28;

    tiles_form[22].x = tiles_form[21].x + 160 + 160 + 10;
    tiles_form[21].y = tiles_form[22].y = h - 55 - 3 * 24;
    ui_table_resize(&tiles_form[20], w - 10 - tiles_form[20].x, tiles_form[21].y - tiles_form[20].y - 4);

    switch(project.type) {
        case TNG_TYPE_ORTHO:
            tiles_form[23].inactive = tiles_form[31].inactive = 0;
            tiles_form[33].inactive = tiles_form[41].inactive = -1;
            tiles_form[23].x = tiles_form[26].x = tiles_form[29].x = tiles_form[21].x + 64;
            tiles_form[24].x = tiles_form[27].x = tiles_form[30].x = tiles_form[23].x + 24;
            tiles_form[25].x = tiles_form[28].x = tiles_form[31].x = tiles_form[24].x + 24;
            tiles_form[23].y = tiles_form[24].y = tiles_form[25].y = tiles_form[21].y;
            tiles_form[26].y = tiles_form[27].y = tiles_form[28].y = tiles_form[21].y + 24;
            tiles_form[29].y = tiles_form[30].y = tiles_form[31].y = tiles_form[21].y + 48;
        break;
        case TNG_TYPE_ISO:
            tiles_form[23].inactive = tiles_form[31].inactive = 0;
            tiles_form[33].inactive = tiles_form[41].inactive = -1;
            tiles_form[23].x = tiles_form[27].x = tiles_form[31].x = tiles_form[21].x + 64 + 24;
            tiles_form[24].x = tiles_form[28].x = tiles_form[21].x + 64 + 24 + 24;
            tiles_form[25].x = tiles_form[21].x + 64 + 24 + 24 + 24;
            tiles_form[26].x = tiles_form[30].x = tiles_form[21].x + 64;
            tiles_form[23].y = tiles_form[21].y;
            tiles_form[25].y = tiles_form[27].y = tiles_form[29].y = tiles_form[21].y + 24;
            tiles_form[29].x = tiles_form[21].x + 40;
            tiles_form[31].y = tiles_form[21].y + 48;
            tiles_form[24].y = tiles_form[26].y = tiles_form[21].y + 12;
            tiles_form[28].y = tiles_form[30].y = tiles_form[21].y + 24 + 12;
        break;
        case TNG_TYPE_HEXV:
            tiles_form[23].inactive = tiles_form[31].inactive = tiles_form[33].inactive = tiles_form[41].inactive = 2;
            tiles_autosprs[0].val = tiles_autosprs[8].val = -1;
            tiles_form[24].x = tiles_form[27].x = tiles_form[30].x = tiles_form[21].x + 64 + 24;
            tiles_form[25].x = tiles_form[28].x = tiles_form[21].x + 64 + 24 + 24;
            tiles_form[26].x = tiles_form[29].x = tiles_form[21].x + 64;
            tiles_form[24].y = tiles_form[21].y;
            tiles_form[27].y = tiles_form[21].y + 24;
            tiles_form[30].y = tiles_form[21].y + 48;
            tiles_form[25].y = tiles_form[26].y = tiles_form[21].y + 12;
            tiles_form[28].y = tiles_form[29].y = tiles_form[21].y + 24 + 12;
        break;
        case TNG_TYPE_HEXH:
            tiles_form[23].inactive = tiles_form[31].inactive = tiles_form[33].inactive = tiles_form[41].inactive = 2;
            tiles_autosprs[0].val = tiles_autosprs[8].val = -1;
            tiles_form[26].x = tiles_form[21].x + 64;
            tiles_form[27].x = tiles_form[26].x + 24;
            tiles_form[28].x = tiles_form[27].x + 24;
            tiles_form[24].x = tiles_form[29].x = tiles_form[21].x + 64 + 12;
            tiles_form[25].x = tiles_form[30].x = tiles_form[31].x = tiles_form[24].x + 24;
            tiles_form[24].y = tiles_form[25].y = tiles_form[21].y;
            tiles_form[26].y = tiles_form[27].y = tiles_form[28].y = tiles_form[21].y + 24;
            tiles_form[29].y = tiles_form[30].y = tiles_form[21].y + 48;
        break;
    }
    tiles_form[32].x = tiles_form[27].x + 82;
    tiles_form[32].y = tiles_form[27].y + 1;
    for(i = 0; i < 9; i++) {
        tiles_form[33 + i].x = tiles_form[23 + i].x + 160;
        tiles_form[33 + i].y = tiles_form[23 + i].y;
    }
}

/**
 * View layer
 */
void tiles_redraw(int tab)
{
    (void)tab;
    ui_form = tiles_form;
    ui_form[0].inactive = (tiles_tbl.val < 0);
    ui_form[1].inactive = ui_form[22].inactive = (tiles_bypic.val < 0);
    ui_form[6].inactive = (tiles_bypic.val < 0 || tiles_combspr.val < 0);
    ui_form[21].inactive = (tiles_automap.val < 0);
}

/**
 * Combine tile with another
 */
void tiles_combine(void *data)
{
    char *s, newname[FILENAME_MAX];
    int i, n = 0, l, mw, mh, mf, sprs[2];
    ui_sprite_t out = { 0 };
    FILE *f;

    (void)data;
    if(tiles_bypic.val < 0 || tiles_combspr.val < 0) return;
    /* construct the new sprite's name */
    s = project.sprites[tiles_bypic.cat][tiles_bypic.val].name;
    l = strlen(s);
    if(l > 5 && s[l - 4] == '_' && s[l - 1] >= '0' && s[l - 1] <= '9' && s[l - 2] >= '0' && s[l - 2] <= '9' &&
      s[l - 3] >= '0' && s[l - 3] <= '9') { n = atoi(s + l - 3); l -= 4; }
    if(l < 1) return;
    memcpy(newname, s, l);
    do {
        n++;
        sprintf(newname + l, "_%03u", n);
        for(i = 0; i < project.spritenum[tiles_bypic.cat] && strcmp(project.sprites[tiles_bypic.cat][i].name, newname); i++);
    } while(i < project.spritenum[tiles_bypic.cat]);
    if(verbose) printf("tiles_combine: combining %s and %s to %s\n", s,
        project.sprites[tiles_bypic.cat][tiles_combspr.val].name, newname);
    s = newname + l + 4;
    *s++ = '_';
    spr_wrname(tiles_bypic.cat, tiles_bypic.val, 0, s);
    s[4] = 0;
    /* combine the sprites */
    sprs[0] = tiles_bypic.val;
    sprs[1] = tiles_combspr.val;
    spr_merge(&out, &mw, &mh, &mf, sprs, 1, 0, 2, tiles_bypic.cat, 0, 1, -1);
    if(out.data) {
        f = project_savefile(0, project_dirs[PROJDIRS_SPRITES + tiles_bypic.cat], newname, "png", NULL, "tiles_coombine");
        if(f) {
            if(image_save(f, mw * mf, mh, mw * mf * 4, (uint8_t*)out.data)) {
                newname[l + 4] = 0;
                spr_add(tiles_bypic.cat, newname, 0, out.type, mf, mw, mh, mw * mf * 4, out.data);
                newname[l + 4] = '_';
            }
            fclose(f);
        }
    }
    tiles_combspr.val = -1;
    newname[l + 4] = 0;
    for(tiles_bypic.val = 0; tiles_bypic.val < project.spritenum[tiles_bypic.cat] &&
        strcmp(project.sprites[tiles_bypic.cat][tiles_bypic.val].name, newname); tiles_bypic.val++);
    if(tiles_bypic.val >= project.spritenum[tiles_bypic.cat]) {
        tiles_bypic.val = -1;
        ui_status(1, lang[ERR_SAVESPR]);
    } else {
        newname[l + 4] = '_';
        sprintf(projfn, "%s" SEP "%s" SEP "%s.png", project.id, project_dirs[PROJDIRS_SPRITES + tiles_bypic.cat], newname);
        ui_status(0, lang[LANG_SAVED]);
    }
}

/**
 * Erase an automap rule from tile
 */
void tiles_autodel(void *data)
{
    int *sprs = (int*)tiles_automap.data;

    (void)data;
    if(!sprs || tiles_automap.val < 0 || tiles_automap.val >= tiles_automap.num) return;
    memcpy(&sprs[tiles_automap.val * 9], &sprs[tiles_automap.val * 9 + 9],
        (tiles_automap.num - tiles_automap.val) * 9 * sizeof(int));
    tiles_automap.num--;
    if(tiles_automap.val >= tiles_automap.num)
        tiles_automap.val = tiles_automap.num - 1;
    if(!tiles_automap.num) { free(sprs); tiles_automap.data = NULL; }
}

/**
 * Add an automap rule to tile
 */
void tiles_autoadd(void *data)
{
    int *sprs = (int*)tiles_automap.data;
    int i, j;

    (void)data;
    for(i = 0; i < 9 && tiles_autosprs[i].val == -1; i++);
    if(i >= 9) return;
    sprs = (int*)realloc(sprs, (tiles_automap.num + 1) * 9 * sizeof(int));
    tiles_automap.data = sprs;
    if(!sprs) main_error(ERR_MEM);
    i = tiles_automap.val = tiles_automap.num++;
    i *= 9;
    for(j = 0; j < 9; j++)
        sprs[i + j] = tiles_autosprs[j].val;
}

/**
 * Clear form, new tile
 */
void tiles_new(void *data)
{
    (void)data;
    tiles_bypic.val = tiles_combspr.val = -1;
    tiles_speeds[0].val = tiles_speeds[1].val = tiles_speeds[2].val = tiles_speeds[3].val = tiles_speeds[4].val = 100;
    tiles_snds[0].val = tiles_snds[1].val = tiles_snds[2].val = tiles_snds[3].val = tiles_snds[4].val = tiles_autosprs[0].val =
        tiles_autosprs[1].val = tiles_autosprs[2].val = tiles_autosprs[3].val = tiles_autosprs[4].val = tiles_autosprs[5].val =
        tiles_autosprs[6].val = tiles_autosprs[7].val = tiles_autosprs[8].val = tiles_automap.clk = tiles_automap.val = -1;
    if(tiles_automap.data) { free(tiles_automap.data); tiles_automap.data = NULL; }
    tiles_automap.num = 0;
}

/**
 * Delete tile
 */
void tiles_delete(void *data)
{
    char **list = (char**)tiles_tbl.data;

    (void)data;
    if(tiles_tbl.val < 0 || !list) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.til", project.id, project_dirs[PROJDIRS_TILES], list[tiles_tbl.val]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("tiles_delete: removing %s\n", projdir);
        remove(projdir);
        tiles_new(NULL);
        tiles_init(SUBMENU_TILES);
    }
}

/**
 * Select tile by picture
 */
void tiles_select(void *data)
{
    char **list = (char**)tiles_tbl.data;

    (void)data;
    if(tiles_bypic.val < 0) return;
    for(tiles_tbl.val = 0; tiles_tbl.val < tiles_tbl.num && list &&
        strcmp(project.sprites[tiles_bypic.cat][tiles_bypic.val].name, list[tiles_tbl.val]); tiles_tbl.val++);
    if(tiles_tbl.val >= tiles_tbl.num) tiles_tbl.val = -1;
    tiles_load(NULL);
}

/**
 * Save tile
 */
void tiles_save(void *data)
{
    int *sprs = (int*)tiles_automap.data;
    char **list;
    int i, j;
    FILE *f;

    (void)data;
    if(tiles_bypic.val < 0) return;
    f = project_savefile(0, project_dirs[PROJDIRS_TILES], project.sprites[tiles_bypic.cat][tiles_bypic.val].name, "til",
        PROJMAGIC_TILE, "tiles_save");
    if(f) {
        for(i = 0; i < 5; i++) {
            fprintf(f, "%u ", tiles_speeds[i].val);
            project_wridx(f, tiles_snds[i].val, project.sounds);
            fprintf(f, "\r\n");
        }
        for(i = 0; sprs && i < tiles_automap.num; i++) {
            for(j = 0; j < 9; j++) {
                if(j) fprintf(f, " ");
                project_wrsprite2(f, sprs[i * 9 + j], tiles_autosprs[0].cat);
            }
            fprintf(f, "\r\n");
        }
        fclose(f);
        sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_TILES]);
        tiles_tbl.data = project_getdir(projdir, ".til", &tiles_tbl.num);
        list = (char**)tiles_tbl.data;
        for(tiles_tbl.val = 0; tiles_tbl.val < tiles_tbl.num && list &&
            strcmp(project.sprites[tiles_bypic.cat][tiles_bypic.val].name, list[tiles_tbl.val]); tiles_tbl.val++);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.til", project.id, project_dirs[PROJDIRS_TILES],
            project.sprites[tiles_bypic.cat][tiles_bypic.val].name);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVETILE]);
}

/**
 * Load tile
 */
int tiles_loadtil(char *fn, int rules)
{
    char *str, *s;
    int i;

    if(!fn || !*fn) return 0;
    str = s = project_loadfile(project_dirs[PROJDIRS_TILES], fn, "til", PROJMAGIC_TILE, "tiles_loadtil");
    if(str) {
        tiles_new(NULL);
        for(i = 0; *s && i < 3; i++) {
            s = project_skipnl(s);
            if(!*s) break;
            s = project_getint(s, &tiles_speeds[i].val, tiles_speeds[i].min, tiles_speeds[i].max);
            s = project_getidx(s, &tiles_snds[i].val, project.sounds, -1);
        }
        if(rules) {
            while(*s) {
                s = project_skipnl(s);
                if(!*s) break;
                for(i = 0; *s && i < 9; i++)
                    s = project_getsprite(s, &tiles_autosprs[i]);
                tiles_autoadd(NULL);
            }
            tiles_autosprs[0].val = tiles_autosprs[1].val = tiles_autosprs[2].val = tiles_autosprs[3].val = tiles_autosprs[4].val =
                tiles_autosprs[5].val = tiles_autosprs[6].val = tiles_autosprs[7].val = tiles_autosprs[8].val = -1;
            tiles_automap.val = 0;
            if(tiles_automap.val >= tiles_automap.num)
                tiles_automap.val = tiles_automap.num - 1;
        }
        free(str);
        return 1;
    }
    return 0;
}

/**
 * Load tile
 */
void tiles_load(void *data)
{
    char **list = (char**)tiles_tbl.data;

    (void)data;
    if(tiles_tbl.val < 0 || !list) return;
    if(!tiles_loadtil(list[tiles_tbl.val], 1))
        ui_status(1, lang[ERR_LOADTILE]);
    else {
        for(tiles_bypic.val = 0; tiles_bypic.val < project.spritenum[tiles_bypic.cat] && list &&
            strcmp(project.sprites[tiles_bypic.cat][tiles_bypic.val].name, list[tiles_tbl.val]); tiles_bypic.val++);
        if(tiles_bypic.val >= project.spritenum[tiles_bypic.cat]) tiles_bypic.val = -1;
    }
}

/**
 * Save tng
 */
int tiles_totng(tngctx_t *ctx)
{
    char **list;
    int i, j, k, num;

    if(!ctx || !project.sprites[1] || !project.spritenum[1]) return 1;
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_TILES]);
    list = project_getdir(projdir, ".til", &num);
    /* check if there's at least one tile referenced */
    for(j = 0; j < num; j++) {
        for(k = 0; k < project.spritenum[1]; k++)
            if(!strcmp(list[j], project.sprites[1][k].name)) break;
        if(k < project.spritenum[1] && project.sprites[1][k].idx != -1) break;
    }
    if(j < num) {
        /* add tiles to output */
        tng_section(ctx, TNG_SECTION_TILES);
        for(j = 0; j < num; j++) {
            for(k = 0; k < project.spritenum[1]; k++)
                if(!strcmp(list[j], project.sprites[1][k].name)) break;
            if(k >= project.spritenum[1] || project.sprites[1][k].idx == -1) continue;
            if(!tiles_loadtil(list[j], 0)) {
                project_freedir((char***)&list, &num);
                ui_switchtab(SUBMENU_TILES);
                tiles_tbl.val = j;
                ui_status(1, lang[ERR_LOADTILE]);
                return 0;
            }
            tng_data(ctx, &project.sprites[1][k].idx, 2);
            for(i = 0; i < 3; i++) {
                tng_data(ctx, &tiles_speeds[i].val, 2);
                tng_ref(ctx, TNG_IDX_SND, tiles_snds[i].val);
                tng_idx(ctx, project.sounds, tiles_snds[i].val);
            }
            /* free resources */
            tiles_new(NULL);
        }
    }
    project_freedir((char***)&list, &num);
    return 1;
}

/**
 * Read from tng
 */
int tiles_fromtng(tngctx_t *ctx)
{
    FILE *f;
    uint8_t *ptr;
    int i, j, len, rl;
    char name[8];

    if(!ctx || !ctx->tbl || !ctx->numtbl) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_TILES; i++);
    if(i >= ctx->numtbl) return 1;
    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
    rl = 2 + ctx->hdr.numtrn * 5;
    ptr = (uint8_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / rl;
    if(len < 1) return 0;
    for(j = 0; j < len; j++) {
        memset(name, 0, sizeof(name));
        sprintf(name, "%04X", *((uint16_t*)ptr));
        f = project_savefile(0, project_dirs[PROJDIRS_TILES], name, "til", PROJMAGIC_TILE, "tiles_fromtng");
        if(f) {
            ptr += 2;
            for(i = 0; i < ctx->hdr.numtrn; i++)
                if(i < 3) {
                    ptr = tng_wrnum(ctx, ptr, 2, f); fprintf(f, " ");
                    ptr = tng_wridx(ctx, ptr, f); fprintf(f, "\r\n");
                } else
                    ptr += 5;
            fclose(f);
        } else
            ptr += rl;
    }
    return 1;
}
