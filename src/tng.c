/*
 * tnge/tng.c
 *
 * Copyright (C) 2022 bzt
 *
 * The TirNanoG File Format is dual licensed:
 *
 * - by default, game files are licensed under CC-by-nc-sa
 *   (attribution required, non-commercial, share-alike)
 *
 * - with the written permission of the TirNanoG Editor's author,
 *   you can use any license you want, including commercial ones
 *
 * @brief TirNanoG File Format routines
 *
 */

#include "main.h"

extern ui_table_t translate_tbl;

/**
 * Add an internal name string
 */
uint32_t tng_str(tngctx_t *ctx, char *name)
{
    int i, l;

    if(!ctx || !name || !*name) return 0;
    if(ctx->str)
        for(i = 0; i < ctx->numstr; i++)
            if(!strcmp(ctx->str[i].name, name)) return ctx->str[i].offs;
    ctx->str = (tng_str_t*)realloc(ctx->str, (ctx->numstr + 1) * sizeof(tng_str_t));
    memset(&ctx->str[ctx->numstr], 0, sizeof(tng_str_t));
    l = strlen(name); if(l > PROJ_NAMEMAX - 1) l = PROJ_NAMEMAX - 1;
    memcpy(ctx->str[ctx->numstr].name, name, l);
    ctx->str[ctx->numstr++].offs = ctx->offstr;
    ctx->offstr += l + 1;
    if(ctx->offstr > TNG_SECTION_MAX) { ctx->err = 1; }
    return ctx->str[ctx->numstr - 1].offs;
}

/**
 * Add a section to section table
 */
void tng_section(tngctx_t *ctx, int type)
{
    if(!ctx) return;
    ctx->tbl = (tng_section_t*)realloc(ctx->tbl, (ctx->numtbl + 1) * sizeof(tng_section_t));
    if(!ctx->tbl) { ctx->numtbl = 0; return; }
    ctx->tbl[ctx->numtbl].offs = ctx->lensec;
    ctx->tbl[ctx->numtbl].size = type << 24;
    ctx->numtbl++;
}

/**
 * Add an asset descriptor to current section
 */
void tng_desc(tngctx_t *ctx, char *name, uint32_t size)
{
    tng_asset_desc_t desc;

    if(!ctx || !ctx->tbl || !ctx->numtbl || ctx->err) return;
    if(ctx->lensec + sizeof(tng_asset_desc_t) > TNG_SECTION_MAX) { ctx->err = 1; return; }
    desc.offs = ctx->total;
    desc.size = size;
    desc.name = tng_str(ctx, name);
    ctx->sec = (uint8_t*)realloc(ctx->sec, ctx->lensec + sizeof(tng_asset_desc_t));
    if(!ctx->sec) { ctx->lensec = 0; return; }
    memcpy(ctx->sec + ctx->lensec, &desc, sizeof(tng_asset_desc_t));
    ctx->lensec += sizeof(tng_asset_desc_t);
    ctx->tbl[ctx->numtbl - 1].size += sizeof(tng_asset_desc_t);
}

/**
 * Add an asset descriptor with index to current section
 */
void tng_desc_idx(tngctx_t *ctx, uint32_t idx, uint32_t size)
{
    tng_asset_desc_t desc;

    if(!ctx || !ctx->tbl || !ctx->numtbl || ctx->err) return;
    if(ctx->lensec + sizeof(tng_asset_desc_t) > TNG_SECTION_MAX) { ctx->err = 1; return; }
    desc.offs = ctx->total;
    desc.size = size;
    desc.name = idx;
    ctx->sec = (uint8_t*)realloc(ctx->sec, ctx->lensec + sizeof(tng_asset_desc_t));
    if(!ctx->sec) { ctx->lensec = 0; return; }
    memcpy(ctx->sec + ctx->lensec, &desc, sizeof(tng_asset_desc_t));
    ctx->lensec += sizeof(tng_asset_desc_t);
    ctx->tbl[ctx->numtbl - 1].size += sizeof(tng_asset_desc_t);
}

/**
 * Add a font specification to current section
 */
void tng_font(tngctx_t *ctx, int style, int size, int fnt)
{
    if(!ctx || !ctx->tbl || !ctx->numtbl || ctx->err) return;
    tng_data(ctx, &style, 1);
    tng_data(ctx, &size, 1);
    tng_ref(ctx, TNG_IDX_FNT, fnt);
    tng_idx(ctx, project.fonts, fnt);
}

/**
 * Add a sprite specification to current section
 */
void tng_sprite(tngctx_t *ctx, int cat, int idx)
{
    if(!ctx || !ctx->tbl || !ctx->numtbl || ctx->err) return;
    tng_data(ctx, &cat, 1);
    if(idx < 0 || idx >= project.spritenum[cat]) {
        idx = -1;
        tng_data(ctx, &idx, 2);
    } else {
        tng_sprref(ctx, cat, idx);
        tng_data(ctx, &project.sprites[cat][idx].idx, 2);
    }
}

/**
 * Add a string index to current section
 */
void tng_idx(tngctx_t *ctx, char **names, int idx)
{
    uint32_t offs = 0;

    if(!ctx || !ctx->tbl || !ctx->numtbl || ctx->err) return;
    if(names && idx >= 0)
        offs = tng_str(ctx, names[idx]);
    tng_data(ctx, &offs, 3);
}

/**
 * Add index of a translatable text to current section
 */
void tng_text(tngctx_t *ctx, char *str)
{
    trn_t *list = (trn_t*)translate_tbl.data;
    int i = -1;

    if(!ctx || !ctx->tbl || !ctx->numtbl || ctx->err) return;

    if(list && str && *str) {
        for(i = 0; i < translate_tbl.num && strcmp(list[i].id, str); i++);
        if(i >= translate_tbl.num) {
            if(verbose) printf("tng_text: this should never happen, no id for '%s'\n", str);
            i = -1;
        }
    }
    tng_data(ctx, &i, 3);
}

/**
 * Add arbitrary data to current section
 */
void tng_data(tngctx_t *ctx, void *data, int size)
{
    if(!ctx || !data || !ctx->tbl || !ctx->numtbl || ctx->err || size < 0) return;
    if(ctx->lensec + size > TNG_SECTION_MAX) { ctx->err = 1; return; }
    ctx->sec = (uint8_t*)realloc(ctx->sec, ctx->lensec + size);
    if(!ctx->sec) { ctx->lensec = 0; return; }
    memcpy(ctx->sec + ctx->lensec, data, size);
    ctx->lensec += size;
    ctx->tbl[ctx->numtbl - 1].size += size;
}

/**
 * Add an internal asset
 */
void tng_asset_int(tngctx_t *ctx, uint8_t *data, uint32_t size)
{
    if(!ctx || ctx->err || !data || !size) return;
    ctx->assets = (tng_asset_t*)realloc(ctx->assets, (ctx->numassets + 1) * sizeof(tng_asset_t));
    if(!ctx->assets) { ctx->numassets = 0; return; }
    ctx->assets[ctx->numassets].external = 0;
    ctx->assets[ctx->numassets].data = data;
    ctx->assets[ctx->numassets].size = size;
    ctx->numassets++;
    ctx->total += size;
}

/**
 * Add an external asset
 */
void tng_asset_ext(tngctx_t *ctx, char *fn, uint32_t size)
{
    uint8_t *buf;
    int i;

    /* allow adding external assets with zero size */
    if(!ctx || ctx->err || !fn) return;
    i = strlen(fn) + 1;
    buf = (uint8_t*)malloc(i);
    if(!buf) return;
    memcpy(buf, fn, i);
    ctx->assets = (tng_asset_t*)realloc(ctx->assets, (ctx->numassets + 1) * sizeof(tng_asset_t));
    if(!ctx->assets) { ctx->numassets = 0; return; }
    ctx->assets[ctx->numassets].external = 1;
    ctx->assets[ctx->numassets].data = buf;
    ctx->assets[ctx->numassets].size = size;
    ctx->numassets++;
    ctx->total += size;
}

/**
 * Add a font specification to asset data
 */
uint8_t *tng_asset_font(uint8_t *ptr, tngctx_t *ctx, int style, int size, int fnt)
{
    if(!ptr || !ctx || ctx->err) return ptr;
    *ptr++ = style;
    *ptr++ = size;
    return tng_asset_idx(ptr, ctx, project.fonts, TNG_IDX_FNT, fnt);
}

/**
 * Add a sprite specification to asset data
 */
uint8_t *tng_asset_sprite(uint8_t *ptr, tngctx_t *ctx, int cat, int idx)
{
    if(!ptr || !ctx || ctx->err) return ptr;
    *ptr++ = cat;
    if(idx < 0 || idx >= project.spritenum[cat]) {
        idx = -1;
        memcpy(ptr, &idx, 2);
    } else {
        tng_sprref(ctx, cat, idx);
        memcpy(ptr, &project.sprites[cat][idx].idx, 2);
    }
    return ptr + 2;
}

/**
 * Add a string index to asset data
 */
uint8_t *tng_asset_idx(uint8_t *ptr, tngctx_t *ctx, char **names, int type, int idx)
{
    uint32_t offs = 0;

    if(!ptr || !ctx || ctx->err) return ptr;
    if(type >= 0) tng_ref(ctx, type, idx);
    if(names && idx >= 0)
        offs = tng_str(ctx, names[idx]);
    memcpy(ptr, &offs, 3);
    return ptr + 3;
}

/**
 * Add index of a translatable text to asset data
 */
uint8_t *tng_asset_text(uint8_t *ptr, tngctx_t *ctx, char *str)
{
    trn_t *list = (trn_t*)translate_tbl.data;
    int i = -1;

    if(!ptr || !ctx || ctx->err) return ptr;

    if(list && str && *str) {
        for(i = 0; i < translate_tbl.num && strcmp(list[i].id, str); i++);
        if(i >= translate_tbl.num) {
            if(verbose) printf("tng_text: this should never happen, no id for '%s'\n", str);
            i = -1;
        }
    }
    memcpy(ptr, &i, 3);
    return ptr + 3;
}

/**
 * Return an uncompressed asset
 */
uint8_t *tng_get_asset(tngctx_t *ctx, uint64_t offs, uint32_t size, int *len)
{
    uint8_t *comp, *ret;

    if(!ctx || !ctx->f || ctx->offs < 70 || size < 1 || !len) return NULL;
    *len = 0;
    comp = (uint8_t*)main_alloc(size);
    project_fseek(ctx->f, ctx->offs + offs);
    if(!fread(comp, 1, size, ctx->f)) { free(comp); return NULL; }
    ret = (uint8_t*)stbi_zlib_decode_malloc_guesssize_headerflag((const char*)comp, (int)size, 4096, len, 1);
    free(comp);
    if(!ret || *len < 1) {
        if(ret) free(ret);
        if(verbose) printf("tng_get_asset: uncompression error (offs %lx size %d)\n", (unsigned long int)offs, size);
        return NULL;
    }
    return ret;
}

/**
 * Save asset to file
 */
void tng_save_asset(tngctx_t *ctx, uint64_t offs, uint32_t size, char *fn)
{
    uint32_t siz;
    uint8_t *buf;
    FILE *f;

    if(!ctx || !ctx->f || ctx->offs < 70 || size < 1) return;
    f = project_fopen(fn, "wb");
    if(f) {
        project_fseek(ctx->f, ctx->offs + offs);
        buf = (uint8_t*)main_alloc(1024 * 1024);
        siz = size;
        do {
            size = fread(buf, 1, siz > 1024 * 1024 ? 1024 * 1024 : siz, ctx->f);
            fwrite(buf, 1, size, f);
            siz -= size;
        } while(siz > 0);
        fclose(f);
        free(buf);
    }
}

/**
 * Allocate local index table
 */
void tng_idx_alloc(tngctx_t *ctx, int type, int num)
{
    ctx->idx[type].ref = (char*)main_alloc(num);
    ctx->idx[type].num = num;
}

/**
 * Return true if local sprite index is a new reference
 */
int tng_sprref(tngctx_t *ctx, int cat, int idx)
{
    if(!ctx || idx < 0 || idx >= project.spritenum[cat] || !project.sprites[cat] || project.sprites[cat][idx].idx != -1) return 0;
    project.sprites[cat][idx].idx = ctx->sprid[cat];
    ctx->sprid[cat]++;
    return 1;
}

/**
 * Return true if index is a new reference
 */
int tng_ref(tngctx_t *ctx, int type, int idx)
{
    if(!ctx || idx < 0 || idx >= ctx->idx[type].num || ctx->idx[type].ref[idx]) return 0;
    ctx->idx[type].ref[idx] = 1;
    return 1;
}

/**
 * Write a string representation of a font
 */
uint8_t *tng_wrfont(tngctx_t *ctx, uint8_t *buf, FILE *f)
{
    int idx = 0;

    if(!ctx || !ctx->sts || !buf || !f) return NULL;
    memcpy(&idx, buf + 2, 3);
    fprintf(f, "%c%c %d %s", buf[0] & SSFN_STYLE_BOLD ? 'B' : '.', buf[0] & SSFN_STYLE_ITALIC ? 'I' : '.',
        buf[1], !idx || idx >= TNG_SECTION_SIZE(&ctx->tbl[0]) ? "-" : (char*)ctx->sts + idx);
    return buf + 5;
}

/**
 * Write a string representation of an index
 */
uint8_t *tng_wridx(tngctx_t *ctx, uint8_t *buf, FILE *f)
{
    int idx = 0;

    if(!ctx || !ctx->sts || !buf || !f) return NULL;
    memcpy(&idx, buf, 3);
    fprintf(f, "%s", !idx || idx >= TNG_SECTION_SIZE(&ctx->tbl[0]) ? "-" : (char*)ctx->sts + idx);
    return buf + 3;
}

/**
 * Write a string representation of a number
 */
uint8_t *tng_wrnum(tngctx_t *ctx, uint8_t *buf, int len, FILE *f)
{
    int idx = 0;

    if(!ctx || !buf || len < 1 || !f) return NULL;
    memcpy(&idx, buf, len);
    fprintf(f, "%d", idx);
    return buf + len;
}

/**
 * Write a string representation of a sprite
 */
uint8_t *tng_wrsprite(tngctx_t *ctx, uint8_t *buf, FILE *f)
{
    if(!ctx || !buf || !f) return NULL;
    if(buf[1] == 0xff && buf[2] == 0xff)
        fprintf(f, "-");
    else
        fprintf(f, "%02X%02X", buf[2], buf[1]);
    return buf + 3;
}

/**
 * Write a string representation of a sprite without category
 */
uint8_t *tng_wrsprite2(tngctx_t *ctx, uint8_t *buf, FILE *f)
{
    if(!ctx || !buf || !f) return NULL;
    if(buf[0] == 0xff && buf[1] == 0xff)
        fprintf(f, "-");
    else
        fprintf(f, "%02X%02X", buf[1], buf[0]);
    return buf + 2;
}

/**
 * Write a string representation of a text
 */
uint8_t *tng_wrtext(tngctx_t *ctx, uint8_t *buf, FILE *f)
{
    trn_t *list = (trn_t*)translate_tbl.data;
    int idx = 0;

    if(!ctx || !list || !buf || !f) return NULL;
    memcpy(&idx, buf, 3);
    if(idx == 0xffffff)
        fprintf(f, "-");
    else
        fprintf(f, "%s", !idx || idx >= translate_tbl.num ? "???" : list[idx].id);
    return buf + 3;
}

/**
 * Write a string representation of a bytecode
 */
uint8_t *tng_wrscript(tngctx_t *ctx, uint8_t *buf, int len, FILE *f)
{
    ui_bc_t bc;

    if(!ctx || !buf || len < 0 || !f) return NULL;
    fprintf(f, "{");
    if(len > 0) {
        fprintf(f, "\r\n");
        bc.len = len;
        bc.data = buf;
        ui_cmd_frombc(ctx, &bc, 0, f);
    }
    fprintf(f, "}\r\n");
    return buf + len;
}

/**
 * Collect project data and construct sections
 */
int tng_sections(tngctx_t *ctx)
{
#define CHECKED(x) (!ctx->opts || ctx->opts[(x)-SUBMENU_ELEMENTS])
    int i, j, l;

    if(!ctx) return 0;
    /* generate header */
    memcpy(ctx->hdr.magic, TNG_MAGIC, 16);
    memcpy(ctx->hdr.id, project.id, 16);
    ctx->hdr.type = (project.type & 0x0F) | (((project.tilew >> 8) & 3) << 4) | (((project.tileh >> 8) & 3) << 6);
    ctx->hdr.tilew = project.tilew & 0xFF;
    ctx->hdr.tileh = project.tileh & 0xFF;
    for(ctx->hdr.mapsize = 0; (1 << ctx->hdr.mapsize) < project.mapsize; ctx->hdr.mapsize++);
    ctx->hdr.fps = FPS;
    ctx->hdr.numact = 0;
    ctx->hdr.numlyr = MAPS_LYR_COLL - MAPS_LYR_GRD1;
    ctx->hdr.numtrn = CMD_TRN_FLY - CMD_TRN_WALK + 1;
    ctx->hdr.sprperlyr = SPRPERLYR;
    chars_loadcfg(&ctx->hdr.deltay);
    ctx->offstr = 1;
    tng_section(ctx, TNG_SECTION_STRINGS);
    /* reset local indeces */
    if(verbose) printf("tng_sections: get local indeces and translation keys\n");
    tng_idx_alloc(ctx, TNG_IDX_FNT, project.numfont);
    tng_idx_alloc(ctx, TNG_IDX_MUS, project.nummusic);
    tng_idx_alloc(ctx, TNG_IDX_SND, project.numsounds);
    tng_idx_alloc(ctx, TNG_IDX_SPC, project.numspeech);
    tng_idx_alloc(ctx, TNG_IDX_MOV, project.nummovies);
    tng_idx_alloc(ctx, TNG_IDX_CHR, project.numchoosers);
    tng_idx_alloc(ctx, TNG_IDX_CUT, project.numcuts);
    tng_idx_alloc(ctx, TNG_IDX_ATR, project.numattrs);
    tng_idx_alloc(ctx, TNG_IDX_DLG, project.numdialogs);
    tng_idx_alloc(ctx, TNG_IDX_CFT, project.numcrafts);
    tng_idx_alloc(ctx, TNG_IDX_QST, project.numquests);
    spr_reset(-1);
    translate_getstrings();
    /* get references to build local indeces by doing a recursive walk */
    if(verbose) printf("tng_sections: get references\n");
    /* it is important to count apriori all which might have further references but not necessarily referenced themselves */
    if(CHECKED(SUBMENU_HUD)) hud_ref(ctx);
    if(CHECKED(SUBMENU_START)) startup_ref(ctx);
    if(CHECKED(SUBMENU_ACTIONS)) actions_ref(ctx);
    if(CHECKED(SUBMENU_CHAR)) chars_ref(ctx);
    if(CHECKED(SUBMENU_MAPS)) maps_ref(ctx);
    /* add sections */
    if(verbose) printf("tng_sections: construct sections\n");
    if(CHECKED(SUBMENU_ELEMENTS)) { if(!elements_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_MAINMENU)) { if(!mainmenu_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_HUD))      { if(!hud_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_ALERTS))   { if(!alerts_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_CREDITS))  { if(!credits_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_CHOOSERS)) { if(!choosers_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_CUTSCENES)){ if(!cutscn_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_START))    { if(!startup_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_ATTRS))    { if(!attrs_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_ACTIONS))  { if(!actions_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_CHAR))     { if(!chars_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_NPCS))     { if(!npcs_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_SPAWNERS)) { if(!spawners_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_DIALOGS))  { if(!dialogs_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_CRAFTS))   { if(!crafts_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_OBJECTS))  { if(!objects_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_TILES))    { if(!tiles_totng(ctx) || ctx->err) return 0; }
    if(CHECKED(SUBMENU_QUESTS))   { if(!quests_totng(ctx) || ctx->err) return 0; }
    /* get total number of assets for the progressbar */
    project_curr = 0;
    project_total = project.numlang;
    for(j = 0; j < PROJ_NUMSPRCATS - 2; j++)
        for(i = 0; project.sprites[j] && i < project.spritenum[j]; i++)
            if(project.sprites[j][i].idx != -1)
                for(l = 0; l < 8; l++)
                    if(project.sprites[j][i].dir[l].data) project_total += 2 * project.sprites[j][i].dir[l].nframe;
    /* maps */
    if(CHECKED(SUBMENU_MAPS)) { project_total += project.nummaps; if(!maps_totng(ctx) || ctx->err) return 0; }
    /* create translatable text section(s) */
    translate_totng(ctx); if(ctx->err) return 0;
    /* add all referenced assets. These just fill in an array with filenames, no need for progressbar */
    fonts_totng(ctx); if(ctx->err) return 0;
    media_totng(ctx); if(ctx->err) return 0;
    spr_totng(ctx); if(ctx->err) return 0;
    /* map assets MUST be the last */
    maps_toassets(ctx); if(ctx->err) return 0;
    /* create strings section and insert as the first section */
    if(verbose) printf("tng_sections: create internal names section\n");
    ctx->sts = (uint8_t*)malloc(ctx->offstr);
    if(!ctx->sts) { ctx->offstr = 0; return 0; }
    ctx->sts[0] = 0;
    ctx->tbl[0].offs = ctx->numtbl * sizeof(tng_section_t);
    ctx->tbl[0].size = ctx->offstr;
    if(ctx->str) {
        for(i = l = 0, j = 1; i < ctx->numstr; j += l, i++) {
            l = strlen(ctx->str[i].name) + 1;
            memcpy(ctx->sts + j, ctx->str[i].name, l);
        }
        free(ctx->str); ctx->str = NULL;
        ctx->numstr = 0;
    }
    for(i = 1; i < ctx->numtbl; i++)
        ctx->tbl[i].offs += ctx->offstr + ctx->numtbl * sizeof(tng_section_t);
    /* we have to reload the current tab */
    ui_switchtab(SUBMENU_NEWSPRITE + ui_tab);
    return 1;
}

/**
 * Free internal buffers
 */
void tng_free(tngctx_t *ctx)
{
    int i;

    if(!ctx) return;

    translate_freestrings();
    for(i = 0; i < TNG_IDX_LAST; i++)
        if(ctx->idx[i].ref) free(ctx->idx[i].ref);
    if(ctx->str) free(ctx->str);
    if(ctx->tbl) free(ctx->tbl);
    if(ctx->sts) free(ctx->sts);
    if(ctx->sec) free(ctx->sec);
    if(ctx->assets) {
        for(i = 0; i < ctx->numassets; i++)
            if(ctx->assets[i].data) free(ctx->assets[i].data);
        free(ctx->assets);
    }
    if(ctx->mapassets) {
        for(i = 0; i < ctx->nummapassets; i++)
            if(ctx->mapassets[i].data) free(ctx->mapassets[i].data);
        free(ctx->mapassets);
    }
    memset(ctx, 0, sizeof(tngctx_t));
}

/**
 * Encode double bytes
 */
void tng_rle_enc16(uint16_t *inbuff, int inlen, uint8_t *outbuff, int *outlen)
{
    int i, k, l, o;

    if(!inbuff || inlen < 1 || !outbuff || !outlen) return;

    k = o = 0; outbuff[o++] = 0;
    for(i = 0; i < inlen; i++) {
        for(l = 1; l < 128 && i + l < inlen && inbuff[i] == inbuff[i + l]; l++);
        if(l > 1) {
            l--; if(outbuff[k]) { outbuff[k]--; outbuff[o++] = 0x80 | l; } else outbuff[k] = 0x80 | l;
            *((uint16_t*)(outbuff + o)) = inbuff[i]; o += 2; k = o; outbuff[o++] = 0; i += l; continue;
        }
        outbuff[k]++; *((uint16_t*)(outbuff + o)) = inbuff[i]; o += 2;
        if(outbuff[k] > 127) { outbuff[k]--; k = o; outbuff[o++] = 0; }
    }
    if(!(outbuff[k] & 0x80)) { if(outbuff[k]) outbuff[k]--; else o--; }
    *outlen = o;
}

/**
 * Decode double bytes
 */
void tng_rle_dec16(uint8_t *inbuff, int inlen, uint16_t *outbuff, int *outlen)
{
    int l, o = 0;
    unsigned char *end = inbuff + inlen;

    if(!inbuff || inlen < 3 || !outbuff || !outlen) return;

    while(inbuff < end && o < *outlen) {
        l = ((*inbuff++) & 0x7F) + 1;
        if(inbuff[-1] & 0x80) {
            while(l-- && o < *outlen) { outbuff[o] = *((uint16_t*)(inbuff)); o++; }
            inbuff += 2;
        } else while(l-- && o < *outlen) { outbuff[o] = *((uint16_t*)(inbuff)); o++; inbuff += 2; }
    }
    *outlen = o;
}
