/*
 * tnge/dialogs.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Route dialogs window
 *
 */

#include "main.h"

void dialogs_load(void *data);
void dialogs_new(void *data);
void dialogs_save(void *data);
void dialogs_delete(void *data);
void dialogs_preview(void *data);
void dialogs_erase(void *data);
void dialogs_up(void *data);
void dialogs_down(void *data);
void dialogs_add(void *data);
void dialogs_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data);

typedef struct {
    int spc;
    int rattr;
    int rel;
    int rval;
    int pattr;
    int pval;
    char answer[64];
} dialogs_opt_t;

char dialogs_name[PROJ_NAMEMAX], dialogs_title[PROJ_TITLEMAX], dialogs_desc[PROJ_DESCMAX], dialogs_answer[PROJ_TITLEMAX];
int dialogs_tab, dialogs_left = 0, dialogs_attrlen;
ui_input_t dialogs_nameinp = { INP_ID, sizeof(dialogs_name), dialogs_name };
ui_input_t dialogs_titinp = { INP_NAME, sizeof(dialogs_title), dialogs_title };
ui_input_t dialogs_descinp = { INP_NAME, sizeof(dialogs_desc), dialogs_desc };
ui_sprsel_t dialogs_portrait = { -1, 3, 186, 1 };
ui_select_t dialogs_speech = { -1, LANG_NONE, NULL };
ui_select_t dialogs_answerspc = { -1, LANG_NONE, NULL };
ui_input_t dialogs_answerinp = { INP_NAME, sizeof(dialogs_answer), dialogs_answer };
ui_select_t dialogs_req = { -1, LANG_NONE, NULL };
ui_select_t dialogs_rel = { 0, 0, project_rels };
ui_num_t dialogs_rval = { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 };
ui_select_t dialogs_prov = { -1, LANG_NONE, NULL };
ui_num_t dialogs_pval = { 0, -PROJ_ATTRMAX, PROJ_ATTRMAX, 1 };

ui_tablehdr_t dialogs_hdr[] = {
    { SUBMENU_DIALOGS, 0, 0, 0 },
    { 0 }
};
ui_table_t dialogs_tbl = { dialogs_hdr, DIALOGS_NONE, 18, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(char*), NULL, NULL,
    NULL, NULL };

ui_tablehdr_t dialogs_opthdr[] = {
    { DIALOGS_OPTS, 0, 0, 0 },
    { 0 }
};
ui_table_t dialogs_opts = { dialogs_opthdr, DIALOGS_NOOPTS, 36, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, sizeof(dialogs_opt_t),
    dialogs_drawcell, NULL, NULL, NULL };

/**
 * The form
 */
ui_form_t dialogs_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, DIALOGS_DELETE, (void*)ICON_REMOVE, dialogs_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, dialogs_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, DIALOGS_PREVIEW, (void*)ICON_PVIEW, dialogs_preview },
    { FORM_BUTTONICN, 0, 0, 20, 20, 0, LANG_CLEARFORM, (void*)ICON_ERASE, dialogs_new },
    { FORM_INPUT, 0, 30, 200, 18, 0, DIALOGS_NAME, &dialogs_nameinp, NULL },
    { FORM_TABLE, 10, 30, 200, 0, 0, 0, &dialogs_tbl, dialogs_load },
    /* 6 */
    { FORM_TEXT, 0, 55, 0, 18, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 54, 0, 20, 0, DIALOGS_TITLE, &dialogs_titinp, NULL },
    /* 8 */
    { FORM_TEXT, 0, 79, 0, 18, 0, 0, NULL, NULL },
    { FORM_INPUT, 0, 78, 0, 20, 0, LANG_ATTRSTR, &dialogs_descinp, NULL },
    /* 10 */
    { FORM_SPCSEL, 0, 102, 0, 20, 0, DIALOGS_SPCH, &dialogs_speech, NULL },
    { FORM_BOOL, 0, 103, 0, 18, 0, DIALOGS_LEFT, &dialogs_left, NULL },
    /* 12 */
    { FORM_SPRITE, 0, 54, 68, 68, 0, DIALOGS_PORT, &dialogs_portrait, NULL },
    /* 13 */
    { FORM_TABLE, 0, 128, 0, 0, 0, 0, &dialogs_opts, NULL },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, DIALOGS_ERASE, (void*)ICON_REMOVE, dialogs_erase },
    { FORM_SPCSEL, 0, 0, 200, 20, 0, 0, &dialogs_answerspc, NULL },
    { FORM_INPUT, 0, 0, 0, 20, 0, 0, &dialogs_answerinp, NULL },
    { FORM_CHARBTN, 0, 0, 20, 20, 0, DIALOGS_UP, (void*)11014, dialogs_up },
    { FORM_CHARBTN, 0, 0, 20, 20, 0, DIALOGS_DOWN, (void*)11015, dialogs_down },
    { FORM_ICONBTN, 0, 0, 20, 20, 0, DIALOGS_ADD, (void*)ICON_ADD, dialogs_add },
    /* 20 */
    { FORM_SELECT, 0, 0, 0, 20, 0, DIALOGS_REQUIRE, &dialogs_req, NULL },
    { FORM_SELECT, 0, 0, 42, 20, 0, DIALOGS_REQUIRE, &dialogs_rel, NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, DIALOGS_REQUIRE, &dialogs_rval, NULL },
    { FORM_SELECT, 0, 0, 0, 20, 0, DIALOGS_PROVIDE, &dialogs_prov, NULL },
    { FORM_TEXT, 0, 0, 16, 18, 0, DIALOGS_PROVIDE, ":=", NULL },
    { FORM_NUM, 0, 0, 62, 20, 0, DIALOGS_PROVIDE, &dialogs_pval, NULL },
    { FORM_LAST }
};

/**
 * Draw table cell
 */
void dialogs_drawcell(SDL_Texture *dst, int idx, int sel, int x, int y, int w, int h, ui_tablehdr_t *hdr, void *data)
{
    dialogs_opt_t *opt = (dialogs_opt_t*)data;
    char tmp[128];

    (void)hdr;
    if(!data) return;
    if(opt->spc >= 0) ui_icon(dst, x + 4, y + (h - 16) / 2, ICON_SPCH);
    if(opt->rattr >= 0 || opt->pattr >= 0) {
        ui_text(dst, x + 24, y + h / 2 - 16, opt->answer);
        ssfn_dst.fg = theme[idx == sel ? THEME_DARK : THEME_LIGHT];
        if(opt->rattr >= 0) {
            sprintf(tmp, "%s%s%d", project.attrs[opt->rattr], project_rels[opt->rel], opt->rval);
            ui_text(dst, x + 24, y + h / 2, tmp);
        }
        if(opt->pattr >= 0) {
            sprintf(tmp, "%s:=%d", project.attrs[opt->pattr], opt->pval);
            ui_text(dst, x + w / 2, y + h / 2, tmp);
        }
        ssfn_dst.fg = theme[THEME_FG];
    } else
        ui_text(dst, x + 24, y + (h - 16) / 2, opt->answer);
}

/**
 * Exit dialogs window
 */
void dialogs_exit(int tab)
{
    (void)tab;
    dialogs_tbl.clk = -1;
}

/**
 * Enter dialogs window
 */
void dialogs_init(int tab)
{
    int i, j;

    dialogs_exit(tab);
    dialogs_form[1].param = lang[LANG_SAVE];
    dialogs_form[1].w = ui_textwidth(dialogs_form[1].param) + 40;
    if(dialogs_form[1].w < 200) dialogs_form[1].w = 200;
    dialogs_form[6].param = lang[CRAFTS_TITLE];
    i = dialogs_tab = ui_textwidth(dialogs_form[6].param); dialogs_form[6].w = i;
    dialogs_form[8].param = lang[DIALOGS_DESC];
    i = ui_textwidth(dialogs_form[8].param); dialogs_form[8].w = i;
    if(i > dialogs_tab) dialogs_tab = i;
    dialogs_form[6].x = dialogs_form[8].x = dialogs_form[13].x = dialogs_form[14].x = dialogs_form[5].x + dialogs_form[5].w + 10;
    dialogs_form[7].x = dialogs_form[9].x = dialogs_form[10].x = dialogs_form[6].x + dialogs_tab + 4;
    dialogs_form[15].x = dialogs_form[20].x = dialogs_form[14].x + dialogs_form[14].w + 32;
    dialogs_form[16].x = dialogs_form[15].x + dialogs_form[15].w + 4;
    dialogs_req.opts = dialogs_prov.opts = project.attrs;
    for(i = dialogs_attrlen = 0; i < project.numattrs; i++) {
        j = ui_textwidth(project.attrs[i]);
        if(j > dialogs_attrlen) dialogs_attrlen = j;
    }
    dialogs_attrlen += 24;
    project_freedir(&project.dialogs, &project.numdialogs);
    sprintf(projfn, "%s" SEP "%s", project.id, project_dirs[PROJDIRS_DIALOGS]);
    project.dialogs = project_getdir(projdir, ".dlg", &project.numdialogs);
    if(!dialogs_name[0]) dialogs_tbl.val = -1;
    dialogs_tbl.data = project.dialogs;
    dialogs_tbl.num = project.numdialogs;
    if(dialogs_tbl.val >= dialogs_tbl.num) dialogs_tbl.val = dialogs_tbl.num - 1;
}

/**
 * Resize the view
 */
void dialogs_resize(int tab, int w, int h)
{
    int i;

    (void)tab;
    dialogs_form[0].y = dialogs_form[1].y = dialogs_form[2].y = dialogs_form[3].y = h - 48;
    dialogs_form[1].x = w - 20 - dialogs_form[1].w;
    dialogs_form[2].x = dialogs_form[1].x - 52;
    ui_table_resize(&dialogs_form[5], dialogs_form[5].w, h - 89);
    dialogs_form[3].x = dialogs_form[5].x + dialogs_form[5].w + 20;
    dialogs_form[4].x = w - 10 - dialogs_form[4].w;
    dialogs_form[12].x = w - 10 - dialogs_form[12].w;
    dialogs_form[7].w = dialogs_form[9].w = dialogs_form[12].x - 4 - dialogs_form[7].x;
    dialogs_form[10].w = dialogs_form[11].w = dialogs_form[7].w / 2;
    dialogs_form[11].x = dialogs_form[10].x + dialogs_form[10].w + 16;
    ui_table_resize(&dialogs_form[13], w - 10 - dialogs_form[13].x, h - 82 - 24 - dialogs_form[13].y);
    for(i = 14; i < 20; i++)
        dialogs_form[i].y = h - 48 - 30 - 24;
    for(i = 20; i < 26; i++)
        dialogs_form[i].y = h - 48 - 30;
    dialogs_form[19].x = w - 30;
    dialogs_form[18].x = dialogs_form[19].x - dialogs_form[18].w - 2;
    dialogs_form[17].x = dialogs_form[18].x - dialogs_form[17].w - 2;
    dialogs_form[16].w = dialogs_form[17].x - dialogs_form[16].x - 20;
    i = (dialogs_form[17].x - dialogs_form[20].x) / 2;
    dialogs_form[23].x = dialogs_form[20].x + i;
    i -= 62 + 52;
    dialogs_form[20].w = dialogs_form[23].w = i < dialogs_attrlen ? i : dialogs_attrlen;
    dialogs_form[21].x = dialogs_form[20].x + dialogs_form[20].w + 2;
    dialogs_form[22].x = dialogs_form[21].x + dialogs_form[21].w + 2;
    dialogs_form[24].x = dialogs_form[23].x + dialogs_form[23].w + 2;
    dialogs_form[25].x = dialogs_form[24].x + dialogs_form[24].w + 2;
}

/**
 * View layer
 */
void dialogs_redraw(int tab)
{
    (void)tab;
    ui_form = dialogs_form;
    ui_form[0].inactive = (dialogs_tbl.val < 0);
    ui_form[1].inactive = (!dialogs_name[0] || !dialogs_desc[0]);
    ui_form[2].inactive = (!dialogs_desc[0]);
    ui_form[12].inactive = (!strcmp(dialogs_title, "{}"));
    ui_form[14].inactive = (dialogs_opts.val < 0);
    ui_form[17].inactive = (dialogs_opts.val < 1);
    ui_form[18].inactive = (dialogs_opts.val < 0 || dialogs_opts.val + 1 >= dialogs_opts.num);
    ui_form[19].inactive = (!dialogs_answer[0]);
    if(ui_form[12].inactive) dialogs_portrait.val = -1;
}

/**
 * Move answer option up
 */
void dialogs_up(void *data)
{
    dialogs_opt_t *list = (dialogs_opt_t*)dialogs_opts.data, tmp;

    (void)data;
    if(!list || dialogs_opts.val < 1 || dialogs_opts.val >= dialogs_opts.num) return;
    memcpy(&tmp, &list[dialogs_opts.val - 1], sizeof(dialogs_opt_t));
    memcpy(&list[dialogs_opts.val - 1], &list[dialogs_opts.val], sizeof(dialogs_opt_t));
    memcpy(&list[dialogs_opts.val], &tmp, sizeof(dialogs_opt_t));
    dialogs_opts.val--;
}

/**
 * Move answer option down
 */
void dialogs_down(void *data)
{
    dialogs_opt_t *list = (dialogs_opt_t*)dialogs_opts.data, tmp;

    (void)data;
    if(!list || dialogs_opts.val < 0 || dialogs_opts.val + 1 >= dialogs_opts.num) return;
    memcpy(&tmp, &list[dialogs_opts.val + 1], sizeof(dialogs_opt_t));
    memcpy(&list[dialogs_opts.val + 1], &list[dialogs_opts.val], sizeof(dialogs_opt_t));
    memcpy(&list[dialogs_opts.val], &tmp, sizeof(dialogs_opt_t));
    dialogs_opts.val++;
}

/**
 * Erase answer option
 */
void dialogs_erase(void *data)
{
    dialogs_opt_t *list = (dialogs_opt_t*)dialogs_opts.data;

    (void)data;
    if(!list || dialogs_opts.val < 0 || dialogs_opts.val >= dialogs_opts.num) return;
    memcpy(&list[dialogs_opts.val], &list[dialogs_opts.val + 1], (dialogs_opts.num - dialogs_opts.val) *
        sizeof(dialogs_opt_t));
    dialogs_opts.num--;
    if(dialogs_opts.val >= dialogs_opts.num)
        dialogs_opts.val = dialogs_opts.num - 1;
    if(!dialogs_opts.num) { free(list); dialogs_opts.data = NULL; }
}

/**
 * Add answer option
 */
void dialogs_add(void *data)
{
    dialogs_opt_t *list = (dialogs_opt_t*)dialogs_opts.data;
    int i;

    (void)data;
    if(!dialogs_answer[0]) return;
    list = (dialogs_opt_t*)realloc(list, (dialogs_opts.num + 1) * sizeof(dialogs_opt_t));
    dialogs_opts.data = list;
    if(!list) main_error(ERR_MEM);
    i = dialogs_opts.num++;
    list[i].spc = dialogs_answerspc.val;
    list[i].rattr = dialogs_req.val;
    list[i].rel = dialogs_rel.val;
    list[i].rval = dialogs_rval.val;
    list[i].pattr = dialogs_prov.val;
    list[i].pval = dialogs_pval.val;
    strcpy(list[i].answer, dialogs_answer);
    dialogs_answerspc.val = dialogs_req.val = dialogs_prov.val = -1;
    dialogs_rel.val = dialogs_rval.val = dialogs_pval.val = 0;
    dialogs_answer[0] = 0;
}

/**
 * Free resources
 */
void dialogs_free()
{
    dialogs_name[0] = dialogs_title[0] = dialogs_desc[0] = dialogs_answer[0] = 0;
    if(dialogs_opts.data) { free(dialogs_opts.data); dialogs_opts.data = NULL; }
    dialogs_opts.num = dialogs_left = 0; dialogs_portrait.val = dialogs_opts.val = dialogs_answerspc.val = dialogs_req.val =
    dialogs_prov.val = -1; dialogs_rel.val = dialogs_rval.val = dialogs_pval.val = 0;
}

/**
 * Clear form, new dialog
 */
void dialogs_new(void *data)
{
    (void)data;
    dialogs_free();
}

/**
 * Delete dialog
 */
void dialogs_delete(void *data)
{
    char **list = (char**)dialogs_tbl.data;

    (void)data;
    if(dialogs_tbl.val < 0 || !list) return;
    sprintf(projfn, "%s" SEP "%s" SEP "%s.dlg", project.id, project_dirs[PROJDIRS_DIALOGS], list[dialogs_tbl.val]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("dialogs_delete: removing %s\n", projdir);
        remove(projdir);
        dialogs_new(NULL);
        dialogs_init(SUBMENU_DIALOGS);
    }
}

/**
 * Save dialog
 */
void dialogs_save(void *data)
{
    dialogs_opt_t *opts = (dialogs_opt_t*)dialogs_opts.data;
    char **list = (char**)dialogs_tbl.data;
    FILE *f;
    int i;

    (void)data;
    if(!dialogs_name[0] || !dialogs_desc[0]) return;
    f = project_savefile(!list || dialogs_tbl.val < 0 || strcmp(list[dialogs_tbl.val], dialogs_name),
        project_dirs[PROJDIRS_DIALOGS], dialogs_name, "dlg", PROJMAGIC_DIALOG, "dialogs_save");
    if(f) {
        fprintf(f, "%s\r\n", dialogs_title[0] ? dialogs_title : "-");
        fprintf(f, "%s\r\n", dialogs_desc);
        project_wridx(f, dialogs_speech.val, project.speech); fprintf(f, " ");
        project_wrsprite(f, &dialogs_portrait); fprintf(f, " ");
        fprintf(f, "%u\r\n", dialogs_left);
        for(i = 0; i < dialogs_opts.num; i++) {
            if(opts[i].rattr >= 0) {
                project_wridx(f, opts[i].rattr, project.attrs);
                fprintf(f, "%s%d ", project_rels[opts[i].rel], opts[i].rval);
            } else
                fprintf(f, "- ");
            if(opts[i].pattr >= 0) {
                project_wridx(f, opts[i].pattr, project.attrs);
                fprintf(f, "=%d ", opts[i].pval);
            } else
                fprintf(f, "- ");
            project_wridx(f, opts[i].spc, project.speech); fprintf(f, " ");
            fprintf(f, "%s\r\n", opts[i].answer);
        }
        fclose(f);
        dialogs_init(SUBMENU_DIALOGS);
        for(dialogs_tbl.val = 0; dialogs_tbl.val < dialogs_tbl.num &&
            strcmp(dialogs_name, project.dialogs[dialogs_tbl.val]); dialogs_tbl.val++);
        sprintf(projfn, "%s" SEP "%s" SEP "%s.dlg", project.id, project_dirs[PROJDIRS_DIALOGS], dialogs_name);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVEDLG]);
}

/**
 * Load dialog
 */
int dialogs_loaddlg(char *fn, int opts)
{
    char *str, *s;

    if(!fn || !*fn) return 0;
    str = project_loadfile(project_dirs[PROJDIRS_DIALOGS], fn, "dlg", PROJMAGIC_DIALOG, "dialogs_loaddlg");
    if(str) {
        dialogs_new(NULL);
        s = project_skipnl(str);
        s = project_getstr2(s, dialogs_title, 2, sizeof(dialogs_title));
        s = project_skipnl(s);
        s = project_getstr2(s, dialogs_desc, 2, sizeof(dialogs_desc));
        s = project_skipnl(s);
        s = project_getidx(s, &dialogs_speech.val, project.speech, -1);
        s = project_getsprite(s, &dialogs_portrait);
        s = project_getint(s, &dialogs_left, 0, 1);
        if(opts) {
            while(*s) {
                s = project_skipnl(s);
                if(!*s) break;
                dialogs_req.val = dialogs_prov.val = dialogs_answerspc.val = -1; dialogs_rel.val = dialogs_rval.val =
                    dialogs_pval.val = 0;
                dialogs_answer[0] = 0;
                if(*s != '-') {
                    s = project_getidx(s, &dialogs_req.val, project.attrs, -1);
                    s = project_getrel(s, &dialogs_rel.val);
                    s = project_getint(s, &dialogs_rval.val, dialogs_rval.min, dialogs_rval.max);
                } else s++;
                while(*s == ' ') s++;
                if(*s != '-') {
                    s = project_getidx(s, &dialogs_prov.val, project.attrs, -1);
                    if(*s == '=')
                        s = project_getint(s + 1, &dialogs_pval.val, dialogs_pval.min, dialogs_pval.max);
                } else s++;
                s = project_getidx(s, &dialogs_answerspc.val, project.speech, -1);
                s = project_getstr2(s, dialogs_answer, 2, sizeof(dialogs_answer));
                dialogs_add(NULL);
            }
        }
        dialogs_req.val = dialogs_prov.val = dialogs_answerspc.val = -1; dialogs_rel.val = dialogs_rval.val =
            dialogs_pval.val = 0;
        dialogs_answer[0] = 0;
        free(str);
        strncpy(dialogs_name, fn, sizeof(dialogs_name) - 1);
        return 1;
    }
    return 0;
}

/**
 * Load dialog handler
 */
void dialogs_load(void *data)
{
    char **list = (char**)dialogs_tbl.data;

    (void)data;
    if(dialogs_tbl.val < 0 || !list) return;
    if(!dialogs_loaddlg(list[dialogs_tbl.val], 1))
        ui_status(1, lang[ERR_LOADDLG]);
}

/**
 * Load strings for translation
 */
void dialogs_loadstr()
{
    char *str, *s;
    int i;

    for(i = 0; i < project.numdialogs; i++) {
        str = project_loadfile(project_dirs[PROJDIRS_DIALOGS], project.dialogs[i], "dlg", PROJMAGIC_DIALOG, "dialogs_loadstr");
        if(str) {
            s = project_skipnl(str);
            s = project_gettrstr(s, 2, sizeof(dialogs_title));
            s = project_skipnl(s);
            s = project_gettrstr(s, 2, sizeof(dialogs_desc));
            s = project_skipnl(s);
            while(*s) {
                s = project_skipnl(s);
                if(!*s) break;
                s = project_skiparg(s, 3);
                s = project_gettrstr(s, 2, sizeof(dialogs_answer));
            }
            free(str);
        }
    }
}

/**
 * Display a dialog
 */
void dialogs_display(SDL_Texture *dst, int w, int h, int dlg, Mix_Chunk **snd, SDL_Texture **portrait, SDL_Rect *dprt)
{
    SDL_Rect rect;
    int pw = 0, nl;
    char *str;
    ui_sprite_t *s = NULL;

    if(!dst || !portrait || !dprt || dlg < 0 || dlg >= project.numdialogs) return;
    SDL_SetTextureBlendMode(dst, SDL_BLENDMODE_BLEND);
    dialogs_loaddlg(project.dialogs[dlg], 0);
    if(dialogs_portrait.val >= 0) {
        s = spr_getidx(dialogs_portrait.cat, dialogs_portrait.val, -1, 0);
        if(s && s->data) {
            pw = (dialogs_left ? -s->w : s->w);
            spr_texture(s, portrait);
        }
        dprt->w = pw; dprt->h = s->h;
    } else {
        *portrait = NULL;
        dprt->w = dprt->h = 0;
    }
    str = elements_dlgstr(dialogs_desc, w * 3 / 4, pw, 3, &nl);
    elements_dlgsize(w, h, nl, 0, &rect.x, &rect.y, &rect.w, &rect.h);
    dprt->x = dialogs_left ? rect.x : rect.x + rect.w - pw; dprt->y = rect.y + rect.h - dprt->h;
    elements_dialog(dst, w, h, rect.x, rect.y, rect.w, rect.h, dialogs_title, str, pw);
    if(dialogs_speech.val >= 0 && snd) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
            project.speech[dialogs_speech.val], lang[-1][0], lang[-1][1]);
        if(verbose) printf("dialogs_dialog: loading speech %s\n", projdir);
        *snd = Mix_LoadWAV_RW(SDL_RWFromFile(projdir, "rb"), 1);
        if(*snd) Mix_VolumeChunk(*snd, MIX_MAX_VOLUME);
    }
    dialogs_free();
}

/**
 * Preview dialog
 */
void dialogs_preview(void *data)
{
    Mix_Music *music = NULL;
    dialogs_opt_t *opts = (dialogs_opt_t*)dialogs_opts.data;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *bg = NULL, *texture = NULL, *portrait = NULL;
    SDL_Rect dlg, sprt, dprt;
    char **dlgopts, *str;
    int wf = SDL_GetWindowFlags(window), i, j, k, x, y, p, pw = 0, nl, noaudio = 0, usrprt;
    uint32_t *pixels;
    ui_sprite_t *s = NULL;

    (void)data;
    if(verbose) printf("dialogs_preview: started\n");

    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_ShowCursor(SDL_DISABLE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;

    usrprt = !strcmp(dialogs_title, "{}");
    if(dialogs_portrait.val >= 0) {
        s = spr_getidx(dialogs_portrait.cat, dialogs_portrait.val, -1, 0);
        if(s) {
            pw = (dialogs_left ? -s->w : s->w);
            dprt.w = sprt.w = s->w; dprt.h = sprt.h = s->h;
            spr_texture(s, &portrait);
        }
    } else
    if(usrprt) {
        pw = (dialogs_left ? -portrait_icon.w : portrait_icon.w);
        dprt.w = portrait_icon.w; dprt.h = portrait_icon.h;
    }
    dlgopts = (char**)main_alloc((dialogs_opts.num + 1) * sizeof(char *));
    for(i = 0; i < dialogs_opts.num; i++)
        dlgopts[i] = (char*)&opts[i].answer;
    dlgopts[i] = NULL;
    elements_load();
    elements_loadfonts();
    str = elements_dlgstr(dialogs_desc, dm.w, pw, 3, &nl);
    elements_dlgsize(dm.w, dm.h, nl, dialogs_opts.num, &dlg.x, &dlg.y, &dlg.w, &dlg.h);
    dprt.x = dialogs_left ? dlg.x : dlg.x + dlg.w - pw; dprt.y = dlg.y + dlg.h - dprt.h;
    sprt.x = sprt.y = 0;

    bg = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
    if(bg) {
        SDL_LockTexture(bg, NULL, (void**)&pixels, &p);
        /* checker */
        for(j = k = 0; j < dm.h; j++, k += p/4)
            for(i = 0; i < dm.w ; i++) {
                x = (i >> 5); y = (j >> 5);
                pixels[k + i] = theme[(y & 1) ^ (x & 1) ? THEME_LIGHT : THEME_DARK];
            }
        SDL_UnlockTexture(bg);
        elements_dialog(bg, dm.w, dm.h, dlg.x, dlg.y, dlg.w, dlg.h, !usrprt ? dialogs_title : lang[MAINMENU_MERLIN], str, pw);
    }
    if(str) free(str);
    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
    if(texture) {
        SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
        memset(pixels, 0, p * dm.h);
        SDL_UnlockTexture(texture);
        elements_dlgopt(texture, dm.w, dm.h, dlg.x, dlg.y, dlg.w, dlg.h, dlgopts, dialogs_opts.num, -1, pw);
    }
    elements_freefonts();
    free(dlgopts);

    if(Mix_OpenAudio(FREQ, AUDIO_S16SYS, 1, 4096) < 0) { ui_status(1, lang[ERR_AUDIO]); noaudio = 1; }

    if(dialogs_speech.val >= 0 && !noaudio) {
        sprintf(projfn, "%s" SEP "%s" SEP "%s_%c%c.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 2],
            project.speech[dialogs_speech.val], lang[-1][0], lang[-1][1]);
        if(verbose) printf("dialogs_preview: loading speech %s\n", projdir);
        music = Mix_LoadMUS(projdir);
        if(music) {
            Mix_VolumeMusic(MIX_MAX_VOLUME);
            Mix_PlayMusic(music, 0);
        } else
            ui_status(1, lang[ERR_LOADSND]);
    }

    while(1) {
        /* events */
        SDL_WaitEvent(&event);
        if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
            SDL_PushEvent(&event); goto cleanup;
        }
        switch(event.type) {
            case SDL_KEYUP: goto cleanup; break;
            case SDL_MOUSEBUTTONUP: goto cleanup; break;
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        if(bg) SDL_RenderCopy(renderer, bg, NULL, NULL);
        if(texture) SDL_RenderCopy(renderer, texture, NULL, NULL);
        if(portrait)
            SDL_RenderCopyEx(renderer, portrait, &sprt, &dprt, 0, NULL, dialogs_left ? SDL_FLIP_HORIZONTAL : 0);
        else
        if(usrprt)
            SDL_RenderCopyEx(renderer, icons, &portrait_icon, &dprt, 0, NULL, dialogs_left ? SDL_FLIP_HORIZONTAL : 0);
        SDL_RenderPresent(renderer);
    }

cleanup:
    if(!noaudio) {
        if(music) { Mix_HaltMusic(); Mix_FreeMusic(music); }
        Mix_CloseAudio();
    }
    if(portrait) SDL_DestroyTexture(portrait);
    if(texture) SDL_DestroyTexture(texture);
    if(bg) SDL_DestroyTexture(bg);
    if(verbose) printf("dialogs_preview: stopped\n");
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Get references
 */
void dialogs_ref(tngctx_t *ctx, int idx)
{
    char *str, *s;
    int j;

    if(!tng_ref(ctx, TNG_IDX_DLG, idx)) return;
    str = project_loadfile(project_dirs[PROJDIRS_DIALOGS], project.dialogs[idx], "dlg", PROJMAGIC_DIALOG, "dialogs_ref");
    if(str) {
        s = project_skipnl(str);
        s = project_skipnl(s);
        s = project_skipnl(s);
        while(*s) {
            s = project_skipnl(s);
            if(!*s) break;
            if(*s != '-') {
                s = project_getidx(s, &j, project.attrs, -1);
                attrs_ref(ctx, j);
                s = project_skiparg(s, 1);
            } else s++;
            while(*s == ' ') s++;
            if(*s != '-') {
                s = project_getidx(s, &j, project.attrs, -1);
                attrs_ref(ctx, j);
            }
        }
        free(str);
    }
}

/**
 * Save tng
 */
int dialogs_totng(tngctx_t *ctx)
{
    dialogs_opt_t *opts;
    uLongf cl;
    uint8_t *buf, *ptr, *comp;
    int i, j, l;

    if(!ctx || !project.dialogs || !project.numdialogs) return 1;
    /* check if there's at least one dialog referenced */
    for(j = 0; j < project.numdialogs && !ctx->idx[TNG_IDX_DLG].ref[j]; j++);
    if(j >= project.numdialogs) return 1;
    /* add dialogs to output */
    tng_section(ctx, TNG_SECTION_DIALOGS);
    for(j = 0; j < project.numdialogs; j++) {
        if(!ctx->idx[TNG_IDX_DLG].ref[j]) continue;
        if(!dialogs_loaddlg(project.dialogs[j], 1)) {
            ui_switchtab(SUBMENU_DIALOGS);
            dialogs_tbl.val = j;
            ui_status(1, lang[ERR_LOADDLG]);
            return 0;
        }
        opts = (dialogs_opt_t*)dialogs_opts.data;
        l = 14 + dialogs_opts.num * 21;
        buf = ptr = (uint8_t*)main_alloc(l);
        ptr = tng_asset_text(ptr, ctx, dialogs_title);
        ptr = tng_asset_text(ptr, ctx, dialogs_desc);
        ptr = tng_asset_idx(ptr, ctx, project.speech, TNG_IDX_SPC, dialogs_speech.val);
        if(!strcmp(dialogs_title, "{}")) {
            *ptr++ = dialogs_portrait.cat; *ptr++ = 0xF0; *ptr++ = 0xFF;
        } else
            ptr = tng_asset_sprite(ptr, ctx, dialogs_portrait.cat, dialogs_portrait.val);
        *ptr++ = dialogs_left;
        *ptr++ = dialogs_opts.num;
        for(i = 0; i < dialogs_opts.num; i++) {
            ptr = tng_asset_idx(ptr, ctx, project.attrs, TNG_IDX_ATR, opts[i].rattr);
            *ptr++ = opts[i].rel;
            memcpy(ptr, &opts[i].rval, 4); ptr += 4;
            ptr = tng_asset_idx(ptr, ctx, project.attrs, TNG_IDX_ATR, opts[i].pattr);
            memcpy(ptr, &opts[i].pval, 4); ptr += 4;
            ptr = tng_asset_idx(ptr, ctx, project.speech, TNG_IDX_SPC, opts[i].spc);
            ptr = tng_asset_text(ptr, ctx, opts[i].answer);
        }
        cl = compressBound(l);
        comp = (uint8_t*)main_alloc(cl);
        compress2(comp, &cl, buf, l, 9);
        if(cl) {
            tng_desc(ctx, project.dialogs[j], cl);
            tng_asset_int(ctx, comp, cl);
        } else
            free(comp);
        free(buf);
        /* free resources */
        dialogs_new(NULL);
    }
    return 1;
}

/**
 * Read from tng
 */
int dialogs_fromtng(tngctx_t *ctx)
{
    FILE *f;
    tng_asset_desc_t *asset;
    uint8_t *buf, *ptr, *end;
    int i, j, l, len, n, r;

    if(!ctx || !ctx->tbl || !ctx->numtbl || !ctx->sts) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_DIALOGS; i++);
    if(i >= ctx->numtbl) return 1;
    asset = (tng_asset_desc_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]) / sizeof(tng_asset_desc_t);
    if(len < 1) return 0;
    for(j = 0; j < len; j++, asset++) {
        ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
        buf = ptr = tng_get_asset(ctx, asset->offs, asset->size, &l);
        if(buf) {
            if(!asset->name || asset->name >= TNG_SECTION_SIZE(&ctx->tbl[0]) || l < 14) { free(buf); continue; }
            end = buf + l;
            f = project_savefile(0, project_dirs[PROJDIRS_DIALOGS], (char*)ctx->sts + asset->name, "dlg", PROJMAGIC_DIALOG,
                "dialogs_fromtng");
            if(f) {
                ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                ptr = tng_wridx(ctx, ptr, f); fprintf(f, " ");
                ptr = tng_wrsprite(ctx, ptr, f); fprintf(f, " ");
                fprintf(f, "%u\r\n", *ptr++);
                n = *ptr++;
                for(i = 0; i < n && ptr < end; i++) {
                    r = 0; memcpy(&r, ptr, 3);
                    if(!r) {
                        fprintf(f, "- ");
                        ptr += 8;
                    } else {
                        ptr = tng_wridx(ctx, ptr, f);
                        r = *ptr++;
                        fprintf(f, "%s%d ", r != 0xff ? project_rels[r] : "=", *((int32_t*)ptr));
                        ptr += 4;
                    }
                    r = 0; memcpy(&r, ptr, 3);
                    if(!r) {
                        fprintf(f, "- ");
                        ptr += 7;
                    } else {
                        ptr = tng_wridx(ctx, ptr, f);
                        fprintf(f, "=%d ", *((int32_t*)ptr));
                        ptr += 4;
                    }
                    ptr = tng_wridx(ctx, ptr, f); fprintf(f, " ");
                    ptr = tng_wrtext(ctx, ptr, f); fprintf(f, "\r\n");
                }
                fclose(f);
            }
            free(buf);
        }
    }
    return 1;
}
