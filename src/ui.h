/*
 * tnge/ui.h
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Common user interface definitions and prototypes
 *
 */

#ifndef SSFN_IMPLEMENTATION
#include "libsfn.h"
#endif

/* minimum screen size */
#define SCR_MINW    784
#define SCR_MINH    512

/* number of sprites per layer */
#define SPRPERLYR   20

/* frames per second */
#define FPS         30

/* file type filters */
enum {
    FILEOPS_TNG, FILEOPS_ZIP, FILEOPS_FONT, FILEOPS_SPRITE, FILEOPS_PALETTE, FILEOPS_AUDIO, FILEOPS_MOVIE, FILEOPS_TRANSLATION
};

/* enumerations for colors, cursors and icons */
enum {
    THEME_BG,   THEME_FG,    THEME_LIGHT,  THEME_DARK,  THEME_LIGHTER, THEME_DARKER, THEME_INPBG, THEME_ERR,
    THEME_BTNB, THEME_BTN0L, THEME_BTN0BL, THEME_BTN0BD,THEME_BTN0D,   THEME_BTN1L,  THEME_BTN1BL,THEME_BTN1BD,THEME_BTN1D,
    THEME_SELBG,THEME_SELFG, THEME_CURSOR, THEME_INA,   THEME_SELBX,   THEME_PATH,   THEME_GRID1, THEME_GRID2,
    THEME_CMNT, THEME_LOOP,  THEME_SWITCH, THEME_LET,   THEME_FUNC,

    THEME_LAST
};

enum { CURSOR_LOADING, CURSOR_PTR, CURSOR_CROSS, CURSOR_MOVE, CURSOR_GRAB };

enum {
    ICON_NEW,    ICON_LOAD,   ICON_SAVE,  ICON_KEYS,   ICON_QUIT,   ICON_FONT,  ICON_IMG,    ICON_MUS,
    ICON_SND,    ICON_SPCH,   ICON_MOV,   ICON_CUTSC,  ICON_UI,     ICON_CRGHT, ICON_CHAR,   ICON_ATTRS,
    ICON_NPC,    ICON_DLG,    ICON_OBJ,   ICON_TILE,   ICON_MAP,    ICON_QUEST, ICON_NEWLNG, ICON_LANG,
    ICON_SRCH,   ICON_HIST,   ICON_DIR,   ICON_FFONT,  ICON_FPIC,   ICON_FSND,  ICON_FMOV,   ICON_FPAL,
    ICON_FTNG,   ICON_FOBJ,   ICON_FZIP,  ICON_FILE,   ICON_PLAYO,  ICON_PLAYC, ICON_WAITC,  ICON_NPCMAP,
    ICON_WALK,   ICON_BLOCK,  ICON_SWIM,  ICON_FLY,    ICON_CLIMB,  ICON_JUMP,  ICON_ABOVE,  ICON_BELOW,
    ICON_INMUS,  ICON_ALTIT,  ICON_SHELF, ICON_DUMMY0, ICON_DUMMY1, ICON_DUMMY2,ICON_DUMMY3, ICON_DUMMY4,
    ICON_N,      ICON_NE,     ICON_E,     ICON_SE,     ICON_S,      ICON_SW,    ICON_W,      ICON_NW,
    ICON_DEATH,  ICON_ACTION, ICON_INACT, ICON_INLNK,  ICON_HELP,   ICON_INBG,  ICON_START,  ICON_ALERT,
    ICON_INFO,   ICON_LAYERS, ICON_ZOUT,  ICON_ZIN,    ICON_ZNORM,  ICON_CUT,   ICON_COPY,   ICON_PASTE,
    ICON_PIPETTE,ICON_PAINT,  ICON_FILL,  ICON_ERASE,  ICON_SELBOX, ICON_SELPLY,ICON_SELFZY, ICON_REMOVE,
    ICON_ADD,    ICON_WARN,   ICON_ERROR, ICON_QMARK,  ICON_PAL,    ICON_SETUP, ICON_END,    ICON_PVIEW,
    ICON_BATTLE, ICON_LINK,   ICON_BOLD,  ICON_ITALIC, ICON_VISIB,  ICON_PLAY,  ICON_SWRB,   ICON_INPAL,
    ICON_INLOAD, ICON_INSAVE, ICON_INKEYS,ICON_INIMG,  ICON_INERAS, ICON_INPLAY,ICON_INSWRB, ICON_CHSR,
    ICON_INREMOV,ICON_INHIST, ICON_INPVW, ICON_INSCR,  ICON_INCUT,  ICON_INCOPY,ICON_INPASTE,ICON_INMOV,
    ICON_INADD,  ICON_INLYRS, ICON_MERGE, ICON_INMERGE,ICON_MOVE,   ICON_INMOVE,ICON_RAND,   ICON_MARKET,
    ICON_LOOP,   ICON_SWITCH, ICON_LET,   ICON_DELAY,  ICON_OBJREPL,ICON_OBJADD,ICON_OBJREM, ICON_GIVE,
    ICON_TAKE,   ICON_HURT,   ICON_SHIELD,ICON_HOME,   ICON_DISPLAY,ICON_INVENT,ICON_BACKGR, ICON_PORT,
    ICON_EVENT,  ICON_SCRIPT, ICON_IDLE,  ICON_RUN,    ICON_BODY,   ICON_HEAD,  ICON_TOOLS,  ICON_CRAFT,
    /* icons after this must match ui_lngicn[] strings in ui.c */
    ICON_LAST
};
#define ICON_NUMACT  1024

/* map masks */
#define MAP_MASK_GRD1   (1<<0)
#define MAP_MASK_GRD2   (1<<1)
#define MAP_MASK_GRD3   (1<<2)
#define MAP_MASK_GRD4   (1<<3)
#define MAP_MASK_OBJ    (1<<4)
#define MAP_MASK_NPC    (1<<5)
#define MAP_MASK_SWNR   (1<<6)
#define MAP_MASK_ROOF1  (1<<7)
#define MAP_MASK_ROOF2  (1<<8)
#define MAP_MASK_ROOF3  (1<<9)
#define MAP_MASK_ROOF4  (1<<10)
#define MAP_MASK_COLL   (1<<11)
#define MAP_MASK_PATH   (1<<12)
#define MAP_MASK_GRID   (1<<13)

/* form spec */
enum {
    FORM_LAST,                          /* used to mark end of form */
    FORM_TNGLOGO,                       /* logos */
    FORM_TITLE,                         /* string formatted as title */
    FORM_TEXT,                          /* normal text */
    FORM_COMMENT,                       /* string formatted as comment */
    FORM_LICENSE,                       /* string formatted as license */
    FORM_BOX,                           /* a box */
    FORM_ICON,                          /* an icon */
    FORM_INPUT,                         /* text input box */
    FORM_INPUTSB,                       /* text input box with selected element background */
    FORM_SELECT,                        /* option selector */
    FORM_SELPTR,                        /* option selector with pointer value */
    FORM_COMBO,                         /* combo box, editable option select */
    FORM_BOOL,                          /* checkbox */
    FORM_NUM,                           /* decimal number box with up / down controls */
    FORM_NUMPTR,                        /* decimal number with pointer to value */
    FORM_NUMPTRSB,                      /* decimal number with pointer to value, but with selection background */
    FORM_TIME,                          /* time box with up / down controls */
    FORM_TIMEPTR,                       /* time with pointer to value */
    FORM_FADE,                          /* fade in / out slider */
    FORM_BUTTON,                        /* a gradiented submit button with text */
    FORM_BUTTONICN,                     /* a gradiented submit button with icon */
    FORM_BUTTONICNP,                    /* a pressed gradiented submit button with icon */
    FORM_CLRVER,                        /* color version selector */
    FORM_COLOR,                         /* color picker */
    FORM_PALETTE,                       /* palette options popup and icon button */
    FORM_TABLE,                         /* a data table */
    FORM_DRAWTBL,                       /* just draw a table, but do not use its event handlers */
    FORM_CHARBTN,                       /* a simple button with a character on it */
    FORM_CHRVBTN,                       /* a simple button with a character from a variable on it */
    FORM_CHRTBTN,                       /* a simple button with animation type on it */
    FORM_CHRDBTN,                       /* a simple button with direction on it */
    FORM_CHARGRP,                       /* multiple simple buttons with characters one for each value */
    FORM_ICONBTN,                       /* a simple button with an icon */
    FORM_ICONBTNP,                      /* a pressed simple button with an icon */
    FORM_ICONTGL,                       /* a simple button with an icon which toggles a flag */
    FORM_ICONSEL,                       /* a simple button with an icon which opens an option selector */
    FORM_ICNSPTR,                       /* a simple button with an icon which opens an option selector for a pointer value */
    FORM_ICONGRP,                       /* multiple simple buttons with icons one for each value */
    FORM_SPRGRP,                        /* multiple simple buttons with icons or sprites one for each value */
    FORM_SPRITE,                        /* sprite selector */
    FORM_MAPSEL,                        /* map selector */
    FORM_MAPOPT,                        /* map option list (no coordinates) */
    FORM_PATHSEL,                       /* path selector */
    FORM_MUSSEL,                        /* music selector */
    FORM_SNDSEL,                        /* sound selector */
    FORM_SPCSEL,                        /* speech selector */
    FORM_MOVSEL,                        /* movie selector */
    FORM_ZOOM,                          /* three simple buttons for zoom control. WARNING param is a ui_form ref to scrarea */
    FORM_SCRAREA,                       /* scrollable area with both horizontal and vertical scrollbars */
    FORM_FRAMES,                        /* animation frames selector */
    FORM_COMMANDS,                      /* script area */
    FORM_CMDPAL,                        /* command palette for scripts */
    FORM_CMDVAR,                        /* variable selector */
    FORM_COORD,                         /* coordinate picker */
    FORM_COLL,                          /* collision mask setter */
    FORM_FLDGRP,                        /* field group */
    FORM_STAT,                          /* define status bar area */
    FORM_CUTSCN,                        /* cutscene editor */
    FORM_CUTSCNSEL,                     /* cutscene selection */
    FORM_EXPR,                          /* expression */
    FORM_CHRSEL,                        /* chooser selector */
    FORM_CUTSEL,                        /* cutscene selector */
    FORM_NPCSEL,                        /* NPC selector */
    FORM_DLGSEL,                        /* dialog selector */
    FORM_OBJSEL,                        /* object selector */
    FORM_CFTSEL,                        /* crafting selector */
    FORM_DIRSEL,                        /* direction selector */
    FORM_QSTSEL,                        /* quest selector */
    FORM_EVTSEL,                        /* event selector */
    FORM_BEHSEL,                        /* behaviour selector */
    FORM_TRNSEL,                        /* transportation selector */
    FORM_CLKSEL,                        /* clock (hour) selector */
    FORM_ACNSEL,                        /* action animation selector */
    FORM_ALERT,                         /* alert message selector */
    FORM_ALTITUDE                       /* altitude selector */
};

enum {
    CMDPAL_ALL,                         /* all options command palette */
    CMDPAL_START,                       /* command palette for the start up script */
    CMDPAL_TIMER,                       /* command palette for the timer scripts */
    CMDPAL_ATTR,                        /* command palette for attribute change scripts */
    CMDPAL_ACTION,                      /* command palette for action scripts */
    CMDPAL_CUTSCENE,                    /* command palette for cutscene scripts */
    CMDPAL_OBJEVT,                      /* command palette for object event scripts */
    CMDPAL_NPCEVT,                      /* command palette for NPC event scripts */
    CMDPAL_PRICE,                       /* command palette for price generation */
    CMDPAL_QUEST                        /* command palette for quest completed scripts */
};

typedef struct _cmd_t cmd_t;

struct _cmd_t {
    int type, w, h, len, aw[4];
    char arg1[PROJ_EXPRMAX + 1], err;
    int arg2, arg3, arg4, arg5;
    struct _cmd_t *children;
};

typedef struct {
    int type;                           /* one of FORM_* defines */
    int x, y, w, h;                     /* position and dimension */
    int inactive;                       /* 0 active, 1 inactive, 2 hidden */
    int status;                         /* statusbas message LANG_* */
    void *param;                        /* type dependent */
    void (*onchange)(void *param);      /* on click / on change handler */
} ui_form_t;
extern ui_form_t *ui_form;

enum { INP_NAME, INP_ID, INP_VAR, INP_EXPR, INP_HEX, INP_TEXT };
typedef struct {                        /* param for FORM_INPUT */
    int type;                           /* one of INP_* defines */
    int maxlen;                         /* maximum length */
    char *str;                          /* string */
} ui_input_t;

typedef struct {                        /* param for FORM_SELECT */
    int val;                            /* currently selected value, indexing opts, can be -1 */
    int first;                          /* if first entry is special, LANG_*, otherwise 0 */
    char **opts;                        /* options */
} ui_select_t;

typedef struct {                        /* param for FORM_COMBO */
    ui_input_t inp;                     /* combo allows editing, and copies the string from opts on select */
    ui_select_t sel;
} ui_combo_t;

typedef struct {                        /* param for FORM_SELPTR */
    int *val;                           /* pointer to currently selected value, indexing opts, can be -1 */
    ui_select_t sel;
} ui_selptr_t;

typedef struct {                        /* param for FORM_NUM, FORM_TIME */
    int val, min, max, step;            /* number, minimum maximum limits and stepping */
} ui_num_t;

typedef struct {                        /* param for FORM_NUMPTR, FORM_TIMEPTR */
    int *val, min, max, step;           /* number, minimum maximum limits and stepping */
} ui_numptr_t;

typedef struct {                        /* param for FORM_FADE */
    int *in;                            /* fade in value */
    int *out;                           /* fade out value */
    int *min;                           /* start time, optional */
    int *max;                           /* end time */
    int si, so;                         /* slider positions, initialize as zero */
} ui_fade_t;

typedef struct {                        /* param for FORM_CLRVER */
    int *val;                           /* selected color */
    uint32_t *pal;                      /* palette */
    int p;                              /* palette pitch */
} ui_clrver_t;

typedef struct {
    int label;                          /* label, LANG_*, last entry must be 0, -1 means no label */
    int width;                          /* non-zero for fixed coloumns */
    int order;                          /* 1 if label is clickable for changing order */
    int w;                              /* calculated width */
} ui_tablehdr_t;

typedef void (*ui_drawcell_t)(
    SDL_Texture *dst,
    int idx,                            /* index of cell */
    int sel,                            /* index of selected cell */
    int x, int y, int w, int h,         /* where to draw */
    ui_tablehdr_t *hdr,                 /* header specification (for a row, cell widths in hdr[i].w) */
    void *data);                        /* pointer to current data entry */

typedef struct {                        /* param for FORM_TABLE */
    ui_tablehdr_t *hdr;                 /* header list */
    int nodata;                         /* text if there's no data, LANG_* */
    int row, col, dcol;                 /* in pixels, col can be zero. dcol draw coloumns, but threat as rows */
    int first;                          /* has a special first element */
    int fld, order, val, scr, page;     /* calculated values, initialize as zero */
    int clk;                            /* calculated value, initialize as -1 */
    int num;                            /* number of data entries */
    int entsize;                        /* size of one data entry */
    ui_drawcell_t drawcell;             /* callback to draw a table row (if col 0) or cell (if col non-zero) */
    ui_drawcell_t rendercell;           /* callback to render a table cell */
    void (*reorder)(void *table);       /* called when header clicked */
    void *data;                         /* list of data entries, each 'entsize' in length, 'num' records */
} ui_table_t;

typedef struct {                        /* param for FORM_BOX */
    uint32_t l, b, d;                   /* light, background and dark colors */
} ui_box_t;

typedef struct {                        /* param for FORM_ICONSEL */
    int icon;                           /* one of ICON_* */
    int *flags;                         /* flags */
    ui_select_t sel;                    /* selection */
} ui_iconsel_t;

typedef struct {                        /* param for FORM_ICNSPTR */
    int icon;                           /* one of ICON_* */
    int *flags;                         /* flags */
    ui_selptr_t sptr;                   /* selection with a value pointer */
} ui_icnsptr_t;

typedef struct {                        /* param for FORM_ICONTGL */
    int icon;                           /* one of ICON_* */
    int mask;                           /* mask to use */
    int *val;                           /* address of value */
} ui_icontgl_t;

typedef struct {
    int icon;                           /* one of ICON_* */
    int status;                         /* statusbar message */
} ui_icongrpelem_t;

typedef struct {                        /* param for FORM_ICONGRP and FORM_CHARGRP */
    int *val;                           /* address of value */
    int first;                          /* offset of first option in value */
    int num;                            /* number of options */
    ui_icongrpelem_t *elems;            /* option elements */
} ui_icongrp_t;

typedef struct {
    char name[FILENAME_MAX];            /* name of the option */
    int icon;                           /* one of ICON_* or zero */
    uint32_t spr[256];                  /* 16 x 16 icon sprite */
    int num;                            /* number of options */
    void *opts;                         /* options */
} ui_sprgrpelem_t;

typedef struct {                        /* param for FORM_SPRGRP */
    int *val;                           /* address of value */
    int first;                          /* offset of first option in value */
    int num;                            /* number of options */
    ui_sprgrpelem_t *elems;             /* option elements */
} ui_sprgrp_t;

typedef struct {                        /* param for FORM_SPRITE */
    int val;                            /* select sprite, -1 if none */
    int cat;                            /* sprite category */
    int icon;                           /* unicode icon if nothing selected */
    int unique;                         /* each cell is for a different sprite */
} ui_sprsel_t;

typedef struct {
    char *dir;
    int w;
} ui_pathelem_t;

typedef struct {                        /* param for FORM_PATHSEL */
    int fld;                            /* pressed path component */
    int num;                            /* number of path components */
    ui_pathelem_t path[256];            /* path elements */
    ui_select_t sel;                    /* drive selector */
} ui_pathsel_t;

#define SCRAREA_VBAR    1
#define SCRAREA_HBAR    2
#define SCRAREA_BOTH    3
typedef struct {                        /* param for FORM_SCRAREA */
    int x, y;                           /* current offsets */
    int w, h;                           /* total size (bigger than form element w, h) */
    int z;                              /* zoom factor */
    int ow, oh;                         /* original width and height */
    int sx, sy;                         /* on screen x and y */
    int sw, sh;                         /* on screen width and height */
} ui_scrarea_t;

typedef struct {
    int *scrx, *scry;                   /* scroll positions */
    int *selx, *sely;                   /* current record selector */
    int origx, origy;                   /* original scroll positions */
    int posx, posy;                     /* position on scrollbar */
    int pagex, numx, pagey, numy;       /* dimensions in records */
    int srtx, sizex, srty, sizey;       /* dimensions in pixels */
    int min;                            /* minimum record */
    int form;                           /* form index */
} ui_scroll_t;

typedef struct {                        /* param for FORM_FRAMES */
    int num;                            /* number of frames */
    int dir;                            /* facing direction */
    int scr;                            /* scroll start */
    int clk;                            /* clicked frame */
    int str;                            /* selection start */
    int cnt;                            /* selected frames count */
    int size;                           /* one frame's width and height */
    uint32_t *data;                     /* pixel buffer, size x size * num */
} ui_frames_t;

typedef struct {
    char name[PROJ_NAMEMAX + 1];        /* name of the sprite */
    char type;                          /* animation type 0 - 2 */
    char dir;                           /* direction 0 - 7 */
    char nframe;                        /* number of frames */
    int ow;                             /* original width */
    int w;                              /* total width, nframe * ow */
    int h;                              /* height */
    int *grid;                          /* grid coordinates where it was read from */
    SDL_Texture *data;                  /* pixel data */
} ui_implist_t;

typedef struct {
    char type;                          /* animation type 0 - 2 */
    char nframe;                        /* number of frames */
    int w;                              /* one frame's width */
    int h;                              /* height */
    uint32_t *data;                     /* pixel data */
} ui_sprite_t;

typedef struct {
    char *name;                         /* common name part */
    char *loc;                          /* localized name */
    int idx;                            /* search index */
    int x, y;                           /* position on atlas if loaded from an atlas */
    ui_sprite_t dir[8];                 /* one animated sprite per direction */
    SDL_Texture *txt;                   /* cached texture */
} ui_sprlist_t;

typedef struct {                        /* param for FORM_MAPSEL and FORM_MAPOPT */
    int *val;                           /* map index */
    int *crd;                           /* coordinates, y << 16 | x */
    int fix;                            /* fixed map index, only coordinates are changeable */
    int icon;                           /* one of ICON_* */
} ui_map_t;

typedef struct {                        /* param for FORM_COORD */
    ui_sprsel_t *spr;                   /* sprite to get coordinate from */
    int *w;                             /* width */
    int *h;                             /* height */
    int *x;                             /* output x */
    int *y;                             /* output y */
} ui_coord_t;

typedef struct {
    int w, h;                           /* mask width and height */
    char *data;                         /* mask data, w * h bytes */
} ui_mask_t;

typedef struct {                        /* param for FORM_COLL */
    ui_sprsel_t *spr;                   /* sprite to get collision mask for */
    int fld;                            /* selected field in widget */
    int evt;                            /* event mask on/off */
    int scrx, scry;                     /* scroll positions */
    int mw, mh;                         /* mask dimensions in pixels */
    int alpha;                          /* alpha */
    ui_mask_t mask;                     /* the collision mask */
} ui_coll_t;

typedef struct {                        /* param for FORM_CMDVAR */
    int type;                           /* if > 2 then it's a local variable */
    int attr;                           /* selected attribute */
    ui_select_t sel;                    /* variable selector */
} ui_cmdvar_t;

typedef struct {                        /* param for FORM_CMDPAL */
    int type;                           /* palette type, CMDPAL_* */
    int scr;                            /* scroll position */
    int num;                            /* number of elements */
} ui_cmdpal_t;

typedef struct {                        /* param for FORM_CMD */
    int scrx, scry;                     /* vertical and horizontal scrolls */
    int err;                            /* flag to indicate if script has errors */
    int clk, clkx, clky;                /* clicked field and position */
    cmd_t *clknode;                     /* clicked node */
    cmd_t root;                         /* script nodes */
} ui_cmd_t;

typedef struct {
    int len, alloc;                     /* real length and allocated length */
    uint8_t *data;                      /* bytecode data */
} ui_bc_t;

typedef struct {
    ssfn_t ctx;                         /* ssfn context */
    uint8_t *buf;                       /* the loaded file data */
} ui_font_t;

typedef struct {                        /* slideshow */
    int s, e;                           /* start end in hsec */
    int fi, fo;                         /* fade in, fade out in hsec */
    int img;                            /* slideshow image */
    int w, h;                           /* resized, cached image's size */
    uint32_t *data;                     /* image cache */
    uint32_t color;                     /* one fill color */
} ui_slide_t;

typedef struct {                        /* speech recordings */
    int s, e;                           /* start end in hsec */
    int spc;                            /* speech recording */
} ui_speech_t;

typedef struct {                        /* subtitles */
    int s, e;                           /* start end in hsec */
    int fi, fo;                         /* fade in, fade out in hsec */
    int x, y, a;                        /* subtitle position and alignment */
    uint32_t color;                     /* color */
    int style, size, font;              /* font */
    char txt[PROJ_TITLEMAX];            /* subtitle text */
} ui_sub_t;

typedef struct {                        /* param for FORM_CUTSCN */
    int scr;                            /* scroll position */
    int len;                            /* length in hundredth of seconds */
    int channel;                        /* selected channel */
    int sels, sele, seli;               /* selection start, end, selected index */
    int scrx, clkx, clk;                /* scroll and clicked button */
    int bglen[4];                       /* durations for background movies and music */
    char *smp;                          /* wave samples */
    int numimg;                         /* number of slideshows */
    ui_slide_t *img;                    /* slides */
    int numspc;                         /* number of speech recordings */
    ui_speech_t *spc;                   /* speech recordings */
    int numsub;                         /* number of subtitles */
    ui_sub_t *sub;                      /* subtitles */
    ui_cmd_t cmd;                       /* script commands */
} ui_cutscn_t;

/* exported variables from ui.c */
extern volatile uint32_t ui_anim_cnt;
extern uint32_t theme[], ui_input_bg, ui_cmd_color, *eegg, *scr_data, *palette_cpals;
extern SDL_Renderer *renderer;
extern SDL_Texture *screen, *icons, *arrows, *pos, *newsprite_sheet, *spr_select, *spr_none;
extern SDL_Window *window;
extern SDL_Event event;
extern SDL_Rect ui_clip, ui_popup_pos, tirnanog_icon, control1_icon, control2_icon, ccbyncsa_icon, portrait_icon;
extern SDL_Rect charwalk_icon, spawner_icon;
extern SDL_Cursor *cursors[5];
extern int scr_w, scr_h, scr_p, ui_menu, ui_tab, ui_over, ui_curx, ui_cury, ui_curbtn, ui_lastx, ui_lasty, ui_pressed;
extern int ui_popup_type, ui_popup_min, ui_popup_max, ui_popup_page, ui_popup_scr, ui_popup_val, ui_menu_langminw, ui_dnd_type;
extern int *ui_popup_flags, palette_sel, palette_num, palette_cie, *ui_sprite_ptr, *ui_sprite_clpbrd, ui_sprite_clpbrd_num;
extern int fileops_type;
extern void *ui_dnd_param;
extern ui_input_t ui_cmd_inp;
extern ui_select_t ui_cmd_sel;
extern ui_scroll_t ui_scroll;
extern ui_sprsel_t *ui_sprite_spr;
extern ui_input_t *ui_input_type;
extern char ui_isctrl, ui_isshift, ui_isalt, ui_ismeta, ui_input_buf[], ui_input_btn, **palette_list;
extern ui_sprite_t ui_spr_none;
extern ui_map_t *ui_map_map;
extern char *alerts_opts[];

/* ui.c - common user interface routines and special widgets */
void ui_gettheme(char *fn);
void ui_init();
void ui_quit();
void ui_input_start(ui_input_t *type, void (*onchange)(void *data));
void ui_switchtab(int tab);
void ui_onchange(int i);
void ui_events();
void ui_resize(int w, int h);
void ui_title(char *str);
void ui_status(int err, char *str);
void ui_progressbar(int step, int numstep, int64_t curr, int64_t total, int msg);
int  ui_modal(int icon, int question, char *param);
void ui_popup(int x, int y, int w, int h, int s);
void ui_dnd_drag(int type, void *param, int icon, uint32_t l, uint32_t b, uint32_t d);
void ui_dnd_drop();
void ui_error(int x, int y);
void ui_redraw(int start, int end);
void ui_render();

/* ui_wgt.c - user interface primitives and widgets */
extern void (*ui_table_btnup)(ui_table_t *table);
int ui_anim(int type, int nframe, int currframe);
void ui_fit(int mw, int mh, int sw, int sh, int *w, int *h);
int  ui_inaicon(int icon);
int  ui_textwidth(char *str);
void ui_text(SDL_Texture *dst, int x, int y, char *str);
void ui_text2(SDL_Texture *dst, int x, int y, char *str, uint32_t c, uint32_t s);
void ui_resample(uint32_t *dst, int x, int y, int w, int h, int p, int a, uint32_t *src, int sw, int sh, int sp);
void ui_combine(uint32_t *dst, int x, int y, int w, int h, int p, uint32_t *src, int sx, int sy, int sw, int sh, int sp);
void ui_combinewp(uint32_t *dst, int x, int y, int w, int h, int p, uint32_t *src, int sx, int sy, int sw, int sh, int sp,
    uint8_t *pal);
void ui_half(uint32_t *dst, int x, int y, int w, int h, int p, uint32_t *src);
void ui_circle(uint32_t *dst, int w, int h, int cx, int cy, int r, int a);
void ui_line(SDL_Texture *dst, int x0, int y0, int x1, int y1, uint32_t color);
void ui_triangle(SDL_Texture *dst, int x0, int y0, int x1, int y1, int x2, int y2, uint32_t color);
void ui_blit(SDL_Texture *dst, int x, int y, int w, int h, SDL_Texture *src, int sx, int sy, int sw, int sh);
void ui_blit2(SDL_Texture *dst, int x, int y, int w, int h, SDL_Texture *src, int sx, int sy, int sw, int sh);
void ui_blitbuf(SDL_Texture *dst, int x, int y, int w, int h, uint32_t *src, int sx, int sy, int sw, int sh, int sp);
void ui_icon(SDL_Texture *dst, int x, int y, int icon);
void ui_rect(SDL_Texture *dst, int x, int y, int w, int h, uint32_t l, uint32_t d);
void ui_box(SDL_Texture *dst, int x, int y, int w, int h, uint32_t l, uint32_t b, uint32_t d);
void ui_box2(SDL_Texture *dst, int x, int y, int w, int h, uint32_t l, uint32_t b, uint32_t d, int t, int s, int r, int c);
void ui_tri(SDL_Texture *dst, int x, int y, int up);
void ui_number(SDL_Texture *dst, int x, int y, char *s, uint32_t c);
void ui_fldgrp(SDL_Texture *dst, int x, int y, int w, int h, char *str);
void ui_vscrbar(SDL_Texture *dst, int x, int y, int w, int h, int scroll, int page, int num, int pressed);
void ui_hscrbar(SDL_Texture *dst, int x, int y, int w, int h, int scroll, int page, int num, int pressed);
void ui_bool(SDL_Texture *dst, int x, int y, int w, int state, char *s);
void ui_pal(SDL_Texture *dst, int x, int y, int num, uint32_t *pal, int pressed);
void ui_num(SDL_Texture *dst, int x, int y, int w, int num, int pressed);
void ui_time(SDL_Texture *dst, int x, int y, int w, int num, int max, int pressed);
void ui_fade(SDL_Texture *dst, int x, int y, int w, int h, ui_fade_t *fade, int pressed);
void ui_fade_set(int w, ui_fade_t *fade);
void ui_button(SDL_Texture *dst, int x, int y, int w, char *str, int icon, int pressed, int ts);
void ui_charbtn(SDL_Texture *dst, int x, int y, int unicode, int pressed);
void ui_iconbtn(SDL_Texture *dst, int x, int y, int icon, int pressed);
void ui_sprbtn(SDL_Texture *dst, int x, int y, uint32_t *icon, int pressed);
void ui_select(SDL_Texture *dst, int x, int y, int w, char *str, int pressed);
void ui_select_popup(SDL_Texture *dst, int x, int y, int w, int h, ui_select_t *sel);
void ui_input(SDL_Texture *dst, int x, int y, int w, char *str, int pressed);
void ui_clrver(SDL_Texture *dst, int x, int y, int val, uint32_t *pal, int p, int pressed, int ask);
void ui_color(SDL_Texture *dst, int x, int y, uint32_t color, int pressed);
void ui_color_picker(int x, int y, uint32_t *value);
int  ui_color_ctrl();
void ui_palette(int x, int y);
int  ui_palette_ctrl();
void ui_table_resize(ui_form_t *elem, int w, int h);
int  ui_table_click(ui_form_t *elem, int x, int y);
void ui_table(SDL_Texture *dst, int x, int y, int w, int h, ui_table_t *table, int pressed);
void ui_table_render(int x, int y, int w, int h, ui_table_t *table);
void ui_pathsel_getdrives(ui_pathsel_t *pathsel);
void ui_pathsel_add(ui_pathsel_t *pathsel, char *str);
void ui_pathsel_set(ui_pathsel_t *pathsel, char *str);
void ui_pathsel_get(ui_pathsel_t *pathsel, char *str);
int  ui_pathsel_click(ui_pathsel_t *pathsel, int x, int y);
int  ui_pathsel_release(ui_pathsel_t *pathsel, int x, int y);
void ui_pathsel(SDL_Texture *dst, int x, int y, int w, int h, ui_pathsel_t *pathsel, int pressed);
void ui_scrarea_set(ui_scrarea_t *s, int x, int y, int w, int h);
void ui_scrarea_win(ui_scrarea_t *s, SDL_Rect *src, SDL_Rect *dst);
void ui_scrarea_zoom(ui_scrarea_t *s, int z);
void ui_scrarea_reset(ui_scrarea_t *s, int w, int h);
void ui_scrarea_btn(ui_scrarea_t *s, int x, int y, int *dx, int *dy);
void ui_frames(SDL_Texture *dst, int x, int y, int w, int h, ui_frames_t *frm, int pressed);
int  ui_frames_click(ui_form_t *elem, int x, int y);
int  ui_frames_over(ui_form_t *elem, int x, int y);
void ui_sprite(SDL_Texture *dst, int x, int y, int w, int h, ui_sprsel_t *spr, int pressed);
void ui_sprite_free(int all);
void ui_sprite_picker(int x, int y, ui_sprsel_t *spr);
void ui_sprite_render();
int  ui_sprite_ctrl();
void ui_map_free();
void ui_map_picker(int x, int y, ui_map_t *spr);
void ui_map_render();
int  ui_map_ctrl();
void ui_coord_picker(int x, int y, ui_coord_t *crd);
void ui_coord_render();
int  ui_coord_ctrl();
void ui_coord_helper(ui_sprsel_t *back, int *x1, int *y1, int *x2, int *y2);
int  ui_coll_click(ui_form_t *elem, int x, int y);
void ui_coll_resize(ui_coll_t *coll, int w, int h);
void ui_coll(SDL_Texture *dst, int x, int y, int w, int h, ui_coll_t *coll, int pressed);
void ui_coll_render(int x, int y, int w, int h, ui_coll_t *coll);
void ui_font_load(ui_font_t *fnt, int style, int size, int family);
void ui_font_free(ui_font_t *fnt);
void ui_cutscn(SDL_Texture *dst, int x, int y, int w, int h, ui_cutscn_t *cutscn, int pressed);
void ui_cutscn_sel(SDL_Texture *dst, int x, int y, int w, ui_cutscn_t *cutscn);
void ui_cutscn_render(int x, int y, int w, int h, ui_cutscn_t *cutscn);
int  ui_cutscn_click(ui_cutscn_t *cutscn, int x, int y);
int  ui_cutscn_release(ui_cutscn_t *cutscn, int x, int y);
int  ui_cutscn_over(ui_cutscn_t *cutscn, int x, int y, int w);

/* ui_cmd.c - script commands */
extern char *ui_cmd_locals[];
void ui_cmdpal(SDL_Texture *dst, int x, int y, int h, ui_cmdpal_t *pal, int pressed);
int  ui_cmdpal_get(int type, int i, uint32_t *l, uint32_t *b, uint32_t *d);
void ui_cmdpal_drag(ui_form_t *elem, int x, int y);
void ui_cmdvar(SDL_Texture *dst, int x, int y, int w, ui_cmdvar_t *var, int pressed);
int  ui_cmdvar_click(ui_form_t *elem, int x, int y);
void ui_cmd(SDL_Texture *dst, int x, int y, int w, int h, int label, ui_cmd_t *cmd, int pressed);
void ui_cmd_status(int x, int y, ui_cmd_t *cmd);
void ui_cmd_dump(cmd_t *parent, cmd_t *node, int level);
void ui_cmd_free(ui_cmd_t *cmd);
void ui_cmd_resize(cmd_t *node);
int  ui_cmd_drag(ui_form_t *elem, int x, int y);
int  ui_cmd_drop(ui_form_t *elem, int x, int y);
void ui_cmd_dropfree();
int  ui_cmd_click(ui_form_t *elem, int x, int y);
int  ui_cmd_release(ui_form_t *elem);
void ui_cmd_wheel(ui_form_t *elem, int x, int y);
void ui_cmd_set(ui_cmd_t *cmd, int i);
int  ui_cmd_validexpr(char *str);
int  ui_cmd_add(cmd_t *node, int type, char *arg1, int arg2, int arg3, int arg4, int arg5, int err);
void ui_cmd_serialize(FILE *f, cmd_t *node, int lvl, int *e);
char *ui_cmd_deserialize(char *str, cmd_t *node, int *e);
int  ui_cmd_expr(tngctx_t *ctx, ui_bc_t *bc, char *data);
void ui_cmd_tobc(tngctx_t *ctx, ui_bc_t *bc, cmd_t *node, int lvl, int *e);
void ui_cmd_refexpr(tngctx_t *ctx, char *str);
void ui_cmd_ref(tngctx_t *ctx, cmd_t *node, int lvl);
void ui_cmd_exprbc(tngctx_t *ctx, ui_bc_t *bc, char *out);
void ui_cmd_frombc(tngctx_t *ctx, ui_bc_t *bc, int lvl, FILE *f);

/* spr.c - sprite manager */
void spr_init();
void spr_free();
void spr_cleanup();
void spr_freecache(int cat, int idx);
void spr_flush();
void spr_reset(int idx);
void spr_move(int idx, int oldcat, int newcat);
void spr_copy(int cat, int idx, char *newname);
void spr_add(int cat, char *name, int dir, int type, int nframe, int w, int h, int p, void *pixels);
void spr_del(int cat, int idx);
char *spr_getmeta(char *prefix, char *line, int *x, int *y, int *w, int *h, char *dir, char *type, char *nframe, char **name);
void spr_getname(char *fn, char *dir, char *type, char *nframe);
void spr_wrname(int cat, int val, int dir, char *d);
ui_sprite_t *spr_getsel(ui_sprsel_t *sel, int dir);
ui_sprite_t *spr_getidx(int cat, int idx, int dir, int none);
void spr_blitsel(ui_sprsel_t *spr, int dir, int a, SDL_Texture *dst, SDL_Rect *rect);
void spr_blit(ui_sprite_t *s, int a, SDL_Texture *dst, SDL_Rect *rect);
void spr_texture(ui_sprite_t *s, SDL_Texture **t);
void spr_render(ui_sprite_t *s, int frame, SDL_Texture *t, SDL_Rect *rect);
void spr_temp(int *sprout, int *sprin, int num, int cat, int stripe);
void spr_merge(ui_sprite_t *sprites, int *w, int *h, int *f, int *sprs, int dir, int one, int num, int cat, int sel, int stripe,
    int dy);
int  spr_totng(tngctx_t *ctx);
int  spr_fromtng(tngctx_t *ctx);
void spr_pack(char *name);
void spr_unpack(char *name);
void spr_listatls(char *name);

/* palette.c - palette functions and loaded palettes widget */
void     palette_rgb2hsv(uint32_t c, int *h, int *s, int *v);
uint32_t palette_hsv2rgb(int a, int h, int s, int v);
void     palette_rgb2lab(unsigned char *rgb, float *L, float *A, float *B);
uint32_t palette_lab2rgb(float L, float A, float B);
void     palette_load(char *fn, uint32_t *out, int mc);
int      palette_save(char *fn, int mc);
void     palette_del(int idx);
void     palette_init();
void     palette_free();
void     palette_show(SDL_Texture *dst, int x, int y, int pressed);

/* image.c */
int     image_save(FILE *f, int w, int h, int p, uint8_t *data);
uint8_t *image_mem(int w, int h, uint8_t *data, int *len, char *com);
uint8_t *image_load(char *fn, int *w, int *h);

/* about.c */
void about_resize(int tab, int w, int h);
void about_redraw(int tab);

/* newproj.c */
void newproj_exit(int tab);
void newproj_init(int tab);
void newproj_resize(int tab, int w, int h);
void newproj_redraw(int tab);

/* loadproj.c */
void loadproj_exit(int tab);
void loadproj_init(int tab);
void loadproj_resize(int tab, int w, int h);
void loadproj_redraw(int tab);

/* fileops.c */
void fileops_getdir(void *elem);
void fileops_exit(int tab);
void fileops_init(int tab);
void fileops_free();
void fileops_resize(int tab, int w, int h);
void fileops_redraw(int tab);
void fileops_load(void *data);

/* saveext.c */
void saveext_exit(int tab);
void saveext_init(int tab);
void saveext_resize(int tab, int w, int h);
void saveext_redraw(int tab);

/* license.c */
void license_exit(int tab);
void license_init(int tab);
void license_resize(int tab, int w, int h);
void license_redraw(int tab);
void license_generate(void *data);

/* fonts.c */
void fonts_exit(int tab);
void fonts_init(int tab);
void fonts_resize(int tab, int w, int h);
void fonts_redraw(int tab);
int  fonts_import(char *path);
int  fonts_totng(tngctx_t *ctx);
int  fonts_fromtng(tngctx_t *ctx);

/* sprites.c */
void sprites_exit(int tab);
void sprites_init(int tab);
void sprites_resize(int tab, int w, int h);
void sprites_redraw(int tab);
void sprites_render(int tab);

/* newsprite.c */
void newsprite_exit(int tab);
void newsprite_init(int tab);
void newsprite_free(void *data);
void newsprite_resize(int tab, int w, int h);
void newsprite_redraw(int tab);
void newsprite_render(int tab);
int  newsprite_ctrl(int tab);
void newsprite_chsel(void *param);
void newsprite_swaprb();
void newsprite_convpal();
void newsprite_empty();
ui_implist_t *newsprite_new(char *name, int type, int dir);
ui_implist_t *newsprite_find(char *name, int type, int dir);
int  newsprite_add(ui_implist_t *imp, int w, int h);
int  newsprite_loadsheet(char *fn);
/* newsprite3d.c */
int  newsprite_loadmodel(char *fn);
void newsprite_initlibs();
void newsprite_freelibs();

/* sprgen.c */
void sprgen_exit(int tab);
void sprgen_init(int tab);
void sprgen_resize(int tab, int w, int h);
void sprgen_redraw(int tab);
void sprgen_render(int tab);
int  sprgen_ctrl(int tab);

/* media.c */
void media_initlibs();
void media_freelibs();
void media_dur(int hsec, char *str);
void media_exit(int tab);
void media_init(int tab);
void media_resize(int tab, int w, int h);
void media_redraw(int tab);
int  media_import(char *path);
void media_play(char *path);
int  media_totng(tngctx_t *ctx);
int  media_fromtng(tngctx_t *ctx);

/* elements.c */
extern ui_num_t elements_pad[];
extern uint32_t elements_colors[];
void elements_exit(int tab);
void elements_init(int tab);
void elements_resize(int tab, int w, int h);
void elements_redraw(int tab);
void elements_load();
void elements_loadfonts();
void elements_freefonts();
void elements_winsizes(ui_tablehdr_t *hdr, int sel, int *w, int *top, int *right, int *bottom, int *left);
void elements_window(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, int h, ui_tablehdr_t *hdr, int sel);
void elements_dialog(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, int h, char *str, char *text, int pw);
int  elements_dlgopt(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, int h, char **opts, int no, int sel, int pw);
void elements_dlgsize(int dstw, int dsth, int nl, int no, int *x, int *y, int *w, int *h);
char *elements_dlgstr(char *str, int dstw, int pw, int fnt, int *nl);
void elements_button(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, char *str, int type);
int  elements_option(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, char **opts, int type, int sel, int idx);
int  elements_text(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, int h, char *str, int type);
void elements_slider(SDL_Texture *dst, int x, int y, int w, int pos);
int elements_vscrsize();
void elements_vscr(SDL_Texture *dst, int x, int y, int h, int r);
void elements_hscr(SDL_Texture *dst, int x, int y, int w);
int  elements_checkbox(SDL_Texture *dst, int x, int y, int chk);
int  elements_totng(tngctx_t *ctx);
int  elements_fromtng(tngctx_t *ctx);

/* mainmenu.c */
void mainmenu_exit(int tab);
void mainmenu_init(int tab);
void mainmenu_resize(int tab, int w, int h);
void mainmenu_redraw(int tab);
int  mainmenu_totng(tngctx_t *ctx);
int  mainmenu_fromtng(tngctx_t *ctx);

/* hud.c */
void hud_exit(int tab);
void hud_init(int tab);
void hud_resize(int tab, int w, int h);
void hud_redraw(int tab);
void hud_load();
void hud_ref(tngctx_t *ctx);
int  hud_totng(tngctx_t *ctx);
int  hud_fromtng(tngctx_t *ctx);

/* alerts.c */
void alerts_exit(int tab);
void alerts_init(int tab);
void alerts_resize(int tab, int w, int h);
void alerts_redraw(int tab);
void alerts_load();
void alerts_loadstr();
int  alerts_totng(tngctx_t *ctx);
int  alerts_fromtng(tngctx_t *ctx);

/* credits.c */
void credits_exit(int tab);
void credits_init(int tab);
void credits_resize(int tab, int w, int h);
void credits_redraw(int tab);
int  credits_totng(tngctx_t *ctx);
int  credits_fromtng(tngctx_t *ctx);

/* choosers.c */
void choosers_exit(int tab);
void choosers_init(int tab);
void choosers_resize(int tab, int w, int h);
void choosers_redraw(int tab);
void choosers_ref(tngctx_t *ctx, int idx);
int  choosers_totng(tngctx_t *ctx);
int  choosers_fromtng(tngctx_t *ctx);

/* cutscn.c */
void cutscn_free();
void cutscn_exit(int tab);
void cutscn_init(int tab);
void cutscn_resize(int tab, int w, int h);
void cutscn_redraw(int tab);
void cutscn_loadstr();
void cutscn_ref(tngctx_t *ctx, int idx);
int  cutscn_totng(tngctx_t *ctx);
int  cutscn_fromtng(tngctx_t *ctx);

/* startup.c */
void startup_exit(int tab);
void startup_init(int tab);
void startup_resize(int tab, int w, int h);
void startup_redraw(int tab);
void startup_ref(tngctx_t *ctx);
int  startup_totng(tngctx_t *ctx);
int  startup_fromtng(tngctx_t *ctx);

/* attrs.c */
extern ui_select_t attrs_defattr[];
void attrs_loadcfg();
void attrs_exit(int tab);
void attrs_init(int tab);
void attrs_resize(int tab, int w, int h);
void attrs_redraw(int tab);
void attrs_loadstr();
void attrs_ref(tngctx_t *ctx, int idx);
int  attrs_totng(tngctx_t *ctx);
int  attrs_fromtng(tngctx_t *ctx);

/* actions.c */
extern char actions_name[9][PROJ_NAMEMAX + 1];
void actions_load(int namesonly);
void actions_exit(int tab);
void actions_init(int tab);
void actions_resize(int tab, int w, int h);
void actions_redraw(int tab);
void actions_ref(tngctx_t *ctx);
int  actions_totng(tngctx_t *ctx);
int  actions_fromtng(tngctx_t *ctx);

/* chars.c */
void chars_free();
void chars_exit(int tab);
void chars_init(int tab);
void chars_resize(int tab, int w, int h);
void chars_redraw(int tab);
int  chars_loadopt(char *name);
char *chars_loadcfg(uint8_t *deltay);
void chars_loadstr();
void chars_sprites(int *sprs, int num, int cat, int sel);
void chars_ref(tngctx_t *ctx);
int  chars_totng(tngctx_t *ctx);
int  chars_fromtng(tngctx_t *ctx);

/* npcs.c */
void npcs_free();
void npcs_exit(int tab);
void npcs_init(int tab);
void npcs_resize(int tab, int w, int h);
void npcs_redraw(int tab);
int  npcs_loadnpc(int idx, int onlysprites);
int  npcs_attrget(int attr);
void npcs_loadstr();
ui_sprite_t *npcs_loadsprites();
void npcs_ref(tngctx_t *ctx, int idx);
int  npcs_totng(tngctx_t *ctx);
int  npcs_fromtng(tngctx_t *ctx);

/* spawners.c */
void spawners_exit(int tab);
void spawners_init(int tab);
void spawners_resize(int tab, int w, int h);
void spawners_redraw(int tab);
void spawners_ref(tngctx_t *ctx, int idx);
int  spawners_totng(tngctx_t *ctx);
int  spawners_fromtng(tngctx_t *ctx);

/* dialogs.c */
void dialogs_exit(int tab);
void dialogs_init(int tab);
void dialogs_resize(int tab, int w, int h);
void dialogs_redraw(int tab);
void dialogs_free();
void dialogs_loadstr();
void dialogs_display(SDL_Texture *dst, int w, int h, int dlg, Mix_Chunk **snd, SDL_Texture **portrait, SDL_Rect *dstp);
void dialogs_ref(tngctx_t *ctx, int idx);
int  dialogs_totng(tngctx_t *ctx);
int  dialogs_fromtng(tngctx_t *ctx);

/* objects.c */
void objects_exit(int tab);
void objects_init(int tab);
void objects_resize(int tab, int w, int h);
void objects_redraw(int tab);
void objects_loadstr();
int  objects_tospr(int idx);
void objects_ref(tngctx_t *ctx, int idx);
int  objects_totng(tngctx_t *ctx);
int  objects_fromtng(tngctx_t *ctx);

/* crafts.c */
void crafts_exit(int tab);
void crafts_init(int tab);
void crafts_resize(int tab, int w, int h);
void crafts_redraw(int tab);
void crafts_free();
void crafts_loadstr();
void crafts_ref(tngctx_t *ctx, int idx);
int  crafts_totng(tngctx_t *ctx);
int  crafts_fromtng(tngctx_t *ctx);

/* tiles.c */
void tiles_exit(int tab);
void tiles_init(int tab);
void tiles_resize(int tab, int w, int h);
void tiles_redraw(int tab);
int  tiles_totng(tngctx_t *ctx);
int  tiles_fromtng(tngctx_t *ctx);

/* maps.c */
void maps_sprite2tilewh(int sw, int sh, int *w, int *h);
void maps_size(int z, int *w, int *h);
void maps_map2scr(ui_scrarea_t *s, int sx, int sy, int *x, int *y);
void maps_scr2map(ui_scrarea_t *s, int sx, int sy, int *x, int *y);
void maps_npc2scr(int idx, int i, int j, int cx, int cy, int w, int h);
void maps_npccmd(int idx, int type, int par1, int par2);
int  maps_npcidle(int idx);
void maps_objplace(uint16_t *map, int idx, int x, int y);
void maps_objanim(int idx);
void maps_rendertmx(ui_scrarea_t *scr, int mask, int cx, int cy);
void maps_freetmx();
int  maps_loadtmx(char *fn, uint16_t *maps, int *neightbors, int onlymap);
void maps_loadnpc(int idx);
void maps_loadlights();
void maps_loadneightbors(uint16_t *maps, int *neightbors);
uint16_t *maps_loadscene(int idx, int tm);
void maps_rendermap(uint16_t *maps, int mask, int x, int y, int w, int h, int cx, int cy);
void maps_free();
void maps_exit(int tab);
void maps_init(int tab);
void maps_resize(int tab, int w, int h);
void maps_redraw(int tab);
void maps_render(int tab);
int  maps_ctrl(int tab);
void maps_loadstr();
void maps_ref(tngctx_t *ctx);
int  maps_totng(tngctx_t *ctx);
void maps_toassets(tngctx_t *ctx);
int  maps_fromtng(tngctx_t *ctx);

/* quests.c */
void quests_exit(int tab);
void quests_init(int tab);
void quests_resize(int tab, int w, int h);
void quests_redraw(int tab);
void quests_loadstr();
void quests_ref(tngctx_t *ctx, int idx);
int  quests_totng(tngctx_t *ctx);
int  quests_fromtng(tngctx_t *ctx);

/* translate.c */
void translate_set(char *code);
void translate_new();
void translate_exit(int tab);
void translate_init(int tab);
void translate_resize(int tab, int w, int h);
void translate_redraw(int tab);
void translate_getstrings();
void translate_freestrings();
void translate_addstr(char *str, int maxlen);
void translate_addstr2(char *str, int maxlen);
int  translate_totng(tngctx_t *ctx);
int  translate_fromtng(tngctx_t *ctx);
