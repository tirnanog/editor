/*
 * tnge/m3dobj.h
 *
 * Copyright (C) 2019 bzt
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief simple 3D model to M3D converter OBJ importer
 * https://gitlab.com/bztsrc/model3d
 *
 */

unsigned char *readfile(char *fn, unsigned int *size);
char *obj_fn = NULL;

/**
 * Parse material file
 */
void mtl_load(char *fn, m3d_t *m3d)
{
    unsigned int siz, i, j, k, l;
    int w, h;
    char *s, *e, *x, *data;
    float r,g,b,a;
    m3dm_t *m = NULL;

    data = (char*)readfile(fn, &siz);
    if(data) {
        if(verbose > 2) printf("  Material library '%s'\n", obj_fn);
        while(*data) {
            while(*data && (*data == ' ' || *data == '\t' || *data == '\r' || *data == '\n')) data++;
            if(!*data) break;
            /* comments */
            if(*data == '#') { data = _m3d_findnl(data); continue; }
            /* material */
            if(!memcmp(data, "newmtl", 6)) {
                m = NULL;
                data = _m3d_findarg(data + 6);
                if(!*data || *data == '\r' || *data == '\n') goto skip;
                s = _m3d_safestr(data, 0);
                if(!s || !*s) goto skip;
                for(j = 0, i = -1U; j < m3d->nummaterial; j++)
                    if(!strcmp(s, m3d->material[j].name)) { i = j; free(s); break; }
                if(i == -1U) {
                    i = m3d->nummaterial++;
                    m3d->material = (m3dm_t*)realloc(m3d->material, m3d->nummaterial * sizeof(m3dm_t));
                    if(!m3d->material) main_error(ERR_MEM);
                    m = &m3d->material[i];
                    m->name = s;
                    m->numprop = 0;
                    m->prop = NULL;
                } else
                    m = &m3d->material[i];
            }
            if(m) {
                k = i = 256;
                if(!memcmp(data, "map_", 4)) {
                    k = m3dpf_map;
                    data += 4;
                }
                for(j = 0; j < sizeof(m3d_propertytypes)/sizeof(m3d_propertytypes[0]); j++)
                    if(!memcmp(data, m3d_propertytypes[j].key, strlen(m3d_propertytypes[j].key))) {
                        i = m3d_propertytypes[j].id;
                        if(k != m3dpf_map) k = m3d_propertytypes[j].format;
                        break;
                    }
                if(i != 256 && k != 256) {
                    data = _m3d_findarg(data);
                    if(!*data || *data == '\r' || *data == '\n') goto skip;
                    i += (k == m3dpf_map && i < 128 ? 128 : 0);
                    for(l = 0, j = -1U; l < m->numprop; l++)
                        if(m->prop[l].type == i) { j = l; break; }
                    if(j == -1U) {
                        j = m->numprop++;
                        m->prop = (m3dp_t*)realloc(m->prop, m->numprop * sizeof(m3dp_t));
                        if(!m->prop) main_error(ERR_MEM);
                        m->prop[j].type = i;
                    }
                    switch(k) {
                        case m3dpf_color:
                            r = g = b = a = 0.0f;
                            data = _m3d_getfloat(data, &r);
                            data = _m3d_getfloat(data, &g);
                            data = _m3d_getfloat(data, &b);
                            if(*data && *data != '\r' && *data != '\n')
                                data = _m3d_getfloat(data, &a);
                            else
                                a = 1.0f;
                            m->prop[j].value.color = ((uint8_t)(a*255) << 24L) |
                                ((uint8_t)(b*255) << 16L) |
                                ((uint8_t)(g*255) <<  8L) |
                                ((uint8_t)(r*255) <<  0L);
                        break;
                        case m3dpf_uint8:
                        case m3dpf_uint16:
                        case m3dpf_uint32: data = _m3d_getint(data, &m->prop[j].value.num); break;
                        case m3dpf_float:  data = _m3d_getfloat(data, &m->prop[j].value.fnum); break;
                        case m3dpf_map:
                            s = e = _m3d_findnl(data);
                            while(e > data && (e[-1] == ' ' || e[-1] == '\t' || e[-1] == '\r' || e[-1] == '\n')) e--;
                            for(x = NULL; s > data && s[-1] != ' ' && s[-1] != '/' && s[-1] != '\\'; s--)
                                if(!x && *s == '.') { x = s; }
                            *e++ = 0;
                            data = e;
                            strcpy(obj_fn, s);
                            if(x) *x = 0;
                            s = _m3d_safestr(s, 0);
                            if(!s || !*s) goto skip;
                            for(i = 0, l = -1U; i < m3d->numtexture; i++) {
                                if(!strcmp(s, m3d->texture[i].name)) { l = i; free(s); break; }
                            }
                            if(l == -1U) {
                                l = m3d->numtexture++;
                                m3d->texture = (m3dtx_t*)realloc(m3d->texture, m3d->numtexture * sizeof(m3dtx_t));
                                if(!m3d->texture) main_error(ERR_MEM);
                                w = h = 0;
                                m3d->texture[l].name = s;
                                m3d->texture[l].d = image_load(fileops_path, &w, &h);
                                m3d->texture[l].w = w; m3d->texture[l].h = h; m3d->texture[l].f = 4;
                                if(m3d->texture[l].d) {
                                    if(verbose > 2) printf("  Loading texture '%s' OK\n", obj_fn);
                                } else {
                                    if(verbose) printf("mtl_load: unable to load texture '%s'\n", fileops_path);
                                }
                            }
                            m->prop[j].value.textureid = l;
                        break;
                    }
                }
            }
skip:       data = _m3d_findnl(data);
        }
    } else
    if(verbose)
        printf("mtl_load: unable to load material library '%s'\n", fileops_path);
}

/**
 * Load a model and convert it's structures into a Model 3D in-memory format
 */
m3d_t *obj_load(char *data)
{
    unsigned int i, j, numv = 0, numn = 0, nump = 0, numm = 0, numc, mi = -1U;
    unsigned int boneid = -1U, maxv = 0, maxn = 0, maxp = 0, maxuv = 0, maxl = 0;
    uint32_t *sp = NULL, *ctrl = NULL, *cidxs = NULL, *sidxs = NULL;
    int idx;
    char *s, *an, *bn;
    float r, g, b, a;
    float mi_x, ma_x, mi_y, ma_y, mi_z, ma_z, scale;
    float vmix = 1e10, vmax = -1e10, vmiy = 1e10, vmay = -1e10, vmiz = 1e10, vmaz = -1e10;
    float nmix = 1e10, nmax = -1e10, nmiy = 1e10, nmay = -1e10, nmiz = 1e10, nmaz = -1e10;
    float pmix = 1e10, pmax = -1e10, pmiy = 1e10, pmay = -1e10, pmiz = 1e10, pmaz = -1e10;
    m3d_t *m3d;
    m3dv_t *v, *vertex = NULL, *normal = NULL, *param = NULL;

    m3d = (m3d_t*)malloc(sizeof(m3d_t));
    if(!m3d) main_error(ERR_MEM);
    memset(m3d, 0, sizeof(m3d_t));
    m3d->flags = M3D_FLG_FREESTR;

    /* hackish, but we need the absolute path for loading material libraries and textures... */
    obj_fn = strrchr(fileops_path, SEP[0]);
    if(!obj_fn) obj_fn = fileops_path; else obj_fn++;

    /* add default position and orientation, may be needed by bones in group statements */
    m3d->numvertex = 2;
    m3d->vertex = (m3dv_t*)malloc(m3d->numvertex * sizeof(m3dv_t));
    if(!m3d->vertex) main_error(ERR_MEM);
    memset(m3d->vertex, 0, 2 * sizeof(m3dv_t));
    m3d->vertex[0].skinid = -1U;
    m3d->vertex[1].skinid = -2U;

    while(*data) {
        while(*data && (*data == ' ' || *data == '\t' || *data == '\r' || *data == '\n')) data++;
        if(!*data) break;

        /* comments */
        if(*data == '#') { data = _m3d_findnl(data); continue; }

        /* annotation label, not in the spec, but I needed it. This is the only statement I've added
         * "a (vertexid) (utf-8 string)"
         */
        if(data[0] == 'a' && data[1] == ' ') {
            data = _m3d_findarg(data + 1);
            if(!*data || *data == '\r' || *data == '\n') goto skip;
            data = _m3d_getint(data, (unsigned int*)&idx);
            if(idx > 0) idx--; else idx = numv - idx;
            data = _m3d_findarg(data);
            if(!*data || *data == '\r' || *data == '\n') goto skip;
            s = _m3d_safestr(data, 2);
            if(!s || !*s) goto skip;
            i = m3d->numlabel++;
            if(m3d->numlabel >= maxl) {
                maxl += 256;
                m3d->label = (m3dl_t*)realloc(m3d->label, maxl * sizeof(m3dl_t));
                if(!m3d->label) main_error(ERR_MEM);
            }
            m3d->label[i].name = NULL;
            m3d->label[i].lang = NULL;
            m3d->label[i].color = 0;
            m3d->label[i].vertexid = idx;
            m3d->label[i].text = s;
        } else

        /* object name and groups */
        if(data[1] == ' ' && (data[0] == 'o' || data[0] == 'g')) {
            s = data;
            data = _m3d_findarg(data + 1);
            if(!*data || *data == '\r' || *data == '\n') goto skip;
            /* there can be several "o " lines, but we only use the first as model name */
            if(s[0] == 'o' && !m3d->name) m3d->name = _m3d_safestr(data, 2);
            /* create a bone for each group */
            boneid = -1U;
            an = _m3d_safestr(data, 0);
            if(an) {
                for(i = 0; i < m3d->numbone; i++)
                    if(!strcmp(an, m3d->bone[i].name)) { boneid = i; break; }
                if(boneid == -1U) {
                    boneid = m3d->numbone++;
                    m3d->bone = (m3db_t*)realloc(m3d->bone, m3d->numbone * sizeof(m3db_t));
                    if(!m3d->bone) main_error(ERR_MEM);
                    memset(&m3d->bone[boneid], 0, sizeof(m3db_t));
                    m3d->bone[boneid].parent = (M3D_INDEX)-1U;
                    m3d->bone[boneid].name = an;
                    m3d->bone[boneid].pos = 0;
                    m3d->bone[boneid].ori = 1;
                } else
                    free(an);
            }
        } else

        /* material library */
        if(!memcmp(data, "mtllib", 6)) {
            data = _m3d_findarg(data + 6);
            if(!*data || *data == '\r' || *data == '\n') goto skip;
            s = bn = _m3d_findnl(data);
            while(bn > data && (bn[-1] == ' ' || bn[-1] == '\t' || bn[-1] == '\r' || bn[-1] == '\n')) bn--;
            while(s > data && s[-1] != '/' && s[-1] != '\\') s--;
            *bn++ = 0;
            data = bn;
            strcpy(obj_fn, s);
            mtl_load(fileops_path, m3d);
        } else

        /* use material */
        if(!memcmp(data, "usemtl", 6)) {
            data = _m3d_findarg(data);
            if(!*data) goto skip;
            mi = (M3D_INDEX)-1U;
            if(*data && *data != '\r' && *data != '\n') {
                s = _m3d_safestr(data, 0);
                if(!s || !*s) goto skip;
                for(i = 0; i < m3d->nummaterial; i++)
                    if(!strcmp(s, m3d->material[i].name)) {
                        mi = (M3D_INDEX)i;
                        break;
                    }
                free(s);
            }
        } else

        /* vertex lists */
        if(data[0] == 'v' && (data[1] == ' ' || (data[2] == ' ' && (data[1] == 'n' || data[1] == 'p')))) {
            s = data + 1;
            data = _m3d_findarg(data);
            if(!*data || *data == '\r' || *data == '\n') goto skip;
            v = NULL;
            switch(*s) {
                case 'n':
                    i = numn++;
                    if(numn >= maxn) {
                        maxn += 1024;
                        normal = (m3dv_t*)realloc(normal, maxn * sizeof(m3dv_t));
                        if(!normal) main_error(ERR_MEM);
                    }
                    v = &normal[i];
                break;
                case 'p':
                    i = nump++;
                    if(nump >= maxp) {
                        maxp += 1024;
                        param = (m3dv_t*)realloc(param, maxp * sizeof(m3dv_t));
                        if(!param) main_error(ERR_MEM);
                    }
                    v = &param[i];
                break;
                default:
                    i = numv++;
                    if(numv >= maxv) {
                        maxv += 1024;
                        vertex = (m3dv_t*)realloc(vertex, maxv * sizeof(m3dv_t));
                        if(!vertex) main_error(ERR_MEM);
                    }
                    v = &vertex[i];
                break;
            }
            if(!v) goto skip;
            memset(v, 0, sizeof(m3dv_t));
            v->skinid = (M3D_INDEX)-1U;
            v->w = (M3D_FLOAT)1.0;
            data = _m3d_getfloat(data, &v->x);
            if(*data && *data != '\r' && *data != '\n') {
                data = _m3d_getfloat(data, &v->y);
                if(*data && *data != '\r' && *data != '\n') {
                    data = _m3d_getfloat(data, &v->z);
                    if((*s == ' ' || *s == 'p') && *data && *data != '\r' && *data != '\n') {
                        data = _m3d_getfloat(data, &v->w);
                        if(*s == ' ' && *data && *data != '\r' && *data != '\n') {
                            r = g = b = a = 0.0f;
                            data = _m3d_getfloat(data, &r);
                            data = _m3d_getfloat(data, &g);
                            if(!*data || *data == '\r' || *data == '\n') {
                                a = 1.0f; b = g; g = r; r = v->w;
                                v->w = (M3D_FLOAT)1.0;
                            } else {
                                data = _m3d_getfloat(data, &b);
                                if(*data && *data != '\r' && *data != '\n')
                                    data = _m3d_getfloat(data, &a);
                                else
                                    a = 1.0f;
                            }
                            v->color = ((uint8_t)(a*255) << 24L) |
                                ((uint8_t)(b*255) << 16L) |
                                ((uint8_t)(g*255) <<  8L) |
                                ((uint8_t)(r*255) <<  0L);
                        }
                    }
                }
            }
            switch(*s) {
                case 'n':
                    if(v->x < nmix) nmix = v->x;
                    if(v->x > nmax) nmax = v->x;
                    if(v->y < nmiy) nmiy = v->y;
                    if(v->y > nmay) nmay = v->y;
                    if(v->z < nmiz) nmiz = v->z;
                    if(v->z > nmaz) nmaz = v->z;
                break;
                case 'p':
                    if(v->x < pmix) pmix = v->x;
                    if(v->x > pmax) pmax = v->x;
                    if(v->y < pmiy) pmiy = v->y;
                    if(v->y > pmay) pmay = v->y;
                    if(v->z < pmiz) pmiz = v->z;
                    if(v->z > pmaz) pmaz = v->z;
                break;
                default:
                    if(v->x < vmix) vmix = v->x;
                    if(v->x > vmax) vmax = v->x;
                    if(v->y < vmiy) vmiy = v->y;
                    if(v->y > vmay) vmay = v->y;
                    if(v->z < vmiz) vmiz = v->z;
                    if(v->z > vmaz) vmaz = v->z;
                break;
            }
        } else

        /* texture map lists */
        if(data[0] == 'v' && data[1] == 't' && data[2] == ' ') {
            data = _m3d_findarg(data + 2);
            if(!*data || *data == '\r' || *data == '\n') goto skip;
            i = m3d->numtmap++;
            if(m3d->numtmap >= maxuv) {
                maxuv += 1024;
                m3d->tmap = (m3dti_t*)realloc(m3d->tmap, maxuv * sizeof(m3dti_t));
                if(!m3d->tmap) main_error(ERR_MEM);
            }
            m3d->tmap[i].u = m3d->tmap[i].v = (M3D_FLOAT)0.0;
            data = _m3d_getfloat(data, &m3d->tmap[i].u);
            if(*data && *data != '\r' && *data != '\n')
                data = _m3d_getfloat(data, &m3d->tmap[i].v);
        } else

        /* mesh face */
        if(data[0] == 'f' && data[1] == ' ') {
            data = _m3d_findarg(data + 1);
            if(!*data || *data == '\r' || *data == '\n') goto skip;

            bn = _m3d_findnl(data);
            i = 3 * ((int)(bn - data) / 2) * sizeof(uint32_t);
            ctrl = (uint32_t*)realloc(ctrl, i);
            if(!ctrl) main_error(ERR_MEM);
            memset(ctrl, 255, i);
            /* get the polygon */
            for(numc = 0; *data && *data != '\r' && *data != '\n'; numc += 3) {
                data = _m3d_getint(data, (unsigned int*)&idx);
                if(idx > 0) idx--; else idx = numv - idx;
                ctrl[numc] = idx;
                if(*data == '/') {
                    data++;
                    if(*data != '/') {
                        data = _m3d_getint(data, (unsigned int*)&idx);
                        if(idx > 0) idx--; else idx = m3d->numtmap - idx;
                        ctrl[numc + 1] = idx;
                    }
                    if(*data == '/') {
                        data = _m3d_getint(data + 1, (unsigned int*)&idx);
                        if(idx > 0) idx--; else idx = numn - idx;
                        ctrl[numc + 2] = idx + numv;
                    }
                }
                data = _m3d_findarg(data);
            }
            if(numc < 9) goto skip;
            /* triangulate, assuming CCW */
            numm = (numc / 3) - 2;
            i = m3d->numface; m3d->numface += numm;
            m3d->face = (m3df_t*)realloc(m3d->face, m3d->numface * sizeof(m3df_t));
            if(!m3d->face) main_error(ERR_MEM);
            memset(&m3d->face[i], 255, sizeof(m3df_t)); /* set all index to -1 by default */
            for(j = 6; j < numc; j += 3, i++) {
                m3d->face[i].materialid = mi;
                m3d->face[i].vertex[0] = (M3D_INDEX)ctrl[0];
                m3d->face[i].texcoord[0] = (M3D_INDEX)ctrl[1];
                m3d->face[i].normal[0] = (M3D_INDEX)ctrl[2];
                m3d->face[i].vertex[1] = (M3D_INDEX)ctrl[j - 3];
                m3d->face[i].texcoord[1] = (M3D_INDEX)ctrl[j - 2];
                m3d->face[i].normal[1] = (M3D_INDEX)ctrl[j - 1];
                m3d->face[i].vertex[2] = (M3D_INDEX)ctrl[j + 0];
                m3d->face[i].texcoord[2] = (M3D_INDEX)ctrl[j + 1];
                m3d->face[i].normal[2] = (M3D_INDEX)ctrl[j + 2];
            }
        }
skip:   data = _m3d_findnl(data);
    }
    i = numv + numn + nump;
    if(i) {
        m3d->vertex = (m3dv_t*)realloc(m3d->vertex, (m3d->numvertex + i) * sizeof(m3dv_t));
        if(!m3d->vertex) main_error(ERR_MEM);
        if(numv) {
            mi_x = vmix < 0.0 ? -vmix : vmix;
            ma_x = vmax < 0.0 ? -vmax : vmax;
            mi_y = vmiy < 0.0 ? -vmiy : vmiy;
            ma_y = vmay < 0.0 ? -vmay : vmay;
            mi_z = vmiz < 0.0 ? -vmiz : vmiz;
            ma_z = vmaz < 0.0 ? -vmaz : vmaz;
            scale = mi_x;
            if(ma_x > scale) scale = ma_x;
            if(mi_y > scale) scale = mi_y;
            if(ma_y > scale) scale = ma_y;
            if(mi_z > scale) scale = mi_z;
            if(ma_z > scale) scale = ma_z;
            if(scale <= 0.0) scale = 1.0;
            for(j = 0; j < numv; j++) {
                vertex[j].x /= scale;
                vertex[j].y /= scale;
                vertex[j].z /= scale;
            }
            m3d->scale = scale;
            memcpy(&m3d->vertex[m3d->numvertex], vertex, numv * sizeof(m3dv_t));
        }
        if(numn) {
            mi_x = nmix < 0.0 ? -nmix : nmix;
            ma_x = nmax < 0.0 ? -nmax : nmax;
            mi_y = nmiy < 0.0 ? -nmiy : nmiy;
            ma_y = nmay < 0.0 ? -nmay : nmay;
            mi_z = nmiz < 0.0 ? -nmiz : nmiz;
            ma_z = nmaz < 0.0 ? -nmaz : nmaz;
            scale = mi_x;
            if(ma_x > scale) scale = ma_x;
            if(mi_y > scale) scale = mi_y;
            if(ma_y > scale) scale = ma_y;
            if(mi_z > scale) scale = mi_z;
            if(ma_z > scale) scale = ma_z;
            if(scale <= 0.0) scale = 1.0;
            for(j = 0; j < numn; j++) {
                normal[j].x /= scale;
                normal[j].y /= scale;
                normal[j].z /= scale;
            }
            memcpy(&m3d->vertex[m3d->numvertex + numv], normal, numn * sizeof(m3dv_t));
        }
        if(nump) {
            mi_x = pmix < 0.0 ? -pmix : pmix;
            ma_x = pmax < 0.0 ? -pmax : pmax;
            mi_y = pmiy < 0.0 ? -pmiy : pmiy;
            ma_y = pmay < 0.0 ? -pmay : pmay;
            mi_z = pmiz < 0.0 ? -pmiz : pmiz;
            ma_z = pmaz < 0.0 ? -pmaz : pmaz;
            scale = mi_x;
            if(ma_x > scale) scale = ma_x;
            if(mi_y > scale) scale = mi_y;
            if(ma_y > scale) scale = ma_y;
            if(mi_z > scale) scale = mi_z;
            if(ma_z > scale) scale = ma_z;
            if(scale <= 0.0) scale = 1.0;
            for(j = 0; j < nump; j++) {
                param[j].x /= scale;
                param[j].y /= scale;
                param[j].z /= scale;
            }
            memcpy(&m3d->vertex[m3d->numvertex + numv + numn], param, nump * sizeof(m3dv_t));
        }
        m3d->numvertex += i;
    }
    if(m3d->numtmap) {
        m3d->tmap = (m3dti_t*)realloc(m3d->tmap, m3d->numtmap * sizeof(m3dti_t));
        if(!m3d->tmap) main_error(ERR_MEM);
    }
    if(m3d->numlabel) {
        m3d->label = (m3dl_t*)realloc(m3d->label, m3d->numlabel * sizeof(m3dl_t));
        if(!m3d->label) main_error(ERR_MEM);
    }
    if(vertex) free(vertex);
    if(normal) free(normal);
    if(param) free(param);
    if(cidxs) free(cidxs);
    if(sidxs) free(sidxs);
    if(ctrl) free(ctrl);
    if(sp) free(sp);

    return m3d;
}
