/*
 * tnge/newsprite3d.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Rasterize a 3D model into a spritesheet import list
 *
 */

#include "main.h"
#include "zbuffer.h"
#include "SDL_loadso.h"

#define SUPPORT_ASSIMP

#define M3D_IMPLEMENTATION
#define M3D_ASCII
#define M3D_LOG(x) do{if(verbose>1)printf("  %s\n",x);}while(0)
#include "m3d.h"        /* default SDK */

extern char fileops_path[], newsprite_type, newsprite_dir;
extern ui_table_t newsprite_table;
extern int newsprite_gridw, newsprite_gridh;
extern ui_frames_t newsprite_frames;
extern ui_num_t newsprite_selstr, newsprite_selcnt;
char newsprite_gridA = 0;
int newsprite_assimp = 0, newsprite_gridf = 12;
ui_numptr_t newsprite_numf = { &newsprite_gridf, 1, 60, 1 };

float mindist = 0, maxdist = 10, distance = 0, pitch = 0, yaw = 0;
float light_position[4] = { 5, 5, 10, 0 }, light_color[4] = { 1, 1, 1, 1 }, shadow_color[4] = { 0, 0, 0, 0.5 };
unsigned int numaction = 0, animlen = 0, frame = -1U, lastframe = 0;
unsigned int numvbo = 0, texture[32];
m3dv_t *vbo = NULL;
#include "m3dobj.h"     /* reads Wavefront OBJ into m3d struct */
#ifdef SUPPORT_ASSIMP
#include "assimp.h"     /* for every other format, if we can dynlink assimp */
#endif

/**
 * Initialize model libs
 */
void newsprite_initlibs()
{
    newsprite_assimp = 0;
#ifdef SUPPORT_ASSIMP
    assimp.handle = SDL_LoadObject(ASSIMP);
    if(assimp.handle) {
        newsprite_assimp = 1;
        if(!(assimp.aiImportFile = SDL_LoadFunction(assimp.handle, "aiImportFile")) ||
           !(assimp.aiReleaseImport = SDL_LoadFunction(assimp.handle, "aiReleaseImport")) ||
           !(assimp.aiGetMaterialString = SDL_LoadFunction(assimp.handle, "aiGetMaterialString")) ||
           !(assimp.aiGetMaterialColor = SDL_LoadFunction(assimp.handle, "aiGetMaterialColor")) ||
           !(assimp.aiGetMaterialFloatArray = SDL_LoadFunction(assimp.handle, "aiGetMaterialFloatArray")) ||
           !(assimp.aiIdentityMatrix4 = SDL_LoadFunction(assimp.handle, "aiIdentityMatrix4")) ||
           !(assimp.aiMultiplyMatrix4 = SDL_LoadFunction(assimp.handle, "aiMultiplyMatrix4")) ||
           !(assimp.aiTransformVecByMatrix4 = SDL_LoadFunction(assimp.handle, "aiTransformVecByMatrix4")) ||
           !(assimp.aiTransformVecByMatrix3 = SDL_LoadFunction(assimp.handle, "aiTransformVecByMatrix3")))
            newsprite_freelibs();
    }
#endif
}

/**
 * Free model libs
 */
void newsprite_freelibs()
{
#ifdef SUPPORT_ASSIMP
    if(assimp.handle) {
        SDL_UnloadObject(assimp.handle); assimp.handle = NULL;
        newsprite_assimp = 0;
    }
#endif
}

/**
 * For debugging
 */
void newsprite_dump(int w, int h, int c, void *data)
{
    unsigned char tmp[18] = { 0 };
    FILE *f;

    f = fopen("img.tga", "wb");
    tmp[2] = 2;
    *((uint16_t*)(tmp + 10)) = *((uint16_t*)(tmp + 12)) = w;
    *((uint16_t*)(tmp + 14)) = h;
    tmp[16] = c * 8;
    tmp[17] = 40;
    fwrite(tmp, 18, 1, f);
    if(data)
        fwrite(data, w * c * h, 1, f);
    fclose(f);
}

/**
 * External asset reader for the readfile callback. Also used to load the model itself
 */
unsigned char *readfile(char *fn, unsigned int *size)
{
    FILE *f;
    unsigned char *ret = NULL, *buff = NULL;
    int i, j;

    *size = 0;
    f = project_fopen(fn, "rb");
    if(f) {
        fseek(f, 0L, SEEK_END);
        *size = (unsigned int)ftell(f);
        fseek(f, 0L, SEEK_SET);
        ret = (unsigned char*)malloc(*size + 1);
        if(ret) {
            i = (int)fread(ret, *size, 1, f);
            /* if it's gzip compressed, uncompress it first */
            if(ret[0] == 0x1f && ret[1] == 0x8b) {
                /* skip over gzip header */
                buff = ret + 3;
                i = *buff++; buff += 6;
                if(i & 4) { j = *buff++; j += (*buff++ << 8); buff += j; }
                if(i & 8) { while(*buff++ != 0); }
                if(i & 16) { while(*buff++ != 0); }
                if(i & 2) buff += 2;
                *size -= (int)(buff - ret);
                buff = (uint8_t*)_m3dstbi_zlib_decode_malloc_guesssize_headerflag((const char*)buff, *size, 4096, &i, 0);
                if(buff) { free(ret); ret = buff; buff = NULL; *size = (unsigned int)i; }
            }
            ret[*size] = 0;
        } else
            *size = 0;
        fclose(f);
    }
    return ret;
}

/**
 * Multiply a vertex with a transformation matrix
 */
void vec3_mul_mat4(m3dv_t *out, m3dv_t *v, float *mat)
{
    out->x = mat[ 0] * v->x + mat[ 1] * v->y + mat[ 2] * v->z + mat[ 3];
    out->y = mat[ 4] * v->x + mat[ 5] * v->y + mat[ 6] * v->z + mat[ 7];
    out->z = mat[ 8] * v->x + mat[ 9] * v->y + mat[10] * v->z + mat[11];
}

/**
 * Convert a standard uint32_t color into OpenGL color
 */
void set_color(uint32_t c, float *f) {
    if(!c) {
        f[0] = f[1] = f[2] = 0.0; f[3] = 1.0;
    } else {
        f[3] = ((float)((c >> 24)&0xff)) / 255;
        f[2] = ((float)((c >> 16)&0xff)) / 255;
        f[1] = ((float)((c >>  8)&0xff)) / 255;
        f[0] = ((float)((c >>  0)&0xff)) / 255;
    }
}

/**
 * Set material to use.
 */
void set_material(m3d_t *model, unsigned int mi)
{
    unsigned int i;
    float color[4];
    m3dm_t *m;

    /* reset GL material */
    glDisable(GL_TEXTURE_2D);
    glColor4f(0.5, 0.3, 0.1, 1);
    set_color(0, (float*)&color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (GLfloat*)&color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (GLfloat*)&color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, (GLfloat*)&color);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 0.0);
    if(mi == -1U || mi >= model->nummaterial) return;
    m = &model->material[mi];
    glEnd();
    /* update with what we have in this new material */
    for(i = 0; i < m->numprop; i++) {
        switch(m->prop[i].type) {
            case m3dp_Kd:
                set_color(m->prop[i].value.color, (float*)&color);
                glColor4fv((GLfloat*)&color);
            break;
            case m3dp_Ka:
                set_color(m->prop[i].value.color, (float*)&color);
                glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (GLfloat*)&color);
            break;
            case m3dp_Ks:
                set_color(m->prop[i].value.color, (float*)&color);
                glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (GLfloat*)&color);
            break;
            case m3dp_Ke:
                set_color(m->prop[i].value.color, (float*)&color);
                glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, (GLfloat*)&color);
            break;
            case m3dp_Ns:
                glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, m->prop[i].value.fnum);
            break;
            case m3dp_map_Kd:
                if(m->prop[i].value.textureid < (int)(sizeof(texture)/sizeof(texture[0])) &&
                  texture[m->prop[i].value.textureid] > 0) {
                    glEnable(GL_TEXTURE_2D);
                    glBindTexture(GL_TEXTURE_2D, texture[m->prop[i].value.textureid]);
                }
            break;
        }
    }
    glBegin(GL_TRIANGLES);
    return;
}

/**
 * Render the model
 */
void newsprite_renderframe(m3d_t *model, unsigned int actionid, unsigned int msec)
{
    unsigned int i, j, k, s, mi = -1U, c = 0, lc = 0;
    float fov, color[4] = { 0, 0, 0, 1 };
    m3dv_t *v, tmp1, tmp2;
    m3db_t *animpose;

    /* display model */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    fov = distance/4;
    glOrtho(-fov, fov, -fov, fov, mindist/5, maxdist*5);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0, 0, -distance);
    glRotatef(-pitch, 1, 0, 0);
    glRotatef(-yaw, 0, 1, 0);
    glTranslatef(0, 0, 0);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    /* draw the mesh */
#ifdef SUPPORT_ASSIMP
    if (g_scene)
        drawscene(g_scene, actionid, msec);
#endif
    if(model) {
        /* animate the mesh if we have a vertex buffer object */
        if(vbo) {
            /* get the animation-pose skeleton*/
            animpose = m3d_pose(model, actionid, msec);
            /* convert mesh vertices from bind-pose into animation-pose */
            for(i = 0; i < numvbo; i++) {
                s = vbo[i].skinid;
                if(s != -1U) {
                    vbo[i].x = vbo[i].y = vbo[i].z = 0;
                    for(j = 0; j < M3D_NUMBONE && model->skin[s].weight[j] > 0.0; j++) {
                        /* transfer from bind-pose model-space into bone-local space */
                        vec3_mul_mat4(&tmp1, &model->vertex[vbo[i].color], (float*)&model->bone[model->skin[s].boneid[j]].mat4);
                        /* transfer from bone-local space into animation-pose model-space */
                        vec3_mul_mat4(&tmp2, &tmp1, (float*)&animpose[ model->skin[s].boneid[j] ].mat4);
                        /* adjust with weight and accumulate */
                        vbo[i].x += tmp2.x * model->skin[s].weight[j];
                        vbo[i].y += tmp2.y * model->skin[s].weight[j];
                        vbo[i].z += tmp2.z * model->skin[s].weight[j];
                    }
                }
            }
            free(animpose);
        }
        set_material(model, -1U);
        glBegin(GL_TRIANGLES);
        for(i = k = 0; i < model->numface; i++) {
            /* if material changed */
            if(mi != model->face[i].materialid) {
                mi = model->face[i].materialid;
                set_material(model, mi);
                lc = 0;
            }
            /* we have triangles */
            for(j = 0; j < 3; j++, k += 2) {
                /* get normals and color */
                if(vbo) {
                    v = &vbo[k+1];
                    glNormal3f(v->x, v->y, v->z);
                    v = &vbo[k];
                    /* so save space, we've stored the original vertex id in vbo colors */
                    c = model->vertex[vbo[k].color].color;
                } else {
                    v = &model->vertex[model->face[i].normal[j]];
                    glNormal3f(v->x, v->y, v->z);
                    v = &model->vertex[model->face[i].vertex[j]];
                    c = v->color;
                }
                /* if there's no material and color changed */
                if(mi == -1U && c != lc) {
                    lc = c;
                    set_color(c, (float*)&color);
                    glColor3fv((GLfloat*)&color);
                }
                if(model->face[i].texcoord[j] != -1U) {
                    glTexCoord2f(model->tmap[model->face[i].texcoord[j]].u, 1.0 - model->tmap[model->face[i].texcoord[j]].v);
                }
                /* add the vertex */
                glVertex3f(v->x, v->y, v->z);
            }
        }
        glEnd();
    }
}

/**
 * Rasterize a 3D model into an import list
 */
int newsprite_loadmodel(char *fn)
{
    int ret = 0, rs, p;
    uint8_t *buff = NULL;
    uint32_t *dst;
    m3d_t *m3d = NULL;
    ui_implist_t *imp;
    unsigned char *data = NULL;
    unsigned int size, i, j, k, msec, dms;
    long int cur = 0, num = 0;
    char *ext = strrchr(fn, '.'), tmp[16];
    ZBuffer* frameBuffer = NULL;

    /* load model */
    data = readfile(fn, &size);
    if(!data || size < 1) { ui_status(1, lang[ERR_IMAGE]); return 0; }

    if(M3D_CHUNKMAGIC(data, '3','D','M','O') || M3D_CHUNKMAGIC(data, '3','d','m','o'))
        m3d = m3d_load(data, readfile, free, NULL);
    else
    if(ext && !strcmp(ext, ".obj"))
        m3d = obj_load((char*)data);
#ifdef SUPPORT_ASSIMP
    else
    if(assimp.handle) {
        ui_progressbar(0, 0, 0, 100, SPRITES_LOADMODEL);
        free(data); data = NULL;
        g_scene = (*assimp.aiImportFile)(fn, aiProcess_Triangulate | aiProcess_JoinIdenticalVertices |
            aiProcess_GenSmoothNormals | aiProcess_GenUVCoords | aiProcess_TransformUVCoords | aiProcess_RemoveComponent);
    }
#endif

    /* check if the load was successful */
#ifdef SUPPORT_ASSIMP
    if((!m3d && !g_scene) || (m3d && (!m3d->numvertex || !m3d->numface)) || (g_scene && !g_scene->mNumMeshes))
#else
    if(!m3d || !m3d->numvertex || !m3d->numface)
#endif
    {
        ui_status(1, lang[ERR_IMAGE]);
        goto cleanup;
    }

    /* clear spritesheet and empty import list */
    newsprite_free(NULL);
    newsprite_empty();

    if(newsprite_gridf < 1) newsprite_gridf = 1;
    if(newsprite_gridf > 120) newsprite_gridf = 120;
    newsprite_frames.size = newsprite_gridh = newsprite_gridw = newsprite_gridw & ~1;
    if(newsprite_frames.size < 16) newsprite_frames.size = 16;
    if(newsprite_frames.size > 256) newsprite_frames.size = 256;
    if(verbose) printf("newsprite_loadmodel: importing %s at %d x %d fps %d facing %d\n", fn, newsprite_frames.size,
        newsprite_frames.size, newsprite_gridf, newsprite_dir);

    /* create an off-screen visual */
    switch(project.type) {
        case TNG_TYPE_ORTHO: pitch = -45; yaw = 0; distance = 6.4; break;
        case TNG_TYPE_ISO:
        case TNG_TYPE_HEXV:
        case TNG_TYPE_HEXH: pitch = -30; yaw = 0/*45*/; distance = 6.4; break;
    }
    /* we render at double the size, and then rescale to half to give anti-aliasing around the contour */
    rs = newsprite_frames.size * 2;
    frameBuffer = ZB_open(rs, rs, ZB_MODE_RGBA, 0);
    glInit(frameBuffer);
    glClearColor(0, 0, 0, 0);
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_BLEND);
    glFrontFace(GL_CCW);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_color);
    glLightfv(GL_LIGHT0, GL_AMBIENT, shadow_color);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
    glViewport(0, 0, rs, rs);
    memset(texture, 0, sizeof(texture));

#ifdef SUPPORT_ASSIMP
    if(g_scene) {
        numaction = g_scene->mNumAnimations;
        initscene(g_scene);
    }
#endif
    if(m3d) {
        numaction = m3d->numaction;
        /* set up vbo array */
        if(m3d->numface && m3d->numbone && m3d->numskin && m3d->numaction) {
            numvbo = m3d->numface * 3 /* triangle */ * 2 /* vertex + normal */;
            vbo = (m3dv_t*)main_alloc(numvbo * sizeof(m3dv_t));
            /* create separate vertex records in vbo, because animation may change differently
             * for different triangles that share vertex coordinates */
            for(i = k = 0; i < m3d->numface; i++)
                for(j = 0; j < 3; j++, k += 2) {
                    memcpy(&vbo[k+0], &m3d->vertex[m3d->face[i].vertex[j]], sizeof(m3dv_t));
                    memcpy(&vbo[k+1], &m3d->vertex[m3d->face[i].normal[j]], sizeof(m3dv_t));
                    /* to avoid allocating extra memory, we store the original vertex id in vbo color */
                    vbo[k+0].color = m3d->face[i].vertex[j];
                    vbo[k+1].color = m3d->face[i].normal[j];
                    /* copy vertex skinid to normal, normals usually are skin neutral in models */
                    vbo[k+1].skinid = vbo[k+0].skinid;
                }
        }
        /* add textures from model */
        if(m3d->numtexture) {
            for(i = 0; i < (int)(sizeof(texture)/sizeof(texture[0])) && i < m3d->numtexture; i++) {
                if(!m3d->texture[i].w || !m3d->texture[i].h || !m3d->texture[i].d) {
                    if(verbose) printf("newsprite_loadmodel: no data for texture '%s'\n", m3d->texture[i].name);
                    continue;
                }
                glGenTextures(1, (GLuint*)&texture[i]);
                glBindTexture(GL_TEXTURE_2D, texture[i]);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
                glTexImage2D(GL_TEXTURE_2D, 0, m3d->texture[i].f, m3d->texture[i].w, m3d->texture[i].h, 0,
                    m3d->texture[i].f == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, m3d->texture[i].d);
            }
        }
    }
    /* we handle no more than 16 actions: idle, walk, swim, fly, block, hurt, die etc. plus user defined action1 to action8 */
    if(numaction > 16) numaction = 16;
    /* get the total number of frames we'll render */
    if(numaction > 0)
        for(num = 0, i = 0; i < numaction; i++) {
            animlen = 0;
            if(m3d && m3d->action) animlen = m3d->action[i].durationmsec;
#ifdef SUPPORT_ASSIMP
            if(g_scene) setanim(i);
#endif
            dms = 1000 / newsprite_gridf;
            if(numaction > 1 && animlen / dms > 31) dms = animlen / 31;
            j = animlen / dms + 1;
            num += j * 8;
        }
    /* failsafes */
    if(num < 8) {
        numaction = 0;
        num = 8;
    }
    if(rs * 4 * rs * num > 1024 * 1024 * 1024 && !ui_modal(ICON_WARN, SPRITES_TOOMUCHRAM, lang[SPRITES_CONTINUE]))
        goto cleanup;

    /* do not use main_alloc() here, that would quit on allocation error */
    buff = (uint8_t*)malloc(rs * 4 * rs * num);
    if(!buff) {
        if(verbose) printf("newsprite_loadmodel: unable to allocate memory for rendered frames\n");
        goto cleanup;
    }
    frameBuffer->maxy = 0; cur = 0;
    /* for every animation */
    for(i = 0; i < (numaction ? numaction : 1) && cur < num; i++) {
        /* for every direction */
        for(yaw = 0, j = 0; j < 8; yaw += 45, j++) {
            /* for every frame */
            animlen = 0;
            if(m3d && m3d->action) animlen = m3d->action[i].durationmsec;
#ifdef SUPPORT_ASSIMP
            if(g_scene) setanim(i);
#endif
            dms = 1000 / newsprite_gridf;
            if(numaction > 1 && animlen / dms > 31) dms = animlen / 31;
            for(msec = 0; msec <= animlen; msec += dms, cur++) {
                newsprite_renderframe(m3d, i, msec);
                memcpy(buff + rs * 4 * rs * cur, frameBuffer->pbuf, rs * 4 * rs);
                ui_progressbar(1, 2, cur, num, SPRITES_RENDER);
            }
        }
    }
    ui_progressbar(2, 2, 0, 100, SPRITES_GENERATE);
    /* move all sprites to the bottom of each frame */
    if(frameBuffer->maxy < rs - 1) {
        memmove(buff + (rs - 1 - frameBuffer->maxy) * rs * 4, buff, (rs * num - (rs - 1 - frameBuffer->maxy)) * rs * 4);
        memset(buff, 0, (rs - 1 - frameBuffer->maxy) * rs * 4);
    }
    /* If we have no actions (static model) or more actions then we can import.
     * But if we have exactly one, we need the user to split the animation into actions */
    if(numaction == 1 || (newsprite_gridA && numaction)) {
        newsprite_frames.data = (uint32_t*)malloc(newsprite_frames.size * 4 * newsprite_frames.size * num);
        if(!newsprite_frames.data) {
            free(buff);
            if(verbose) printf("newsprite_loadmodel: unable to allocate memory for sprites\n");
            goto cleanup;
        }
        memset(newsprite_frames.data, 0, newsprite_frames.size * 4 * newsprite_frames.size * num);
        newsprite_frames.num = num / 8;
        newsprite_frames.str = newsprite_frames.cnt = 1; newsprite_frames.scr = 0;
        newsprite_frames.dir = -newsprite_dir;
    }
    /* for every animation */
    for(i = 0, cur = 0; i < (numaction ? numaction : 1) && cur < num; i++) {
        sprintf(tmp, "anm%03u", i + 1);
        /* for every direction */
        for(j = 0; j < 8; j++) {
            /* for every frame */
            animlen = 0; imp = NULL;
            if(m3d && m3d->action) {
                animlen = m3d->action[i].durationmsec;
                if(numaction != 1 && !newsprite_gridA)
                    imp = newsprite_new(m3d->action[i].name && m3d->action[i].name[0] ? m3d->action[i].name : tmp, 2,
                        (j + newsprite_dir) & 7);
            }
#ifdef SUPPORT_ASSIMP
            if(g_scene) {
                setanim(i);
                if(numaction != 1 && !newsprite_gridA && curanim)
                    imp = newsprite_new(curanim->mName.data[0] ? curanim->mName.data : tmp, 2, (j + newsprite_dir) & 7);
            }
#endif
            if(!numaction && !imp) imp = newsprite_new("", 0, (j + newsprite_dir) & 7);
            dms = 1000 / newsprite_gridf;
            if(numaction > 1 && animlen / dms > 31) dms = animlen / 31;
            for(msec = 0; msec <= animlen; msec += dms, cur++) {
                if(imp && newsprite_add(imp, newsprite_frames.size, newsprite_frames.size) && imp->data) {
                    SDL_LockTexture(imp->data, NULL, (void**)&dst, &p);
                    ui_half(dst, (imp->nframe - 1) * newsprite_frames.size, 0, newsprite_frames.size, newsprite_frames.size, p,
                        (uint32_t*)(buff + rs * 4 * rs * cur));
                    SDL_UnlockTexture(imp->data);
                } else
                    ui_half(newsprite_frames.data, 0, newsprite_frames.size * cur, newsprite_frames.size, newsprite_frames.size,
                        newsprite_frames.size * 4, (uint32_t*)(buff + rs * 4 * rs * cur));
                ui_progressbar(2, 2, cur, num, SPRITES_GENERATE);
            }
        }
    }
    free(buff);
    newsprite_dir = newsprite_type = newsprite_gridA = 0;
    ret = 1;
cleanup:
    ui_progressbar(0, 0, 0, 0, SPRITES_GENERATE);
    glClose();
    if(frameBuffer) ZB_close(frameBuffer);
    if(m3d) m3d_free(m3d);
    if(data) free(data);
    if(vbo) { free(vbo); vbo = NULL; numvbo = 0; }
#ifdef SUPPORT_ASSIMP
    if(g_scene) freescene(&g_scene);
#endif
    if(!ret) {
        if(newsprite_frames.data) { free(newsprite_frames.data); newsprite_frames.data = NULL; }
        newsprite_frames.size = newsprite_frames.num = 0;
    }
    return ret;
}
