/*
 * tnge/about.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief About window
 *
 */

#include "main.h"

/**
 * The form
 */
ui_form_t about_form[] = {
    { FORM_TNGLOGO, 0, 58, 0, 0, 0, 0, site_url, NULL },
    { FORM_TITLE, 0, 128, 0, 0, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 180, 38*8, 16, 0, 0, "Copyright (C) 2022 bzt (bzt@codeberg)", NULL },
    { FORM_COMMENT, 0, 0, 200, 80, 0, 0, NULL, NULL },
    { FORM_COMMENT, 0, 0, 200, 80, 0, 0, NULL, NULL },
    { FORM_LICENSE, 0, 0, 0, 16, 0, 0, "GPLv3+", NULL },
    { FORM_TEXT, 0, 0, 0, 16, 0, 0, NULL, NULL },
    { FORM_LICENSE, 0, 0, 0, 16, 0, 0, "CC-by-nc-sa", NULL },
    { FORM_TEXT, 0, 0, 0, 16, 0, 0, NULL, NULL },
    { FORM_LAST }
};

/**
 * Resize the view
 */
void about_resize(int tab, int w, int h)
{
    int i, j;

    (void)tab;
    about_form[0].x = (w - tirnanog_icon.w) / 2;
    about_form[0].w = tirnanog_icon.w;
    about_form[0].h = tirnanog_icon.h;

    about_form[1].x = (w - ui_textwidth(lang[ABOUT_TNGE])) / 2;
    about_form[2].x = (w - about_form[2].w) / 2;

    about_form[3].param = lang[ABOUT_LICENSE1];
    about_form[4].param = lang[ABOUT_LICENSE2];
    i = ui_textwidth(about_form[3].param);
    j = ui_textwidth(about_form[4].param);
    if(j > i) i = j;
    about_form[3].w = about_form[4].w = i;
    about_form[3].x = about_form[4].x = (w - i) / 2;
    about_form[3].y = h - 64 - about_form[3].h - about_form[4].h;
    about_form[4].y = h - 64 - about_form[4].h;

    i = ui_textwidth(lang[ABOUT_GPL]);
    if(project.id[0] && project.author[0]) {
        j = ui_textwidth(lang[ABOUT_COMMERCIAL]);
        about_form[7].param = "*";
        about_form[8].param = lang[ABOUT_COMMERCIAL];
    } else {
        j = ui_textwidth(lang[ABOUT_CCBYNCSA]);
        about_form[8].param = lang[ABOUT_CCBYNCSA];
    }
    if(j > i) i = j;
    about_form[5].x = about_form[7].x = (w - i - 96) / 2;
    about_form[6].x = about_form[8].x = about_form[5].x + 96;

    about_form[5].y = about_form[6].y = about_form[2].y + 24;
    about_form[7].y = about_form[8].y = about_form[5].y + 16;
    about_form[6].param = lang[ABOUT_GPL];
}

/**
 * View layer
 */
void about_redraw(int tab)
{
    int i;

    (void)tab;
    ui_form = about_form;
    /* easter egg for those who know what this logo means... :-) */
    ssfn_dst.x = scr_w - 16; ssfn_dst.y = scr_h - 35; ssfn_dst.fg = theme[THEME_LIGHT]; ssfn_putc(9);
    ssfn_dst.x = scr_w - 18; ssfn_dst.y = scr_h - 37; ssfn_dst.fg = theme[THEME_DARK]; ssfn_putc(9);
    ssfn_dst.x = scr_w - 17; ssfn_dst.y = scr_h - 36; ssfn_dst.fg = theme[THEME_BG]; ssfn_putc(9);
    ssfn_dst.fg = theme[THEME_FG];
    /* ...and if you right-click on it */
    if(ui_curbtn > 1 && ui_curx > scr_w - 18 && ui_curx < scr_w - 2 && ui_cury > scr_h - 37 && ui_cury < scr_h - 21) {
        ui_resample(scr_data, (scr_w - 270) / 2, scr_h - 298, 270, 270, scr_p, 255, eegg, 256, 256, 1024);
        for(i = 3; about_form[i].type; i++) about_form[i].inactive = 2;
    } else {
        for(i = 3; about_form[i].type; i++) about_form[i].inactive = 0;
    }
}
