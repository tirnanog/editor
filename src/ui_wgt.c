/*
 * tnge/ui.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief User interface widgets
 *
 */

#include "main.h"

#ifdef __WIN32__
/* only include these when necessary, it conflicts with SDL_mixer... */
#include <windows.h>
#endif

#define CW (26+256+26)          /* color popup width */
#define CH (32+256+8)           /* color popup height */
#define PH (256+8)              /* palette popup height */
#define SI 66                   /* sprite popup icon size + 2*/
#define SW (SI*8+4+2+8)         /* sprite popup width */
#define MW (4+200+4+512+12+4)   /* map popup width */
#define MH (32+256+12+4)        /* map popup height */

/*
 * NOTE: these must work on any destination texture. However locking can be VERY slow, so we handle
 * the main screen texture specially, only locking and unlocking once per refresh in ui_redraw().
 * This means the widgets must not lock / unlock the destination texture if dst == screen.
 */

/* global sdl structs and variables required by widgets */
extern SDL_Texture *popup, *dnd;
extern char ws[0x110000], *ui_input_cur, *ui_input_scr, *ui_input_end, newsprite_dir;

/* variables for tables */
void (*ui_table_btnup)(ui_table_t *table) = NULL;

/* variables for color picker */
static char ui_color_hex[9];
static ui_input_t ui_color_str = { INP_HEX, sizeof(ui_color_hex), ui_color_hex };
static uint32_t *ui_color_value, ui_color_tmp, ui_color_last;
static int ui_color_fld = 0, ui_color_btn = 0, hue = 0, sat = 0, val = 0;

/* variables for sprite picker */
static char ui_sprite_srch[FILENAME_MAX];
static ui_input_t ui_sprite_str = { INP_NAME, sizeof(ui_sprite_srch), ui_sprite_srch };
int *ui_sprite_ptr = NULL, *ui_sprite_clpbrd, ui_sprite_clpbrd_num = 0;
ui_sprsel_t *ui_sprite_spr = NULL;
SDL_Texture *ui_sprite_anims[9] = { 0 };

/* variables for map coordinate picker */
static char ui_map_srch[PROJ_NAMEMAX];
static ui_input_t ui_map_str = { INP_ID, sizeof(ui_map_srch), ui_map_srch };
ui_map_t *ui_map_map = NULL;
ui_scrarea_t ui_map_scr;

/* variables for coordinate picker */
static int ui_coord_w, ui_coord_h, *ui_coord_x = NULL, *ui_coord_y = NULL;
ui_sprite_t *ui_coord_spr = NULL;

/******************** UI primitives ********************/

/**
 * Calculate the current frame index
 */
int ui_anim(int type, int nframe, int currframe)
{
    int ret = 0;

    if(nframe < 2) return 0;
    if(currframe < 0) currframe = ui_anim_cnt;
    switch(type) {
        case 0:
            ret = currframe % (nframe + 8);
            if(ret >= nframe) ret = nframe - 1;
        break;
        case 1:
            ret = currframe % (2 * nframe - 2);
            if(ret >= nframe) ret = 2 * nframe - ret - 2;
        break;
        case 2:
            ret = currframe % nframe;
        break;
    }
    return ret;
}

/**
 * Recalculate size (sw, sh) to fit into (mw, mh) keeping aspect ratio
 */
void ui_fit(int mw, int mh, int sw, int sh, int *w, int *h)
{
    if(!w || !h) return;
    if(mw < 1 || mh < 1 || sw < 1 || sh < 1) { *w = *h = 0; return; }
    *w = mw; *h = sh * mw / sw;
    if(*h > mh) { *h = mh; *w = sw * mh / sh; }
    if(*w < 1) *w = 1;
    if(*h < 1) *h = 1;
}

/**
 * Return inactive version of icon
 */
int ui_inaicon(int icon)
{
    /* admitedly, I could have organized the icons and a simple shift would be enough */
    switch(icon) {
        case ICON_LOAD:   icon = ICON_INLOAD;  break;
        case ICON_SAVE:   icon = ICON_INSAVE;  break;
        case ICON_KEYS:   icon = ICON_INKEYS;  break;
        case ICON_IMG:    icon = ICON_INIMG;   break;
        case ICON_MUS:    icon = ICON_INMUS;   break;
        case ICON_BACKGR: icon = ICON_INBG;    break;
        case ICON_ERASE:  icon = ICON_INERAS;  break;
        case ICON_PLAY:   icon = ICON_INPLAY;  break;
        case ICON_SWRB:   icon = ICON_INSWRB;  break;
        case ICON_REMOVE: icon = ICON_INREMOV; break;
        case ICON_HIST:   icon = ICON_INHIST;  break;
        case ICON_PVIEW:  icon = ICON_INPVW;   break;
        case ICON_SCRIPT: icon = ICON_INSCR;   break;
        case ICON_CUT:    icon = ICON_INCUT;   break;
        case ICON_COPY:   icon = ICON_INCOPY;  break;
        case ICON_PASTE:  icon = ICON_INPASTE; break;
        case ICON_MOV:    icon = ICON_INMOV;   break;
        case ICON_ADD:    icon = ICON_INADD;   break;
        case ICON_PAL:    icon = ICON_INPAL;   break;
        case ICON_LAYERS: icon = ICON_INLYRS;  break;
        case ICON_MERGE:  icon = ICON_INMERGE; break;
        case ICON_MOVE:   icon = ICON_INMOVE;  break;
        case ICON_ACTION: icon = ICON_INACT;   break;
        case ICON_LINK:   icon = ICON_INLNK;   break;
        default: icon = -1; break;
    }
    return icon;
}

/**
 * Return text width in pixels
 */
int ui_textwidth(char *str)
{
    int ret = 0, i = 0, u;

    if(!str || !*str) return 0;
    while(*str) {
        u = ssfn_utf8(&str);
        if(u == '\n') { if(ret < i) ret = i; i = 0; }
        else i += (int)ws[u];
    }
    if(ret < i) ret = i;
    return ret;
}

/**
 * Display text
 */
void ui_text(SDL_Texture *dst, int x, int y, char *str)
{
    char *s = str;
    int p, u;

    ssfn_dst.w = ui_clip.x + ui_clip.w;
    ssfn_dst.h = ui_clip.y + ui_clip.h;
    if(y + 16 <= ui_clip.y || x > ssfn_dst.w || y > ssfn_dst.h) return;
    if(dst == screen) { ssfn_dst.ptr = (unsigned char *)scr_data; p = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&ssfn_dst.ptr, &p);
    ssfn_dst.p = p;
    ssfn_dst.x = x;
    ssfn_dst.y = y;
    while(s && *s) {
        u = ssfn_utf8(&s);
        if(u == '\n') { ssfn_dst.x = x; ssfn_dst.y += 16; } else
        if(ssfn_dst.x >= ui_clip.x && ssfn_dst.x < ssfn_dst.w)
            ssfn_putc(u);
        else
            ssfn_dst.x += ws[u];
    }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Display text with shadow
 */
void ui_text2(SDL_Texture *dst, int x, int y, char *str, uint32_t c, uint32_t s)
{
    uint32_t f = ssfn_dst.fg, b = ssfn_dst.bg;
    ssfn_dst.fg = s;
    ui_text(dst, x + 1, y + 1, str);
    ssfn_dst.bg = 0;
    ssfn_dst.fg = c;
    ui_text(dst, x, y, str);
    ssfn_dst.bg = b;
    ssfn_dst.fg = f;
}

/**
 * Resample a raw image data to a different size and blit to position
 */
#define RESAMPLE_MAXSIZE 4096
static int ui_sax[RESAMPLE_MAXSIZE + 1], ui_say[RESAMPLE_MAXSIZE + 1];
void ui_resample(uint32_t *dst, int x, int y, int w, int h, int p, int a, uint32_t *src, int sw, int sh, int sp)
{
    uint8_t *c, b[4];
    uint32_t *c00, *c01, *c10, *c11, *csp, *dp, *ep;
    int o = y * p + x * 4, sx, sy, *csax, *csay, csx, csy, ex, ey, t1, t2, sstep, lx, ly, l;
    int ix1 = ui_clip.x - x, ix2 = ui_clip.x + ui_clip.w - x, iy = ui_clip.y - y;

    if(!dst || !src || sw < 1 || sh < 1 || w > RESAMPLE_MAXSIZE || h > RESAMPLE_MAXSIZE) return;
    if(w < 2) w = 2;
    if(h < 2) h = 2;
    if(w == sw && h == sh && a == 255) {
        ui_combine(dst, x, y, w, h, p, src, 0, 0, sw, sh, sp);
        return;
    }
    sx = (int) (65536.0 * (float)sw / (float)w);
    sy = (int) (65536.0 * (float)sh / (float)h);
    if(y + h > ui_clip.y + ui_clip.h) h = ui_clip.y + ui_clip.h - y;
    if(h < 1) return;
    sp /= 4;
    csp = src; ep = src + sp * sh - 1; dp = (uint32_t*)((uint8_t*)dst + o);
    csx = 0; csax = ui_sax; for(x = 0; x <= w; x++) { *csax = csx; csax++; csx &= 0xffff; csx += sx; }
    csy = 0; csay = ui_say; for(y = 0; y <= h; y++) { *csay = csy; csay++; csy &= 0xffff; csy += sy; }
    csay = ui_say; ly = 0;
    for(y = l = 0; y < h; y++) {
        c00 = csp; c01 = csp; c01++; c10 = csp; c10 += sp; c11 = c10; c11++;
        csax = ui_sax; lx = 0;
        for(x = 0; x < w - 1; x++) {
            if(c00 > ep) c00 = ep;
            if(c01 > ep) c01 = ep;
            if(c10 > ep) c10 = ep;
            if(c11 > ep) c11 = ep;
            if(y >= iy && x >= ix1 && x < ix2) {
                ex = (*csax & 0xffff); ey = (*csay & 0xffff);
                t1 = (((((*c01 >> 24) - (*c00 >> 24)) * ex) >> 16) + (*c00 >> 24)) & 0xff;
                t2 = (((((*c11 >> 24) - (*c10 >> 24)) * ex) >> 16) + (*c10 >> 24)) & 0xff;
                b[3] = ((((t2 - t1) * ey) >> 16) + t1); if(b[3] == 254) b[3] = 255;
                t1 = (((((*c01 & 0xff) - (*c00 & 0xff)) * ex) >> 16) + (*c00 & 0xff)) & 0xff;
                t2 = (((((*c11 & 0xff) - (*c10 & 0xff)) * ex) >> 16) + (*c10 & 0xff)) & 0xff;
                b[0] = ((((t2 - t1) * ey) >> 16) + t1);
                t1 = ((((((*c01 >> 8) & 0xff) - ((*c00 >> 8) & 0xff)) * ex) >> 16) + ((*c00 >> 8) & 0xff)) & 0xff;
                t2 = ((((((*c11 >> 8) & 0xff) - ((*c10 >> 8) & 0xff)) * ex) >> 16) + ((*c10 >> 8) & 0xff)) & 0xff;
                b[1] = ((((t2 - t1) * ey) >> 16) + t1);
                t1 = ((((((*c01 >> 16) & 0xff) - ((*c00 >> 16) & 0xff)) * ex) >> 16) + ((*c00 >> 16) & 0xff)) & 0xff;
                t2 = ((((((*c11 >> 16) & 0xff) - ((*c10 >> 16) & 0xff)) * ex) >> 16) + ((*c10 >> 16) & 0xff)) & 0xff;
                b[2] = ((((t2 - t1) * ey) >> 16) + t1);
                c = (uint8_t*)dp;
                if(!p || !c[3]) {
                    *dp = *((uint32_t*)b);
                } else {
                    b[3] = b[3] * (a & 0xff) / 255;
                    if(a < 256) c[3] = (b[3]*b[3] + (256 - b[3])*c[3]) >> 8; else c[3] = 0xff;
                    c[2] = (b[2]*b[3] + (256 - b[3])*c[2]) >> 8;
                    c[1] = (b[1]*b[3] + (256 - b[3])*c[1]) >> 8;
                    c[0] = (b[0]*b[3] + (256 - b[3])*c[0]) >> 8;
                }
            }
            dp++; csax++;
            if(*csax > 0) {
                sstep = (*csax >> 16); lx += sstep;
                if(lx < sw) { c00 += sstep; c01 += sstep; c10 += sstep; c11 += sstep; }
            }
        }
        csay++;
        if(*csay > 0) { sstep = (*csay >> 16); ly += sstep; if(ly < sh) csp += (sstep * sp); }
        o += p ? p : w * 4;
        dp = (uint32_t*)((uint8_t*)dst + o);
    }
}

/**
 * Combine a pixel buffer to another pixel buffer
 */
void ui_combine(uint32_t *dst, int x, int y, int w, int h, int p, uint32_t *src, int sx, int sy, int sw, int sh, int sp)
{
    int i = ui_clip.x + ui_clip.w, j = ui_clip.y + ui_clip.h;
    uint8_t *s = (uint8_t*)src, *d = (uint8_t*)dst, *a, *b;

    if(x < ui_clip.x) { w -= ui_clip.x - x; sx += ui_clip.x - x; x = ui_clip.x; }
    if(y < ui_clip.y) { h -= ui_clip.y - y; sy += ui_clip.y - y; y = ui_clip.y; }
    if(x + w >= i) w = i - x - 1;
    if(y + h >= j) h = j - y - 1;
    if(x >= i || y >= j || w < 1 || h < 1 || !src || !dst) return;

    s += sp * sy + sx * 4; d += p * y + x * 4;
    for(j = 0; j < h; j++, d += p) {
        for(i = 0, a = d; i < w; i++, a += 4) {
            b = s + (j % sh) * sp + (i % sw) * 4;
            if(!a[3]) {
                a[3] = b[3];
                a[2] = b[2];
                a[1] = b[1];
                a[0] = b[0];
            } else
            if(b[3]) {
                a[3] = (b[3]*b[3] + (256 - b[3])*a[3]) >> 8;
                a[2] = (b[2]*b[3] + (256 - b[3])*a[2]) >> 8;
                a[1] = (b[1]*b[3] + (256 - b[3])*a[1]) >> 8;
                a[0] = (b[0]*b[3] + (256 - b[3])*a[0]) >> 8;
            }
        }
    }
}

/**
 * Combine a pixel buffer to another pixel buffer with optional palette
 */
void ui_combinewp(uint32_t *dst, int x, int y, int w, int h, int p, uint32_t *src, int sx, int sy, int sw, int sh, int sp,
    uint8_t *pal)
{
    int i, j;
    uint8_t *s = (uint8_t*)src, *d = (uint8_t*)dst, *a, *b, *c;

    if(w < 1 || h < 1 || !src || !dst) return;

    s += sp * sy + sx * 4; d += p * y + x * 4;
    for(j = 0; j < h; j++, d += p) {
        for(i = 0, a = d; i < w; i++, a += 4) {
            b = c = s + (j % sh) * sp + (i % sw) * 4;
            if(pal && b[0] == b[1] && b[0] == b[2] && !(b[0] & 0x0F) && (b[0] & 0x10) && pal[3] && (pal[0] || pal[1] || pal[2]))
                c = pal + (7 - (b[0] >> 5)) * 4;
            if(!a[3]) {
                a[3] = b[3];
                a[2] = c[2];
                a[1] = c[1];
                a[0] = c[0];
            } else
            if(b[3]) {
                a[3] = (b[3]*b[3] + (256 - b[3])*a[3]) >> 8;
                a[2] = (c[2]*b[3] + (256 - b[3])*a[2]) >> 8;
                a[1] = (c[1]*b[3] + (256 - b[3])*a[1]) >> 8;
                a[0] = (c[0]*b[3] + (256 - b[3])*a[0]) >> 8;
            }
        }
    }
}

/**
 * Optimized resize to half, also swaps red and blue channels. src expected to be w * 2 x h * 2 in size
 */
void ui_half(uint32_t *dst, int x, int y, int w, int h, int p, uint32_t *src)
{
    uint8_t *s = (uint8_t*)src, *e = (uint8_t*)(dst + y * w + x), *d;
    int X, Y, p2 = w * 8;

    if(!dst || !src) return;
    for(Y = 0; Y < h; Y++, s += p2, e += p)
        for(X = 0, d = e; X < w; X++, d += 4, s += 8) {
            d[2] = (s[0] + s[4] + s[p2 + 0] + s[p2 + 4]) >> 2;
            d[1] = (s[1] + s[5] + s[p2 + 1] + s[p2 + 5]) >> 2;
            d[0] = (s[2] + s[6] + s[p2 + 2] + s[p2 + 6]) >> 2;
            d[3] = (s[3] + s[7] + s[p2 + 3] + s[p2 + 7]) >> 2;
        }
}

/**
 * Draw a filled circle to the alpha channel
 */
void ui_circle(uint32_t *dst, int w, int h, int cx, int cy, int r, int a)
{
    int i, j, k, x, y, X, Y, err, r2 = r / 2;
    uint8_t *ptr = (uint8_t*)dst;

    if(!dst || w < 1 || h < 1 || r < 2 || cx < 0 || cx >= w || cy < 0 || cy >= h || a < 1) return;
    while(r > 0) {
        x = -r; y = 0; err = 2 - 2 * r;
        k = (r < r2 ? 0 : (r - r2) * a / r2);
        do {
            X = cx - x;
            if(X >= 0 && X < w) {
                Y = cy + y;
                if(Y >= 0 && Y < h) {
                    j = ((Y * w + X) << 2) + 3;
                    if(k < ptr[j]) ptr[j] = k;
                    if(k < ptr[j + 4]) ptr[j + 4] = k;
                }
                Y = cy - y;
                if(Y >= 0 && Y < h) {
                    j = ((Y * w + X) << 2) + 3;
                    if(k < ptr[j]) ptr[j] = k;
                    if(k < ptr[j + 4]) ptr[j + 4] = k;
                }
            }
            X = cx + x;
            if(X >= 0 && X < w) {
                Y = cy - y;
                if(Y >= 0 && Y < h) {
                    j = ((Y * w + X) << 2) + 3;
                    if(k < ptr[j]) ptr[j] = k;
                    if(k < ptr[j - 4]) ptr[j - 4] = k;
                }
                Y = cy + y;
                if(Y >= 0 && Y < h) {
                    j = ((Y * w + X) << 2) + 3;
                    if(k < ptr[j]) ptr[j] = k;
                    if(k < ptr[j - 4]) ptr[j - 4] = k;
                }
            }
            i = err;
            if(i <= y) err += ++y * 2 + 1;
            if(i > x || err > y) err += ++x * 2 + 1;
        } while(x <= 0);
        r--;
    }
}

/**
 * Draw a line
 */
void ui_line(SDL_Texture *dst, int x0, int y0, int x1, int y1, uint32_t color)
{
    int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
    int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
    int err = dx+dy, e2, p;
    uint32_t *d;

    if(dst == screen) { d = (uint32_t*)scr_data; p = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&d, &p);
    for (;;){
        d[y0 * p/4 + x0] = color;
        e2 = 2*err;
        if (e2 >= dy) { if (x0 == x1) { break; } err += dy; x0 += sx; }
        if (e2 <= dx) { if (y0 == y1) { break; } err += dx; y0 += sy; }
    }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Draw a filled rectangle
 */
void ui_triangle(SDL_Texture *dst, int x0, int y0, int x1, int y1, int x2, int y2, uint32_t color)
{
    int i, j, t, h, s, xa, ya, xb, yb, p;
    float A, B;
    uint8_t *a, *b = (uint8_t*)&color, *d;

    if(dst == screen) { d = (uint8_t*)scr_data; p = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&d, &p);

    if((y0 == y1 && y0 == y2) || (x0 == x1 && x0 == x2)) return;
    if(y0 > y1) { i = x0; x0 = x1; x1 = i; i = y0; y0 = y1; y1 = i; }
    if(y0 > y2) { i = x0; x0 = x2; x2 = i; i = y0; y0 = y2; y2 = i; }
    if(y1 > y2) { i = x1; x1 = x2; x2 = i; i = y1; y1 = y2; y2 = i; }
    t = y2 - y0;
    for(i = 0; i < t; i++) {
        h = i > y1 - y0 || y1 == y0; s = h ? y2 - y1 : y1 - y0;
        A = (float)i / (float)t; B = ((float)i - (float)(h ? y1 - y0 : 0)) / (float)s;
        xa = (1.0 - A) * x0 + x2 * A; ya = (1.0 - A) * y0 + y2 * A;
        xb = h ? (1.0 - B) * x1 + x2 * B : (1.0 - B) * x0 + x1 * B;
        yb = h ? (1.0 - B) * y1 + y2 * B : (1.0 - B) * y0 + y1 * B;
        if(xa > xb) { j = xa; xa = xb; xb = j; j = ya; ya = yb; yb = j; }
        for(j = xa; j <= xb; j++) {
            a = &d[(y0 + i) * p + j * 4];
            if(!a[3]) memcpy(a, b, 4);
            else {
                a[3] = b[3];
                a[2] = (b[2]*b[3] + (256 - b[3])*a[2]) >> 8;
                a[1] = (b[1]*b[3] + (256 - b[3])*a[1]) >> 8;
                a[0] = (b[0]*b[3] + (256 - b[3])*a[0]) >> 8;
            }
        }
    }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Blit a texture to the screen texture
 * ehh, rendercopy does not work on streaming textures, and target textures do not allow locking...
 */
void ui_blit(SDL_Texture *dst, int x, int y, int w, int h, SDL_Texture *src, int sx, int sy, int sw, int sh)
{
    int i = ui_clip.x + ui_clip.w, j = ui_clip.y + ui_clip.h, p1, p2;
    uint8_t *s, *d, *a, *b;

    if(x < ui_clip.x) { w -= ui_clip.x - x; sx += ui_clip.x - x; x = ui_clip.x; }
    if(y < ui_clip.y) { h -= ui_clip.y - y; sy += ui_clip.y - y; y = ui_clip.y; }
    if(x + w >= i) w = i - x - 1;
    if(y + h >= j) h = j - y - 1;
    if(x >= i || y >= j || w < 1 || h < 1 || !src || !dst) return;

    SDL_LockTexture(src, NULL, (void**)&s, &p1);
    if(dst == screen) { d = (uint8_t*)scr_data; p2 = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&d, &p2);
    s += p1 * sy + sx * 4; d += p2 * y + x * 4;
    for(j = 0; j < h; j++, d += p2) {
        for(i = 0, a = d; i < w; i++, a += 4) {
            b = s + (j % sh) * p1 + (i % sw) * 4;
            if(dst != popup && dst != dnd) a[3] = b[3];
            a[2] = (b[2]*b[3] + (256 - b[3])*a[2]) >> 8;
            a[1] = (b[1]*b[3] + (256 - b[3])*a[1]) >> 8;
            a[0] = (b[0]*b[3] + (256 - b[3])*a[0]) >> 8;
        }
    }
    SDL_UnlockTexture(src);
    if(dst != screen) SDL_UnlockTexture(dst);
}
void ui_blit2(SDL_Texture *dst, int x, int y, int w, int h, SDL_Texture *src, int sx, int sy, int sw, int sh)
{
    int i = ui_clip.x + ui_clip.w, j = ui_clip.y + ui_clip.h, p1, p2;
    uint8_t *s, *d, *a, *b;

    if(x < ui_clip.x) { w -= ui_clip.x - x; sx += ui_clip.x - x; x = ui_clip.x; }
    if(y < ui_clip.y) { h -= ui_clip.y - y; sy += ui_clip.y - y; y = ui_clip.y; }
    if(x + w >= i) w = i - x - 1;
    if(y + h >= j) h = j - y - 1;
    if(x >= i || y >= j || w < 1 || h < 1 || !src || !dst) return;

    SDL_LockTexture(src, NULL, (void**)&s, &p1);
    if(dst == screen) { d = (uint8_t*)scr_data; p2 = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&d, &p2);
    s += p1 * sy + sx * 4; d += p2 * y + x * 4;
    for(j = 0; j < h; j++, d += p2) {
        for(i = 0, a = d; i < w; i++, a += 4) {
            b = s + (j % sh) * p1 + (i % sw) * 4;
            a[2] = (b[2]*b[3] + (256 - b[3])*a[2]) >> 8;
            a[1] = (b[1]*b[3] + (256 - b[3])*a[1]) >> 8;
            a[0] = (b[0]*b[3] + (256 - b[3])*a[0]) >> 8;
        }
    }
    SDL_UnlockTexture(src);
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Blit a pixel buffer to the screen texture
 */
void ui_blitbuf(SDL_Texture *dst, int x, int y, int w, int h, uint32_t *src, int sx, int sy, int sw, int sh, int sp)
{
    int i = ui_clip.x + ui_clip.w, j = ui_clip.y + ui_clip.h, p;
    uint8_t *s = (uint8_t*)src, *d, *a, *b;

    if(x < ui_clip.x) { w -= ui_clip.x - x; sx += ui_clip.x - x; x = ui_clip.x; }
    if(y < ui_clip.y) { h -= ui_clip.y - y; sy += ui_clip.y - y; y = ui_clip.y; }
    if(x + w >= i) w = i - x - 1;
    if(y + h >= j) h = j - y - 1;
    if(x >= i || y >= j || w < 1 || h < 1 || !src || !dst) return;

    if(dst == screen) { d = (uint8_t*)scr_data; p = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&d, &p);
    s += sp * sy + sx * 4; d += p * y + x * 4;
    for(j = 0; j < h; j++, d += p) {
        for(i = 0, a = d; i < w; i++, a += 4) {
            b = s + (j % sh) * sp + (i % sw) * 4;
            a[3] = b[3];
            a[2] = (b[2]*b[3] + (256 - b[3])*a[2]) >> 8;
            a[1] = (b[1]*b[3] + (256 - b[3])*a[1]) >> 8;
            a[0] = (b[0]*b[3] + (256 - b[3])*a[0]) >> 8;
        }
    }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Display an icon
 * <  0 skip
 * >= 0 normal icon
 * >  ICON_NUMACT an action icon with number
 */
void ui_icon(SDL_Texture *dst, int x, int y, int icon)
{
    char tmp[2];

    if(icon >= 0 && icon < ICON_NUMACT /*ICON_LAST + (int)(sizeof(ui_lngicn)/sizeof(ui_lngicn[0]))*/)
        ui_blit(dst, x, y, 16, 16, icons, (icon & 7) << 4, (icon & ~7) << 1, 16, 16);
    if(icon >= ICON_NUMACT) {
        ui_blit(dst, x, y, 16, 16, icons, (ICON_ACTION & 7) << 4, (ICON_ACTION & ~7) << 1, 16, 16);
        tmp[0] = icon - ICON_NUMACT + '0'; tmp[1] = 0;
        ui_number(dst, x + 12, y + 11, tmp, theme[THEME_FG]);
    }
}

/**
 * Display a rectangle
 */
void ui_rect(SDL_Texture *dst, int x, int y, int w, int h, uint32_t l, uint32_t d)
{
    int i, j, x2 = ui_clip.x + ui_clip.w, y2 = ui_clip.y + ui_clip.h, p, p2, pitch;
    uint32_t *data;

    if(x >= x2 || x + w < ui_clip.x || y >= y2 || y + h < ui_clip.y || w < 1 || h < 1 || !dst) return;

    if(dst == screen) { data = scr_data; pitch = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&data, &pitch);
    p = y * pitch/4 + x;
    p2 = (y + h - 1) * pitch/4 + x;
    for(i=0; i + 1 < w && x + i + 1 < x2; i++) {
        if(y >= ui_clip.y && x + i >= ui_clip.x) data[p + i] = l;
        if(y + h + 1 >= ui_clip.y && y + h - 1 < y2 && x + i >= ui_clip.x) data[p2 + i + 1] = d;
    }
    p += pitch/4;
    for(j=1; j + 1 < h && y + j < y2; j++, p += pitch/4)
        if(y + j >= ui_clip.y) {
            if(x >= ui_clip.x) data[p] = l;
            if(x + w - 1 < x2) data[p + w - 1] = d;
        }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Display a filled box
 */
void ui_box(SDL_Texture *dst, int x, int y, int w, int h, uint32_t l, uint32_t b, uint32_t d)
{
    int i, j, x2 = ui_clip.x + ui_clip.w, y2 = ui_clip.y + ui_clip.h, p, p2, pitch;
    uint32_t *data;

    if(x >= x2 || x + w < ui_clip.x || y >= y2 || y < ui_clip.y || w < 1 || h < 1 || !dst) return;

    if(dst == screen) { data = scr_data; pitch = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&data, &pitch);
    p = y * pitch/4 + x;
    p2 = (y + h - 1) * pitch/4 + x;
    for(i=0; i + 1 < w && x + i + 1 < x2; i++) {
        if(y >= ui_clip.y && x + i >= ui_clip.x) data[p + i] = l;
        if(y + h + 1 >= ui_clip.y && y + h - 1 < y2 && x + i >= ui_clip.x) data[p2 + i + 1] = d;
    }
    p += pitch/4;
    for(j=1; j + 1 < h && y + j < y2; j++, p += pitch/4)
        if(y + j >= ui_clip.y) {
            if(x >= ui_clip.x) data[p] = l;
            if(x + w - 1 < x2) data[p + w - 1] = d;
            for(i = 1; i + 1 < w && x + i + 1 < x2; i++)
                if(x + i >= ui_clip.x) data[p + i] = b;
        }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Display a filled box with altering background
 * t - pixels to skip at top (header)
 * s - scroll position
 * r - size of a row in pixels
 * c - size of a coloumn in pixels or zero if there are only rows
 */
void ui_box2(SDL_Texture *dst, int x, int y, int w, int h, uint32_t l, uint32_t b, uint32_t d, int t, int s, int r, int c)
{
    int i, j, k, x2 = ui_clip.x + ui_clip.w, y2 = ui_clip.y + ui_clip.h, p, p2, pitch;
    uint32_t *data, B, a;

    if(x >= x2 || x + w < ui_clip.x || y >= y2 || y < ui_clip.y || w < 1 || h < 1 || !dst) return;

    if(dst == screen) { data = scr_data; pitch = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&data, &pitch);
    p = y * pitch/4 + x;
    p2 = (y + h - 1) * pitch/4 + x;
    for(i=0; i + 1 < w && x + i + 1 < x2; i++) {
        if(y >= ui_clip.y && x + i >= ui_clip.x) data[p + i] = l;
        if(y + h + 1 >= ui_clip.y && y + h - 1 < y2 && x + i >= ui_clip.x) data[p2 + i + 1] = d;
    }
    p += pitch/4; p2 = p;
    for(j=1; j + 1 < h && y + j < y2; j++, p += pitch/4)
        if(y + j >= ui_clip.y) {
            if(x >= ui_clip.x) data[p] = l;
            if(x + w - 1 < x2) data[p + w - 1] = d;
        }
    y += t; p = p2 + t * pitch/4; s &= 1;
    B = (b & 0xFF000000) | ((b + 0x040404) & 0x7F7F7F);
    if(c) {
        for(j=0; j + t + 2 < h && y + j < y2; j++, p += pitch/4)
            for(i=1; i + 1 < w && x + i < x2; ) {
                a = s ^ ((j / r) & 1) ^ ((i / c) & 1) ? B : b;
                for(k = 0; k < c && i + 1 < w && x + i < x2; i++, k++) data[p + i] = a;
            }
    } else {
        for(j=0; j + t + 2 < h && y + j < y2; j++, p += pitch/4) {
            a = s ^ ((j / r) & 1) ? B : b;
            for(i=1; i + 1 < w && x + i < x2; i++) data[p + i] = a;
        }
    }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Display a triangle
 */
void ui_tri(SDL_Texture *dst, int x, int y, int up)
{
    int i, j, p;
    uint32_t *data, l, d, b;

    if(x < ui_clip.x || y < ui_clip.y || x + 7 >= ui_clip.x + ui_clip.w || y + 7 >= ui_clip.y + ui_clip.h) return;

    if(dst == screen) { data = scr_data; p = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&data, &p);
    if(ui_input_bg == theme[THEME_INPBG]) {
        l = theme[THEME_LIGHTER];
        d = theme[THEME_DARKER];
        b = theme[THEME_BG];
    } else {
        l = (ui_input_bg + 0x101010) & 0x7F7F7F;
        d = (ui_input_bg - 0x101010) & 0x7F7F7F;
        b = ui_input_bg;
    }
    p /= 4; j = p * y + x;
    if(up) {
        data[j+3] = b;
        j += p;
        data[j+2] = d;
        data[j+3] = b;
        data[j+4] = l;
        j += p;
        data[j+1] = d;
        data[j+2] = b;
        data[j+3] = b;
        data[j+4] = b;
        data[j+5] = l;
        j += p;
        data[j] = d;
        data[j+1] = b;
        data[j+2] = b;
        data[j+3] = b;
        data[j+4] = b;
        data[j+5] = b;
        data[j+6] = l;
        j += p;
        for(i = 0; i < 7; i++)
            data[j + i] = l;
    } else {
        for(i = 0; i < 7; i++)
            data[j + i] = d;
        j += p;
        data[j] = d;
        data[j+1] = b;
        data[j+2] = b;
        data[j+3] = b;
        data[j+4] = b;
        data[j+5] = b;
        data[j+6] = l;
        j += p;
        data[j+1] = d;
        data[j+2] = b;
        data[j+3] = b;
        data[j+4] = b;
        data[j+5] = l;
        j += p;
        data[j+2] = d;
        data[j+3] = b;
        data[j+4] = l;
        j += p;
        data[j+3] = l;
    }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Display a number string with extra small glyphs (4x5)
 */
void ui_number(SDL_Texture *dst, int x, int y, char *s, uint32_t c)
{
    int i, j, k, l, d = 0, p, pitch, x2 = ui_clip.x + ui_clip.w, y2 = ui_clip.y + ui_clip.h;
    uint32_t *data, n[] = {0x4aaa4,0x4c444,0xc248e,0xc2c2c,0x24ae2,0xe8c2c,0x68eac,0xe2444,0x4a4a4,0xea62c,0x04040,0x0004};

    if(x < ui_clip.x || y + 5 < ui_clip.y || x >= x2 || y >= y2) return;

    if(dst == screen) { data = scr_data; pitch = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&data, &pitch);
    pitch /= 4; p = pitch * y;
    for(; *s && x < x2; s++, x += 4) {
        d = *s == '.' ? 11 : (*s == ':' ? 10 : *s - '0');
        for(k = 1<<19, j = l = 0; j < 5 && y + j < y2; j++, l += pitch)
            for(i = 0; i < 4 && x + i < x2; i++, k >>= 1)
                if((n[d] & k) && x + i >= ui_clip.x) data[p + l + x + i] = c;
    }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/******************** complex widgets ********************/

/**
 * Field group
 */
void ui_fldgrp(SDL_Texture *dst, int x, int y, int w, int h, char *str)
{
    int l, p;
    uint32_t c;

    ui_rect(dst, x, y, w, h, theme[THEME_DARKER], theme[THEME_LIGHTER]);
    ui_rect(dst, x + 1, y + 1, w - 2, h - 2, theme[THEME_LIGHTER], theme[THEME_DARKER]);
    p = ui_clip.w; c = ssfn_dst.bg; ssfn_dst.bg = theme[THEME_BG];
    if(w) ui_clip.w = x + w - ui_clip.x - 10;
    l = ui_textwidth(str) + 4;
    if(x + 8 + l > ui_clip.x + ui_clip.w) l = ui_clip.x + ui_clip.w - x - 8;
    ui_box(dst, x + 8, y - 1, l, 4, theme[THEME_BG], theme[THEME_BG], theme[THEME_BG]);
    ui_text2(dst, x + 10, y - 10, str, theme[THEME_DARKER], theme[THEME_LIGHTER]);
    ui_clip.w = p; ssfn_dst.bg = c;
}

/**
 * Vertical scrollbar
 */
void ui_vscrbar(SDL_Texture *dst, int x, int y, int w, int h, int scroll, int page, int num, int pressed)
{
    if(!num || page > num) {
        ui_box(dst, x, y, w, h, theme[pressed ? THEME_DARK : THEME_LIGHT], theme[THEME_BG],
            theme[pressed ? THEME_LIGHT : THEME_DARK]);
        return;
    }
    if(scroll + page > num) scroll = num - page;
    if(scroll < 0) scroll = 0;
    ui_box(dst, x, y, w, h, theme[THEME_DARKER], theme[THEME_DARKER], theme[THEME_DARKER]);
    ui_box(dst, x, y + (h - 20) * scroll / num, w, 20 + (h - 20) * page / num,
        theme[pressed ? THEME_DARK : THEME_LIGHT], theme[THEME_BG], theme[pressed ? THEME_LIGHT : THEME_DARK]);
}

/**
 * Horizontal scrollbar
 */
void ui_hscrbar(SDL_Texture *dst, int x, int y, int w, int h, int scroll, int page, int num, int pressed)
{
    if(!num || page > num) {
        ui_box(dst, x, y, w, h, theme[pressed ? THEME_DARK : THEME_LIGHT], theme[THEME_BG],
            theme[pressed ? THEME_LIGHT : THEME_DARK]);
        return;
    }
    if(scroll + page > num) scroll = num - page;
    if(scroll < 0) scroll = 0;
    ui_box(dst, x, y, w, h, theme[THEME_DARKER], theme[THEME_DARKER], theme[THEME_DARKER]);
    ui_box(dst, x + (w - 20) * scroll / num, y, 20 + (w - 20) * page / num, h,
        theme[pressed ? THEME_DARK : THEME_LIGHT], theme[THEME_BG], theme[pressed ? THEME_LIGHT : THEME_DARK]);
}

/**
 * Display a checkbox
 */
void ui_bool(SDL_Texture *dst, int x, int y, int w, int state, char *s)
{
    int i, p;
    uint32_t *data, f = ssfn_dst.fg, l, d;

    if(state == -1) ssfn_dst.fg = theme[THEME_INA];
    if(s && *s && w > 16) ui_text(dst, x + 16, y, s);
    ssfn_dst.fg = f;
    l = 0xFF000000 | ((ui_input_bg + 0x101010) & 0x7F7F7F);
    d = 0xFF000000 | ((ui_input_bg - 0x101010) & 0x7F7F7F);
    if(state == -1)
        l = d = theme[THEME_INA];
    if(state <= -2)
        ui_rect(dst, x, y + 4, 12, 12, theme[THEME_FG], theme[THEME_FG]);
    else
        ui_box(dst, x, y + 4, 12, 12, d, state == -1 ? theme[THEME_INA] : ui_input_bg, l);
    if(state == -1 || x < ui_clip.x || y < ui_clip.y || x + 8 >= ui_clip.x + ui_clip.w || y + 16 >= ui_clip.y + ui_clip.h)
        return;
    if(dst == screen) { data = scr_data; p = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&data, &p);
    p /= 4;
    if(state > 0 || state == -3) {
        for(i = 0; i < 6; i++) {
            data[(y + 7 + i) * p + x + 8 - i] = theme[THEME_FG];
            data[(y + 7 + i) * p + x + 7 - i] = theme[THEME_FG];
        }
        for(i = 0; i < 3; i++) {
            data[(y + 9 + i) * p + x + 2] = theme[THEME_FG];
            data[(y + 9 + i) * p + x + 3] = theme[THEME_FG];
        }
    }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Display a palette
 */
void ui_pal(SDL_Texture *dst, int x, int y, int num, uint32_t *pal, int pressed)
{
    int i, j, k, p, pitch, a = pressed ? 0 : 1;
    uint8_t *data, *P = (uint8_t*)pal;

    if(x < 0 || y < 0 || num < 1 || !pal) return;

    if(dst == screen) { data = (uint8_t*)scr_data; pitch = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&data, &pitch);
    pitch /= 4; p = y * pitch + x;
    for(j=0; j < 16 && (j/4)*64 < num; j++, p += pitch) {
        k = (j/4)*64;
        for(i=0; i < 256 && k + i/4 < num; i++) {
            data[(p + i) * 4 + 0] = P[(k + i/4) * 4 + 0] >> a;
            data[(p + i) * 4 + 1] = P[(k + i/4) * 4 + 1] >> a;
            data[(p + i) * 4 + 2] = P[(k + i/4) * 4 + 2] >> a;
        }
    }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Display a number box
 * pressed = -1 inactive
 *           0  active
 *           1  up pressed
 *           2  down pressed
 */
void ui_num(SDL_Texture *dst, int x, int y, int w, int num, int pressed)
{
    char numstr[16];
    uint32_t c = ssfn_dst.fg, l, d, b;
    int ox = ui_clip.x;

    if(y < 0 || x >= ui_clip.x + ui_clip.w || y >= ui_clip.y + ui_clip.h || x + w <= ui_clip.x || y + 20 <= ui_clip.y) return;
    if(ui_input_bg == theme[THEME_INPBG]) {
        l = theme[THEME_LIGHTER];
        d = theme[THEME_DARKER];
        b = theme[THEME_BG];
    } else {
        l = (ui_input_bg + 0x101010) & 0x7F7F7F;
        d = (ui_input_bg - 0x101010) & 0x7F7F7F;
        b = ui_input_bg;
    }
    if(pressed == -1) {
        ssfn_dst.fg = theme[THEME_LIGHT];
        l = b = d = theme[THEME_INA];
    }
    ui_box(dst, x, y, w, 20, d, pressed == -1 ? theme[THEME_INA] : ui_input_bg, l);
    ui_box(dst, x + w - 12, y + 1, 11, 9, pressed == 1 ? d : l, b, pressed == 1 ? l : d);
    if(pressed != -1) ui_tri(dst, x + w - 10, y + 3, 1);
    ui_box(dst, x + w - 12, y + 10, 11, 9, pressed == 2 ? d : l, b, pressed == 2 ? l : d);
    if(pressed != -1) ui_tri(dst, x + w - 10, y + 12, 0);
    ssfn_dst.bg = 0;
    sprintf(numstr, "%6d", num);
    ui_clip.x = x;
    ui_text(dst, x + w - 62, y + 1, numstr);
    ui_clip.x = ox;
    ssfn_dst.fg = c;
}

/**
 * Display a time box
 */
void ui_time(SDL_Texture *dst, int x, int y, int w, int num, int max, int pressed)
{
    int mw = max <= 6000 && w < 80 ? 56 : 80;
    char numstr[16];
    uint32_t c = ssfn_dst.fg, l, d, b;

    if(w < mw) w = mw;
    if(y < 0 || x >= ui_clip.x + ui_clip.w || y >= ui_clip.y + ui_clip.h || x + w <= ui_clip.x || y + 20 <= ui_clip.y) return;
    if(ui_input_bg == theme[THEME_INPBG]) {
        l = theme[THEME_LIGHTER];
        d = theme[THEME_DARKER];
        b = theme[THEME_BG];
    } else {
        l = (ui_input_bg + 0x101010) & 0x7F7F7F;
        d = (ui_input_bg - 0x101010) & 0x7F7F7F;
        b = ui_input_bg;
    }
    if(pressed == -1) {
        ssfn_dst.fg = theme[THEME_LIGHT];
        l = b = d = theme[THEME_INA];
    }
    ui_box(dst, x, y, w, 20, d, pressed == -1 ? theme[THEME_INA] : ui_input_bg, l);
    ui_box(dst, x + w - 12, y + 1, 11, 9, pressed == 1 ? d : l, b, pressed == 1 ? l : d);
    if(pressed != -1) ui_tri(dst, x + w - 10, y + 3, 1);
    ui_box(dst, x + w - 12, y + 10, 11, 9, pressed == 2 ? d : l, b, pressed == 2 ? l : d);
    if(pressed != -1) ui_tri(dst, x + w - 10, y + 12, 0);
    ssfn_dst.bg = 0;
    if(max <= 6000 && w < 80)
        sprintf(numstr, "%02u.%02u", num / 100, num % 100);
    else
        sprintf(numstr, "%02u:%02u.%02u", num / 6000, (num / 100) % 60, num % 100);
    ui_text(dst, x + 2, y + 1, numstr);
    ssfn_dst.fg = c;
}

/**
 * Display a fade slider
 */
void ui_fade(SDL_Texture *dst, int x, int y, int w, int h, ui_fade_t *fade, int pressed)
{
    char numstr[16];
    int i, j, k, pitch, p, P, x2 = ui_clip.x + ui_clip.w, y2 = ui_clip.y + ui_clip.h;
    uint32_t *data, c;
    uint8_t *a = (uint8_t*)&ui_input_bg, *b = (uint8_t*)&theme[THEME_SELBG], *C = (uint8_t*)&c;

    if(x < 0 || y < 0 || x >= x2 || y >= y2) return;
    if(w < 20) w = 20;
    if(h < 7) h = 7;

    i = (pressed == -1 || !fade || !fade->in || !fade->out || !fade->max);
    ui_box(dst, x, y, w, h, theme[i ? THEME_INA : THEME_DARKER], i ? theme[THEME_INA] : ui_input_bg,
        theme[i ? THEME_INA : THEME_LIGHT]);
    ui_box(dst, x, y + h + 1, w, 5, theme[THEME_BG], theme[THEME_BG], theme[THEME_BG]);
    if(i) return;

    if(dst == screen) { data = scr_data; pitch = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&data, &pitch);
    x += 5; w -= 11; y++; h -= 2;
    if(fade->si == 0 && fade->so == 0) fade->so = w;
    if(fade->si + 9 >= fade->so) {
        if(pressed == 2) fade->si = fade->so - 9;
        else fade->so = fade->si + 9;
    }
    if(fade->si < 0) fade->si = 0;
    if(fade->si > w - 9) fade->si = w - 9;
    if(fade->so > w) fade->so = w;
    if(fade->so < 9) fade->so = 9;
    P = pitch/4; p = y * P + x;
    for(i = 0; i < w && x + i < x2; i++, p++) {
        if(i < fade->si) {
            C[0] = ((fade->si - i) * (unsigned int)a[0] + i * (unsigned int)b[0]) / fade->si;
            C[1] = ((fade->si - i) * (unsigned int)a[1] + i * (unsigned int)b[1]) / fade->si;
            C[2] = ((fade->si - i) * (unsigned int)a[2] + i * (unsigned int)b[2]) / fade->si;
        } else
        if(i > fade->so) {
            C[0] = ((i - fade->so) * (unsigned int)a[0] + (w - i) * (unsigned int)b[0]) / (w - fade->so);
            C[1] = ((i - fade->so) * (unsigned int)a[1] + (w - i) * (unsigned int)b[1]) / (w - fade->so);
            C[2] = ((i - fade->so) * (unsigned int)a[2] + (w - i) * (unsigned int)b[2]) / (w - fade->so);
        } else
            c = theme[THEME_SELBG];
        C[3] = 0xFF;
        for(j = 0; j < h && y + j < y2; j++)
            data[p + j * P] = c;
    }
    if(dst != screen) SDL_UnlockTexture(dst);
    ui_box(dst, x + fade->si - 4, y, 9, h, theme[pressed == 1 ? THEME_DARKER : THEME_LIGHTER], theme[THEME_BG],
        theme[pressed == 1 ? THEME_LIGHTER : THEME_DARKER]);
    ui_box(dst, x + fade->so - 4, y, 9, h, theme[pressed == 2 ? THEME_DARKER : THEME_LIGHTER], theme[THEME_BG],
        theme[pressed == 2 ? THEME_LIGHTER : THEME_DARKER]);
    i = fade->min ? *(fade->min) : 0;
    k = *(fade->max) - i;
    *(fade->in) = j = (fade->si * k + k / 2) / w + i;
    *(fade->out) = p = (fade->so * k + k / 2) / w + i;
    sprintf(numstr, "%02u:%02u.%02u", j / 6000, (j / 100) % 60, j % 100);
    i = fade->si; if(i < 12) { i = 12; } if(i > w - 12) i = w - 12;
    j = fade->so; if(j < 12) { j = 12; } if(j > w - 12) j = w - 12;
    if(i + 36 > j) {
        i += (j - i) / 2; j = i + 18; i -= 18;
        if(i < 12) { i = 12; j = 12 + 36; }
        if(j > w - 12) { i = w - 12 - 36; j = w - 12; }
    }
    ui_number(dst, x + i - 16, y + h + 2, numstr, theme[THEME_FG]);
    sprintf(numstr, "%02u:%02u.%02u", p / 6000, (p / 100) % 60, p % 100);
    ui_number(dst, x + j - 16, y + h + 2, numstr, theme[THEME_FG]);
}

/**
 * Set the fade slider
 */
void ui_fade_set(int w, ui_fade_t *fade)
{
    int k, m;

    if(w < 20) w = 20;
    w -= 11;
    if(fade && fade->in && fade->out && fade->max && *(fade->max) > 0) {
        m = fade->min ? *(fade->min) : 0;
        k = *(fade->max) - m; if(k < 1) k = 1;
        fade->si = (*(fade->in) == m ? 0 : ((*(fade->in) - m) * w + w / 2) / k);
        fade->so = ((*(fade->out) - m) * w + w / 2) / k;
    }
}

/**
 * Display a submit button (both string and icon)
 * ts = theme selector, 0 = default, 1 = blueish
 */
void ui_button(SDL_Texture *dst, int x, int y, int w, char *str, int icon, int pressed, int ts)
{
    int i, j, p, p2, p3, pitch, x2 = ui_clip.x + ui_clip.w, y2 = ui_clip.y + ui_clip.h;
    uint32_t l, b, B, d, t = theme[THEME_FG], c = theme[THEME_BTNB], P;
    uint32_t *data;

    if(x < 1 || y < 1 || x >= x2 || y >= y2 || w < 1) return;

    if(!ts) {
        l = theme[THEME_BTN0L]; b = theme[THEME_BTN0BL]; B = theme[THEME_BTN0BD]; d = theme[THEME_BTN0D];
    } else {
        l = theme[THEME_BTN1L]; b = theme[THEME_BTN1BL]; B = theme[THEME_BTN1BD]; d = theme[THEME_BTN1D];
    }
    if(pressed == -1) {
        l = d = b = B = theme[THEME_INA];
        t = theme[THEME_LIGHTER]; c = theme[THEME_DARKER];
        if(icon) icon = ui_inaicon(icon);
        pressed = 0;
    }

    if(dst == screen) { data = scr_data; pitch = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&data, &pitch);
    P = pitch/4; p = y * P + x; p2 = (y + 20 - 1) * P + x;
    for(i=0; i < w && x + i < x2; i++) {
        if(i && i < w - 1) data[p + i - P] = c;
        data[p + i] = !i || i == w - 1 ? c : (!pressed ? l : d);
        if(y + 20 < y2) {
            data[p2 + i] = !i || i == w - 1 ? c : (!pressed ? d : l);
            if(i && i < w - 1) data[p2 + i + P] = c;
        }
    }
    p += P;
    for(j=1, p3 = p; j+1 < 20 && y + j < y2; j++, p3 += P) {
        data[p3 - 1] = c;
        data[p3] = (pressed & 1 ? d : l);
        if(x + w < x2) {
            data[p3 + w - 1] = (!pressed ? d : l);
            data[p3 + w] = c;
        }
    }
    for(j=1; j + 1 < 20 && y + j < y2; j++, p += P)
        for(i=1; i + 1 < w && x + i < x2; i++)
            data[p + i] = !pressed ? (j < 12 ? b : B) : (j < 8 ? B : b);
    if(dst != screen) SDL_UnlockTexture(dst);
    ui_clip.w = x + w - ui_clip.x;
    if(str && *str) {
        ssfn_dst.bg = 0; l = ssfn_dst.fg;
        x += (w - ui_textwidth(str) - (icon ? 16 + 4 : 0)) / 2;
        if(icon) {
            ui_icon(dst, x, y + (!pressed ? 2 : 3), icon);
            x += 16 + 4;
        }
        if(t != theme[THEME_LIGHTER]) {
            ssfn_dst.fg = theme[THEME_BTNB];
            ui_text(dst, x - 1, y + (!pressed ? 0 : 1), str);
        }
        ssfn_dst.fg = t;
        ui_text(dst, x, y + (!pressed ? 1 : 2), str);
        ssfn_dst.fg = l;
    } else {
        ui_icon(dst, x + (w - 16) / 2, y + (!pressed ? 2 : 3), icon);
    }
    ui_clip.w = x2 - ui_clip.x;
}

/**
 * Button with character as icon
 */
void ui_charbtn(SDL_Texture *dst, int x, int y, int unicode, int pressed)
{
    uint32_t f = ssfn_dst.fg;
    int p;

    if(pressed == -1)
        ui_box(dst, x, y, 20, 20, theme[THEME_INA], theme[THEME_INA], theme[THEME_INA]);
    else
        ui_rect(dst, x, y, 20, 20, theme[pressed == 1 ? THEME_DARKER : THEME_LIGHTER],
            theme[pressed == 1 ? THEME_LIGHTER : THEME_DARKER]);
    ssfn_dst.w = ui_clip.x + ui_clip.w;
    ssfn_dst.h = ui_clip.y + ui_clip.h;
    if(dst == screen) { p = scr_p; ssfn_dst.ptr = (unsigned char *)scr_data; }
    else SDL_LockTexture(dst, NULL, (void**)&ssfn_dst.ptr, &p);
    ssfn_dst.p = p;
    ssfn_dst.x = x + 10 - ws[unicode] / 2;
    ssfn_dst.y = y + (pressed == 1 ? 3 : 2);
    ssfn_dst.fg = theme[pressed == -1 ? THEME_LIGHT : THEME_FG];
    if(ssfn_dst.x >= ui_clip.x)
        ssfn_putc(unicode);
    ssfn_dst.fg = f;
    if(dst != screen) SDL_UnlockTexture(dst);

}

/**
 * Button with icon
 */
void ui_iconbtn(SDL_Texture *dst, int x, int y, int icon, int pressed)
{
    if(pressed == -1)
        ui_box(dst, x, y, 20, 20, theme[THEME_INA], theme[THEME_INA], theme[THEME_INA]);
    else
        ui_rect(dst, x, y, 20, 20, theme[pressed == 1 ? THEME_DARKER : THEME_LIGHTER],
            theme[pressed == 1 ? THEME_LIGHTER : THEME_DARKER]);
    ui_icon(dst, x + 2, y + (pressed == 1 ? 3 : 2), pressed == -1 ? ui_inaicon(icon) : icon);
}

/**
 * Button with sprite
 */
void ui_sprbtn(SDL_Texture *dst, int x, int y, uint32_t *icon, int pressed)
{
    if(pressed == -1)
        ui_box(dst, x, y, 20, 20, theme[THEME_INA], theme[THEME_INA], theme[THEME_INA]);
    else
        ui_rect(dst, x, y, 20, 20, theme[pressed == 1 ? THEME_DARKER : THEME_LIGHTER],
            theme[pressed == 1 ? THEME_LIGHTER : THEME_DARKER]);
    ui_blitbuf(dst, x + 2, y + (pressed == 1 ? 3 : 2), 16, 16, icon, 0, 0, 16, 16, 64);
}

/**
 * Display a select box
 */
void ui_select(SDL_Texture *dst, int x, int y, int w, char *str, int pressed)
{
    uint32_t f = ssfn_dst.fg, b = ssfn_dst.bg;
    int cw = ui_clip.w;

    ssfn_dst.bg = pressed == -1 ? theme[THEME_INA] : ui_input_bg;
    ssfn_dst.fg = theme[pressed == -1 ? THEME_LIGHT : THEME_FG];
    ui_box(dst, x, y, w, 20, theme[pressed == -1 ? THEME_INA : THEME_DARKER], ssfn_dst.bg,
        theme[pressed == -1 ? THEME_INA : THEME_LIGHTER]);
    ui_clip.w = x + w - 20 - ui_clip.x;
    ui_text(dst, x + 2, y + 1, str);
    ui_clip.w = cw;
    ssfn_dst.bg = b;
    ssfn_dst.fg = f;
    ui_box(dst, x + w - 19, y + 1, 18, 18, theme[pressed == -1 ? THEME_INA : (pressed == 1 ? THEME_DARKER : THEME_LIGHTER)],
        theme[THEME_BG],
        theme[pressed == -1 ? THEME_INA : (pressed == 1 ? THEME_LIGHTER : THEME_DARKER)]);
    if(pressed != -1) ui_tri(dst, x + w - 19 + 6, y + 8, pressed);
}

/**
 * Display a select box popup (option list)
 */
void ui_select_popup(SDL_Texture *dst, int x, int y, int w, int h, ui_select_t *sel)
{
    char tmp[256];
    int j, p, o = ui_clip.x;
    uint32_t B = (ui_input_bg & 0xFF000000) | ((ui_input_bg + 0x0A0A0A) & 0x7F7F7F), f = ssfn_dst.fg;

    (void)y;
    ui_box2(dst, 0, 0, w, h, theme[THEME_DARKER], ui_input_bg, theme[THEME_LIGHTER], 0, ui_popup_scr * 18, 18, 0);
    if(!sel || !sel->opts) return;
    if(sel->first) {
        tmp[0] = '(';
        strncpy(tmp + 1, lang[sel->first], sizeof(tmp) - 3);
        strcat(tmp + 1, ")");
    }
    if(ui_popup_val < ui_popup_scr) ui_popup_scr = ui_popup_val;
    if(ui_popup_val > ui_popup_scr + ui_popup_page - 1) ui_popup_scr = ui_popup_val - ui_popup_page + 1;
    if(ui_popup_scr < ui_popup_min) ui_popup_scr = ui_popup_min;
    w -= 2;
    if(ui_popup_page < ui_popup_max - ui_popup_min) {
        j = ui_popup_scr; x = ui_popup_max - ui_popup_min;
        if(x < 1) x = 1;
        if(j + ui_popup_page > x) j = x - ui_popup_page;
        if(j < 0) j = 0;
        ui_box(dst, w - 3, 2, 4, h - 4, ui_input_bg, ui_input_bg, ui_input_bg);
        ui_box(dst, w - 3, 2 + (h - 4 - 20) * j / x, 4, 20 + (h - 4 - 20) * ui_popup_page / x, B, B, B);
        w -= 3;
    }
    p = ui_clip.w;
    ui_clip.w = w;
    ui_clip.x = 0;
    for(j = ui_popup_scr, x = 0; x < ui_popup_page && j < ui_popup_max - ui_popup_min; x++, j++) {
        if(j == ui_popup_val) {
            ui_box(dst, 1, 2 + x * 18, w, 18, theme[THEME_SELBG], theme[THEME_SELBG],
                theme[THEME_SELBG]);
            ssfn_dst.fg = theme[THEME_SELFG];
        } else
            ssfn_dst.fg = theme[THEME_FG];
        if(ui_popup_flags && (*ui_popup_flags & (1 << j)))
            ui_icon(dst, 2, 2 + x * 18, ICON_VISIB);
        ui_text(dst, 2 + (ui_popup_flags ? 18 : 0), 2 + x * 18, j < 0 ? tmp : sel->opts[j]);
    }
    ssfn_dst.fg = f;
    ui_clip.w = p;
    ui_clip.x = o;
}

/**
 * Display a text input box
 */
void ui_input(SDL_Texture *dst, int x, int y, int w, char *str, int pressed)
{
    uint32_t f = ssfn_dst.fg, b = ssfn_dst.bg, *data = NULL;
    int i, cw = ui_clip.w, pitch;
    char *s;

    ssfn_dst.bg = pressed == -1 ? theme[THEME_INA] : ui_input_bg;
    ssfn_dst.fg = theme[pressed == -1 ? THEME_LIGHT : THEME_FG];
    ui_box(dst, x, y, w, 20, theme[pressed == -1 ? THEME_INA : (pressed ? THEME_CURSOR : THEME_DARKER)], ssfn_dst.bg,
        theme[pressed == -1 ? THEME_INA : (pressed ? THEME_CURSOR : THEME_LIGHTER)]);
    ssfn_dst.w = x + w - 2; ui_clip.w = ssfn_dst.w - ui_clip.x; ssfn_dst.h = ui_clip.y + ui_clip.h;
    if(pressed == 1 && ui_input_type) {
        /* edit mode */
        if(dst == screen) { data = scr_data; pitch = scr_p; }
        else SDL_LockTexture(dst, NULL, (void**)&data, &pitch);
        ssfn_dst.ptr = (uint8_t*)data;
        ssfn_dst.p = pitch;
        ssfn_dst.x = x + 2;
        ssfn_dst.y = y + 1;
        if(ui_input_cur < ui_input_scr) ui_input_scr = ui_input_cur;
        do {
            for(i = ssfn_dst.x, s = ui_input_scr; s < ui_input_cur && *s && i < ssfn_dst.w; i += ws[ssfn_utf8(&s)]);
            if(i < ssfn_dst.w || s == ui_input_scr) break;
            do { ui_input_scr++; } while(ui_input_scr < ui_input_end && (*ui_input_scr & 0xC0) == 0x80);
        } while(1);
        s = ui_input_scr;
        while(ssfn_dst.x < ssfn_dst.w) {
            if(s == ui_input_cur) {
                for(i = 2; i < 16; i++)
                    data[(ssfn_dst.y + i) * pitch/4 + ssfn_dst.x - 1] = theme[THEME_CURSOR];
                data[(ssfn_dst.y+1) * pitch/4 + ssfn_dst.x - 2] =
                data[(ssfn_dst.y+1) * pitch/4 + ssfn_dst.x] =
                data[(ssfn_dst.y+i) * pitch/4 + ssfn_dst.x - 2] =
                data[(ssfn_dst.y+i) * pitch/4 + ssfn_dst.x] = theme[THEME_CURSOR];
            }
            if(!*s || s == ui_input_end) break;
            ssfn_putc(ssfn_utf8(&s));
        }
        if(dst != screen) SDL_UnlockTexture(dst);
    } else {
        /* display mode */
        ui_text(dst, x + 2, y + 1, str);
    }
    ui_clip.w = ssfn_dst.w = cw;
    ssfn_dst.bg = b;
    ssfn_dst.fg = f;
}

/**
 * Color version selector
 */
void ui_clrver(SDL_Texture *dst, int x, int y, int val, uint32_t *pal, int p, int pressed, int ask)
{
    int i, j, k, n, r, g, b;
    uint8_t *P;
    uint32_t c;

    if(x < 0 || y < 0) return;

    for(i = 0; i < 16; i++) {
        j = pressed == -1 || !pal || (pal[i * p / 4] & 0xff000000) != 0xff000000 || !(pal[i * p / 4] & 0xffffff);
        ui_box(dst, x + i * 12, y, 10, 10, theme[j ? THEME_INA : (val == i ? THEME_DARKER : THEME_LIGHTER)],
            theme[j ? THEME_INA : THEME_BG],
            theme[j ? THEME_INA : (val == i ? THEME_LIGHTER : THEME_DARKER)]);
        if(!j) {
            P = (uint8_t*)&pal[i * p / 4];
            for(k = n = r = g = b = 0; k < 8; k++, P += 4) {
                if(P[3] == 255 && (P[0] || P[1] || P[2])) {
                    r += P[0]; g += P[1]; b += P[2];
                    n++;
                }
            }
            if(n > 0) { r /= n; g /= n; b /= n; }
            c = 0xff000000 | (b << 16) | (g << 8) | r;
            ui_box(dst, x + i * 12 + 1, y + (val == i ? 2 : 1), 8, 7, c, c, c);
        }
    }
    if(ask) {
        ui_box(dst, x + i * 12, y, 10, 10, theme[(val >= i ? THEME_DARKER : THEME_LIGHTER)],
            theme[THEME_BG],
            theme[val >= i ? THEME_LIGHTER : THEME_DARKER]);
        ui_text(dst, x + i * 12 + 3, y + (val >= i ? 3 : 2), "΀");
    }
}

/******************** color picker ********************/

/**
 * Color selector button
 */
void ui_color(SDL_Texture *dst, int x, int y, uint32_t color, int pressed)
{
    int i, j, pitch, p, x2 = ui_clip.x + ui_clip.w, y2 = ui_clip.y + ui_clip.h;
    uint32_t *data;
    uint8_t *d, *a = (uint8_t*)&theme[THEME_LIGHT], *b = (uint8_t*)&theme[THEME_DARK], *C = (uint8_t*)&color;

    if(x < 0 || y < 0 || x >= x2 || y >= y2 || x + 20 <= ui_clip.x || y + 20 <= ui_clip.y) return;

    ui_box(dst, x, y, 20, 20, theme[pressed == -1 ? THEME_INA : (pressed == 1 ? THEME_DARKER : THEME_LIGHTER)],
        theme[pressed == -1 ? THEME_INA : THEME_BG],
        theme[pressed == -1 ? THEME_INA : (pressed == 1 ? THEME_LIGHTER : THEME_DARKER)]);
    if(pressed == -1) return;
    if(dst == screen) { data = scr_data; pitch = scr_p; }
    else SDL_LockTexture(dst, NULL, (void**)&data, &pitch);
    pitch /= 4; p = (y + 4 + (pressed == 1 ? 1 : 0)) * pitch + x + 4;
    for(j=0; j + 8 < 20 && y + j < y2; j++, p += pitch)
        for(i=0; i + 8 < 20 && x + i < x2; i++) {
            d = ((j / 6) & 1) ^ ((i / 6) & 1) ? a : b;
            ((uint8_t*)&data[p+i])[0] = (C[0]*C[3] + (256 - C[3])*d[0])>>8;
            ((uint8_t*)&data[p+i])[1] = (C[1]*C[3] + (256 - C[3])*d[1])>>8;
            ((uint8_t*)&data[p+i])[2] = (C[2]*C[3] + (256 - C[3])*d[2])>>8;
        }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Display the color picker popup
 */
void ui_color_picker(int x, int y, uint32_t *value)
{
    int i, j, pitch, p, x2 = ui_clip.x + ui_clip.w, y2 = ui_clip.y + ui_clip.h;
    uint32_t *data, h;
    uint8_t *d, *a = (uint8_t*)&theme[THEME_LIGHT], *b = (uint8_t*)&theme[THEME_DARK], *C = (uint8_t*)&ui_color_tmp;

    if(ui_popup_type != FORM_COLOR) {
        ui_popup_type = FORM_COLOR;
        ui_popup(x, y, CW, CH, 3);
        ui_color_fld = (palette_sel == -1 ? 0 : 2);
        ui_color_value = value;
        ui_color_tmp = ui_color_last = *value;
    }
    if(!popup) return;
    ui_box(popup, 0, 0, CW, CH, theme[THEME_LIGHTER], theme[THEME_BG], theme[THEME_DARKER]);

    SDL_LockTexture(popup, NULL, (void**)&data, &pitch);
    pitch /= 4; p = 4 * pitch + 4;
    for(j=0; j + 8 < 20 && y + j < y2; j++, p += pitch)
        for(i=0; i + 8 < 20 && x + i < x2; i++) {
            d = ((j / 6) & 1) ^ ((i / 6) & 1) ? a : b;
            ((uint8_t*)&data[p+i])[0] = (C[0]*C[3] + (256 - C[3])*d[0])>>8;
            ((uint8_t*)&data[p+i])[1] = (C[1]*C[3] + (256 - C[3])*d[1])>>8;
            ((uint8_t*)&data[p+i])[2] = (C[2]*C[3] + (256 - C[3])*d[2])>>8;
        }
    SDL_UnlockTexture(popup);

    sprintf(ui_color_hex, "%02X%02X%02X%02X", C[3], C[2], C[1], C[0]);
    ui_input(popup, CW - 104, 4, 72, ui_color_hex, ui_input_type != NULL);

    ui_iconbtn(popup, CW - 24, 4, ICON_PAL, ui_color_fld != 0);

    switch(ui_color_fld) {
        case 0:
            SDL_LockTexture(popup, NULL, (void**)&data, &pitch);
            pitch /= 4;
            for(p = 32 * pitch + 8, j = 0; j < 256; j++, p += pitch) {
                if(C[3] == 255-j) {
                    data[p - 4] = theme[THEME_SELFG]; data[p - 3] = theme[THEME_SELFG]; data[p - 2] = theme[THEME_SELFG];
                    data[p - 3 - pitch] = theme[THEME_SELFG]; data[p - 3 + pitch] = theme[THEME_SELFG];
                    data[p - 4 - 2*pitch] = theme[THEME_SELFG]; data[p - 4 - pitch] = theme[THEME_SELFG];
                    data[p - 4 + pitch] = theme[THEME_SELFG]; data[p - 4 + 2*pitch] = theme[THEME_SELFG];
                    for(i = 0; i < 15; i++)
                        data[p + i] = theme[THEME_SELFG];
                } else
                    for(i = 0; i < 15; i++) {
                        d = (j & 8) ^ (i & 8) ? a : b;
                        ((uint8_t*)&data[p+i])[0] = (d[0]*j + (256 - j)*C[0])>>8;
                        ((uint8_t*)&data[p+i])[1] = (d[1]*j + (256 - j)*C[1])>>8;
                        ((uint8_t*)&data[p+i])[2] = (d[2]*j + (256 - j)*C[2])>>8;
                    }
            }
            palette_rgb2hsv(0xFF000000 | ui_color_tmp, &hue, &sat, &val);
            for(p = 32 * pitch + 26, j = 0; j < 256; j++, p += pitch) {
                for(i = 0; i < 256; i++) {
                    h = 255-j == val ? j+(((val * i) >> 8) & 0xFF) : j; h |= 0xFF000000 | (h << 16) | (h << 8);
                    data[p + i] = sat == i || 255-j == val ? h : palette_hsv2rgb(255, hue,i,255-j);
                }
            }
            for(p = 32 * pitch + 26 + 256 + 2, j = 0; j < 256; j++, p += pitch) {
                h = palette_hsv2rgb(255, j, 255, 255);
                if(hue == j) {
                    h = theme[THEME_SELFG];
                    data[p + 19] = h; data[p + 18] = h; data[p + 17] = h; data[p + 18 - pitch] = h;
                    data[p + 18 + pitch] = h; data[p + 19 - 2*pitch] = h;
                    data[p + 19 - pitch] = h; data[p + 19 + pitch] = h;
                    data[p + 19 + 2*pitch] = h;
                }
                for(i = 0; i < 15; i++)
                    data[p + i] = h;
            }
            SDL_UnlockTexture(popup);
        break;
        case 1:
            palette_show(popup, 17, 32, ui_color_btn);
        break;
        case 2:
            SDL_LockTexture(popup, NULL, (void**)&data, &pitch);
            pitch /= 4;
            C = (uint8_t*)&palette_cpals[palette_sel * 257 + 1];
            for(y = 0; y < 16; y++) {
                for(x = 0; x < 16 && y * 16 + x < (int)palette_cpals[palette_sel * 257]; x++, C += 4) {
                    p = (32 + y * 16) * pitch + 18 + x * 17;
                    for(j=0; j < 15; j++)
                        for(i=0; i < 16; i++) {
                            d = ((j / 8) & 1) ^ ((i / 8) & 1) ? a : b;
                            ((uint8_t*)&data[p+j*pitch+i])[0] = (C[0]*C[3] + (256 - C[3])*d[0])>>8;
                            ((uint8_t*)&data[p+j*pitch+i])[1] = (C[1]*C[3] + (256 - C[3])*d[1])>>8;
                            ((uint8_t*)&data[p+j*pitch+i])[2] = (C[2]*C[3] + (256 - C[3])*d[2])>>8;
                        }
                }
            }
            SDL_UnlockTexture(popup);
        break;
    }
}

/**
 * Color picker hex edit callback
 */
void ui_color_cb(void *data)
{
    uint8_t *C = (uint8_t*)&ui_color_tmp;
    (void)data;
    if(C) {
        ui_color_tmp = 0;
        C[3] = gethex(ui_input_buf + 0, 2);
        C[2] = gethex(ui_input_buf + 2, 2);
        C[1] = gethex(ui_input_buf + 4, 2);
        C[0] = gethex(ui_input_buf + 6, 2);
    }
}

/**
 * Color picker controller
 */
int ui_color_ctrl()
{
    uint8_t *C = (uint8_t*)&ui_color_tmp;
    int i;

    switch(event.type) {
        case SDL_MOUSEBUTTONDOWN:
            ui_color_btn = 0;
            if(event.button.x > ui_popup_pos.x + CW - 104 && event.button.x < ui_popup_pos.x + CW - 104 + 72 &&
              event.button.y > ui_popup_pos.y + 4 && event.button.y < ui_popup_pos.y + 24) {
                ui_input_start(&ui_color_str, ui_color_cb);
            } else {
                if(ui_input_type) SDL_StopTextInput();
                ui_input_type = NULL;
            }
            if(event.button.x > ui_popup_pos.x + CW - 24 && event.button.x < ui_popup_pos.x + CW - 4 &&
              event.button.y > ui_popup_pos.y + 4 && event.button.y < ui_popup_pos.y + 24) {
                if(ui_color_fld) { ui_color_fld = 0; palette_sel = -1; }
                else { ui_color_fld = 1; if(palette_sel < 0) palette_sel = 0; }
                ui_color_picker(0, 0, ui_color_value);

            }
            if(ui_color_fld == 0 && event.button.y >= ui_popup_pos.y + 32 && event.button.y < ui_popup_pos.y + 32 + 256) {
                if(event.button.x >= ui_popup_pos.x + 8 && event.button.x < ui_popup_pos.x + 8 + 16) ui_color_btn = 1;
                if(event.button.x >= ui_popup_pos.x + 26 && event.button.x < ui_popup_pos.x + 26 + 256) ui_color_btn = 2;
                if(event.button.x >= ui_popup_pos.x + 26 + 256 + 2 && event.button.x < ui_popup_pos.x + 26 + 256 + 26)
                    ui_color_btn = 3;
            }
            if(ui_color_fld == 1 && event.button.y > ui_popup_pos.y + 53 && event.button.y < ui_popup_pos.y + 53 + 200) {
                if(event.button.x > ui_popup_pos.x + 17 && event.button.x < ui_popup_pos.x + 17 + 256 + 6) {
                    i = (event.button.y - ui_popup_pos.y - 53) / 20;
                    if(i == palette_sel) ui_color_fld = 2;
                    else palette_sel = i;
                }
                if(event.button.x > ui_popup_pos.x + 17 + 256 + 6 && event.button.x < ui_popup_pos.x + 17 + 256 + 6 + 12)
                    ui_color_btn = 4;
                ui_color_picker(0, 0, ui_color_value);
                ui_color_last = ui_color_tmp + 1;
            }
            if(ui_color_fld == 2 && event.button.y >= ui_popup_pos.y + 32 && event.button.y < ui_popup_pos.y + 32 + 256 &&
                event.button.x >= ui_popup_pos.x + 18 && event.button.x < ui_popup_pos.x + 18 + 16 * 17) {
                    i = ((event.button.y - ui_popup_pos.y - 32) / 16) * 16 + (event.button.x - ui_popup_pos.x - 18) / 17;
                    if(i >= 0 && i < (int)palette_cpals[palette_sel * 257]) {
                        ui_color_btn = 3;
                        ui_color_tmp = palette_cpals[palette_sel * 257 + 1 + i];
                        ui_color_picker(0, 0, ui_color_value);
                    }
            }
        break;
        case SDL_MOUSEBUTTONUP:
            if(!ui_input_type && event.button.y >= ui_popup_pos.y + 32 && (ui_color_fld == 0 || ui_color_fld == 2) &&
              ui_color_tmp == ui_color_last) {
                /* with clicking only allow full transparency with full black */
                if(!C[3] && ui_color_tmp) C[3] = 255;
                *ui_color_value = ui_color_tmp;
                return -1;
            }
            ui_color_last = ui_color_tmp;
            ui_color_btn = 0;
            ui_color_picker(0, 0, ui_color_value);
        break;
        case SDL_MOUSEWHEEL:
            if(ui_color_fld == 1) {
                if(event.wheel.y > 0 && palette_sel > 0) palette_sel--;
                if(event.wheel.y < 0 && palette_sel < palette_num - 1) palette_sel++;
                ui_color_picker(0, 0, ui_color_value);
            }
        break;
        case SDL_KEYDOWN:
            if((ui_color_fld == 0 || ui_color_fld == 2) && event.key.keysym.sym == SDLK_RETURN) {
                *ui_color_value = ui_color_tmp;
                return -1;
            }
            if(ui_color_fld == 1) {
                switch(event.key.keysym.sym) {
                    case SDLK_UP: if(palette_sel > 0) palette_sel--; break;
                    case SDLK_DOWN: if(palette_sel < palette_num - 1) palette_sel++; break;
                    case SDLK_RETURN: ui_color_fld = 2; break;
                }
                ui_color_picker(0, 0, ui_color_value);
            }
        break;
    }
    if(!ui_input_btn) ui_color_btn = 0;
    if(ui_input_btn && !ui_color_fld && ui_cury >= ui_popup_pos.y + 32 && ui_cury < ui_popup_pos.y + 32 + 256) {
        switch(ui_color_btn) {
            case 1:
                C[3] = 255 - (ui_cury - ui_popup_pos.y - 32);
                ui_color_picker(0, 0, ui_color_value);
            break;
            case 2:
                sat = ui_curx - ui_popup_pos.x - 26; val = 255 - (ui_cury - ui_popup_pos.y - 32);
                if(sat < 0) sat = 0;
                if(sat > 255) sat = 255;
                ui_color_tmp = palette_hsv2rgb(C[3], hue, sat, val);
                ui_color_picker(0, 0, ui_color_value);
            break;
            case 3:
                hue = (ui_cury - ui_popup_pos.y - 32);
                ui_color_tmp = palette_hsv2rgb(C[3], hue, sat, val);
                ui_color_picker(0, 0, ui_color_value);
            break;
        }
    }
    return (!ui_color_fld && ui_cury >= ui_popup_pos.y + 32 && ui_cury < ui_popup_pos.y + 32 + 256 &&
        ui_curx >= ui_popup_pos.x + 26 && ui_curx < ui_popup_pos.x + 26 + 256);
}

/******************** palette options ********************/

/**
 * Display the palette options popup
 */
void ui_palette(int x, int y)
{
    if(ui_popup_type != FORM_PALETTE) {
        ui_popup_type = FORM_PALETTE;
        ui_popup(x, y + 20, CW, PH, 3);
        ui_color_btn = palette_cie = 0;
    }
    if(!popup) return;
    ui_box(popup, 0, 0, CW, PH, theme[THEME_LIGHTER], theme[THEME_BG], theme[THEME_DARKER]);

    palette_show(popup, 17, 4, ui_color_btn == 1);
    ui_button(popup, 17, PH - 31, 20, NULL, ICON_REMOVE, palette_sel < 0 ? -1 : ui_color_btn == 2, 0);
    ui_button(popup, 71, PH - 31, 36, NULL, ICON_SWRB, ui_color_btn == 3, 0);
    ui_bool(popup, 138, PH - 30, 56, palette_sel < 0 ? -1 : palette_cie, "CIE76");
    ui_button(popup, CH - 59 - 36, PH - 31, 36, NULL, ICON_PAL, palette_sel < 0 ? -1 : ui_color_btn==4, 0);
    ui_button(popup, CH - 25, PH - 31, 20, NULL, ICON_ADD, ui_color_btn == 5, 0);
}

/**
 * Palette options controller
 */
int ui_palette_ctrl()
{
    switch(event.type) {
        case SDL_MOUSEBUTTONDOWN:
            ui_color_btn = 0;
            if(event.button.y > ui_popup_pos.y + 20 && event.button.y < ui_popup_pos.y + 20 + 200) {
                if(event.button.x > ui_popup_pos.x + 17 + 256 + 6 && event.button.x < ui_popup_pos.x + 17 + 256 + 6 + 12)
                    ui_color_btn = 1;
                else
                    palette_sel = (event.button.y - ui_popup_pos.y - 25) / 20;
            }
            if(event.button.y > ui_popup_pos.y + PH - 31 && event.button.y < ui_popup_pos.y + PH - 11) {
                if(palette_sel >= 0 && event.button.x > ui_popup_pos.x + 17 && event.button.x < ui_popup_pos.x + 37)
                    ui_color_btn = 2;
                if(event.button.x > ui_popup_pos.x + 71 && event.button.x < ui_popup_pos.x + 107)
                    ui_color_btn = 3;
                if(palette_sel >= 0 && event.button.x > ui_popup_pos.x + 138 &&
                  event.button.x < ui_popup_pos.x + 138 + 56)
                    palette_cie ^= 1;
                if(palette_sel >= 0 && event.button.x > ui_popup_pos.x + CH - 59 - 36 &&
                  event.button.x < ui_popup_pos.x + CH - 59)
                    ui_color_btn = 4;
                if(event.button.x > ui_popup_pos.x + CH - 25 && event.button.x < ui_popup_pos.x + CH - 5) ui_color_btn = 5;
            }
            ui_palette(0, 0);
        break;
        case SDL_MOUSEWHEEL:
            if(event.wheel.y > 0 && palette_sel > 0) palette_sel--;
            if(event.wheel.y < 0 && palette_sel < palette_num - 1) palette_sel++;
            ui_palette(0, 0);
        break;
        case SDL_KEYDOWN:
            switch(event.key.keysym.sym) {
                case SDLK_UP: if(palette_sel > 0) palette_sel--; break;
                case SDLK_DOWN: if(palette_sel < palette_num - 1) palette_sel++; break;
            }
            ui_palette(0, 0);
        break;
        case SDL_MOUSEMOTION:
            ui_status(0, "");
            if(event.motion.y > ui_popup_pos.y + PH - 31 && event.motion.y < ui_popup_pos.y + PH - 11) {
                if(event.motion.x > ui_popup_pos.x + 17 && event.motion.x < ui_popup_pos.x + 37)
                    ui_status(0, lang[SPRITES_DELPAL]);
                if(event.motion.x > ui_popup_pos.x + 71 && event.motion.x < ui_popup_pos.x + 107)
                    ui_status(0, lang[SPRITES_SWAPRB]);
                if(event.motion.x > ui_popup_pos.x + 138 && event.motion.x < ui_popup_pos.x + 138 + 56)
                    ui_status(0, lang[SPRITES_CIE76]);
                if(event.motion.x > ui_popup_pos.x + CH - 59 - 36 && event.motion.x < ui_popup_pos.x + CH - 59)
                    ui_status(0, lang[SPRITES_PAL]);
                if(event.motion.x > ui_popup_pos.x + CH - 25 && event.motion.x < ui_popup_pos.x + CH - 5)
                    ui_status(0, lang[SPRITES_IMPPAL]);
            }
        break;
        case SDL_MOUSEBUTTONUP:
            if(event.button.y > ui_popup_pos.y + PH - 31 && event.button.y < ui_popup_pos.y + PH - 11) {
                if(ui_color_btn == 2 && event.button.x > ui_popup_pos.x + 17 && event.button.x < ui_popup_pos.x + 37 &&
                  ui_modal(ICON_WARN, LANG_SURE, NULL)) {
                    palette_del(palette_sel);
                }
                if(ui_color_btn == 3 && event.button.x > ui_popup_pos.x + 71 && event.button.x < ui_popup_pos.x + 107) {
                    newsprite_swaprb();
                    return -1;
                }
                if(ui_color_btn == 4 && event.button.x > ui_popup_pos.x + CH - 59 - 36 &&
                  event.button.x < ui_popup_pos.x + CH - 59) {
                    newsprite_convpal();
                    return -1;
                }
                if(ui_color_btn == 5 && event.button.x > ui_popup_pos.x + CH - 25 && event.button.x < ui_popup_pos.x + CH - 5) {
                    fileops_type = FILEOPS_PALETTE; ui_switchtab(SUBMENU_IMPORT);
                    return -1;
                }
            }
            ui_color_btn = 0;
            ui_palette(0, 0);
        break;
    }
    return 0;
}

/******************** path selector ********************/

/**
 * Refresh drives list
 */
void ui_pathsel_getdrives(ui_pathsel_t *pathsel)
{
#ifdef __WIN32__
    DWORD drives = GetLogicalDrives();
    int i, j = 0;
    char old = 0;
#endif

    if(!pathsel) return;
    if(pathsel->sel.opts) {
#ifdef __WIN32__
        old = pathsel->sel.opts[pathsel->sel.val][0];
#endif
        project_freedir(&pathsel->sel.opts, NULL);
    }
    pathsel->sel.val = 0;
#ifdef __WIN32__
    pathsel->sel.opts = (char**)main_alloc(28 * sizeof(char*));
    for(i = 0; i < 27; i++)
        if(drives & (1 << i)) {
            pathsel->sel.opts[j] = (char*)malloc(4);
            if(pathsel->sel.opts[j]) {
                pathsel->sel.opts[j][0] = 'A' + i;
                if(old == pathsel->sel.opts[j][0])
                    pathsel->sel.val = j;
                pathsel->sel.opts[j][1] = ':';
                pathsel->sel.opts[j][2] = '\\';
                pathsel->sel.opts[j++][3] = 0;
            }
        }
#endif
}

/**
 * Add one directory component to path selector
 */
void ui_pathsel_add(ui_pathsel_t *pathsel, char *str)
{
    char *e;

    if(!pathsel || !str || !*str || *str == SEP[0]) return;
    if(pathsel->path[pathsel->num].dir)
        free(pathsel->path[pathsel->num].dir);
    for(e = str; *e && *e != SEP[0]; e++);
    pathsel->path[pathsel->num].dir = (char*)main_alloc(e - str + 2);
    memcpy(pathsel->path[pathsel->num].dir, str, e - str);
    strcpy(pathsel->path[pathsel->num++].dir + (uintptr_t)(e - str), SEP);
}

/**
 * Set a string path into path selector
 */
void ui_pathsel_set(ui_pathsel_t *pathsel, char *str)
{
    int i;
    char drive;

    if(!pathsel || !str || (str[0] != '/' && str[1] != ':')) return;

    pathsel->num = 0;
    if(str[0] == '/') str++;
    else {
        if(!pathsel->sel.opts) return;
        drive = str[0] - (str[0] >= 'a' && str[0] <= 'z' ? ('a' - 'A') : 0);
        for(i = 0; pathsel->sel.opts[i] && pathsel->sel.opts[i][0] != drive; i++);
        if(!pathsel->sel.opts[i]) return;
        pathsel->sel.val = i;
        str += 3;
    }
    while(*str) {
        ui_pathsel_add(pathsel, str);
        while(*str && *str != SEP[0]) str++;
        if(*str) str++;
    }
}

/**
 * Get the path into a string
 */
void ui_pathsel_get(ui_pathsel_t *pathsel, char *str)
{
    int i;

    if(!pathsel || !str) return;

    if(pathsel->sel.opts) {
        strcpy(str, pathsel->sel.opts[pathsel->sel.val]);
        str += 3;
    } else
        *str++ = SEP[0];
    *str = 0;
    for(i = 0; i < pathsel->num && pathsel->path[i].dir; i++) {
        strcpy(str, pathsel->path[i].dir);
        str += strlen(pathsel->path[i].dir);
    }
}

/**
 * Path selector click handler
 */
int ui_pathsel_click(ui_pathsel_t *pathsel, int x, int y)
{
    ui_select_t *sel;
    int dw, i, j;

    if(!pathsel || x < 0 || y < 0) return 0;
    ui_pathsel_getdrives(pathsel);

    sel = &(pathsel->sel);
    dw = sel && sel->opts ? 32 : 16;
    if(x < dw) { pathsel->fld = -1; return 1; }
    for(j = dw + 4, i = 0; i < pathsel->num && pathsel->path[i].dir; i++, j += dw + 4) {
        dw = ui_textwidth(pathsel->path[i].dir) + 8;
        if(x >= j && x < j + dw) { pathsel->fld = i; return 1; }
    }
    return 0;
}

/**
 * Path selector release handler
 */
int ui_pathsel_release(ui_pathsel_t *pathsel, int x, int y)
{
    ui_select_t *sel;
    int dw, i, j;

    if(!pathsel || x < 0 || y < 0) return 0;

    sel = &(pathsel->sel);
    dw = sel && sel->opts ? 32 : 16;
    if(x < dw) {
        if(sel->opts) return 1;
        pathsel->num = 0; return 0;
    }
    for(j = dw + 4, i = 0; i < pathsel->num && pathsel->path[i].dir; i++, j += dw + 4) {
        dw = ui_textwidth(pathsel->path[i].dir) + 8;
        if(x >= j && x < j + dw && pathsel->fld == i) {
            pathsel->num = i + 1;
            break;
        }
    }
    return 0;
}

/**
 * Display a path selector
 */
void ui_pathsel(SDL_Texture *dst, int x, int y, int w, int h, ui_pathsel_t *pathsel, int pressed)
{
    ui_select_t *sel;
    int dw, i;

    if(!dst || !pathsel || w < 1 || h < 1) return;
    if(pressed == -1) {
        ui_box(dst, x, y, w, h, theme[THEME_INA], theme[THEME_INA], theme[THEME_INA]);
        return;
    }
    sel = &(pathsel->sel);
    dw = sel && sel->opts ? 32 : 16;
    ui_box(dst, x, y, dw, 18, theme[pressed && pathsel->fld == -1 ? THEME_DARKER : THEME_LIGHTER], theme[THEME_BG],
        theme[pressed && pathsel->fld == -1 ? THEME_LIGHTER : THEME_DARKER]);
    ui_text(dst, x + 4, y + (pressed && pathsel->fld == -1 ? 1 : 0), sel && sel->opts ? sel->opts[sel->val] : SEP);
    x += dw + 4;
    for(i = 0; i < pathsel->num && pathsel->path[i].dir; i++, x += dw + 4) {
        dw = ui_textwidth(pathsel->path[i].dir) + 8;
        ui_box(dst, x, y, dw, 18, theme[pressed && pathsel->fld == i ? THEME_DARKER : THEME_LIGHTER], theme[THEME_BG],
            theme[pressed && pathsel->fld == i ? THEME_LIGHTER : THEME_DARKER]);
        ui_text(dst, x + 4, y + (pressed && pathsel->fld == i ? 1 : 0), pathsel->path[i].dir);
    }
}

/******************** data table ********************/

/**
 * Resize table
 */
void ui_table_resize(ui_form_t *elem, int w, int h)
{
    int i, j, hw = 0, hn = 0;
    ui_table_t *table;

    if(!elem || w < 1 || h < 1) return;
    table = (ui_table_t*)elem->param;
    elem->w = w;
    elem->h = h;
    if(table) {
        table->clk = -1;
        if(table->hdr) {
            for(i = hn = hw = 0; table->hdr[i].label; i++)
                if(table->hdr[i].width) {
                    table->hdr[i].w = table->hdr[i].width;
                    hw += table->hdr[i].width;
                } else hn++;
            if(hn) {
                hn = (w - 2 - hw) / hn;
                for(j = i = 0; table->hdr[i].label; i++)
                    if(!table->hdr[i].width) { table->hdr[i].w = hn; hw += hn; j = i; }
                /* take care for rounding errors by using the entire remaining space for the last resizable coloumn */
                table->hdr[j].w = w - 2 - (hw - hn);
            }
        }
    }
}

/**
 * Table button click handler
 */
int ui_table_click(ui_form_t *elem, int x, int y)
{
    ui_table_t *table;
    int i, j, dy, d1, d2;

    if(!elem || !elem->param || x < 0 || y < 0) return 0;
    table = elem->param;
    table->fld = 0;
    dy = 1 + (table->hdr ? 20 : 0);
    if(table->hdr && y < dy) {
        for(j = i = 0; table->hdr[i].label; i++) {
            if(table->hdr[i].order && x >= j && x < j + table->hdr[i].w) {
                table->fld = i;
                if(table->order == i) {
                    table->hdr[i].order = table->hdr[i].order == 1 ? 2 : 1;
                } else {
                    table->order = i;
                    table->hdr[i].order = 1;
                }
                table->val = 0;
                if(table->reorder)
                    (*table->reorder)(table);
                return 1;
            }
            j += table->hdr[i].w;
        }

    }
    if(y >= dy && y < elem->h && (table->first || table->num > 0)) {
        if(x > elem->w - 13) {
            d1 = (elem->h - 20) * table->scr / (table->num + table->first);
            d2 = 20 + (elem->h - 20) * table->page / (table->num + table->first);
            if(y >= dy + d1 && y < dy + d1 + d2) {
                table->fld = -1;
                ui_scroll.scry = &table->scr;
                ui_scroll.sely = &table->val;
                ui_scroll.posy = y - (dy + d1);
                ui_scroll.pagey = table->page;
                ui_scroll.numy = table->num + table->first;
                ui_scroll.srty = elem->y + dy;
                ui_scroll.sizey = elem->h;
                ui_scroll.min = -table->first;
                return 1;
            }
        } else {
            if(table->col) {
                j = (elem->w - 15) / table->col;
                table->val = i = (y - dy) / table->row * j + ((x - 1) / table->col) + table->scr;
            } else {
                table->val = i = (y - dy) / table->row + table->scr;
            }
            if(table->val < -table->first) table->val = -table->first;
            if(table->val > table->num - 1) table->val = table->num - 1;
            if(table->clk != i) {
                table->clk = table->val;
                return 1;
            } else {
                if(!elem->onchange) {
                    ui_pressed = -1;
                    return 0;
                } else
                    ui_onchange(ui_pressed);
                return 1;
            }
        }
    }
    return 0;
}

/**
 * Display a data table
 */
void ui_table(SDL_Texture *dst, int x, int y, int w, int h, ui_table_t *table, int pressed)
{
    uint8_t *data;
    uint32_t f = ssfn_dst.fg;
    int i, j, k, l, dy, dh, hx, y2, page, line;
    SDL_Rect save;

    if(!dst || !table || w < 1 || h < 1) return;
    if(pressed == -1) {
        ui_box(dst, x, y, w, h, theme[THEME_INA], theme[THEME_INA], theme[THEME_INA]);
        return;
    }
    dy = y + 1 + (table->hdr ? 20 : 0);
    dh = h - 2 - (table->hdr ? 20 : 0);
    ui_box2(dst, x, y, w, h, theme[THEME_DARKER], ui_input_bg, theme[THEME_LIGHTER], table->hdr ? 20 : 0, table->scr * 18,
        table->row, table->col ? table->col : table->dcol);

    if(table->hdr) {
        for(i = 0, hx = x + 1; table->hdr[i].label && hx < x + w; i++) {
            j = pressed && table->hdr[i].label < NUMTEXTS && table->hdr[i].order && table->fld == i;
            ui_box(dst, hx, y + 1, table->hdr[i].w, 20, theme[j ? THEME_DARKER : THEME_LIGHTER], theme[THEME_BG],
                theme[j ? THEME_LIGHTER : THEME_DARKER]);
            if(table->hdr[i].label != -1) {
                if(table->hdr[i].label > 0 && table->hdr[i].label < NUMTEXTS) {
                    if(table->hdr[i].order && table->order == i)
                        ui_tri(dst, hx + table->hdr[i].w - 12, y + (j ? 11 : 10) - table->hdr[i].order,
                            table->hdr[i].order == 1 ? 0 : 1);
                    ui_text2(dst, hx + 4, y + 2 + j, lang[table->hdr[i].label], theme[THEME_DARKER], theme[THEME_LIGHTER]);
                } else
                if(table->hdr[i].label > NUMTEXTS)
                    ui_icon(dst, hx + (table->hdr[i].w - 16) / 2, y + 3, table->hdr[i].label - NUMTEXTS);
                else {
                    l = (table->hdr[i].w - ws[-table->hdr[i].label]) / 2;
                    if(dst == screen) { ssfn_dst.ptr = (uint8_t*)scr_data; k = scr_p; }
                    else SDL_LockTexture(dst, NULL, (void**)&ssfn_dst.ptr, &k);
                    ssfn_dst.p = k;
                    ssfn_dst.y = y + 3 + j;
                    ssfn_dst.x = hx + l + 1;
                    ssfn_dst.fg = theme[THEME_LIGHT];
                    ssfn_putc(-table->hdr[i].label);
                    ssfn_dst.x = hx + l;
                    ssfn_dst.y--;
                    ssfn_dst.fg = theme[THEME_DARK];
                    ssfn_putc(-table->hdr[i].label);
                    if(dst != screen) SDL_UnlockTexture(dst);
                    ssfn_dst.fg = f;
                }
            }
            hx += table->hdr[i].w;
        }
    }
    if(table->val >= table->num) table->val = table->num - 1;

    if(table->entsize) {
        if(table->first || (table->data && table->num > 0)) {
            page = dh / (table->row ? table->row : 1);
            line = (w - 14) / (table->col ? table->col : 1);
            table->page = table->col ? (page * line + 1) : page;
            if(!table->data || !table->num) table->val = table->scr = -table->first;
            if(table->col) {
                if(table->val < table->scr) table->scr = table->val - (table->val % line);
                if(table->val > table->scr + (page - 1) * line) table->scr = table->val - (table->val % line) - (page - 1) * line;
            } else {
                if(table->val < table->scr) table->scr = table->val;
                if(table->val > table->scr + table->page - 1) table->scr = table->val - table->page + 1;
            }
            if(table->scr < -table->first) table->scr = -table->first;
            data = (uint8_t*)table->data + table->entsize * table->scr;
            ui_vscrbar(dst, x + w - 13, dy, 12, dh, table->scr, table->page, table->num + table->first, pressed &&
                table->fld == -1);
            memcpy(&save, &ui_clip, sizeof(ui_clip)); ui_clip.x = x + 1; ui_clip.y = dy; ui_clip.w = w - 14; ui_clip.h = dh;
            y2 = dy + dh;
            if(table->col) {
                for(i = table->scr; i < table->num && dy < y2; dy += table->row) {
                    for(j = l = 0; i < table->num && l + table->col < w - 14; j++, i++, l += table->col, data += table->entsize) {
                        k = i == table->val && (!table->fld || table->fld < -1 || !pressed);
                        ssfn_dst.fg = theme[k ? THEME_SELFG : THEME_FG];
                        if(k)
                            ui_box(dst, x + l + 1, dy, table->col, table->row, theme[THEME_SELBG], theme[THEME_SELBG],
                                theme[THEME_SELBG]);
                        if(table->drawcell)
                            (*table->drawcell)(dst, i, table->val, x + l, dy, table->col, table->row, table->hdr,
                                i < 0 ? NULL : data);
                    }
                }
            } else {
                for(i = table->scr; i < table->num && dy < y2; i++, dy += table->row, data += table->entsize) {
                    k = i == table->val && (!table->fld || table->fld < -1 || !pressed);
                    ssfn_dst.fg = theme[k ? THEME_SELFG : THEME_FG];
                    if(k)
                        ui_box(dst, x + 1, dy, w - 14, table->row, theme[THEME_SELBG], theme[THEME_SELBG], theme[THEME_SELBG]);
                    if(table->drawcell)
                        (*table->drawcell)(dst, i, table->val, x, dy, w - 14, table->row, table->hdr, i < 0 ? NULL : data);
                    else if(i >= 0)
                        ui_text(dst, x + 4, dy + 1, *((char**)data));
                }
            }
            memcpy(&ui_clip, &save, sizeof(ui_clip));
        } else
        if(table->nodata) {
            i = ui_textwidth(lang[table->nodata]);
            ssfn_dst.fg = theme[THEME_LIGHT];
            ui_text(dst, x + ((w - i) / 2), y - 1 + (table->hdr ? 20 : 0) + ((dh - 16) / 2), lang[table->nodata]);
        }
    }
    ssfn_dst.fg = f;
}

/**
 * Render a row (if table->cell zero) or cell
 */
void ui_table_render(int x, int y, int w, int h, ui_table_t *table)
{
    uint8_t *data;
    int i, j, l, dy, dh, y2;

    if(!table || !table->rendercell || w < 1 || h < 1) return;
    dy = y + 1 + (table->hdr ? 20 : 0);
    dh = h - 2 - (table->hdr ? 20 : 0);
    if(table->first || (table->data && table->num > 0)) {
        if(!table->data || !table->num) table->val = table->scr = -table->first;
        data = (uint8_t*)table->data + table->entsize * table->scr;
        y2 = dy + dh;
        ui_clip.h = table->row - 1;
        if(table->col) {
            ui_clip.w = table->col;
            for(i = table->scr; i < table->num && dy < y2; dy += table->row) {
                for(j = l = 0; i < table->num && l + table->col < w - 14; j++, i++, l += table->col, data += table->entsize) {
                    ui_clip.x = x + l + 1; ui_clip.y = dy + 1;
                    if(ui_clip.y + ui_clip.h > y + h - 1) ui_clip.h = y + h - ui_clip.y - 1;
                    SDL_RenderSetClipRect(renderer, &ui_clip);
                    (*table->rendercell)(NULL, i, table->val, x + l + 1, dy + 1, table->col - 2, table->row - 2, table->hdr,
                        i < 0 ? NULL : data);
                }
            }
        } else {
            ui_clip.x = x; ui_clip.w = w - 14;
            for(i = table->scr; i < table->num && dy < y2; i++, dy += table->row, data += table->entsize) {
                ui_clip.y = dy; if(ui_clip.y + ui_clip.h > y + h - 1) ui_clip.h = y + h - ui_clip.y - 1;
                SDL_RenderSetClipRect(renderer, &ui_clip);
                (*table->rendercell)(NULL, i, table->val, x, dy, w - 14, table->row, table->hdr, i < 0 ? NULL : data);
            }
        }
    }
    ui_clip.x = ui_clip.y = 0; ui_clip.w = scr_w; ui_clip.h = scr_h;
    SDL_RenderSetClipRect(renderer, NULL);
}

/******************** scrollable area ********************/

/**
 * Set scroll area
 */
void ui_scrarea_set(ui_scrarea_t *s, int x, int y, int w, int h)
{
    s->sx = x; s->sy = y;
    s->sw = w; s->sh = h;
}

/**
 * Calculate view window for scroll area
 */
void ui_scrarea_win(ui_scrarea_t *s, SDL_Rect *src, SDL_Rect *dst)
{
    dst->x = s->sx; dst->y = s->sy;
    dst->w = s->sw; dst->h = s->sh;
    src->y = s->y * s->oh / s->h;
    src->h = s->sh * s->oh / s->h;
    if(src->y + src->h > s->oh) { dst->h = dst->h * s->oh / src->h; src->h = s->oh; }
    src->x = s->x * s->ow / s->w;
    src->w = s->sw * s->ow / s->w;
    if(src->x + src->w > s->ow) { dst->w = dst->w * s->ow / src->w; src->w = s->ow; }
}

/**
 * Set zooming
 * z = 0 set to 1:1, z < 0 zoom out, z > 0 zoom in
 */
void ui_scrarea_zoom(ui_scrarea_t *s, int z)
{
    int w, h;

    if(!s || s->w < 1 || s->h < 1) return;
    w = s->w; h = s->h;
    if(!z) s->z = 0; else
    if(z < 0 && s->z > -3) { s->z--; s->w >>= 1; s->h >>= 1; } else
    if(z > 0 && s->z <  3) { s->z++; s->w <<= 1; s->h <<= 1; }
    if(!s->ow)
        maps_size(s->z, &s->w, &s->h);
    else
        if(!s->z) { s->w = s->ow; s->h = s->oh; }
    if(s->w < 1) s->w = 1;
    if(s->h < 1) s->h = 1;
    s->x = (s->x + s->sw / 2) * s->w / w - s->sw / 2;
    s->y = (s->y + s->sh / 2) * s->h / h - s->sh / 2;
    if(s->x < 0) s->x = 0;
    if(s->y < 0) s->y = 0;
    if(s->x + s->sw > s->w) s->x = s->w - s->sw;
    if(s->x < 0) s->x = 0;
    if(s->y + s->sh > s->h) s->y = s->h - s->sh;
    if(s->y < 0) s->y = 0;
}

/**
 * Reset scroll area
 */
void ui_scrarea_reset(ui_scrarea_t *s, int w, int h)
{
    s->x = s->y = s->z = 0;
    s->w = s->ow = w;
    s->h = s->oh = h;
}

/**
 * Convert screen coordinates to scroll area coordinates
 */
void ui_scrarea_btn(ui_scrarea_t *s, int x, int y, int *dx, int *dy)
{
    *dx = (x + s->x) * s->ow / s->w;
    if(*dx < 0) *dx = 0;
    if(*dx >= s->ow) *dx = s->ow;
    *dy = (y + s->y) * s->oh / s->h;
    if(*dy < 0) *dy = 0;
    if(*dy >= s->oh) *dy = s->oh;
}

/******************** frames selector ********************/

/**
 * Frames selector button
 */
void ui_frames(SDL_Texture *dst, int x, int y, int w, int h, ui_frames_t *frm, int pressed)
{
    int i, j, p, ox = ui_clip.x, ow = ui_clip.w;
    uint32_t c, *data;

    if(pressed == -1 || !frm || frm->num < 1) {
        ui_box(dst, x, y, w, h, theme[THEME_INA], theme[THEME_INA], theme[THEME_INA]);
        return;
    }
    if(frm) {
        ui_box(dst, x, y, w, h, theme[THEME_BG], theme[THEME_BG], theme[THEME_BG]);
        ui_box(dst, x, y + h - 12, w, 12, theme[THEME_DARKER], theme[THEME_DARKER], theme[THEME_DARKER]);
        i = (w - 20) * (frm->str - 1) / frm->num;
        ui_box(dst, x + i, y + h - 12,
            20 + (frm->str - 1 + frm->cnt >= frm->num ? w - 20 : (w - 20) * (frm->str - 1 + frm->cnt) / frm->num) - i, 12,
            theme[pressed && ui_popup_page == -1 ? THEME_DARK : THEME_LIGHT], theme[THEME_BG],
            theme[pressed && ui_popup_page == -1 ? THEME_LIGHT : THEME_DARK]);
        if(frm->scr > frm->num * 68 - w) frm->scr = frm->num * 68 - w;
        if(frm->scr < 0) frm->scr = 0;
        ui_clip.x = x; ui_clip.w = w;
        for(i = 0, j = -frm->scr; i < frm->num && j < w; i++, j += 68) {
            if(j + 68 < 0) continue;
            c = theme[(i >= frm->str - 1 && i < frm->str - 1 + frm->cnt ? (pressed && ui_popup_page == 1 ? THEME_SELBX :
                THEME_SELBG) : THEME_INPBG)];
            ui_box(dst, x + j, y, 64, 64, c, c, c);
            if(dst == screen) { data = scr_data; p = scr_p; }
            else SDL_LockTexture(dst, NULL, (void**)&data, &p);
            ui_resample(data, x + j, y, 64, 64, p, 255, frm->data + (((newsprite_dir + frm->dir) & 7) * frm->num + i) *
                frm->size * frm->size, frm->size, frm->size, frm->size * 4);
            if(dst != screen) SDL_UnlockTexture(dst);
        }
        ui_clip.x = ox; ui_clip.w = ow;
    }
}

/**
 * Frames on click handler
 */
int ui_frames_click(ui_form_t *elem, int x, int y)
{
    ui_frames_t *frm;
    int d1, d2;

    if(!elem || !elem->param || x < 0 || y < 0) return 0;
    frm = elem->param;
    if(frm->num < 1) return 0;
    if(y > elem->h - 12) {
        ui_popup_page = 0;
        d1 = (elem->w - 20) * (frm->str - 1) / frm->num;
        d2 = 20 + (frm->str - 1 + frm->cnt >= frm->num ? elem->w - 20 : (elem->w - 20) * (frm->str - 1 + frm->cnt) / frm->num);
        if(x >= elem->x + d1 && x < elem->x + d2) {
            ui_popup_page = -1;
            ui_scroll.scrx = &frm->str;
            ui_scroll.posx = x - (elem->x + d1);
            ui_scroll.pagex = 0;
            ui_scroll.numx = frm->num;
            ui_scroll.srtx = elem->x;
            ui_scroll.sizex = elem->w - 20;
            ui_scroll.min = 1;
        }
    } else {
        if(ui_curbtn == 1) {
            frm->clk = (frm->scr + x) / 68;
            frm->str = frm->clk + 1;
            frm->cnt = 1;
            ui_popup_page = 1;
        } else {
            ui_popup_page = -3;
            ui_scroll.scrx = &frm->scr;
            ui_scroll.posx = x;
            ui_scroll.pagex = 0;
            ui_scroll.numx = frm->num * 68;
            ui_scroll.srtx = elem->x;
            ui_scroll.sizex = elem->w;
            ui_scroll.min = 0;
            ui_scroll.origx = x + frm->scr;
        }
    }
    return 1;
}

/**
 * Frames mouse over handler
 */
int ui_frames_over(ui_form_t *elem, int x, int y)
{
    ui_frames_t *frm;
    int i;

    if(!elem || !elem->param || x < 0 || y < 0) return 0;
    frm = elem->param;
    if(x < 12 && frm->scr > 0) { frm->scr -= 16; return 1; }
    if(x > elem->w - 12 && frm->scr < frm->num * 68 - elem->w) { frm->scr += 16; return 1; }
    if(ui_curbtn) {
        i = (frm->scr + x) / 68;
        if(frm->clk != i && i >= frm->str - 1) {
            frm->clk = i;
            frm->cnt = i - (frm->str - 1) + 1;
            newsprite_chsel((void*)-1);
            return 1;
        }
    }
    return 0;
}


/******************** sprite picker ********************/

/**
 * Sprite selector button
 * unique:
 * 0 - select a sprite with all of its direction variations
 * 1 - select a sprite with only south variation
 * 2 - as 1, but allow selecting masks too (start with '~')
 * 3 - as 1, but only allow marked sprites
 * 4 - as 3, but do not allow empty selection
 */
void ui_sprite(SDL_Texture *dst, int x, int y, int w, int h, ui_sprsel_t *spr, int pressed)
{
    SDL_Rect rect;
    uint32_t f = ssfn_dst.fg;
    int p;

    if(pressed == -1)
        ui_box(dst, x, y, w, h, theme[THEME_INA], theme[THEME_INA], theme[THEME_INA]);
    else
    if(pressed == -2)
        ui_rect(dst, x, y, w, h, theme[THEME_DARKER], theme[THEME_DARKER]);
    else
        ui_rect(dst, x, y, w, h, theme[pressed == 1 ? THEME_DARKER : THEME_LIGHTER],
            theme[pressed == 1 ? THEME_LIGHTER : THEME_DARKER]);
    if(spr) {
        if(spr->unique == 4 && (spr->val < 0 || spr->val >= project.spritenum[spr->cat] ||
          !(project.sprites[spr->cat][spr->val].y & 0x10000))) {
            for(p = 0; p < project.spritenum[spr->cat] && !(project.sprites[spr->cat][p].y & 0x10000); p++);
            spr->val = p >= project.spritenum[spr->cat] ? -1 : p;
        }
        if(spr->val == -1 || spr->val >= project.spritenum[spr->cat] || pressed <= -1) {
            if(spr->icon) {
                ssfn_dst.w = ui_clip.x + ui_clip.w;
                ssfn_dst.h = ui_clip.y + ui_clip.h;
                if(dst == screen) { p = scr_p; ssfn_dst.ptr = (unsigned char *)scr_data; }
                else SDL_LockTexture(dst, NULL, (void**)&ssfn_dst.ptr, &p);
                ssfn_dst.p = p;
                ssfn_dst.y = y + (h - 16) / 2 + (pressed == 1 ? 1 : 0);
                ssfn_dst.x = x + (w - 16) / 2 + (pressed != -1 ? 1 : 0);
                ssfn_dst.fg = theme[pressed == -2 ? THEME_DARK : THEME_LIGHT];
                if(ssfn_dst.x >= ui_clip.x) {
                    ssfn_putc(spr->icon);
                    if(pressed > -1) {
                        ssfn_dst.x = x + (w - 16) / 2;
                        ssfn_dst.y--;
                        ssfn_dst.fg = theme[THEME_DARK];
                        ssfn_putc(spr->icon);
                    }
                }
                ssfn_dst.fg = f;
                if(dst != screen) SDL_UnlockTexture(dst);
            }
        } else {
            rect.x = x + 2; rect.h = h - 4;
            rect.y = y + (pressed ? 3 : 2);
            if(spr->unique > 2) {
                p = ui_clip.w; ui_clip.w = x + w - 4 - ui_clip.x;
                ui_text(dst, rect.x + rect.h + 2, rect.y, project.sprites[spr->cat][spr->val].name);
                ui_clip.w = p;
                rect.w = rect.h;
            } else
                rect.w = w - 4;
            spr_blitsel(spr, -1, 256 + 255, dst, &rect);
        }
    }
}

/**
 * Sprite picker search box callback
 */
void ui_sprite_cb(void *data)
{
    int i, j, l, k;

    (void)data;
    if(!ui_sprite_spr) return;
    if(!ui_input_buf[0]) {
        for(i = ui_popup_max = 0; i < project.spritenum[ui_sprite_spr->cat]; i++) {
            if(ui_sprite_spr->unique == 2 && !ui_sprite_spr->cat && (project.sprites[0][i].dir[0].w != 8 || project.sprites[0][i].dir[0].h != 16))
                continue;
            if(ui_sprite_spr->unique != 2 && project.sprites[ui_sprite_spr->cat][i].name[0] == '~') continue;
            if(ui_sprite_spr->unique >= 3 && !(project.sprites[ui_sprite_spr->cat][i].y & 0x10000)) continue;
            project.sprites[ui_sprite_spr->cat][ui_popup_max++].idx = i;
        }
    } else {
        ui_popup_val = ui_popup_scr = ui_popup_max = 0; l = strlen(ui_input_buf);
        for(j = 0; j < project.spritenum[ui_sprite_spr->cat]; j++) {
            if(ui_sprite_spr->unique == 2 && !ui_sprite_spr->cat && (project.sprites[0][j].dir[0].w != 8 || project.sprites[0][j].dir[0].h != 16))
                continue;
            if(ui_sprite_spr->unique != 2 && project.sprites[ui_sprite_spr->cat][j].name[0] == '~') continue;
            if(ui_sprite_spr->unique >= 3 && !(project.sprites[ui_sprite_spr->cat][j].y & 0x10000)) continue;
            if(project.sprites[ui_sprite_spr->cat][j].loc) {
                k = strlen(project.sprites[ui_sprite_spr->cat][j].loc);
                if(k >= l) {
                    k -= l;
                    for(i = 0; i <= k && strncasecmp(project.sprites[ui_sprite_spr->cat][j].loc + i, ui_input_buf, l); i++);
                    if(i <= k) {
                        project.sprites[ui_sprite_spr->cat][ui_popup_max++].idx = j;
                        continue;
                    }
                }
            }
            k = strlen(project.sprites[ui_sprite_spr->cat][j].name);
            if(k < l) continue;
            k -= l;
            for(i = 0; i <= k && strncasecmp(project.sprites[ui_sprite_spr->cat][j].name + i, ui_input_buf, l); i++);
            if(i > k) continue;
            project.sprites[ui_sprite_spr->cat][ui_popup_max++].idx = j;
        }
    }
    ui_sprite_picker(0, 0, ui_sprite_spr);
}

/**
 * Free sprite picker resources
 */
void ui_sprite_free(int all)
{
    int i;

    for(i = 0; i < (all ? 9 : 8); i++)
        if(ui_sprite_anims[i]) {
            SDL_DestroyTexture(ui_sprite_anims[i]);
            ui_sprite_anims[i] = NULL;
        }
}

/**
 * Display the sprite picker popup
 */
void ui_sprite_picker(int x, int y, ui_sprsel_t *spr)
{
    ui_sprite_t *s;
    SDL_Rect rect;
    uint32_t f = ssfn_dst.fg, B = 0xFF000000 | ((theme[THEME_BG] + 0x0A0A0A) & 0x7F7F7F);
    int i, j, k, p, o = !spr || spr->unique ? 0 : 20;

    if(ui_popup_type != FORM_SPRITE) {
        if(!spr || !project.sprites[spr->cat]) return;
        ui_popup_type = FORM_SPRITE;
        ui_popup(x, y, SW, CH + o + 6 + SI / 2, 3);
        if(ui_sprite_ptr) spr->val = *ui_sprite_ptr;
        ui_sprite_spr = spr;
        strcpy(ui_input_buf, ui_sprite_srch);
        ui_sprite_cb(NULL);
        for(i = 0; i < ui_popup_max; i++)
            if(project.sprites[ui_sprite_spr->cat][i].idx == (spr->val < 0 ? 0 : spr->val)) {
                ui_popup_val = i;
                break;
            }
    }
    if(!popup) return;
    ui_popup_page = (ui_popup_pos.h - 38 - o) / SI;
    ui_box(popup, 0, 0, SW, ui_popup_pos.h, theme[THEME_LIGHTER], theme[THEME_BG], theme[THEME_DARKER]);

    rect.x = 4; rect.w = 28;
    rect.y = 4; rect.h = 28;
    spr_blitsel(ui_sprite_spr, 0, 256 + 255, popup, &rect);
    if(!ui_sprite_spr->unique) {
        if(ui_popup_val >= ui_popup_max) ui_popup_val = ui_popup_max - 1;
        if(ui_popup_val < ui_popup_scr) ui_popup_scr = ui_popup_val;
        if(ui_popup_val > ui_popup_scr + (ui_popup_page - 1)) ui_popup_scr = ui_popup_val - (ui_popup_page - 1);
        if(ui_popup_scr + (ui_popup_page - 1) > ui_popup_max) ui_popup_scr = ui_popup_max - (ui_popup_page - 1);
    } else {
        i = (ui_popup_page - 1) * 8;
        if(ui_popup_val >= ui_popup_max) ui_popup_val = ui_popup_max - 1;
        if(ui_popup_val < ui_popup_scr) ui_popup_scr = ui_popup_val & ~7;
        if(ui_popup_val > ui_popup_scr + i) ui_popup_scr = (ui_popup_val - i) & ~7;
        if(ui_popup_scr + i > ui_popup_max) ui_popup_scr = (ui_popup_max - i) & ~7;
    }
    if(ui_popup_scr < 0) ui_popup_scr = 0;
    if(ui_popup_val >= 0 && ui_popup_val < ui_popup_max && project.sprites[spr->cat]) {
        i = ui_clip.w; ui_clip.w = SW - 188;
        if(project.sprites[spr->cat][project.sprites[spr->cat][ui_popup_val].idx].loc) {
            ui_text(popup, 40, 2, project.sprites[spr->cat][project.sprites[spr->cat][ui_popup_val].idx].name);
            ui_text(popup, 40,18, project.sprites[spr->cat][project.sprites[spr->cat][ui_popup_val].idx].loc);
        } else
            ui_text(popup, 40, 8, project.sprites[spr->cat][project.sprites[spr->cat][ui_popup_val].idx].name);
        ui_clip.w = i;
    }
    ui_input(popup, SW - 164, 4, 160, ui_sprite_srch, ui_input_type != NULL);
    ui_icon(popup, SW - 186, 6, ICON_SRCH);
    ui_box2(popup, 4, 36, SW - 8, ui_popup_pos.h - 40, theme[THEME_DARKER], theme[THEME_BG], theme[THEME_LIGHTER],
        o, ui_popup_scr * SI, SI, SI);
    if(spr->val == -1) {
        SDL_LockTexture(popup, NULL, (void**)&ssfn_dst.ptr, &p);
        ssfn_dst.p = p;
        ssfn_dst.y = 9;
        ssfn_dst.x = 11;
        ssfn_dst.fg = theme[THEME_LIGHT];
        ssfn_putc(spr->icon);
        ssfn_dst.y = 8;
        ssfn_dst.x = 10;
        ssfn_dst.fg = theme[THEME_DARK];
        ssfn_putc(spr->icon);
        SDL_UnlockTexture(popup);
        ssfn_dst.fg = f;
    }
    if(ui_popup_val > ui_popup_max - 1) ui_popup_val = ui_popup_max - 1;
    if(ui_popup_val < 0) ui_popup_val = 0;
    ui_clip.h = ui_popup_pos.h - 5;
    ui_sprite_free(0);
    if(!ui_sprite_spr->unique) {
        for(i = 0; i < 8; i++) {
            ui_rect(popup, 5 + i * SI, 37, SI + (i == 7 ? 4 : 0), 20, theme[THEME_LIGHTER], theme[THEME_DARKER]);
            SDL_LockTexture(popup, NULL, (void**)&ssfn_dst.ptr, &p);
            ssfn_dst.p = p;
            ssfn_dst.y = 40;
            ssfn_dst.x = (SI - 8) / 2 + i * SI;
            ssfn_dst.fg = theme[THEME_LIGHT];
            ssfn_putc(17 + i);
            ssfn_dst.x = (SI - 8) / 2 - 1 + i * SI;
            ssfn_dst.y--;
            ssfn_dst.fg = theme[THEME_DARK];
            ssfn_putc(17 + i);
            SDL_UnlockTexture(popup);
        }
        ssfn_dst.fg = f;
        if(project.sprites[ui_sprite_spr->cat])
            for(j = 0, i = ui_popup_scr; i < ui_popup_max && j <= ui_popup_page; j++, i++) {
                if(i == ui_popup_val)
                    ui_box(popup, 5, 57 + j * SI, SW - 14, SI, theme[THEME_SELBG], theme[THEME_SELBG], theme[THEME_SELBG]);
                for(k = 0; k < 8; k++) {
                    rect.y = 57 + j * SI;
                    rect.x = 6 + k * SI;
                    s = spr_getidx(ui_sprite_spr->cat, project.sprites[ui_sprite_spr->cat][i].idx, k, 0);
                    if(s) {
                        ui_fit(SI - 2, SI - 2, s->w, s->h, &rect.w, &rect.h);
                        rect.x += (SI - 2 - rect.w) / 2; rect.y += SI - 2 - rect.h;
                        if(i == ui_popup_val)
                            spr_texture(s, &ui_sprite_anims[k]);
                        else
                            spr_blit(s, 256 + 160, popup, &rect);
                    }
                }
            }
    } else {
        if(project.sprites[ui_sprite_spr->cat])
            for(j = 0, i = ui_popup_scr; i < ui_popup_max && j < 8; j++)
                for(k = 0; k < 8 && i < ui_popup_max; k++, i++) {
                    s = spr_getidx(ui_sprite_spr->cat, project.sprites[ui_sprite_spr->cat][i].idx, -1, 0);
                    rect.x = 6 + k * SI; rect.w = SI - 2;
                    rect.y = 37 + j * SI; rect.h = SI - 2;
                    if(i == ui_popup_val) {
                        ui_box(popup, rect.x - 1, rect.y, SI, SI, theme[THEME_SELBG], theme[THEME_SELBG], theme[THEME_SELBG]);
                        spr_texture(s, &ui_sprite_anims[0]);
                    } else
                    if(s) {
                        ui_fit(SI - 2, SI - 2, s->w, s->h, &rect.w, &rect.h);
                        rect.x += (SI - 2 - rect.w) / 2; rect.y += SI - 2 - rect.h;
                        spr_blit(s, 256 + 160, popup, &rect);
                    }
                }
    }
    j = ui_popup_pos.h - 40 - o;                    /* h */
    if(ui_popup_max) {
        i = ui_sprite_spr->unique ? (ui_popup_page - 1) * 8 : ui_popup_page - 1;
        p = ui_popup_scr;
        if(p + i > ui_popup_max) p = ui_popup_max - i;
        if(p < 0) p = 0;
        ui_box(popup, SW - 9, 37 + o, 4, j - 2, theme[THEME_INPBG], theme[THEME_INPBG], theme[THEME_INPBG]);
        ui_box(popup, SW - 9, 37 + o + (j - 20) * p / ui_popup_max, 4, 20 + (j - 20) * i / ui_popup_max, B, B, B);
    } else
        ui_box(popup, SW - 9, 37 + o, 4, j - 2, B, B, B);
}

/**
 * Render sprite picker
 */
void ui_sprite_render()
{
    int i;
    SDL_Rect rect;

    if(ui_popup_type != FORM_SPRITE || !ui_sprite_spr || !project.sprites[ui_sprite_spr->cat] || !ui_popup_max) return;
    rect.x = ui_popup_pos.x + 6; rect.y = ui_popup_pos.y + 37; rect.w = ui_popup_pos.w - 10; rect.h = ui_popup_pos.h - 42;
    SDL_RenderSetClipRect(renderer, &rect);
    if(ui_sprite_spr->unique) {
        i = ui_popup_val - ui_popup_scr;
        rect.x = ui_popup_pos.x + 6 + (i & 7) * SI; rect.y = ui_popup_pos.y + 37 + (i >> 3) * SI; rect.w = rect.h = SI - 2;
        spr_render(spr_getidx(ui_sprite_spr->cat, project.sprites[ui_sprite_spr->cat][ui_popup_val].idx, -1, 0), -1,
            ui_sprite_anims[0], &rect);
    } else {
        rect.y = ui_popup_pos.y + 57 + (ui_popup_val - ui_popup_scr) * SI; rect.w = rect.h = SI - 2;
        for(i = 0; i < 8; i++) {
            rect.x = ui_popup_pos.x + 6 + i * SI;
            if(ui_sprite_anims[i])
                spr_render(spr_getidx(ui_sprite_spr->cat, project.sprites[ui_sprite_spr->cat][ui_popup_val].idx, i, 0), -1,
                    ui_sprite_anims[i], &rect);
        }
    }
    SDL_RenderSetClipRect(renderer, NULL);
}

/**
 * Sprite picker controller
 */
int ui_sprite_ctrl()
{
    int i, o = ui_sprite_spr->unique ? 0 : 20;

    if(!project.sprites[ui_sprite_spr->cat]) return 0;
    switch(event.type) {
        case SDL_MOUSEBUTTONDOWN:
            if(event.button.x > ui_popup_pos.x + SW - 164 && event.button.x < ui_popup_pos.x + SW - 4 &&
              event.button.y > ui_popup_pos.y + 4 && event.button.y < ui_popup_pos.y + 24) {
                ui_input_start(&ui_sprite_str, ui_sprite_cb);
            } else {
                if(ui_input_type) SDL_StopTextInput();
                ui_input_type = NULL;
            }
            if(event.button.x > ui_popup_pos.x + 4 && event.button.x < ui_popup_pos.x + SW - 9 &&
              event.button.y > ui_popup_pos.y + 37 + o && event.button.y < ui_popup_pos.y + ui_popup_pos.h - 5) {
                if(ui_sprite_spr->unique)
                    i = ((event.button.y - (ui_popup_pos.y + 37 + o)) / SI) * 8 + (event.button.x - ui_popup_pos.x - 4) / SI;
                else
                    i = (event.button.y - (ui_popup_pos.y + 37 + o)) / SI;
                i += ui_popup_scr;
                if(ui_popup_val != i) ui_popup_val = i;
                else {
                    ui_sprite_spr->val = project.sprites[ui_sprite_spr->cat][ui_popup_val].idx;
                    if(ui_sprite_ptr) *ui_sprite_ptr = ui_sprite_spr->val;
                    ui_sprite_ptr = NULL;
                    return -1;
                }
                ui_sprite_picker(0, 0, ui_sprite_spr);
              }
        break;
        case SDL_MOUSEBUTTONUP:
            ui_sprite_picker(0, 0, ui_sprite_spr);
        break;
        case SDL_MOUSEWHEEL:
            if(ui_input_type) {
                SDL_StopTextInput(); ui_input_type = NULL;
                memcpy(ui_sprite_srch, ui_input_buf, sizeof(ui_sprite_srch));
            }
            i = ui_sprite_spr->unique ? 8 : 1;
            if(event.wheel.y > 0 && ui_popup_val - i >= 0) ui_popup_val -= i;
            if(event.wheel.y < 0) ui_popup_val += i;
            if(event.wheel.x > 0 && ui_popup_val - 1 >= 0) ui_popup_val--;
            if(event.wheel.x < 0 && ui_popup_val + 1 < ui_popup_max) ui_popup_val++;
            ui_sprite_picker(0, 0, ui_sprite_spr);
        break;
        case SDL_KEYDOWN:
            i = ui_sprite_spr->unique ? 8 : 1;
            switch(event.key.keysym.sym) {
                case SDLK_RETURN:
                    ui_sprite_spr->val = project.sprites[ui_sprite_spr->cat][ui_popup_val].idx;
                    if(ui_sprite_ptr) *ui_sprite_ptr = ui_sprite_spr->val;
                    ui_sprite_ptr = NULL;
                    return -1;
                case SDLK_ESCAPE:
                    ui_sprite_ptr = NULL;
                    return -1;
                case SDLK_UP:
                    if(ui_popup_val - i >= 0) ui_popup_val -= i;
                break;
                case SDLK_DOWN:
                    ui_popup_val += i;
                break;
                case SDLK_LEFT:
                    if(ui_sprite_spr->unique && ui_popup_val - 1 >= 0) ui_popup_val--;
                break;
                case SDLK_RIGHT:
                    if(ui_sprite_spr->unique && ui_popup_val + 1 < ui_popup_max) ui_popup_val++;
                break;
                case SDLK_PAGEUP:
                    if(ui_sprite_spr->unique) {
                        i = (ui_popup_page - 1) * 8;
                        if(ui_popup_val - i >= 0) ui_popup_val -= i;
                    } else {
                        ui_popup_val -= ui_popup_page - 1;
                        if(ui_popup_val < 0) ui_popup_val = 0;
                    }
                break;
                case SDLK_PAGEDOWN:
                    if(ui_sprite_spr->unique) {
                        i = (ui_popup_page - 1) * 8;
                        if(ui_popup_val + i < ui_popup_max) ui_popup_val += i;
                    } else {
                        ui_popup_val += ui_popup_page - 1;
                        if(ui_popup_val > ui_popup_max - 1) ui_popup_val = ui_popup_max - 1;
                    }
                break;
                case SDLK_HOME: ui_popup_val = 0; break;
                case SDLK_END: ui_popup_val = ui_popup_max - 1; break;
            }
            ui_sprite_picker(0, 0, ui_sprite_spr);
        break;
    }
    return 0;
}

/******************** map coordinate picker ********************/

/**
 * Load a map
 */
void ui_map_load()
{
    int i = (ui_popup_val < 0 ? 0 : project.mapidx[ui_popup_val]);

    if(project.maps && i >= 0 && i < project.nummaps) {
        maps_loadtmx(project.maps[i], NULL, NULL, 1);
    }
}

/**
 * Free a map
 */
void ui_map_free()
{
    if(!ui_map_map) return;
    maps_freetmx();
    memset(&ui_map_scr, 0, sizeof(ui_scrarea_t));
}

/**
 * Map coordinate picker search box callback
 */
void ui_map_cb(void *data)
{
    int i, j, l, k;

    (void)data;
    if(!ui_map_map) return;
    if(!ui_input_buf[0]) {
        ui_popup_max = project.nummaps;
        for(i = 0; i < ui_popup_max; i++)
            project.mapidx[i] = i;
    } else {
        ui_popup_max = 0; l = strlen(ui_input_buf);
        for(j = 0; j < project.nummaps; j++) {
            k = strlen(project.maps[j]);
            if(k < l) continue;
            k -= l;
            for(i = 0; i <= k && strncasecmp(project.maps[j] + i, ui_input_buf, l); i++);
            if(i > k) continue;
            project.mapidx[ui_popup_max++] = j;
        }
    }
    ui_popup_val = -1;
    ui_map_picker(0, 0, ui_map_map);
}

/**
 * Display the map coordinate picker popup
 */
void ui_map_picker(int x, int y, ui_map_t *map)
{
    char tmp[256];
    uint32_t B = (ui_input_bg & 0xFF000000) | ((ui_input_bg + 0x0A0A0A) & 0x7F7F7F), f = ssfn_dst.fg;
    int i, j, p = ui_clip.w, w, h, o = ui_clip.x, page;

    if(ui_popup_type != FORM_MAPSEL) {
        if(!map || project.nummaps < 1) return;
        ui_popup_type = FORM_MAPSEL;
        ui_map_map = map;
        strcpy(ui_input_buf, ui_map_srch);
        ui_map_cb(NULL);
        i = (map->crd ? scr_h - 64 : (ui_popup_max < 11 ? ui_popup_max + 1 : 10) * 18 + 28 + 4 + 2);
        ui_popup(x, y, map->crd ? scr_w - 64 : 207, i, 3);
        if(*(map->val) < 0)
            ui_popup_val = -1;
        else
            for(i = 0; i < ui_popup_max; i++)
                if(project.mapidx[i] == *(map->val)) {
                    ui_popup_val = i;
                    break;
                }
        if(ui_map_map->crd) {
            ui_map_free();
            ui_popup_scr = 0;
            ui_map_load();
            maps_size(0, &ui_map_scr.w, &ui_map_scr.h);
            ui_map_scr.ow = ui_map_scr.oh = ui_map_scr.z = 0;
            ui_scrarea_set(&ui_map_scr, ui_popup_pos.x + 4, ui_popup_pos.y + 28, ui_popup_pos.w - 4 - 204 - 12 - 4,
                ui_popup_pos.h - 28 - 12 - 4);
            i = *(ui_map_map->crd) & 0x7fff;
            j = (*(ui_map_map->crd) >> 16) & 0x7fff;
            maps_map2scr(&ui_map_scr, i, j, &w, &h);
            ui_map_scr.x = w - ui_map_scr.sx - ui_map_scr.sw / 2;
            ui_map_scr.y = h - ui_map_scr.sy - ui_map_scr.sh / 2;
            ui_scrarea_zoom(&ui_map_scr, -1);
        } else
            ui_popup_scr = ui_pressed = -1;
    }
    if(!popup || !ui_map_map) return;
    ui_clip.x = 0;
    ui_box(popup, 0, 0, ui_popup_pos.w, ui_popup_pos.h, theme[THEME_LIGHTER], theme[THEME_BG], theme[THEME_DARKER]);
    ui_input(popup, ui_popup_pos.w - 164, 4, 160, ui_map_srch, ui_map_map->fix ? -1 : (ui_input_type != NULL));
    ui_icon(popup, ui_popup_pos.w - 186, 6, ICON_SRCH);
    if(ui_map_map->crd) {
        ui_vscrbar(popup, 4 + ui_map_scr.sw, 28, 12, ui_map_scr.sh, ui_map_scr.y, ui_map_scr.sh, ui_map_scr.h,
            (ui_popup_page == -1 || ui_popup_page == -3));
        ui_hscrbar(popup, 4, 28 + ui_map_scr.sh, ui_map_scr.sw, 12, ui_map_scr.x, ui_map_scr.sw, ui_map_scr.w,
            (ui_popup_page == -2 || ui_popup_page == -3));
        ui_clip.w = ui_popup_pos.w - 204 - 74;
        sprintf(tmp, "%3u,%3u", *(ui_map_map->crd) & 0x7fff, (*(ui_map_map->crd) >> 16) & 0x7fff);
        ui_text(popup, 4, 4, tmp);
        if(project.maps && ui_map_map->val && *ui_map_map->val >= 0 && project.mapidx[*ui_map_map->val] < project.nummaps)
            ui_text(popup, 4 + 64, 4, project.maps[project.mapidx[*ui_map_map->val]]);
        ui_clip.w = ui_popup_pos.w;
        ui_iconbtn(popup, ui_popup_pos.w - 204 - 72, 4, ICON_ZOUT, ui_popup_page == -4 ? 1 : 0);
        ui_iconbtn(popup, ui_popup_pos.w - 204 - 72 + 24, 4, ICON_ZNORM, ui_popup_page == -5 ? 1 : 0);
        ui_iconbtn(popup, ui_popup_pos.w - 204 - 72 + 48, 4, ICON_ZIN, ui_popup_page == -6 ? 1 : 0);
        i = 0;
    } else {
        ui_icon(popup, 2, 2, ICON_LINK);
        tmp[0] = '(';
        strncpy(tmp + 1, lang[LANG_NONE], sizeof(tmp) - 3);
        strcat(tmp + 1, ")");
        i = -1;
    }
    page = (ui_popup_pos.h - 28 - 4 - 2) / 18;
    if(ui_popup_val < ui_popup_scr) ui_popup_scr = ui_popup_val;
    if(ui_popup_val > ui_popup_scr + page - 1) ui_popup_scr = ui_popup_val - page + 1;
    if(ui_popup_scr < i) ui_popup_scr = i;
    ui_box2(popup, ui_popup_pos.w - 204, 28, 200, ui_popup_pos.h - 28 - 4, theme[THEME_DARKER], theme[THEME_BG],
        theme[THEME_LIGHTER], 0, ui_popup_scr, 18, 0);
    w = 198;
    h = (ui_popup_pos.h - 28 - 4);
    if(page < ui_popup_max) {
        j = ui_popup_scr; x = ui_popup_max;
        if(x < 1) x = 1;
        if(j + page > x) j = x - page;
        if(j < 0) j = 0;
        ui_box(popup, w - 3, 29, 4, h, ui_input_bg, ui_input_bg, ui_input_bg);
        ui_box(popup, w - 3, 29 + (h - 20) * j / x, 4, 20 + (h - 20) * page / x, B, B, B);
        w -= 3;
    }
    ui_clip.w = ui_popup_pos.w - 204 + w;
    for(j = ui_popup_scr, x = 0; x < page && j < ui_popup_max; x++, j++) {
        if(j == ui_popup_val) {
            ui_box(popup, ui_popup_pos.w - 203, 29 + x * 18, w, 18, theme[THEME_SELBG], theme[THEME_SELBG],
                theme[THEME_SELBG]);
            ssfn_dst.fg = theme[THEME_SELFG];
        } else
            ssfn_dst.fg = theme[THEME_FG];
        if(j < 0)
            ui_text(popup, ui_popup_pos.w - 202, 29 + x * 18, tmp);
        else
            ui_text(popup, ui_popup_pos.w - 202, 29 + x * 18, project.maps[project.mapidx[j]]);
    }
    ssfn_dst.fg = f;
    ui_clip.w = p;
    ui_clip.x = o;
}

/**
 * Render map coordinate picker
 */
void ui_map_render()
{
    if(!ui_map_map || !ui_map_map->crd) return;
    maps_rendertmx(&ui_map_scr, MAP_MASK_GRID | MAP_MASK_OBJ | MAP_MASK_GRD1|MAP_MASK_GRD2,
        ui_map_map->crd ? *(ui_map_map->crd) & 0x7fff : -1,
        ui_map_map->crd ? (*(ui_map_map->crd) >> 16) & 0x7fff : -1);
}

/**
 * Map coordinate picker controller
 */
int ui_map_ctrl()
{
    int i, j;

    switch(event.type) {
        case SDL_MOUSEBUTTONDOWN:
            ui_popup_page = 0;
            if(event.button.x > ui_popup_pos.x + ui_popup_pos.w - 204 && event.button.x < ui_popup_pos.x + ui_popup_pos.w - 9 &&
              event.button.y > ui_popup_pos.y + 33 && event.button.y < ui_popup_pos.y + ui_popup_pos.h - 5) {
                if(!ui_map_map->fix) {
                    i = ((event.button.y - (ui_popup_pos.y + 33)) / 18) + ui_popup_scr;
                    if(ui_popup_val != i) {
                        if(ui_map_map->val)
                            *(ui_map_map->val) = (project.mapidx && i >= 0 ? project.mapidx[i] : i);
                        ui_popup_val = i;
                        if(ui_map_map->crd)
                            ui_map_load();
                        else
                            return -1;
                    }
                }
            } else
            if(ui_map_map->crd) {
                ui_scroll.form = -1;
                ui_scroll.scrx = ui_scroll.scry = ui_scroll.selx = ui_scroll.sely = NULL;
                ui_scroll.numx = ui_map_scr.w;
                ui_scroll.numy = ui_map_scr.h;
                if(ui_map_scr.w > 0)
                    ui_scroll.posx = event.button.x - (ui_map_scr.sx + (ui_map_scr.sw - 20) * ui_map_scr.x / ui_map_scr.w);
                if(ui_map_scr.h > 0)
                    ui_scroll.posy = event.button.y - (ui_map_scr.sy + (ui_map_scr.sh - 20) * ui_map_scr.y / ui_map_scr.h);
                ui_scroll.srtx = ui_map_scr.sx;
                ui_scroll.srty = ui_map_scr.sy;
                ui_scroll.sizex = ui_map_scr.sw - 20;
                ui_scroll.pagex = ui_map_scr.sw;
                ui_scroll.sizey = ui_map_scr.sh - 20;
                ui_scroll.pagey = ui_map_scr.sh;
                if(event.button.y > ui_popup_pos.y + 4 && event.button.y < ui_popup_pos.y + 24) {
                    if(!ui_input_type) {
                        if(event.button.x > ui_popup_pos.x + ui_popup_pos.w - 204 - 72 &&
                          event.button.x < ui_popup_pos.x + ui_popup_pos.w - 204 - 72 + 20) {
                            ui_popup_page = -4;
                            ui_scrarea_zoom(&ui_map_scr, -1);
                        } else
                        if(event.button.x > ui_popup_pos.x + ui_popup_pos.w - 204 - 72 + 24 &&
                          event.button.x < ui_popup_pos.x + ui_popup_pos.w - 204 - 72 + 44) {
                            ui_popup_page = -5;
                            ui_scrarea_zoom(&ui_map_scr, 0);
                        } else
                        if(event.button.x > ui_popup_pos.x + ui_popup_pos.w - 204 - 72 + 48 &&
                          event.button.x < ui_popup_pos.x + ui_popup_pos.w - 204 - 72 + 68) {
                            ui_popup_page = -6;
                            ui_scrarea_zoom(&ui_map_scr, 1);
                        }
                    }
                    if(event.button.x > ui_popup_pos.x + ui_popup_pos.w - 164 && event.button.x < ui_popup_pos.x +
                      ui_popup_pos.w - 4)
                        ui_input_start(&ui_map_str, ui_map_cb);
                    else {
                        if(ui_input_type) SDL_StopTextInput();
                        ui_input_type = NULL;
                    }
                } else
                if(event.button.x >= ui_popup_pos.x + 4 + ui_map_scr.sw && event.button.x < ui_popup_pos.x + 4 +
                  ui_map_scr.sw + 12 && event.button.y >= ui_popup_pos.y + 28 && event.button.y < ui_popup_pos.y + 28 +
                  ui_map_scr.sh) {
                    ui_popup_page = -1;
                    ui_scroll.scry = &ui_map_scr.y;
                } else
                if(event.button.x >= ui_popup_pos.x + 4 && event.button.x < ui_popup_pos.x + 4 + ui_map_scr.sw &&
                  event.button.y >= ui_popup_pos.y + 28 + ui_map_scr.sh && event.button.y < ui_popup_pos.y + 28 +
                  ui_map_scr.sh + 12) {
                    ui_popup_page = -2;
                    ui_scroll.scrx = &ui_map_scr.x;
                } else
                if(event.button.x >= ui_popup_pos.x + 4 && event.button.x < ui_popup_pos.x + 4 + ui_map_scr.sw &&
                  event.button.y >= ui_popup_pos.y + 28 && event.button.y < ui_popup_pos.y + 28 + ui_map_scr.sh) {
                    if(event.button.button != 1) {
                        ui_popup_page = -3;
                        ui_scroll.scry = &ui_map_scr.y;
                        ui_scroll.scrx = &ui_map_scr.x;
                        ui_scroll.origy = event.button.y - ui_map_scr.sy + ui_map_scr.y;
                        ui_scroll.origx = event.button.x - ui_map_scr.sx + ui_map_scr.x;
                        ui_map_picker(0, 0, ui_map_map);
                        return 1;
                    } else {
                        maps_scr2map(&ui_map_scr, ui_curx, ui_cury, &i, &j);
                        if(ui_map_map->val)
                            *(ui_map_map->val) = (project.mapidx && ui_popup_val >= 0 ? project.mapidx[ui_popup_val] :
                                ui_popup_val);
                        if(ui_map_map->crd)
                            *(ui_map_map->crd) = (i & 0x7fff) | ((j & 0x7fff) << 16);
                        return -1;
                    }
                }
            }
            ui_map_picker(0, 0, ui_map_map);
        break;
        case SDL_MOUSEBUTTONUP:
            ui_popup_page = 0;
            ui_map_picker(0, 0, ui_map_map);
        break;
        case SDL_MOUSEMOTION:
            if(ui_map_map->crd && ui_curbtn) {
                ui_vscrbar(popup, 4 + ui_map_scr.sw, 28, 12, ui_map_scr.sh, ui_map_scr.y, ui_map_scr.sh, ui_map_scr.h,
                    (ui_popup_page == -1 || ui_popup_page == -3));
                ui_hscrbar(popup, 4, 28 + ui_map_scr.sh, ui_map_scr.sw, 12, ui_map_scr.x, ui_map_scr.sw, ui_map_scr.w,
                    (ui_popup_page == -2 || ui_popup_page == -3));
                return 1;
            }
        break;
        case SDL_MOUSEWHEEL:
            ui_popup_page = 0;
            if(ui_cury >= ui_popup_pos.y + 32 && ui_cury < ui_popup_pos.y + ui_popup_pos.h - 16) {
                if(ui_curx > ui_popup_pos.x + ui_popup_pos.w - 204) {
                    if(event.wheel.y > 0 && ui_popup_val > (ui_map_map->crd ? 0 : -1)) ui_popup_val--;
                    if(event.wheel.y < 0 && ui_popup_val + 1 < ui_popup_max) ui_popup_val++;
                } else
                if(ui_map_map->crd && ui_curx >= ui_popup_pos.x + 4 && ui_curx < ui_popup_pos.x + ui_popup_pos.w - 4 - 204 - 12) {
                    if(event.wheel.y > 0) ui_map_scr.y -= ui_map_scr.z;
                    if(event.wheel.y < 0) ui_map_scr.y += ui_map_scr.z;
                    if(event.wheel.x > 0) ui_map_scr.x -= ui_map_scr.z;
                    if(event.wheel.x < 0) ui_map_scr.x -= ui_map_scr.z;
                    i = (ui_map_scr.h - ui_map_scr.sh) & ~(ui_map_scr.z - 1);
                    if(ui_map_scr.y > i) ui_map_scr.y = i;
                    if(ui_map_scr.y < 0) ui_map_scr.y = 0;
                    i = (ui_map_scr.w - ui_map_scr.sw) & ~(ui_map_scr.z - 1);
                    if(ui_map_scr.x > i) ui_map_scr.x = i;
                    if(ui_map_scr.x < 0) ui_map_scr.x = 0;
                }
                ui_map_picker(0, 0, ui_map_map);
            }
        break;
        case SDL_KEYDOWN:
            ui_popup_page = 0;
            switch(event.key.keysym.sym) {
                case SDLK_UP:
                    if(ui_popup_val > (ui_map_map->crd ? 0 : -1)) ui_popup_val--;
                break;
                case SDLK_DOWN:
                    if(ui_popup_val + 1 < ui_popup_max) ui_popup_val++;
                break;
                case SDLK_HOME: ui_popup_val = (ui_map_map->crd ? 0 : -1); break;
                case SDLK_END: ui_popup_val = ui_popup_max - 1; break;
            }
            ui_map_picker(0, 0, ui_map_map);
        break;
    }
    return 0;
}

/******************** coordinate picker ********************/

/**
 * Display the coordinate picker popup
 */
void ui_coord_picker(int x, int y, ui_coord_t *crd)
{
    if(ui_popup_type != FORM_COORD) {
        ui_coord_spr = NULL; ui_coord_x = ui_coord_y = NULL; ui_coord_w = ui_coord_h = 0;
        if(!crd || !crd->spr || crd->spr->val < 0 || crd->spr->val >= project.spritenum[crd->spr->cat] ||
          !project.sprites[crd->spr->cat] || !project.sprites[crd->spr->cat][crd->spr->val].dir[0].data || !crd->x || !crd->y) {
            ui_pressed = -1;
            return;
        }
        ui_coord_spr = spr_getidx(crd->spr->cat, crd->spr->val, -1, 0);
        if(ui_coord_spr) {
            ui_popup_type = FORM_COORD;
            spr_texture(ui_coord_spr, &ui_sprite_anims[0]);
            if(crd->w && *(crd->w) > 1) ui_coord_w = *(crd->w);
            if(crd->h && *(crd->h) > 1) ui_coord_h = *(crd->h);
            ui_coord_x = crd->x; ui_coord_y = crd->y;
            ui_popup(x, y, ui_coord_spr->w + 16, ui_coord_spr->h + 16, 3);
        }
    }
    if(!popup || !ui_coord_spr) return;
    ui_box(popup, 0, 0, ui_popup_pos.w, ui_popup_pos.h, theme[THEME_LIGHTER], theme[THEME_BG], theme[THEME_DARKER]);
}

/**
 * Render coordinate picker
 */
void ui_coord_render()
{
    SDL_Rect rect;

    if(ui_popup_type != FORM_COORD || !ui_coord_spr) return;
    rect.x = ui_popup_pos.x + 8; rect.y = ui_popup_pos.y + 8; rect.w = ui_popup_pos.w - 16; rect.h = ui_popup_pos.h - 16;
    spr_render(ui_coord_spr, -1, ui_sprite_anims[0], &rect);
    if(ui_coord_w < 2 || ui_coord_h < 2 || ui_curx < ui_popup_pos.x + 8 || ui_curx >= ui_popup_pos.x + ui_popup_pos.w - 8 ||
        ui_cury < ui_popup_pos.y + 8 || ui_cury >= ui_popup_pos.y + ui_popup_pos.h - 8) return;
    SDL_RenderSetClipRect(renderer, &rect);
    rect.w = ui_coord_w; rect.h = ui_coord_h; rect.x = ui_curx - (rect.w / 2); rect.y = ui_cury - (rect.h / 2);
    SDL_RenderCopy(renderer, spr_select, NULL, &rect);
    SDL_RenderSetClipRect(renderer, NULL);
}

/**
 * Coordinate picker controller
 */
int ui_coord_ctrl()
{
    int ret = ui_curx >= ui_popup_pos.x + 8 && ui_curx < ui_popup_pos.x + ui_popup_pos.w - 8 &&
        ui_cury >= ui_popup_pos.y + 8 && ui_cury < ui_popup_pos.y + ui_popup_pos.h - 8;

    if(event.type == SDL_MOUSEBUTTONUP && ret) {
        *ui_coord_x = ui_curx - ui_popup_pos.x - 8 - ui_coord_w / 2; if(*ui_coord_x < 0) *ui_coord_x = 0;
        *ui_coord_y = ui_cury - ui_popup_pos.y - 8 - ui_coord_h / 2; if(*ui_coord_y < 0) *ui_coord_y = 0;
        return -1;
    }
    return ret;
}

/**
 * Percentage coordinate picker helper, tricky, because percentage not necessarily on pixel border
 */
void ui_coord_helper(ui_sprsel_t *back, int *x1, int *y1, int *x2, int *y2)
{
    SDL_Event event;
    SDL_Texture *texture = NULL, *bg = NULL;
    SDL_Rect src, dst, sel;
    char tmp[32];
    int i, j, p, dx, dy, csr = CURSOR_PTR, last = CURSOR_PTR, btn = 0, s1x = -1, s1y = -1, s2x = -1, s2y = -1;
    uint32_t *pixels, f = ssfn_dst.fg, b = ssfn_dst.bg;
    ui_sprite_t *s = NULL;

    SDL_SetCursor(cursors[CURSOR_PTR]);

refresh:
    src.x = src.y = 0;
    if(back && back->val >= 0) {
        s = spr_getsel(back, 0);
        ui_fit(scr_w, scr_h, s->w, s->h, &dst.w, &dst.h);
        src.w = s->w; src.h = s->h;
        spr_texture(s, &bg);
    } else {
        dst.x = dst.y = 0;
        dst.w = src.w = scr_w; dst.h = src.h = scr_h;
    }
    /* take care of rounding errors */
    dx = dst.w / 100; if(dx < 1) dx = 1;
    dy = dst.h / 100; if(dy < 1) dy = 1;
    dst.w = dx * 100;
    dst.h = dy * 100;
    dst.x = (scr_w - dst.w) / 2; dst.y = (scr_h - dst.h) / 2;

    if(dx > 1 && dy > 1) {
        texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dst.w, dst.h);
        if(texture) {
            SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
            SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
            memset(pixels, 0, p * dst.h);
            for(j = 0; j < 100; j++)
                for(i = 0; i < 100; i++)
                    pixels[j * dy * p / 4 + i * dx] = 0xFF000000 | (theme[THEME_SELBX] & 0xFFFFFF);
            SDL_UnlockTexture(texture);
            ssfn_dst.fg = theme[THEME_SELBX];
            ssfn_dst.bg = theme[THEME_BG];
        }
    }

    while(1) {
        /* events */
        if(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
                SDL_PushEvent(&event); goto cleanup;
            }
            switch(event.type) {
                case SDL_KEYUP: goto cleanup; break;
                case SDL_MOUSEBUTTONDOWN:
                    if(event.button.x >= dst.x && event.button.x < dst.x + dst.w &&
                      event.button.y >= dst.y && event.button.y < dst.y + dst.h && event.button.button == 1) {
                        if(!x2 || !y2) {
                            if(x1) *x1 = s1x;
                            *y1 = s1y;
                            while(event.type != SDL_MOUSEBUTTONUP) SDL_WaitEvent(&event);
                            goto cleanup;
                        }
                        btn = 1;
                    } else
                        goto cleanup;
                break;
                case SDL_MOUSEBUTTONUP:
                    if(event.button.button == 1) {
                        if(s1x > s2x) { i = s1x; s1x = s2x; s2x = i; }
                        if(s1y > s2y) { i = s1y; s1y = s2y; s2y = i; }
                        *x1 = s1x;
                        *y1 = s1y;
                        *x2 = s2x + 1;
                        *y2 = s2y + 1;
                    }
                    goto cleanup;
                break;
                case SDL_MOUSEMOTION:
                    if(event.motion.x >= dst.x && event.motion.x < dst.x + dst.w &&
                      event.motion.y >= dst.y && event.motion.y < dst.y + dst.h) {
                        i = (event.motion.x - dst.x) / dx; if(i > 99) i = 99;
                        j = (event.motion.y - dst.y) / dy; if(j > 99) j = 99;
                        if(btn) { s2x = i; s2y = j; }
                        else { s1x = s2x = i; s1y = s2y = j; }
                        csr = CURSOR_CROSS;
                        if(texture && (!x2 || !y2)) {
                            sprintf(tmp, "%2d,%2d", i, j);
                            ui_text(texture, dst.w - 5 * 8, dst.h - 16, tmp);
                        }
                    } else
                        csr = CURSOR_PTR;
                    if(last != csr) { last = csr; SDL_SetCursor(cursors[csr]); }
                break;
                case SDL_WINDOWEVENT:
                    switch(event.window.event) {
                        case SDL_WINDOWEVENT_RESIZED:
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            if(event.window.data1 < SCR_MINW || event.window.data2 < SCR_MINH) {
                                if(event.window.data1 < SCR_MINW) event.window.data1 = SCR_MINW;
                                if(event.window.data2 < SCR_MINH) event.window.data2 = SCR_MINH;
                                SDL_SetWindowSize(window, event.window.data1, event.window.data2);
                            }
                            ui_resize(event.window.data1, event.window.data2);
                            if(texture) { SDL_DestroyTexture(texture); texture = NULL; }
                            goto refresh;
                        break;
                    }
                break;
            }
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        if(bg)
            SDL_RenderCopy(renderer, bg, &src, &dst);
        else {
            SDL_SetRenderDrawColor(renderer, theme[THEME_BG] & 0xff, (theme[THEME_BG] >> 8) & 0xff,
                (theme[THEME_BG] >> 16) & 0xff, 0);
            SDL_RenderFillRect(renderer, &dst);
        }
        if(spr_none && y1 && *y1) {
            if(x1) { sel.x = dst.x + *x1 * dx; sel.w = dx * (x2 ? *x2 - *x1 : 1); }
            else { sel.x = dst.x; sel.w = dst.w; }
            sel.y = dst.y + *y1 * dy; sel.h = dy * (y2 ? *y2 - *y1 : 1);
            SDL_RenderCopy(renderer, spr_none, NULL, &sel);
        }
        if(s1x != -1 && s1y != -1) {
            if(s1x <= s2x) { sel.x = s1x; sel.w = s2x - s1x + 1; } else { sel.x = s2x; sel.w = s1x - s2x + 1; }
            if(s1y <= s2y) { sel.y = s1y; sel.h = s2y - s1y + 1; } else { sel.y = s2y; sel.h = s1y - s2y + 1; }
            if(x1) { sel.x = dst.x + sel.x * dx; sel.w *= dx; } else { sel.x = dst.x; sel.w = dst.w; }
            sel.y = dst.y + sel.y * dy; sel.h *= dy;
            SDL_RenderCopy(renderer, spr_select, NULL, &sel);
        }
        if(texture) SDL_RenderCopy(renderer, texture, NULL, &dst);
        SDL_RenderPresent(renderer);
        SDL_Delay(10);
    }

cleanup:
    if(bg) SDL_DestroyTexture(bg);
    if(texture) SDL_DestroyTexture(texture);
    SDL_SetCursor(cursors[CURSOR_PTR]);
    ssfn_dst.fg = f; ssfn_dst.bg = b;
}

/******************** collision mask ********************/

/**
 * Mask setter click handler
 */
int ui_coll_click(ui_form_t *elem, int x, int y)
{
    ui_coll_t *coll;
    int i, j, k, l, m, n;

    if(!elem || !elem->param || x < 0 || y < 0) return 0;
    coll = elem->param;
    coll->fld = 0;
    if(y < 21) {
        if(x >= 1 && x < 21) {
            coll->fld = -3;
            if(ui_curbtn == 1) {
                if(coll->alpha < 3) coll->alpha++; else coll->alpha = 0;
            } else {
                if(coll->alpha > 0) coll->alpha--; else coll->alpha = 3;
            }
        }
        if(x >= 21 && x < 41) { coll->evt ^= 1; }
    } else
    if(x > elem->w - 13 && y > 21 && y < elem->h - 13) {
        coll->fld = -1;
        if(coll->mh > 0) {
            k = elem->h - 21 - 13;
            ui_scroll.scry = &coll->scry;
            ui_scroll.posy = y - (21 + (k - 20) * coll->scry / coll->mh);
            ui_scroll.pagey = k;
            ui_scroll.numy = coll->mh;
            ui_scroll.srty = elem->y + 21;
            ui_scroll.sizey = k - 20;
            ui_scroll.min = 0;
        }
        return 1;
    } else
    if(y >= elem->h - 13 && x < elem->w - 13) {
        coll->fld = -2;
        if(coll->mw > 0) {
            k = elem->w - 1 - 13;
            ui_scroll.scrx = &coll->scrx;
            ui_scroll.posx = x - (1 + (k - 20) * coll->scrx / coll->mw);
            ui_scroll.pagex = k;
            ui_scroll.numx = coll->mw;
            ui_scroll.srtx = elem->x + 1;
            ui_scroll.sizex = k - 20;
            ui_scroll.min = 0;
        }
        return 1;
    } else
    if(coll->mask.data) {
        /* get clicked mask's index in i */
        i = -1;
        switch(project.type) {
            case TNG_TYPE_ORTHO:
                i = ((y - 21 + coll->scry) / project.tileh) * coll->mask.w + ((x - 1 + coll->scrx) / project.tilew);
            break;
            case TNG_TYPE_ISO:
                j = (y - 21 + coll->scry) / project.tileh; l = (y - 21 + coll->scry) % project.tileh;
                k = (x - 1 + coll->scrx) / project.tilew; m = (x - 1 + coll->scrx) % project.tilew;
                i = j * 2 * coll->mask.w + k;
                if(l < project.tileh / 2) {
                    n = project.tilew / 2 - 1 - (project.tilew / 2 * l / (project.tileh / 2));
                    if(m < n) { if(!k) i = -1; else i -= coll->mask.w + 1; }
                    if(m >= project.tilew - n) { if(k + 1>= coll->mask.w) i = -1; else i -= coll->mask.w; }
                } else {
                    n = project.tilew / 2 - 1 - (project.tilew / 2 * (project.tileh - l - 1) / (project.tileh / 2));
                    if(m < n) { if(!k) i = -1; else i += coll->mask.w - 1; }
                    if(m >= project.tilew - n) { if(k + 1 >= coll->mask.w) i = -1; else i += coll->mask.w; }
                }
            break;
            case TNG_TYPE_HEXV:
            case TNG_TYPE_HEXH:
                /* TODO: hexagonal grids */
            break;
        }
        /* set the mask */
        if(i >= 0 && i < coll->mask.w * coll->mask.h) {
            if(coll->evt)
                coll->mask.data[i] ^= 8;
            else {
                if(ui_curbtn == 1) {
                    if((coll->mask.data[i] & 7) < 7)
                        coll->mask.data[i]++;
                    else
                        coll->mask.data[i] &= ~7;
                } else {
                    if((coll->mask.data[i] & 7) > 0)
                        coll->mask.data[i]--;
                    else
                        coll->mask.data[i] = (coll->mask.data[i] & 8) + 7;
                }
            }
        }
    }
    return 0;
}

/**
 * Resize the mask setter
 */
void ui_coll_resize(ui_coll_t *coll, int w, int h)
{
    if(!coll || w < 15 || h < 35) return;
    w -= 14; h -= 34;
    if(w < coll->mw) coll->scrx = (coll->mw - w) / 2;
    if(h < coll->mh) coll->scry = coll->mh - h;
}

/**
 * Display the mask setter
 */
void ui_coll(SDL_Texture *dst, int x, int y, int w, int h, ui_coll_t *coll, int pressed)
{
    int i, j, k, l, m, X, Y, p, x2 = x + w - 13, y2 = y + h - 13;
    uint32_t *pixels, b = ui_input_bg, B = (b & 0xFF000000) | ((b + 0x080808) & 0x7F7F7F);

    if(!dst || !coll || w < 1 || h < 1) return;
    if(pressed == -1) {
        ui_box(dst, x, y, w, h, theme[THEME_INA], theme[THEME_INA], theme[THEME_INA]);
        return;
    }
    spr_texture(spr_getidx(coll->spr->cat, coll->spr->val, 0, 0), &ui_sprite_anims[8]);
    ui_box(dst, x, y, w, h, theme[THEME_DARKER], theme[THEME_BG], theme[THEME_LIGHTER]);
    ui_iconbtn(dst, x + 1, y + 1, ICON_VISIB, pressed && coll->fld == -3 ? 1 : 0);
    ui_iconbtn(dst, x + 21, y + 1, ICON_EVENT, coll->evt);
    ui_rect(dst, x + 41, y + 1, w - 42, 20, theme[THEME_LIGHTER], theme[THEME_DARKER]);
    ui_text2(dst, x + 45, y + 2, lang[LANG_COLLMASK], theme[THEME_DARKER], theme[THEME_LIGHTER]);
    if(coll->spr && coll->spr->val >= 0 && project.sprites[coll->spr->cat]) {
        X = project.sprites[coll->spr->cat][coll->spr->val].dir[0].w;
        Y = project.sprites[coll->spr->cat][coll->spr->val].dir[0].h;
        maps_sprite2tilewh(X, Y, &coll->mask.w, &coll->mask.h);
    } else
        coll->mask.w = coll->mask.h = 0;
    switch(project.type) {
        case TNG_TYPE_ORTHO:
            coll->mw = coll->mask.w * project.tilew;
            coll->mh = coll->mask.h * project.tileh;
        break;
        case TNG_TYPE_ISO:
            coll->mw = coll->mask.w * project.tilew;
            coll->mh = coll->mask.h * project.tileh / 2 + (coll->mask.h & 1 ? project.tileh / 2 : 0);
        break;
        case TNG_TYPE_HEXH:
        case TNG_TYPE_HEXV:
        break;
    }
    if(coll->scrx < 0) ui_coll_resize(coll, w, h);
    if(!coll->mask.data && coll->mask.w > 0 && coll->mask.h > 0) {
        coll->mask.data = (char*)main_alloc(coll->mask.w * coll->mask.h);
        coll->mask.data[(coll->mask.h - 1) * coll->mask.w + coll->mask.w / 2] = 9;
        ui_coll_resize(coll, w, h);
    }
    ui_vscrbar(dst, x + w - 13, y + 21, 12, h - 34, coll->scry, h - 34, coll->mh, pressed && coll->fld == -1);
    ui_hscrbar(dst, x + 1, y + h - 13, w - 14, 12, coll->scrx, w - 14, coll->mw, pressed && coll->fld == -2);
    if(dst == screen) { p = scr_p; pixels = scr_data; }
    else SDL_LockTexture(dst, NULL, (void**)&pixels, &p);
    x++; y += 21; p /= 4;
    switch(project.type) {
        case TNG_TYPE_ORTHO:
            for(Y = y, j = coll->scry; j < coll->mh && Y < y2; j++, Y++)
                for(X = x, i = coll->scrx; i < coll->mw && X < x2; i++, X++)
                    pixels[Y * p + X] = (((j / project.tileh) & 1) ^ ((i / project.tilew) & 1) ? b : B);
        break;
        case TNG_TYPE_ISO:
            for(Y = y, j = coll->scry; j < coll->mh && Y < y2; j++, Y++)
                for(X = x, i = coll->scrx; i < coll->mw && X < x2; i++, X++) {
                    l = i % project.tilew; m = j % project.tileh;
                    k = project.tilew / 2 - 1 - (project.tilew / 2 * (m < project.tileh / 2 ? m : project.tileh - m -1) /
                        (project.tileh / 2));
                    pixels[Y * p + X] = (l < k || l >= project.tilew - k ? b : B);
                }
        break;
        case TNG_TYPE_HEXH:
        case TNG_TYPE_HEXV:
        break;
    }
    if(dst != screen) SDL_UnlockTexture(dst);
}

/**
 * Render mask setter
 */
void ui_coll_render(int x, int y, int w, int h, ui_coll_t *coll)
{
    SDL_Rect src, dst;
    ui_sprite_t *s;
    int i, j, k;

    if(!coll || !coll->spr || !ui_sprite_anims[8] || w < 1 || h < 1) return;
    s = spr_getidx(coll->spr->cat, coll->spr->val, 0, 0);
    if(!s) return;
    ui_clip.x = x + 1; ui_clip.w = w - 2 - 12; ui_clip.y = y + 21; ui_clip.h = h - 23 - 12;
    SDL_RenderSetClipRect(renderer, &ui_clip);

    src.x = ui_anim(s->type, s->nframe, -1) * s->w; src.y = 0; src.w = dst.w = s->w; src.h = dst.h = s->h;
    dst.x = x + 1 - coll->scrx + (coll->mw - dst.w) / 2;
    dst.y = y + 21 - coll->scry + coll->mh - dst.h;
    SDL_SetTextureAlphaMod(ui_sprite_anims[8], (coll->alpha + 1) * 64 - 1);
    SDL_RenderCopy(renderer, ui_sprite_anims[8], &src, &dst);

    src.w = src.h = dst.w = dst.h = 16;
    if(coll->mask.data)
        switch(project.type) {
            case TNG_TYPE_ORTHO:
                dst.y = y + 21 - coll->scry + project.tileh / 2 - 8;
                for(j = 0; j < coll->mask.h; j++, dst.y += project.tileh) {
                    dst.x = x + 1 - coll->scrx  + project.tilew / 2 - 8;
                    for(i = 0; i < coll->mask.w; i++, dst.x += project.tilew) {
                        k = coll->mask.data[j * coll->mask.w + i];
                        if(k & 7) {
                            /* make the icon checker happy: ICON_BLOCK, ICON_SWIM, ICON_FLY, ICON_CLIMB, ICON_JUMP, ICON_ABOVE, ICON_BELOW */
                            src.x = (((ICON_WALK + (k & 7)) & 7) << 4);
                            src.y = (((ICON_WALK + (k & 7)) & ~7) << 1);
                            SDL_RenderCopy(renderer, icons, &src, &dst);
                        }
                        if(k & 8) {
                            src.x = ((ICON_EVENT & 7) << 4);
                            src.y = ((ICON_EVENT & ~7) << 1);
                            if(!coll->evt) SDL_SetTextureAlphaMod(icons, 95);
                            SDL_RenderCopy(renderer, icons, &src, &dst);
                            SDL_SetTextureAlphaMod(icons, 255);
                        }
                    }
                }
            break;
            case TNG_TYPE_ISO:
                dst.y = y + 21 - coll->scry + project.tileh / 2 - 8;
                for(j = 0; j < coll->mask.h; j++, dst.y += project.tileh / 2) {
                    dst.x = x + 1 - coll->scrx + project.tilew / 2 - 8 + (j & 1 ? project.tilew / 2 : 0);
                    for(i = 0; i < coll->mask.w - (j & 1); i++, dst.x += project.tilew) {
                        k = coll->mask.data[j * coll->mask.w + i];
                        if(k & 7) {
                            src.x = (((ICON_WALK + (k & 7)) & 7) << 4);
                            src.y = (((ICON_WALK + (k & 7)) & ~7) << 1);
                            SDL_RenderCopy(renderer, icons, &src, &dst);
                        }
                        if(k & 8) {
                            src.x = ((ICON_EVENT & 7) << 4);
                            src.y = ((ICON_EVENT & ~7) << 1);
                            if(!coll->evt) SDL_SetTextureAlphaMod(icons, 95);
                            SDL_RenderCopy(renderer, icons, &src, &dst);
                            SDL_SetTextureAlphaMod(icons, 255);
                        }
                    }
                }
            break;
            case TNG_TYPE_HEXH:
            case TNG_TYPE_HEXV:
            break;
        }
    ui_clip.x = ui_clip.y = 0; ui_clip.w = scr_w; ui_clip.h = scr_h;
    SDL_RenderSetClipRect(renderer, NULL);
}

/******************** fonts ********************/

/**
 * Create a font context
 */
void ui_font_load(ui_font_t *fnt, int style, int size, int family)
{
    FILE *f;
    int s;

    if(!fnt) return;
    memset(fnt, 0, sizeof(ui_font_t));
    if(family < 0) {
        /* default font */
        ssfn_load(&fnt->ctx, binary_default_sfn);
    } else {
        /* load font file */
        sprintf(projfn, "%s" SEP "%s" SEP "%s.sfn", project.id, project_dirs[PROJDIRS_FONTS], project.fonts[family]);
        f = project_fopen(projdir, "rb");
        if(f) {
            fseek(f, 0, SEEK_END);
            s = (int)ftell(f);
            fseek(f, 0, SEEK_SET);
            fnt->buf = (uint8_t*)malloc(s + 1);
            if(fnt->buf) {
                memset(fnt->buf, 0, s + 1);
                if(fread(fnt->buf, 1, s, f))
                    ssfn_load(&fnt->ctx, fnt->buf);
            }
            fclose(f);
        }
    }
    /* add unifont as fallback for undefined glyphs */
    ssfn_load(&fnt->ctx, ssfn_src);
    /* select drawing style */
    ssfn_select(&fnt->ctx, SSFN_FAMILY_ANY, NULL, style | SSFN_STYLE_A, size);
}

/**
 * Free a font context
 */
void ui_font_free(ui_font_t *fnt)
{
    if(!fnt) return;
    ssfn_free(&fnt->ctx);
    if(fnt->buf) free(fnt->buf);
    memset(fnt, 0, sizeof(ui_font_t));
}

/******************** cutscene editor ********************/

/**
 * Display the cutscene editor
 */
void ui_cutscn(SDL_Texture *dst, int x, int y, int w, int h, ui_cutscn_t *cutscn, int pressed)
{
    char tmp[16];
    int i, j, k, l, p, t, s, e;
    uint32_t *pixels, b, c, B = 0xFF000000 | ((ui_input_bg + 0x040404) & 0x7F7F7F);
    uint32_t C = 0xFF000000 | ((theme[THEME_SELBX] >> 2) & 0x3F3F3F);
    uint32_t S = 0xFF000000 | ((C + 0x040404) & 0x7F7F7F);
    ui_sprite_t *spr;

    if(!dst || !cutscn || w < 1 || h < 1) return;
    if(pressed == -1) {
        ui_box(dst, x, y, w, h, theme[THEME_INA], theme[THEME_INA], theme[THEME_INA]);
        return;
    }
    ui_box(dst, x + 1, y + 11, 31, 48, theme[cutscn->channel == 0 ? THEME_DARK : THEME_LIGHT], theme[THEME_BG],
        theme[cutscn->channel == 0 ? THEME_LIGHT : THEME_DARK]);
    ui_icon(dst, x + 8, y + 11 + 24 - 17 + (cutscn->channel == 0 ? 1 : 0), ICON_MOV);
    ui_icon(dst, x + 8, y + 11 + 24 + 1 + (cutscn->channel == 0 ? 1 : 0), ICON_MUS);
    ui_box(dst, x + 1, y + 11 + 48, 31, 48, theme[cutscn->channel == 1 ? THEME_DARK : THEME_LIGHT], theme[THEME_BG],
        theme[cutscn->channel == 1 ? THEME_LIGHT : THEME_DARK]);
    ui_icon(dst, x + 8, y + 11 + 48 + 24 - 8 + (cutscn->channel == 1 ? 1 : 0), ICON_BACKGR);
    ui_box(dst, x + 1, y + 11 + 48 + 48, 31, 24, theme[cutscn->channel == 2 ? THEME_DARK : THEME_LIGHT], theme[THEME_BG],
        theme[cutscn->channel == 2 ? THEME_LIGHT : THEME_DARK]);
    ui_icon(dst, x + 8, y + 11 + 48 + 48 + 12 - 8 + (cutscn->channel == 2 ? 1 : 0), ICON_SPCH);
    ui_box(dst, x + 1, y + 11 + 48 + 48 + 24, 31, 24, theme[cutscn->channel == 3 ? THEME_DARK : THEME_LIGHT], theme[THEME_BG],
        theme[cutscn->channel == 3 ? THEME_LIGHT : THEME_DARK]);
    ui_icon(dst, x + 8, y + 11 + 48 + 48 + 24 + 12 - 8 + (cutscn->channel == 3 ? 1 : 0), ICON_FONT);
    ui_box(dst, x + 1, y + 11 + 48 + 48 + 48, 31, 24, theme[cutscn->channel == 4 ? THEME_DARK : THEME_LIGHT], theme[THEME_BG],
        theme[cutscn->channel == 4 ? THEME_LIGHT : THEME_DARK]);
    ui_icon(dst, x + 8, y + 11 + 48 + 48 + 48 + 12 - 8 + (cutscn->channel == 4 ? 1 : 0), ICON_SCRIPT);
    ui_hscrbar(dst, x + 32, y + 11 + 48 + 48 + 24 + 24 + 24, w - 33, 12, cutscn->scr, w - 33, cutscn->len, pressed);
    ui_rect(dst, x, y + 10, w, h - 10, theme[THEME_DARKER], theme[THEME_LIGHTER]);
    ui_box(dst, x, y, w + 2, 10, theme[THEME_BG], theme[THEME_BG], theme[THEME_BG]);
    if(dst == screen) { p = scr_p; pixels = scr_data; }
    else SDL_LockTexture(dst, NULL, (void**)&pixels, &p);
    p /= 4; j = (y + 5) * p + x + 32;
    for(i = 0; i < w - 33 && cutscn->scr + i < cutscn->len; i++)
        if(!((cutscn->scr + i) % 10)) {
            if(!((cutscn->scr + i) % 100)) {
                pixels[j + i] = pixels[j + p + i] = pixels[j + p + p + i] = theme[THEME_DARKER];
                pixels[j + p + i + 1] = pixels[j + p + p + i + 1] = pixels[j + p + p + p + i + 1] = theme[THEME_LIGHT];
            }
            pixels[j + p + p + p + i] = pixels[j + p + p + p + p + i] = theme[THEME_DARKER];
            pixels[j + p + p + p + p + i + 1] = theme[THEME_LIGHT];
        }
    s = cutscn->sels - cutscn->scr; e = cutscn->sele - cutscn->scr;
    if(s > e) { t = s; s = e; e = t; }
    t = (cutscn->bglen[0] ? cutscn->bglen[0] - cutscn->scr + 1 : w);
    k = (cutscn->bglen[1] ? cutscn->bglen[1] - cutscn->scr + 1 : w);
    for(l = 0, j = (y + 11) * p + x + 32; l < 48 + 48 + 24 + 24 + 24; l++, j += p) {
        if(l < 48 || (l >= 48 + 48 && l < 48 + 48 + 24) || (l >= 48 + 48 + 48 && l < 48 + 48 + 48 + 24)) {
            b = ui_input_bg; c = C;
        } else {
            b = B; c = S;
        }
        if(l == 24 || l == 48 + 24 || l == 48 + 48 + 12 || l == 48 + 48 + 24 + 12 || l == 48 + 48 + 24 + 24 + 12) {
            b = theme[THEME_LIGHT]; c = theme[THEME_SELBX];
        }
        for(i = 0; i < w - 33 && cutscn->scr + i < cutscn->len; i++)
            pixels[j + i] = (i == t || i == k ? (s <= i && e > i ? b : B) : (s <= i && e > i ? c : b));
    }
    /* draw wave pattern */
    if(cutscn->smp)
        for(i = 0, t = cutscn->scr * 2; i < w - 33 && cutscn->scr + i < cutscn->len; i++, t += 2) {
            j = (y + 11 + 24 + cutscn->smp[t + 0]) * p + x + 32 + i;
            b = (s <= i && e > i ? theme[THEME_SELBX] : theme[THEME_LIGHT]);
            for(l = cutscn->smp[t + 0]; l <= cutscn->smp[t + 1]; l++, j += p)
                pixels[j] = b;
        }
    /* draw slideshow items */
    if(cutscn->img) {
        for(k = 0; k < cutscn->numimg; k++)
            if(((cutscn->channel == 1 && k == cutscn->seli) || cutscn->img[k].img >= 0 || cutscn->img[k].color) &&
              ((cutscn->img[k].s >= cutscn->scr && cutscn->img[k].s < cutscn->scr + w - 33) ||
              (cutscn->img[k].e >= cutscn->scr && cutscn->img[k].e < cutscn->scr + w - 33) ||
              (cutscn->img[k].s < cutscn->scr && cutscn->img[k].e >= cutscn->scr + w - 33))) {
                for(i = (cutscn->img[k].s < cutscn->scr ? cutscn->scr : cutscn->img[k].s);
                    i < cutscn->scr + w - 33 && i < cutscn->img[k].e; i++) {
                        t = (i < cutscn->img[k].fi ? ((i - cutscn->img[k].s) * 23 / (cutscn->img[k].fi - cutscn->img[k].s)) :
                            (i > cutscn->img[k].fo ? (23 - (i - cutscn->img[k].fo) * 23 / (cutscn->img[k].e - cutscn->img[k].fo))
                            : 23));
                        j = (y + 11 + 48 + 24 - t) * p + x + 32 + i - cutscn->scr;
                        b = (s <= i - cutscn->scr && e > i - cutscn->scr ? theme[THEME_SELBX] : theme[THEME_LIGHT]);
                        if(i > cutscn->img[k].s && i <= cutscn->img[k].fi)
                            for(l = 0; l < t - ((i - 1 - cutscn->img[k].s) * 23 / (cutscn->img[k].fi - cutscn->img[k].s)); l++)
                                pixels[j + l * p - 1] = pixels[j + (2 * t - l) * p - 1] =
                                    (s <= i - cutscn->scr - 1 && e > i - cutscn->scr - 1 ? S : B);
                        if(i == cutscn->img[k].s && cutscn->img[k].s == cutscn->img[k].fi)
                            b = (s <= i - cutscn->scr - 1 && e > i - cutscn->scr - 1 ? S : B);
                        for(l = -t; l <= t; l++, j += p)
                            pixels[j] = b;
                }
                t = ui_clip.w; l = ui_clip.x;
                if(cutscn->img[k].color) {
                    ui_clip.x = x + 32 + cutscn->img[k].fi - cutscn->scr; ui_clip.w = cutscn->img[k].fo - cutscn->img[k].fi;
                    ui_combine(pixels, ui_clip.x, y + 11 + 50, 45, 45, p * 4, &cutscn->img[k].color, 0, 0, 1, 1, 4);
                } else
                if(cutscn->img[k].img >= 0) {
                    spr = spr_getidx(5, cutscn->img[k].img, 0, 1);
                    if(!cutscn->img[k].data) {
                        ui_fit(w, 45, spr->w, spr->h, &cutscn->img[k].w, &cutscn->img[k].h);
                        cutscn->img[k].data = (uint32_t*)main_alloc(cutscn->img[k].w * 4 * cutscn->img[k].h);
                        ui_clip.x = 0; ui_clip.w = cutscn->img[k].w;
                        ui_resample(cutscn->img[k].data, 0, 0, cutscn->img[k].w, cutscn->img[k].h, cutscn->img[k].w * 4, 255,
                            spr->data, spr->w, spr->h, spr->w * spr->nframe * 4);
                    }
                    ui_clip.x = x + 32 + cutscn->img[k].fi - cutscn->scr; ui_clip.w = cutscn->img[k].fo - cutscn->img[k].fi;
                    ui_combine(pixels, x + 32 + cutscn->img[k].fi - cutscn->scr, y + 11 + 50, cutscn->img[k].w, cutscn->img[k].h,
                        p * 4, cutscn->img[k].data, 0, 0, cutscn->img[k].w, cutscn->img[k].h, cutscn->img[k].w * 4);
                }
                ui_clip.w = t; ui_clip.x = l;
            }
    }
    ssfn_dst.fg = theme[THEME_DARKER];
    /* draw speech records */
    if(cutscn->spc) {
        for(k = 0; k < cutscn->numspc; k++)
            if(((cutscn->channel == 2 && k == cutscn->seli) || cutscn->spc[k].spc >= 0) &&
              ((cutscn->spc[k].s >= cutscn->scr && cutscn->spc[k].s < cutscn->scr + w - 33) ||
              (cutscn->spc[k].s + cutscn->spc[k].e >= cutscn->scr && cutscn->spc[k].s + cutscn->spc[k].e < cutscn->scr + w - 33) ||
              (cutscn->spc[k].s < cutscn->scr && cutscn->spc[k].s + cutscn->spc[k].e >= cutscn->scr + w - 33))) {
                for(i = (cutscn->spc[k].s < cutscn->scr ? 0 : cutscn->spc[k].s - cutscn->scr);
                    i < w - 33 && i < cutscn->spc[k].s - cutscn->scr + cutscn->spc[k].e && i < cutscn->len - cutscn->scr; i++) {
                    b = (s <= i && e > i ? theme[THEME_SELBX] : theme[THEME_LIGHT]);
                    for(l = 0, j = (y + 11 + 48 + 48 + 2) * p + x + 32 + i; l < 20; l++, j += p)
                        pixels[j] = b;
                }
                if(cutscn->spc[k].s + cutscn->spc[k].e > cutscn->len)
                    ui_icon(screen, x + 16 + i, y + 11 + 48 + 48 + 4, ICON_WARN);
                if(cutscn->spc[k].spc >= 0) {
                    t = ui_clip.w; l = ui_clip.x; ui_clip.x = x + 30; ui_clip.w = i;
                    ui_text(screen, x + 34 + cutscn->spc[k].s - cutscn->scr, y + 11 + 48 + 48 + 4,
                        project.speech[cutscn->spc[k].spc]);
                    ui_clip.w = t; ui_clip.x = l;
                }
            }
    }
    /* draw subtitles */
    if(cutscn->sub) {
        for(k = 0; k < cutscn->numsub; k++)
            if(((cutscn->channel == 3 && k == cutscn->seli) || cutscn->sub[k].txt[0]) &&
              ((cutscn->sub[k].s >= cutscn->scr && cutscn->sub[k].s < cutscn->scr + w - 33) ||
              (cutscn->sub[k].e >= cutscn->scr && cutscn->sub[k].e < cutscn->scr + w - 33) ||
              (cutscn->sub[k].s < cutscn->scr && cutscn->sub[k].e >= cutscn->scr + w - 33))) {
                for(i = (cutscn->sub[k].s < cutscn->scr ? 0 : cutscn->sub[k].s - cutscn->scr);
                    i < w - 33 && i < cutscn->sub[k].e - cutscn->scr; i++) {
                    b = (s <= i && e > i ? theme[THEME_SELBX] : theme[THEME_LIGHT]);
                    for(l = 0, j = (y + 11 + 48 + 48 + 24 + 2) * p + x + 32 + i; l < 20; l++, j += p)
                        pixels[j] = b;
                }
                if(cutscn->sub[k].txt[0]) {
                    t = ui_clip.w; l = ui_clip.x; ui_clip.x = x + 30; ui_clip.w = cutscn->sub[k].e - cutscn->scr;
                    ui_text(screen, x + 34 + cutscn->sub[k].s - cutscn->scr, y + 11 + 48 + 48 + 24 + 4, cutscn->sub[k].txt);
                    ui_clip.w = t; ui_clip.x = l;
                }
            }
    }
    /* draw scripts */
    if(cutscn->cmd.root.len && cutscn->cmd.root.children) {
        k = (cutscn->cmd.root.children[0].type == CMD_DELAY - CMD_NOP ? cutscn->cmd.root.children[0].arg3 : 0);
        for(i = (k < cutscn->scr ? 0 : k - cutscn->scr);
            i < w - 33 && i < cutscn->len - cutscn->scr; i++) {
            b = (s <= i && e > i ? theme[THEME_SELBX] : theme[THEME_LIGHT]);
            for(l = 0, j = (y + 11 + 48 + 48 + 48 + 2) * p + x + 32 + i; l < 20; l++, j += p)
                pixels[j] = b;
        }
    }
    if(dst != screen) SDL_UnlockTexture(dst);
    ssfn_dst.fg = theme[THEME_FG];
    sprintf(tmp, "%02u:%02u.%02u", cutscn->scr / 6000, (cutscn->scr / 100) % 60, cutscn->scr % 100);
    ui_number(dst, x + 4, y, tmp, theme[THEME_FG]);
    i = cutscn->scr + w - 33;
    if(i > cutscn->len) i = cutscn->len;
    sprintf(tmp, "%02u:%02u.%02u", i / 6000, (i / 100) % 60, i % 100);
    i -= cutscn->scr;
    ui_number(dst, x + 4 + i, y, tmp, theme[THEME_FG]);
}

/**
 * Display the cutscene's selection
 */
void ui_cutscn_sel(SDL_Texture *dst, int x, int y, int w, ui_cutscn_t *cutscn)
{
    char tmp[24];
    int t, s, e;

    if(!cutscn) return;
    s = cutscn->sels; e = cutscn->sele;
    if(s > e) { t = s; s = e; e = t; }
    sprintf(tmp, "%02u:%02u.%02u - %02u:%02u.%02u", s / 6000, (s / 100) % 60, s % 100, e / 6000, (e / 100) % 60, e % 100);
    t = ui_clip.w;
    ui_clip.w = x + w - ui_clip.x;
    ui_text(dst, x, y, tmp);
    ui_clip.w = t;
}

/**
 * Render position
 */
void ui_cutscn_render(int x, int y, int w, int h, ui_cutscn_t *cutscn)
{
    SDL_Rect dst;
    uint32_t *data;
    int i, p;
    char tmp[16];

    if(!cutscn || !pos || w < 1 || h < 1 || ui_curx < x + 32 || ui_curx >= x + w - 1 || ui_cury < y + 11 ||
      ui_cury >= y + h - 13 || (i = cutscn->scr + ui_curx - x - 32) > cutscn->len)
        return;
    SDL_LockTexture(pos, NULL, (void**)&data, &p);
    memset(data, 0, p * 8);
    data[29 + 6 * p / 4] = data[29 + 7 * p / 4] = data[29 + 8 * p / 4] = data[29 + 9 * p / 4] = theme[THEME_FG];
    SDL_UnlockTexture(pos);
    sprintf(tmp, "%02u:%02u.%02u", i / 6000, (i / 100) % 60, i % 100);
    ui_number(pos, 0, 0, tmp, theme[THEME_FG]);
    dst.x = ui_curx - 28; dst.y = y; dst.w = 32; dst.h = 10;
    SDL_RenderCopy(renderer, pos, NULL, &dst);
}

/**
 * Cutscene editor mouse press handler
 */
int ui_cutscn_click(ui_cutscn_t *cutscn, int x, int y)
{
    if(!cutscn) return 0;
    cutscn->clkx = x - 31;
    if(ui_curbtn != 1) {
        cutscn->scrx = cutscn->scr;
        cutscn->clk = 2;
    } else {
        cutscn->seli = -1;
        if(y >= 0 && y < 48) cutscn->channel = 0; else
        if(y >= 48 && y < 48 + 48) cutscn->channel = 1; else
        if(y >= 48 + 48 && y < 48 + 48 + 24) cutscn->channel = 2; else
        if(y >= 48 + 48 + 24 && y < 48 + 48 + 48) cutscn->channel = 3; else
        if(y >= 48 + 48 + 48 && y < 48 + 48 + 48 + 24) cutscn->channel = 4;
        cutscn->clk = 1;
        if(x >= 31)
            cutscn->sels = cutscn->sele = 0;
        else {
            ui_cutscn_release(cutscn, x, y);
            cutscn->clk = 1;
        }
    }
    return 1;
}

/**
 * Cutscene editor mouse release handler
 */
int ui_cutscn_release(ui_cutscn_t *cutscn, int x, int y)
{
    int i;

    if(!cutscn) return 0;
    (void)x; (void)y;
    cutscn->seli = -1;
    if(cutscn->clk == 1) {
        if(cutscn->sels > cutscn->sele) { i = cutscn->sels; cutscn->sels = cutscn->sele; cutscn->sele = i; }
        if(cutscn->sele > cutscn->len) cutscn->sele = cutscn->len;
        if(cutscn->sels < 0) cutscn->sels = 0;
        switch(cutscn->channel) {
            case 1:
                if(cutscn->sele) {
                    /* look for exact match */
                    for(i = 0; i < cutscn->numimg && (cutscn->img[i].s != cutscn->sels || cutscn->img[i].e != cutscn->sele); i++);
                    /* if not found, look for empty slot */
                    if(i >= cutscn->numimg)
                        for(i = 0; i < cutscn->numimg && (cutscn->img[i].img != -1 || cutscn->img[i].color); i++);
                    /* if neither, add a new slot */
                    if(i >= cutscn->numimg) {
                        cutscn->numimg++;
                        cutscn->img = (ui_slide_t*)realloc(cutscn->img, cutscn->numimg * sizeof(ui_slide_t));
                        if(!cutscn->img) { cutscn->numimg = 0; break; }
                        memset(&cutscn->img[i], 0, sizeof(ui_slide_t));
                        cutscn->img[i].img = -1;
                    }
                    cutscn->img[i].s = cutscn->img[i].fi = cutscn->sels;
                    cutscn->img[i].e = cutscn->img[i].fo = cutscn->sele;
                    cutscn->seli = i;
                } else {
                    for(i = 0; i < cutscn->numimg &&
                      !(cutscn->img[i].s <= cutscn->clkx + cutscn->scr && cutscn->img[i].e >= cutscn->clkx + cutscn->scr); i++);
                    if(i < cutscn->numimg) {
                        cutscn->sels = cutscn->img[i].s;
                        cutscn->sele = cutscn->img[i].e;
                        cutscn->seli = i;
                    }
                }
            break;
            case 2:
                if(cutscn->sele) {
                    for(i = 0; i < cutscn->numspc && cutscn->spc[i].s != cutscn->sels; i++);
                    if(i >= cutscn->numspc)
                        for(i = 0; i < cutscn->numspc && cutscn->spc[i].spc != -1; i++);
                    if(i >= cutscn->numspc) {
                        cutscn->numspc++;
                        cutscn->spc = (ui_speech_t*)realloc(cutscn->spc, cutscn->numspc * sizeof(ui_speech_t));
                        if(!cutscn->spc) { cutscn->numspc = 0; break; }
                        cutscn->spc[i].spc = -1;
                    }
                    cutscn->spc[i].s = cutscn->sels;
                    if(cutscn->spc[i].spc == -1) cutscn->spc[i].e = cutscn->sele - cutscn->sels;
                    else cutscn->sele = cutscn->spc[i].s + cutscn->spc[i].e;
                    cutscn->seli = i;
                } else {
                    for(i = 0; i < cutscn->numspc && !(cutscn->spc[i].s <= cutscn->clkx + cutscn->scr &&
                        cutscn->spc[i].s + cutscn->spc[i].e >= cutscn->clkx + cutscn->scr); i++);
                    if(i < cutscn->numspc) {
                        cutscn->sels = cutscn->spc[i].s;
                        cutscn->sele = cutscn->spc[i].s + cutscn->spc[i].e;
                        cutscn->seli = i;
                    }
                }
            break;
            case 3:
                if(cutscn->sele) {
                    for(i = 0; i < cutscn->numsub && (cutscn->sub[i].s != cutscn->sels || cutscn->sub[i].e != cutscn->sele); i++);
                    if(i >= cutscn->numsub)
                        for(i = 0; i < cutscn->numsub && cutscn->sub[i].txt[0]; i++);
                    if(i >= cutscn->numsub) {
                        cutscn->numsub++;
                        cutscn->sub = (ui_sub_t*)realloc(cutscn->sub, cutscn->numsub * sizeof(ui_sub_t));
                        if(!cutscn->sub) { cutscn->numsub = 0; break; }
                        memset(&cutscn->sub[i], 0, sizeof(ui_sub_t));
                        cutscn->sub[i].x = 50;
                        cutscn->sub[i].y = 97;
                        cutscn->sub[i].a = 1;
                        cutscn->sub[i].color = 0xFFE0E0E0;
                        cutscn->sub[i].size = 16;
                        cutscn->sub[i].font = -1;
                    }
                    cutscn->sub[i].s = cutscn->sub[i].fi = cutscn->sels;
                    cutscn->sub[i].e = cutscn->sub[i].fo = cutscn->sele;
                    cutscn->seli = i;
                } else {
                    for(i = 0; i < cutscn->numsub &&
                      !(cutscn->sub[i].s <= cutscn->clkx + cutscn->scr && cutscn->sub[i].e >= cutscn->clkx + cutscn->scr); i++);
                    if(i < cutscn->numsub) {
                        cutscn->sels = cutscn->sub[i].s;
                        cutscn->sele = cutscn->sub[i].e;
                        cutscn->seli = i;
                    }
                }
            break;
            case 4:
                if(cutscn->sele) {
                    if(cutscn->sels) {
                        if(!cutscn->cmd.root.children || cutscn->cmd.root.children[0].type != CMD_DELAY - CMD_NOP) {
                            cutscn->cmd.root.children = (cmd_t*)realloc(cutscn->cmd.root.children,
                                (cutscn->cmd.root.len + 1) * sizeof(cmd_t));
                            if(cutscn->cmd.root.children) {
                                memmove(&cutscn->cmd.root.children[1], cutscn->cmd.root.children,
                                    cutscn->cmd.root.len * sizeof(cmd_t));
                                memset(cutscn->cmd.root.children, 0, sizeof(cmd_t));
                                cutscn->cmd.root.children[0].type = CMD_DELAY - CMD_NOP;
                                cutscn->cmd.root.len++;
                            } else
                                cutscn->cmd.root.len = 0;
                            ui_cmd_resize(&cutscn->cmd.root);
                        }
                        if(cutscn->cmd.root.children && cutscn->cmd.root.children[0].type == CMD_DELAY - CMD_NOP)
                            cutscn->cmd.root.children[0].arg3 = cutscn->sels;
                    } else {
                        if(cutscn->cmd.root.len && cutscn->cmd.root.children &&
                          cutscn->cmd.root.children[0].type == CMD_DELAY - CMD_NOP) {
                            cutscn->cmd.root.len--;
                            memmove(cutscn->cmd.root.children, &cutscn->cmd.root.children[1],
                                cutscn->cmd.root.len * sizeof(cmd_t));
                            ui_cmd_resize(&cutscn->cmd.root);
                        }
                    }
                } else
                    cutscn->sels = cutscn->sele = 0;
            break;
        }
    }
    cutscn->scrx = cutscn->clkx = cutscn->clk = 0;
    return 1;
}

/**
 * Cutscene editor on mouse over handler
 */
int ui_cutscn_over(ui_cutscn_t *cutscn, int x, int y, int w)
{
    if(!cutscn) return 0;
    if(!ui_curbtn && cutscn->clk) {
        /* little fix when mouse left the cutscn area during selection */
        ui_cutscn_release(cutscn, x, y);
        cutscn->clk = 0;
        return 4;
    }
    switch(cutscn->clk) {
        case 0:
            if(y >= 0 && y < 48) ui_status(0, lang[CUTSCN_CH_MOV]); else
            if(y >= 48 && y < 48 + 48) ui_status(0, lang[CUTSCN_CH_IMG]); else
            if(y >= 48 + 48 && y < 48 + 48 + 24) ui_status(0, lang[CUTSCN_CH_SPH]); else
            if(y >= 48 + 48 + 24 && y < 48 + 48 + 48) ui_status(0, lang[CUTSCN_CH_TXT]); else
            if(y >= 48 + 48 + 48 && y < 48 + 48 + 48 + 24) ui_status(0, lang[CUTSCN_CH_SCR]);
            return (x >= 32 && y < 48 + 48 + 48 + 24);
        break;
        case 1:
            if(cutscn->clkx != -1) {
                cutscn->sels = cutscn->scr + cutscn->clkx;
                cutscn->clkx = -1;
                if(cutscn->sels > cutscn->len) cutscn->sels = cutscn->len;
                if(cutscn->sels < 0) cutscn->sels = 0;
            }
            cutscn->sele = cutscn->scr + x - 31;
            if(cutscn->sele > cutscn->len) cutscn->sele = cutscn->len;
            if(cutscn->sele < 0) cutscn->sele = 0;
            return 2;
        break;
        case 2:
            cutscn->scr = cutscn->scrx + cutscn->clkx - x + 31;
            if(cutscn->scr + w - 33 > cutscn->len) cutscn->scr = cutscn->len - w + 33;
            if(cutscn->scr < 0) cutscn->scr = 0;
        break;
    }
    return 3;
}
