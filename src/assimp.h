/*
 * tnge/assimp.h
 *
 * Copyright (C) 2019 bzt
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief use dynamically loaded libassimp to load model
 *
 */

/* do not rely on default assimp headers:
 * 1. it is possible that the user compiling our code doesn't have them
 * 2. they are disgusting, ugly and weight more than a megabyte! We can do it in less than 170 SLoC
 * 3. we don't need 99% of its definitions and structures
 * 4. public and internal API isn't separated in the assimp headers, internal (and configurable) structures are directly
 *    exposed, and there's no way of knowing if we've configured the headers the same way as the shared library on the end
 *    user's machine was compiled with... So this configuration is just as good as any other.
 */
enum aiPostProcessSteps
{
    aiProcess_JoinIdenticalVertices = 0x2,
    aiProcess_Triangulate = 0x8,
    aiProcess_RemoveComponent = 0x10,
    aiProcess_GenSmoothNormals = 0x40,
    aiProcess_GenUVCoords = 0x40000,
    aiProcess_TransformUVCoords = 0x80000
};

struct aiString
{
    uint32_t length;
    char data[1024];
};

struct aiColor4D {
    float r, g, b, a;
};

struct aiVector3D {
    float x, y, z;
};

struct aiMatrix3x3 {
    float a1, a2, a3;
    float b1, b2, b3;
    float c1, c2, c3;
};

struct aiMatrix4x4 {
    float a1, a2, a3, a4;
    float b1, b2, b3, b4;
    float c1, c2, c3, c4;
    float d1, d2, d3, d4;
};

struct aiQuaternion {
    float w, x, y, z;
};

struct aiVectorKey
{
    double mTime;
    struct aiVector3D mValue;
};

struct aiQuatKey
{
    double mTime;
    struct aiQuaternion mValue;
};

struct aiMaterial
{
    void** mProperties;
    unsigned int mNumProperties;
    unsigned int mNumAllocated;
};

struct aiFace
{
    unsigned int mNumIndices;
    unsigned int* mIndices;
};

struct aiVertexWeight {
    unsigned int mVertexId;
    float mWeight;
};

struct aiBone {
    struct aiString mName;
    unsigned int mNumWeights;
    struct aiVertexWeight* mWeights;
    struct aiMatrix4x4 mOffsetMatrix;
};

struct aiNode
{
    struct aiString mName;
    struct aiMatrix4x4 mTransformation;
    struct aiNode* mParent;
    unsigned int mNumChildren;
    struct aiNode** mChildren;
    unsigned int mNumMeshes;
    unsigned int* mMeshes;
    void* mMetaData;
};

struct aiNodeAnim {
    struct aiString mNodeName;
    unsigned int mNumPositionKeys;
    struct aiVectorKey* mPositionKeys;
    unsigned int mNumRotationKeys;
    struct aiQuatKey* mRotationKeys;
    unsigned int mNumScalingKeys;
    struct aiVectorKey* mScalingKeys;
    int mPreState;
    int mPostState;
};

#define AI_MAX_NUMBER_OF_COLOR_SETS 8
#define AI_MAX_NUMBER_OF_TEXTURECOORDS 8
struct aiAnimMesh
{
    struct aiString mName;
    struct aiVector3D* mVertices;
    struct aiVector3D* mNormals;
    struct aiVector3D* mTangents;
    struct aiVector3D* mBitangents;
    struct aiColor4D* mColors[AI_MAX_NUMBER_OF_COLOR_SETS];
    struct aiVector3D* mTextureCoords[AI_MAX_NUMBER_OF_TEXTURECOORDS];
    unsigned int mNumVertices;
    float mWeight;
};

struct aiAnimation {
    struct aiString mName;
    double mDuration;
    double mTicksPerSecond;
    unsigned int mNumChannels;
    struct aiNodeAnim** mChannels;
    unsigned int mNumMeshChannels;
    struct aiMeshAnim** mMeshChannels;
    unsigned int mNumMorphMeshChannels;
    void **mMorphMeshChannels;
};

struct aiMesh
{
    unsigned int mPrimitiveTypes;
    unsigned int mNumVertices;
    unsigned int mNumFaces;
    struct aiVector3D* mVertices;
    struct aiVector3D* mNormals;
    struct aiVector3D* mTangents;
    struct aiVector3D* mBitangents;
    struct aiColor4D* mColors[AI_MAX_NUMBER_OF_COLOR_SETS];
    struct aiVector3D* mTextureCoords[AI_MAX_NUMBER_OF_TEXTURECOORDS];
    unsigned int mNumUVComponents[AI_MAX_NUMBER_OF_TEXTURECOORDS];
    struct aiFace* mFaces;
    unsigned int mNumBones;
    struct aiBone** mBones;
    unsigned int mMaterialIndex;
    struct aiString mName;
    unsigned int mNumAnimMeshes;
    void** mAnimMeshes;
    unsigned int mMethod;
    struct aiVector3D mMin;
    struct aiVector3D mMax;
};

struct aiScene
{
    unsigned int mFlags;
    struct aiNode* mRootNode;
    unsigned int mNumMeshes;
    struct aiMesh** mMeshes;
    unsigned int mNumMaterials;
    struct aiMaterial** mMaterials;
    unsigned int mNumAnimations;
    struct aiAnimation** mAnimations;
    unsigned int mNumTextures;
    void** mTextures;
    unsigned int mNumLights;
    void** mLights;
    unsigned int mNumCameras;
    void** mCameras;
    void* mMetaData;
};

#define MIN(a,b) ((a) < (b) ? (a) : (b))
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define CLAMP(x,a,b) MIN(MAX(x,a),b)

/**
 * The dynamic library loader
 */
typedef struct {
    void *handle;
    struct aiScene* (*aiImportFile)(const char* pFile, unsigned int pFlags);
    void (*aiReleaseImport)(const struct aiScene* pScene);
    int (*aiGetMaterialString)(const struct aiMaterial* pMat, const char* pKey, unsigned int type, unsigned int index, struct aiString* pOut);
    int (*aiGetMaterialColor)(const struct aiMaterial* pMat, const char* pKey, unsigned int type, unsigned int index, struct aiColor4D* pOut);
    int (*aiGetMaterialFloatArray)(const struct aiMaterial* pMat, const char* pKey, unsigned int type, unsigned int index, float* pOut, void* pMax);
    void (*aiIdentityMatrix4)(struct aiMatrix4x4* mat);
    void (*aiMultiplyMatrix4)(struct aiMatrix4x4* dst, const struct aiMatrix4x4* src);
    void (*aiTransformVecByMatrix4)(struct aiVector3D* vec, const struct aiMatrix4x4* mat);
    void (*aiTransformVecByMatrix3)(struct aiVector3D* vec, const struct aiMatrix3x3* mat);
} assimp_loader_t;
assimp_loader_t assimp = { 0 };

/**
 * Most of this code was adopted from this BSD-licensed source:
 * https://github.com/ccxvii/asstools
 * but stripped down and modified heavily by me. I've also improved the material loader considerably.
 */
struct aiScene *g_scene = NULL;
struct aiAnimation *curanim = NULL;
char *assimp_fn = NULL;
unsigned int meshcount = 0;
struct mesh {
    struct aiMesh *mesh;
    unsigned int texture;
    int vertexcount, elementcount;
    float *position;
    float *normal;
    float *texcoord;
    int *element;
} *meshlist = NULL;

void transposematrix(float m[16], struct aiMatrix4x4 *p)
{
    m[0] = p->a1; m[4] = p->a2; m[8] = p->a3; m[12] = p->a4;
    m[1] = p->b1; m[5] = p->b2; m[9] = p->b3; m[13] = p->b4;
    m[2] = p->c1; m[6] = p->c2; m[10] = p->c3; m[14] = p->c4;
    m[3] = p->d1; m[7] = p->d2; m[11] = p->d3; m[15] = p->d4;
}

void extract3x3(struct aiMatrix3x3 *m3, struct aiMatrix4x4 *m4)
{
    m3->a1 = m4->a1; m3->a2 = m4->a2; m3->a3 = m4->a3;
    m3->b1 = m4->b1; m3->b2 = m4->b2; m3->b3 = m4->b3;
    m3->c1 = m4->c1; m3->c2 = m4->c2; m3->c3 = m4->c3;
}

void mixvector(struct aiVector3D *p, struct aiVector3D *a, struct aiVector3D *b, float t)
{
    p->x = a->x + t * (b->x - a->x);
    p->y = a->y + t * (b->y - a->y);
    p->z = a->z + t * (b->z - a->z);
}

float dotquaternions(struct aiQuaternion *a, struct aiQuaternion *b)
{
    return a->x*b->x + a->y*b->y + a->z*b->z + a->w*b->w;
}

void normalizequaternion(struct aiQuaternion *q)
{
    float d = sqrt(dotquaternions(q, q));
    if (d >= 0.00001) {
        d = 1 / d;
        q->x *= d;
        q->y *= d;
        q->z *= d;
        q->w *= d;
    } else {
        q->x = q->y = q->z = 0;
        q->w = 1;
    }
}

void mixquaternion(struct aiQuaternion *q, struct aiQuaternion *a, struct aiQuaternion *b, float t)
{
    struct aiQuaternion tmp;
    if (dotquaternions(a, b) < 0) {
        tmp.x = -a->x; tmp.y = -a->y; tmp.z = -a->z; tmp.w = -a->w;
        a = &tmp;
    }
    q->x = a->x + t * (b->x - a->x);
    q->y = a->y + t * (b->y - a->y);
    q->z = a->z + t * (b->z - a->z);
    q->w = a->w + t * (b->w - a->w);
    normalizequaternion(q);
}

void composematrix(struct aiMatrix4x4 *m,
    struct aiVector3D *t, struct aiQuaternion *q, struct aiVector3D *s)
{
    m->a1 = 1 - 2 * (q->y * q->y + q->z * q->z);
    m->a2 = 2 * (q->x * q->y - q->z * q->w);
    m->a3 = 2 * (q->x * q->z + q->y * q->w);
    m->b1 = 2 * (q->x * q->y + q->z * q->w);
    m->b2 = 1 - 2 * (q->x * q->x + q->z * q->z);
    m->b3 = 2 * (q->y * q->z - q->x * q->w);
    m->c1 = 2 * (q->x * q->z - q->y * q->w);
    m->c2 = 2 * (q->y * q->z + q->x * q->w);
    m->c3 = 1 - 2 * (q->x * q->x + q->y * q->y);
    m->a1 *= s->x; m->a2 *= s->x; m->a3 *= s->x;
    m->b1 *= s->y; m->b2 *= s->y; m->b3 *= s->y;
    m->c1 *= s->z; m->c2 *= s->z; m->c3 *= s->z;
    m->a4 = t->x; m->b4 = t->y; m->c4 = t->z;
    m->d1 = 0; m->d2 = 0; m->d3 = 0; m->d4 = 1;
}

unsigned int animationlength(struct aiAnimation *anim)
{
    unsigned int i, len = 0;
    struct aiNodeAnim *chan;
    if(!anim) return 0;
    for (i = 0; i < anim->mNumChannels; i++) {
        chan = anim->mChannels[i];
        len = MAX(len, chan->mNumPositionKeys);
        len = MAX(len, chan->mNumRotationKeys);
        len = MAX(len, chan->mNumScalingKeys);
    }
    return len;
}

void setanim(unsigned int i)
{
    if (!g_scene) return;
    curanim = NULL;
    if (g_scene->mNumAnimations == 0) return;
    i = MIN(i, g_scene->mNumAnimations - 1);
    curanim = g_scene->mAnimations[i];
    animlen = animationlength(curanim);
}

void measuremesh(struct mesh *mesh, struct aiMatrix4x4 transform, float bboxmin[3], float bboxmax[3])
{
    struct aiVector3D p;
    int i;
    for (i = 0; i < mesh->vertexcount; i++) {
        p.x = mesh->position[i*3+0];
        p.y = mesh->position[i*3+1];
        p.z = mesh->position[i*3+2];
        (*assimp.aiTransformVecByMatrix4)(&p, &transform);
        bboxmin[0] = MIN(bboxmin[0], p.x);
        bboxmin[1] = MIN(bboxmin[1], p.y);
        bboxmin[2] = MIN(bboxmin[2], p.z);
        bboxmax[0] = MAX(bboxmax[0], p.x);
        bboxmax[1] = MAX(bboxmax[1], p.y);
        bboxmax[2] = MAX(bboxmax[2], p.z);
    }
}

void measurenode(struct aiNode *node, struct aiMatrix4x4 world, float bboxmin[3], float bboxmax[3])
{
    struct aiMatrix4x4 identity;
    struct mesh *mesh;
    unsigned int i;

    (*assimp.aiMultiplyMatrix4)(&world, &node->mTransformation);
    (*assimp.aiIdentityMatrix4)(&identity);

    for (i = 0; i < node->mNumMeshes; i++) {
        mesh = meshlist + node->mMeshes[i];
        if (mesh->mesh->mNumBones == 0) {
            measuremesh(mesh, world, bboxmin, bboxmax);
        } else {
            measuremesh(mesh, identity, bboxmin, bboxmax);
        }
    }

    for (i = 0; i < node->mNumChildren; i++)
        measurenode(node->mChildren[i], world, bboxmin, bboxmax);
}

float measurescene(struct aiScene *scene)
{
    struct aiMatrix4x4 world;
    float bboxmin[3];
    float bboxmax[3];
    float dx, dy, dz;
    float center[3];

    bboxmin[0] = 1e10; bboxmax[0] = -1e10;
    bboxmin[1] = 1e10; bboxmax[1] = -1e10;
    bboxmin[2] = 1e10; bboxmax[2] = -1e10;

    (*assimp.aiIdentityMatrix4)(&world);
    measurenode(scene->mRootNode, world, bboxmin, bboxmax);

    center[0] = (bboxmin[0] + bboxmax[0]) / 2;
    center[1] = (bboxmin[1] + bboxmax[1]) / 2;
    center[2] = (bboxmin[2] + bboxmax[2]) / 2;

    dx = MAX(center[0] - bboxmin[0], bboxmax[0] - center[0]);
    dy = MAX(center[1] - bboxmin[1], bboxmax[1] - center[1]);
    dz = MAX(center[2] - bboxmin[2], bboxmax[2] - center[2]);

    return sqrt(dx*dx + dy*dy + dz*dz);
}

struct aiNode *findnode(struct aiNode *node, char *name)
{
    unsigned int i;
    if (!strcmp(name, node->mName.data))
        return node;
    for (i = 0; i < node->mNumChildren; i++) {
        struct aiNode *found = findnode(node->mChildren[i], name);
        if (found)
            return found;
    }
    return NULL;
}

void transformnode(struct aiMatrix4x4 *result, struct aiNode *node)
{
    if (node->mParent) {
        transformnode(result, node->mParent);
        (*assimp.aiMultiplyMatrix4)(result, &node->mTransformation);
    } else {
        *result = node->mTransformation;
    }
}

void transformmesh(struct aiScene *scene, struct mesh *mesh)
{
    struct aiMesh *amesh = mesh->mesh;
    struct aiBone *bone;
    struct aiNode *node;
    struct aiMatrix4x4 skin4;
    struct aiMatrix3x3 skin3;
    struct aiVector3D position, normal;
    unsigned int i, k, v;
    float w;

    if (amesh->mNumBones == 0)
        return;

    memset(mesh->position, 0, mesh->vertexcount * 3 * sizeof(float));
    memset(mesh->normal, 0, mesh->vertexcount * 3 * sizeof(float));

    for (k = 0; k < amesh->mNumBones; k++) {
        bone = amesh->mBones[k];
        node = findnode(scene->mRootNode, bone->mName.data);

        transformnode(&skin4, node);
        (*assimp.aiMultiplyMatrix4)(&skin4, &bone->mOffsetMatrix);
        extract3x3(&skin3, &skin4);

        for (i = 0; i < bone->mNumWeights; i++) {
            v = bone->mWeights[i].mVertexId;
            w = bone->mWeights[i].mWeight;

            position = amesh->mVertices[v];
            (*assimp.aiTransformVecByMatrix4)(&position, &skin4);
            mesh->position[v*3+0] += position.x * w;
            mesh->position[v*3+1] += position.y * w;
            mesh->position[v*3+2] += position.z * w;

            normal = amesh->mNormals[v];
            (*assimp.aiTransformVecByMatrix3)(&normal, &skin3);
            mesh->normal[v*3+0] += normal.x * w;
            mesh->normal[v*3+1] += normal.y * w;
            mesh->normal[v*3+2] += normal.z * w;
        }
    }
}

void animatescene(struct aiScene *scene, struct aiAnimation *anim, float tick)
{
    struct aiVectorKey *p0, *p1, *s0, *s1;
    struct aiQuatKey *r0, *r1;
    struct aiVector3D p, s;
    struct aiQuaternion r;
    struct aiNodeAnim *chan;
    struct aiNode *node;
    unsigned int i;
    int frame = floor(tick);
    float t = tick - floor(tick);

    for (i = 0; i < anim->mNumChannels; i++) {
        chan = anim->mChannels[i];
        node = findnode(scene->mRootNode, chan->mNodeName.data);
        p0 = chan->mPositionKeys + (frame+0) % chan->mNumPositionKeys;
        p1 = chan->mPositionKeys + (frame+1) % chan->mNumPositionKeys;
        r0 = chan->mRotationKeys + (frame+0) % chan->mNumRotationKeys;
        r1 = chan->mRotationKeys + (frame+1) % chan->mNumRotationKeys;
        s0 = chan->mScalingKeys + (frame+0) % chan->mNumScalingKeys;
        s1 = chan->mScalingKeys + (frame+1) % chan->mNumScalingKeys;
        mixvector(&p, &p0->mValue, &p1->mValue, t);
        mixquaternion(&r, &r0->mValue, &r1->mValue, t);
        mixvector(&s, &s0->mValue, &s1->mValue, t);
        composematrix(&node->mTransformation, &p, &r, &s);
    }

    for (i = 0; i < meshcount; i++)
        transformmesh(scene, meshlist + i);
}

unsigned int assimp_loadmaterial(struct aiMaterial *material)
{
    struct aiString str = { 0 };
    unsigned int texture;
    unsigned char *image;
    char *fn;
    int w = 0, h = 0;

    if (assimp_fn && !(*assimp.aiGetMaterialString)(material, "$tex.file",1,0, &str) && str.data[0]) {
        fn = strrchr(str.data, '/');
        if(!fn) fn = strrchr(str.data, '\\');
        if(!fn) fn = str.data; else fn++;
        strcpy(assimp_fn, fn);
        image = image_load(fileops_path, &w, &h);
        if (!image) {
            if(verbose) printf("assimp_loadmaterial: unable to load texture '%s'\n", fileops_path);
            return 0;
        }
        else
        if(verbose > 2) printf("  Loading texture '%s' OK\n", fn);
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexImage2D(GL_TEXTURE_2D, 0, 4, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
        free(image);
        return texture;
    }
    return 0;
}

void initmesh(struct aiScene *scene, struct mesh *mesh, struct aiMesh *amesh)
{
    unsigned int i;
    mesh->mesh = amesh;

    mesh->texture = assimp_loadmaterial(scene->mMaterials[amesh->mMaterialIndex]);

    mesh->vertexcount = amesh->mNumVertices;
    mesh->position = calloc(mesh->vertexcount * 3, sizeof(float));
    mesh->normal = calloc(mesh->vertexcount * 3, sizeof(float));
    mesh->texcoord = calloc(mesh->vertexcount * 2, sizeof(float));

    for (i = 0; (int)i < mesh->vertexcount; i++) {
        mesh->position[i*3+0] = amesh->mVertices[i].x;
        mesh->position[i*3+1] = amesh->mVertices[i].y;
        mesh->position[i*3+2] = amesh->mVertices[i].z;

        if (amesh->mNormals) {
            mesh->normal[i*3+0] = amesh->mNormals[i].x;
            mesh->normal[i*3+1] = amesh->mNormals[i].y;
            mesh->normal[i*3+2] = amesh->mNormals[i].z;
        }

        if (amesh->mTextureCoords[0]) {
            mesh->texcoord[i*2+0] = amesh->mTextureCoords[0][i].x;
            mesh->texcoord[i*2+1] = 1 - amesh->mTextureCoords[0][i].y;
        }
    }

    mesh->elementcount = amesh->mNumFaces * 3;
    mesh->element = calloc(mesh->elementcount, sizeof(int));

    for (i = 0; i < amesh->mNumFaces; i++) {
        struct aiFace *face = amesh->mFaces + i;
        mesh->element[i*3+0] = face->mIndices[0];
        mesh->element[i*3+1] = face->mIndices[1];
        mesh->element[i*3+2] = face->mIndices[2];
    }
}

void drawmesh(struct mesh *mesh)
{
    int i;
    float f, color[4];
    struct aiMaterial *material = g_scene->mMaterials[mesh->mesh->mMaterialIndex];
    struct aiColor4D c;

    if(!(*assimp.aiGetMaterialColor)(material, "$clr.diffuse",0,0, &c)) {
        color[0] = c.r; color[1] = c.g; color[2] = c.b; color[3] = c.a;
        glColor4fv((GLfloat*)&color);
    } else glColor4f(0.9, 0.7, 0.7, 1);
    if(!(*assimp.aiGetMaterialColor)(material, "$clr.ambient",0,0, &c)) {
        color[0] = c.r; color[1] = c.g; color[2] = c.b; color[3] = c.a;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (GLfloat*)&color);
    }
    if(!(*assimp.aiGetMaterialColor)(material, "$clr.specular",0,0, &c)) {
        color[0] = c.r; color[1] = c.g; color[2] = c.b; color[3] = c.a;
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (GLfloat*)&color);
    }
    if(!(*assimp.aiGetMaterialColor)(material, "$clr.emissive",0,0, &c)) {
        color[0] = c.r; color[1] = c.g; color[2] = c.b; color[3] = c.a;
        glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, (GLfloat*)&color);
    }
    if(!(*assimp.aiGetMaterialFloatArray)(material, "$mat.shininess",0,0, &f, NULL))
        glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, f);
    if (mesh->texture > 0) glBindTexture(GL_TEXTURE_2D, mesh->texture);
    /* TinyGL does not support glDrawElements */
    /*
    glVertexPointer(3, GL_FLOAT, 0, mesh->position);
    glNormalPointer(GL_FLOAT, 0, mesh->normal);
    glTexCoordPointer(2, GL_FLOAT, 0, mesh->texcoord);
    glDrawElements(GL_TRIANGLES, mesh->elementcount, GL_UNSIGNED_INT, mesh->element);
    */
    glBegin(GL_TRIANGLES);
    for(i = 0; i < mesh->elementcount; i++) {
        glNormal3fv(mesh->normal + mesh->element[i] * 3);
        glTexCoord2fv(mesh->texcoord + mesh->element[i] * 2);
        glVertex3fv(mesh->position + mesh->element[i] * 3);
    }
    glEnd();
}

void drawnode(struct aiNode *node, struct aiMatrix4x4 world)
{
    struct mesh *mesh;
    float mat[16];
    unsigned int i;

    (*assimp.aiMultiplyMatrix4)(&world, &node->mTransformation);
    transposematrix(mat, &world);

    for (i = 0; i < node->mNumMeshes; i++) {
        mesh = meshlist + node->mMeshes[i];
        if (mesh->mesh->mNumBones == 0) {
            glPushMatrix();
            glMultMatrixf(mat);
            drawmesh(mesh);
            glPopMatrix();
        } else {
            drawmesh(mesh);
        }
    }

    for (i = 0; i < node->mNumChildren; i++)
        drawnode(node->mChildren[i], world);
}

void drawscene(struct aiScene *scene, unsigned int actionid, unsigned int msec)
{
    struct aiMatrix4x4 world;
    if(scene->mNumAnimations)
        setanim(actionid);
    if (curanim)
        animatescene(g_scene, curanim, msec/1000.0);
    (*assimp.aiIdentityMatrix4)(&world);
    drawnode(scene->mRootNode, world);
}

void initscene(struct aiScene *scene)
{
    unsigned int i;
    float radius;

    /* hackish, but we need the absolute path for loading textures... */
    assimp_fn = strrchr(fileops_path, SEP[0]);
    if(!assimp_fn) assimp_fn = fileops_path; else assimp_fn++;

    meshcount = scene->mNumMeshes;
    meshlist = main_alloc(meshcount * sizeof(*meshlist));
    for(i = 0; i < meshcount; i++) {
        initmesh(scene, meshlist + i, scene->mMeshes[i]);
        transformmesh(scene, meshlist + i);
    }
    radius = measurescene(scene);
    if(radius > distance / 5) {
        distance = radius * 5;
        mindist = radius * 0.1;
        maxdist = radius * 10;
    }
    setanim(0);
}

void freescene(struct aiScene **scene)
{
    unsigned int i;

    if(meshlist) {
        for(i = 0; i < meshcount; i++) {
            if(meshlist[i].position) free(meshlist[i].position);
            if(meshlist[i].normal) free(meshlist[i].normal);
            if(meshlist[i].texcoord) free(meshlist[i].texcoord);
            if(meshlist[i].element) free(meshlist[i].element);
        }
        free(meshlist); meshlist = NULL;
    }
    if(assimp.handle) (*assimp.aiReleaseImport)(*scene);
    *scene = NULL;
}
