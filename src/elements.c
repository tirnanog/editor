/*
 * tnge/elements.c
 *
 * Copyright (C) 2022 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief User Interface elements window
 *
 */

#include "main.h"

enum {
    E_PTR, E_CLK, E_NOALLOW, E_ACTION, E_LOADING,                                       /* cursors */
    E_CHK0, E_CHK1,                                                                     /* checkbox */
    E_OPTPREV, E_OPTPREVPRESS, E_OPTBG, E_OPTNEXT, E_OPTNEXTPRESS,                      /* option box */
    E_SLDL, E_SLDBTN, E_SLDBG, E_SLDR,                                                  /* slider */
    E_SCRL, E_SCRHBG, E_SCRR, E_SCRU, E_SCRVBG, E_SCRD, E_SCRBTN,                       /* scroll bars */
    E_WINNL, E_WINNBG, E_WINNR,                                                         /* normal window tab */
    E_WINIL, E_WINIBG, E_WINIR,                                                         /* inactive window tab */
    E_WINTL, E_WINTBG, E_WINTR,                                                         /* window top */
    E_WINML, E_WINBG, E_WINMR,                                                          /* window middle */
    E_WINBL, E_WINBBG, E_WINBR,                                                         /* window bottom */
    E_DLGNL, E_DLGNBG, E_DLGNR,                                                         /* dialog tab */
    E_DLGOL, E_DLGOBG, E_DLGOR,                                                         /* dialog option */
    E_DLGTL, E_DLGTBG, E_DLGTR,                                                         /* dialog top */
    E_DLGML, E_DLGBG, E_DLGMR,                                                          /* dialog middle */
    E_DLGBL, E_DLGBBG, E_DLGBR,                                                         /* dialog bottom */
    E_BTNNL, E_BTNNBG, E_BTNNR,                                                         /* normal button */
    E_BTNSL, E_BTNSBG, E_BTNSR,                                                         /* selected button */
    E_BTNPL, E_BTNPBG, E_BTNPR,                                                         /* pressed button */
    E_BTNIL, E_BTNIBG, E_BTNIR,                                                         /* inactive button */
    E_INPL, E_INPBG, E_INPR,                                                            /* chat input */
    E_APPICO                                                                            /* application icon */
};

void elements_save(void *data);
void elements_delete(void *data);
void elements_preview(void *data);

uint32_t elements_colors[10];
ui_font_t elements_fonts[6];
int elements_is, elements_style[6];
ui_icontgl_t elements_bold[] = {
    { ICON_BOLD, SSFN_STYLE_BOLD, &elements_style[0] },
    { ICON_BOLD, SSFN_STYLE_BOLD, &elements_style[1] },
    { ICON_BOLD, SSFN_STYLE_BOLD, &elements_style[2] },
    { ICON_BOLD, SSFN_STYLE_BOLD, &elements_style[3] },
    { ICON_BOLD, SSFN_STYLE_BOLD, &elements_style[4] },
    { ICON_BOLD, SSFN_STYLE_BOLD, &elements_style[5] }
};
ui_icontgl_t elements_italic[] = {
    { ICON_ITALIC, SSFN_STYLE_ITALIC, &elements_style[0] },
    { ICON_ITALIC, SSFN_STYLE_ITALIC, &elements_style[1] },
    { ICON_ITALIC, SSFN_STYLE_ITALIC, &elements_style[2] },
    { ICON_ITALIC, SSFN_STYLE_ITALIC, &elements_style[3] },
    { ICON_ITALIC, SSFN_STYLE_ITALIC, &elements_style[4] },
    { ICON_ITALIC, SSFN_STYLE_ITALIC, &elements_style[5] }
};
ui_num_t elements_size[] = {
    { 16, 8, 192, 1 },
    { 16, 8, 192, 1 },
    { 16, 8, 192, 1 },
    { 16, 8, 192, 1 },
    { 16, 8, 192, 1 },
    { 16, 8, 192, 1 }
};
ui_select_t elements_fnt[] = {
    { -1, LANG_DEF, NULL },
    { -1, LANG_DEF, NULL },
    { -1, LANG_DEF, NULL },
    { -1, LANG_DEF, NULL },
    { -1, LANG_DEF, NULL },
    { -1, LANG_DEF, NULL }
};
ui_num_t elements_pad[] = {
    { 0, -128, 128, 1 },
    { 0, -128, 128, 1 },
    { 0, -128, 128, 1 },
    { 0, -128, 128, 1 },
    { 0, -128, 128, 1 },
    { 0, -128, 128, 1 }
};
ui_select_t elements_btnssnd = { -1, LANG_NONE, NULL };
ui_select_t elements_btnpsnd = { -1, LANG_NONE, NULL };
ui_select_t elements_errsnd = { -1, LANG_NONE, NULL };

ui_sprsel_t elements_sprites[] = {
    { -1, 0, 128, 1 },  /* E_PTR */
    { -1, 0, 129, 1 },  /* E_CLK */
    { -1, 0, 130, 1 },  /* E_NOALLOW */
    { -1, 0, 131, 1 },  /* E_ACTION */
    { -1, 0, 132, 1 },  /* E_LOADING */

    { -1, 0, 133, 1 },  /* E_CHK0 */
    { -1, 0, 134, 1 },  /* E_CHK1 */

    { -1, 0, 135, 1 },  /* E_OPTPREV */
    { -1, 0, 135, 1 },  /* E_OPTPREVPRESS */
    { -1, 0, 138, 1 },  /* E_OPTBG */
    { -1, 0, 136, 1 },  /* E_OPTNEXT */
    { -1, 0, 136, 1 },  /* E_OPTNEXTPRESS */

    { -1, 0, 144, 1 },  /* E_SLDL */
    { -1, 0, 145, 1 },  /* E_SLDBTN */
    { -1, 0, 144, 1 },  /* E_SLDBG */
    { -1, 0, 144, 1 },  /* E_SLDR */

    { -1, 0, 137, 1 },  /* E_SCRL */
    { -1, 0, 138, 1 },  /* E_SCRHBG */
    { -1, 0, 139, 1 },  /* E_SCRR */
    { -1, 0, 140, 1 },  /* E_SCRU */
    { -1, 0, 141, 1 },  /* E_SCRVBG */
    { -1, 0, 142, 1 },  /* E_SCRD */
    { -1, 0, 143, 1 },  /* E_SCRBTN */

    { -1, 0, 149, 1 },  /* E_WINNL */
    { -1, 0, 138, 1 },  /* E_WINNBG */
    { -1, 0, 150, 1 },  /* E_WINNR */
    { -1, 0, 149, 1 },  /* E_WINIL */
    { -1, 0, 138, 1 },  /* E_WINIBG */
    { -1, 0, 150, 1 },  /* E_WINIR */
    { -1, 0, 148, 1 },  /* E_WINTL */
    { -1, 0, 144, 1 },  /* E_WINTBG */
    { -1, 0, 151, 1 },  /* E_WINTR */
    { -1, 0, 152, 1 },  /* E_WINML */
    { -1, 0, 32, 1 },   /* E_WINBG */
    { -1, 0, 152, 1 },  /* E_WINMR */
    { -1, 0, 153, 1 },  /* E_WINBL */
    { -1, 0, 144, 1 },  /* E_WINBBG */
    { -1, 0, 154, 1 },  /* E_WINBR */

    { -1, 0, 149, 1 },  /* E_DLGNL */
    { -1, 0, 138, 1 },  /* E_DLGNBG */
    { -1, 0, 150, 1 },  /* E_DLGNR */
    { -1, 0, 146, 1 },  /* E_DLGOL */
    { -1, 0, 138, 1 },  /* E_DLGOBG */
    { -1, 0, 147, 1 },  /* E_DLGOR */
    { -1, 0, 148, 1 },  /* E_DLGTL */
    { -1, 0, 144, 1 },  /* E_DLGTBG */
    { -1, 0, 151, 1 },  /* E_DLGTMR */
    { -1, 0, 152, 1 },  /* E_DLGML */
    { -1, 0, 32, 1 },   /* E_DLGBG */
    { -1, 0, 152, 1 },  /* E_DLGMR */
    { -1, 0, 153, 1 },  /* E_DLGBL */
    { -1, 0, 144, 1 },  /* E_DLGBBG */
    { -1, 0, 154, 1 },  /* E_DLGBR */

    { -1, 0, 146, 1 },  /* E_BTNNL */
    { -1, 0, 138, 1 },  /* E_BTNNBG */
    { -1, 0, 147, 1 },  /* E_BTNNR */
    { -1, 0, 146, 1 },  /* E_BTNSL */
    { -1, 0, 138, 1 },  /* E_BTNSBG */
    { -1, 0, 147, 1 },  /* E_BTNSR */
    { -1, 0, 146, 1 },  /* E_BTNPL */
    { -1, 0, 138, 1 },  /* E_BTNPBG */
    { -1, 0, 147, 1 },  /* E_BTNPR */
    { -1, 0, 146, 1 },  /* E_BTNIL */
    { -1, 0, 138, 1 },  /* E_BTNIBG */
    { -1, 0, 147, 1 },  /* E_BTNIR */

    { -1, 0, 146, 1 },  /* E_INPL */
    { -1, 0, 138, 1 },  /* E_INPBG */
    { -1, 0, 147, 1 },  /* E_INPR */
    { -1, 0, 9876, 1 }  /* E_APPICO */
};

/**
 * The form
 */
ui_form_t elements_form[] = {
    { FORM_BUTTONICN, 20, 0, 20, 20, 0, ELEMENTS_DELETE, (void*)ICON_REMOVE, elements_delete },
    { FORM_BUTTON, 0, 0, 0, 20, 0, 0, NULL, elements_save },
    { FORM_BUTTONICN, 0, 0, 40, 20, 0, ELEMENTS_PREVIEW, (void*)ICON_PVIEW, elements_preview },
    /* 3 */
    { FORM_FLDGRP, 10, 30, 0, 0, 0, 0, NULL, NULL },
    { FORM_FLDGRP, 0, 30, 0, 0, 0, 0, NULL, NULL },
    { FORM_FLDGRP, 10, 0, 0, 0, 0, 0, NULL, NULL },
    { FORM_FLDGRP, 0, 0, 0, 0, 0, 0, NULL, NULL },
    { FORM_FLDGRP, 0, 0, 0, 0, 0, 0, NULL, NULL },
    { FORM_FLDGRP, 10, 0, 0, 12 + 6 * 24, 0, 0, NULL, NULL },
    /* 9 */
    { FORM_COLOR, 0, 0, 18, 18, 0, 0, &elements_colors[0], NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_bold[0], NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_italic[0], NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, 0, &elements_size[0], NULL },
    { FORM_SELECT, 0, 0, 0, 18, 0, 0, &elements_fnt[0], NULL },
    /* 14 */
    { FORM_COLOR, 0, 0, 18, 18, 0, 0, &elements_colors[1], NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_bold[1], NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_italic[1], NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, 0, &elements_size[1], NULL },
    { FORM_SELECT, 0, 0, 0, 18, 0, 0, &elements_fnt[1], NULL },
    /* 19 */
    { FORM_COLOR, 0, 0, 18, 18, 0, 0, &elements_colors[2], NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_bold[2], NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_italic[2], NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, 0, &elements_size[2], NULL },
    { FORM_SELECT, 0, 0, 0, 18, 0, 0, &elements_fnt[2], NULL },
    /* 24 */
    { FORM_COLOR, 0, 0, 18, 18, 0, 0, &elements_colors[3], NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_bold[3], NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_italic[3], NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, 0, &elements_size[3], NULL },
    { FORM_SELECT, 0, 0, 0, 18, 0, 0, &elements_fnt[3], NULL },
    /* 29 */
    { FORM_COLOR, 0, 0, 18, 18, 0, 0, &elements_colors[4], NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_bold[4], NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_italic[4], NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, 0, &elements_size[4], NULL },
    { FORM_SELECT, 0, 0, 0, 18, 0, 0, &elements_fnt[4], NULL },
    /* 34 */
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_bold[5], NULL },
    { FORM_ICONTGL, 0, 0, 18, 18, 0, 0, &elements_italic[5], NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, 0, &elements_size[5], NULL },
    { FORM_SELECT, 0, 0, 0, 18, 0, 0, &elements_fnt[5], NULL },
    /* 38 */
    { FORM_SNDSEL, 0, 0, 0, 18, 0, ELEMENTS_ONSEL, &elements_btnssnd, NULL },
    { FORM_SNDSEL, 0, 0, 0, 18, 0, ELEMENTS_ONCLK, &elements_btnpsnd, NULL },

    /* 40 */
    { FORM_TEXT, 0, 0, 0, 18, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 0, 0, 18, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 0, 0, 18, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 0, 0, 18, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 0, 0, 18, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 0, 0, 18, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 0, 0, 18, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 0, 0, 18, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 0, 0, 18, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 0, 0, 18, 0, 0, NULL, NULL },
    { FORM_TEXT, 0, 0, 0, 18, 0, 0, NULL, NULL },
    /* 51 */
    { FORM_TEXT, 0, 0, 18, 18, 0, 0, "\x1D", NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, ELEMENTS_TITPAD, &elements_pad[0], NULL },
    { FORM_TEXT, 0, 0, 18, 18, 0, 0, "\x1D", NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, ELEMENTS_SELPAD, &elements_pad[1], NULL },
    { FORM_TEXT, 0, 0, 18, 18, 0, 0, "\x1D", NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, ELEMENTS_DLGPAD, &elements_pad[2], NULL },
    { FORM_TEXT, 0, 0, 18, 18, 0, 0, "\x1F", NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, ELEMENTS_WINTPAD, &elements_pad[3], NULL },
    { FORM_TEXT, 0, 0, 18, 18, 0, 0, "\x1E", NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, ELEMENTS_WINLPAD, &elements_pad[4], NULL },
    { FORM_TEXT, 0, 0, 18, 18, 0, 0, "\x1D", NULL },
    { FORM_NUM, 0, 0, 42, 18, 0, ELEMENTS_BTNPPAD, &elements_pad[5], NULL },

    /* 63 */
    { FORM_COLOR, 0, 0, 18, 18, 0, 0, &elements_colors[5], NULL },
    { FORM_COLOR, 0, 0, 18, 18, 0, 0, &elements_colors[6], NULL },
    { FORM_COLOR, 0, 0, 18, 18, 0, 0, &elements_colors[7], NULL },
    { FORM_COLOR, 0, 0, 18, 18, 0, 0, &elements_colors[8], NULL },
    { FORM_COLOR, 0, 0, 18, 18, 0, 0, &elements_colors[9], NULL },
    /* 68 */
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_PTR], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_CLK], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_NOALLOW], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_ACTION], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_LOADING], NULL },
    /* 73 */
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_CHK0], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_CHK1], NULL },
    /* 75 */
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_OPTPREV], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_OPTPREVPRESS], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_OPTBG], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_OPTNEXT], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_OPTNEXTPRESS], NULL },
    /* 80 */
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_SLDL], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_SLDBTN], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_SLDBG], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_SLDR], NULL },
    /* 84 */
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_SCRL], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_SCRHBG], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_SCRR], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_SCRU], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_SCRVBG], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_SCRD], NULL },
    { FORM_SPRITE, 0, 40, 0, 0, 0, 0, &elements_sprites[E_SCRBTN], NULL },
    /* 91 */
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINNL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINNBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINNR], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINIL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINIBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINIR], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINTL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINTBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINTR], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINML], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINMR], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINBL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINBBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_WINBR], NULL },
    /* 106 */
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGNL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGNBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGNR], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGOL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGOBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGOR], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGTL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGTBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGTR], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGML], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGMR], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGBL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGBBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_DLGBR], NULL },
    /* 121 */
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNNL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNNBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNNR], NULL },

    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNSL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNSBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNSR], NULL },

    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNPL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNPBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNPR], NULL },

    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNIL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNIBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_BTNIR], NULL },

    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_INPL], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_INPBG], NULL },
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_INPR], NULL },
    /* 136 */
    { FORM_SPRITE, 0, 0, 0, 0, 0, 0, &elements_sprites[E_APPICO], NULL },
    { FORM_SNDSEL, 0, 0, 0, 18, 0, ELEMENTS_ONERR, &elements_errsnd, NULL },

    { FORM_LAST }
};

/**
 * Exit ui elements window
 */
void elements_exit(int tab)
{
    int i;

    (void)tab;
    for(i = 0; i < 10; i++) elements_colors[i] = 0xFFEFEFEF;
    for(i = 0; i < 6; i++) { elements_style[i] = 0; elements_size[i].val = 16; elements_fnt[i].val = -1; }
    elements_btnssnd.val = elements_btnpsnd.val = -1;
    for(i = 0; i < (int)(sizeof(elements_sprites)/sizeof(elements_sprites[0])); i++) elements_sprites[i].val = -1;
}

/**
 * Enter ui elements window
 */
void elements_init(int tab)
{
    int i, j;

    elements_exit(tab);
    elements_form[1].param = lang[LANG_SAVE];
    elements_form[1].w = ui_textwidth(elements_form[1].param) + 40;
    if(elements_form[1].w < 200) elements_form[1].w = 200;
    elements_form[3].param = lang[ELEMENTS_CURSORS];
    elements_form[4].param = lang[ELEMENTS_INPUTS];
    elements_form[5].param = lang[ELEMENTS_WINDOW];
    elements_form[6].param = lang[ELEMENTS_DIALOGS];
    elements_form[7].param = lang[ELEMENTS_BUTTONS];
    elements_form[8].param = lang[ELEMENTS_FONTS];
    j = 0;
    elements_form[40].param = lang[ELEMENTS_NORMAL];    i = ui_textwidth(elements_form[40].param); if(i > j) j = i;
    elements_form[41].param = lang[ELEMENTS_SELECTED];  i = ui_textwidth(elements_form[41].param); if(i > j) j = i;
    elements_form[42].param = lang[ELEMENTS_PRESSED];   i = ui_textwidth(elements_form[42].param); if(i > j) j = i;
    elements_form[43].param = lang[ELEMENTS_INACTIVE];  i = ui_textwidth(elements_form[43].param); if(i > j) j = i;
    elements_form[44].param = lang[ELEMENTS_CHAT];      i = ui_textwidth(elements_form[44].param); if(i > j) j = i;
    elements_form[40].w = elements_form[41].w = elements_form[42].w = elements_form[43].w = elements_form[44].w = j;
    j = 0;
    elements_form[45].param = lang[ELEMENTS_WINTITLE];  i = ui_textwidth(elements_form[45].param); if(i > j) j = i;
    elements_form[46].param = lang[ELEMENTS_INATITLE];  i = ui_textwidth(elements_form[46].param); if(i > j) j = i;
    elements_form[47].param = lang[ELEMENTS_DLGTITLE];  i = ui_textwidth(elements_form[47].param); if(i > j) j = i;
    elements_form[48].param = lang[ELEMENTS_NORMAL];    i = ui_textwidth(elements_form[48].param); if(i > j) j = i;
    elements_form[49].param = lang[ELEMENTS_SELECTED];  i = ui_textwidth(elements_form[49].param); if(i > j) j = i;
    elements_form[50].param = lang[ELEMENTS_BUTTONS];   i = ui_textwidth(elements_form[50].param); if(i > j) j = i;
    elements_form[45].w = elements_form[46].w = elements_form[47].w = elements_form[48].w = elements_form[49].w =
        elements_form[50].w = j;

    elements_btnssnd.opts = elements_btnpsnd.opts = elements_errsnd.opts = project.sounds;
    for(i = 0; i < 6; i++)
        elements_fnt[i].opts = project.fonts;

    elements_load();
}

/**
 * Resize the view
 */
void elements_resize(int tab, int w, int h)
{
    int i, j, k;

    (void)tab;
    elements_form[0].y = elements_form[1].y = elements_form[2].y = h - 48;
    elements_form[1].x = w - 20 - elements_form[1].w;
    elements_form[2].x = elements_form[1].x - 52;
    /* get the biggest possible icon size on screen and work up the form from there */
    i = (h - 202 - (12 + 5 * 24)) / 6;
    j = (w - 128) / 24;
    elements_is = i < j ? i : j;
    for(i = 68; i < 137; i++) {
        elements_form[i].w = elements_form[i].h = elements_is;
        elements_form[i].status = i - 68 + ELEMENTS_SPR_CSR_PTR;
    }
    elements_form[3].h = elements_form[4].h = elements_is + 16;
    elements_form[3].w = 8 + 5 * (elements_is + 4);
    for(i = 68; i < 73; i++)
        elements_form[i].x = 16 + (i - 68) * (elements_is + 4);
    elements_form[4].x = elements_form[3].x + elements_form[3].w + 10;
    elements_form[73].x = elements_form[4].x + 6 + (i - 73) * (elements_is + 4);
    elements_form[74].x = elements_form[4].x + 6 + (elements_is + 4);
    for(i = 75; i < 80; i++) elements_form[i].x = elements_form[74].x + 8 + (i - 74) * (elements_is + 4);
    for(i = 80; i < 84; i++) elements_form[i].x = elements_form[79].x + 8 + (i - 79) * (elements_is + 4);
    for(i = 84; i < 91; i++) elements_form[i].x = elements_form[83].x + 8 + (i - 83) * (elements_is + 4);
    elements_form[4].w = elements_form[90].x + 6 + elements_is - elements_form[4].x;
    elements_form[5].y = elements_form[6].y = elements_form[7].y = elements_form[3].y + elements_form[3].h + 16;
    elements_form[5].h = elements_form[6].h = elements_form[7].h = 12 + 5 * (elements_is + 4);
    elements_form[5].w = elements_form[6].w = 8 + 3 * (elements_is + 4);
    elements_form[6].x = elements_form[5].x + elements_form[5].w + 10;
    elements_form[7].x = elements_form[6].x + elements_form[6].w + 10;
    elements_form[7].w = w - 20 - elements_is - elements_form[7].x;
    elements_form[8].y = elements_form[5].y + elements_form[5].h + 16;
    elements_form[8].w = w - 10 - elements_form[8].x;
    for(j = 0; j < 6; j++) {
        elements_form[45 + j].x = elements_form[8].x + 6;
        elements_form[45 + j].y = elements_form[8].y + 10 + j * 24;
    }
    for(j = 0; j < 5; j++) {
        elements_form[40 + j].x = elements_form[7].x + 6;
        elements_form[40 + j].y = elements_form[7].y + 10 + j * (elements_is + 4) + (elements_is - elements_form[40 + j].h) / 2;;
        elements_form[63 + j].x = elements_form[40].x + elements_form[40].w + 14;
        elements_form[63 + j].y = elements_form[7].y + 10 + j * (elements_is + 4) + (elements_is - elements_form[63 + j].h) / 2;
        for(i = 0; i < 3; i++) {
            elements_form[91 + j * 3 + i].x = elements_form[5].x + 6 + i * (elements_is + 4);
            elements_form[91 + j * 3 + i].y = elements_form[5].y + 10 + j * (elements_is + 4);
            elements_form[106 + j * 3 + i].x = elements_form[6].x + 6 + i * (elements_is + 4);
            elements_form[106 + j * 3 + i].y = elements_form[6].y + 10 + j * (elements_is + 4);
            elements_form[121 + j * 3 + i].x = elements_form[63 + j].x + elements_form[63 + j].w + 4 + i * (elements_is + 4);
            elements_form[121 + j * 3 + i].y = elements_form[7].y + 10 + j * (elements_is + 4);
        }
        k = elements_form[45].x + elements_form[45].w + 14;
        for(i = 0; i < 5; i++) {
            elements_form[9 + j * 5 + i].x = k;
            elements_form[9 + j * 5 + i].y = elements_form[8].y + 10 + 24 * j;
            k += elements_form[9 + j * 5 + i].w + 4;
        }
        elements_form[9 + j * 5 + 4].w = w - 30 - elements_form[9 + j * 5 + 4].x - elements_form[51].w - elements_form[52].w;
        elements_form[52 + j * 2].x = w - 20 - elements_form[52 + j * 2].w;
        elements_form[51 + j * 2].x = elements_form[52 + j * 2].x - elements_form[51 + j * 2].w;
        elements_form[51 + j * 2].y = elements_form[52 + j * 2].y = elements_form[8].y + 10 + 24 * j;
    }
    k = elements_form[30].x;
    for(i = 34; i < 38; i++) {
        elements_form[i].x = k;
        elements_form[i].y = elements_form[30].y + 24;
        k += elements_form[i].w + 4;
    }
    elements_form[37].w = w - 30 - elements_form[37].x - elements_form[51].w - elements_form[52].w;
    elements_form[62].x = w - 20 - elements_form[62].w;
    elements_form[61].x = elements_form[62].x - elements_form[61].w;
    elements_form[61].y = elements_form[62].y = elements_form[59].y + 24;
    elements_form[38].x = elements_form[39].x = elements_form[137].x = elements_form[126].x + elements_form[126].w + 24;
    elements_form[38].w = elements_form[39].w = elements_form[137].w = w - 30 - elements_is - elements_form[38].x;
    elements_form[38].y = elements_form[64].y;
    elements_form[39].y = elements_form[65].y;
    elements_form[137].y = elements_form[66].y;

    elements_form[136].y = elements_form[91].y;
    elements_form[136].x = w - 10 - elements_is;
}

/**
 * View layer
 */
void elements_redraw(int tab)
{
    (void)tab;
    ui_form = elements_form;
}

/**
 * Delete ui elements
 */
void elements_delete(void *data)
{
    (void)data;
    sprintf(projfn, "%s" SEP "%s" SEP "elements.cfg", project.id, project_dirs[PROJDIRS_UI]);
    if(ui_modal(ICON_WARN, LANG_SURE, projfn)) {
        if(verbose) printf("elements_delete: removing %s\n", projdir);
        remove(projdir);
        elements_exit(SUBMENU_ELEMENTS);
    }
}

/**
 * Save ui element configuration
 */
void elements_save(void *data)
{
    FILE *f;
    int i;

    (void)data;
    f = project_savefile(0, project_dirs[PROJDIRS_UI], "elements", "cfg", PROJMAGIC_ELEMENTS, "elements_save");
    if(f) {
        for(i = 0; i < 10; i++) fprintf(f, "%s%08X", i ? " " : "", elements_colors[i]);
        fprintf(f, "\r\n");
        for(i = 0; i < 6; i++) {
            project_wrfont(f, elements_style[i], elements_size[i].val, elements_fnt[i].val);
            fprintf(f, " %d\r\n", elements_pad[i].val);
        }
        project_wridx(f, elements_btnssnd.val, project.sounds); fprintf(f, " ");
        project_wridx(f, elements_btnpsnd.val, project.sounds); fprintf(f, " ");
        project_wridx(f, elements_errsnd.val, project.sounds); fprintf(f, "\r\n");
        for(i = 0; i < (int)(sizeof(elements_sprites)/sizeof(elements_sprites[0])); i++) {
            project_wrsprite(f, &elements_sprites[i]); fprintf(f, "\r\n");
        }
        fclose(f);
        ui_status(0, lang[LANG_SAVED]);
    } else
        ui_status(1, lang[ERR_SAVEUI]);
}

/**
 * Load the ui element configuration
 */
void elements_load()
{
    char *str, *s;
    int i;

    str = project_loadfile(project_dirs[PROJDIRS_UI], "elements", "cfg", PROJMAGIC_ELEMENTS, "elements_load");
    if(str) {
        s = project_skipnl(str);
        for(i = 0; i < 10; i++)
            s = project_getcolor(s, &elements_colors[i]);
        s = project_skipnl(s);
        for(i = 0; i < 6; i++) {
            s = project_getfont(s, &elements_style[i], &elements_size[i].val, &elements_fnt[i].val);
            s = project_getint(s, &elements_pad[i].val, elements_pad[i].min, elements_pad[i].max);
            s = project_skipnl(s);
        }
        s = project_getidx(s, &elements_btnssnd.val, project.sounds, -1);
        s = project_getidx(s, &elements_btnpsnd.val, project.sounds, -1);
        s = project_getidx(s, &elements_errsnd.val, project.sounds, -1);
        s = project_skipnl(s);
        for(i = 0; i < (int)(sizeof(elements_sprites)/sizeof(elements_sprites[0])); i++) {
            s = project_getsprite(s, &elements_sprites[i]);
            s = project_skipnl(s);
        }
        free(str);
    } else
        ui_status(1, lang[ERR_LOADUI]);
}

/**
 * Load the currently set fonts
 */
void elements_loadfonts()
{
    int i;

    for(i = 0; i < 6; i++)
        ui_font_load(&elements_fonts[i], elements_style[i], elements_size[i].val, elements_fnt[i].val);
}

/**
 * Free the fonts
 */
void elements_freefonts()
{
    int i;

    for(i = 0; i < 6; i++)
        ui_font_free(&elements_fonts[i]);
}

/**
 * Return the maximum window border sizes
 */
void elements_winsizes(ui_tablehdr_t *hdr, int sel, int *w, int *top, int *right, int *bottom, int *left)
{
    ui_sprite_t *s;
    int ret, i, j, k;

    if(hdr && w) {
        for(i = ret = 0; hdr[i].label; i++) {
            ssfn_bbox(&elements_fonts[i == sel ? 0 : 1].ctx, lang[hdr[i].label], &hdr[i].width, &j, &k, &hdr[i].order);
            hdr[i].width += k;
            ret += hdr[i].width;
            s = spr_getsel(&elements_sprites[i == sel ? E_WINNL : E_WINIL], 0); ret += s->w;
            s = spr_getsel(&elements_sprites[i == sel ? E_WINNR : E_WINIR], 0); ret += s->w;
        }
        s = spr_getsel(&elements_sprites[E_WINTR], 0); ret += s->w;
        s = spr_getsel(&elements_sprites[E_WINTL], 0); ret += s->w;
        s = spr_getsel(&elements_sprites[E_WINML], 0); ret -= s->w;
        s = spr_getsel(&elements_sprites[E_WINMR], 0); ret -= s->w;
        *w = ret;
    }
    if(top) {
        for(ret = 0, i = E_WINNL; i <= E_WINTR; i++) {
            s = spr_getsel(&elements_sprites[i], 0);
            if(ret < s->h) ret = s->h;
        }
        *top = ret;
    }
    if(right) {
        s = spr_getsel(&elements_sprites[E_WINMR], 0);
        *right = s->w;
    }
    if(bottom) {
        s = spr_getsel(&elements_sprites[E_WINBL], 0); ret = s->h;
        s = spr_getsel(&elements_sprites[E_WINBBG], 0); if(ret < s->h) ret = s->h;
        s = spr_getsel(&elements_sprites[E_WINBR], 0); if(ret < s->h) ret = s->h;
        *bottom = ret;
    }
    if(left) {
        s = spr_getsel(&elements_sprites[E_WINML], 0);
        *left = s->w;
    }
}

/**
 * Display a window with ui elements
 */
void elements_window(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, int h, ui_tablehdr_t *hdr, int sel)
{
    ui_sprite_t *s;
    ssfn_buf_t buf;
    int i, k, l, p, tabw = 0, dx = x, dw = w, top;
    char *str;

    elements_winsizes(hdr, sel, &tabw, &top, NULL, NULL, NULL);
    s = spr_getsel(&elements_sprites[E_WINML], 0); dx -= s->w; dw += s->w; tabw += s->w;
    s = spr_getsel(&elements_sprites[E_WINMR], 0); dw += s->w; tabw += s->w;
    s = spr_getsel(&elements_sprites[E_WINTR], 0); dw -= s->w; tabw -= s->w;
    s = spr_getsel(&elements_sprites[E_WINTL], 0); dw -= s->w; tabw -= s->w;
    ui_blitbuf(dst, dx, y - s->h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    dx += s->w;
    s = spr_getsel(&elements_sprites[E_WINTBG], 0);
    if(tabw) {
        k = (dw - tabw) / 2;
        ui_blitbuf(dst, dx, y - s->h, k, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
        dx += k; dw -= k;
        for(i = 0; hdr[i].label; i++) {
            s = spr_getsel(&elements_sprites[i == sel ? E_WINNL : E_WINIL], 0);
            ui_blitbuf(dst, dx, y - s->h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
            dx += s->w; dw -= s->w;
            s = spr_getsel(&elements_sprites[i == sel ? E_WINNBG : E_WINIBG], 0);
            ui_blitbuf(dst, dx, y - s->h, hdr[i].width, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
            SDL_LockTexture(dst, NULL, (void**)&buf.ptr, &p);
            buf.p = p;
            buf.w = dstw;
            buf.h = dsth;
            buf.x = dx;
            buf.y = y - top + hdr[i].order + elements_pad[i == sel ? 0 : 1].val;
            buf.fg = elements_colors[i == sel ? 0 : 1];
            buf.bg = 0;
            str = lang[hdr[i].label];
            while(*str && ((l = ssfn_render(&elements_fonts[i == sel ? 0 : 1].ctx, &buf, str)) > 0 || l == SSFN_ERR_NOGLYPH))
                str += (l < 1 ? 1 : l);
            SDL_UnlockTexture(dst);
            dx += hdr[i].width; dw -= hdr[i].width;
            s = spr_getsel(&elements_sprites[i == sel ? E_WINNR : E_WINIR], 0);
            ui_blitbuf(dst, dx, y - s->h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
            dx += s->w; dw -= s->w;
        }
        s = spr_getsel(&elements_sprites[E_WINTBG], 0);
    }
    ui_blitbuf(dst, dx, y - s->h, dw, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    s = spr_getsel(&elements_sprites[E_WINTR], 0);
    ui_blitbuf(dst, dx + dw, y - s->h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    s = spr_getsel(&elements_sprites[E_WINML], 0);
    ui_blitbuf(dst, x - s->w, y, s->w, h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    s = spr_getsel(&elements_sprites[E_WINBG], 0);
    ui_blitbuf(dst, x, y, w, h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    s = spr_getsel(&elements_sprites[E_WINMR], 0);
    ui_blitbuf(dst, x + w, y, s->w, h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    dx = x; dw = w;
    s = spr_getsel(&elements_sprites[E_WINML], 0); dx -= s->w; dw += s->w;
    s = spr_getsel(&elements_sprites[E_WINMR], 0); dw += s->w;
    s = spr_getsel(&elements_sprites[E_WINBR], 0); dw -= s->w;
    s = spr_getsel(&elements_sprites[E_WINBL], 0); dw -= s->w;
    ui_blitbuf(dst, dx, y + h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    dx += s->w;
    s = spr_getsel(&elements_sprites[E_WINBBG], 0);
    ui_blitbuf(dst, dx, y + h, dw, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    s = spr_getsel(&elements_sprites[E_WINBR], 0);
    ui_blitbuf(dst, dx + dw, y + h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
}

/**
 * Display a dialog with ui elements
 */
void elements_dialog(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, int h, char *str, char *text, int pw)
{
    ssfn_buf_t buf;
    ui_sprite_t *s;
    int j, k, l, p, tx, tabw = 0, dx = x, dw = w, hw, hb;

    if(str && *str) {
        ssfn_bbox(&elements_fonts[2].ctx, str, &hw, &j, &k, &hb);
        tabw += hw;
        s = spr_getsel(&elements_sprites[E_DLGNL], 0); tabw += s->w;
        s = spr_getsel(&elements_sprites[E_DLGNR], 0); tabw += s->w;
    }
    s = spr_getsel(&elements_sprites[E_DLGML], 0); dx -= s->w; dw += s->w;
    s = spr_getsel(&elements_sprites[E_DLGMR], 0); dw += s->w;
    s = spr_getsel(&elements_sprites[E_DLGTR], 0); dw -= s->w;
    s = spr_getsel(&elements_sprites[E_DLGTL], 0); dw -= s->w;
    ui_blitbuf(dst, dx, y - s->h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    dx += s->w;
    s = spr_getsel(&elements_sprites[E_DLGTBG], 0);
    if(tabw) {
        k = s->w; if(pw < 0) k += -pw;
        ui_blitbuf(dst, dx, y - s->h, k, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
        dx += k; dw -= k;
        s = spr_getsel(&elements_sprites[E_DLGNL], 0);
        ui_blitbuf(dst, dx, y - s->h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
        dx += s->w; dw -= s->w;
        s = spr_getsel(&elements_sprites[E_DLGNBG], 0);
        ui_blitbuf(dst, dx, y - s->h, hw, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
        SDL_LockTexture(dst, NULL, (void**)&buf.ptr, &p);
        buf.p = p;
        buf.w = dstw;
        buf.h = dsth;
        buf.x = dx;
        buf.y = y - s->h + hb + elements_pad[2].val;
        buf.fg = elements_colors[2];
        buf.bg = 0;
        while(*str && ((l = ssfn_render(&elements_fonts[2].ctx, &buf, str)) > 0 || l == SSFN_ERR_NOGLYPH)) str += (l < 1 ? 1 : l);
        SDL_UnlockTexture(dst);
        dx += hw; dw -= hw;
        s = spr_getsel(&elements_sprites[E_DLGNR], 0);
        ui_blitbuf(dst, dx, y - s->h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
        dx += s->w; dw -= s->w;
        s = spr_getsel(&elements_sprites[E_DLGTBG], 0);
    }
    ui_blitbuf(dst, dx, y - s->h, dw, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    s = spr_getsel(&elements_sprites[E_DLGTR], 0);
    ui_blitbuf(dst, dx + dw, y - s->h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    s = spr_getsel(&elements_sprites[E_DLGML], 0);
    ui_blitbuf(dst, x - s->w, y, s->w, h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    s = spr_getsel(&elements_sprites[E_DLGBG], 0);
    ui_blitbuf(dst, x, y, w, h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    SDL_LockTexture(dst, NULL, (void**)&buf.ptr, &p);
    buf.p = p;
    buf.w = dstw;
    buf.h = dsth;
    buf.x = tx = x + elements_pad[4].val + (pw < 0 ? -pw : 0);
    buf.y = y + elements_size[3].val + elements_pad[3].val;
    buf.fg = elements_colors[3];
    buf.bg = 0;
    str = text;
    j = ui_clip.w; ui_clip.w = x + w - (pw > 0 ? pw : 0);
    while(str && *str) {
        if(*str == '\n') {
            buf.x = tx;
            buf.y += elements_size[3].val;
            str++;
        } else
        if((l = ssfn_render(&elements_fonts[3].ctx, &buf, str)) > 0 || l == SSFN_ERR_NOGLYPH) str += (l < 1 ? 1 : l);
    }
    ui_clip.w = j;
    SDL_UnlockTexture(dst);

    s = spr_getsel(&elements_sprites[E_DLGMR], 0);
    ui_blitbuf(dst, x + w, y, s->w, h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    dx = x; dw = w;
    s = spr_getsel(&elements_sprites[E_DLGML], 0); dx -= s->w; dw += s->w;
    s = spr_getsel(&elements_sprites[E_DLGMR], 0); dw += s->w;
    s = spr_getsel(&elements_sprites[E_DLGBR], 0); dw -= s->w;
    s = spr_getsel(&elements_sprites[E_DLGBL], 0); dw -= s->w;
    ui_blitbuf(dst, dx, y + h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    dx += s->w;
    s = spr_getsel(&elements_sprites[E_DLGBBG], 0);
    ui_blitbuf(dst, dx, y + h, dw, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    s = spr_getsel(&elements_sprites[E_DLGBR], 0);
    ui_blitbuf(dst, dx + dw, y + h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
}

/**
 * Display a dialog option with ui elements
 */
int elements_dlgopt(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, int h, char **opts, int no, int sel, int pw)
{
    ssfn_buf_t buf;
    ui_sprite_t *s;
    int i, ih, k, l, p, dx, hb, m;
    char *str;

    if(!opts || no < 1) return 0;
    s = spr_getsel(&elements_sprites[E_DLGOBG], 0); ih = s->h;
    s = spr_getsel(&elements_sprites[E_DLGOR], 0); w -= s->w; if(ih < s->h) ih = s->h;
    s = spr_getsel(&elements_sprites[E_DLGOL], 0); w -= s->w; if(ih < s->h) ih = s->h;
    x += elements_pad[4].val; w -= 2 * elements_pad[4].val;
    y += elements_pad[3].val; h -= 2 * elements_pad[3].val;
    y = y + h - no * ih - (no - 1) * 10;
    if(pw < 0) { x -= pw; w += pw; } else w -= pw;
    for(i = 0, dx = x; i < no && opts[i]; i++, y += ih + 10, dx = x) {
        s = spr_getsel(&elements_sprites[E_DLGOL], 0);
        ui_blitbuf(dst, dx, y, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
        dx += s->w;

        s = spr_getsel(&elements_sprites[E_DLGOBG], 0);
        m = s->h < elements_size[i == sel ? 4 : 3].val ? ih : s->h;
        ui_blitbuf(dst, dx, y, w, m, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

        str = opts[i];
        ssfn_bbox(&elements_fonts[i == sel ? 4 : 3].ctx, str, &p, &l, &k, &hb);
        SDL_LockTexture(dst, NULL, (void**)&buf.ptr, &p);
        buf.p = p;
        buf.w = dstw;
        buf.h = dsth;
        buf.x = dx + elements_pad[4].val;
        buf.y = y + (m - elements_size[i == sel ? 4 : 3].val) / 2 + hb;
        buf.fg = elements_colors[i == sel ? 4 : 3];
        buf.bg = 0;
        while(*str && ((l = ssfn_render(&elements_fonts[i == sel ? 4 : 3].ctx, &buf, str)) > 0 || l == SSFN_ERR_NOGLYPH))
            str += (l < 1 ? 1 : l);
        SDL_UnlockTexture(dst);

        s = spr_getsel(&elements_sprites[E_DLGOR], 0);
        ui_blitbuf(dst, dx + w, y, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    }
    return ih;
}

/**
 * Calculate dialog's size and position
 */
void elements_dlgsize(int dstw, int dsth, int nl, int no, int *x, int *y, int *w, int *h)
{
    int dh, i;
    ui_sprite_t *s;

    s = spr_getsel(&elements_sprites[E_DLGOBG], 0); dh = s->h;
    s = spr_getsel(&elements_sprites[E_DLGOR], 0); if(dh < s->h) dh = s->h;
    s = spr_getsel(&elements_sprites[E_DLGOL], 0); if(dh < s->h) dh = s->h;
    *w = dstw * 3 / 4;
    *h = nl * elements_size[3].val + 2 * elements_pad[3].val + no * (dh + 10) + (no > 0 ? elements_pad[3].val : 0);
    s = spr_getsel(&elements_sprites[E_DLGBL], 0); i = s->h;
    s = spr_getsel(&elements_sprites[E_DLGBBG], 0); if(i < s->h) i = s->h;
    s = spr_getsel(&elements_sprites[E_DLGBR], 0); if(i < s->h) i = s->h;
    s = spr_getsel(&elements_sprites[E_INPL], 0);
    *x = (dstw - *w) / 2; *y = dsth - *h - 10 - s->h - i;
}

/**
 * Cut string to fit into dialog
 */
char *elements_dlgstr(char *str, int dstw, int pw, int fnt, int *nl)
{
    ssfn_buf_t buf;
    char *ret, *d, *s, *o, *last = NULL;
    int l, w = dstw - 2 * elements_pad[4].val - (pw < 0 ? -pw : pw);

    *nl = 1;
    if(!str || !*str || w < 16) return NULL;
    s = o = project_parsestr(str);
    if(!s) return NULL;
    memset(&buf, 0, sizeof(ssfn_buf_t));
    buf.w = dstw; buf.h = elements_size[fnt].val;
    ret = d = (char*)main_alloc(2 * strlen(s) + 1);
    while(*s) {
        if(*s == ' ') { last = d; }
        l = ssfn_render(&elements_fonts[fnt].ctx, &buf, s);
        if(l == SSFN_ERR_NOGLYPH) l = 1;
        if(l < 0) break;
        if(buf.x > w) {
            (*nl) += 1;
            buf.x = 0;
            if(last) {
                *last++ = '\n';
                s -= d - last;
                d = last;
                last = NULL;
                continue;
            } else {
                *d++ = '\n';
            }
        }
        memcpy(d, s, l);
        d += l;
        s += l;
    }
    free(o);
    return ret;
}

/**
 * Display a button with ui elements
 */
void elements_button(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, char *str, int type)
{
    ssfn_buf_t buf;
    ui_sprite_t *s;
    int ih, k, l, p, hb;

    s = spr_getsel(&elements_sprites[type * 3 + E_BTNNR], 0); w -= s->w; ih = s->h;
    s = spr_getsel(&elements_sprites[type * 3 + E_BTNNBG], 0); if(ih < s->h) ih = s->h;
    s = spr_getsel(&elements_sprites[type * 3 + E_BTNNL], 0); w -= s->w; if(ih < s->h) ih = s->h;
    ui_blitbuf(dst, x, y - s->h / 2, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    x += s->w;

    s = spr_getsel(&elements_sprites[type * 3 + E_BTNNBG], 0);
    ui_blitbuf(dst, x, y - s->h / 2, w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    if(str) {
        ssfn_bbox(&elements_fonts[5].ctx, str, &l, &p, &k, &hb);
        SDL_LockTexture(dst, NULL, (void**)&buf.ptr, &p);
        buf.p = p;
        buf.w = dstw;
        buf.h = dsth;
        buf.x = x + (w - l) / 2;
        buf.y = y - (ih - elements_size[5].val) / 2 + hb + elements_pad[5].val + (type == 2 ? 1 : 0);
        buf.fg = elements_colors[type + 5];
        buf.bg = 0;
        while(*str && ((l = ssfn_render(&elements_fonts[5].ctx, &buf, str)) > 0 || l == SSFN_ERR_NOGLYPH)) str += (l < 1 ? 1 : l);
        SDL_UnlockTexture(dst);
    }

    s = spr_getsel(&elements_sprites[type * 3 + E_BTNNR], 0);
    ui_blitbuf(dst, x + w, y - s->h / 2, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
}

/**
 * Display an option box with ui elements
 */
int elements_option(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, char **opts, int type, int sel, int idx)
{
    ssfn_buf_t buf;
    ui_sprite_t *s;
    int ih, k, l, p, hb, m, tw = w < 0 ? -w : w;
    char *str;

    s = spr_getsel(&elements_sprites[type == 2 ? E_OPTNEXTPRESS : E_OPTNEXT], 0); tw -= s->w;
    s = spr_getsel(&elements_sprites[type == 1 ? E_OPTPREVPRESS : E_OPTPREV], 0); ih = s->h; tw -= s->w;
    if(!dst) return ih;
    ui_blitbuf(dst, x, y, s->w, ih, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    x += s->w;

    s = spr_getsel(&elements_sprites[E_OPTBG], 0);
    m = s->h < elements_size[idx].val ? ih : s->h;
    ui_blitbuf(dst, x, y + (ih - m) / 2, tw, m, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    if(opts && opts[sel]) {
        str = opts[sel];
        ssfn_bbox(&elements_fonts[idx].ctx, str, &l, &p, &k, &hb);
        SDL_LockTexture(dst, NULL, (void**)&buf.ptr, &p);
        buf.p = p;
        buf.w = dstw;
        buf.h = dsth;
        buf.x = x + (w < 0 ? tw - elements_pad[4].val - l : elements_pad[4].val);
        buf.y = y + (m - elements_size[idx].val) / 2 + hb;
        buf.fg = elements_colors[idx];
        buf.bg = 0;
        while(*str && ((l = ssfn_render(&elements_fonts[idx].ctx, &buf, str)) > 0 || l == SSFN_ERR_NOGLYPH)) str += (l < 1 ? 1 : l);
        SDL_UnlockTexture(dst);
    }

    s = spr_getsel(&elements_sprites[type == 2 ? E_OPTNEXTPRESS : E_OPTNEXT], 0);
    ui_blitbuf(dst, x + tw, y + (ih - s->h) / 2, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    return ih;
}

/**
 * Display text ui elements
 */
int elements_text(SDL_Texture *dst, int dstw, int dsth, int x, int y, int w, int h, char *str, int type)
{
    ssfn_buf_t buf;
    int k, l = 0, p, hb, t = type < 0 ? -type : type;

    if(str) {
        if(!h) h = elements_size[t].val;
        ssfn_bbox(&elements_fonts[t].ctx, str, &l, &p, &k, &hb);
        SDL_LockTexture(dst, NULL, (void**)&buf.ptr, &p);
        buf.ptr += 4 * (x + (type < 0 ? (w - l) / 2 : 0));
        buf.p = p;
        buf.w = w < dstw - x ? w : dstw - x;
        buf.h = dsth;
        buf.x = 0;
        buf.y = y + (h - elements_size[t].val) / 2 + hb;
        buf.fg = elements_colors[t];
        buf.bg = 0;
        elements_fonts[t].ctx.style &= ~SSFN_STYLE_A;
        while(*str && ((k = ssfn_render(&elements_fonts[t].ctx, &buf, str)) > 0 || k == SSFN_ERR_NOGLYPH))
            str += (k < 1 ? 1 : k);
        elements_fonts[t].ctx.style |= SSFN_STYLE_A;
        SDL_UnlockTexture(dst);
    }
    return l;
}

/**
 * Display a slider with ui elements
 */
void elements_slider(SDL_Texture *dst, int x, int y, int w, int pos)
{
    ui_sprite_t *s;
    int ih;

    s = spr_getsel(&elements_sprites[E_SLDR], 0); ih = s->h; w -= s->w;
    s = spr_getsel(&elements_sprites[E_SLDBG], 0); if(ih < s->h) ih = s->h;
    s = spr_getsel(&elements_sprites[E_SLDBTN], 0); if(ih < s->h) ih = s->h;
    s = spr_getsel(&elements_sprites[E_SLDL], 0); w -= s->w; if(ih < s->h) ih = s->h;
    ui_blitbuf(dst, x, y + (ih - s->h) / 2, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    x += s->w;

    s = spr_getsel(&elements_sprites[E_SLDBG], 0);
    ui_blitbuf(dst, x, y + (ih - s->h) / 2, w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    s = spr_getsel(&elements_sprites[E_SLDR], 0);
    ui_blitbuf(dst, x + w, y + (ih - s->h) / 2, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    s = spr_getsel(&elements_sprites[E_SLDBTN], 0);
    ui_blitbuf(dst, x + pos, y + (ih - s->h) / 2, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
}

/**
 * Return scrollbar sizes
 */
int elements_vscrsize()
{
    ui_sprite_t *s = spr_getsel(&elements_sprites[E_SCRD], 0);
    return s->w;
}

/**
 * Display a vertical scrollbar
 */
void elements_vscr(SDL_Texture *dst, int x, int y, int h, int r)
{
    int i;
    ui_sprite_t *s;

    s = spr_getsel(&elements_sprites[E_SCRD], 0); h -= s->h; i = s->w;
    if(r) x -= s->w;
    ui_blitbuf(dst, x - i, y + h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    s = spr_getsel(&elements_sprites[E_SCRU], 0); h -= s->h;
    ui_blitbuf(dst, x - i, y, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    y += s->h;

    s = spr_getsel(&elements_sprites[E_SCRVBG], 0);
    ui_blitbuf(dst, x - i + (i - s->w) / 2, y, /*i*/s->w, h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    s = spr_getsel(&elements_sprites[E_SCRBTN], 0);
    ui_blitbuf(dst, x - i + (i - s->w) / 2, y + s->h, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
}

/**
 * Display a horizontal scrollbar
 */
void elements_hscr(SDL_Texture *dst, int x, int y, int w)
{
    int i;
    ui_sprite_t *s;

    s = spr_getsel(&elements_sprites[E_SCRR], 0); w -= s->w; i = s->h;
    ui_blitbuf(dst, x + w, y - i, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    s = spr_getsel(&elements_sprites[E_SCRL], 0); w -= s->w;
    ui_blitbuf(dst, x, y - i, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    x += s->w;

    s = spr_getsel(&elements_sprites[E_SCRHBG], 0);
    ui_blitbuf(dst, x, y - i / 2 - s->h / 2, w, /*i*/s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);

    s = spr_getsel(&elements_sprites[E_SCRBTN], 0);
        ui_blitbuf(dst, x + s->w, y - i / 2 - s->h / 2, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
}

/**
 * Display a checkbox
 */
int elements_checkbox(SDL_Texture *dst, int x, int y, int chk)
{
    ui_sprite_t *s = spr_getsel(&elements_sprites[E_CHK0 + chk], 0);
    ui_blitbuf(dst, x, y, s->w, s->h, s->data, 0, 0, s->w, s->h, s->w * s->nframe * 4);
    return s->h;
}

/**
 * Preview ui elements
 */
void elements_preview(void *data)
{
    Mix_Chunk *selected = NULL, *pressed = NULL;
    SDL_DisplayMode dm;
    SDL_Event event;
    SDL_Texture *bg = NULL, *texture = NULL, *csrtxt = NULL;
    SDL_Rect win, dlg, src, dst, rect, clk[4];
    int wf = SDL_GetWindowFlags(window), i, j, k, x, y, p;
    int ih = 0, iw, ow = 0, hb, dh = 1, chk = 0, sel = 0, btn = 0, over = -1, last = -1, clicked = 0, csr_nf = 0, csr_last = -1;
    char *dlgopts[4];
    uint32_t *pixels, c;
    ui_sprite_t *s;
    ui_tablehdr_t hdr[] = {
        { ELEMENTS_NORMAL, 0, 0, 0 },
        { ELEMENTS_INACTIVE, 0, 0, 0 },
        { 0 }
    };

    (void)data;
    if(verbose) printf("elements_preview: started\n");

    SDL_GetDesktopDisplayMode(0, &dm);
    SDL_GetWindowPosition(window, &i, &j);
    SDL_GetMouseState(&x, &y);
    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    SDL_SetCursor(cursors[CURSOR_PTR]);
    if(elements_sprites[E_PTR].val >= 0) SDL_ShowCursor(SDL_DISABLE);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
    ui_clip.x = ui_clip.y = 0; ui_clip.w = dm.w; ui_clip.h = dm.h;
    win.x = win.y = win.w = win.h = 0;
    elements_loadfonts();
    SDL_WarpMouseInWindow(window, x + i, y + j);

    if(elements_btnssnd.val >= 0 || elements_btnpsnd.val >= 0) {
        if(Mix_OpenAudio(FREQ, AUDIO_S16LSB, 2, 4096) < 0)
            ui_status(1, lang[ERR_AUDIO]);
        else {
            if(elements_btnssnd.val >= 0) {
                sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 1],
                    project.sounds[elements_btnssnd.val]);
                selected = Mix_LoadWAV_RW(SDL_RWFromFile(projdir, "rb"), 1);
                if(selected) Mix_VolumeChunk(selected, MIX_MAX_VOLUME);
            }
            if(elements_btnpsnd.val >= 0) {
                sprintf(projfn, "%s" SEP "%s" SEP "%s.ogg", project.id, project_dirs[PROJDIRS_MEDIA + 1],
                    project.sounds[elements_btnpsnd.val]);
                pressed = Mix_LoadWAV_RW(SDL_RWFromFile(projdir, "rb"), 1);
                if(pressed) Mix_VolumeChunk(pressed, MIX_MAX_VOLUME);
            }
        }
    }

    s = spr_getsel(&elements_sprites[E_PTR], 0);
    src.y = 0;
    SDL_GetMouseState(&dst.x, &dst.y);

    bg = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
    if(bg) {
        SDL_LockTexture(bg, NULL, (void**)&pixels, &p);
        /* checker */
        for(j = k = 0; j < dm.h; j++, k += p/4)
            for(i = 0; i < dm.w ; i++) {
                x = (i >> 5); y = (j >> 5);
                c = theme[(y & 1) ^ (x & 1) ? THEME_LIGHT : THEME_DARK];
                if(x >= 1 && x < 3) {
                    if(y >= 1 && y < 3) c += 0x1F0000; else
                    if(y >= 4 && y < 6) c += 0x001F00; else
                    if(y >= 7 && y < 9) c += 0x00001F;
                }
                pixels[k + i] = c;
            }
        SDL_UnlockTexture(bg);

        /* draw window */
        win.w = (dm.w / 2) & ~1; win.x = (dm.w - win.w) / 2; win.y = 0;
        ow = win.w / 2 - elements_pad[4].val;
        for(i = E_WINNR; i <= E_WINTR; i++) {
            s = spr_getsel(&elements_sprites[i], 0);
            if(s->h > win.y) win.y = s->h;
        }
        win.y += 20;
        ih = elements_size[3].val > elements_size[4].val ? elements_size[3].val : elements_size[4].val;
        s = spr_getsel(&elements_sprites[E_CHK0], 0); if(ih < s->h) ih = s->h;
        clk[0].x = clk[1].x = dm.w / 2; clk[0].y = win.y + elements_pad[3].val; clk[0].w = s->w; clk[0].h = s->h;
        s = spr_getsel(&elements_sprites[E_OPTPREV], 0); if(ih < s->h) ih = s->h;
        clk[1].w = s->w; clk[1].h = s->h;
        s = spr_getsel(&elements_sprites[E_SLDBTN], 0); if(ih < s->h) ih = s->h;
        s = spr_getsel(&elements_sprites[E_BTNNL], 0); if(ih < s->h) ih = s->h;
        clk[1].y = clk[2].y = clk[0].y + ih + 10;
        s = spr_getsel(&elements_sprites[E_SCRL], 0);
        win.h = s->h + 4 * (ih + 10) + 3 * elements_pad[3].val;
        elements_window(bg, dm.w, dm.h, win.x, win.y, win.w, win.h, hdr, 0);
        j = s->h + 3 * (ih + 10) + elements_pad[3].val;
        s = spr_getsel(&elements_sprites[E_SCRR], 0);
        elements_vscr(bg, win.x + win.w - elements_pad[4].val, win.y + elements_pad[3].val, j - s->h, 0);
        s = spr_getsel(&elements_sprites[E_SCRD], 0); ow -= 10 + s->w;
        elements_hscr(bg, win.x + elements_pad[4].val, win.y + elements_pad[3].val + j, win.w - s->w - 2 * elements_pad[4].val);
        s = spr_getsel(&elements_sprites[E_OPTNEXT], 0);
        clk[2].w = s->w; clk[2].h = s->h; clk[2].x = dm.w / 2 + ow - s->w;
        ssfn_bbox(&elements_fonts[5].ctx, lang[ELEMENTS_INACTIVE], &iw, &j, &p, &hb);
        iw += 2 * elements_pad[4].val;
        s = spr_getsel(&elements_sprites[E_BTNIR], 0); iw += s->w;
        s = spr_getsel(&elements_sprites[E_BTNIL], 0); iw += s->w;
        elements_button(bg, dm.w, dm.h, win.x + (win.w / 2 - iw) / 2, win.y + win.h - elements_pad[3].val - ih / 2, iw,
            lang[ELEMENTS_INACTIVE], 3);

        /* draw dialog */
        elements_dlgsize(dm.w, dm.h, 1, 3, &dlg.x, &dlg.y, &dlg.w, &dlg.h);
        elements_dialog(bg, dm.w, dm.h, dlg.x, dlg.y, dlg.w, dlg.h, lang[ELEMENTS_DLGNAME], lang[ELEMENTS_DLGTXT],
            portrait_icon.w);
        elements_button(bg, dm.w, dm.h, dlg.x, dm.h - (dm.h - dlg.y - dlg.h) / 2, dlg.w, NULL, 4);
    }
    dlgopts[0] = lang[ELEMENTS_DLGOPT1];
    dlgopts[1] = lang[ELEMENTS_DLGOPT2];
    dlgopts[2] = lang[ELEMENTS_DLGOPT3];
    dlgopts[3] = NULL;

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, dm.w, dm.h);
redraw:
    if(texture) {
        /* clear */
        SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
        SDL_LockTexture(texture, NULL, (void**)&pixels, &p);
        memset(pixels, 0, p * dm.h);
        SDL_UnlockTexture(texture);
        /* window elements */
        elements_text(texture, dm.w, dm.h, win.x + elements_pad[4].val, win.y + elements_pad[3].val,
            win.w / 2 - elements_pad[4].val, ih, lang[ELEMENTS_WINCHK], over == 0 ? 4 : 3);
        elements_checkbox(texture, dm.w / 2, win.y + elements_pad[3].val, chk);

        elements_text(texture, dm.w, dm.h, win.x + elements_pad[4].val, win.y + elements_pad[3].val + ih + 10,
            win.w / 2 - elements_pad[4].val, ih, lang[ELEMENTS_OPTION], over == 1 ? 4 : 3);
        elements_option(texture, dm.w, dm.h, dm.w / 2, win.y + elements_pad[3].val + ih + 10, ow, dlgopts, btn, sel,
            over == 1 ? 4 : 3);

        elements_text(texture, dm.w, dm.h, win.x + elements_pad[4].val, win.y + elements_pad[3].val + 2 * (ih + 10),
            ow, ih, lang[ELEMENTS_SLIDER], over == 2 ? 4 : 3);
        elements_slider(texture, dm.w / 2, win.y + elements_pad[3].val + 2 * (ih + 10), ow, ow / 3);

        ssfn_bbox(&elements_fonts[5].ctx, lang[ELEMENTS_NORMAL], &iw, &j, &p, &hb);
        iw += 2 * elements_pad[4].val;
        j = btn == 3 ? 6 : (over == 3 ? 3 : 0);
        s = spr_getsel(&elements_sprites[j + E_BTNNR], 0); iw += s->w;
        s = spr_getsel(&elements_sprites[j + E_BTNNL], 0); iw += s->w;
        clk[3].x = dm.w / 2 + (win.w / 2 - iw) / 2; clk[3].y = win.y + win.h - elements_pad[3].val - ih;
        clk[3].w = iw; clk[3].h = s->h;
        elements_button(texture, dm.w, dm.h, dm.w / 2 + (win.w / 2 - iw) / 2, win.y + win.h - elements_pad[3].val -ih / 2, iw,
            lang[ELEMENTS_NORMAL], btn == 3 ? 2 : (over == 3 ? 1 : 0));
        /* dialog elements */
        dh = elements_dlgopt(texture, dm.w, dm.h, dlg.x, dlg.y, dlg.w, dlg.h, dlgopts, 3, over - 4, portrait_icon.w);
    }

    while(1) {
        /* events */
        SDL_WaitEvent(&event);
        if(event.type == SDL_QUIT || (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)) {
            SDL_PushEvent(&event); goto cleanup;
        }
        switch(event.type) {
            case SDL_KEYUP: goto cleanup;
            case SDL_MOUSEBUTTONDOWN:
                if(event.button.button != 1) { btn = -1; break; }
                btn = 0; clicked = 1;
                for(i = 0; i < 4; i++)
                    if(dst.x > clk[i].x && dst.x < clk[i].x + clk[i].w && dst.y > clk[i].y && dst.y < clk[i].y + clk[i].h) {
                        if(!i) chk ^= 1;
                        if(i == 1 && sel > 0) sel--;
                        if(i == 2 && sel < 2) sel++;
                        if(i == 3 && pressed) Mix_PlayChannelTimed(1, pressed, 0, -1);
                        btn = i;
                        event.type = SDL_WINDOWEVENT;
                        event.window.event = SDL_WINDOWEVENT_EXPOSED;
                        event.window.windowID = SDL_GetWindowID(window);
                        SDL_PushEvent(&event);
                        goto redraw;
                    }
            break;
            case SDL_MOUSEBUTTONUP:
                clicked = 0;
                if(btn) {
                    btn = 0;
                    event.type = SDL_WINDOWEVENT;
                    event.window.event = SDL_WINDOWEVENT_EXPOSED;
                    event.window.windowID = SDL_GetWindowID(window);
                    SDL_PushEvent(&event);
                    goto redraw;
                }
                if(event.button.x < win.x || event.button.x > win.x + win.w ||
                  event.button.y < win.y || event.button.y > win.y + win.h) goto cleanup;
            break;
            case SDL_MOUSEMOTION:
                dst.x = event.motion.x; dst.y = event.motion.y; over = -1;
                if(dst.x > win.x && dst.x < win.x + win.w && dst.y > win.y + elements_pad[3].val && dst.y < win.y + win.h) {
                    i = win.y + elements_pad[3].val + 3 * (ih + 10);
                    if(dst.y < i) over = (dst.y - win.y - elements_pad[3].val) / (ih + 10);
                    else if(dst.x > clk[3].x && dst.x < clk[3].x + clk[3].w &&
                        dst.y > clk[3].y && dst.y < clk[3].y + clk[3].h) over = 3;
                }
                if(dst.x > dlg.x && dst.x < dlg.x + dlg.w && dst.y > dlg.y && dst.y < dlg.y + dlg.h) {
                    i = dlg.y + dlg.h - elements_pad[3].val - 3 * (dh + 10);
                    if(dst.y > i) over = (dst.y - i) / (dh + 10) + 4;
                }
                if(over != last) {
                    if(over != -1 && selected) Mix_PlayChannelTimed(1, selected, 0, -1);
                    last = over;
                    goto redraw;
                }
            break;
            case SDL_USEREVENT:
                /* don't redraw if the cursor isn't animated */
                if(csr_nf < 2) continue;
            break;
        }
        /* render */
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
        SDL_RenderClear(renderer);
        /* windows */
        if(bg) SDL_RenderCopy(renderer, bg, NULL, NULL);
        if(texture) SDL_RenderCopy(renderer, texture, NULL, NULL);
        /* portrait */
        rect.w = portrait_icon.w; rect.h = portrait_icon.h;
        rect.x = dlg.x + dlg.w - rect.w; rect.y = dlg.y + dlg.h - rect.h;
        SDL_RenderCopy(renderer, icons, &portrait_icon, &rect);
        if(btn == -1)
            for(i = 0; i < 4; i++)
                SDL_RenderCopy(renderer, spr_select, NULL, &clk[i]);
        /* cursor */
        i = E_PTR + (elements_sprites[E_CLK].val >= 0 ? clicked : 0);
        if(dst.x >= 32 && dst.x < 96) {
            if(dst.y >= 32 && dst.y < 96) i = E_NOALLOW;
            if(dst.y >= 128 && dst.y < 192) i = E_ACTION;
            if(dst.y >= 224 && dst.y < 288) i = E_LOADING;
        }
        if(i != E_PTR || elements_sprites[E_PTR].val >= 0) {
            s = spr_getsel(&elements_sprites[i], 0);
            csr_nf = s->nframe;
            src.w = dst.w = s->w; src.h = dst.h = s->h; dst.x -= dst.w / 2; dst.y -= dst.h / 2;
            src.x = ui_anim(s->type, s->nframe, -1) * s->w;
            if(i != csr_last) {
                csr_last = i;
                spr_texture(s, &csrtxt);
            }
            if(csrtxt)
                SDL_RenderCopy(renderer, csrtxt, &src, &dst);
            dst.x += dst.w / 2; dst.y += dst.h / 2;
        }
        SDL_RenderPresent(renderer);
    }

cleanup:
    if(elements_btnssnd.val >= 0 || elements_btnpsnd.val >= 0) {
        if(Mix_Playing(-1)) Mix_HaltChannel(-1);
        if(selected) Mix_FreeChunk(selected);
        if(pressed) Mix_FreeChunk(pressed);
        Mix_CloseAudio();
    }
    if(csrtxt) SDL_DestroyTexture(csrtxt);
    if(texture) SDL_DestroyTexture(texture);
    if(bg) SDL_DestroyTexture(bg);
    elements_freefonts();
    if(verbose) printf("elements_preview: stopped\n");
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetWindowFullscreen(window, 0);
    if(wf & SDL_WINDOW_MAXIMIZED) SDL_MaximizeWindow(window);
}

/**
 * Save tng
 */
int elements_totng(tngctx_t *ctx)
{
    int i;

    if(!ctx) return 1;
    elements_load();
    tng_section(ctx, TNG_SECTION_UI);
    i = 10; tng_data(ctx, &i, 1);
    for(i = 0; i < 10; i++) tng_data(ctx, &elements_colors[i], 4);
    i = 6; tng_data(ctx, &i, 1);
    for(i = 0; i < 6; i++) {
        tng_font(ctx, elements_style[i], elements_size[i].val, elements_fnt[i].val);
        tng_data(ctx, &elements_pad[i].val, 2);
    }
    i = 3; tng_data(ctx, &i, 1);
    tng_ref(ctx, TNG_IDX_SND, elements_btnssnd.val);
    tng_idx(ctx, project.sounds, elements_btnssnd.val);
    tng_ref(ctx, TNG_IDX_SND, elements_btnpsnd.val);
    tng_idx(ctx, project.sounds, elements_btnpsnd.val);
    tng_ref(ctx, TNG_IDX_SND, elements_errsnd.val);
    tng_idx(ctx, project.sounds, elements_errsnd.val);
    for(i = 0; i < (int)(sizeof(elements_sprites)/sizeof(elements_sprites[0])); i++)
        tng_sprite(ctx, elements_sprites[i].cat, elements_sprites[i].val);
    return 1;
}

/**
 * Read from tng
 */
int elements_fromtng(tngctx_t *ctx)
{
    FILE *f;
    uint8_t *buf, *end;
    int i, l, len;

    if(!ctx || !ctx->tbl || !ctx->numtbl) return 0;
    for(i = 0; i < ctx->numtbl && TNG_SECTION_TYPE(&ctx->tbl[i]) != TNG_SECTION_UI; i++);
    if(i >= ctx->numtbl) return 1;
    ui_progressbar(0, 0, ctx->curr++, ctx->total, LANG_EXTRACTING);
    buf = (uint8_t*)(ctx->sec + ctx->tbl[i].offs);
    len = TNG_SECTION_SIZE(&ctx->tbl[i]);
    end = buf + len;
    if(len < 4) return 0;
    f = project_savefile(0, project_dirs[PROJDIRS_UI], "elements", "cfg", PROJMAGIC_ELEMENTS, "elements_fromtng");
    if(f) {
        len = *buf++; l = len < 10 ? len : 10;
        for(i = 0; i < l; i++) {
            if(i < 10) fprintf(f, "%s%08X", i ? " " : "", i < len ? *((uint32_t*)buf) : 0);
            if(i < len) buf += 4;
        }
        fprintf(f, "\r\n");
        len = *buf++; l = len < 6 ? len : 6;
        for(i = 0; i < l; i++) {
            if(i < 6 && i < len) {
                buf = tng_wrfont(ctx, buf, f);
                fprintf(f, " %d\r\n", *((int16_t*)buf)); buf += 2;
            } else {
                if(i < 6) fprintf(f, ".. 16 - 0\r\n");
                if(i < len) buf += 7;
            }
        }
        len = *buf++; l = len < 3 ? len : 3;
        for(i = 0; i < l; i++) {
            if(i) fprintf(f, " ");
            if(i < 3 && i < len) {
                buf = tng_wridx(ctx, buf, f);
            } else {
                if(i < 3) fprintf(f, "-");
                if(i < len) buf += 3;
            }
        }
        fprintf(f, "\r\n");
        l = (int)(sizeof(elements_sprites)/sizeof(elements_sprites[0]));
        for(i = 0; i < l && buf < end; i++) {
            buf = tng_wrsprite(ctx, buf, f);
            fprintf(f, "\r\n");
        }
        fclose(f);
        return 1;
    }
    return 0;
}
